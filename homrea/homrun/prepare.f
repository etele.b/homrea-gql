      subroutine PREPRO(ITRM,ndim,neq,nc,NZEV,nspec,lrv,nprop,HPROP,
     1     nvert,ncell,ICORD,IRET,IY,IVERT,n2dim,
     1     SYMPRO,OTMAT,
     1     ERVROW,ERVCOL,RPAR,IPAR,LRW,RW,liw,IW,ierr)
c***********************************************************************
c
c     Subroutine for preparation of ILDM output
c
c     input: HPROP = property field from -NEWGEN- contains
c                    ndim,Y,F,RV,Zs,(Zs)**-1 for each ILDM point
c            nprop = number of properties in HPROP
c            npropn= number of properties of PROPN
c            ndim  = dimension of ILDM
c            nspec = number of species
c            nc    = number of controlling variables (ne + ndim)
c            neq   = number of equations (nspec+2)
c            RV    = data field of thermophysical quantities
c            lrv   = length of RV
c            nvert = number of ILDM points
c            ncell = number of ILDM cells
c            ICORD,IVERT,IRET,IY = integer fields for grid tabulation
c            n2dim = number of points per cell (2**ndim)
c            SYMPRP= symbols for the head of the data file
c
c
c***********************************************************************
      implicit double precision(a-h,o-z)
      include 'homdim.f'
c***********************************************************************
c     Variables
c***********************************************************************
      logical test
      character(LEN=*) SYMPRO(NPROP)
      COMMON/BLOCPR/NIAUX,NISMF,NIPF,NIRV,NIPRO,NIDPT,
     1   NIDPSE,NIHFLA,NIICOR,NIEGVA,NPRONT
      COMMON/BLOCOR/IIRAT,IISMF,IISMFP,IIRV,IIPRO,IIPNU,IILAM,NPROOT
      DIMENSION KONS(NPROP+1)
c***********************************************************************
c     Arrays
c***********************************************************************
      DIMENSION IRET(NVERT),IY(NDIM,NCELL)
      DIMENSION IVERT(2**NDIM,NCELL)
      INTEGER, DIMENSION(NDIM,NVERT)                   :: ICORD
      DOUBLE PRECISION, DIMENSION(NPROP, NVERT)       :: HPROP
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:,:)   :: PROPN
      CHARACTER(LEN=LSYMB),   ALLOCATABLE, DIMENSION(:)     :: SYMPRP
      DIMENSION OTMAT(NC,NEQ)
      DIMENSION RW(LRW),IW(LIW)
      DIMENSION RPAR(*),IPAR(*)
      DOUBLE PRECISION, DIMENSION(LRV)            :: RV
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)        :: ERVROW,ERVCOL
c***********************************************************************
c     Initialize
c***********************************************************************
      CALL ISETUP(NDIM,NEQ,LRV,NS,NE,IEQ,NPROPN,IPAR,RPAR)
      IF(.NOT.ALLOCATED(PROPN))  ALLOCATE (PROPN(NPROPN,NVERM))
      IF(.NOT.ALLOCATED(SYMPRP)) ALLOCATE (SYMPRP(NPROPN))
      NREAC = IPAR(9)
      MIX = 1
      CALL TABSYM(NDIM,NC,NEQ,SYMPRO,LRV,NPROPN,SYMPRP)
c***********************************************************************
c     Initialize
c***********************************************************************
      CALL PREPRC(IEQ,NDIM,NEQ,NC,NZEV,NSPEC,LRV,NPROP,HPROP,
     1           NPROPN,NVERT,NCELL,ICORD,IRET,IY,IVERT,N2DIM,
     1           PROPN,OTMAT,
     1           ERVROW,ERVCOL,RPAR,IPAR,LRW,RW,LIW,IW,IERR,ITRM)
c***********************************************************************
c
c     Loop over all ILDM points
c
c***********************************************************************
      NUCALL = 0
      NPLP = NPROPN
      NCONS = 3
      KONS(1) = 0
      KONS(2) = 0
      KONS(3) = 0
cMG   WRITE(*,*) 'aaaaaaaaaaaa'
c     CALL PLTM(NUCALL,'I','N',NDIM,
c    1     NCELL,NVERT,NPLP,PROPN,NPROPN,IVERT,SYMPRP,NCONS,KONS,
c    1     LIW,IW,LRW,RW)
c     WRITE(*,*) 'aaaaaaaaaaaa'
c***********************************************************************
c
c     Loop over all ILDM points
c
c***********************************************************************
      NFI96 = 96
      open(NFI96,status='unknown')
      NSPEC = IPAR(3)
      CALL IOGPAR(1,NFI96,NPROPN,NCELL,NCELL,NVERT,NDIM,N2DIM,
     1            NSPEC,NREAC,IY,IVERT,ICORD,IRET,PROPN,SYMPRP,
     2            OTMAT,MIX,NC)
      CLOSE(NFI96,STATUS='KEEP')
C***********************************************************************
C     write tecplot file fort.1001
C***********************************************************************
      NFIPIL = 86
      OPEN(NFIPIL,STATUS='UNKNOWN')
      write(*,*)'NVERT,NCELL',NVERT,NCELL
      CALL XTPOUT(NFIPIL,NPROPN,SYMPRP,NVERT,NPROPN,PROPN,
     1     ICORD,NDIM,N2DIM,NCELL,IVERT)
      CLOSE(NFIPIL,STATUS='KEEP')
C***********************************************************************
C A.Neagos: Write output of SYMPROP and PROPERT into fort.88 by
C calling XTPOUT, subr. in evbres.f
C***********************************************************************
      CALL GRTHEOUT(NEQ,NPROPN,SYMPRP,NVERT,PROPN,
     1     ICORD,NDIM,N2DIM,NCELL,IVERT)
C***********************************************************************
C A.Neagos
C***********************************************************************
      RETURN
c***********************************************************************
c     Error exits
c***********************************************************************
  900 continue
      goto 999
  999 continue
      write(*,998)
  998 format(3(' ****   Error in - PREPRO -  ****',/))
      stop
c***********************************************************************
c     End of - PREPRO -
c***********************************************************************
      END
      subroutine PREPRC(ieq,ndim,neq,nc,NZEV,nspec,lrv,nprop,HPROP,
     1           npropn,nvert,ncell,ICORD,IRET,IY,IVERT,n2dim,
     1           PROPN,OTMAT,
     1           ERVROW,ERVCOL,RPAR,IPAR,LRW,RW,liw,IW,ierr,ITRM)
c************************************************************************
c                                                                       *
c     Subroutine for preparation of ILDM output                         *
c                                                                       *
c     input: HPROP = property field from -NEWGEN- contains              *
c                    ndim,Y,F,RV,Zs,(Zs)**-1 for each ILDM point        *
c            nprop = number of properties in HPROP                      *
c            npropn= number of properties of PROPN                      *
c            ndim  = dimension of ILDM                                  *
c            nspec = number of species                                  *
c            nc    = number of controlling variables (ne + ndim)        *
c            neq   = number of equations (nspec+2)                      *
c            RV    = data field of thermophysical quantities            *
c            lrv   = length of RV                                       *
c            nvert = number of ILDM points                              *
c            ncell = number of ILDM cells                               *
c            ICORD,IVERT,IRET,IY = integer fields for grid tabulation   *
c            n2dim = number of points per cell (2**ndim)                *
c            DIST  = distance between ILDM Points in each direction     *
c            SYMPRP= symbols for the head of the data file              *
c                                                                       *
c    output: PROPN = desired property field  contains                   *
c                                                                       *
c                    for fix  coordinates(ipara=1):                     *
c                    ndim,Y,CF,RV,CZs(Zs)**-1,DYeta,Dheta,flag          *
c                                                                       *
c                    for gen. coordinates(ipara=2):                     *
c                    ndim,Y,Y+F,RV,Y+Zs(Zs)**-1,DYtheta,Dheta,flag      *
c                                                                       *
c************************************************************************
      use RDMESH
      implicit double precision(a-h,o-z)
      include 'homdim.f'
c************************************************************************
c     Variables
c************************************************************************
      logical test
      EXTERNAL MASRES,MASJAC
      parameter (one = 1.0d0, zero = 0.0d0)
      common/BSSPV/SSPV(20*mnspe)
      COMMON/ILDMOP/IPARA,ISTAOP,MANPAR,NOFORM,ANAFUE,INMAN,lasloo,
     1              ICALTR,MASENS,IUPVER,NTRANS
      COMMON/BLOCPR/NIAUX,NISMF,NIPF,NIRV,NIPRO,NIDPT,
     1   NIDPSE,NIHFLA,NIICOR,NIEGVA,NPRONT
      COMMON/BLOCOR/IIRAT,IISMF,IISMFP,IIRV,IIPRO,IIPNU,IILAM,NPROOT
      COMMON/TTTEMP/TEMP1(MNSPE+2,1,NVERM),TEMP2(MNSPE+2,1,NVERM),
     1     TEMP3(MNSPE+2,1,NVERM)
c************************************************************************
c     Arrays
c************************************************************************
      dimension IRET(NVERT),IY(NDIM*NCELL)
      dimension IVERT(N2DIM*NCELL)
      dimension HPROP(NPROP,NVERT)
      DOUBLE PRECISION, DIMENSION(NPROPN,NVERT)   :: PROPN
      dimension CI(MNSPE)
      dimension NEIGH(2*NDIM),DIST(ndim)
      dimension OTMAT(nc,neq)
      dimension PRO(nc,neq)
      dimension PF(nc),DPT(nspec,ndim)
      dimension scama(neq,neq)
      dimension RW(LRW),IW(LIW)
      dimension RPAR(*),IPAR(*)
      DOUBLE PRECISION, DIMENSION(LRV)            :: RV
      DOUBLE PRECISION, DIMENSION(NSPEC)          :: DSE,DSP,DES
      DOUBLE PRECISION, DIMENSION(NSPEC,NSPEC)    :: DSS
      DOUBLE PRECISION, DIMENSION(NC,NEQ)         :: ZST,TMAT
      DOUBLE PRECISION, DIMENSION(NEQ,NC)         :: ZS
C Sl
      DOUBLE PRECISION, DIMENSION(NEQ,NC)         :: ZC
      DOUBLE PRECISION, DIMENSION(NEQ)            :: ART, AIT
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)        :: QC, QF
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)        :: QInvC, QInvF
C Sl
      DOUBLE PRECISION, DIMENSION(NSPEC,NEQ)      :: DMATS,DXDS
      DOUBLE PRECISION, DIMENSION(NEQ)            :: DMATE,DTDS,DPDS,
     1                                               SMF,SMFP,
     1                                               AR,AI,TEST1
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)        :: ERVROW,ERVCOL,
     1                                               XROW,XCOL,
     1                                               XJAC,SCHUR,ARTEST,
     1                                               AVR,AVL,
     1                                               DUMMAT,DUMINV
      DOUBLE PRECISION, DIMENSION(NDIM)           :: DPSE
      DOUBLE PRECISION, DIMENSION(NDIM,NDIM)      :: TEMGRA,ET
      DOUBLE PRECISION, DIMENSION(NDIM,NDIM)      :: XXX
      DOUBLE PRECISION, DIMENSION(NDIM,NEQ)       :: GRAINV,TMA
      DOUBLE PRECISION, DIMENSION(NEQ,NDIM)       :: GRAPSI,PET,GRTEST1,
     1                                               GRTEST2,GRTEST3
      INTEGER, DIMENSION(NDIM,NVERT)              :: ICORD
c************************************************************************
c     Initialize
c************************************************************************
      test  = .false.
      PROPN = zero
      TMA   = zero
      ET    = zero
      PRO   = zero
      SMFP  = zero
      DPT   = zero
C
      TMAT = OTMAT
      DUMMAT=0.0D0
      DUMINV=0.0D0
c************************************************************************
c
c     Loop over all ILDM points
c
c************************************************************************
      do 1000 l = 1,nvert
c************************************************************************
c     Copy ndim,theta,SMF,RV from HPROP and ICORD into PROPN
c************************************************************************
c output
c     WRITE(*,*)'Point:',L,'FLAG:',IRET(L)
c ndim
c state SMF and rates SMFP
        SMF  = HPROP(IISMF: IISMF -1+NEQ,L)
        SMFP = HPROP(IISMFP:IISMFP-1+NEQ,L)
          IF(IPARA.NE.1) THEN
        PROPN(NIAUX:NIAUX-1+NDIM,L) = HPROP(1:NDIM,L)
          ELSE
        PROPN(NIAUX:NIAUX-1+NDIM,L) = MATMUL(TMAT(NC-NDIM+1:NC,:),SMF)
c       write(*,*) PROPN(NIAUX:NIAUX-1+NDIM,L)
          ENDIF
c************************************************************************
c     C*F = PF project source terms
c************************************************************************
c data lrv
      RV(1:LRV) = HPROP(IIRV:IIRV-1+LRV,l)
      IFLAG = NINT(RV(1))
      IFLAU = NINT(RV(10))
CCC
      PROPN(NISMF:NISMF-1+NEQ,L)    = SMF
      PROPN(NIRV:NIRV-1+LRV,l)      = RV
      PROPN(NIICOR:NIICOR-1+NDIM,L) = ICORD(1:NDIM,L)
      IF(NPRONT.NE.0) PROPN(NPRONT:NPRONT-1+NEQ,L)    = SMFP
c************************************************************************
c     Recalculate eigenvectors and projection matrix
c************************************************************************
c     goto 5666
      RDIM = 0
      MDIM = NC
      TIME = 0
      IPRI = 0
      NFILE6 = 6
      CALL EGVBAS(1,0,MDIM,RDIM,NZEV,NEQ,MASRES,MASJAC,TIME,SMF,
     1            ERVCOL,ERVROW,XCOL,XROW,
     1            AR,AI,AVR,AVL,XJAC,SCHUR,
     1            RPAR,IPAR,IPRI,NFILE6,IERBAS)
      IF(IERBAS.NE.0) THEN
        WRITE(*,*) IERBAS
        STOP
      ENDIF
c************************************************************************
c     Get Projection information
c************************************************************************
C     DO J=1,NC
C      DO I=1,NEQ
C       ZS(I,J) = HPROP(IIPRO-1 + (J-1)*NEQ + I,L)
C      ENDDO
C     ENDDO
C     DO I=1,NEQ
C      DO J=1,NC
C       ZST(J,I) = HPROP(IIPRO-1+NC*NEQ + (J-1)*NEQ + I,L)
C      ENDDO
C     ENDDO
c************************************************************************
c     overwrite projection matrix
c************************************************************************
      ZS(1:NEQ,1:NC)  = XCOL(1:NEQ,1:NC)
C-Sl-
      ZC(1:NEQ,1:NC-NDIM)  = XCOL(1:NEQ,1:NC-NDIM)
C      ZC(1:NEQ,1:NC-NDIM)  = ERVCOL(1:NEQ,1:NC-NDIM)
C-Sl-
      ZST(1:NC,1:NEQ) = XROW(1:NC,1:NEQ)
 5666 continue
c************************************************************************
c     Get neighbour points NEIGH of the vertex
c************************************************************************
      IMES = 1
      call CHKNEI(l,nvert,ndim,ICORD,NEIGH,ierr,IRET,IMES)
c************************************************************************
c     Compute gradient PSItheta (case projected coordinates: PSIeta)
c************************************************************************
C Warum um alles in der Welt?
C-Test
        DIST = ONE
C-TestELSE
C-Test  WRITE(*,*)'Fix coordinates - No grid distance known'
C-Test  STOP
C-TestENDIF
c
      call CALGRA(neq,ndim,nprop,HPROP(IISMF,1),l,
     1      GRAPSI,NEIGH,DIST,ierr)
c************************************************************************
c     Decision between gen./fix. coordinates
c************************************************************************
      if(IPARA.NE.2) THEN
c************************************************************************
c     Reduce dimension of TMAT (nc to ndim) and store in array TMA
c************************************************************************
          TMA(1:NDIM,1:NEQ) = TMAT(NC-NDIM+1:NC,1:NEQ)
c************************************************************************
c     Calculate TMA * PSItheta = ETAtheta
c************************************************************************
        call DGEMM('n','n',ndim,ndim,neq,one,TMA,ndim,GRAPSI,
     1             neq,zero,ET,ndim)
c************************************************************************
c     Calculate inverse of ETAtheta -> THETAeta
c************************************************************************
        call DGEINV(ndim,ET,RW,IW,IERR)
        if(ierr.ne.0) goto 900
c************************************************************************
c     Compute PSIeta ( -> PET) = PSItheta * THETAeta
c************************************************************************
      call DGEMM('n','n',neq,ndim,ndim,one,GRAPSI,neq,ET,ndim,
     1            zero,PET,neq)
c************************************************************************
c     Decision fix/general coordinates:
c************************************************************************
      ELSE
c************************************************************************
c     Compute parameterization matrix PSIthetaplus (now GRAINV)
c     (Pseudo-inverse (P^T P)^-1 P^T)
c************************************************************************
      SCAMA(:,:) = 0
      scama(1,1) = 1.e-12
      scama(2,2) = 1.e-12
      do i=3,neq
      scama(i,i) = 1.0
      enddo
      CALL GENPSI(NEQ,NDIM,NEQ,SCAMA,GRAPSI,GRAINV)
c************************************************************************
c     Write parametrization matrix Psithetaplus into TMAT
c************************************************************************
            TMAT(NC-NDIM+1:NC,1:NEQ) = GRAINV(1:NDIM,1:NEQ)
      ENDIF
C************************************************************************
C     Set projected source terms to zero for update points
C************************************************************************
C     IF(IFLAG.NE.0.OR.IFLAU.NE.NEQ-NC) SMFP(1:NEQ) = 0
!      IF(IFLAG.NE.0) SMFP(1:NEQ) = 0
c
      PF = ZERO
      call DGEMV('N',NC,NEQ,ONE,TMAT,NC,SMFP,1,ZERO,PF,1)
C************************************************************************
C     Set projected source terms to zero for update points
C************************************************************************
C     IF(IRET(L).EQ.-6.OR.IRET(L).EQ.-66) THEN
C       PF(NC-NDIM+1:NC) = ZERO
C      ENDIF
c************************************************************************
c     Compute projection operator PRO(nc*neq) = C*ZS*ZST of phys.
c************************************************************************
C     IF(IFLAG.EQ.0.AND.IFLAU.EQ.NEQ-NC) THEN
C     IF(IFLAG.EQ.0) THEN
      IF(1.EQ.0) THEN
        PRO = MATMUL(MATMUL(TMAT,ZS),ZST)
      ELSE
        PRO = TMAT
      ENDIF
C     PF(NC-NDIM+1:NC)=MATMUL(PRO(NC-NDIM+1:NC,1:NEQ),SMFP(1:NEQ))
C************************************************************************
C     Store projected source terms PF in PROPN
C************************************************************************
        PROPN(NIPF:NIPF-1+NDIM,L) = PF(NC-NDIM+1:NC)
c************************************************************************
c     Store projection operator PRO in PROPN
c************************************************************************
      DO J=1,NEQ
        DO I=1,NDIM
          PROPN(NIPRO-1+(J-1)*NDIM+I,L) = PRO(NC-NDIM+I,J)
        ENDDO
      ENDDO
c************************************************************************
c     For fix coordinates (before projection):
c     Write PSIeta (= PET) in array GRAPSI
c************************************************************************
CUUU  if(ipara.NE.2) THEN
CUUU      GRAPSI = PET
CUUU  ENDIF
c************************************************************************
c************************************************************************
c     Calculate transport properties
c************************************************************************
      if(ICALTR.eq.0) then
        write(*,*)' ## Impossible to generate diffusion matrix ##',
     1           '- transport data not available !',
     1  'Use option *Transp* and copy MOLNEW to fort.1 in go file'
      endif
      call TRAPRO(ITRM,ieq,neq,SMF,nspec,DSS,DSE,DSP,DES,DEE,DEP,
     1     XLA,VIS,DDDG,ierr)
      IF (IERR.NE.0)THEN
      WRITE(*,*)'ERROR CALL TRAPRO IN PREPRC'
      end if
c
c************************************************************************
c     Calculate transformation matrices from h,p,phi to t,p,x
c************************************************************************
      infder = 1
      call TRASSP(ieq,neq,SMF,nspec,t,c,ci,rho,h,p,SSPV,ierr,
     1                  infder,DXDS,NSPEC,DTDS,DPDS)
c
      if(ieq.ne.1) then
         write(*,*) '-TRADIF- does not yet include pressure influence'
         stop
      endif
c************************************************************************
c     Calculate diffusion flux formulation
c     j   = DMATS * grad(h,p,phi)
c     j_q = DMATE * grad(h,p,phi)
c************************************************************************
C************************************************************************
C A. Neagos:                                                            *
C DXDS steht hier f�r die Ableitung der Massenbr�che nach den spezifi-  *
C schen Molzahlen: dw/dPsi. Anwendung, wenn FUSO=ITRM=4                 *
C Vgl. �nderungen in physchem.f/DIFMA!                                  *
C************************************************************************
      IF (ITRM .EQ. 4) THEN
      DXDS(:,1:NEQ) = ZERO
      DO I=1,NSPEC
      DXDS(I,I+2)=XMOL(I)
      END DO
      END IF
      call TRADIF(neq,nspec,SMF,
     1     DSS,DSE,DSP,DES,DEE,DEP,DXDS,DTDS,DMATS,DMATE,ierr)
c************************************************************************
c     Scale such that diffusion flux is in terms of phi
c************************************************************************
c
c       do i=1,NSPEC
c       DMATS(I,:) = DMATS(I,:)*XMINV(i)
c      ENDDO
c       DMATE  =  DMATE
c      return
c
c************************************************************************
c     Compute DMATS*GRAPSI/DMATE*GRAPSI and store in PROPN
c************************************************************************
      DPT = MATMUL(DMATS,GRAPSI)
      DPSE = MATMUL(DMATE,GRAPSI)
      PET = 0
      PET(1,1:NDIM) = DPSE
      PET(3:NEQ,1:NDIM) = DPT
      DO J=1,NDIM
        DO I=1,NSPEC
          PROPN(NIDPT-1+(J-1)*NSPEC+I,L) = DPT(I,J)
        ENDDO
      ENDDO
c************************************************************************
c     store viscosity
c************************************************************************
      IF(NIHFLA.NE.0) PROPN(NIHFLA,L)    = VIS
c************************************************************************
c     store dsp
c************************************************************************
      PROPN(NIDPSE:NIDPSE-1+NDIM,l) = DPSE(1:NDIM)
c************************************************************************
c     Store Eigenvalues
c************************************************************************
C-Sl- *******************************************************************
C-Sl- Define of normal space projected source term linearisation ********
C     AR and AI are real and imaginary parts of the eigenvalues
C-Sl- *******************************************************************
C      WRITE(*,*) '!!!CALL OF ORTHOGONALISATION AND SATABILITY!!!'
C      WRITE(*,*) 'NDIM = ', NDIM, 'NEQ = ', NEQ, 'NC = ', NC
      ZC(1:NEQ,NC-NDIM+1:NC)  = GRAPSI(1:NEQ,1:NDIM)
C-Sl- QF is used as a work array...
      CALL ORBAS(NEQ,NC,NEQ,ZC,NEQ,QC,NEQ*NEQ,QF,LIW,IW,2,IERR)
C
C      WRITE(*,*) 'Check ZC = ', ZC(1:NEQ,1:NC-NDIM)
C      WRITE(*,*) '****ORTHOGONALITY****'
C      WRITE(*,*) 'Check  = ', QC(1:NEQ,1:NC)-ZC(1:NEQ,1:NC)
C-Sl- Define projection for slow & conserved values, they are orthogonal
      NF= NEQ - NC
      QF(1:NEQ,1:NF)=QC(1:NEQ,NC+1:NEQ)
      QF(1:NEQ,NF+1:NEQ)=QC(1:NEQ,1:NC)
C-Sl-
      QInvC=QC
      CALL DGEINV(NEQ,QInvC,RW,IW,IERR)
*      IF(IERR.GT.0) WRITE(*,*) 'IERR in prepare QInvC= ', IERR
      QInvF=QF
      CALL DGEINV(NEQ,QInvF,RW,IW,IERR)
*      IF(IERR.GT.0) WRITE(*,*) 'IERR in prepare QInvF=', IERR
C-Sl- Stability Analysis------------------------------------------------
      ART = ZERO
      AIT = ZERO
C---- First N-NC eigenvalues of AR, AI are fast subsystem eigenvalues!--
C      WRITE(*,*) '****STABILITY****'
      IF(IERR.EQ.0)THEN
      IERRS=0
!      CALL STABAN( -1, NEQ, NC, NF, QInvC, SMF, TIME, ART, AIT,
!     $     RPAR, IPAR, IERRS )
      IF(IERRS.GT.0) WRITE(*,*) 'IERRS = ', IERRS
C
      AR(NC+1:NEQ)=ART(1:NF)
      AI(NC+1:NEQ)=AIT(1:NF)
C
      IERRS=0
!      CALL STABAN( -1, NEQ, NF, NC, QInvF, SMF, TIME, ART, AIT,
!     $     RPAR, IPAR, IERRS )
      IF(IERRS.GT.0) WRITE(*,*) 'IERRS = ', IERRS
C
      AR(1:NC)=ART(1:NC)
      AI(1:NC)=AIT(1:NC)
      end if
C
C      READ(*,*)
C      STOP
C-Sl- *******************************************************************
C-Sl- *******************************************************************
      IF(NIEGVA.NE.0) THEN
      PROPN(NIEGVA:NIEGVA-1+NEQ,l)         = AR(1:NEQ)
      PROPN(NIEGVA+NEQ:NIEGVA-1+NEQ+NEQ,l) = AI(1:NEQ)
CCCCCCCCCCC
      PROPN(NIEGVA:NIEGVA-1+NEQ,l)         = TEMP1(1:NEQ,1,l)
      PROPN(NIEGVA+NEQ:NIEGVA-1+NEQ+NEQ,l) = temp2(1:neq,1,l)
CCCCCCCCCCC
CCCCCCCCCCC
      ENDIF
c************************************************************************
c
C      XXX=MATMUL(PRO(NC-NDIM:NC,3:NEQ),DPT(1:NSPEC,1:NDIM))
c     WRITE(*,8675) DPT(:,1)
c     WRITE(*,8675) DPT(:,2)
c     WRITE(*,8675) PRO(1,:)
c     WRITE(*,8675) PRO(2,:)
c     WRITE(*,8675) XXX
 8675 FORMAT(1X,8(1pe9.2,1x))
c     End of loop over vertices
c
c************************************************************************
 1000 continue
C     PROPN(NIDPSE:NIDPSE-1+NDIM,1) = PROPN(NIDPSE:NIDPSE-1+NDIM,2)
C     PROPN(NIDPT:NIDPT-1+NDIM*NSPEC+I,1) =
C    1  PROPN(NIDPT:NIDPT-1+NDIM*NSPEC+I,2)
C     PROPN(NIPRO:NIPRO-1+NEQ*NDIM,1) = PROPN(NIPRO:NIPRO-1+NEQ*NDIM,2)
C     PROPN(NIDPSE:NIDPSE-1+NDIM,NVERT) =
C    1     PROPN(NIDPSE:NIDPSE-1+NDIM,NVERT-1)
C     PROPN(NIDPT:NIDPT-1+NDIM*NSPEC+I,NVERT) =
C    1  PROPN(NIDPT:NIDPT-1+NDIM*NSPEC+I,NVERT-1)
C     PROPN(NIPRO:NIPRO-1+NEQ*NDIM,NVERT) =
C    1          PROPN(NIPRO:NIPRO-1+NEQ*NDIM,NVERT-1)
      return
c************************************************************************
c     Error exits
c************************************************************************
  900 continue
      write(*,901)
  901 format(' Error in matrix inversion ')
      goto 999
  999 continue
      write(*,998)
  998 format(3(' ****   Error in - PREPRO -  ****',/))
      stop
c************************************************************************
c     End of - PREPRO -
c************************************************************************
      end
c
      subroutine CALGRA(neq,ndim,nprop,PSI,ivc,GRAPSI,NEIGH,D,ierr)
c***********************************************************************
c                                                                      *
c     Calculate Gradient GRAPSI                                        *
c                                                                      *
c          input:                                                      *
c          nvert: number of vertices                                   *
c          ndim : dimension of the ILDM                                *
c          NEIGH: indices of the neighbours (numbers of points)        *
c          D    : distances in ndim dimensions                         *
c                                                                      *
c          output:                                                     *
c          GRAPSI: gradient d(PSI)/dtheta                              *
c                                                                      *
c***********************************************************************
      implicit double precision(a-h,o-z)
      dimension NEIGH(2,ndim),D(ndim)
      dimension PSI(nprop,*),GRAPSI(neq,ndim)
      logical test
      parameter (zero=0.0D0,ONE=1.0D0,onem=-1.0D0)
c***********************************************************************
      test = .false.
      ierr=0
c***********************************************************************
c     loop over dimensions
c***********************************************************************
      do 100 k=1,ndim
c
        ilef = NEIGH(1,k)
        irig = NEIGH(2,k)
c
        fac1 =  D(k)    ** onem
        fac2 = (D(k)*2) ** onem
c
        if(ilef.ne.0.and.irig.ne.0) then
          if(test) write(6,*) ' Central differences '
c
          call DCOPY(neq,PSI(1,irig),     1,GRAPSI(1,k),1)
          call DAXPY(neq,onem,PSI(1,ilef),1,GRAPSI(1,k),1)
          call DSCAL(neq,fac2,GRAPSI(1,k),1)
c
        else if(ilef.eq.0.and.irig.ne.0) then
c
          call DCOPY(neq,PSI(1,irig),1,GRAPSI(1,k),1)
          call DAXPY(neq,onem,PSI(1,ivc),  1,GRAPSI(1,k),1)
          call DSCAL(neq,fac1,GRAPSI(1,k),1)
c
        else if(ilef.ne.0.and.irig.eq.0) then
c
          call DCOPY(neq,PSI(1,ivc),1,GRAPSI(1,k),1)
          call DAXPY(neq,onem,PSI(1,ilef),  1,GRAPSI(1,k),1)
          call DSCAL(neq,fac1,GRAPSI(1,k),1)
c
        else
          write(6,*) ' Fatal error, no neighbours in dir.',K
          ierr=1
        stop
      endif
c
  100 continue
      return
c***********************************************************************
c     End of CALGRA
c***********************************************************************
      END
c
      SUBROUTINE CHKNEI(IVC,NVERT,NDIM,ICORD,NEIGH,IERR,IRET,IMES)
C***********************************************************************
C
C     This routine gets the indices of the neighbours
C
C     input:  NVERT: number of vertices
C             NDIM:  dimension
C             ICORD: coordinates of the vertices
C             IVC  : number of the point, to which neighbours are saught
C             IMES : task
C
C     output: NEIGH: indices of the neighbours
C             IERR : flag
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION NEIGH(1,2,NDIM),ICORD(NDIM,NVERT)
cjb
      DIMENSION ICO(NDIM)
      DIMENSION IRET(NVERT)
      DIMENSION NEXT(NDIM)
      LOGICAL   LOC1,LOC2
cjb--
      LOGICAL TEST
C***********************************************************************
C     INITIALIZE
C***********************************************************************
      TEST = .FALSE.
      IVCT = 1
      IFOU = 0
      DO 110 K=1,NDIM
        NEIGH(IVCT,1,K) = 0
        NEIGH(IVCT,2,K) = 0
  110 CONTINUE
C***********************************************************************
C     Loop over possible neighbours
C***********************************************************************
      DO 200 J=1,NVERT
        IDCORD = 0
        DO 150 K=1,NDIM
          IDCORD = IDCORD + ABS(ICORD(K,J)-ICORD(K,IVC))
  150   CONTINUE
C---- if IDCORD is 1, then a neighbour has been found
        IF(IDCORD.EQ.1) THEN
        DO 160 K=1,NDIM
        IF(ICORD(K,J)-ICORD(K,IVC).EQ.-1) THEN
          NEIGH(IVCT,1,K) = J
          IFOU = IFOU + 1
          GOTO 170
        ENDIF
        IF(ICORD(K,J)-ICORD(K,IVC).EQ.+1) THEN
          NEIGH(IVCT,2,K) = J
          IFOU = IFOU + 1
          GOTO 170
        ENDIF
  160   CONTINUE
        WRITE(6,*) ' schwachkopf'
        STOP
        ENDIF
  170   CONTINUE
C---- leave loop if 2*ndim neighbours have been found
        IF(IFOU.EQ.2*NDIM) GOTO 210
C***********************************************************************
C     End of loop over possible neighbours
C***********************************************************************
  200 CONTINUE
  210 CONTINUE
                           IERR = -1
        IF(IFOU.EQ.2*NDIM) IERR = 0
cjb
      IF(IMES.EQ.1) THEN
C***********************************************************************
C     IF target point reached, or neighbours have flag 66, choose
C     neighbour point pairs
C***********************************************************************
C     For syngas:
c      IEIN = 1
C     For methan:
      IEIN = -1
      NEXT = 0
C
      DO IH=2,NDIM
        DO LH =1,2
          IND1 = NEIGH(IVCT,LH,IH)
          IF(IND1.NE.0) THEN
          IF(IRET(IND1).EQ.-66) NEXT(IH) = NEXT(IH) + 1
          ENDIF
        ENDDO
      ENDDO
C
        DO 880 III=2,NDIM
          LOC1 = (NEXT(III).EQ.2)
          LOC2 = (NEXT(III).EQ.1.AND.IRET(IVC).EQ.-66)
          IF(LOC1.OR.LOC2) THEN
            ID = 0
            DO 800 LLL=1,2
              IND = NEIGH(IVCT,LLL,III)
              IF(IND.EQ.0) GOTO 800
              DO I=1,NDIM
                ICO(I) = ICORD(I,IND)
              ENDDO
              ICO(1) = ICO(1) - IEIN
              DO 400 JJ=1,NVERT
                IDCORD = 0
                DO II=1,NDIM
                  IF(ICO(II).EQ.ICORD(II,JJ)) THEN
                    IDCORD = IDCORD + 1
                  ENDIF
                ENDDO
                IF(IDCORD.EQ.NDIM) THEN
                  ID = JJ
                  GOTO 401
                ENDIF
 400          CONTINUE
                ID = 0
 401          CONTINUE
              NEIGH(IVCT,LLL,III) = ID
 800        CONTINUE
          ENDIF
 880    CONTINUE
C***********************************************************************
C     Check
C***********************************************************************
c        IND1 = IRET(IVC)
c        IF(IND1.NE.-66.AND.IND1.NE.-6) THEN
c          DO I=1,NDIM
c            DO J=1,2
c              IND2 = IRET(NEIGH(1,J,I))
c              IF(IND2.EQ.-6.OR.IND2.EQ.-66) THEN
c                NEIGH(IVCT,J,I) = 0
c              ENDIF
c            ENDDO
c          ENDDO
c        ENDIF
C
      ENDIF
cjb--
C***********************************************************************
C     End of loop over vertices
C***********************************************************************
C     test = .true.
      IF(TEST) THEN
      WRITE(6,*) 'vertex',IVC,':',(ICORD(J,IVC),J=1,NDIM)
      DO 660 K=1,NDIM
      ILEF = NEIGH(IVCT,1,K)
      IRIG = NEIGH(IVCT,2,K)
      WRITE(6,*) 'left ',ILEF,(ICORD(J,ILEF),J=1,NDIM)
      WRITE(6,*) 'right',IRIG,(ICORD(J,IRIG),J=1,NDIM)
  660 CONTINUE
      ENDIF
      RETURN
C***********************************************************************
C     end CHKNEI
C***********************************************************************
      END
      SUBROUTINE CHKVER(NVERTC,NVERT,NCELL,NDIM,N2DIM,NC,
     1        CMAT,NEQ,NPROP,
     1                  IY,ICORD,IVERT,IRET,PROP)
      implicit double precision(a-h,o-z)
      include 'homdim.f'
      PARAMETER (ONE = 1.0D0, ZERO = 0.0D0)
C***********************************************************************
C     COMMON BLOCKS
C***********************************************************************
      COMMON/ILDMOP/IPARA,ISTAOP,MANPAR,NOFORM,ANAFUE,INMAN,lasloo,
     1              ICALTR,MASENS,IUPVER,NTRANS
      COMMON/BLOCOR/IIRAT,IISMF,IISMFP,IIRV,IIPRO,IIPNU,IILAM,NPROOT
c***********************************************************************
c     arrays
c***********************************************************************
      DIMENSION IRET(*),IY(*)
      DIMENSION ICORD(*),IVERT(*)
      DIMENSION NEIGH(2*NDIM)
      DIMENSION PROP(NPROP,*),SMF(MNEQU)
      DIMENSION CMA(NDIM,NEQ),PSMF(NDIM),PNEI(NDIM)
      DIMENSION CMAT(NC,NEQ)
C***********************************************************************
C     Initialize
C***********************************************************************
      CMA = ZERO
C***********************************************************************
C     Get parameterization of RPVs
C***********************************************************************
      CMA(1:NDIM,1:NEQ) = CMAT(NC-NDIM+1:NC,1:NEQ)
  113 CONTINUE
C***********************************************************************
C     Loop over boundary vertices
C***********************************************************************
      DO 1000 IV=NVERTC+1,NVERT
C---- Get neighbours of point IV
      CALL CHKNEI(IV,NVERT,NDIM,ICORD,NEIGH,IERR,IRET,IMES)
C---- Loop over neighbours
      DO IN=1,2*NDIM
         IF(NEIGH(IN).NE.0)THEN
C---- Check, if the boundary point has the same values
C---- (with respect to the RPVs) as his neighbours
            SMF(1:NEQ) = PROP(IISMF:IISMF-1+NEQ,IV)
            PSMF = MATMUL(CMA,SMF)
            SMF(1:NEQ) = PROP(IISMF:IISMF-1+NEQ,NEIGH(IN))
            PNEI = PSMF - MATMUL(CMA,SMF)
            DO J=1,NDIM
C---- If at least one value is almost the same => delete point IV
               IF(DABS(PNEI(J)).EQ.ZERO)THEN
                  CALL DELPOI(IV,NDIM,N2DIM,NVERT,NPROP,NCELM,NCELL,
     1                        IY,ICORD,IRET,IVERT,PROP)
                  GOTO 113
               ENDIF
            ENDDO
         ENDIF
      ENDDO
C***********************************************************************
C     End of Loop
C***********************************************************************
 1000 CONTINUE
      RETURN
C***********************************************************************
C     End of -CHKVER-
C***********************************************************************
      END
      SUBROUTINE TRAPRO(ITRM,IEQ,NEQ,SMF,NSPEC,DSS,DSE,DSP,DES,DEE,DEP,
     1     XLA,VIS,DDDG,IERR)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *          CALCULATION OF TRANSPORT COEFFICIENTS            *
C     *                                                           *
C     *                AUTHOR : U.MAAS                            *
C     *                                                           *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C
C***********************************************************************
C***********************************************************************
C
C     CHAPTER A: STORAGE ORGANIZATION, STATEMENT FUNCTIONS
C
C***********************************************************************
C
C***********************************************************************
C     I.1: TYPES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      PARAMETER (ZERO=0.0D0,ONE=1.0D0)
      LOGICAL TEST
C***********************************************************************
C     II.4: VARIABLE DIMENSIONS
C***********************************************************************
      DIMENSION SMF(NEQ)
C     DIMENSION S(MNSPE+2)
      PARAMETER (MNDCI = MNSPE + MNTHB)
      DIMENSION W(MNSPE),X(MNSPE),CI(MNDCI)
      DIMENSION HI(MNSPE)
      PARAMETER (LRW=3*MNSPE)
      DIMENSION RW(LRW)
C***********************************************************************
C
C***********************************************************************
      DIMENSION DSS(NSPEC,NSPEC),DSE(NSPEC),DSP(NSPEC),DES(NSPEC)
      DOUBLE PRECISION, DIMENSION(1) :: DXDS,DTDS,DPDS
C***********************************************************************
C     II.3: COMMON BLOCK FOR LOCATIONS
C***********************************************************************
      COMMON/LOCS/NU1,NP,NU2,NU3,NT,NU4,NU5,NU6,NW,NWS,NU7
      COMMON/BSSPV/SSPV(20*MNSPE)
C***********************************************************************
C
C     Initialize
C
C***********************************************************************
      TEST = .FALSE.
      CALL TRASSP(IEQ,NEQ,SMF,NSPEC,T,C,CI,D,HSPEC,P,SSPV,IEX,
     1     0,DXDS,1,DTDS,DPDS)
      IF(TEST) THEN
         WRITE(6,*) IEQ,NEQ,(SMF(I),I=1,NEQ)
         WRITE(6,*) nspec,t,c,d,hspec,p
         WRITE(6,*) (ci(i),i=1,nspec)
      ENDIF
      IF(IEX.NE.0) THEN
         WRITE(6,*) IEQ,NEQ,(SMF(I),I=1,NEQ)
         WRITE(6,*) nspec,t,c,d,hspec,p
         WRITE(6,*) (ci(i),i=1,nspec)
         write(6,*) ' iex = ',iex,' in trapro'
         IERR = 9
         RETURN
      ENDIF
C***********************************************************************
C     Calculate X, W
C***********************************************************************
      DO 30 I=1,NSPEC
         X(I) = CI(I) / C
         IF(IEQ.EQ.1.OR.IEQ.EQ.2.OR.IEQ.EQ.5.OR.IEQ.EQ.6) THEN
            W(I) = SMF(I+2) * XMOL(I)
         ELSE
            W(I) = SMF(I+2)
         ENDIF
 30   CONTINUE
      XMOLM = D / C
C***********************************************************************
C     Calculate CP, HI
C***********************************************************************
      IHI   = NSPEC*18+1
      ICPI  = NSPEC*19+1
      CALL DCOPY(NSPEC,SSPV(IHI),1,HI,1)
      CP = ZERO
      DO 10 I=1,NSPEC
      HI(I) = HI(I) / XMOL(I)
      CP = CP + SSPV(ICPI-1+i) * X(I)
   10 CONTINUE
      CP = CP / XMOLM
C***********************************************************************
C
C     Calculation of the necessary data
C
C***********************************************************************
C---- wird aber in difma gar nicht gebraucht
C***********************************************************************
C     Get diffusion matrices
C***********************************************************************
      CALL DIFMA(ITRM,NSPEC,
     1     P,T,D,W,X,XMOLM,CP,HI,XLA,VIS,
     1     DSS,DSE,DSP,DES,DEE,DEP,LRW,RW,IERR,
     1     XMOL,LRSPEC,RSPEC,NEPS,NSIG,NHL,NHSW,
     1     NETA,NLCOEF,NLCOEM,NLCOED,NDCOEF,1)
C***********************************************************************
C
C***********************************************************************
      DDDG = XLA/(D*CP)
      IF(TEST) THEN
      write(6,*) '***************************************'
      write(6,*) ' P,T,C,D ',P,T,C,D
      write(6,*) ' w ', (W(I),i=1,nspec)
      WRITE(6,*) ' LA,VIS,CP',XLA,VIS,CP
      ENDIF
C***********************************************************************
C     Error exits
C***********************************************************************
      IERR=0
      RETURN
C***********************************************************************
C     Error exits
C***********************************************************************
  910 CONTINUE
      WRITE(6,*) ' at 910'
      STOP
  920 CONTINUE
      WRITE(6,*) ' at 920'
      STOP
C***********************************************************************
C
C***********************************************************************
      END
      SUBROUTINE TABSYM(NDIM,NC,NEQ,SYMPRO,LRV,NPROP,SYMPRP)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      include 'homdim.f'
      CHARACTER(LEN=*)  SYMPRO(*),SYMPRP(NPROP)
      COMMON/BLOCPR/NIAUX,NISMF,NIPF,NIRV,NIPRO,NIDPT,
     1   NIDPSE,NIHFLA,NIICOR,NIEGVA,NPRONT
      COMMON/BLOCOR/IIRAT,IISMF,IISMFP,IIRV,IIPRO,IIPNU,IILAM,NPROOT
C***********************************************************************
c                                                                      *
c     Subroutine for header of data files                              *
c                                                                      *
c***********************************************************************
      LSPRP=LEN(SYMPRP(1))
      DO I=1,LSPRP
      DO J=1,NPROP
      SYMPRP(J)(I:I) =' '
      ENDDO
      ENDDO
      nspec=neq-2
      SYMPRP(1:NPROP) = 'XXXXXXXX'
C***********************************************************************
C     assign symbols for rates of the parameters
C***********************************************************************
      DO I=1,NDIM
        WRITE(SYMPRP(NIAUX-1+I),81) I
   81 FORMAT(I3,'-Rate')
      ENDDO
C***********************************************************************
C     assign symbols for SMF
C***********************************************************************
      DO I=1,NEQ
        WRITE(SYMPRP(NISMF-1+I),82) SYMPRO(IISMF-1+I)
      ENDDO
   82 FORMAT(A)
C***********************************************************************
C     assign symbols for projected source terms(ndim at first,fix coord)
C***********************************************************************
      DO I=1,NDIM
        WRITE(SYMPRP(NIPF-1+I),83) I
      ENDDO
   83 FORMAT('rETA',I4)
C***********************************************************************
C     assign symbols for RV
C***********************************************************************
      DO I=1,LRV
      WRITE(SYMPRP(NIRV-1+I),84) SYMPRO(IIRV-1+I)
      ENDDO
   84 FORMAT(A)
c***********************************************************************
C     assign symbols for projection matrix
C***********************************************************************
      DO I=1,NDIM*NEQ
      WRITE(SYMPRP(NIPRO-1+I),91) i
      ENDDO
   91 FORMAT('PM  ',i4)
c***********************************************************************
c     assign D*PSItheta
c***********************************************************************
      do i=1,nspec*ndim
      write(SYMPRP(NIDPT-1+i),92) i
      ENDDO
   92 FORMAT('DPSI',i4)
c***********************************************************************
c     assign P*h_theta
c***********************************************************************
      do i=1,ndim
       write(SYMPRP(NIDPSE-1+i),93) i
      ENDDO
  93   format('DPSE',i4)
c***********************************************************************
c     Assign niicor
c***********************************************************************
      do i=1,NDIM
        write(SYMPRP(NIICOR-1+i),289) i
      enddo
 289  format('icor',I4)
c***********************************************************************
c
c***********************************************************************
      IF(NIEGVA.NE.0) THEN
      do i=1,NEQ
        write(SYMPRP(NIEGVA-1+i),188) i
        write(SYMPRP(NIEGVA-1+i+NEQ),189) i
      enddo
      ENDIF
 188  format('la_r',I4)
 189  format('la_i',I4)
c***********************************************************************
c
c***********************************************************************
      IF(NPRONT.NE.0) THEN
      DO I=1,NEQ
        WRITE(SYMPRP(NPRONT-1+I),82) SYMPRO(IISMFP-1+I)
      ENDDO
      ENDIF
c***********************************************************************
c
c***********************************************************************
      IF(NIHFLA.NE.0) THEN
        WRITE(SYMPRP(NIHFLA),82) 'mu      '
      ENDIF
c***********************************************************************
c     Assign all remaining variables
c***********************************************************************
C     isto = NIEGVA + 2*NEQ - 1
C     do 90 i=isto+1,nprop
C       write(SYMPRP(i),94) i-isto
C 94  format('DIF ',i4)
C 90  continue
C***********************************************************************
C     End of -TABSYM-
C***********************************************************************
      return
      END
      SUBROUTINE ISETUP(NDIM,NEQ,LRV,NS,NE,IEQ,NPROPN,IPAR,RPAR)
C***********************************************************************
C     STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      LOGICAL WEGV,WSMFP,WVIS
C***********************************************************************
C     DIMENSION
C***********************************************************************
      DIMENSION IPAR(*),RPAR(*)
C***********************************************************************
C     COMMON BLOCKS
C***********************************************************************
      COMMON/BLOCPR/NIAUX,NISMF,NIPF,NIRV,NIPRO,NIDPT,
     1   NIDPSE,NIHFLA,NIICOR,NIEGVA,NPRONT
C***********************************************************************
C     FIRST EXECUTABLE STATEMENT FOLLOWS
C***********************************************************************
      NS  = IPAR(3)
      NE  = IPAR(6)
      IEQ = IPAR(10)
      WEGV = .true.
      WSMFP = .TRUE.
      WVIS  = .TRUE.
C***********************************************************************
C     New Property field
C***********************************************************************
      NIAUX = 1
      NISMF = NIAUX + NDIM
      NIPF  = NISMF + NEQ
      NIRV  = NIPF  + NDIM
      NIPRO = NIRV  + LRV
      NIDPT = NIPRO + NDIM*NEQ
      NIDPSE = NIDPT + NDIM*NS
      NIICOR = NIDPSE + NDIM
      NEXT   = NIICOR +  NDIM
      IF(WEGV) THEN
        NIEGVA = NEXT
        NEXT   = NEXT + 2*NEQ
      ELSE
        NIEGVA = 0
      ENDIF
      IF(WSMFP) THEN
        NPRONT = NEXT
        NEXT   = NEXT + NEQ
      ELSE
        NPRONT = 0
      ENDIF
      IF(WVIS) THEN
        NIHFLA = NEXT
        NEXT   = NEXT + 1
      ELSE
        NIHFLA = 0
      ENDIF
      NILAST = NEXT
      NPROPN = NILAST - 1
      RETURN
C***********************************************************************
C     End of NSETUP
C***********************************************************************
      END
