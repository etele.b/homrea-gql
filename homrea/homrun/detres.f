C>    detres
      SUBROUTINE DETRES(TIME,S,SP,F,IRES,RPAR,IPAR)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *            INTERFACE FOR CONSERVATION EQUATIONS           *
C     *              (ADAPTED FOR SUBROUTINE -DASSL-)             *
C     *                                                           *
C     *               U.MAAS                                      *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C     INPUT/OUTPUT: SEE SUBROUTINE -DASSL-
C
C***********************************************************************
C***********************************************************************
C     CHAPTERI: STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      COMMON/LOCS0D/NW,NWS,NP,NK,NI,NH
      DIMENSION S(*),SP(*),F(*),RPAR(*),IPAR(*)
      DIMENSION BV(1),IR(1),IC(1),IFP(1)
C***********************************************************************
C     CHAPTER II: CALL OF -detDES-
C***********************************************************************
      NEQ=IPAR(1)
      NS =IPAR(3)
      NZV = 0
      IFC = 0
      CALL DETRIG(NEQ,NZV,TIME,S,F,BV,IR,IC,RPAR,IPAR,IFC,IFP)
      IF(IFC.EQ.9) THEN
        IFC = 0
        IRES = 1
      ENDIF
      DO 15 J=1,NS
   15 F(J)=SP(J)-F(J)
      F(NK) = SP(NK) - F(NK)
      F(NI) = SP(NI) - F(NI)
      F(NH) = SP(NH) - F(NH)
      RETURN
C***********************************************************************
C     END OF -detRES-
C***********************************************************************
      END
      SUBROUTINE DETLEF (NZC,B,IR,IC)
C***********************************************************************
C
C     DEFINITION OF LEFT HAND SIDE (SEE -ULIMEX-)
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      COMMON/LOCS0D/NW,NWS,NP,NK,NI,NH
      DIMENSION B(*),IR(*),IC(*)
      DATA ONE/1.0D0/,ZERO/0.0D0/
C***********************************************************************
C
C***********************************************************************
      DO 10 I=1,NZC
      IR(I)=I
      IC(I)=I
  10  B(I)=ONE
      B(NP) = ZERO
      RETURN
C***********************************************************************
C     END OF LEFT
C***********************************************************************
      END
      SUBROUTINE DETRIG (NEQNEQ,NZV,TIME,S,F,BV,IR,IC,RPAR,IPAR,IFC,IFP)
C***********************************************************************
C
C     DEFINITION OF RIGHT HAND SIDE (SEE -ULIMEX-)
C
C***********************************************************************
C***********************************************************************
C     I.I.: TYPES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION S(*),F(*),BV(*),IR(*),IC(*)
      DIMENSION RPAR(*),IPAR(*)
      DIMENSION IFP(*)
C***********************************************************************
C     CALL OF PDESYS
C***********************************************************************
      NEQ= IPAR(1)
      NS =IPAR(3)
      NE =IPAR(6)
      CALL DETDES(NEQ,NS,TIME,S,F,RPAR,IPAR,IERR)
      IF(IERR.GT.0) IFC = 9
      RETURN
C***********************************************************************
C     END OF FCN
C***********************************************************************
      END
      SUBROUTINE DETDES(NEQ,NSPEC,TIME,Y,F,RPAR,IPAR,IERR)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *           RESIDUALS OF THE CONSERVATION EQUATIONS         *    *
C     *               IN MASS FRACTION FORMULATION                *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C***********************************************************************
C
C     CHAPTER I: STORAGE ORGANIZATION                                  *
C
C***********************************************************************
C***********************************************************************
C     I.I.: TYPES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      COMMON/LOCS0D/NW,NWS,NP,NK,NI,NH
      LOGICAL DETONA,PCON,VCON,TEST,TCON
      INTEGER NWALL,NCOMPLX
C***********************************************************************
C     I.II.: DIMENSIONS
C***********************************************************************
      DIMENSION Y(NEQ),F(NEQ),RPAR(*),IPAR(*)
      DIMENSION CI(MNSPE+MNTHB+1),HI(MNSPE),CPI(MNSPE)
      DOUBLE PRECISION, DIMENSION(1) :: DXDS,DTDS,DPDS
C**********************************************************************C
C     I.III.: ARRAY COMMON BLOCKS
C**********************************************************************C
      COMMON/BHCP/HLHH(7,MNSPE*2),HINF(3,MNSPE)
      COMMON/BSSPV/SSPV(20*MNSPE)
C**********************************************************************C
C     I.IV.: NON ARRAY OR FIXED ARRAY COMMON BLOCKS
C**********************************************************************C
      COMMON/BOPTL/PCON,VCON,TCON,DETONA,NWALL,NCOMPLX,NPSR
      COMMON/BDETON/DETVEL,SIGMA,THETA,REY,ETA,TSHO,TUDI,UU,VV,V,SOUND
C***********************************************************************
C     BLOCK FOR INFORMATIONS ABOUT THE REACTION MECHANISM
C***********************************************************************
      COMMON/BSENS/LSMAX,NSENS,NRSENS,NISENS,NPSENS,
     1       IRSENS,IISENS,IPSENS,ISENSI(MNSEN)
      DIMENSION WORK1(MNREA)
      DATA LRW1/MNREA/
C**********************************************************************C
C     I.V.: DATA STATEMENTS
C**********************************************************************C
      DATA NFILE6/06/
      DATA ZERO/0.0D0/,RGAS/8.3143D0/
C***********************************************************************
C                                                                      *
C     CHAPTER II: CALCULATION OF FUNCTION                              *
C                                                                      *
C***********************************************************************
      IERR = 0
      TEST = .FALSE.
C***********************************************************************
C     II.2.1: CALC. MASS FRACTION, TEMPERATURE, PRESSURE, VOLUME       *
C***********************************************************************
      IEQ = -3
      P   = Y(NP)
      RHOV  = Y(NK)
      V   = (Y(NI) - P) / RHOV
      RHO = RHOV / V
      H   = Y(NH) - V**2/2.0D0
C***********************************************************************
C     CALCULATE MEAN MOLAR MASS AND MOLE FRACTIONS                     *
C***********************************************************************
      IF(TCON)      GOTO 999
C---- F used as work array
      NNN = NSPEC+2
      F(1) = H
      F(2) = P
      CALL DCOPY(NSPEC,Y(NW),1,F(3),1)
      CALL TRASSP(-IEQ,NNN,F,NSPEC,T,C,CI,RHODUM,H,P,SSPV,IERR,
     1                  0,DXDS,1,DTDS,DPDS)
      IF(IERR.GT.0) GOTO 910
C---- here only used to compute HI
      CALL CALHCP(NSPEC,CI,T,HLHH,HINF,CPI,CPTOT,HI,IEHCP)
      CPM = CPTOT / C
      CP  = CPTOT / RHO
      IF(IERR.GT.0) GOTO 910
C***********************************************************************
C     II.2.4: CALCULATON OF MOLAR REACTION RATES                       *
C***********************************************************************
      IPRIM = 0
      CALL MECHJA(0,NSPEC,NREAC,NM,NRINS,NRTAB,
     1  IREAC(NMATLL),IREAC(NNRVEC),RREAC(NPK),RREAC(NTEXP),RREAC(NEXA),
     1  RREAC(NVT4),RREAC(NVT5),RREAC(NVT6),IREAC(NIARRH),
     2  RREAC(NXZSTO),RREAC(NRATCO),
     3  C,CI,T,F(NW),NRSENS,ISENSI(IRSENS),RPAR(IRSENS),
     1  LRW1,WORK1,IPRIM,NFILE6,IERR,
     4  RREAC(NAINF),RREAC(NBINF),RREAC(NEINF),RREAC(NT3),
     5  RREAC(NT1),RREAC(NT2),RREAC(NAFA),RREAC(NBFA),HLHH,HINF,
     1     1,1,DGDC,DGDT,DODC,DODT)
C***********************************************************************
C     III.2: P GIVEN
C***********************************************************************
      DO 110 I=1,NSPEC
      F(I+NWS)=F(I+NWS)*XMOL(I)/RHOV
  110 CONTINUE
      F(NP)= P - RHO * RGAS * T / (RHODUM / C)
      SIGMA = 0.3164D0 * RHO * (DETVEL-V)**2 / (2.0D0* REY**0.25D0)
      THETA = (0.3164D0 / REY**0.25D0 ) *RHO*(DETVEL-V)*
     1 (CP*(T-TSHO)+(DETVEL-V)**2/2.0D0)
      F(NK)= ZERO
      F(NI)= 4.0D0 * SIGMA / TUDI
      F(NH)= (4.0D0 * (SIGMA *DETVEL - THETA)/TUDI)/RHOV
C***********************************************************************
C     CHECK FOR SINGULARITY
C***********************************************************************
C---- check for singularity
      CVM = CPM - RGAS
      GAMMA = CPM / CVM
      SOUND = DSQRT(GAMMA * P / RHO)
      SDT = 0.0D0
      DO 476 I = 1,NSPEC
      SDT = SDT + F(I+NWS)*(RHO / (XMOL(I)*C))*(HI(I)/(T*CPM)-1.0D0)
  476 CONTINUE
      U1 = 4.0D0 * SIGMA * V / (RHO * TUDI)
      U2 = 4.0D0 *(GAMMA-1.D0)*(THETA- SIGMA/(DETVEL-V))/ (RHO*TUDI)
      U3 = V * GAMMA * P / RHO * SDT
      UU = U1 + U2 + U3
      VV = V**2 - SOUND**2
C***********************************************************************
C     REGULAR EXIT
C***********************************************************************
  490 CONTINUE
      RETURN
C***********************************************************************
C     CHAPTER IV: ABNORMAL END
C***********************************************************************
  910 CONTINUE
      IERR = 1
      RETURN
 2221 FORMAT('0',2X)
  999 CONTINUE
 2222 WRITE(NFILE6,2221)
 2223 FORMAT('0',32('+'),' ABNORMAL END ',32('+'))
      WRITE(NFILE6,2223)
      WRITE(NFILE6,2223)
      WRITE(NFILE6,2223)
      WRITE(NFILE6,2221)
      WRITE(NFILE6,2221)
      STOP
C***********************************************************************
C     END OF -DESYS-
C***********************************************************************
      END
