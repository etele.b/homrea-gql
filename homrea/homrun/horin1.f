C>    horin1
      SUBROUTINE HORIN1(NSPEC,NSYMB,NFILE3,NFIL10,NFILE6,IPAR,TSO,STOP,
     1     LIW,IW,LRW,RW,NOPT,LRP,RPAR,MULTIZ)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *         INPUT OF SECONDARY DATA FROM UNIT NFILE3          *
C     *                                                           *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C     INPUT DATA IN SI UNITS (IF NOT SPECIFIED)
C
C***********************************************************************
C**********************************************************************
C
C     CHAPTER I: STORAGE ORGANIZATION, INITIAL VALUES
C
C***********************************************************************
C***********************************************************************
C     I.1 TYPES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      PARAMETER (LCMAT=MNCOV*MNEQU)
      PARAMETER (LEMAT=(MNSPE+2)*MNCON)
      CHARACTER*8 ESYMB(MNELE)
      CHARACTER(LEN=*) NSYMB(*)
      LOGICAL DETONA,PCON,VCON,TEST,STOP,TCON,TSO
      INTEGER NWALL,NCOMPLX
      LOGICAL MOLEFR,MASSFR,LPLO
C***********************************************************************
C     I.2 ARRAYS
C***********************************************************************
      DIMENSION IPAR(*),NOPT(*),RPAR(LRP)
      DIMENSION RW(LRW)
C---- DUMMY ARRAY
      DIMENSION IDUM(1)
      DIMENSION IW(LIW)
      DIMENSION AMAT(MNCON*MNEQU)
C**********************************************************************C
C     I.3 ARRAY COMMON BLOCKS
C**********************************************************************C
      DIMENSION IFSP(MNSPE)
      COMMON/BOUT/KONINT,KONTAB,KONPLO,KONSEN,KONMEC,KONANA,KONRED,
     1  KONSPE(MNSPE),KONSID(MNSPE),NSC,
     1  ICRIT,KOGSEN,KONRST,MOLEFR,MASSFR,LPLO
      COMMON/ILDMOP/IPARA,ISTAOP,MANPAR,NOFORM,ANAFUE,INMAN,LASLOO,
     1              ICALTR,MASENS,IUPVER,NTRANS
      COMMON/BHCP/HLHH(7,MNSPE*2),HINF(3,MNSPE)
      COMMON/BICMAT/COMAT(MNCOV*MNEQU),EMAT((MNSPE+2)*MNCON)
      COMMON/BECVA/ECV(2,MNCOV)
      COMMON/BLIMIT/XLIMIT(MNLIM*(MNSPE+2)),ELIMIT(2,MNLIM  ),NLIM
      COMMON/BULIMI/ULIMIT(MNLIM*(MNSPE+2)),UELIM(2,MNLIM  ),NULIM
      COMMON/BGRX/GRVE(MNLIM*(MNSPE+2)),GRVAL(2,MNLIM  ),NGRV
      COMMON/BGRS/GSVE(MNLIM*(MNSPE+2)),GSVAL(2,MNLIM  ),NGSV
      PARAMETER (LXLIM=(MNSPE+2)*MNLIM,LULIM=(MNSPE+2)*MNLIM)
      COMMON/BCVB/CVBACK(MNCON*MNCON)
      COMMON/BCHIS/EWF(MNCON,7),SMFF(MNEQU,7),PAT(2,MNTCO),
     1       MIXFRA,MENFRA,MPRFRA
      COMMON/BREDUC/NRPE,NSQS,NLEF,IRPE,ISQS,ILEF,IRED(MNSPE)
      COMMON/BSENS/LSMAX,NSENS,NRSENS,NISENS,NPSENS,
     1       IRSENS,IISENS,IPSENS,ISENSI(MNSEN)
      COMMON/BMIX/OMEGA,SMFMIX(MNEQU),SMFCHA(MNEQU),VDIR(MNEQU,NDIMM)
C**********************************************************************C
C     I.4 NON-ARRAY OR FIXED ARRAY COMMON BLOCKS
C**********************************************************************C
      COMMON/BDIM/NSMAX,NRMAX,NCRMAX,NPOMAX,NELMAX
      COMMON/BTIME/TBEG,TEND,TSTEPS,ATOL,RTOL,ATOLS,RTOLS,STEP
      COMMON/BINFO/INFORM(20)
      COMMON/BOPTL/PCON,VCON,TCON,DETONA,NWALL,NCOMPLX,NPSR
      COMMON/SPOPT1/NEXCET,NENGIN
      COMMON/BCRE/STOBR(MNCRE,6),NUMS(MNCRE,6),ORD(MNCRE,6),
     1            ABRU(MNCRE),EBRU(MNCRE),TBRU(MNCRE),NBRUT
      COMMON/BSSPV/SSPV(20*MNSPE)
      COMMON/ILDMPA/IMAILD,ILMILD,IOILDM
      COMMON/BIMIXP/IMIXP
      COMMON/BREDIM/REDIMOPT(20)
      DATA MNREDO/20/
C**********************************************************************C
C     I.5.: INITIALIZATION
C**********************************************************************C
      DATA ONE/1.0D0/,ZERO/0.0D0/,XLARGE/1.D30/
C***********************************************************************
C---- STORE NUMBERS IN IPAR
      DO 410 I=1,5
      IPAR(I)=0
  410 CONTINUE
C***********************************************************************
C
C     CHAPTER II: INPUT OF DATA
C
C***********************************************************************
C***********************************************************************
C     II.1: INPUT OF OPTIONS
C***********************************************************************
C1234 TEST=.TRUE.
      REWIND NFILE3
      READ(NFILE3,800)  (NOPT(I),I=1,60)
      TEST=.FALSE.
      IF(NOPT(39).GE.4) TEST=.TRUE.
      TSO=.FALSE.
      IF(NOPT(40).EQ.1) TSO=.TRUE.
      STOP=.TRUE.
      IF(TEST) WRITE(NFILE6,800)  (NOPT(I),I=1,60)
      MULTIZ=0
      IF(NOPT(32).NE.0) MULTIZ = NOPT(32)
      NENGIN=0
      IF(NOPT(5).NE.0) THEN
        NENGIN = NOPT(5)
        MULTIZ = MAX(1,NOPT(32))
      ENDIF
      NEXCET = 0
      IF(NOPT(36).NE.0) NEXCET = NOPT(36)
      NTRANS = NOPT(21)
      IF(NTRANS.EQ.1) NTRANS = 0
      IF(NPSR.NE.0.AND.(.NOT.VCON)) GOTO 990
C***********************************************************************
C     CENTER OPTION IS CHOOSEN, VPRO OPTION HAVE TO BE 1
C     (A.Schubert)
C***********************************************************************
      IF(NEXCET .GT. 0) NOPT(1) = 1
C***********************************************************************
C***********************************************************************
C     DEFINE LOGICAL EXPRESSIONS FOR OPTIONS AND CHECK THEM
C***********************************************************************
      VCON=.FALSE.
      IF(NOPT(1).NE.0) VCON=.TRUE.
      PCON=.NOT.VCON
      TCON=.FALSE.
      IF(NOPT(2).NE.0) TCON=.TRUE.
      NPSR = NOPT(6)
      NWALL=0
      IF(NOPT(4).NE.0) THEN
        NWALL  = NOPT(4)
        MULTIZ = MAX(1,NOPT(32))
      ENDIF
      NCOMPLX=0
      IF(NOPT(35).NE.0) NCOMPLX = NOPT(35)
      DETONA=.FALSE.
      IF(NOPT(2).EQ.2) DETONA = .TRUE.
      IF(DETONA) TCON=.FALSE.
C---- CHECK FOR VALIDITY
      IF(PCON.AND.(.NOT.TCON)) IEQ = 1
      IF(VCON.AND.(.NOT.TCON)) IEQ = 2
      IF(PCON.AND.      TCON)  IEQ = 5
      IF(VCON.AND.      TCON)  IEQ = 6
      IPAR(10) = IEQ
C***********************************************************************
C     Input of number of species and elements
C***********************************************************************
      READ(NFILE3,801)  NSPEC
      IF(TEST) WRITE(NFILE6,801)  NSPEC
      IF(NSPEC.GT.MNSPE) GO TO 960
      READ(NFILE3,801)  NE
      IF(TEST) WRITE(NFILE6,801)  NE
      IF(NE.GT.MNELE) GO TO 960
C***********************************************************************
C     Input of species symbols and molar masses
C***********************************************************************
      IERR = 1
      ICALTR = NOPT(21)
      IF(ICALTR.GT.0) IERR = 0
      LRSPEC = MNRSPE
      CALL INPSPE(NSPEC,NE,NFILE3,NFILE6,1,IERR,NSYMB,XMOL,ESYMB,EMOL,
     1            XE,LRSPEC,RSPEC,NEPS,NSIG,NHL,NHSW,NETA,NLCOEF,
     1            NLCOEM,NLCOED,NDCOEF)
      IF(IERR.GT.0) GOTO 999
      IPAR(2) = NSPEC + 2
C**********************************************************************
C     Calculate inverse of molar mass
C**********************************************************************
      IF (NOPT(7).NE.0) THEN
         DO I=1,NSPEC
            XMINV(I) = ONE / XMOL(I)
         ENDDO
      ENDIF
C***********************************************************************
C     II.5 INPUT OF THERMODYNAMIC DATA
C***********************************************************************
      DO 110 I=1,NSPEC
      READ(NFILE3,807)
     1        (HLHH(K,I),K=1,7),(HLHH(K,I+NSPEC),K=1,7),
     1        (HINF(K,I),K=1,3)
      IF(TEST) WRITE(NFILE6,804)  NSYMB(I),(HLHH(K,I)      ,K=1,7),
     1                            NSYMB(I),(HLHH(K,I+NSPEC),K=1,7),
     1                            NSYMB(I),(HINF(K,I),K=1,3)
  110 CONTINUE
C***********************************************************************
C     II.6 INPUT OF ELEMENTARY REACTION MECHANISM
C***********************************************************************
      IZER = 0
      IONE = 1
      ITWO = 2
      LIREAC = MNIREA
      LRREAC = MNRREA
      NRTAB  = 20
      CALL INPMEC(NSPEC,NSYMB,NFILE3,NFIL10,NFILE6,IZER,IERR,
     1    NREAC,NM,NRINS,NRTAB,NINERT,
     1    LIREAC,IREAC,NIREV,NIARRH,NMATLL,NMATLR,NNRVEC,
     1    LRREAC,RREAC,NPK,NTEXP,NEXA,NVT4,NVT5,NVT6,
C-OL 1    NXZSTO,NRATCO,IONE,IDUM)
     1    NXZSTO,NRATCO,IONE,IDUM,
     1    NAINF,NBINF,NEINF,NT3,NT1,NT2,NAFA,NBFA)
CMM 28/05/10 Check if number of third bodies exceeds
CMM predefined maximum number of third bodies MNTHB in homdim.f
      if(NM.GT.MNTHB) then
         write(*,*)'*** Adjust maximum number of third bodies MNTHB
     1in homdim.f ***'
         write(*,333)'Number of third bodies',nm,
     1' greater than MNTHB: ',mnthb
         stop
      endif
  333 format(A,I2,A,I2)
C***********************************************************************
C     II.5 INPUT OF ELEMENT COMPOSITION INFORMATION
C***********************************************************************
C***********************************************************************
C     CALCULATE STOICHIOMETRIC MATRIX FOR ORIGINAL VECTORS
C***********************************************************************
      NS = NSPEC
      IF(NOPT(7).NE.0) THEN
      IXRVE = 1
      IXGVE = IXRVE  + NE * NSPEC
      IXLIM = IXGVE  + MNCOV*NSPEC
      IUXLIM= IXLIM  + MNLIM*NSPEC
      ILRW  = IUXLIM + MNLIM*NSPEC
      LLRW  = LRW - ILRW
      IFEGV = 1
      MNDM = MNCOV - 2
      MNLIT = MNLIM-2
      CALL GENPMA(NFILE3,NSPEC,NE,
     1    RW(IXRVE),NGVE,RW(IXGVE),ECV(1,3),MNDM,
     1     NLS,RW(IXLIM),ELIMIT(1,3),NULS,RW(IUXLIM),UELIM(1,3),
     1     NGRV,GRVE,GRVAL,NGSV,GSVE,GSVAL,
     1   MNLIT,IERR)
C hier REDIM Optionen lesen
      if (nopt(24).ge.1)then
      call readredimopt(NFILE3,MNREDO,NUMREDO,redimopt)
      end if
      ELIMIT(1,1:2) = -XLARGE
      ELIMIT(2,1:2) =  XLARGE
      UELIM(1,1:2) =  -XLARGE
      UELIM(2,1:2) =  XLARGE
      CALL INILDM(NSPEC,NHPW,IEQ,
     1    NC,COMAT,LCMAT,NCS,EMAT,LEMAT,NLIM,XLIMIT,LXLIM,
     1    NULIM,ULIMIT,LULIM,
     1    NE,RW(IXRVE),NGVE,RW(IXGVE),NLS,RW(IXLIM),NULS,RW(IUXLIM),
     1    IFEGV,LIP,IPAR,LRP,RPAR,
     1    LIW,IW,LLRW,RW(ILRW),NFILE6)
C*******************************************************************
C     read composition of fuel and oxidizer stream
C*******************************************************************
C***********************************************************************
C A. Neagos:
C Initialize MIXFRA,MENFRA AND MPFRA
C***********************************************************************
      MIXFRA=0
      MENFRA=0
      MPRFRA=0
C***********************************************************************
C A. Neagos:
C Not necessary if INMAN(NOPT(24))=1, e.g. if REDIM is generated
C***********************************************************************
      IF (NOPT(24).LT.1)THEN
      READ(NFILE3,892) ICPRO
      IPAR(39) = ICPRO
      READ(NFILE3,893) MIXFRA,MENFRA,MPRFRA
      IF(MIXFRA.GT.6) THEN
      WRITE(NFILE6,*) ' MIXFRA(',MIXFRA,') > 6'
      STOP
      ENDIF
  892 FORMAT(20I3)
  893 FORMAT(3(I3,5X))
      READ(NFILE3,824) PAT(1,1),PAT(2,1),PAT(1,4),PAT(2,4)
      READ(NFILE3,824) PAT(1,2),PAT(2,2)
      READ(NFILE3,824) PAT(1,3),PAT(2,3)
      MFP1 = MIXFRA + 1
      DO JJ=1,MFP1
      READ(NFILE3,824) HHEL,PHEL,RHOHEL,THEL,(SMFF(I+2,JJ),I=1,NSPEC)
      READ(NFILE3,824) HWF,PWF,RHOWF,TWF,(EWF(I+2,JJ),I=1,NE)
      IF(IEQ.EQ.1) THEN
      SMFF(1,JJ)=HHEL
      SMFF(2,JJ)=PHEL
      EWF(1,JJ) =HWF
      EWF(2,JJ) =PWF
      ELSEIF(IEQ.EQ.2) THEN
      SMFF(1,JJ)=HHEL
      SMFF(2,JJ)=RHOHEL
      EWF(1,JJ) =HWF
      EWF(2,JJ) =RHOWF
      ELSEIF(IEQ.EQ.5) THEN
      SMFF(1,JJ)=THEL
      SMFF(2,JJ)=PHEL
      EWF(1,JJ) =TWF
      EWF(2,JJ) =PWF
      ELSEIF(IEQ.EQ.6) THEN
      SMFF(1,JJ)=THEL
      SMFF(2,JJ)=RHOHEL
      EWF(1,JJ) =TWF
      EWF(2,JJ) =RHOWF
      ELSE
         write(*,*) 'IEQ not suitable for ILDM'
         STOP
      ENDIF
      ENDDO
      IF(MIXFRA.EQ.0) THEN
            SMFF(1:NSPEC+2,2) = SMFF(1:NSPEC+2,1)
            EWF(1:NE+2,2) = EWF(1:NE+2,1)
      ENDIF
C***********************************************************************
C     Calculate matrix for conserved variable tabulation
C***********************************************************************
      SMFMIX(1:NSPEC+2) = 0
      READ(NFILE3,891) IMIXP,OMEGA
      IF(IMIXP.NE.0) THEN
      IF(IEQ.EQ.1) THEN
        READ(NFILE3,824) SMFMIX(1),SMFMIX(2),DU1,DU2,SMFMIX(2+1:NSPEC+2)
      ELSEIF(IEQ.EQ.2) THEN
        READ(NFILE3,824) SMFMIX(1),DU1,SMFMIX(2),DU2,SMFMIX(2+1:NSPEC+2)
      ELSEIF(IEQ.EQ.5) THEN
        READ(NFILE3,824) DU1,SMFMIX(2),DU2,SMFMIX(1),SMFMIX(2+1:NSPEC+2)
      ELSEIF(IEQ.EQ.6) THEN
        READ(NFILE3,824) DU1,DU2,SMFMIX(2),SMFMIX(1),SMFMIX(2+1:NSPEC+2)
      ENDIF
      ENDIF
      READ(NFILE3,891) ICHAP,DUMMY
      IF(ICHAP.NE.0) THEN
      IF(IEQ.EQ.1) THEN
        READ(NFILE3,824) SMFCHA(1),SMFCHA(2),DU1,DU2,SMFCHA(2+1:NSPEC+2)
      ELSEIF(IEQ.EQ.2) THEN
        READ(NFILE3,824) SMFCHA(1),DU1,SMFCHA(2),DU2,SMFCHA(2+1:NSPEC+2)
      ELSEIF(IEQ.EQ.5) THEN
        READ(NFILE3,824) DU1,SMFCHA(2),DU2,SMFCHA(1),SMFCHA(2+1:NSPEC+2)
      ELSEIF(IEQ.EQ.6) THEN
        READ(NFILE3,824) DU1,DU2,SMFCHA(2),SMFCHA(1),SMFCHA(2+1:NSPEC+2)
      ENDIF
      ENDIF
  891 FORMAT(I3,20x,1PE10.3)
C***********************************************************************
C     Calculate matrix for conserved variable tabulation
C***********************************************************************
C---- Will be calculated in Subroutine -AUTPAR-
C***********************************************************************
C     Calculate matrix for CVUPD
C***********************************************************************
C---- form A^T * A
      NEP2 = NE+2
      CALL DGEMM('T','N',MFP1,MFP1,NEP2,
     1     ONE,EWF,MNCON,EWF,MNCON,ZERO,AMAT,MFP1)
      CALL DGEINV(MFP1,AMAT,CVBACK,IW,INFO)
      IF(INFO.GT.0) THEN
         WRITE(NFILE6,*) 'ERROR IN INVERSION OF AMAT'
C        RETURN
      ENDIF
      CALL DGEMM('N','T',MFP1,NEP2,MFP1,
     1     ONE,AMAT,MFP1,EWF,MNCON,ZERO,CVBACK,MFP1)
C***********************************************************************
C     REDUCED MECHANISM
C***********************************************************************
      IRPE=1
      READ(NFILE3,826) NRPE
      IF(NRPE.GT.0) READ(NFILE3,823) (IRED(I-1+IRPE),I=1,NRPE)
      ISQS=IRPE+NRPE
      READ(NFILE3,826) NSQS
      IF(NSQS.GT.0) READ(NFILE3,823) (IRED(I-1+ISQS),I=1,NSQS)
      ILEF=ISQS+NSQS
      READ(NFILE3,826) NLEF
      IF(NLEF.GT.0) READ(NFILE3,823) (IRED(I-1+ILEF),I=1,NLEF)
  826 FORMAT(I3,13X,15(I3))
  820 FORMAT(1X)
  821 FORMAT(I6,I6,I6)
  823 FORMAT(25I3)
  824 FORMAT(5(1PE16.9))
      ENDIF
      ENDIF
C***********************************************************************
C     II.7 INPUT OF COMPLEX REACTION MECHANISM
C***********************************************************************
      READ(NFILE3,801)            NBRUT
      IF(TEST) WRITE(NFILE6,801)  NBRUT
      IF(NBRUT.EQ.0) GO TO 160
      DO 150 I=1,NBRUT
         READ(NFILE3,842) (STOBR(I,K),K=1,6)
         IF(TEST) WRITE(NFILE6,842) (STOBR(I,K),K=1,6)
         READ(NFILE3,843) (NUMS(I,K),K=1,6)
         IF(TEST) WRITE(NFILE6,843) (NUMS(I,K),K=1,6)
         READ(NFILE3,842) (ORD(I,K),K=1,6)
         IF(TEST) WRITE(NFILE6,842) (ORD(I,K),K=1,6)
C---- RATE COEFFICIENTS A(M,MOL,S), B(-), E(J/MOL)
         READ(NFILE3,802) ABRU(I),TBRU(I),EBRU(I)
         IF(TEST) WRITE(NFILE6,802) ABRU(I),EBRU(I),TBRU(I)
 150  CONTINUE
 160  CONTINUE
C***********************************************************************
C     OUTPUT OF STRING FOR END ON NFILE3
C***********************************************************************
 809  FORMAT(1(15X,20I3))
      READ(NFILE3,809) (IFSP(I),I=1,NSPEC)
C**********************************************************************C
C     COMPUTE  ICOSPE                                                  C
C**********************************************************************C
      NSC = 0
      DO 2133 I=1,NSPEC
         IF(IFSP(I).NE.0) THEN
            NSC=NSC+1
            KONSPE(NSC)=I
         ENDIF
 2133 CONTINUE
C**********************************************************************
C     Set up thermodynamics vector
C**********************************************************************
      CALL DCOPY(NSPEC   ,XMOL,1,SSPV(1)         ,1)
      CALL DCOPY(NSPEC*14,HLHH,1,SSPV(NSPEC+1)   ,1)
      CALL DCOPY(NSPEC* 3,HINF,1,SSPV(NSPEC*15+1),1)
C***********************************************************************
C***********************************************************************
C
C      INITIALIZE CONTROL VARIABLES
C
C***********************************************************************
C**********************************************************************
C     IV.: SET UP LOCATIONS
C**********************************************************************
                    NEQ=NSPEC
      IF(.NOT.TCON) NEQ=NSPEC+1
      IF(DETONA) NEQ = NSPEC + 4
      IPAR(1)=NEQ
      IPAR(3)=NSPEC
      IPAR(6)=NE
      IPAR(9)=NREAC
      IPAR(7)=NE + 2
C***********************************************************************
C     II.8 SET UP POINTERS FOR OUTPUT CONTROL
C***********************************************************************
      IF(.NOT.TCON) THEN
          NSC=NSC+1
          KONSPE(NSC)=NSPEC+1
      ENDIF
C***********************************************************************
C     II.8 SET UP POINTERS FOR SENSITIVITY ANALYSIS
C***********************************************************************
      KONISE = NOPT(9)
C---- CHECK INPUT FOR SENSITIVITY ANALYSIS WITH RESPECT TO MECHANISM
      KONSEN=NOPT(11)
      KOGSEN=NOPT(10)
C---- LOCAL AND GLOBAL SENSITIVITY NOT ALLOWED
      IF((KONSEN.NE.0).AND.(KOGSEN.NE.0)) GO TO 980
      IF((KONSEN.GT.0).AND.(NSC.EQ.0))    KONSEN=0
      IF((KOGSEN.GT.0).AND.(NSC.EQ.0))    KOGSEN=0
      KONRSE = 0
      IF(KONSEN.GT.0) KONRSE = 1
      IF(KOGSEN.GT.0) KONRSE = 2
      CALL INISEN(NEQ,NSENS,NISENS,NRSENS,NPSENS,
     1     IISENS,IRSENS,IPSENS,ISENSI,
     1     NSPEC,KONISE,NREAC,KONRSE,IREAC(NIREV),KONOSE,NSC,KONSPE)
      MOLEFR=.FALSE.
      IF(NOPT(17).EQ.1) MOLEFR=.TRUE.
      MASSFR=.FALSE.
C     IF(NOPT(7).GE.1)  MASSFR=.TRUE.
      IF(NOPT(17).EQ.2) MASSFR=.TRUE.
      LPLO=.FALSE.
      IF(NOPT(19).EQ.1) LPLO=.TRUE.
C***********************************************************************
C     INITIALIZE CONTROL VARIABLES FOR INTEGRATION
C***********************************************************************
C---- USE OF EXTRAPOLATION INTEGRATOR
      IPAR(4) = NSENS
      IPAR(5) = NSENS + 1
C***********************************************************************
C     INITIALIZE CONTROL VARIABLES FOR OUTPUT
C***********************************************************************
C---- OUTPUT OF INTERMEDIATE RESULTS
      KONINT=NOPT(39)
C---- OUTPUT OF TABLES AT END OF CALCULATION
      KONTAB=NOPT(15)
C---- OUTPUT OF PLOTS
      KONPLO=NOPT(16)
C---- OUTPUT OF DATA FOR MECHANISM ANALYSIS
      KONMEC=NOPT(12)
C---- Input files
      KONRST=NOPT(3)
C     KONRST not used right now
C---- EIGENVECTOR ANALYSIS
      KONANA=NOPT(7)
C---- REDUCED MECHANISM
      KONRED = NOPT(13)
C---- STARTING OPTIONS FOR REDUCED MECHANISM
      IF(NOPT(13).LT.0) ISTAOP = NOPT(13)
      IF(NOPT(13).EQ.0) ISTAOP = 0
      IF(NOPT(13).EQ.1) ISTAOP = 0
      IF(NOPT(13).EQ.2) ISTAOP = 1
      IF(NOPT(13).EQ.3) ISTAOP = 2
      IF(NOPT(13).EQ.99) ISTAOP = 99
c---- Last Loop
      LASLOO = NOPT(26)
      IMAILD = NOPT(29)
      ILMILD = NOPT(30)
      IOILDM  = NOPT(31)
C---- MANUAL PARAMETERIZATION
      MANPAR = NOPT(14)
c---- fixed or generalized coordinates
      IF(NOPT(25).EQ.0) IPARA = 1
      IF(NOPT(25).EQ.1) IPARA = 1
      IF(NOPT(25).EQ.2) IPARA = 2
      IF(NOPT(25).LT.0.OR.NOPT(25).GT.2)THEN
         WRITE(*,*) 'ERROR - invalid option THETA'
         STOP
      ENDIF
C---- ANALYSIS OF THE EIGENVALUES OF TWO FUELS
      ANAFUE = 0
      IF(NOPT(13).EQ.3.AND.NOPT(23).EQ.1) ANAFUE = 1
C---- CALCULATION OF AN INERTIAL MANIFOLD
      INMAN = 0
c     IF(NOPT(13).EQ.3.AND.NOPT(24).EQ.1) INMAN = 1
      IF(NOPT(13).NE.0.AND.NOPT(24).GE.1) INMAN = NOPT(24)
      IF(NOPT(24).LT.0) INMAN = NOPT(24)
C---- Options for REDIM
      NOFORM = NOPT(22)
C----
      IUPVER = NOPT(33)
C---- SPECIES CONSIDERED IN IGNITION CRITERION (OR TEMPERATURE)
      ICRIT=NOPT(18)
C     IF(ICRIT.NE.0.AND.(.NOT.TCON)) ICRIT=NSPEC+1
      IF(ICRIT.GT.NSPEC.AND.(.NOT.TCON)) ICRIT=NSPEC+1
      IF(ICRIT.GT.NSPEC.AND.TCON) ICRIT=NSPEC
C***********************************************************************
C     INITIALIZE ADDITIONAL SYMBOLS IN NSYMB
C***********************************************************************
      LCS=LEN(NSYMB(NSPEC+1))
C---- ASSIGN SYMBOLS FOR PHYSICAL VARIABLES
      NSYMB(NSPEC+1) =  ' T (K)  '//REPEAT(' ',LCS-8)
      NSYMB(NSPEC+2) =  ' P (BAR)'//REPEAT(' ',LCS-8)
      NSYMB(NSPEC+3) =  '   C    '//REPEAT(' ',LCS-8)
C**********************************************************************
C***********************************************************************
C
C     CHAPTER VI: OUTPUT OF DATA FOR MECHANISM ANALYSIS
C
C***********************************************************************
C***********************************************************************
      IF(KONMEC.EQ.0) GOTO 510
      WRITE(NFIL10,800) (NOPT(I),I=1,40)
      WRITE(NFIL10,851) NSPEC,NREAC,NRINS,NM
      WRITE(NFIL10,853) (NSYMB(I),I=1,NSPEC)
      WRITE(NFIL10,855) (IREAC(NIREV-1+I),I=1,NREAC)
      WRITE(NFIL10,857)
  857 FORMAT('>MATRIX')
      WRITE(NFIL10,856)  (
     1      (IREAC(NMATLL+(I-1)*3+K-1),K=1,3),
     2      (IREAC(NMATLR+(I-1)*3+K-1),K=1,3),
     2                               I=1,NREAC)
  856 FORMAT(6I5)
  859 FORMAT(3I5)
      WRITE(NFIL10,858)
  858 FORMAT('>NRVEC ')
      WRITE(NFIL10,859) ((
     1       IREAC(NNRVEC+(I-1)*3+K-1),K=1,3),I=1,NRINS)
      WRITE(NFIL10,861)
  861 FORMAT('>DATA  ')
  510 CONTINUE
C***********************************************************************
C
C     CHAPTER VI: SET UP INFORMATIONS VECTOR
C
C***********************************************************************
C     INFORM(10) :    INFORMATION ABOUT SOLUTION METHOD
C                     0  BDF-CODE DASSL  IS USED
C                     1  EXTRAPOLATION CODE LIMEX IS USED
C                    -1  EXPLICIT EXTRAPOLATION CODE EULEX IS USED
      IF(NOPT(20).EQ.-1) THEN
        INFORM(10)=-1
      ELSE IF(NOPT(20).EQ.1) THEN
        INFORM(10)=1
      ELSE
        INFORM(10)=0
      ENDIF
C***********************************************************************
C
C     CHAPTER VII: REGULAR EXIT
C
C***********************************************************************
C***********************************************************************
      RETURN
C***********************************************************************
C***********************************************************************
C
C     CHAPTER  ERROR EXITS
C
C***********************************************************************
C***********************************************************************
C 940 CONTINUE
C     IF(NREAC.GT.MNREA) WRITE(NFILE6,942) NREAC,MNREA
C 942 FORMAT(' ',5X,'ERROR - TOO MANY REACTIONS: NREAC =',I4,
C    1       '  >  MNREA = ',I4,')',/,
C    2       6X,'CHECK COMMON BLOCK BSENS ')
C     GOTO 999
  960 CONTINUE
      IF(NSPEC.GT.MNSPE) WRITE(NFILE6,962) NSPEC,MNSPE
  962 FORMAT(' ',5X,'ERROR - NSPEC = ',I4,'  >  MNSPE = ',I4,/,
     1       6X,'CHECK COMMON BLOCKS BXZSTO,BHCP AND BSH IN HORIN1  ')
      GO TO 999
  980 WRITE(NFILE6,981)
  981 FORMAT('1',5X,'ERROR - OPTIONS LSEN,GSEN,ISEN INCOMPATIBLE')
      GO TO 999
  990 WRITE(NFILE6,981)
  991 FORMAT('1',5X,'ERROR - OPTIONS PSR PCON  INCOMPATIBLE')
      GO TO 999
  999 CONTINUE
      WRITE(NFILE6,998)
      WRITE(NFILE6,998)
      WRITE(NFILE6,998)
  998 FORMAT(' ','   ++++++      ERROR IN HORIN1      ++++++   ')
      STOP
C***********************************************************************
C     CHAPTER IX: FORMAT STATEMENTS
C***********************************************************************
  800 FORMAT(12X,20I3)
  801 FORMAT(I5)
  802 FORMAT(12X,5(1PE11.4,1X))
  804 FORMAT(' HL  (',A,')',4(1X,1PE11.4),/,11X,3(1X,1PE11.4),/,
     1       ' HH  (',A,')',4(1X,1PE11.4),/,11X,3(1X,1PE11.4),/,
     1       ' HINF(',A,')',4(1X,1PE11.4))
  806 FORMAT(15(I3,1X))
  807 FORMAT(16X,5(1PE11.4,1X),/5(1PE11.4,1X),/,
     1      4(1PE11.4,1X),2X,3(1X,1PE9.2))
C 831 FORMAT(12X,30I2)
  842 FORMAT(12X,8(F6.3,1X))
  843 FORMAT(12X,8(2X,I2,3X))
C---- FORMAT STATEMENTS FOR OUTPUT ON NFIL10
  851 FORMAT(' ','NS= ',I5,' NR=',I5,' NG=',I5,' NM=',I5)
  853 FORMAT(' ',A)
  855 FORMAT(' ','IRUECK',30I2)
C***********************************************************************
C     END OF -HORIN1-                                                  *
C***********************************************************************
      END

      SUBROUTINE HORIN2(NE,NS,NSYMB,NFILE3,NFILE6,NOPT,STOP,FINISH,
     1    TIME,NEQ,S,CINT,MULTIZ)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *       INPUT OF THE SECONDARY DATA FROM UNIT NFILE3        *
C     *                                                           *
C     *              LAST CHANGE: 27.04.1987/MAAS                 *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C     INPUT DATA IN SI UNITS (IF NOT SPECIFIED)
C
C***********************************************************************
C     CHAPTER I: STORAGE ORGANIZATION, INITIAL VALUES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      CHARACTER(LEN=*)  NSYMB(MNSPE+MNTHB+1)
      LOGICAL TEST,STOP,FINISH,DETONA,PCON,VCON,TCON
      INTEGER NWALL,NCOMPLX
      COMMON/BPSR/DNDTIN,DNDTOU,TMIX,CMIX,CIMIX(MNSPE)
      COMMON/BOPTL/PCON,VCON,TCON,DETONA,NWALL,NCOMPLX,NPSR
      COMMON/SPOPT1/NEXCET,NENGIN
      COMMON/BDIM/NSMAX,NRMAX,NCRMAX,NPOMAX,NELMAX
      COMMON/BTIME/TBEG,TEND,TSTEPS,ATOL,RTOL,ATOLS,RTOLS,STEP
      COMMON/BSURR/TSURR,OVOL,ALPHA
      COMMON/BINIT/P0,T0,V0,H0,XU(MNSPE)
      COMMON/BPOI/POI(3,MNPOI),TIM(3,MNPOI),NPO(3)
      DIMENSION S(NEQ),AFRAMU(MNPOI)
      DIMENSION NOPT(*),CINT(10)
      DOUBLE PRECISION, DIMENSION(MNZONE+1):: ALPHA
      DATA RGAS/8.3143D0/
C**********************************************************************
C     CHAPTER II: INPUT OF DATA
C**********************************************************************
      TEST=.FALSE.
      FINISH=.FALSE.
      READ(NFILE3,804,END=100) NPO(1),NPO(2),NPO(3)
      DO 20 LL=1,3
      NPNTS=NPO(LL)
      IF(NPNTS.GT.MNPOI) GO TO 910
      DO 20 J=1,NPNTS
      READ(NFILE3,806,END=920) POI(LL,J),TIM(LL,J)
   20 CONTINUE
      READ(NFILE3,809,END=920)   (XU(I),I=1,NS)
      IF(TEST) WRITE(NFILE6,802) (XU(I),I=1,NS)
C---  CONDITIONS
      READ(NFILE3,802,END=920)   TBEG,TEND,TSTEPS
      IF(TEST) WRITE(NFILE6,802) TBEG,TEND,TSTEPS
      READ(NFILE3,802,END=920)   ATOL,RTOL,STEP,ATOLS,RTOLS
      IF(TEST) WRITE(NFILE6,802) ATOL,RTOL,STEP,ATOLS,RTOLS
C A. Schubert, 02/10/2005
      IF(NWALL .GE. 1) THEN
        READ(NFILE3,802,END=920)   TSURR,OVOL,ALPHA(1),ALPHA(2)
        IF(TEST) WRITE(NFILE6,802) TSURR,OVOL,ALPHA(1),ALPHA(2)
      ENDIF
      IF(NPSR  .NE. 0) THEN
        READ(NFILE3,802,END=920)   DNDTIN,DNDTOU,TMIX,CIMIX(1:NS)
        IF(TEST) WRITE(NFILE6,802)  DNDTIN,DNDTOU,TMIX,CIMIX(1:NS)
      ENDIF
      IF(MULTIZ.GE.1) THEN
        CALL MULIN1(NFILE3,NFILE6,NOPT,NSYMB,MULTIZ,NS,XU,AFRAMU,POI)
      ENDIF
      IF(NENGIN.GT.0) THEN
        CALL ENGIN1(NENGIN,XU,NS,NSYMB,TBEG,TEND,NFILE3,NFILE6)
      ENDIF
Cend A. Schubert
      CINT(1) =TBEG
      CINT(2) =TEND
      CINT(3) =TBEG
      CINT(4) =TSTEPS
      CINT(5) =ATOL
      CINT(6) =RTOL
      CINT(7) =ATOLS
      CINT(8) =RTOLS
      CINT(9) =STEP
C***********************************************************************
C     CHAPTER III.: SET INITIAL PROFILES FOR VARIABLES
C***********************************************************************
      TIME=0.D0
      CALL HORVAR(NFILE6,1,TIME,P0,PPR0)
      CALL HORVAR(NFILE6,2,TIME,V0,VPR0)
      CALL HORVAR(NFILE6,3,TIME,T0,TPR0)
      CIN=P0/(RGAS*T0)
      SUM=0.D0
      DO 8 I=1,NS
      SUM = SUM + XU(I)
    8 CONTINUE
      DO 10 I=1,NS
      S(I)=XU(I)*CIN/SUM
   10 CONTINUE
      IF(.NOT.TCON) S(NS+1)=T0
C*************
C Changed 27.6.2014 UM
      CMIX=P0/(RGAS*TMIX)
      SUM=0.D0
      DO I=1,NS
      SUM = SUM + CIMIX(I)
      ENDDO
      CIMIX(1:NS)=CIMIX(1:NS)*CMIX/SUM
C***********************************************************************
C     CHAPTER III.: SET INITIAL VALUES FOR SENS. WITH RESPECT TO REACT.
C***********************************************************************
      RETURN
C***********************************************************************
C     CHAPTER III: REGULAR EXIT (IF NOMORE DATASET IS AVAILABLE)
C***********************************************************************
  100 CONTINUE
      FINISH=.TRUE.
      RETURN
C***********************************************************************
C     CHAPTER IV: ERROR EXITS
C***********************************************************************
  910 WRITE(NFILE6,911) MNPOI,LL
  911 FORMAT(' ',5X,'ERROR - MORE THAN ',I3,' POINTS FOR VARIABLE NO.',
     F       I1,' DEFINED',/,6X,'CHECK COMMON BLOCK BPOI')
      GOTO 999
  920 WRITE(NFILE6,921)
  921 FORMAT(' ',5X,'ERROR - INPUT DATA INCOMPLETE')
      GOTO 999
  999 CONTINUE
      WRITE(NFILE6,998)
      WRITE(NFILE6,998)
      WRITE(NFILE6,998)
  998 FORMAT(' ','   ++++++      ERROR IN HORIN2      ++++++   ')
      STOP
C***********************************************************************
C     CHAPTER V: FORMAT STATEMENT                                      *
C***********************************************************************
  802 FORMAT(12X,5(1PE11.4,1X))
  809 FORMAT(12X,5(1PE20.13,1X))
  804 FORMAT(1X,I4,1X,I4,1X,I4)
  806 FORMAT(12X,1PE9.2,13X,1PE9.2)
C***********************************************************************
C     END OF -HORIN2-                                                  *
C***********************************************************************
      END
      subroutine readredimopt(nfile3,MNREDO,NUMREDO,redimopt)
        logical equsym
        character*13 flag
        double precision, dimension(MNREDO)        ::redimopt
        redimopt=0
        read(nfile3,801) NUMREDO,flag
        IF(NUMREDO.GT.MNREDO) GOTO 900
        if(.not.(equsym(13,flag,'REDIM options'))) goto 930
         write(*,*) 'hhhhhhhhhhhhhe'
        read(nfile3,824,end=940,err=900)(redimopt(i),i=1,NUMREDO)
C
C
C
  824 format(5(1pe16.9))
  801 FORMAT(I3,1X,A13)
      return
  900 CONTINUE
      WRITE(NFILE6,901)
  901 FORMAT('0',' Error in input occured in -readredimopt-')
      GOTO 999
  930 CONTINUE
      WRITE(NFILE6,931)
  931 FORMAT('0','Block for REDIM Options not found')
      GOTO 999
  940 CONTINUE
      WRITE(NFILE6,941)
  941 FORMAT('0',' end of file detected during input in readredimopt')
      GOTO 999
  999 CONTINUE
      WRITE(NFILE6,998)
  998 FORMAT(3(/,' +++++ Error in readredimopt +++++'))
      STOP

      end subroutine readredimopt

