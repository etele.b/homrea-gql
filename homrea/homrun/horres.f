C>    horres
      SUBROUTINE HORRES(TIME,S,SP,F,IRES,RPAR,IPAR)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *            INTERFACE FOR CONSERVATION EQUATIONS           *
C     *              (ADAPTED FOR SUBROUTINE -DASSL-)             *
C     *                                                           *
C     *               U.MAAS                                      *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C     INPUT/OUTPUT: SEE SUBROUTINE -DASSL-
C
C***********************************************************************
C***********************************************************************
C     CHAPTERI: STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION S(*),SP(*),F(*),RPAR(*),IPAR(*)
C***********************************************************************
C     CHAPTER II: CALL OF -HORDES-
C***********************************************************************
      NEQ=IPAR(1)
      CALL HORDES(NEQ,TIME,S,F,RPAR,IPAR)
      DO 15 J=1,NEQ
   15 F(J)=SP(J)-F(J)
      RETURN
C***********************************************************************
C     END OF -HORRES-
C***********************************************************************
      END
      SUBROUTINE HORRIG (NEQNEQ,NZV,TIME,S,F,BV,IR,IC,RPAR,IPAR,IFC,IFP)
C***********************************************************************
C
C     DEFINITION OF RIGHT HAND SIDE (SEE -ULIMEX-)
C
C***********************************************************************
C***********************************************************************
C     I.I.: TYPES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION S(*),F(*),BV(*),IR(*),IC(*)
      DIMENSION RPAR(*),IPAR(*)
      DIMENSION IFP(*)
C***********************************************************************
C     CALL OF PDESYS
C***********************************************************************
      NEQ= IPAR(1)
      CALL HORDES(NEQ,TIME,S,F,RPAR,IPAR)
      RETURN
C***********************************************************************
C     END OF FCN
C***********************************************************************
      END
      SUBROUTINE HORLEF (NZC,B,IR,IC)
C***********************************************************************
C
C     DEFINITION OF LEFT HAND SIDE (SEE -ULIMEX-)
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION B(*),IR(*),IC(*)
      DATA ONE/1.0D0/
C***********************************************************************
C
C***********************************************************************
      DO 10 I=1,NZC
      IR(I)=I
      IC(I)=I
  10  B(I)=ONE
      RETURN
C***********************************************************************
C     END OF LEFT
C***********************************************************************
      END
      SUBROUTINE UDFDYP(NSYS,NEQ,T,Y,YP,E,II,NZ,RPAR,IPAR)
      INTEGER NSYS,NEQ,II(1),NZ,IPAR(1)
      DOUBLE PRECISION T,Y(1),YP(1), E(1),RPAR(1)
C***********************************************************************
C     DUMMY SUBROUTINE FOR UDASSL
C***********************************************************************
      RETURN
      END
      SUBROUTINE HORDES(NEQ,TIME,S,F,RPAR,IPAR)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *           RESIDUALS OF THE CONSERVATION EQUATIONS         *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C                                                                      *
C     INPUT/OUTPUT: SEE  -DASSL AND LIMEX -                            *
C                                                                      *
C***********************************************************************
C                                                                      *
C                                                                      *
C                                                                      *
C***********************************************************************
C***********************************************************************
C
C     CHAPTER I: STORAGE ORGANIZATION                                  *
C
C***********************************************************************
C***********************************************************************
C     I.I.: TYPES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      LOGICAL PCON,VCON,TEST,STOP,TCON,DETONA
      LOGICAL PSR
      INTEGER NWALL,NCOMPLX
C***********************************************************************
C     I.II.: DIMENSIONS
C***********************************************************************
      DIMENSION S(*),F(*),RPAR(*),IPAR(*)
      DIMENSION HL(MNSPE),RATL(MNSPE),CPI(MNSPE)
      DIMENSION CPIMIX(MNSPE),HMIXTL(MNSPE),HMIXTM(MNSPE)
      COMMON/BHCP/HLHH(7,MNSPE*2),HINF(3,MNSPE)
      DIMENSION CIL(MNSPE+MNTHB+1)
      DIMENSION DGDC(1,1),DGDT(1),DODC(1,1),DODT(1)
C**********************************************************************C
C     I.III.: ARRAY COMMON BLOCKS
C**********************************************************************C
      COMMON/BINIT/P0,T0,V0,H0,XU(MNSPE)
      COMMON/BPSR/DNDTIN,DNDTOU,TMIX,CMIX,CIMIX(MNSPE)
C**********************************************************************C
C     I.IV.: NON ARRAY OR FIXED ARRAY COMMON BLOCKS
C**********************************************************************C
      COMMON/BOPTL/PCON,VCON,TCON,DETONA,NWALL,NCOMPLX,NPSR
      COMMON/SPOPT1/NEXCET,NENGIN
C***********************************************************************
C     BLOCK FOR INFORMATIONS ABOUT THE REACTION MECHANISM
C***********************************************************************
      COMMON/BICRES/ICRES
      COMMON/BRATES/TIMRAT,RATTEM(MNREA),RATINT(MNREA)
      COMMON/BSENS/LSMAX,NSENS,NRSENS,NISENS,NPSENS,
     1       IRSENS,IISENS,IPSENS,ISENSI(MNSEN)
      DIMENSION WORK1(MNREA)
      DATA LRW1/MNREA/
C**********************************************************************C
C     I.V.: DATA STATEMENTS
C**********************************************************************C
      DATA NFILE6/06/
      DATA ZERO/0.0D0/,ONE/1.0D0/,RGAS/8.3143D0/
C***********************************************************************
C
C     CHAPTER II: INITIALIZATION                                       *
C
C***********************************************************************
      STOP=.TRUE.
      TEST=.FALSE.
      NEQ=IPAR(1)
      NSPEC=IPAR(3)
      PSR = .FALSE.
      IF(NPSR.GT.0) PSR = .TRUE.
C***********************************************************************
C                                                                      *
C     CHAPTER II: CALCULATION OF FUNCTION                              *
C                                                                      *
C***********************************************************************
C***********************************************************************
C     II.2: SPECIES MASS CONSERVATION                                  *
C***********************************************************************
C***********************************************************************
C     II.2.1: CALC. MASS FRACTION, TEMPERATURE, PRESSURE, VOLUME       *
C***********************************************************************
C---- TEMPERATURE
      IF(.NOT.TCON) TL=S(NEQ)
      IF(TCON) CALL HORVAR(NFILE6,3,TIME,TL,TPR)
C---- PRESSURE
      IF(PCON) CALL HORVAR(NFILE6,1,TIME,PL,PPR)
C---- VOLUME
      IF(VCON.OR.PSR) CALL HORVAR(NFILE6,2,TIME,VL,VPR)
C---- RESTRICT TEMPERATURE
      TL = DMAX1(1.D1,TL)
C---- RESTRICT CONCENTRATIONS AND CALCULATE TOTAL CONCENTRATION
      CL = ZERO
C---- VORLAEUFIG
      NSPEC=NEQ
      IF(.NOT.TCON) NSPEC=NEQ-1
C
      DO 110 I=1,NSPEC
      CIL(I)=S(I)
      CL=CIL(I)+CL
  110 CONTINUE
C-NEW
C     CL = PL / (RGAS*TL)
C***********************************************************************
C     II.2.4: CALCULATON OF MOLAR REACTION RATES                       *
C***********************************************************************
      IPRIM = 0
      CALL MECHJA(0,NSPEC,NREAC,NM,NRINS,NRTAB,
     1  IREAC(NMATLL),IREAC(NNRVEC),RREAC(NPK),RREAC(NTEXP),RREAC(NEXA),
     1  RREAC(NVT4),RREAC(NVT5),RREAC(NVT6),IREAC(NIARRH),RREAC(NXZSTO),
     1  RREAC(NRATCO),CL,CIL,TL,RATL,NRSENS,ISENSI(IRSENS),RPAR(IRSENS),
     1  LRW1,WORK1,IPRIM,NFILE6,IERR,RREAC(NAINF),RREAC(NBINF),
     1  RREAC(NEINF),RREAC(NT3),RREAC(NT1),RREAC(NT2),RREAC(NAFA),
     1  RREAC(NBFA),HLHH,HINF,1,1,DGDC,DGDT,DODC,DODT)
C***********************************************************************
C     CALCULATION FOR MECHANISM ANALYSIS                               *
C***********************************************************************
      IF(ICRES.NE.0) THEN
      TIMRAT=TIME
      DO 2700 J=1,NREAC
      RATTEM(J)=WORK1(J)
 2700 CONTINUE
      ENDIF
C***********************************************************************
C     TAKE INTO ACCOUNT SURFACE REACTIONS
C***********************************************************************
      IF (NCOMPLX.NE.0) CALL HORWRE(NSPEC,CL,CIL,TL,NFILE6,RATL)
C---- SUM OF PRODUCTION TERM RATLI
      SUMRAT=ZERO
      DO 120 I=1,NSPEC
      SUMRAT=SUMRAT+RATL(I)
  120 CONTINUE
C***********************************************************************
C     OUTPUT FOR TESTING                                               *
C***********************************************************************
C=304 FORMAT('0','C,CIL',11X,6(1PE9.2,1X)/(16X,6(1PE9.2,1X)))
C=    IF(TEST) WRITE(NFILE6,304) CL,(CIL(I),I=1,NSPEC)
C=325 FORMAT('0','RATL',11X,6(1PE9.2,1X)/(16X,6(1PE9.2,1X)))
C=    IF(TEST) WRITE(NFILE6,325) (RATL(I),I=1,NSPEC)
C***********************************************************************
C     III.3: CALCULATION FOR ENERGY CONSERVATION
C***********************************************************************
      IF(TCON) GOTO 190
C---- CALC. OF HEAT CAPACITIES CP,CPI AND ENTHALPIES H
      CALL CALHCP(NSPEC,CIL,TL,HLHH,HINF,CPI,CPTOT,HL,IEHCP)
      CPL = CPTOT/CL
C---- PRODUCTION TERM HI*CI
      SUMH  =ZERO
      DO 130 I=1,NSPEC
      SUMH=SUMH+RATL(I)*HL(I)
  130 CONTINUE
      SUHPSR = ZERO
C---- CALC. OF HEAT CAPACITIES CP,CPI AND ENTHALPIES H for PSR mixing
      IF(PSR) THEN
C---  cp not needed, simply used as work array
      CALL CALHCP(NSPEC,CIMIX,TL  ,HLHH,HINF,CPIMIX,CPTOTM,HMIXTL,IEHCP)
      CALL CALHCP(NSPEC,CIMIX,TMIX,HLHH,HINF,CPIMIX,CPTOTM,HMIXTM,IEHCP)
C- H is here molar      
C     IF(IEHCP.GT.0) GO TO 2222
      SUHPSR= ZERO
      DO 135 I=1,NSPEC
      SUHPSR=SUHPSR+(HMIXTM(I)-HMIXTL(I))*CIMIX(I)/CMIX
  135 CONTINUE
      ENDIF
C***********************************************************************
C     OUTPUT FOR TESTING                                               *
C***********************************************************************
C3141 FORMAT('0','CP,CPI',9X,6(1PE9.2,1X)/(16X,6(1PE9.2,1X)))
C              WRITE(NFILE6,3141) CPL,(HL(I),I=1,NSPEC)
C=    IF(TEST) WRITE(NFILE6,188) ELOSS
C=188 FORMAT(' ','ELOSS=  ',E10.2,1X)
C=    IF(TEST) WRITE(NFILE6,189) QADD
C=189 FORMAT(' ','QADD=  ',E10.2,1X)
C
  190 CONTINUE
C***********************************************************************
C     CHAPTER III: DEFINITION OF THE EQUATIONS
C***********************************************************************
      IF(.NOT.PSR) THEN
      IF(NEXCET .GT. 0) GOTO 480      
      IF(TCON.AND.PCON) GOTO 300
      IF(TCON.AND.VCON) GOTO 350
      IF(.NOT.TCON.AND.PCON) GOTO 400
      IF(.NOT.TCON.AND.VCON) GOTO 450
      ELSE
        GOTO 470
      ENDIF
      WRITE(NFILE6,9211) 
 9211 FORMAT(1X,' This option is not yet available in HORDES')
      GOTO 9999
C***********************************************************************
C     III.1: T AND P GIVEN
C***********************************************************************
  300 CONTINUE
      RR= PPR/PL - SUMRAT/CL - TPR/TL
      DO 310 I=1,NSPEC
      F(I)=RATL(I)+CIL(I)*RR
  310 CONTINUE
      GOTO 490
C***********************************************************************
C     III.2: T AN V GIVEN
C***********************************************************************
  350 CONTINUE
      DO 360 I=1,NSPEC
      F(I)=RATL(I)-CIL(I) * VPR/VL
  360 CONTINUE
      GOTO 490
C***********************************************************************
C     III.3: P GIVEN
C***********************************************************************
  400 CONTINUE
      RR=  (ONE-RGAS/CPL)*PPR/PL - SUMRAT/CL + SUMH/(TL*CL*CPL)
      DO 410 I=1,NSPEC
      F(I)=RATL(I)+CIL(I)*RR
  410 CONTINUE
      F(NEQ)= (PPR-SUMH)/(CL*CPL)
      GOTO 490
C***********************************************************************
C     III.4: V GIVEN
C***********************************************************************
  450 CONTINUE
      DO 460 I=1,NSPEC
      F(I)=RATL(I)-CIL(I)*VPR/VL
  460 CONTINUE
      HELP1= (RGAS*TL*SUMRAT-SUMH)/CL
      HELP2= RGAS*TL*VPR/VL
      F(NEQ)= (HELP1-HELP2) / (CPL-RGAS)
      GOTO 490
C***********************************************************************
C     III.4: PSR
C***********************************************************************
  470 CONTINUE
      IF(.NOT.TCON) THEN  
      HELP1= -SUMH +  DNDTIN/VL * SUHPSR
      TPR = HELP1 / (CPL *  CL)
      F(NEQ)= TPR
      ENDIF
      RR=   - SUMRAT/CL - TPR/TL  
      F(1:NSPEC)=
     1     RATL(1:NSPEC)
     1   + CIL(1:NSPEC)*RR 
     1   + DNDTIN / VL * (CIMIX(1:NSPEC)/CMIX-CIL(1:NSPEC)/CL)
      GOTO 490
C***********************************************************************
C     III.5: MODEL OF AN AUTOIGNITION CENTER AND ITS DYNAMIC BEHAVIOUR
C***********************************************************************
  480 CONTINUE
      CALL EXCINI(NEQ,NSPEC,TIME,NFILE6,V0,P0,CIL,CL,CPL,SUMH,
     1            SUMRAT,VL,TL,RATL,F)  
      GOTO 490
C***********************************************************************
C     REGULAR EXIT
C***********************************************************************
  490 CONTINUE
      RETURN
C***********************************************************************
C     CHAPTER IV: ABNORMAL END
C***********************************************************************
 9999 CONTINUE
 2221 FORMAT('0',2X)
 2222 WRITE(NFILE6,2221)
 2223 FORMAT('0',32('+'),' ABNORMAL END ',32('+'))
      WRITE(NFILE6,2223)
      WRITE(NFILE6,2223)
      WRITE(NFILE6,2223)
      WRITE(NFILE6,2221)
      WRITE(NFILE6,2221)
      STOP
C***********************************************************************
C     END OF -DESYS-
C***********************************************************************
      END
      SUBROUTINE HORWRE(NSPEC,CT,C,T,NFILE6,RATE)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *    PROGRAM FOR THE CALC. OF RATES OF CHEMICAL FORMATION   *
C     *               IN COMPLEX REACTIONS                        *
C     *                       26.09.1985                          *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C     NSPEC         = NUMBER OF SPECIES
C     CT            = OVER ALL CONCENTRATION
C     C(1)          = SPECIES CONCENTRATIONS, MOL/M**3 (NSPEC+NM)
C     T             = TEMPERATURE, K
C     RATE(1)       = RATE OF FORMATION
C     NFILE6        = NUMBER OF OUTPUT FILE FOR LISTING
C
C     OUTPUT :
C
C     RATE(1)       = MOLAR SCALE RATES OF FORMATION
C                     (RATE OF FORM. IN COMPLEX REACTIONS IS ADDED TO
C                     INPUT VALUE OF RATE(1)
C
C     INPUT FROM COMMON BLOCK:
C
C     STOICHIOMETRIC COEFFICIENTS OF COMPLEX REACTIONS
C
C***********************************************************************
C
C
C***********************************************************************
C     CHAPTER I.: STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      DIMENSION C(*),RATE(*),G(MNCRE)
      COMMON/BCRE/STOBR(MNCRE,6),NUMS(MNCRE,6),ORD(MNCRE,6),
     1            ABRU(MNCRE),EBRU(MNCRE),TBRU(MNCRE),NBRUT
      DATA RGAS/8.3143D0/
C***********************************************************************
C     CHAPTER II.: INITIAL CHECKS
C***********************************************************************
      IF(NBRUT.EQ.0)      GOTO 100
      IF(NBRUT.GT.MNCRE)  GOTO 910
C***********************************************************************
C     CHAPTER III.: CALCULATION OF REACTION RATES
C***********************************************************************
      DO 50 L=1,NBRUT
C***********************************************************************
C     CHAPTER III.1.: CALCULATION OF THE RATE COEFFICIENTS
C***********************************************************************
      HELP=-EBRU(L)/(T*RGAS)
      G(L)=ABRU(L)* T**TBRU(L) * DEXP(HELP)
      DO 10 KK=1,3
      NNN=NUMS(L,KK)
      MMM=ORD(L,KK)
      IF(NNN.EQ.0.OR.MMM.EQ.0) GOTO 10
      G(L)=G(L)*C(NNN)**MMM
  10  CONTINUE
C***********************************************************************
C     CHAPTER III.2.: CALCULATION OF THE RATES OF FORMATION
C***********************************************************************
      DO 20 KK=1,3
      NNN=NUMS(L,KK)
      IF(NNN.EQ.0) GOTO 20
      RATE(NNN)=RATE(NNN)-G(L)*STOBR(L,KK)
  20  CONTINUE
      DO 30 KK=4,6
      NNN=NUMS(L,KK)
      IF(NNN.EQ.0) GOTO 30
      RATE(NNN)=RATE(NNN)+G(L)*STOBR(L,KK)
  30  CONTINUE
  50  CONTINUE
C***********************************************************************
C     CHAPTER IV.: REGULAR EXIT
C***********************************************************************
  100 CONTINUE
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
  910 CONTINUE
  911 FORMAT('1',5X,'ERROR - TOO MANY COMPLEX REACTIONS: ',
     F       /,6X,'NBRUT =',I3,'  >  MNCRE =',I3,'  IN HORCRE')
      WRITE(NFILE6,911) NBRUT,MNCRE
      GOTO 999
C
  999 CONTINUE
      WRITE(NFILE6,998)
      WRITE(NFILE6,998)
      WRITE(NFILE6,998)
  998 FORMAT(' ','++++++      ERROR IN -HORWRE-      ++++++')
      STOP
C***********************************************************************
C     END OF -HORWRE-
C***********************************************************************
      END
      SUBROUTINE HORVAR(NFILE6,NF,TIME,PX,PPR)
C**********************************************************************
C                                                                     *
C    **************************************************************   *
C    *                                                            *   *
C    *   COMPUTATION OF TEMPERATURE, PRESSURE AND VOLUME PROFILES *   *
C    *                                                            *   *
C    **************************************************************   *
C                                                                     *
C**********************************************************************
C                                                                     *
C                                                                     *
C**********************************************************************
C     I: STORAGE ORGANISATION                                         *
C**********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      COMMON/BPOI/POI(3,MNPOI),TIM(3,MNPOI),NPO(3)
C**********************************************************************
C     II.: CALCULATION FOR LINEAR INTERPOLATION                       *
C**********************************************************************
      NP=NPO(NF)
      IF(NP.EQ.1.OR.TIME.LE.TIM(NF,1)) GOTO 10
      IF(TIME.GE.TIM(NF,NP)) GOTO 20
      GOTO 30
  10  PX=POI(NF,1)
      PPR=0.0D0
      GOTO 100
  20  PX=POI(NF,NP)
      PPR=0.0D0
      GOTO 100
  30  CONTINUE
      DO 35 J=2,NP
      IF(TIME.GE.TIM(NF,J)) GOTO 35
      PPR= ( POI(NF,J)-POI(NF,J-1) ) /
     1             ( TIM(NF,J)-TIM(NF,J-1) )
      PX=POI(NF,J-1)+PPR*(TIME-TIM(NF,J-1))
      GOTO 100
  35  CONTINUE
C
C     YOU SHOULD NEVER ARRIVE HERE
C
      WRITE(NFILE6,9101)
 9101 FORMAT(' ','+++++++++  ERROR IN HORVAR  +++++++++++')
      STOP
C
 100  CONTINUE
      RETURN
C***********************************************************************
C     END OF PVAR                                                      *
C***********************************************************************
      END
      SUBROUTINE HOREUL(NEQ,TIME,S,F,RPAR,IPAR)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *            INTERFACE FOR CONSERVATION EQUATIONS           *
C     *              (ADAPTED FOR SUBROUTINE -EULEX-)             *
C     *                                                           *
C     *               U.MAAS                                      *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C
C***********************************************************************
C***********************************************************************
C     CHAPTERI: STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION S(*),F(*),RPAR(*),IPAR(*)
C***********************************************************************
C     CHAPTER II: CALL OF -HORDES-
C***********************************************************************
      CALL HORDES(NEQ,TIME,S,F,RPAR,IPAR)
      RETURN
C***********************************************************************
C     END OF -HORRES-
C***********************************************************************
      END
