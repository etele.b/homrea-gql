module inttra_mod
    use tra_mod
    implicit none
    integer, save :: info, nslo, nfas

    integer, parameter :: lrw = 50000, liw = 50000
    double precision :: rtol(1) = 1.0d-3, atol(1) = 1.0d-10,&
        rtols(1) = 1.0d-2, atols(1) = 1.0d-9, h = 1.d-10, hmax = 1.d+10
    ! Work arrays
    integer :: iw(liw)
    double precision :: rw(lrw)

    contains
        subroutine inttra(jobin, nci, p, t, qin, qiin, ierr)
            !external trarig, tralef, dumjac
            integer, intent(in) :: jobin, nci 
            type(tra), intent(out) :: t
            double precision, intent(inout) :: p(NEQ)
            double precision, intent(in) :: qin(NEQ, NEQ), &
                qiin(NEQ, NEQ)
            integer, intent(out) :: ierr

            ! Local  variables
            integer :: ir, irmax, nzv, nzc, ijob(20), icall
            double precision :: time, smf(neq), smfp(neq)
            double precision, allocatable :: stem(:, :)

            ! Set required parameters
           
            irmax = 100
            ierr = 0
            info  = jobin
            icall = 0
            ijob(1:20) = 0
            nfas = NEQ - nci
            nslo = nci
            nzv = 0
            if (info == 1) then
                nzc = NEQ*nslo
            else if (info == 2) then
                nzc = NEQ
            end if

            ijob(7) = 5
            ijob(1) = 1
            allocate(stem(NEQ + 1, irmax))
            qb = qin
            qbi = qiin

            time = 0.0d0
            ir = 0
            smf = p
            ! Start integration
            do while ((time < I_tend) .and. (ir < irmax))
                call xlimex(NEQ, nzc, nzv, tralef, trarig, dumjac, &
                    time, smf, smfp, I_tend, &
                    rtol, atol, rtols(1), atols(1), &
                    hmax, h, ijob, lrw, rw, liw, iw, icall, rpar, ipar,&
                    IO_intouti)
                if (ijob(7) < 0) goto 101
                ir = ir + 1
                stem(1, ir) = time
                stem(2:NEQ+1, ir) = smf
                icall = 2
                ijob(7) = 0
            end do

            ! Normal exit
            allocate(t%d(NEQ,IR),t%time(ir))
            t%nvert = ir
            t%time = stem(1, 1:IR)
            t%d = stem(2:NEQ+1,1:IR)
            p = stem(2:NEQ+1, IR)
            deallocate(stem, qb, qbi)
            return

            ! Error exists
        101 continue
            ierr = 1
            write(IO_errf,*) 'Error in intslo: XLIMEX finished with ', ijob(7)
            return
        end subroutine inttra

        subroutine tralef(nzc, b, ir, ic)
        integer :: nzc, ir(*), ic(*)
        double precision :: b(*)

        integer :: i, j, cnt

        if (info == 1) then
            cnt = 0
            do i=1, nslo
                do j = 1, NEQ
                    cnt = cnt + 1
                    b(cnt) = qb(i, j)
                    ir(cnt) = i
                    ic(cnt) = j
                end do
            end do
        else if (info == 2) then
            do i=1, nzc
                ir(i) = i
                ic(i) = i
            end do
            b(1:nzc) = 1.0d0
        end if

        return

        end subroutine tralef

        subroutine trarig(neqin, nzv, time, y, f, bv, ir, ic, rpar, ipar, ifc, ifp)
        integer :: neqin, nzv, ifc, ir(*), ic(*), ipar(*), ifp(*)
        double precision :: time, y(neq), f(neq), help(neq), bv(*), rpar(*)

        ! Local varibles
        integer :: ierr

        call trares(1, y, f = f, ierr = ierr)
        if (ierr > 0) goto 901

        if (info == 1) then
            f = matmul(qb(1:NEQ, 1:NEQ), f(1:NEQ))
        elseif (info == 2) then
            help(1:nfas) = matmul(qb(NEQ-nfas+1:NEQ, 1:NEQ), f(1:NEQ))
            f(1:NEQ) = matmul(qbi(1:NEQ, NEQ-nfas+1:NEQ), help(1:nfas))
        end if
        

        return
        ! Error exists
    901 continue
        write(IO_errf,*) 'Error in slorig: Masres exited with an error: ', ierr
        return
        end subroutine trarig

        subroutine trares(job, y, f, jac, ssp, g, stoma, rever, mat, ierr)
            integer, intent(in) :: job
            double precision, intent(in) :: y(NEQ)
            double precision, optional, intent(out) :: f(NEQ), jac(NEQ, NEQ)
            integer, intent(out) :: ierr
            integer, optional, intent(out) :: stoma(NSPEC, NREAC), &
                rever(NREAC), mat(6, NREAC)
            double precision, optional :: ssp(5), g(NREAC)


            integer :: i, iex, iprim, lrw1
            double precision :: tl, cp, cl, rho, hspec, pl, s, mms,& 
                stomcom, ss, dxds(1), dtds(1), dpds(1), &
                cil(MNSPE+MNTHB+1), work1(MNREA), fac, xmolm
            double precision, dimension(NSPEC) :: dodt, si, hi, dsdpsi, fn, &
                yn, help1, help2, rat
            double precision, dimension(NREAC) :: dgdt, gr
            double precision :: dgdc(NREAC, NSPEC), dodc(NSPEC, NSPEC)

            iex = 0

            call trassp(IEQ, NEQ, y, NSPEC, tl, cl, cil, rho, hspec, pl, SSPV,&
                iex, 0, dxds, 1, dtds, dpds)
            xmolm = rho/cl

            if (job == 1) iex = 0
            if (job == 2) iex = 1
            lrw1 = MNREA
            ierr = 0
            iprim = 0
            call mechja(iex, NSPEC, NREAC, NM, NRINS, NRTAB, IREAC(NMATLL), &
                IREAC(NNRVEC), rREAC(NPK), RREAC(NTEXP), RREAC(NEXA), &
                RREAC(NVT4), RREAC(NVT5), RREAC(NVT6), IREAC(NIARRH), &
                RREAC(NXZSTO), RREAC(NRATCO), &
                cl, cil, tl, rat, NRSENS, ISENSI(IRSENS),&
                RPAR(IRSENS), lrw1, work1, iprim, IO_intouti, ierr, &
                RREAC(NAINF), RREAC(NBINF), RREAC(NEINF), RREAC(NT3), &
                RREAC(NT1), RREAC(NT2), RREAC(NAFA), RREAC(NBFA), hlhh, hinf, &
                NSPEC, NREAC, dgdc, dgdt, dodc, dodt)
            f(3:2+NSPEC) = rat(1:NSPEC)/rho
            f(1:2) = 0.d0

            ! Check if matrix needs to be calculated
            if (job == 2 .or. job == 3) then
                dodt = dodt + rat / tl
                fac = -1/rho

                call dger(NSPEC, NSPEC, fac, rat, 1, SSPV(1), 1, dodc, &
                    NSPEC)
                
                hi = sspv(18*NSPEC+1:19*NSPEC)
                cp = dot_product(sspv(19*NSPEC+1:20*NSPEC), y(3:NEQ))
                jac(1:2, 1:NEQ) = 0.d0
                jac(3:NEQ,3:NEQ) = dodc

                help1(1:nspec) = matmul(dodc, y(3:NEQ))
                help2(1:nspec) = hi/(tl*cp) - xmolm

                call dger(NSPEC, NSPEC, 1.d0, HELP1, 1, HELP2, 1,jac(3,3), NEQ)

                fac = -1.d0/(rho*cp)
                jac(3:NEQ, 3:NEQ) = jac(3:NEQ, 3:NEQ) + &
                    fac*matmul(reshape(dodt,(/ NSPEC, 1/)), &
                    reshape(hi, (/1, NSPEC/)))

                jac(3:NEQ, 1) = - matmul(dodc, y(3:NEQ)) / (TL*cp)
                jac(3:NEQ, 1) = jac(3:NEQ, 1) + dodt / (rho * cp)
                jac(3:NEQ, 2) = help1/pl
            else if (job == 3) then
                g(1:NREAC) = work1(1:NREAC)/RHO
                ! Calculate entropy
                ss = 0.d0
                mms = sum(y(3:NEQ))
                do i = 1, NSPEC
                    call calhsi(NSPEC, i, tl, hlhh, hinf, hi(i), si(i), IO_intouti)
                    if (y(i+2) > 1d-99) then
                        si(i) = si(i) -RGAS*log(y(i+2)*mms)
                        dsdpsi(i) = si(i) - RGAS
                        ss = ss + si(i)*y(i+2)
                    end if
                end do
                ssp(1) = ss
                ssp(2) = tl
                ssp(3) = rho
                ssp(4) = mms
            end if
            return
        end subroutine trares



        subroutine dumjac
            stop
        end subroutine dumjac
end module inttra_mod
