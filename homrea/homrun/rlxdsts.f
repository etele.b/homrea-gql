       SUBROUTINE RLXDSTS(NEQ,NVERT,Y)
C***********************************************************************
C Subroutine for reading relaxed states by homrea calculation 
C with initial data from REDIM table
C Author: Alexander Neagos
C***********************************************************************
       IMPLICIT NONE
C***********************************************************************
C
C***********************************************************************
      INTEGER I,J,IDATA1,IDATA2,DUMVERT,NEQ,NVERT,NSPEC,MAXLINE,IDUM,ST
      INTEGER, ALLOCATABLE, DIMENSION(:) :: DATNR
      INTEGER, ALLOCATABLE, DIMENSION(:) :: VERTNR
C***********************************************************************
      DOUBLE PRECISION, DIMENSION(NEQ,NVERT) :: Y
C***********************************************************************
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:,:) :: PSI
C***********************************************************************
      CHARACTER*4 STRING
      CHARACTER*8 DUMCHAR
C***********************************************************************
C Number of species
C***********************************************************************
      NSPEC=NEQ-2
C***********************************************************************
C Maximum allowed number of lines in read files
C***********************************************************************
      MAXLINE=1000000
C***********************************************************************
C Open homrea file with relaxed states
C***********************************************************************
      OPEN(UNIT=20,FILE='relaxed_states',STATUS='OLD',ACTION='READ')
C***********************************************************************
C Count number of datasets for allocation
C***********************************************************************
      IDATA1=0
      DO I=1,MAXLINE
        READ(20,801,END=101,ERR=901)STRING
        IF (STRING .EQ. 'DATA') IDATA1=IDATA1+1
      END DO
  101 CONTINUE
C***********************************************************************
C Allocate memory for arrays
C***********************************************************************
      ALLOCATE(PSI(NSPEC,IDATA1))
      ALLOCATE(DATNR(IDATA1))
      ALLOCATE(VERTNR(IDATA1))
C***********************************************************************
C Rewind homrea output file and read both number of data sets DATNR and 
C result vector PSI
C***********************************************************************
      REWIND(20)
      DO I=1,IDATA1
        READ(20,802,END=901,ERR=901)DATNR(I),(PSI(J,I),J=1,NSPEC)
      END DO
      CLOSE(20)
C***********************************************************************
C Open file with initial values for homrea calculation. Also 
C includes number of REDIM vertices belonging to initial states
C***********************************************************************
      OPEN(UNIT=21,FILE='initial_homrea',STATUS='OLD',ACTION='READ')
C***********************************************************************
      REWIND(21)
      IDATA2=0
      IDUM=1
C***********************************************************************
C Write vertice number in array VERTNR, if number read dataset 
C corresponds to dataset number of computed relaxed states 
C***********************************************************************
      DO I=1,MAXLINE
        READ(21,803,END=102,IOSTAT=ST)DUMCHAR,DUMVERT
        IF (DUMCHAR.EQ.'****VERT') THEN
          IDATA2=IDATA2+1
          IF (IDATA2.EQ.DATNR(IDUM)) THEN
            VERTNR(IDUM)=DUMVERT
            IF (IDUM.LT.IDATA1)IDUM=IDUM+1
          END IF
        END IF
      END DO
  102 CONTINUE
      CLOSE(21)
C***********************************************************************
C Replace states on REDIM by relaxed states from homrea calculation
C*********************************************************************** 
      DO I=1,IDATA1
        Y(3:NEQ,VERTNR(I))=PSI(1:NSPEC,I)
      END DO
      RETURN
C***********************************************************************
C FORMAT STATEMENTS
C*********************************************************************** 
  801 FORMAT(A4)
  802 FORMAT(4X,I4,/,6(1PE20.13))
  803 FORMAT(A,I4)
C***********************************************************************
C ERROR EXITS
C***********************************************************************
 901  CONTINUE
      WRITE(*,902)
 902  FORMAT('ERROR DURING READ IN RLXDSTS!')
      STOP
C***********************************************************************
C
C***********************************************************************
       END SUBROUTINE RLXDSTS
