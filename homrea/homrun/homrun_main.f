      PROGRAM HOMRUN
C***********************************************************************
C
C     .-----------------------------------------------------------.
C     |                                                           |
C     |     PROGRAM (INTEGRATION PART)  FOR THE SIMULATION OF     |
C     | CHEMICAL REACTION WITH VARIABLE TEMPERATURE AND PRESSURE  |
C     |                                                           |
C     '-----------------------------------------------------------'
C
C***********************************************************************
C
C     INPUT  : DATA FROM FROM UNIT NFILE3
C     OUTPUT : DATA ON UNIT NFILE6
C
C***********************************************************************
C                                                                      C
C                                                                      C
C     DESCRIPTION OF PARAMETERS                                        C
C     =========================                                        C
C                                                                      C
C  1. NUMBERS:                                                         C
C     --------                                                         C
C                                                                      C
C NSPEC :   NUMBER OF SPECIES     (NSPEC .LE. MNSPE)                   C
C NEQ   :   NUMBER OF ORDINARY DIFFERENTIAL EQUATIONS, NEQ=NSPEC+1     C
C           (ENERGY CONSERVATION)                                      C
C NREAC :   NUMBER OF ELEMENTARY REACTIONS (NREAC .LE. MNREA)
C NSENS :   NUMBER OF REACTIONS VARIED IN SENSITIVITY TEST (.LE. MNREA)C
C NSC   :   NUMBER OF SPECIES CONSIDERED                               C
C                                                                      C
C                                                                      C
C  2. SOLUTION:                                                        C
C     ---------                                                        C
C                                                                      C
C S (NEQ,1)         : SOLUTION AT TIME 'TIME'    (SI UNITS)            C
C S (NEQ,2..NSENS+1): LOCAL SENSITIVITY COEFFICIENTS                   C
C SP(NEQ,1)         : DERIVATIVES AT TIME 'TIME' (SI UNITS)            C
C F (NEQ)           : RESIDUAL OF THE DIFFERENTIAL/ALGEBR. SYSTEM      C
C SG(NEQ,2..NREAC+1): GLOBAL SENSITIVITY COEFFICIENTS                  C
C TIME              : CURRENT VALUE OF TIME (S)                        C
C                                                                      C
C VALUE(I)  : I=ACCORDING TO KEY         (I=1,...,NEQ)                 C
C                                                                      C
C             I=1       CONCENTRATION OF FIRST SPECIES                 C
C             I=NSPEC    "      "     "  LAST     "                    C
C             I=2       TEMPERATURE         (K)                        C
C                                                                      C
C                                                                      C
C  3. INPUT PARAMETERS:                                                C
C     -----------------                                                C
C                                                                      C
C XU(NSPEC)   : INITIAL VALUES OF MOLE FRACTIONS                       C
C                                                                      C
C                                                                      C
C  4. INTEGRATION CONTROL PARAMETERS                                   C
C     ------------------------------                                   C
C                                                                      C
C RTOL        : RELATIVE TOLERANCE                                     C
C ATOL        : ABSOLUTE TOLERANCE                                     C
C STEP        : INITIAL STEPSIZE                                       C
C                                                                      C
C                                                                      C
C  5. OUTPUT CONTROL PARAMETERS                                        C
C     -------------------------                                        C
C                                                                      C
C TBEG   : TIME OF FIRST OUTPUT                                        C
C TEND   : END TIME OF INTEGRATION                                     C
C TSTEPS : NUMBER OF OUTPUT POINTS                                     C
C                                                                      C
C                                                                      C
C  6. VALUES REQUIRED BY DASSL:                                        C
C     -------------------------                                        C
C                                                                      C
C ( SEE DESCRIPTION OF DASSL )                                         C
C INFO(15)      : INFORMATION ON DETAILS OF INTEGRATION                C
C SWORK(MNEQU)  : WORK ARRAY FOR SENSITIVITY TEST
C ISWORK(2)     :  "     "    "      "        "                        C
C RPAR(5001)    : ARRAY FOR COMMUNICATION WITH SUBROUT. DASSL
C IPAR(*)       :   "    "       "         "       "        "
C LIWORK        : LENGTH OF INTEGER WORK ARRAY FOR SUBROUT. DASSL      C
C LRWORK        :   "    "  REAL     "     "    "     "       "        C
C LISWOR        : LENGTH OF INTEGER WORK ARRAY FOR SENSITIVITY TEST    C
C LSWORK        :   "    "  REAL     "     "    "  SENSITIVITY TEST    C
C RTOL          : RELATIVE ERROR TOLERANCE                             C
C ATOL          : ABSOLUTE   "      "                                  C
C                                                                      C
C                                                                      C
C  7. UNITS:                                                           C
C     ------                                                           C
C                                                                      C
C UNITS :  SI UNITS                                                    C
C RGAS  :  GAS CONSTANT (8.3143 J/MOL*K)                               C
C                                                                      C
C**********************************************************************C
C**********************************************************************C
C                                                                      C
C     A: STORAGE ORGANIZATION                                          C
C                                                                      C
C**********************************************************************C
C**********************************************************************C
C     I: TYPES OF VARIABLES
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      LOGICAL DETONA,PCON,VCON,TEST,STOP,TCON,FINISH,TSO
      INTEGER NWALL,NCOMPLX,MULTIZ
      LOGICAL MOLEFR,LPLO,MASSFR
C**********************************************************************C
C     Fixed arrays
C**********************************************************************C
      COMMON/BINFO/INFORM(20)
      COMMON/BICMAT/COMAT(MNCOV*MNEQU),EMAT((MNSPE+2)*MNCON)
      DIMENSION NOPT(60),CINT(10)
      COMMON/BDIM/NSMAX,NRMAX,NCRMAX,NPOMAX,NELMAX
      COMMON/BOPTL/PCON,VCON,TCON,DETONA,NWALL,NCOMPLX,NPSR
      COMMON/SPOPT1/NEXCET,NENGIN
      COMMON/BTIME/TBEG,TEND,TSTEPS,VATOL,VRTOL,VATOLS,VRTOLS,STEP
C---- CONTROL PARAMETERS FOR SOLVERS
      COMMON/LIMLOC/
     1      JM,KM,NDV,NDC,NUMP,NDP,NGES,NDI,NDJ,III,
     1      I1 ,I2 ,I3 ,I4 ,I5 ,I6 ,I7 ,I8 ,I9 ,I10,
     1      I11,I12,I13,I14,I15,I16,I17,I18,I19,I20,
     1      I21,I22,I23,I24,I25,I26,I27,I28,I29,I30,
     1      N1 ,N2 ,N3 ,N4 ,N5 ,N6 ,N7 ,N8 ,N9 ,N10,
     1      N11,N12,N13,N14,N15,N16,N17,N18,N19,N20,
     1      N21,N22,N23,N24,N25,N26,N27,N28,N29,N30,
     1      IMAT(10)
C**********************************************************************C
C     II: ARRAYS dimensioned by parameter statements
C**********************************************************************C
C_ASSA      DIMENSION F(MNEQU),SMF(MNEQU),SMFP(MNEQU)
      DIMENSION F(MNEQU*MNZONE),SMF(MNEQU),SMFP(MNEQU),SMF0(MNEQU)
      CHARACTER(LEN=LSYMB) NSYMB(MNSPE+MNTHB+1+2)
C_ASSA changed for UM      CHARACTER*8 NSYMB(MNEQU+MNTHB+2)
      CHARACTER(LEN=LSYMB)  SYMPRP(MNPROP)
      COMMON/BSSPV/SSPV(20*MNSPE)
      COMMON/BMIX/OMEGA,SMFMIX(MNEQU),SMFCHA(MNEQU),VDIR(MNEQU,NDIMM)
      COMMON/BINIT/P0,T0,V0,H0,XU(MNSPE)
C     COMMON/BHCP/HL(7,MNSPE),HH(7,MNSPE),HINF(3,MNSPE)
      COMMON/BHCP/HLHH(7,MNSPE*2),HINF(3,MNSPE)
      COMMON/BOUT/KONINT,KONTAB,KONPLO,KONSEN,KONMEC,KONANA,KONRED,
     1  KONSPE(MNSPE),KONSID(MNSPE),NSC,
     1  ICRIT,KOGSEN,KONRST,MOLEFR,MASSFR,LPLO
      COMMON/BREDUC/NRPE,NSQS,NLEF,IRPE,ISQS,ILEF,IRED(MNSPE)
      COMMON/BCHIS/EWF(MNCON,7),SMFF(MNEQU,7),PAT(2,MNTCO),
     1       MIXFRA,MENFRA,MPRFRA
      COMMON/BECVA/ECV(2,MNCOV)
      COMMON/BCVB/CVBACK(MNCON*MNCON)
      COMMON/BSENS/LSMAX,NSENS,NRSENS,NISENS,NPSENS,
     1       IRSENS,IISENS,IPSENS,ISENSI(MNSEN)
      DIMENSION S(MNEQU*(MNSEN+1)),SP(MNEQU*(MNSEN+1)),
     1          SENMAX(MNEQU*(MNSEN+1)),SENMIN(MNEQU*(MNSEN+1))
C_ASSA      DIMENSION SMIN(MNEQU),SMAX(MNEQU)
      DIMENSION SMIN(MNEQU*MNZONE),SMAX(MNEQU*MNZONE)
C---- decomment if trapro is called:1
C     DIMENSION DSS(MNSPE*MNSPE),DSE(MNSPE),DSP(MNSPE),DES(MNSPE)
      COMMON/BEXJAC/EXAJAC((MNEQU+1)*(MNEQU+1)),ISUPJA
      COMMON/BPOI/POI(3,MNPOI),TIM(3,MNPOI),NPO(3)
C***********************************************************************
C     BLOCK FOR MECHANISM ANALYSIS
C***********************************************************************
      COMMON/BRATES/TIMRAT,RATTEM(MNREA),RATINT(MNREA)
      COMMON/BICRES/ICRES
C***********************************************************************
C     BLOCK FOR COMPLEX REACTIONS
C***********************************************************************
      COMMON/BCRE/STOBR(MNCRE,6),NUMS(MNCRE,6),ORD(MNCRE,6),
     1            ABRU(MNCRE),EBRU(MNCRE),TBRU(MNCRE),NBRUT
C***********************************************************************
c     WORK ARRAYS
C***********************************************************************
      PARAMETER (LRWORK=80200600, LIWORK=8791750, LSWORK=MNEQU,
     1     LISWOR=2, LRP=MNRPAR)
      DIMENSION RWORK(LRWORK),IWORK(LIWORK)
      DIMENSION SWORK(LSWORK),ISWORK(LISWOR)
C---- OWN INTEGER WORK ARRAY
      DIMENSION  MIWORK(MNSPE*MNREA)
      COMMON/BPARM/RPAR(MNRPAR),IPAR(MNIPAR)
      DIMENSION RV(15)
      DOUBLE PRECISION, DIMENSION(1) :: DXDS,DTDS,DPDS
      DATA LRV/15/
C**********************************************************************C
C     IV: DATA STATEMENTS                                              C
C**********************************************************************C
C***********************************************************************
C     assign units
C***********************************************************************
      DATA NFILE6/06/,NFILE3/03/,NFILE8/08/,NFIL10/10/,NFIL18/18/,
     1     NFIL21/21/
      DATA ZERO/0.0D0/
      MLIW=MNSPE*MNREA
C**********************************************************************C
C     B: Initialization
C***********************************************************************
      OMEGA = ZERO
      SP    = ZERO
C**********************************************************************C
C
C**********************************************************************C
      STOP=.TRUE.
C---- AVOID UNDERFLOWS
      CALL NOUFLO
      CALL TIMER(0,DUMMY)
C***********************************************************************
C     OUTPUT OF HEADING ON MSGFIL
C***********************************************************************
CBZ version number included
      WRITE(NFILE6,8100) VERSION_NUM,VERSION_DATE,
     1                   COMLIB_NUM,COMLIB_DATE
 8100 FORMAT(1X,/,1X,79('*'),/,
     1       1X,4('*'),4X,'PROGRAM HOMREA - HOMRUN',/,
     1       1X,4('*'),4X,'BUILD VERSION: ',A,/,
     1       1X,4('*'),4X,'BUILD DATE: ',A,/,
     1       1X,4('*'),4X,'LIBRARY VERSION: ',A,/,
     1       1X,4('*'),4X,'LIBRARY DATE: ',A,/,
     1       1X,79('*'))
CBZ end
C***********************************************************************
C
C     III: GET INPUT DATA
C
C***********************************************************************
      CALL HORIN1(NSPEC,NSYMB(3),NFILE3,NFIL10,NFILE6,IPAR,TSO,STOP,
     1     LIWORK,IWORK,LRWORK,RWORK,NOPT,LRP,RPAR,MULTIZ)
      CALL SPAKOE(NSPEC,NREAC,IREAC(NMATLL),IREAC(NMATLR),MIWORK,NFILE6)
C**********************************************************************C
C     III.1 INITIALIZE LOGICAL VARIABLES
C**********************************************************************C
                      TEST   = .FALSE.
      IF(KONINT.GE.4) TEST   = .TRUE.
C**********************************************************************
C     III.2: SET UP LOCATIONS
C**********************************************************************
      NEQ     = IPAR(1)
      NHPW    = IPAR(2)
      NSPEC   = IPAR(3)
      NSENS   = IPAR(4)
      ND2     = IPAR(5)
      NE      = IPAR(6)
      IEQ     = IPAR(10)
      NC      = IPAR(11)
C**********************************************************************
C     III.3: Check arrays
C**********************************************************************
      NEEDA=(NSENS+1)*NEQ
      IF(NSENS.GT.MNSEN) GOTO 920
      IF(NEEDA.GT.(MNSEN+1)*MNEQU)   GOTO 910
      IF(NSPEC.GT.MNSPE) GOTO 940
      NRED  = NHPW   -NE - 2
C***********************************************************************
C***********************************************************************
C
C     IV: LOOP FOR CALCULATIONS
C
C**********************************************************************
C**********************************************************************
      NGET = 1
      NCA=0
C***********************************************************************
C A. Neagos:
C Jump to manifold calculation if OPTION ILDM is 1
C***********************************************************************
      IF(NOPT(24).GE.1) GOTO 8001
 1000 CONTINUE
      NCA=NCA+1
      ICRES=0
      TIME=ZERO
      CALL OPNFIL(0,NFILE8,'fort.8',IERR)
      REWIND NFILE8
C***********************************************************************
C     IV.I: GET INPUT DATA FROM SUBROUTINE HORIN2
C***********************************************************************
      CALL HORIN2(NE,NSPEC,NSYMB(3),NFILE3,NFILE6,NOPT,STOP,
     1    FINISH,TIME,NEQ,S,CINT,MULTIZ)
C***********************************************************************
C     IV.4: Check whether data have been received
C***********************************************************************
      IF(FINISH) THEN
       WRITE(NFILE6,*)' '
       IF(     TSO) WRITE(NFILE6, 829)
       IF(.NOT.TSO) WRITE(NFILE6,1829)
       WRITE(NFILE6,1830)'CONGRATULATION  -  SIMULATION WAS SUCCESSFUL '
       IF(     TSO) WRITE(NFILE6, 829)
       IF(.NOT.TSO) WRITE(NFILE6,1829)
!        WRITE(NFILE6,8211)
!  8211 FORMAT(1X,' no data received from file',I2)
        GOTO 8000
      ENDIF
C**********************************************************************
C     CALCULATE THE STATE SPACE IN (H,P,WI/MI) SPACE
C***********************************************************************
      IF(.NOT.TCON) STE=S(NEQ)
      IF(TCON) CALL HORVAR(NFILE6,3,TIME,STE,TPR)
      CALL TRASSP(-IEQ,NHPW,SMF0,NSPEC,STE,C,S(1),DDU,HDU,PDU,SSPV,
     1        IEX,0,DXDS,1,DTDS,DPDS)
C***********************************************************************
C     I.: GET INPUT DATA FROM SUBROUTINE HORIN2
C***********************************************************************
      IF(TSO)      WRITE(NFILE6,827) NCA
      IF(.NOT.TSO) WRITE(NFILE6,837) NCA
C***********************************************************************
C
C     CALCULATE A TRAJECTORY
C
C***********************************************************************
      IINT = INFORM(10)
      KINTE = KONINT
      KFINO = KONPLO
      KLSEN = KONSEN
      KGSEN = KOGSEN
      KMANA = KONMEC
C----
      ISTO = 1
      IF (MULTIZ.GT.0) ISTO = 0
C***********************************************************************
C     Open Storage File 18 as scratch file
C***********************************************************************
      IF (ISTO.GE.1) THEN
        CALL OPNFIL(0,NFIL18,'fort.18',IERR)
        REWIND NFIL18
      ENDIF
CCCC
      IF(MULTIZ.GT.0) THEN
        CALL HORMUC(NE,NSPEC,NSYMB(3),NEQR,MULTIZ,NFILE6,NOPT,TIME,S,
     1              CINT)
        CALL MULZON(NCA,NEQR,MULTIZ,NSPEC,NSYMB(3),TIME,S,SP,F,RPAR,
     1              IPAR,SMIN,SMAX,NTA,TSO,NFILE6,NFILE8,NFIL10,
     1              KINTE,NSC,KONSPE,NSENS,CINT,IINT,LRWORK,
     1              RWORK,LIWORK,IWORK,LSWORK,SWORK,LISWOR,ISWORK)
      ELSE
CCCC
        CALL TRAJE(NCA,ISTO,NEQ,ND2,NSPEC,NREAC,NSYMB(3),TIME,S,SP,F,
     1             RPAR,IPAR,SMIN,SMAX,NTA,TSO,NFILE6,NFILE8,NFIL10,
     1             NFIL18,KINTE,KFINO,KLSEN,KGSEN,KMANA,NSC,KONSPE,
     1             NSENS,NRSENS,NISENS,NPSENS,ICRIT,CINT,IINT,
     1             SENMAX,SENMIN,RATINT,LRWORK,RWORK,LIWORK,IWORK,
     1             LSWORK,SWORK,LISWOR,ISWORK,NOPT(42))
      ENDIF
C***********************************************************************
C     READ RESULTS OF TRAJECTORIES AND PROCESS
C***********************************************************************
 8001 CONTINUE
      IF(ISTO.GE.1) CALL CLSFIL(NFIL18)
      IF(NOPT(13).EQ.0.AND.NOPT(42).EQ.0) GOTO 1000
C***********************************************************************
C
C     D: FINAL OUTPUT
C
C***********************************************************************
C***********************************************************************
C     PERFORM ANALYSIS OF THE EQUILIBRIUM POINT
C***********************************************************************
      IF(KONRED.NE.0.OR.NOPT(42).GE.0) THEN
C**********************************************************************
C     Initialize symbols
C**********************************************************************
        IF(IEQ.EQ.1.OR.IEQ.EQ.2) THEN
        NSYMB(1)=' H(J/KG)'
        ELSE
        NSYMB(1) =' T(K)   '
        ENDIF
        IF(IEQ.EQ.1.OR.IEQ.EQ.5) THEN
        NSYMB(2)='  P(Pa) '
        ELSE
        NSYMB(2)='rho kg,m'
        ENDIF
C**********************************************************************
C     CALCULATE THE STATE SPACE IN (H,P,WI/MI) SPACE
C***********************************************************************
        IF(.NOT.TCON) STE=S(NEQ)
        IF(TCON) CALL HORVAR(NFILE6,3,TIME,STE,TPR)
        CALL TRASSP(-IEQ,NHPW,SMF,NSPEC,STE,C,S(1),DDU,HDU,PDU,SSPV,
     1        IEX,0,DXDS,1,DTDS,DPDS)
        IF (NOPT(13).EQ.0) GOTO 1000
C***********************************************************************
C     SET INITIAL VALUES FOR CONTROLING VARIABLES
C***********************************************************************
      NDIM = NC - NE - 2 + MIXFRA + MENFRA + MPRFRA
      IPAR(12) = NDIM
C***********************************************************************
C     Simplified Manifold Routine
C***********************************************************************
      IF(NOPT(28).NE.0) THEN
      CALL SIMMAN(0,NC,NHPW,COMAT,LRWORK,RWORK,LIWORK,IWORK,
     1   SMF,SMFP,NSYMB,NFILE6,RPAR,IPAR,LRV,RV)
      ELSEIF(nopt(27).EQ.1) THEN
C***********************************************************************
C     Call table setup
C***********************************************************************
          IF(nopt(10).NE.0.OR.nopt(11).NE.0) THEN
            NE    = IPAR(6)
            NZEV = NE + 2
            CALL MANSEN(ndim,nc,nzev,nhpw,SYMPRP,nfile6,rpar,ipar)
             write(*,*) '****MANSEN SUCCESSFULLY FINISHED****'
         ENDIF
      ELSE
      NE    = IPAR(6)
      NDIM  = NC - (NE + 2)  + MIXFRA
         CALL NEWGEN(NCA,NHPW,NC,NDIM,SMF,SMF0,RPAR,IPAR,LRWORK,RWORK,
     1               LIWORK,IWORK,IERR,NSYMB)
      IF(NOPT(24).GE.1)GOTO 8000
      ENDIF
C***********************************************************************
C A. Neagos:
C Call newgen if INMAN=1
C***********************************************************************

C***********************************************************************
      REWIND NFIL21
      ENDIF
C-Sl- Opening unit 8
      IF(NOPT(24).LT.1)THEN
      OPEN(UNIT=NFILE8,FILE='fort.8',POSITION='APPEND')
C-Sl-
      WRITE(NFILE8,877)
      END IF
  877 FORMAT('<')
      IF(     TSO) WRITE(NFILE6, 829)
      IF(.NOT.TSO) WRITE(NFILE6,1829)
C**********************************************************************
C     I: END OF LOOP FOR CALCULATIONS
C**********************************************************************
      IF(     TSO) WRITE(NFILE6, 829)
      IF(.NOT.TSO) WRITE(NFILE6,1829)
C***********************************************************************
C     II: RESTART FOR NEXT DATA SET
C***********************************************************************
      GOTO 1000
C***********************************************************************
C
C     PERFORM ANALYSIS OF THE EQUILIBRIUM POINT
C
C***********************************************************************
C***********************************************************************
C---- OUTPUT FOR MECHANISM ANALYSIS
C***********************************************************************
      WRITE(NFIL21,877)
 8000 CONTINUE
      IF(NOPT(42).GE.0) THEN
          CALL ANALYSIS(NSYMB(1:NEQ), RPAR, IPAR)
      ENDIF
      STOP
C***********************************************************************
C***********************************************************************
C
C     E: ERROR EXITS
C
C***********************************************************************
C***********************************************************************
  910 CONTINUE
      WRITE(NFILE6,911) NEEDA,(MNSEN+1)*MNEQU
  911 FORMAT('1',1X,'ERROR - NEEDED ARRAY FOR INDEP. VAR. (',I5,') > ',
     1              'SUPPLIED ARRAY(',I5,')')
      GOTO 9999
  920 CONTINUE
      WRITE(NFILE6,921) NSENS,MNSEN
  921 FORMAT('1',1X,'ERROR - NEEDED ARRAY FOR RPAR (',I5,') > ',
     1              'SUPPLIED ARRAY(',I5,')')
      GOTO 9999
  940 CONTINUE
      WRITE(NFILE6,941) NSPEC,MNSPE
  941 FORMAT('1',1X,'ERROR - TOO MANY SPECIES: NSPEC(',I3,') > ',
     1              'MNSPE(',I3,')')
      GOTO 9999
 9999 CONTINUE
 9998 FORMAT('0',3(32('+'),' ABNORMAL END ',32('+'),/))
      WRITE(NFILE6,9998)
      STOP
C***********************************************************************
C***********************************************************************
C
C     F: FORMAT STATEMENTS
C
C***********************************************************************
C***********************************************************************
  827 FORMAT('0',/,1X,79('*'),/,
     1      ' ****',20X,'CALCULATION FOR DATA SET NO.',I4,19X,'****',/,
     1      ' ',79('*'),/)
  837 FORMAT('1',132('*'),/,
     1      ' ****',46X,'CALCULATION FOR DATA SET NO.',I4,46X,'****',/,
     1      ' ',132('*'),/)
  829 FORMAT(1X,79('*'))
 1829 FORMAT(1X,132('*'))
 1830 FORMAT(1X,10('*'),7X,A45,7X,10('*'))
C***********************************************************************
C***********************************************************************
C
C     END OF MAIN PROGRAM
C
C***********************************************************************
C***********************************************************************
      END
      SUBROUTINE INTTEG(NEQ,SMF,TBEG,TEND,MSGFIL,IERR,RPAR,IPAR)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      EXTERNAL INTRIG,INTLEF,DUMJAC
C***********************************************************************
C     arrays for solvers
C***********************************************************************
      DIMENSION RTOL(1),ATOL(1),RTOLS(1),ATOLS(1),RPAR(*),IPAR(*)
      DIMENSION IJOB(20)
      DOUBLE PRECISION, DIMENSION(NEQ) :: SMF,SMFP
C***********************************************************************
C     STORAGE ARRAY FOR THE TABLE
C***********************************************************************
      DIMENSION RW(5000),IW(5000)
      COMMON/BSCRS/ERVROW(MNEQU*MNEQU),ERVCOL(MNEQU*MNEQU)
      COMMON/BHEL/QEVB(MNEQU*MNEQU)
C***********************************************************************
C     STORAGE ARRAY FOR TRAJECTORIES
C***********************************************************************
      DATA LRW /5000/,LIW /5000/
      DATA RTOL(1) /1.0D-3/,ATOL(1) /1.0D-8/
      DATA RTOLS(1)/1.0D-2/,ATOLS(1)/1.0D-5/
C***********************************************************************
C     USEFUL CONSTANTS
C***********************************************************************
      DATA ZERO/0.0D0/,ONE/1.0D0/
C***********************************************************************
C     SET INTEGRATION CONTROL PARAMETERS
C***********************************************************************
      ICALL    = 0
      IJOB(1:20) = 0
      NZV = 0
      NZC = NEQ
      IJOB(1)=1
      IJOB(7)=4
      H    = 1.D-7
      HMAX = 1.D+10
      TIME=TBEG
C***********************************************************************
C
C***********************************************************************
      DO WHILE (TIME.LT.TEND)
      CALL XLIMEX(NEQ,NZC,NZV,INTLEF,INTRIG,DUMJAC,
     *      TIME,SMF,SMFP,TEND,RTOL,ATOL,RTOLS(1),ATOLS(1),
     *      HMAX,H,IJOB,LRW,RW,LIW,IW,ICALL,RPAR,IPAR)
C**********************************************************************
C     check error code
C**********************************************************************
      write(*,*) 'kkkkkkk',SMF(1:NEQ)
      IF(IJOB(7).LT.0) THEN
        WRITE(MSGFIL,*) ' IJOB',IJOB(7),' IERR set to 1'
        GOTO 900
      ENDIF
C**********************************************************************
C     check current time
C**********************************************************************
      ICALL = 2
      IJOB(7) = 4
      ENDDO
C**********************************************************************
C     Regular exit
C**********************************************************************
      RETURN
 900  CONTINUE
      IERR = 1
      TEND = TIME
      END
      SUBROUTINE INTRIG (NEQ,NZV,TIME,Y,F,BV,IR,IC,RPAR,IPAR,IFC,IFP)
C***********************************************************************
C
C     DEFINITION OF RIGHT HAND SIDE (SEE -ULIMEX-)
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION Y(NEQ),F(NEQ),BV(*),IR(*),IC(*)
      DIMENSION RPAR(*),IPAR(*),IFP(*)
C***********************************************************************
C     CALL OF PDESYS
C***********************************************************************
      CALL INTRES(NEQ,TIME,Y,F,RPAR,IPAR,IEMD)
      RETURN
C***********************************************************************
C     END OF FCN
C***********************************************************************
      END
      SUBROUTINE INTLEF(NZC,B,IR,IC)
C***********************************************************************
C
C     DEFINITION OF LEFT HAND SIDE (SEE -ULIMEX-)
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION B(*),IR(*),IC(*)
      PARAMETER(ONE=1.0D0)
C***********************************************************************
C
C***********************************************************************
      DO I=1,NZC
        IR(I)=I
        IC(I)=I
      ENDDO
      B(1:NZC) = ONE
      RETURN
C***********************************************************************
C     END OF FASLEF
C***********************************************************************
      END
      SUBROUTINE INTRES(NEQ,TIME,SMF,DEL,RPAR,IPAR,IFC)
C***********************************************************************
C
C
C***********************************************************************
C***********************************************************************
C     Storage organisation
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION SMF(NEQ),DEL(NEQ),RPAR(*),IPAR(*)
C***********************************************************************
C     COMMON BLOCKS
C***********************************************************************
      INCLUDE 'homdim.f'
C***********************************************************************
C     DATA Statements
C***********************************************************************
      DATA NFILE6/6/
C***********************************************************************
C     Call function evaluation
C***********************************************************************
      CALL MASRES(NEQ,TIME,SMF,DEL,RPAR,IPAR,IEREXD)
      IFC = 0
      IF(IEREXD.GT.0) IFC = 9
      RETURN
C***********************************************************************
C     END OF
C***********************************************************************
      END
