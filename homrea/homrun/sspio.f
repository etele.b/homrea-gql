C>    sspio
      SUBROUTINE GETIVA(IEQ,NTA,NFILE6,NFIL19,NEQ,SMFSYM,
     1         TIME,SMF,SMFP,NEQS,S,LRV,RV,RVS,FINISH)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *           LAST CHANGE:               / MAAS               *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C                                                                      *
C     INPUT  :
C
C
C     OUTPUT :
C
C     ARRAYS FOR POST-PROCESSING
C
C***********************************************************************
C
C
C
C***********************************************************************
C     I: STORAGE
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      COMMON/BSSPV/SSPV(20*MNSPE)
      LOGICAL FINISH
      CHARACTER(LEN=*)  SMFSYM(NEQ),RVS(LRV)
      CHARACTER*1 HEADER(40)
      DIMENSION SMF(NEQ),SMFP(NEQ),S(NEQS),RV(LRV)
      DOUBLE PRECISION, DIMENSION(1) :: DXDS,DTDS,DPDS
      DATA NZH/40/
C***********************************************************************
C     II: CHECK FOR OUTPUT
C***********************************************************************
      FINISH=.FALSE.
      NEQ2=1
      ICON = -NTA
C---  s used as a work array
      CALL INOUT3(ICON,NZH,HEADER,NFILE6,NFIL19,
     1    NEQ,SMF,SMFSYM,NEQ,SMFP,SMFSYM,LRV,RV,RVS,IERR)
      WRITE(NFILE6,8100) NFIL19
 8100 FORMAT(' ','===>  initial values read from unit ',I2)
      IF(IERR.EQ.-1.OR.IERR.GT.0) THEN
        FINISH=.TRUE.
        GOTO 700
      ENDIF
C***********************************************************************
C     calculate species concentrations
C***********************************************************************
      NSPEC=NEQ-2
      CALL TRASSP(IEQ,NEQ,SMF,NSPEC,S(NSPEC+1),C,S,DDU,HDU,PDU,SSPV,IEX,
     1       0,DXDS,1,DTDS,DPDS)
C***********************************************************************
C     set time
C***********************************************************************
      TIME=0.D0
C***********************************************************************
C
C***********************************************************************
  700 CONTINUE
      RETURN
C***********************************************************************
C     CHAPTER VI: FORMAT STATEMENTS
C***********************************************************************
C 910 CONTINUE
C     NTA=1
C     RETURN
C***********************************************************************
C
C***********************************************************************
      END
      SUBROUTINE INOUT3(ICON,NZH,HEADER,NFIERR,NFISTO,
     1    NEQ1,VAL1,SYMB1, NEQ2,VAL2,SYMB2, NEQ3,VAL3,SYMB3,IERR)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *    Program for the input and output of arrays             *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C                                                                      *
C     INPUT  :
C
C     ICON            = CONTROL VARIABLE
C
C                       ICON < -1:   INPUT  OF DATA
C                       ICON = -1:   INPUT  OF HEADER, SYMBOLS & DATA
C                       ICON =  0:   REWIND NFISTO
C                       ICON = +1:   OUTPUT OF HEADER, SYMBOLS & DATA
C                       ICON > +1:   OUTPUT OF DATA
C
C     NEQ             = NUMBER OF VARIABLES
C
C     NFISTO          = UNIT FOR IN-AND OUTPUT
C     NFIERR          = UNIT FOR ERROR MESSAGES
C
C     OUTPUT:
C
C     IERR            = ERROR FLAG
C
C                       IERR =  0:   REGULAR EXIT
C                       IERR = -1:   NO DATASET AVAILABLE
C                       IERR =  1:   DATASET DOES NOT FIT
C                       IERR =  2:   DATASET INCOMPLETE
C
C     INPUT OR OUTPUT:
C
C     SYMB(NEQ)       = CHARACTER*8 SYMBOLS FOR VARIABLES
C     VAL(NEQ)        = VALUES OF VARIABLES
C     HEADER(NZH)     = CHARACTER*1 VARIABLE FOR HEADING
C
C
C***********************************************************************
C
C***********************************************************************
C     I: STORAGE
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      CHARACTER(LEN=*) SYMB1(NEQ1),SYMB2(NEQ2),SYMB3(NEQ3)
      DIMENSION VAL1(NEQ1),VAL2(NEQ2),VAL3(NEQ3)
      CHARACTER*1 CHAR1,HEADER(NZH)
      CHARACTER*4 CHAR4
C***********************************************************************
C     II: INITIALIZE
C***********************************************************************
      IERR=0
      CHAR1=' '
      CHAR4='>DAT'
      NEQ = NEQ1+NEQ2+NEQ3
C***********************************************************************
C     III: ICON = 0
C***********************************************************************
      IF(ICON.EQ.0) REWIND NFISTO
C***********************************************************************
C     III: ABS(ICON) = 1
C***********************************************************************
      IF(IABS(ICON).EQ.1) THEN
        IF(ICON.LT.0) THEN
          READ(NFISTO,81,END=900) CHAR4,ND,(HEADER(I),I=1,NZH)
          IF(ND.NE.NEQ) GOTO 910
          READ(NFISTO,82,END=920) CHAR1
          READ(NFISTO,83,END=920)
     1     (SYMB1(K),K=1,NEQ1),(SYMB2(K),K=1,NEQ2),(SYMB3(K),K=1,NEQ3)
        ELSE
          WRITE(NFISTO,81) CHAR4,NEQ,(HEADER(I),I=1,NZH)
          WRITE(NFISTO,82) '        '
          WRITE(NFISTO,83)
     1     (SYMB1(K),K=1,NEQ1),(SYMB2(K),K=1,NEQ2),(SYMB3(K),K=1,NEQ3)
        ENDIF
      ENDIF
C***********************************************************************
C     IV: ABS(ICON) > 1
C***********************************************************************
      IF(IABS(ICON).GE.1) THEN
        IF(ICON.LT.0) THEN
          READ(NFISTO,84,END=920)
     1     (VAL1(K),K=1,NEQ1),(VAL2(K),K=1,NEQ2),(VAL3(K),K=1,NEQ3)
        ELSE
          WRITE(NFISTO,84)
     1     (VAL1(K),K=1,NEQ1),(VAL2(K),K=1,NEQ2),(VAL3(K),K=1,NEQ3)
        ENDIF
      ENDIF
C***********************************************************************
C     V: REGULAR EXIT
C***********************************************************************
  700 CONTINUE
      IERR=0
      RETURN
C***********************************************************************
C     CHAPTER VI: FORMAT STATEMENTS
C***********************************************************************
   81 FORMAT(A4,1X,I5,70A1)
   82 FORMAT(3X,A,6X,1PE25.18)
   83 FORMAT(1(1X,5(4X,A,4X)))
   84 FORMAT(1(1X,5(1PE15.8,1X)))
C***********************************************************************
C     error exits
C***********************************************************************
  900 CONTINUE
      IERR=-1
      RETURN
  910 CONTINUE
      IERR= 1
      WRITE(NFIERR,911)
  911 FORMAT(' ',' Input data do not fit ')
      GOTO 999
  920 CONTINUE
      IERR= 2
      WRITE(NFIERR,921)
  921 FORMAT(' ',' Input data incomplete ')
      GOTO 999
  999 CONTINUE
      WRITE(NFIERR,998)
  998 FORMAT(' ',' Error in INOUT3 ')
      STOP
C***********************************************************************
C
C***********************************************************************
      END
