C***********************************************************************
C
C    Block for Character length                                        *
C
C***********************************************************************
      PARAMETER(LSYMB=8)
C***********************************************************************
C
C    Block for global parameters                                       *
C
C***********************************************************************
C     MNSPE:     maximum number of species
C     MNELE:     maximum number of elements
C     MNREA:     maximum number of reactions
C     MNTHB:     maximum number of different third bodies in the react.
C     MNEQU:     maximum number of equations
C     MNXEQU:    maximum number of additional equations
C     MNPOI:     maximum number of point for time history of P, V, T
C     MNCRE:     maximum number of complex reactions
C     MNRREA:    length of real array for the reaction mechanim
C     MNIREA:    length of integer array for the reaction mechanim
C     MNSEN:     maximum number of sensitivity parameters
C     MNZONE:    maximum number of reactors
C***********************************************************************
cc MNEQU anpassen bei Fehlermeldung bei Aufruf von DTRSEN (high HC's)
      PARAMETER (MNSPE  =  1086, MNELE  =    10, MNREA=10000, MNTHB=90,
     1           MNXEQU =     5, MNEQU  = (MNSPE+MNXEQU),
     1           MNSEN  =  8500, MNPOI  =  1000, MNCRE  =    20,
     1           MNRREA =300000, MNIREA =200000,
     1           MNRSPE = (34+4*MNSPE)*MNSPE)
C    1           MNRREA =155461, MNIREA =157825, MNRSPE = 10293)
C***********************************************************************
C     BLOCK FOR MULTIZONE MODEL   (Schubert 17.06.05)
C***********************************************************************
      PARAMETER (MNZONE=50)
C***********************************************************************
C
C    Block for interface with CFD
C
C***********************************************************************
      PARAMETER (MNPDE = 5)
C***********************************************************************
C
C    Block for IPAR and RPAR
C
C***********************************************************************
      PARAMETER (MNRPAR = 120001, MNIPAR = 14001)
C***********************************************************************
C155461
C    Block for ILDM table setup
C
C***********************************************************************
C     MNCOV:     maximum number of controlling variables
C     MNTCO:     maximum number of tabulation coordinates
C     MNCON:     maximum number of conserved quantities
C     MNPROP:    maximum number of properties stored in the table
C
C     MNRG:      length of real array for the tabulation
C     MNIG:      length of integer array for the tabulation
C
C     MNRGO:     length of real array for the tabulation    (old grid)
C     MNIGO:     length of integer array for the tabulation (old grid)
C                these variables can be set to 1 (usually)
C     MNCGV:     maximum number of vertices in the coarse grid
C     MNLIM:     maximum number of domain limiting vectors
C
C     NCELM:     maximum number of cells
C     NVERM:     maximum number of vertices fo the mesh
C
      PARAMETER (NCELM=4000,NVERM=4000)

      PARAMETER (MNCOV=20, MNTCO=6, MNCON = 10, MNPROP=4200,
     1           MNCGV=1000, MNLIM = 100,
     1           MNRG   = (MNPROP*NVERM), MNIG  = 1000,
     1           MNRGO  =    100, MNIGO =     100)
C
      PARAMETER (NDIMM=4,N2DIMM=2**NDIMM)
      PARAMETER (LTW= 1000)
      PARAMETER (MPROMA=1000)
C
      PARAMETER ( mxpkte= 1, mypkte= 1, mzpkte= 1 , mnpkte= 10)
C***********************************************************************
C     Aenderung fuer berechnung und ausgabe von chi Mischungsbruch
C***********************************************************************
      parameter ( mncri= 5 )
      parameter ( meisch= 30 ,mpunk= 30 )
C***********************************************************************
C
C    COMMON BLOCKS
C
C***********************************************************************
C***********************************************************************
C     BLOCK FOR SPECIES DATA
C***********************************************************************
      COMMON/BRSPEC/RSPEC(MNRSPE),LRSPEC,NEPS,NSIG,NHL,NHSW,NETA,
     1                           NLCOEF,NLCOEM,NLCOED,NDCOEF
      COMMON/BXMOL/XMOL(MNSPE),XMINV(MNSPE),EMOL(MNELE),XE(MNSPE*MNELE)
C***********************************************************************
C     BLOCK FOR REACTION MECHANISM DATA
C***********************************************************************
      COMMON/BNREAC/NREAC,NM,NRINS,NRTAB,NINERT
      COMMON/BRREAC/RREAC(MNRREA),LRREAC,NPK,NTEXP,NEXA,NVT4,NVT5,NVT6,
     1           NAINF,NBINF,NEINF,NT3,NT1,NT2,NAFA,NBFA,
     1           NXZSTO,NRATCO
      COMMON/BIREAC/IREAC(MNIREA),LIREAC,NIREV,NIARRH,
     1       NMATLL,NMATLR,NNRVEC
