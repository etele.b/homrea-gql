      SUBROUTINE NEWGEN(NCA,NEQ,NC,NDIM,SMF,SMF0,
     1           RPAR,IPAR,LRW,RW,LIW,IW,IERR,SYMB)
C***********************************************************************
C     SUBROUTINE NEEDED BY NEWTON SOLVER
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      EXTERNAL MASRES
      PARAMETER (ONE = 1.0D0,  ZERO = 0.0D0)
      LOGICAL COSGRI,TEST,DONE,IHAVEM,TRAANA,CLEAN
      PARAMETER (LRV=15)
C***********************************************************************
C     CHARACTER
C***********************************************************************
      CHARACTER*1 IAPLOT
      CHARACTER*1 POSTSC
      CHARACTER(LEN=*)  SYMB(NEQ)
      PARAMETER(LRETSY=8)
      CHARACTER(LEN=LRETSY) RETSYM(LRV)
C***********************************************************************
C     ARRAYS
C***********************************************************************
      DIMENSION RV(LRV)
      DIMENSION RW(LRW),IW(LIW)
      DIMENSION RPAR(*),IPAR(*)
      DIMENSION KONS(MNPROP+1)
C***********************************************************************
C     ARRAYS
C***********************************************************************
      DOUBLE PRECISION, DIMENSION(NC)                :: SCALD
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)           :: QEVB,ERVROW,
     1                                                  ERVCOL
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:,:)  :: PROP
      CHARACTER(LEN=LSYMB), ALLOCATABLE, DIMENSION(:) :: SYMPRP,SYMPRO
C***********************************************************************
C     Mesh information
C***********************************************************************
      INTEGER, DIMENSION(NDIM,2**NDIM)  :: IPAIR,IUNIT
      INTEGER, DIMENSION(NDIM,NCELM)    :: IY
      INTEGER, DIMENSION(NDIM,NVERM)    :: ICORD
      INTEGER, DIMENSION(2**NDIM,NCELM) :: IVERT
      INTEGER, DIMENSION(NVERM)         :: IRET
C***********************************************************************
      DOUBLE PRECISION, DIMENSION(NC,NEQ)   :: PMROW,SPMROW
      DOUBLE PRECISION, DIMENSION(NC,NEQ)   :: CMAT
      DOUBLE PRECISION, DIMENSION(NEQ,NC)   :: PMCOL,SPMCOL
      DOUBLE PRECISION, DIMENSION(NC,NC)    :: PCR
      DOUBLE PRECISION, DIMENSION(NEQ)      :: SMF,SMF0
      DOUBLE PRECISION, DIMENSION(NC,NC)    :: SCMA
C***********************************************************************
C     COMMON BLOCKS
C***********************************************************************
      COMMON/BTAUCO/TAUCO(MNCON*(2+MNSPE))
      COMMON/ILDMOP/IPARA,ISTAOP,MANPAR,NOFORM,ANAFUE,INMAN,LASLOO,
     1              ICALTR,MASENS,IUPVER,NTRANS
      COMMON/ILDMPA/IMAILD,ILMILD,IOILDM
      COMMON/BOUT/KONINT,KONTAB,KONPLO,KONSEN,KONMEC,KONANA,KONRED,
     1            KONSPE(MNSPE),KONSID(MNSPE),NSC,
     1            ICRIT,KOGSEN,KONRST,MOLEFR,MASSFR,LPLO
      COMMON/BIGENI/IGENI,LNF
      COMMON/BLOCPR/NIAUX,NISMF,NIPF,NIRV,NIPRO,NIDPT,
     1   NIDPSE,NIHFLA,NIICOR,NIEGVA,NPRONT
      COMMON/BLOCOR/IIRAT,IISMF,IISMFP,IIRV,IIPRO,IIPNU,IILAM,NPROOT
      COMMON/BICHKD/ICHKD
      COMMON/BIMIXP/IMIXP
      COMMON/BINDL/INDLO1,INDLO2
      COMMON/BERV/ERVRO((MNSPE+2)**2),ERVCO((MNSPE+2)**2),NZVT
C***********************************************************************
C     USEFUL CONSTANTS
C***********************************************************************
      DATA NFIOUT/6/,POSTSC/'N'/,IAPLOT/'I'/
      DATA NFIOIL/85/,NFITRA/33/,NFIL21/21/,NFITAB/94/
      DATA RETSYM( 1)/'Flag    '/,RETSYM( 2)/'IEGTAG  '/,
     1     RETSYM( 3)/'zero    '/,RETSYM( 4)/'lamsr   '/,
     1     RETSYM( 5)/'lamsi   '/,RETSYM( 6)/'lamfr   '/,
     1     RETSYM( 7)/'lamfi   '/,RETSYM( 8)/'drealra '/,
     1     RETSYM( 9)/'realra  '/,RETSYM(10)/'ineg/LNF'/,
     1     RETSYM(11)/'T(K)    '/,RETSYM(12)/'rho-p   '/,
     1     RETSYM(13)/'mmolm   '/,RETSYM(14)/'     x4 '/,
     1     RETSYM(15)/'     x5 '/
C***********************************************************************
C     Data statements
C***********************************************************************
      INDLO1 = 3
      INDLO2 = 4
C---- transport model
      ITRM = NTRANS
      IPAR(13) = ITRM
      CLEAN = .FALSE.
      NZEV  = IPAR(6)+2
      IEQ   = IPAR(10)
      NSPEC = IPAR(3)
      MIXFRA = NDIM + NZEV - NC
                      ISCVAR = 0
      IF(MIXFRA.EQ.0) ISCVAR = 1
CRASH
C                     ISCVAR = 0
      N2DIM = 2**NDIM
      ILAST = 0
      IHAVEM = .FALSE.
      TRAANA = .FALSE.
      IF(ISTAOP.LT.0) TRAANA =.TRUE.
c***********************************************************************
c     Get informtion about system
c***********************************************************************
      CALL GETNUM(NLIMI)
c***********************************************************************
c     Information about the use of mixture fraction as coordinate
c***********************************************************************
C     SMF(1) = -2.0728695E+05
      mix = 1
C***********************************************************************
C
C     INITIALIZE
C
C***********************************************************************
      TEST = .FALSE.
      DONE = .FALSE.
      NUCALL = 0
      NCONS=3
      KONS = 0
C***********************************************************************
C     Print info abou output of properties
C***********************************************************************
C---- IPARA is 2 for Option THETA = 2, and otherwise 1
!       IF (IPARA.EQ.1) WRITE(6,33)
!       IF (IPARA.EQ.2) WRITE(6,34)
!    33 FORMAT(3('*'),2x,1('ILDM-Output in fix. coordinates'),2x,3('*'))
!    34 FORMAT(3('*'),2x,1('ILDM-Output in gen. coordinates'),2x,3('*'))
      IF (INMAN.LT.1) WRITE(NFIOUT,8001)
 8001 FORMAT(80('*'),/,'      ILDM Routine Called    ',/,80('*'))
C***********************************************************************
C     Setup numbers, assign symbols and allocate large arrays
C***********************************************************************
C---- change 1 to 0 for constant parameterization
      CALL NSETUP(1,NDIM,NC,NEQ,NZEV,MIXFRA,LRV,IPAR,
     1            ICHKD,SCALD,NPROP)
      IF(.NOT.ALLOCATED(PROP))  ALLOCATE (PROP(NPROP,NVERM))
      IF(.NOT.ALLOCATED(SYMPRP)) ALLOCATE (SYMPRP(NPROP))
      IF(.NOT.ALLOCATED(SYMPRO)) ALLOCATE (SYMPRO(NPROP))
      PROP  = 0.D0
      CALL TABSYG(NDIM,NEQ,SYMB,LRV,RETSYM,NPROP,SYMPRP)
C***********************************************************************
C     Initialize unit vector
C***********************************************************************
C***********************************************************************
C A. Neagos:
C Not necessary if INMAN(NOPT(24))=1, e.q. REDIM is generated
C***********************************************************************
      IF (INMAN.LT.1)THEN
      CALL UNISET(NDIM,N2DIM,IUNIT,IPAIR)
      ENDIF
C***********************************************************************
C     Get matrix for transf. into conserved and non-conserved variables
C***********************************************************************
      CALL INIERV(NEQ,NZEV,ERVROW,ERVCOL)
      CALL CONPAR(NEQ,NZEV,MIXFRA,NC,CMAT,MSGFIL)
      CALL DCOPY(NEQ*NEQ,ERVROW,1,ERVRO,1)
      CALL DCOPY(NEQ*NEQ,ERVCOL,1,ERVCO,1)
      NZVT = NZEV
C***********************************************************************
C     Store initial point in PROP
C***********************************************************************
      PROP(IISMF:IISMF-1+NEQ,1)  = SMF(1:NEQ)
      NVERT               = 1
      ICORD(1:NDIM,NVERT) = 0
      IRET(NVERT)         = 0
      NCELL               = 1
      IY(1:NDIM,NCELL)    = 0
C***********************************************************************
C     Get pointers for parameterization matrix
C***********************************************************************
      COSGRI = .TRUE.
      IF(MANPAR.LE.1) COSGRI=.FALSE.
      IF(COSGRI) THEN
        INFOD = -2
      ELSE
        INFOD = 2
      ENDIF
C***********************************************************************
C
C
C     Read trajectory results and process
C
C
C***********************************************************************
      IF(TRAANA) THEN
      REWIND NFIL21
      IREV = 0
 2010 CONTINUE
      IREV = IREV+1
c     IREV = 1
      READ(NFIL21,882,END=2090) RV(15),PROP(NDIM+1:NDIM+NEQ,IREV)
 882  FORMAT(8(1PE14.7,1X))
      write(*,*) irev,nzev,neq,ndim,nc,nprop,lrv,liw,lrw
      write(*,*) nverm
      CALL CALEVI(IREV,NEQ,NZEV,NDIM,NC,NPROP,NVERM,PROP,
     1       IRET,LRV,RV,LIW,IW,LRW,RW,IPAR,RPAR,CMAT,ERVROW,ERVCOL)
c     if(irev.gt.2) goto 2090
      GOTO 2010
 2090 CONTINUE
      NVERT = IREV -1
      CALL TTPOUT(NCA,NFITRA,NPROP,SYMPRP,NVERT,NPROP,PROP,NDIM)
      GOTO 8000
      ENDIF
C***********************************************************************
C
C
C     Read starting solution for a hierarchical generation of the
C      manifold
C
C
C***********************************************************************
      IF(ISTAOP.GT.0.AND.KONRST.NE.2)THEN
        ISPARA = IPARA
        CALL INPDAT(NDIM,N2DIM,NCELL,NVERT,NEQ,NC,LRV,NSPECLOW,
     1               IY,IVERT,ICORD,IRET,NPROP,PROP,SYMPRP,SYMPRO,CMAT)
        IPARA = ISPARA
        NVERTO = NVERT
      ENDIF
C***********************************************************************
C     Simply print the old manifold and exit
C***********************************************************************
      IF(ISTAOP.EQ.99)THEN
        CALL XTPOUT(111,NPROP,SYMPRP,NVERT,NPROP,PROP,ICORD,NDIM,N2DIM,
     1    NCELL,IVERT)
        CALL IOGPAR(1,NFITAB,NPROP,NCELL,NCELL,NVERT,NDIM,N2DIM,
     1            NSPEC,NREAC,IY,IVERT,ICORD,IRET,PROP,SYMPRP,
     2            CMAT,MIX,NC)
        GOTO 8000
      ENDIF
C***********************************************************************
C
C
C     Block for handling read manifold
C         This block handles all cases except for the calculation of
C         manifold points
C
C
C***********************************************************************
C***********************************************************************
C
C     Compute invariant manifold
C
C***********************************************************************
C***********************************************************************
C     Read initia guess from file NFITAB
C***********************************************************************
      IF(INMAN.GE.1) THEN
      WRITE(NFIOUT,8881)
 8881 FORMAT(1X,79('*'),/,' *  REDIM started',/,1x,79('*'))
        IF(KONRST.EQ.3) THEN
        CALL IOGPAR(-1,NFITAB,NPROP,NCELL,NCELL,NVERT,NDIM,N2DIM,
     1            NSPEC,NREAC,IY,IVERT,ICORD,IRET,PROP,SYMPRP,
     2            CMAT,MIX,NC)
C       IF(KONPLO.NE.0)
C    1  CALL PLTM(NUCALL,IAPLOT,POSTSC,NDIM,NCELL,NVERT,NPLP,
C    1            PROP,NPROP,IVERT,SYMPRP,NCONS,KONS,LIW,IW,LRW,RW)
      ENDIF
C***********************************************************************
C
C***********************************************************************
        NDINV = NEQ-NDIM
C***********************************************************************
C     Read initia guess from file NFITAB
C        for KONRST = 2 read data for gradients
C        for KONRST = 3 read data for gradients and initial guess
C***********************************************************************
      IF(KONRST.GE.2) THEN
      IF(NDIM.GT.3) THEN
         WRITE(*,*) ' NDIM > 3 for XXXIN'
         STOP
      ENDIF
C---  infoxx = 0 means that initial values are taken from xxxin
      IF(KONRST.LT.3) THEN
        INFOXX = 0
      ELSE
        INFOXX = 1
      ENDIF
      CALL XXXIN(INFOXX,77,NEQ,NSPEC,NVERM,NCELM,
     1     NPROP,PROP,SYMPRP(IISMF),SYMPRO,
     1     NVERT,NCELL,ICORD,IRET,IY,IVERT,NDIM,IEQ,ITRM)
C--- plot these values
c       IF(KONPLO.NE.0) THEN
c          CALL PLTM(NUCALL,IAPLOT,POSTSC,NDIM,NCELL,
c    1   NVERT,NPLP,PROP,NPROP,IVERT,SYMPRP,NCONS,KONS,LIW,IW,LRW,RW)
c      write(*,*) 'UUUUUUUUUUUUUU'
c     ENDIF
      ENDIF
C***********************************************************************
C     check if transport properties are available
C***********************************************************************
        IF(ICALTR.EQ.0) THEN
            WRITE(*,*) ' no transport properties available for INERTM'
            STOP
        ENDIF
C***********************************************************************
C     Call REDIM routine
C***********************************************************************
        IF(KONPLO.NE.0) CALL PLTM(NUCALL,IAPLOT,POSTSC,NDIM,NCELL,
     1   NVERT,NPLP,PROP,NPROP,IVERT,SYMPRP,NCONS,KONS,LIW,IW,LRW,RW)
        CALL INERTM(ISCVAR,NDIM,NEQ,NC,CMAT,LRV,NPROP,PROP,NVERT,NCELL,
     1        ICORD,IRET,IY,IVERT,N2DIM,SYMPRP,
     1        RV,RPAR,IPAR,LRW,RW,LIW,IW,NDINV,MIX)
      NUCALL = 0
       write(*,*) LRV,NPROP,NVERT,NCELL,NDIM
       write(*,*) nplp,iaplot,postsc,nucall,ncons,liw,lrw
c       IF(KONPLO.NE.0) THEN
c       CALL PLTM(NUCALL,IAPLOT,POSTSC,NDIM,
c    1     NCELL,NVERT,NPLP,PROP,NPROP,IVERT,SYMPRP,NCONS,KONS,
c    1     LIW,IW,LRW,RW)
c       write(*,*) '2PPPPPPPPPPPPPPP'
c     ENDIF
         GOTO 7000
      ENDIF
C***********************************************************************
C
C
C
C     Analyse hierarchy with respect to the mechanism
C
C
C
C***********************************************************************
      IF(ANAFUE.EQ.1)THEN
         NEQL = NSPECLOW+2
         NEQU = NEQ-NEQL
         CALL ANAHIERA(NEQ,NC,NZEV,NVERT,NPROP,PROP,RPAR,IPAR,
     1                 LRW,RW,LIW,IW,IERR,NEQL,NEQU,SYMPRP)
         CALL XTPOUT(1001,NPROP,SYMPRP,NVERT,NPROP,PROP,ICORD,
     1               NDIM,N2DIM,NCELL,IVERT)
         CALL IOGPAR(1,NFITAB,NPROP,NCELL,NCELL,NVERT,NDIM,N2DIM,
     1            NSPEC,NREAC,IY,IVERT,ICORD,IRET,PROP,SYMPRP,
     2            CMAT,MIX,NC)
         GOTO 8000
      ENDIF
C***********************************************************************
C
C
C
C     This block handles the manifold points obtained from
C     an old solution or calculates the manifold point for the
C     initial point on the manifold
C
C
C
C***********************************************************************
      DO ILOW=1,NVERT
      CALL CALIPM(INFO,
     1   ILOW,NDIM,NVERT,NPROP,PROP,IRET,NEQ,NC,NZEV,MIXFRA,
     1   LRV,RV,
     1   CMAT,TAUCO,ERVCOL,ERVROW,QEVB,LIW,IW,LRW,RW,IPAR,RPAR,NFIOUT)
      NEQNC=NEQ*NC
      ENDDO
C***********************************************************************
C     calculate trajectory
C***********************************************************************
      IF(1.EQ.2) THEN
      TEND = 1.0
      MSGFIL = 6
      CALL INTFAS(NEQ,SMF0,TEND,MSGFIL,IERR,RPAR,IPAR,
     1          QEVB,ERVROW,ERVCOL)
      ENDIF
C***********************************************************************
C
C     The following loop creates the first cell or the first cell
C        array (in the case of a hierarchical generation)
C
C***********************************************************************
C     new deal for parameterization matrix
C***********************************************************************
C     Initialize Parameterization matrix
C***********************************************************************
      IF(ISTAOP.EQ.1)THEN
C---- Calculate missing direction
         ICEL = 1
         SMF(1:NEQ) = PROP(NDIM+1:NDIM+NEQ,ICEL)
         IF(MANPAR.EQ.0) THEN
         IADD = 1
         CALL AUTPAR(IADD,NEQ,NZEV,SMF,ERVROW,ERVCOL,
     1        NC,CMAT,TAUCO,LIW,IW,LRW,RW,IPAR,RPAR,NFIOUT)
          ENDIF
      ENDIF
      CALL DLACPY('A',NC,NEQ,TAUCO,NC,PMROW,NC)
  800 FORMAT(28X,I2)
  802 FORMAT(7(1PE10.3,1X))
C***********************************************************************
C     Scale the parameterization matrix
C***********************************************************************
      ISCA = 1
      DO I = 1,NC
      PMROW(I,1:NEQ) = PMROW(I,1:NEQ) / SCALD(I)
      ENDDO
C***********************************************************************
C     Initialize direction vectors (pseudo-inverse of pmrow
C***********************************************************************
      PMCOL(1:NEQ,1:NC) = ZERO
      PCR = MATMUL(PMROW,TRANSPOSE(PMROW))
      CALL DGEINV(NC,PCR,RW,IW,IERINV)
      IF(IERINV.NE.0) THEN
          WRITE(*,*) ' This should not happen - error in newgen'
          STOP
      ENDIF
      PMCOL = MATMUL(TRANSPOSE(PMROW),PCR)
C***********************************************************************
C     Calculate cells
C***********************************************************************
      IF(KONRST.EQ.0) THEN
      IF(ISTAOP.NE.2)THEN
         DO IIY=1,NCELL
            INFOG = 1
            IF(ISTAOP.EQ.0) INFOG = 0
            INDC = IIY
            INFODT = 2
            IGENI = 0
C           IF(INMAN.LT.0) IGENI = ABS(INMAN)
            CALL GENCEL(INDC,INFOG,ICF,NDIM,N2DIM,IY(1,IIY),
     1           IVERT(1,IIY),NVERM,NVERT,ICORD,IRET,IUNIT,
     1           IPAIR,NPROP,PROP,PMCOL,PMROW,MIXFRA,
     1           INFODT,NEQ,NZEV,NC,QEVB,ERVROW,ERVCOL,
     1           LRV,RV,RPAR,IPAR,LRW,RW,LIW,IW,
     1           NFIOUT,IERR)
       ENDDO
       ENDIF
C***********************************************************************
C     For hierarchical generation cells should be complete
C***********************************************************************
C---- Check the number of vertices
         IF(ISTAOP.EQ.1.AND.NVERT.NE.2*NVERTO)THEN
            WRITE(NFIOUT,150) NVERT,2*NVERTO
  150 FORMAT ('Error in starting solution:',/,
     1        'NVERT',I5,' is not equal 2*NVERTO',I5)
            STOP
         ENDIF
      ENDIF
C***********************************************************************
C     For hierarchical generation cells should be complete
C***********************************************************************
      IF(KONRST.EQ.0) THEN
      NUCALL = 0
        IF(KONPLO.NE.0)
     1CALL PLTM(NUCALL,IAPLOT,POSTSC,NDIM,
     1     NCELL,NVERT,NPLP,PROP,NPROP,IVERT,SYMPRP,NCONS,KONS,
     1     LIW,IW,LRW,RW)
      ENDIF
C***********************************************************************
C***********************************************************************
C     calculate neighbouring cells
C***********************************************************************
      ICELL = NCELL
      NCELO = NCELL
      NCEL1 = NCELL
      IF(ISTAOP.GT.0) NCEL1 = 1
      NCEL2 = NCELL
C***********************************************************************
C     Check if this is a continuation
C***********************************************************************
      NCEL2T = 0
      NVERTT = 0
CBUG
      IF(KONRST.NE.0.AND.KONRST.NE.2) THEN
      OPEN(NFITAB,STATUS='UNKNOWN')
      CALL IOGPAR(-1,NFITAB,NPROP,NCEL1T,NCEL2T,NVERTT,NDIM,N2DIM,
     1            NSPECT,NREAC,IY,IVERT,ICORD,IRET,PROP,SYMPRP,
     2            CMAT,mix,NC)
      IF(NCEL2T.EQ.0.OR.NVERTT.EQ.0) THEN
         WRITE(NFIOUT,*) ' No data received from restart file        '
         STOP
      ENDIF
      IF(NSPEC.NE.NSPECT) THEN
         WRITE(NFIOUT,*) ' Species number inconsistent for rest. ILDM'
         STOP
      ENDIF
      CLOSE(NFITAB,STATUS='KEEP')
      ENDIF
C***********************************************************************
C     Set numbers
C***********************************************************************
      IF(NCEL2T.GT.0) THEN
CUUU    NCEL1 = NCEL1T
        NCEL1 = 1
        NCEL2 = NCEL2T
        NVERT = NVERTT
        NCELL = NCEL2
        ICELL = NCELL
      ENDIF
C***********************************************************************
C     Plot results
C***********************************************************************
C---- Minigraphic
      NPLP = NDIM + 2 * NEQ + LRV
        IF(KONPLO.NE.0)
     1CALL PLTM(NUCALL,IAPLOT,POSTSC,NDIM,
     1   NCELL,NVERT,NPLP,PROP,NPROP,IVERT,SYMPRP,NCONS,KONS,
     1          LIW,IW,LRW,RW)
C---- Tecplot
      IF(ISTAOP.EQ.2) GOTO 7000
      IF(ISTAOP.NE.0) GOTO 100
C***********************************************************************
C
C     Cleanup loop to update vertices  of cells
C
C***********************************************************************
      IF(IUPVER.NE.0) THEN
         DO I=1,NCELL
            INFOG = IUPVER
                           IGENI = 0
            IF(INMAN.LT.0) IGENI = ABS(INMAN)
            CALL UPDCEL(I,INFOG,ICF,NDIM,N2DIM,IY(1,I),
     1           IVERT(1,I),NVERT,ICORD,IRET,IUNIT,IPAIR,
     1           NPROP,PROP,PMCOL,PMROW,MIXFRA,INFOD,NEQ,NZEV,NC,
     1           QEVB,ERVROW,ERVCOL,
     1           LRV,RV,RPAR,IPAR,LRW,RW,LIW,IW,NFIOUT,
     1           IERR)
         ENDDO
      NCEL1 = 1
C---- Minigraphic
      NUCALL = 0
      NPLP = NDIM + 2 * NEQ + LRV
        IF(KONPLO.NE.0)
     1CALL PLTM(NUCALL,IAPLOT,POSTSC,NDIM,
     1   NCELL,NVERT,NPLP,PROP,NPROP,IVERT,SYMPRP,NCONS,KONS,
     1          LIW,IW,LRW,RW)
      GOTO 6000
      ENDIF
C***********************************************************************
C
C     Cleanup loop for NCEL1 = NCEL2
C
C***********************************************************************
C---- the following block make no sense for a starting solution based
C     generation
CUUU
C     IF(NCEL2-NCEL1.EQ.0.AND.NCELL.GT.2) THEN
      IF(NCELL.GT.2.AND.LASLOO.NE.0) THEN
C***********************************************************************
C    Close boundary cells
C***********************************************************************
      IF(LASLOO.EQ.1)THEN
         NCEL1 = 1
         ILAST = 2
         GOTO 100
      ELSE IF(LASLOO.EQ.2) THEN
         NCEL1 = 1
         ILAST = 3
         GOTO 100
C***********************************************************************
C     allow to add cells even if not all flag are 0 in old cell
C***********************************************************************
      ELSE IF(LASLOO.EQ.3) THEN
         WRITE(*,8872)
         NCEL1 = 1
         ILAST = 1
         GOTO 100
      ELSE
         GOTO 6000
      ENDIF
      ENDIF
      IF(LASLOO.LT.0) GOTO 6000
C***********************************************************************
C     Loop for cell addition
C***********************************************************************
 100  CONTINUE
      INILO = 0
 111  CONTINUE
      INILO = INILO + 1
C***********************************************************************
C
C     Build up manifold
C
C***********************************************************************
      WRITE(NFIOUT,8400) NCEL1,NCEL2
 8400 FORMAT(' ',60('*'),/,' ***',/,
     1     ' *** Loop from cell',I6,' to',I6,/,' ***',/,' ',60('*'))
C***********************************************************************
C     Start loop
C***********************************************************************
      DO 630 KK = NCEL1,NCEL2
         NCELO = NCELL
C
C        IF(ILAST.EQ.3.OR.ILAST.EQ.0) THEN
         IF(ILAST.EQ.3) THEN
         DO I=1,N2DIM
         IF(IRET(IVERT(I,KK)).NE.0) GOTO 630
         ENDDO
         ENDIF
C
         CALL NEICEL(KK,NDIM,NCELL,NCELM,IY,NFIOUT,IERR)
         SPMCOL = PMCOL
         SPMROW = PMROW
C        CALL DIRCEL(INFOD,-1,0,NDIM,N2DIM,IVERT(1,KK),
         CALL DIRCEL(INFOD, 0,0,NDIM,N2DIM,IVERT(1,KK),
C------TTTTTT
     1        IRET,IPAIR,PROP(NDIM+1,1),NPROP,NEQ,
     1        SPMCOL(1,NZEV-MIXFRA+1),NFIOUT,IERR)
         SCMA = MATMUL(PMROW,SPMCOL)
C        write(*,7865) 'ZZZ',(SCMA(i,i),I=1,nc)
 7865 FORMAT(A3,6(1pe9.2,1x))
         IF(INFOD.GT.0) THEN
            CALL UPDPAR(NC,NZEV,NEQ,SPMCOL,SPMROW,NFIOUT)
         ELSE
            DO I=1,NC
C           SPMROW(I,1:NEQ) = SPMROW(I,1:NEQ)  / SCMA(I,I)
            SPMCOL(1:NEQ,I) = SPMCOL(1:NEQ,I)  / SCMA(I,I)
            ENDDO
         ENDIF
C***********************************************************************
C     create new neighboring cells for one old cell
C***********************************************************************
         I = NCELO
         DO 140 II = NCELO+1,NCELL
            I = I + 1
           TEST = .FALSE.
C***********************************************************************
C     copy parameterization matrices from neighbouring cell
C***********************************************************************
            PMCOL = SPMCOL
            PMROW = SPMROW
            TEST = .TRUE.
            IF(TEST) WRITE(NFIOUT,8011) KK,II
 8011       FORMAT(' parameterization matrix from cell',
     1           I5,' used for cell',I5)
C***********************************************************************
C     calculate the cell
C***********************************************************************
c     I = icell (index of the cell)
            INFOG = 0
            IF(ILAST.EQ.1) INFOG = 1
            IF(ILAST.EQ.2) INFOG = 5
            IF(ILAST.EQ.3) INFOG = 1
            write(*,*) 'ILAST,INFOG', ILAST, INFOG
                            IGENI = 0
            IF(INMAN.LT.0) IGENI = ABS(INMAN)
            CALL GENCEL(I,INFOG,ICF,NDIM,N2DIM,IY(1,I),
     1           IVERT(1,I),NVERM,NVERT,ICORD,IRET,IUNIT,IPAIR,
     1           NPROP,PROP,PMCOL,PMROW,MIXFRA,
     1           INFOD,NEQ,NZEV,NC,QEVB,ERVROW,ERVCOL,LRV,RV,
     1           RPAR,IPAR,LRW,RW,LIW,IW,NFIOUT,IERR)
C------- Trick
C        IRET(1) = -1
C***********************************************************************
C     delete the cell if it hasn't been created
C***********************************************************************
            IF(ICF.EQ.0) THEN
               IY(1:NDIM,I:NCELL-1)=IY(1:NDIM,I+1:NCELL)
               I = I - 1
            ENDIF
 140     CONTINUE
C***********************************************************************
C     End of loop for inner cell creation
C***********************************************************************
         NCELL =  I
 630  CONTINUE
C***********************************************************************
C
C     Loop over one old cell row is complete
C
C***********************************************************************
      NCEL1 = NCEL2+1
      NCEL2 = NCELL
      IF(NCEL1.GT.NCEL2) THEN
         DONE = .TRUE.
         NCEL1 = MIN(NCEL1,NCEL2)
      ENDIF
C***********************************************************************
C     Output of intermediate results
C***********************************************************************
C     IF(DONE.OR.NCELL.GE.NCELM.OR.ILAST.EQ.2) THEN
      IF(2.EQ.2) THEN
      open(nfitab,status='unknown')
C---- storage file
      CALL IOGPAR(1,nfitab,NPROP,NCEL1,NCEL2,NVERT,NDIM,N2DIM,
     1     NSPEC,NREAC,IY,IVERT,ICORD,IRET,PROP,SYMPRP,
     2     CMAT,mix,NC)
      close(NFITAB,status='keep')
C---- Terminal plot
      NUCALL = 0
        IF(KONPLO.NE.0)
     1 CALL PLTM(NUCALL,IAPLOT,POSTSC,NDIM,
     1     NCELL,NVERT,NPLP,PROP,NPROP,IVERT,SYMPRP,NCONS,KONS,
     1     LIW,IW,LRW,RW)
C---- Tecplot
      open(nfioil,status='unknown')
      CALL XTPOUT(nfioil,NPROP,
     1     SYMPRP,NVERT,NPROP,PROP,ICORD,NDIM,N2DIM,NCELL,
     1     IVERT)
      close(nfioil,status='keep')
      ENDIF
C***********************************************************************
C     decide about continuation
C***********************************************************************
C     IF(DONE.OR.NCELL.GE.NCELM.OR.ILAST.GT.0) GOTO 6000
      IF(DONE.OR.NCELL.GE.NCELM.OR.ILAST.EQ.2) GOTO 6000
      GOTO 111
C***********************************************************************
C
C     Cleanup loops
C
C***********************************************************************
C***********************************************************************
 6000 CONTINUE
      IF(.NOT.CLEAN) THEN
         NCEL1 = 1
         ILAST = 2
         CLEAN = .TRUE.
         GOTO 100
       ENDIF
C***********************************************************************
C     allow to add cells even if not all flag are 0 in old cell
C***********************************************************************
 8872 FORMAT(' allow to continue with completed solution ')
C***********************************************************************
C
C     Cell deletion for lasloo .lt. 0
C
C***********************************************************************
      IF(LASLOO.LT.0) THEN
C***********************************************************************
C     Check, if there are vertices with the same values
C***********************************************************************
      IF(LASLOO.EQ.-1)  THEN
         NVERTC = NVERT
         CALL CHKVER(NVERTC,NVERT,NCELL,NDIM,N2DIM,NC,CMAT,NEQ,NPROP,
     1               IY,ICORD,IVERT,IRET,PROP)
C***********************************************************************
C     delete cells which are not completely o.k.
C***********************************************************************
      ELSE IF(LASLOO.LE.-2) THEN
                       IIDEL = 1
      IF(LASLOO.EQ.-3) IIDEL = -1
      IF(LASLOO.EQ.-4) IIDEL =  2
      IF(LASLOO.EQ.-5) IIDEL =  3
      IF(LASLOO.EQ.-6) IIDEL =  4
      IF(LASLOO.EQ.-7) IIDEL =  5
      IF(LASLOO.EQ.-8) IIDEL =  0
      IREC = IIRV -1 + 14
      VREC = 0.40
      I = 0
 2131 CONTINUE
      I = I + 1
      CALL DELCEL(IIDEL,I,NCELL,NVERT,NDIM,N2DIM,NPROP,
     1    IRET,ICORD,PROP,NEQ,
     1  IISMF,IREC,VREC,IVERT,IY,NCELLN,NVERTN,NFIOUT)
      IF(NCELL.NE.NCELLN) I=I-1
      NCELL = NCELLN
      NVERT = NVERTN
C     NUCALL = 0
C       IF(KONPLO.NE.0)
C    1CALL PLTM(NUCALL,IAPLOT,POSTSC,NDIM,
C    1   NCELL,NVERT,NPLP,PROP,NPROP,IVERT,SYMPRP,NCONS,KONS,
C    1          LIW,IW,LRW,RW)
      IF(I.LT.NCELL) GOTO 2131
      NUCALL = 0
        IF(KONPLO.NE.0)
     1CALL PLTM(NUCALL,IAPLOT,POSTSC,NDIM,
     1   NCELL,NVERT,NPLP,PROP,NPROP,IVERT,SYMPRP,NCONS,KONS,
     1          LIW,IW,LRW,RW)
C---- Tecplot
      open(nfioil,status='unknown')
      CALL XTPOUT(nfioil,NPROP,
     1     SYMPRP,NVERT,NPROP,PROP,ICORD,NDIM,N2DIM,NCELL,
     1     IVERT)
      close(nfioil,status='keep')
      NCEL1 = 1
      NCEL2 = NCELL
      ENDIF
      ENDIF
C***********************************************************************
C
C     Generate output of chemical and physical quantities
C
C***********************************************************************
 7000 CONTINUE
C***********************************************************************
C     write data file fort.94
C***********************************************************************
      OPEN(NFITAB,STATUS='UNKNOWN')
      NCEL1 = NCELL
      NCEL2 = NCELL
      CALL IOGPAR(1,NFITAB,NPROP,NCEL1,NCEL2,NVERT,NDIM,N2DIM,
     1     NSPEC,NREAC,IY,IVERT,ICORD,IRET,PROP,SYMPRP,
     2     CMAT,MIX,NC)
      CLOSE(NFITAB,STATUS='KEEP')
C***********************************************************************
C     calculate transport properties
C***********************************************************************
      IF(ICALTR.EQ.0) GOTO 8000
      call PREPRO(ITRM,NDIM,NEQ,NC,NZEV,NSPEC,LRV,
     1       NPROP,PROP,
     1      NVERT,NCELL,ICORD,IRET,IY,IVERT,N2DIM,SYMPRP,
     1      CMAT,ERVROW,ERVCOL,RPAR,IPAR,LRW,RW,LIW,IW,IERR)
c***********************************************************************
C     Error exits
C***********************************************************************
 8000 RETURN
c***********************************************************************
C     Error exits
C***********************************************************************
 9200 CONTINUE
      WRITE(NFIOUT,9201)
 9201 FORMAT(' GRIGEN failed to compute values for origin ')
      GOTO 9999
 9250 CONTINUE
      WRITE(NFIOUT,9251)
 9251 FORMAT(' MASRES failed to compute chemical source terms ')
      GOTO 9999
 9999 CONTINUE
      WRITE(NFIOUT,9998)
 9998 FORMAT(3(' +++++ Error in NEWGEN ++++',/))
C***********************************************************************
C     END OF NEWGEN
C***********************************************************************
      END
      SUBROUTINE CHKDOM(INFO,NEQ,SMF1,SMF2,IRCH,VHIT,TAUMIN)
C***********************************************************************
C
C     check if vertex is in the allowed domain
C
C     IRCH     = 0   neither SMF1 nor SMF2 have components < BOUY-ATOL
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      COMMON/BLIMIT/XLIMIT(MNLIM*(MNSPE+2)),ELIMIT(2,MNLIM  ),NLIM
      DIMENSION SMF1(NEQ),SMF2(NEQ)
      PARAMETER (ZERO=0.0D0,ONE=1.0D0,XLARGE=1.D30)
C***********************************************************************
C     Initialize Bounds
C***********************************************************************
      IRCH = 0
C***********************************************************************
C     Initialize TAUMIN
C***********************************************************************
      TAUMIN = ONE
C***********************************************************************
C     Check whether SMF1 has components outside threshhold
C***********************************************************************
      DO 120 I=1,NLIM
        H = DDOT(NEQ,XLIMIT(I),NLIM,SMF1,1)
        IF(H.LT.ELIMIT(1,I)) THEN
          IRCH = -I
          VHIT = ELIMIT(2,I)
!           write(*,*)'H IN EVBRES',H
!           write(*,*)'ELIMIT IN EVBRES',ELIMIT(1,I)
!           write(*,*)'IRCH',-IRCH
        ENDIF
  120 CONTINUE
      IF(INFO.EQ.0) THEN
        IRCH = - IRCH
        RETURN
      ENDIF
C***********************************************************************
C     Check whether SMF2 has components outside threshhold
C***********************************************************************
      DO 220 I=1,NLIM
        H = DDOT(NEQ,XLIMIT(I),NLIM,SMF2,1)
        IF(H.LT.ELIMIT(1,I)) GOTO 230
  220 CONTINUE
      IRCH = 0
      RETURN
  230 CONTINUE
C***********************************************************************
C     Check which component crosses a hyperplane first
C***********************************************************************
      TAUMIN = XLARGE
      DO 340 I = 1,NLIM
        H1= DDOT(NEQ,XLIMIT(I),NLIM,SMF1,1)
        H2= DDOT(NEQ,XLIMIT(I),NLIM,SMF2,1)
C----   TAULO denote values of TAU, for which bounds are hit
          DDD = H2 - H1
        IF (H2.EQ.H1) DDD = 1.D-20
        TAULO = (ELIMIT(1,I) - H1 ) / DDD
c-----  lower target hit
        IF(TAULO.GE.ZERO.AND.TAULO.LT.ONE) THEN
          IF(TAULO.LT.TAUMIN) THEN
            TAUMIN = TAULO
            IRCH   = I
            VHIT   = ELIMIT(2,I)
C           write(*,*)'TAULO',TAULO
          ENDIF
        ENDIF
  340 CONTINUE
      CALL DSCAL(NEQ,TAUMIN,SMF2,1)
      HELP = ONE - TAUMIN
      CALL DAXPY(NEQ,HELP,SMF1,1,SMF2,1)
      RETURN
C***********************************************************************
C
C***********************************************************************
      END
c
      SUBROUTINE XTPOUT(NFIPLO,NPLP,SYMPRP,NVERT,NPROP,PROP,
     1                  ICORD,NDIM,N2DIM,NCELL,IVERT)
C***********************************************************************
C
C	tecplot output ----  data file
C	nur bis 3D moeglich!!!
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      PARAMETER(LOSYM=8)
      DIMENSION PROP(NPROP,NVERT),IVERT(N2DIM,NCELL),ICORD(NDIM,NVERT)
      CHARACTER(LEN=LOSYM) XABSYM(ndimm),TABCVS(ndimm)
      CHARACTER(LEN=*)  SYMPRP(NPLP)
C***********************************************************************
c     GNUPLOT output
C***********************************************************************
      if(1.eq.0) then
         NFIGNU = 84
         WRITE(NFIGNU,838) ('Tabco   ',i=1,NDIM),(SYMPRP(I),I=1,NPLP),
     1        ('Rflag   ',j=1,4)
         NFIGNU = 85
         do I=1,NVERT
            WRITE(NFIGNU,839) (FLOAT(ICORD(J,I)),J=1,NDIM),
     1           (PROP(K,I),K=1,NPLP)
         enddo
 838  FORMAT(93(A,1X))
 839  FORMAT(93(1PE10.3,1X))
      endif
C***********************************************************************
c     TECPLOT output
C***********************************************************************
      do i=1,ndim
        write(XABSYM(i),840)i
        write(TABCVS(i),841)i
      enddo
 840  FORMAT('Tabco',I3)
 841  FORMAT('Tabcv',I3)
c
      WRITE(NFIPLO,837) (XABSYM(i),i=1,NDIM),
     1     (SYMPRP(I),I=1,NPLP)
      IF(NDIM.EQ.1) WRITE(NFIPLO,842) NVERT,NCELL
      IF(NDIM.EQ.2) WRITE(NFIPLO,842) NVERT,NCELL
      IF(NDIM.EQ.3) WRITE(NFIPLO,843) NVERT,NCELL
      IF(NDIM.GE.4) WRITE(NFIPLO,844) NVERT,NCELL
      DO 20 I=1,NVERT
         WRITE(NFIPLO,834) (FLOAT(ICORD(J,I)),J=1,NDIM),
     1        (PROP(K,I),K=1,NPLP)
 20   CONTINUE
      if(NDIM.LE.3) then
         DO 30 I=1,NCELL
            IF(NDIM.EQ.1) THEN
      WRITE(NFIPLO,810) IVERT(1,I),IVERT(2,I),IVERT(2,I),IVERT(1,I)
            ELSE IF(NDIM.EQ.2) THEN
      WRITE(NFIPLO,810) IVERT(1,I),IVERT(2,I),IVERT(4,I),IVERT(3,I)
            ELSE IF(NDIM.EQ.3) THEN
      WRITE(NFIPLO,810) IVERT(1,I),IVERT(2,I),IVERT(4,I),IVERT(3,I),
     1                  IVERT(5,I),IVERT(6,I),IVERT(8,I),IVERT(7,I)
            ELSE
            WRITE(*,*) ' No Tecplot output for ndim>3 '
              STOP
            ENDIF
   30    CONTINUE
      else
       write(6,*) ' no tecplot ouput possible -- ndim = ',NDIM
      endif
  810 FORMAT(10I8)
  820 FORMAT(6(1PE13.6))
  834 FORMAT(7(1PE21.14,1X))
  837 FORMAT('VARIABLES =',/,7('"',A,'"',:','))
  842 FORMAT('ZONE N=',I5,', E=',I5,', ET= QUADRILATERAL, F=FEPOINT')
  843 FORMAT('ZONE N=',I5,', E=',I5,', ET= BRICK, F=FEPOINT')
  844 FORMAT('ZONE N=',I6,', E=',I6)
      RETURN
C***********************************************************************
C     End of -XTPOUT-
C***********************************************************************
      END
c
      SUBROUTINE IOGPAR(ITASK,NFIGR1,NPROP,NCEL1,NCEL2,NVERT,NDIM,N2DIM,
     1                     NSPEC,NREAC,IY,IVERT,ICORD,IRET,PROP,SYMPRP,
     1                     CMAT,mix,NC)
C***********************************************************************
C
C  THIS ROUTINE WRITES AND READS THE GRID DATA
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION  IY(NDIM,*),IVERT(N2DIM,*),ICORD(NDIM,*),IRET(*),
     1           PROP(NPROP,*),CMAT(*)
      CHARACTER*4 STRING
      character(LEN=*) SYMPRP(nprop)
      DATA MSGFIL/6/
      common/ILDMOP/IPARA,ISTAOP,MANPAR,NOFORM,ANAFUE,INMAN,lasloo,
     1              ICALTR,MASENS,IUPVER,NTRANS
C
      IERIOG = 0
      IF(ITASK.GT.0) THEN
c***********************************************************************
c     Information about the use of fix/generalized coordinates         *
c***********************************************************************
      NEQ = NSPEC+2
      REWIND NFIGR1
      write(nfigr1,830) '>THE'
      write(nfigr1,840) ipara
      write(NFIGR1,830) '<   '
      write(nfigr1,830) '>CMA'
      write(nfigr1,820)(CMAT(i),i=1,nc*neq)
      write(NFIGR1,830) '<   '
      write(nfigr1,830) '>MIX'
      write(nfigr1,840) mix
      write(NFIGR1,830) '<   '
      write(nfigr1,830) '>RED'
      write(nfigr1,818) nprop
      WRITE(NFIGR1,850) (SYMPRP(I),I=1,nprop)
      write(NFIGR1,830) '<   '
      WRITE(NFIGR1,830) '>TAB'
      WRITE(NFIGR1,810) NCEL1,NCEL2,NVERT,NDIM,NPROP,
     1                  NSPEC,NREAC,0,NC
      WRITE(NFIGR1,810) ((IY(J,I),   J=1,NDIM ),I=1,NCEL2)
      WRITE(NFIGR1,810) ((IVERT(J,I),J=1,N2DIM),I=1,NCEL2)
      WRITE(NFIGR1,810) ((ICORD(J,I),J=1,NDIM), I=1,NVERT)
      WRITE(NFIGR1,810) (IRET(I),I=1,NVERT)
      WRITE(NFIGR1,820) ((PROP(J,I), J=1,NPROP),   I=1,NVERT)
      WRITE(NFIGR1,830) '<   '
      ELSE
      REWIND NFIGR1
      WRITE(*,*)'READ THE'
  130 read(nfigr1,830,end=910,err=912) string
      if(string.ne.'>THE') goto 130
C- read only dummy
      read(nfigr1,840) iparad
      REWIND NFIGR1
      WRITE(*,*)'READ CMA'
  150 read(nfigr1,830) string
      if(string.ne.'>CMA') goto 150
        read(nfigr1,820) (CMAT(i),i=1,NC*NEQ)
      REWIND NFIGR1
      WRITE(*,*)'READ MIX'
  120 read(nfigr1,830) string
      if(string.ne.'>MIX') goto 120
      read(nfigr1,840) mix
      REWIND NFIGR1
      WRITE(*,*)'READ RED'
  110 read(nfigr1,830,end=910,err=912) string
      if(string.ne.'>RED') goto 110
      read(nfigr1,878) nprop
      read(nfigr1,850) (SYMPRP(I),I=1,nprop)
      REWIND NFIGR1
      WRITE(*,*)'READ TAB'
  140 READ(NFIGR1,830,END=910,ERR=912) STRING
      IF(STRING.NE.'>TAB') GOTO 140
      READ(NFIGR1,810) NCEL1,NCEL2,NVERT,NDIM,NPROP,
     1                 NSPEC,NREAC
      READ(NFIGR1,810) ((IY(J,I),   J=1,NDIM ),I=1,NCEL2)
      READ(NFIGR1,810) ((IVERT(J,I),J=1,N2DIM),I=1,NCEL2)
      READ(NFIGR1,810) ((ICORD(J,I),J=1,NDIM), I=1,NVERT)
      READ(NFIGR1,810) (IRET(I),I=1,NVERT)
      READ(NFIGR1,820) ((PROP(J,I), J=1,NPROP),   I=1,NVERT)
      ENDIF
  810 FORMAT(10I8)
C 820 FORMAT(6(1PE13.6))
  820 FORMAT(6(1PE20.13))
  818 FORMAT(I8,' Properties')
  878 FORMAT(I8)
  830 FORMAT(20A4)
  840 FORMAT(10I1)
C 850 FORMAT(6('"',A,'"',5X))
  850 FORMAT(6(A,5X))
      RETURN
C***********************************************************************
C     Error exits
C***********************************************************************
  910 CONTINUE
      WRITE(MSGFIL,911)
  911 FORMAT(' End of file during input in IOGPAR')
  912 CONTINUE
      WRITE(MSGFIL,913)
  913 FORMAT(' Error during read in IOGPAR')
      NCEL1 = 0
      NCEL2 = 0
      NCVERT= 0
      RETURN
C***********************************************************************
C     End of -IOGPAR-
C***********************************************************************
      END
      SUBROUTINE PLTM(NUCALL,SINTER,POSTSC,NDIM,
     1   NCELL,NVERT,NPLP,HPROP,NPROP,IVERT,SYMPRP,NCONS,KONS,
     1          LIW,IW,LRW,RW)
C***********************************************************************
C     MINIGRAFIK
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      CHARACTER(LEN=*)  SYMPRP(*)
      CHARACTER*1 SINTER,POSTSC
      DIMENSION IVERT(*),HPROP(*),KONS(*),
     1          YMIN(NPROP),YMAX(NPROP),RW(LRW),IW(LIW)
      SAVE
        write(*,*) '2PPPPPPPPPPPPPPP'
C***********************************************************************
C
C***********************************************************************
      YMIN(1:NPROP) = 0.D0
      YMAX(1:NPROP) = 0.D0
      LSYM=LEN(SYMPRP(1))
C=======================================================================
C     MiniGraphik
C-----------------------------------------------------------------------
C     This part is by default DISABLED.
C     To enable: "./configure --with-mg" and then "make all"
C     A.Koksharov
C-----------------------------------------------------------------------
#ifdef MINI
!      IF(NDIM.EQ.2) THEN
!       CALL MG3DPL(NUCALL,'N',SINTER,'N',POSTSC,8,' ILDM   ',
!     1   NCELL,NVERT,NPLP,HPROP,NPROP,2,IVERT,4,SYMPRP,KONS,
!     1          YMIN,YMAX)
!      ELSE IF(NDIM.EQ.1) THEN
!      XMIN = 0.0D0
!      XMAX = 0.0D0
!      NXCOR  = KONS(1)
!C     NXCOR = 0
!      NCONST = NCONS -1
!      CALL MG2DX(NUCALL,NOPL,
!     1          'N',SINTER,'S',POSTSC,10,'  ILDM    ',
!     1  NCELL,NVERT,IVERT,HPROP,NPROP,2,
!     1    SYMPRP,LSYM,NXCOR,NCONST,KONS(2),
!     1          YMIN,YMAX,LIW,IW,LRW,RW)
!      KONS(1) = NXCOR
!      ELSE
!      ENDIF
#endif
C-----------------------------------------------------------------------
C     MiniGraphik
C=======================================================================
      RETURN
C***********************************************************************
C     End of -PLTM-
C***********************************************************************
      END
      SUBROUTINE TTPOUT(ICA,NFIPLO,NPLP,SYMPRP,NVERT,NPROP,PROP,NDIM)
C***********************************************************************
C     Tecplot output for trajectory result
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      DIMENSION PROP(NPROP,NVERT)
      PARAMETER(LTSYM=8)
      CHARACTER(LEN=*)  SYMPRP(NPLP)
      CHARACTER(LEN=LTSYM)  XABSYM(NDIMM),TABCVS(NDIMM)
C***********************************************************************
c     TECPLOT output
C***********************************************************************
      IF(ICA.EQ.1) THEN
      WRITE(NFIPLO,831)
      DO I=1,NDIM
        WRITE(XABSYM(I),840)I
        WRITE(TABCVS(I),841)I
      ENDDO
      WRITE(NFIPLO,837) (XABSYM(i),i=1,NDIM),
     1     (TABCVS(i),i=1,NDIM),(SYMPRP(I),I=1,NPLP)
      ENDIF
      WRITE(NFIPLO,842) NVERT
      DO I=1,NVERT
         WRITE(NFIPLO,834) (0.0,J=1,2*NDIM),(PROP(K,I),K=1,NPLP)
      ENDDO
      RETURN
  831 FORMAT('TITLE= " Results of trajectory calculations "')
  834 FORMAT(7(1PE10.3,1X))
  837 FORMAT('VARIABLES =',/,7('"',A,'"',:','))
 842  FORMAT('ZONE I=',I4,', J=1, F=POINT')
 840  FORMAT('Tabco',I3)
 841  FORMAT('Tabcv',I3)
C***********************************************************************
C     End of -TTPOUT-
C***********************************************************************
      END
      SUBROUTINE CHKUDO(NEQ,SMF,IRCH)
C***********************************************************************
C
C     check if vertex is in the allowed domain
C
C     IRCH     = 0   SMF inside domain
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      COMMON/BULIMI/ULIMIT(MNLIM*(MNSPE+2)),UELIM(2,MNLIM  ),NULIM
      DIMENSION SMF(NEQ)
      PARAMETER (ZERO=0.0D0,ONE=1.0D0,XLARGE=1.D30)
C***********************************************************************
C     Initialize Bounds
C***********************************************************************
      IRCH = 0
C***********************************************************************
C     Check whether SMF1 has components outside threshhold
C***********************************************************************
CUM
C---- old version  one limit is enough
      DO I=1,NULIM
        H = DDOT(NEQ,ULIMIT(I),NULIM,SMF,1)
        IF(H.LT.UELIM(1,I)) THEN
          IRCH = I
          GOTO 20
        ENDIF
      ENDDO
   20 CONTINUE
      RETURN
C***********************************************************************
C
C***********************************************************************
      END
      SUBROUTINE GETMP(MJOB,NEQ,NDM,NDMD,VV,VVP,PMAA,NDP,SMF,SMFM)
C***********************************************************************
C     STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
C***********************************************************************
C     COMMON BLOCKS
C***********************************************************************
      DOUBLE PRECISION, DIMENSION(NEQ)         :: SMF,SMFM
      DOUBLE PRECISION, DIMENSION(NEQ,NDMD)     :: VV
      DOUBLE PRECISION, DIMENSION(NDMD,NEQ)     :: VVP
      DOUBLE PRECISION, DIMENSION(NDP,NEQ)   :: PMAA
      DOUBLE PRECISION, DIMENSION(NDMD,NDMD)      :: VTVI
      DOUBLE PRECISION, DIMENSION(NDMD)          :: RW
      INTEGER         , DIMENSION(NDMD)          :: IW
C***********************************************************************
C     COMMON BLOCKS
C***********************************************************************
      COMMON/BCHIS/EWF(MNCON,7),SMFF(MNEQU,7),PAT(2,MNTCO),
     1       MIXFRA,MENFRA,MPRFRA
      COMMON/BMIX/OMEGA,SMFMIX(MNEQU),SMFCHA(MNEQU),VDIR(MNEQU,NDIMM)
      COMMON/BIMIXP/IMIXP
C***********************************************************************
C     Data statements
C***********************************************************************
      PARAMETER (NFIOUT=6)
      PARAMETER (ZERO=0.0D0,ONE=1.0D0)
C***********************************************************************
C     Use mixing point if given
C***********************************************************************
       IF(IMIXP.NE.0) THEN
         SMFM(1:NEQ) = SMFMIX(1:NEQ)
C        write(*,*) 'mixing comp given'
         RETURN
       ELSE IF(MIXFRA.EQ.0) THEN
        WRITE(*,*) ' mixing point must be given for MIXFRA=0'
        STOP
       ELSE
       ENDIF
C***********************************************************************
C
C     Fisrt call, setup matrices
C
C***********************************************************************
       IF(NDM.EQ.0) THEN
         SMFM(1:NEQ) = SMFF(1:NEQ,1)
         RETURN
       ENDIF
C***********************************************************************
C
C     Fisrt call, setup matrices
C
C***********************************************************************
      IF(MJOB.EQ.0) THEN
C***********************************************************************
C     Calculate VV
C***********************************************************************
         DO I=1,NDM
           VV(1:NEQ,I) = SMFF(1:NEQ,1+I) - SMFF(1:NEQ,1)
         ENDDO
C***********************************************************************
C     Calculate VVP
C***********************************************************************
      VTVI  = MATMUL(TRANSPOSE(VV),VV)
      CALL DGEINV(NDM,VTVI,RW,IW,IERR)
      IF(IERR.NE.0) GOTO 900
        VVP    = MATMUL(VTVI,TRANSPOSE(VV))
      ENDIF
C***********************************************************************
C
C     Calculate mixing point
C
C***********************************************************************
C     SMFM(1:NEQ) = SMFF(1:NEQ,1) +
C    1        MATMUL(VV,MATMUL(VVP,SMF(1:NEQ)-SMFF(1:NEQ,1)))
      SMFM(1:NEQ) = SMFF(1:NEQ,1) +
     1    MATMUL(VV,MATMUL(PMAA(1:NDM,1:NEQ),SMF(1:NEQ)-SMFF(1:NEQ,1)))
C***********************************************************************
C     Regular exit
C***********************************************************************
      RETURN
C***********************************************************************
C     End of
C***********************************************************************
  900 CONTINUE
      WRITE(NFIOUT,*) ' ERROR in GETMP '
      STOP
C***********************************************************************
C     End of
C***********************************************************************
      END
      SUBROUTINE INIVDI(INFO,NDIM,NDM,NEQ,NZEV,NC,SMF,SMFM,VDIN,
     1      NVERT,INDVER,INDVV,ICORD,IRET,NPROP,PROP,ISMF,NFIOUT,IERR)
C***********************************************************************
C     get mixing information
C         INDVER index of vertex which has to be generated
C         INDVV  vertex for starting guess
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      DOUBLE PRECISION, DIMENSION(NEQ)                :: SMF
      DIMENSION IRET(*),ICORD(NDIM,*)
      DIMENSION PROP(NPROP,*)
C***********************************************************************
C     Work arrays
C***********************************************************************
      DOUBLE PRECISION, DIMENSION(NDIM*NEQ)           :: VV,VVP
      DOUBLE PRECISION, DIMENSION(NEQ,5**NDIM)        :: VDITEM
      DOUBLE PRECISION, DIMENSION(NEQ)                :: SMFM
      DOUBLE PRECISION, DIMENSION(5**NDIM,5**NDIM)   :: QQQ,VVV
      DOUBLE PRECISION, DIMENSION(5**NDIM)           :: XLAS
      DOUBLE PRECISION, DIMENSION(8*5**NDIM)         :: RW
      INTEGER          , DIMENSION(5*5**NDIM)        :: IW
      INTEGER          , DIMENSION(5**NDIM)          :: IFF
C     INTEGER          , DIMENSION(2,NDIM)           :: NEIGH
C***********************************************************************
C     COMMON BLOCKS
C***********************************************************************
      COMMON/BMIX/OMEGA,SMFMIX(MNEQU),SMFCHA(MNEQU),VDIR(MNEQU,NDIMM)
      COMMON/BTAUCO/TAUCO(MNCON*(2+MNSPE))
C***********************************************************************
C     Initialization
C***********************************************************************
      IERR = 0
      LRW = 8*5**NDIM
      N3DIM = 3**NDIM
      N5DIM = 5**NDIM
C***********************************************************************
C
C     determine the mixing projection
C
C***********************************************************************
      NDIDUM = MAX(1,NDM)
      CALL GETMP(0,NEQ,NDM,NDIDUM,VV,VVP,TAUCO(NZEV-NDM+1),NC,SMF,SMFM)
      IF(INFO.EQ.0) RETURN
      SMFMIX(1:NEQ) = SMFM(1:NEQ)
         VDIR(1:NEQ,1) = SMF(1:NEQ)  - SMFM(1:NEQ)
         VDINOR = DOT_PRODUCT(VDIR(1:NEQ,1),VDIR(1:NEQ,1))
         VDIN   = SQRT(VDIN)
      write(*,*) 'llllllll',smfm(1:neq),smf(1:neq)
      write(*,*) ' vddddd',vdir(1:NEQ,1)
      IFAL  = -1
      ITRYA = 0
  221 IFAL = IFAL +1
      IF(NDIM.EQ.1) GOTO 7676
C***********************************************************************
C     Check for neighbouring vertices
C***********************************************************************
      NOMT  = 0
      WRITE(*,*) 'icord ',ICORD(1:NDIM,INDVER)
      DO I=1,NVERT
        IRTT = IRET(I)
        IF(I.NE.INDVV) THEN
          IF(IRTT.EQ.0.OR.(IRTT.LE.-1.AND.ITRYA.NE.0)) THEN
          IADT  = 0
          DO J=1,NDIM
            IAD= ABS(ICORD(J,I)-ICORD(J,INDVV))
CUU
CC          IF(ABS(ICORD(J,I)-ICORD(J,INDVV)).GT.1) GOTO 200
CC          IF(ABS(ICORD(J,I)-ICORD(J,INDVER)).GT.1) GOTO 200
            IF(ABS(ICORD(J,I)-ICORD(J,INDVER)).GT.2) GOTO 200
            IADT = IADT +IAD
        ENDDO
        WRITE(*,*) 'found', I,'icord ',ICORD(1:NDIM,I)
C---- copy vertex
        NOMT  = NOMT + 1
        VDITEM(1:NEQ,NOMT) = PROP(ISMF:ISMF-1+NEQ,I)  - SMF(1:NEQ)
      ENDIF
      ENDIF
  200 CONTINUE
      ENDDO
C***********************************************************************
C     New
C***********************************************************************
C     NOMT = 0
C     IMES = 0
C     call CHKNEI(INDVER,nvert,ndim,ICORD,NEIGH,ierr,IRET,IMES)
C     write(*,*) 'icord   ',ICORD(1:NDIM,INDVER)
C     write(*,*) 'llllllll',NEIGH(1,1:NDIM)
C     write(*,*) 'rrrrrrrr',NEIGH(2,1:NDIM)
C     DO I=1,NDIM
C     IVLE = NEIGH(1,I)
C     IRLE = IRET(IVLE)
C     IVRI = NEIGH(2,I)
C     IRRI = IRET(IVRI)
C     write(*,*) 'iiiiiii',IVLE,IRLE
C     write(*,*) 'jjjjjii',IVRI,IRRI
C     ITL  = 0
C     ITR  = 0
C     IF(IVLE.NE.0) THEN
C     write(*,*) 'icordl  ',ICORD(1:NDIM,IVLE)
C       IF(IRLE.EQ.0.OR.(IRLE.LE.-1.AND.ITRYA.NE.0)) ITL = IVLE
C       IF(IRLE.EQ.0) ITL = IVLE
C     ENDIF
C     IF(IVRI.NE.0) THEN
C     write(*,*) 'icordr  ',ICORD(1:NDIM,IVRI)
C       IF(IRRI.EQ.0.OR.(IRRI.LE.-1.AND.ITRYA.NE.0)) ITR = IVRI
C       IF(IRRI.EQ.0) ITR = IVRI
C     ENDIF
C     write(*,*) 'kkkkkkkk',ITL,ITR
C     IF(ITL.NE.0.AND.ITR.NE.0) THEN
C       NOMT = NOMT + 1
C       VDITEM(1:NEQ,NOMT) = (PROP(ISMF:ISMF-1+NEQ,ITL) -
C    1                        PROP(ISMF:ISMF-1+NEQ,ITR) )
C       NOMT = NOMT + 1
C       VDITEM(1:NEQ,NOMT) = (PROP(ISMF:ISMF-1+NEQ,ITL) - SMF(1:NEQ))
C       NOMT = NOMT + 1
C       VDITEM(1:NEQ,NOMT) = (PROP(ISMF:ISMF-1+NEQ,ITR) - SMF(1:NEQ))
C       write(*,*) 'B used',ITL,ITR
C     ELSE IF(ITL.NE.0) THEN
C       NOMT = NOMT + 1
C       VDITEM(1:NEQ,NOMT) = PROP(ISMF:ISMF-1+NEQ,ITL)  - SMF(1:NEQ)
C       write(*,*) 'B used',ITL
C     ELSE IF(ITR.NE.0) THEN
C       NOMT = NOMT + 1
C       VDITEM(1:NEQ,NOMT) = PROP(ISMF:ISMF-1+NEQ,ITR)  - SMF(1:NEQ)
C       write(*,*) 'C used',ITR
C     ELSE
C     ENDIF
C     ENDDO
C***********************************************************************
C     If(NOMT onto mixing vector
C***********************************************************************
      IF(NOMT.LT.1) THEN
         ITRYA = 1
         IF(IFAL.EQ.0) GOTO 221
         WRITE(*,*) 'errrr in evan',INFOX,MFO,NOMT
         STOP
      ENDIF
C***********************************************************************
C     Project onto mixing vector
C***********************************************************************
      TTTMIN = 1.D0
      DO I=1,NOMT
        TTT   = DOT_PRODUCT(VDIR(1:NEQ,1),VDITEM(1:NEQ,I)) /VDINOR
        TTT2  = DOT_PRODUCT(VDITEM(1:NEQ,I),VDITEM(1:NEQ,I))
        VDITEM(1:NEQ,I) = VDITEM(1:NEQ,I) - TTT * VDIR(1:NEQ,1)
        COSANG = TTT*VDINOR /SQRT(TTT2*VDINOR)
        TTTMIN = MIN(TTTMIN,ABS(COSANG))
      ENDDO
C***********************************************************************
C     check for pathological cases
C***********************************************************************
      IF(TTTMIN.GT.0.999999999) THEN
C     IF(TTTMIN.GT.0.995      ) THEN
        WRITE(NFIOUT,*)' linear prolongation ill conditioned '
        Write(*,*) 'NOMTLLLL',NOMT
          IERR = -6
      ENDIF
C---- VDITEM cantains now the orthogonal projections onto the mixing v.
C***********************************************************************
C     Calculate best approximation
C***********************************************************************
      QQQ(1:NOMT,1:NOMT) =
     1      MATMUL(TRANSPOSE(VDITEM(1:NEQ,1:NOMT)),VDITEM(1:NEQ,1:NOMT))
C***********************************************************************
C     Calculate eigenvalues and eigenvectors of the symmetric matrix
C***********************************************************************
      ABSTOL = 2.0D0 * DLAMCH('S')
      CALL DSYEVX('V','A','U',NOMT,QQQ,N5DIM,DD1,DD2, IL,IU,
     $            ABSTOL,MFO,XLAS,VVV,N5DIM,RW,LRW,IW,IFF,INFOX)
      write(*,*) IFF(1:NOMT),MFO,'XLAS',XLAS(1:NOMT)
      IF(INFOX.NE.0.OR.MFO.NE.NOMT.OR.1.EQ.0) THEN
         ITRYA = 1
         IF(IFAL.EQ.0) GOTO 221
         WRITE(*,*) 'errrr in evan',INFOX,MFO,NOMT
         STOP
      ENDIF
       VDIR(1:NEQ,2:NDIM) =
     1     MATMUL(VDITEM(1:NEQ,1:NOMT),VVV(1:NOMT,NOMT-NDIM+2:NOMT))
C      VDIR(1:NEQ,2:NDIM) =VDITEM(1:NEQ,1:NDIM-1)
C      VDIR(1:NEQ,2:NDIM) =VDITEM(1:NEQ,NOMT-NDIM+2:NOMT)
 7676 CONTINUE
C***********************************************************************
C
C***********************************************************************
      RETURN
      END
      SUBROUTINE CALIPM(INFO,
     1   ILOW,NDIM,NVERT,NPROP,PROP,IRET,NEQ,NC,NZEV,MIXFRA,
     1   LRV,RV,
     1   CMAT,TAUCO,ERVCOL,ERVROW,QEVB,LIW,IW,LRW,RW,IPAR,RPAR,NFIOUT)
C***********************************************************************
C
C
C     ILOW              Input, Integer: Index of vertex in PROP
C     NVERT             Input, Integer: Number of vertices
C     NDIM              Input, Integer: Dimension of Table
C     NPROP             Input, Integer: Number of properties in PROP
C     NEQ               Input, Integer: Dimension of the equation system
C     NC                Input, Integer: Number of controling variables
C     ERVROW(NEQ,NEQ)   Input, Double : transformation matrix
C     ERVCOL(NEQ,NEQ)   Input, Double : transformation matrix
C     PROP(NPROP,NVERT) I/O  Property field
C     MIXFRA            Input, Integer: number of streams -1
C     This block handles the manifold points obtained from
C     an old solution or calculates the manifold point for the
C     initial point on the manifold
C***********************************************************************
!      USE GQLTOOLS
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C***********************************************************************
C     Local arrays
C***********************************************************************
      DOUBLE PRECISION, DIMENSION(NPROP,NVERT)   :: PROP
      INTEGER,          DIMENSION(NVERT)         :: IRET
      DOUBLE PRECISION, DIMENSION(NC,NEQ)        :: TAUCO,CMAT
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)       :: ERVROW,ERVCOL,
     1                                              QEVB
      DOUBLE PRECISION, DIMENSION(LRW)           :: RW
      INTEGER,          DIMENSION(LIW)           :: IW
      DOUBLE PRECISION, DIMENSION(LRV)           :: RV
      DOUBLE PRECISION, DIMENSION(*)             :: RPAR
      INTEGER,          DIMENSION(*)             :: IPAR
      PARAMETER (ZERO=0.D0)
C***********************************************************************
C     Local arrays
C***********************************************************************
      DOUBLE PRECISION, DIMENSION(NEQ)           :: SMF,SMFP,AR1,AI1,
     1                                              PREDUM
      DOUBLE PRECISION, DIMENSION(NC)            :: CVCEN,CVCENP
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)       :: XTRDIM1,XTCDIM1,
     1                                              GQL,QCAL
      DOUBLE PRECISION, DIMENSION(NEQ,MIXFRA)    :: VV
      DOUBLE PRECISION, DIMENSION(MIXFRA,NEQ)    :: VVP
C***********************************************************************
C     Local arrays
C***********************************************************************
      COMMON/ILDMPA/IMAILD,ILMILD,IOILDM
      COMMON/BIGENI/IGENI,LNF
      COMMON/BICHKD/ICHKD
      COMMON/BIMIXP/IMIXP
      COMMON/BLOCOR/IIRAT,IISMF,IISMFP,IIRV,IIPRO,IIPNU,IILAM,NPROOT
C***********************************************************************
C     Automatic Parameterization
C***********************************************************************
      LOGICAL INITC
C***********************************************************************
C     Automatic Parameterization
C***********************************************************************
      WRITE(NFIOUT,*) ' Point number',ILOW
      INITC= .FALSE.
      IF(INFO.EQ.0) INITC = .TRUE.
C***********************************************************************
C     Get state from PROP
C***********************************************************************
      SMF(1:NEQ) = PROP(IISMF:IISMF-1+NEQ,ILOW)
C***********************************************************************
C     Automatic Parameterization
C***********************************************************************
      CALL AUTPAR(0,NEQ,NZEV,SMF,ERVROW,ERVCOL,
     1     NC,CMAT,TAUCO,LIW,IW,LRW,RW,IPAR,RPAR,NFIOUT)
C***********************************************************************
C     Compute and store  points
C***********************************************************************
      CVCEN(1:NC) = MATMUL(TAUCO(1:NC,1:NEQ),SMF(1:NEQ))
C     write(*,*) 'xxcvc',CVCEN(1:NC)
C     write(*,*) 'xxcvc',SMF(1:NEQ)
C***********************************************************************
C     Make element composition consitent with fuel
C***********************************************************************
      NDIDUM = MAX(1,MIXFRA)
      IMIXPO = IMIXP
C-UUU
      IF(MIXFRA.NE.0) IMIXP  = 0
      CALL GETMP(0,NEQ,MIXFRA,NDIDUM,VV,VVP,TAUCO(NZEV-MIXFRA+1,1),NC,
     1      SMF,RW)
      IMIXP = IMIXPO
      CVCEN(1:NZEV) = MATMUL(TAUCO(1:NZEV,1:NEQ),RW(1:NEQ))
C     write(*,*) 'cvc',CVCEN(1:NC)
      BETOLD1  = ZERO
C---- not needed, set to 0
      PREDUM   = ZERO
      NCCC = NC
      IF(INITC) THEN
        IISS = ILMILD
        ILMILD = 0
      ENDIF
      IGENI = 0
      CALL MANPOI(3,NEQ,SMF,SMFP,NCCC,CVCEN,CVCENP,LRV,RV,IRETC,
     1     LIW,IW,LRW,RW,RPAR,IPAR,
     1     NZEV,NDIM,IGENI,ICHKD,IMAILD,ILMILD,IOILDM,
     1     BETOLD1,PREDUM,TAUCO,ERVROW,ERVCOL,QEVB,
     1     AR1,AI1,XTRDIM1,XTCDIM1)
      IF(IRETC.NE.0.AND.ILOW.LE.1) GOTO 9200
      IF(INITC) THEN
        ILMILD = IISS
      ENDIF
C***********************************************************************
C     Store transformation matrix at initial point
C***********************************************************************
      IRET(ILOW) = IRETC
C---- Check point ILOW
      WRITE(NFIOUT,8202)
 8202 FORMAT(' GRIGEN compute grid origin according to CVCEN ')
C***********************************************************************
C
C     Calculate matrix for global quasi linearization or const. matrix
C
C***********************************************************************
      IF(INITC) THEN
        IF(IMAILD.EQ.3) THEN
!        CALL GLOBQL(0,NEQ,NC,SMF,GQL,QCAL,TIME,RPAR,IPAR,IERR)
!          WRITE(*,*) 'Calculation of Approximation! Error Flag = ', IERR
!           QEVB=QCAL
!          CALL STABAN( 0,NEQ,NC,NEQ-NC,QEVB,SMF,TIME,AR1,AI1,
!     1           RPAR,IPAR,IERR )
!        ELSE
!          QEVB=XTRDIM1
!          WRITE(*,*) 'Const. Appr. based on Jac. Mat.! LocJac=', ILMILD
        ENDIF
      ENDIF
C***********************************************************************
C     Store values
C***********************************************************************
      CALL STOVAL(ILOW,NDIM,NC,NEQ,LRV,NPROP,PROP,SMF,SMFP,
     1     RV,AR1,AI1,XTRDIM1,XTCDIM1,CVCEN,CVCENP,INDC,IRET)
      RETURN
C***********************************************************************
C
C     End of loop
C
C***********************************************************************
 9200 CONTINUE
      WRITE(NFIOUT,9201)
 9201 FORMAT(' CALIPM failed to compute values for origin ')
      GOTO 9999
 9999 CONTINUE
      WRITE(NFIOUT,9998)
 9998 FORMAT(3(' +++++ Error in CALIPM ++++',/))
      STOP
      END
      SUBROUTINE CALEVI(IPOI,NEQ,NZEV,NDIM,NC,NPROP,NVERM,PROP,
     1       IRET,LRV,RV,LIW,IW,LRW,RW,IPAR,RPAR,CMAT,ERVROW,ERVCOL)
C***********************************************************************
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C***********************************************************************
C
C***********************************************************************
      DOUBLE PRECISION, DIMENSION(NPROP,NVERM)   :: PROP
      INTEGER,          DIMENSION(*)         :: IRET
      DOUBLE PRECISION, DIMENSION(LRV)           :: RV
      DOUBLE PRECISION, DIMENSION(NC,NEQ)        :: CMAT
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)       :: ERVROW,ERVCOL,QEVB
      INTEGER,          DIMENSION(LIW)           :: IW
      DOUBLE PRECISION, DIMENSION(LRW)           :: RW
      INTEGER,          DIMENSION(*)             :: IPAR
      DOUBLE PRECISION, DIMENSION(*)             :: RPAR
C***********************************************************************
C     Local arrays
C***********************************************************************
      DOUBLE PRECISION, DIMENSION(NEQ)           :: SMF,SMFP,RAD1,
     1                                              AR,AI
      DOUBLE PRECISION, DIMENSION(NC)            :: CVCEN,CVCENP
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)       :: XTR,XTC
C***********************************************************************
C     Common blocks
C***********************************************************************
      COMMON/BLOCOR/IIRAT,IISMF,IISMFP,IIRV,IIPRO,IIPNU,IILAM,NPROOT
C***********************************************************************
C
C***********************************************************************
      IDD1 = 0
      IDD2 = 0
      IDD3 = 0
      IDD4 = 0
      IDD5 = 0
      RDD1 = 0
      RAD1 = 0
C***********************************************************************
C
C***********************************************************************
      SMF(1:NEQ)  =  PROP(IISMF:IISMF-1+NEQ,IPOI)
      NCTT = NEQ
      STORV = RV(15)
C---- this causes only egvbas to be executed and flags to be set
      CALL MANPOI(3,NEQ,SMF,SMFP,NCTT,CVCEN,CVCENP,
     1    LRV,RV,IRET(IPOI),LIW,IW,LRW,RW,RPAR,IPAR,
     1     NZEV,NDIM,IDD4,IDD5,IDD1,IDD2,IDD3,
     1     RDD1,RAD1,CMAT,ERVROW,ERVCOL,QEVB,
     1     AR,AI,XTR,XTC)
      RV(15)=STORV
      CALL STOVAL(IPOI,NDIM,NC,NEQ,LRV,NPROP,PROP,SMF,SMFP,
     1     RV,AR,AI,
     1     XTR,XTC,CVCEN,CVCENP,IDD1,IRET)
C***********************************************************************
C
C***********************************************************************
      RETURN
C***********************************************************************
C
C***********************************************************************
      END

