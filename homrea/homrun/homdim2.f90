!
!    Block for Character length                                        
!
      INTEGER, PARAMETER :: LSYMB=8, MNSPE = 1086, MNELE = 10, &
          MNREA=10000, MNTHB=90, MNXEQU = 5, MNEQU = (MNSPE+MNXEQU), &
          MNSEN = 8500, MNPOI = 1000, MNCRE = 20, MNRREA = 300000, &
          MNIREA=200000, MNRSPE = (34+4*MNSPE)*MNSPE
!
!     BLOCK FOR MULTIZONE MODEL   (Schubert 17.06.05)
!
      INTEGER, PARAMETER :: MNZONE=50
!
!    Block for interface with CFD
!
      INTEGER, PARAMETER :: MNPDE = 5
!
!    Block for IPAR and RPAR
!
      INTEGER, PARAMETER :: MNRPAR = 120001, MNIPAR = 14001
!
!    Block for ILDM table setup
!
!     MNCOV:     maximum number of controlling variables
!     MNTCO:     maximum number of tabulation coordinates
!     MNCON:     maximum number of conserved quantities
!     MNPROP:    maximum number of properties stored in the table
!     MNRG:      length of real array for the tabulation
!     MNIG:      length of integer array for the tabulation
!
!     MNRGO:     length of real array for the tabulation    (old grid)
!     MNIGO:     length of integer array for the tabulation (old grid)
!                these variables can be set to 1 (usually)
!     MNCGV:     maximum number of vertices in the coarse grid
!     MNLIM:     maximum number of domain limiting vectors
!
!     NCELM:     maximum number of cells
!     NVERM:     maximum number of vertices fo the mesh
!
      INTEGER, PARAMETER :: NCELM = 4000, NVERM = 4000

      INTEGER, PARAMETER :: MNCOV = 20, MNTCO = 6, MNCON = 10, &
          MNPROP = 4200, MNCGV = 1000, MNLIM = 100, & 
          MNRG = (MNPROP*NVERM), MNIG = 1000, MNRGO = 100, MNIGO = 100
!
      INTEGER, PARAMETER :: NDIMM= 4, N2DIMM = 2**NDIMM, LTW = 1000, &
          MPROMA = 1000
!
      INTEGER, PARAMETER :: mxpkte = 1, mypkte = 1, mzpkte = 1 , &
          mnpkte = 10
!
!     Aenderung fuer berechnung und ausgabe von chi Mischungsbruch
!
      integer, parameter :: mncri= 5,&
          meisch= 30 ,mpunk= 30 
!    DATA TYPE DECLARATION FOR COMMON BLOCKS
      integer :: LRSPEC, NEPS, NSIG, NHL, NHSW, NETA, NLCOEF, NLCOEM, &
          NLCOED, NDCOEF, NREAC, NM, NRINS, NRTAB, NINERT, LRREAC, &
          NPK, NTEXP, NEXA, NVT4, NVT5, NVT6, NAINF, NBINF, NEINF, &
          NT3, NT1, NT2, NAFA, NBFA, NXZSTO, NRATCO, IREAC, &
          LIREAC, NIREV, NIARRH, NMATLL, NMATLR, NNRVEC, LSMAX, NSENS,&
          NRSENS, NISENS, NPSENS, IRSENS, IISENS, IPSENS, ISENSI
      double precision :: RSPEC, XMOL, XMINV, EMOL, XE, RREAC, SSPV,&
          STOMCOM, HLHH, HINF
!
!    COMMON BLOCKS
!
!
!     BLOCK FOR SPECIES DATA
!
      COMMON/BRSPEC/RSPEC(MNRSPE),LRSPEC,NEPS,NSIG,NHL,NHSW,NETA,&
          NLCOEF,NLCOEM,NLCOED,NDCOEF
      COMMON/BXMOL/XMOL(MNSPE),XMINV(MNSPE),EMOL(MNELE),XE(MNSPE*MNELE)
      COMMON/BSSPV/SSPV(20*MNSPE)

!     BLOCK FOR REACTION MECHANISM DATA
!
      COMMON/BNREAC/NREAC,NM,NRINS,NRTAB,NINERT
      COMMON/BRREAC/RREAC(MNRREA),LRREAC,NPK,NTEXP,NEXA,NVT4,NVT5,NVT6,&
          NAINF,NBINF,NEINF,NT3,NT1,NT2,NAFA,NBFA,NXZSTO,NRATCO
      COMMON/BIREAC/IREAC(MNIREA),LIREAC,NIREV,NIARRH,NMATLL,NMATLR,&
          NNRVEC
      COMMON/STOHMAT/STOMCOM(MNEQU,MNREA)
      COMMON/BSENS/LSMAX,NSENS,NRSENS,NISENS,NPSENS,IRSENS,IISENS,&
          IPSENS,ISENSI(MNSEN)
      COMMON/BHCP/HLHH(7, MNSPE*2), HINF(3, MNSPE)
