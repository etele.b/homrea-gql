C
C
      subroutine interpolate(neq,itheta1,itheta2,Y)
      IMPLICIT DOUBLE PRECISION  (A-H,O-Z)
      double precision, dimension(neq,itheta1*itheta2) ::Y
      double precision, dimension(neq,itheta1,itheta2) ::REDIM
      DOUBLE PRECISION, DIMENSION(NEQ,itheta1)            :: ARR,S1
      DOUBLE PRECISION, DIMENSION(NEQ,itheta2)            :: AR2,S2
      DOUBLE PRECISION, DIMENSION(NEQ,2)                 :: PARMAT
      DOUBLE PRECISION, DIMENSION(itheta1)                :: xnew1
      DOUBLE PRECISION, DIMENSION(itheta2)                :: xnew2
      parmat=0.0d0
      parmat(3,1)=1.0d0
*      parmat(5,1)=-1.0d0
*      parmat(5,2)=1.0d0
*      parmat(4,2)=1.0d0
      do i=1,itheta2
      do j=1,itheta1
      REDIM(1:NEQ,j,i)=Y(1:neq,j+(i-1)*itheta1)
      end do
      end do
      X1=dot_product(parmat(:,1),REDIM(:,1,1))
      Xlast=dot_product(parmat(:,1),REDIM(:,itheta1,1))
      DO L=1,itheta1
      XNEW1(L) = X1 + FLOAT(L-1)/FLOAT(itheta1-1)*(xlast-x1)
      ENDDO
      DO L=1,itheta2
      S1(1:NEQ,1:itheta1) = REDIM(1:NEQ,1:itheta1,L)
        CALL INTGRI(INFO,NEQ,NEQ,itheta1,S1(1,1),itheta1,XNEW1,ARR,
     1     IERR,MSGFIL,PARMAT(:,1))
        REDIM(1:NEQ,1:itheta1,L) = ARR(1:NEQ,1:itheta1)
      ENDDO
*      DO I=1,itheta1
*      S2(1:NEQ,1:itheta2) = REDIM(1:NEQ,I,1:itheta2)
*      IOUTT = 0
*      CALL INTGR2(INFO,NEQ,NEQ,itheta2,S2,itheta2,XNEW2,AR2,IERR,IOUTT,
*     1    MSGFIL,PARMAT(:,2))
*      REDIM(1:NEQ,I,1:itheta2) = AR2(1:NEQ,1:itheta2)
*      ENDDO
      do i=1,itheta2
      do j=1,itheta1
      Y(1:neq,j+(i-1)*itheta1)=REDIM(1:NEQ,j,i)
      end do
      end do
      end subroutine interpolate
C
C
      SUBROUTINE INTGRI(INFO,NEQ,NF,NOLD,FOLD,NNEW,XNEW,FNEW,IERR,
     1  MSGFIL,PARMAT)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *           INTERPOLATION OF ONTO A NEW GRID                *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C      INPUT  :
C
C
C
C***********************************************************************
C     STORAGE ORGANISATION
C***********************************************************************
C***********************************************************************
C     TYPES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION  (A-H,O-Z)
      LOGICAL STOP,SKIP
C***********************************************************************
C     DIMENSIONS
C***********************************************************************
      DOUBLE PRECISION, DIMENSION(NOLD)        :: XOLD,DER1,F1
      DOUBLE PRECISION, DIMENSION(NF,NOLD)     :: FOLD
      DOUBLE PRECISION, DIMENSION(NNEW)        :: XNEW,F2
      DOUBLE PRECISION, DIMENSION(NF,NNEW)     :: FNEW
      DOUBLE PRECISION, DIMENSION(NOLD,2)      :: WK
      DOUBLE PRECISION, DIMENSION(NEQ)         :: PARMAT
      DIMENSION IC(2),VC(2)
C***********************************************************************
C     DATA STAEMENTS
C***********************************************************************
CRASH DATA ZERO/0.0D0/,SWITCH/0.0D0/,DISTOL/3.D-3/
      DATA ZERO/0.0D0/,SWITCH/0.0D0/,DISTOL/1.D-4/
C***********************************************************************
C     INITIALIZE
C***********************************************************************
      NWK = 2*NOLD
      IC(1)=0
      IC(2)=0
C     IC(1)=1
C     VC(1) = 0
C     IC(2)=1
C     VC(2) = 0
C***********************************************************************
C     GET XOLD
C***********************************************************************
      XOLD(1:NOLD) = MATMUL(PARMAT,FOLD(1:NEQ,1:NOLD))
C***********************************************************************
C     THROW AWAY Non-Increasing points
C***********************************************************************
      NOLDA = NOLD
      LAUF = 2
   11 CONTINUE
C     IF(XOLD(LAUF).LE.XOLD(LAUF-1)) THEN
      RELDIS =
     1    ABS((XOLD(LAUF)-XOLD(LAUF-1))/
     1    MAX(ABS(XOLD(LAUF)),ABS(XOLD(LAUF-1))) )
      IF(RELDIS.LE.DISTOL) THEN
      XOLD(LAUF:NOLDA-1) = XOLD(LAUF+1:NOLDA)
      FOLD(1:NF,LAUF:NOLDA-1) = FOLD(1:NF,LAUF+1:NOLDA)
      NOLDA = NOLDA-1
      ELSE
      LAUF = LAUF + 1
      ENDIF
      IF(LAUF.LE.NOLDA) GOTO 11
C8877 format(a,9(1x,1pe9.2))
C      WRITE(*,8877) '=old===',XOLD(1:NOLDA)
C     WRITE(*,8877) '=new===',XNEW(1:NNEW)
C***********************************************************************
C     INTERPOLATION OF FUNCTIONS
C***********************************************************************
      SKIP=.TRUE.
      DO I=1,NF
      F1(1:NOLDA)=FOLD(I,1:NOLDA)
CRASH IF(NOLDA.GT.3) THEN
      IF(0.EQ.1) THEN
        CALL DPCHIC(IC,VC,SWITCH,NOLDA,XOLD,F1,DER1,1,WK,NWK,IERR)
        IF(IERR.LT.0) GOTO 930
        CALL DPCHFE(NOLDA,XOLD,F1,DER1,1,SKIP,NNEW,XNEW,F2,IERR)
        IF(IERR.LT.0) GOTO 930
      ELSE
        CALL DLIFE(NOLDA, XOLD, F1, 1, SKIP, NNEW, XNEW, F2, IERR)
        IF(IERR.LT.0) GOTO 930
      ENDIF
      FNEW(I,1:NNEW)=F2(1:NNEW)
C     IF(I.EQ.4) THEN
C       write(*,*) 'PPPXOLD',DER1(1:NOLDA)
C       write(*,*) 'PPPXOLD',XOLD(1:NOLDA)
C       write(*,*) 'PPPXNEW',XNEW(1:NNEW)
C       write(*,*) 'PPPF1PP',F1(1:NOLDA)
C       write(*,*) 'PPPFNEW',FNEW(I,1:NNew)
C     ENDIF
      ENDDO
C***********************************************************************
C     REGULAR EXIT S
C***********************************************************************
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
C
C---- ERROR IN INTERPOLATION
C
  930 CONTINUE
      WRITE(MSGFIL,931) IERR
  931 FORMAT(1H ,' ERROR IN DPCHIC IERR =',I2,'   CALLED BY MODGRI')
      IF(IERR.EQ.-3) WRITE(MSGFIL,932)
      IF(IERR.EQ.-7) WRITE(MSGFIL,933)
  932 FORMAT(1H ,' X- ARRAY NOT STRICTLY INCREASING  ')
  933 FORMAT(1H ,' NOT ENOUGH STORAGE FOR DPCHIC     ')
      GOTO 999
C
C
C
  999 STOP=.FALSE.
      WRITE(MSGFIL,998)
  998 FORMAT(3(/,1X,'++++++      ERROR IN -INTGRI-      ++++++'))
      STOP
C***********************************************************************
C     END OF -MODGRI-
C***********************************************************************
      END
      SUBROUTINE INTGR2(INFO,NEQ,NF,NOLD,FOLD,NNEW,XNEW,FNEW,IERR,
     1     IOUT,MSGFIL,PARMAT)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *           INTERPOLATION OF ONTO A NEW GRID                *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C      INPUT  :
C
C
C
C***********************************************************************
C     STORAGE ORGANISATION
C***********************************************************************
C***********************************************************************
C     TYPES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION  (A-H,O-Z)
      LOGICAL STOP,SKIP
C***********************************************************************
C     DIMENSIONS
C***********************************************************************
      DOUBLE PRECISION, DIMENSION(NOLD)        :: XOLD,DER1,F1
      DOUBLE PRECISION, DIMENSION(NF,NOLD)     :: FOLD
      DOUBLE PRECISION, DIMENSION(NNEW)        :: XNEW,F2
      DOUBLE PRECISION, DIMENSION(NF,NNEW)     :: FNEW
      DOUBLE PRECISION, DIMENSION(NF)          :: FF
      DOUBLE PRECISION, DIMENSION(NOLD,2)      :: WK
      DOUBLE PRECISION, DIMENSION(NEQ)         :: PARMAT
      DIMENSION IC(2),VC(2)
      INTEGER, DIMENSION(1) :: II1
C***********************************************************************
C     DATA STAEMENTS
C***********************************************************************
      DATA ZERO/0.0D0/,SWITCH/0.0D0/,SMALL/1.D-6/
C***********************************************************************
C     INITIALIZE
C***********************************************************************
      NWK = 2*NOLD
      IC(1)=0
      IC(2)=0
C     IC(1)=1
C     VC(1) = 0
C     IC(2)=1
C     VC(2) = 0
C***********************************************************************
C     GET XOLD
C***********************************************************************
      XOLD(1:NOLD) = MATMUL(PARMAT,FOLD(1:NEQ,1:NOLD))
C     write(*,*) 'qqq',XOLD(1:NOLD)
      DO I=1,NOLD-1
      II1 = MINLOC(XOLD(I:NOLD))
      ISW = II1(1) + I -1
c     write(*,*) ISW,'qqq',XOLD(1:NOLD)
      TD        = XOLD(I)
      XOLD(I)   = XOLD(ISW)
      XOLD(ISW) = TD
      FF(1:NF)  = FOLD(1:NF,I)
      FOLD(1:NF,I)  = FOLD(1:NF,ISW)
      FOLD(1:NF,ISW) = FF(1:NF)
      ENDDO
      IF(IOUT.GT.0) write(*,*) 'qqq',XOLD(1:NOLD)
      DELTOL = 1.D-3*(XOLD(NOLD)-XOLD(1))
C***********************************************************************
C     THROW AWAY Non-Increasing points
C***********************************************************************
      NOLDA = NOLD
      LAUF = 2
   11 CONTINUE
C======
c     IF(XOLD(LAUF).LE.XOLD(LAUF-1)+DELTOL) THEN
c     IF(IOUT.GT.0) write(*,*) ' not increasing',XOLD(LAUF-1),XOLD(LAUF)
c     XOLD(LAUF:NOLDA-1) = XOLD(LAUF+1:NOLDA)
c     FOLD(1:NF,LAUF:NOLDA-1) = FOLD(1:NF,LAUF+1:NOLDA)
c     NOLDA = NOLDA-1
c     ELSE
c     LAUF = LAUF + 1
c     ENDIF
C======
      IF(XOLD(LAUF).LE.XOLD(LAUF-1)+DELTOL) THEN
      IF(IOUT.GT.0) write(*,*) ' not increasing',XOLD(LAUF-1),XOLD(LAUF)
      XOLD(LAUF:NOLDA-1) = XOLD(LAUF+1:NOLDA)
      FOLD(1:NF,LAUF:NOLDA-1) = FOLD(1:NF,LAUF+1:NOLDA)
      NOLDA = NOLDA-1
      ELSE
      LAUF = LAUF + 1
      ENDIF
      IF(LAUF.LE.NOLDA) GOTO 11
      IF(IOUT.GT.0) write(*,*) 'vvv',XOLD(1:NOLDA)
C***********************************************************************
C     INTERPOLATION OF FUNCTIONS
C***********************************************************************
      DELT = ABS(XOLD(NOLDA)-XOLD(1))
     1          /MAX(ABS(XOLD(NOLDA)),ABS(XOLD(1)),1.D-6)
      DO L=1,NNEW
      XNEW(L) = XOLD(1) + FLOAT(L-1)/FLOAT(NNEW-1)*(XOLD(NOLDA)-XOLD(1))
      ENDDO
      IF(IOUT.GT.0) write(*,*) 'XNEW',XOLD(NOLDA),XOLD(1),'XNEW',XNEW
      SKIP=.TRUE.
      DO I=1,NF
      IF(DELT.GT.SMALL) THEN
        F1(1:NOLDA)=FOLD(I,1:NOLDA)
C       IF(NOLDA.GT.3) THEN
        IF(1.EQ.0) THEN
          CALL DPCHIC(IC,VC,SWITCH,NOLDA,XOLD,F1,DER1,1,WK,NWK,IERR)
          IF(IERR.LT.0) GOTO 930
          CALL DPCHFE(NOLDA,XOLD,F1,DER1,1,SKIP,NNEW,XNEW,F2,IERR)
          IF(IERR.LT.0) GOTO 930
        ELSE
          CALL DLIFE(NOLDA, XOLD, F1, 1, SKIP, NNEW, XNEW, F2, IERR)
          IF(IERR.LT.0) GOTO 930
        ENDIF
        FNEW(I,1:NNEW)=F2(1:NNEW)
      ELSE
        FNEW(I,1:NNEW)=FOLD(I,1)
      ENDIF
      ENDDO
C***********************************************************************
C     REGULAR EXIT S
C***********************************************************************
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
C
C---- ERROR IN INTERPOLATION
C
  930 CONTINUE
      WRITE(MSGFIL,931) IERR
  931 FORMAT(1H ,' ERROR IN DPCHIC IERR =',I2,'   CALLED BY MODGRI')
      IF(IERR.EQ.-3) WRITE(MSGFIL,932)
      IF(IERR.EQ.-7) WRITE(MSGFIL,933)
  932 FORMAT(1H ,' X- ARRAY NOT STRICTLY INCREASING  ')
  933 FORMAT(1H ,' NOT ENOUGH STORAGE FOR DPCHIC     ')
      GOTO 999
C
C
C
  999 STOP=.FALSE.
      WRITE(MSGFIL,998)
  998 FORMAT(3(/,1X,'++++++      ERROR IN -INTGRI-      ++++++'))
      STOP
C***********************************************************************
C     END OF -MODGRI-
C***********************************************************************
      END
      SUBROUTINE DLIFE(N, X, F, INCFD, SKIP, NE, XE, FE, IERR)
C
C-----------------------------------------------------------------------
C
C          DLIFE :  PIECEWISE LINEAR FUNCTION EVALUATOR
C
C     EVALUATES THE LINEAR FUNCTION DEFINED BY  N, X, F, D  AT
C     THE POINTS  XE(J), J=1(1)NE.
C
C     TO PROVIDE COMPATIBILITY WITH DPCHIM AND DPCHIC, INCLUDES AN
C     INCREMENT BETWEEN SUCCESSIVE VALUES OF THE F- AND D-ARRAYS.
C
C-----------------------------------------------------------------------
C
C  CALLING SEQUENCE:
C
C           CALL  DLIFE  (N, X, F,  INCFD, SKIP, NE, XE, FE, IERR)
C
      INTEGER  N, INCFD, NE, IERR
      DOUBLE PRECISION  X(N), F(INCFD,N),  XE(NE), FE(NE)
      LOGICAL  SKIP
C
C   PARAMETERS:
C
C     N -- (INPUT) NUMBER OF DATA POINTS.  (ERROR RETURN IF N.LT.2 .)
C
C     X -- (INPUT) REAL ARRAY OF INDEPENDENT VARIABLE VALUES.  THE
C           ELEMENTS OF X MUST BE STRICTLY INCREASING:
C                X(I-1) .LT. X(I),  I = 2(1)N.
C           (ERROR RETURN IF NOT.)
C
C     F -- (INPUT) REAL ARRAY OF FUNCTION VALUES.  F(1+(I-1)*INCFD) IS
C           THE VALUE CORRESPONDING TO X(I).
C
C
C     INCFD -- (INPUT) INCREMENT BETWEEN SUCCESSIVE VALUES IN F AND D.
C           (ERROR RETURN IF  INCFD.LT.1 .)
C
C     SKIP -- (INPUT/OUTPUT) LOGICAL VARIABLE WHICH SHOULD BE SET TO
C           .TRUE. IF THE USER WISHES TO SKIP CHECKS FOR VALIDITY OF
C           PRECEDING PARAMETERS, OR TO .FALSE. OTHERWISE.
C           THIS WILL SAVE TIME IN CASE THESE CHECKS HAVE ALREADY
C           BEEN PERFORMED (SAY, IN DPCHIM OR DPCHIC).
C           SKIP WILL BE SET TO .TRUE. ON NORMAL RETURN.
C
C     NE -- (INPUT) NUMBER OF EVALUATION POINTS.  (ERROR RETURN IF
C           NE.LT.1 .)
C
C     XE -- (INPUT) REAL ARRAY OF POINTS AT WHICH THE FUNCTION IS TO BE
C           EVALUATED.
C          NOTES:
C           1. THE EVALUATION WILL BE MOST EFFICIENT IF THE ELEMENTS
C              OF XE ARE INCREASING RELATIVE TO X;
C              THAT IS,   XE(J) .GE. X(I)
C              IMPLIES    XE(K) .GE. X(I),  ALL K.GE.J .
C           2. IF ANY OF THE XE ARE OUTSIDE THE INTERVAL @X(1),X(N)@,
C              VALUES ARE EXTRAPOLATED FROM THE NEAREST EXTREME CUBIC,
C              AND A WARNING ERROR IS RETURNED.
C
C     FE -- (OUTPUT) REAL ARRAY OF VALUES OF THE CUBIC HERMITE FUNCTION
C           DEFINED BY  N, X, F, D  AT THE POINTS  XE.
C
C     IERR -- (OUTPUT) ERROR FLAG.
C           NORMAL RETURN:
C              IERR = 0  (NO ERRORS).
C           WARNING ERROR:
C              IERR.GT.0  MEANS THAT EXTRAPOLATION WAS PERFORMED AT
C                 IERR POINTS.
C           "RECOVERABLE" ERRORS:
C              IERR = -1  IF N.LT.2 .
C              IERR = -2  IF INCFD.LT.1 .
C              IERR = -3  IF THE X-ARRAY IS NOT STRICTLY INCREASING.
C              IERR = -4  IF NE.LT.1 .
C             (THE FE-ARRAY HAS NOT BEEN CHANGED IN ANY OF THESE CASES.)
C               NOTE:  THE ABOVE ERRORS ARE CHECKED IN THE ORDER LISTED,
C                   AND FOLLOWING ARGUMENTS HAVE **NOT** BEEN VALIDATED.
C              IERR = -5  IF AN ERROR HAS OCCURRED IN THE LOWER-LEVEL
C                         ROUTINE DCHFEV.  NB: THIS SHOULD NEVER HAPPEN.
C                         NOTIFY THE AUTHOR **IMMEDIATELY** IF IT DOES.
C
C  OTHER ROUTINES USED:  DLIEV
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C  DECLARE LOCAL VARIABLES.
C
      INTEGER  I, IERC, IR, J, JFIRST, NEXT(2), NJ
C
C  VALIDITY-CHECK ARGUMENTS.
C
      IF (SKIP)  GO TO 5
C
      IF ( N.LT.2 )  GO TO 5001
      IF ( INCFD.LT.1 )  GO TO 5002
      DO 1  I = 2, N
         IF ( X(I).LE.X(I-1) )  GO TO 5003
    1 CONTINUE
C
C  FUNCTION DEFINITION IS OK, GO ON.
C
    5 CONTINUE
      IF ( NE.LT.1 )  GO TO 5004
      IERR = 0
      SKIP = .TRUE.
C
C  LOOP OVER INTERVALS.        (   INTERVAL INDEX IS  IL = IR-1  . )
C                              ( INTERVAL IS X(IL).LE.X.LT.X(IR) . )
      JFIRST = 1
      IR = 2
   10 CONTINUE
C
C     SKIP OUT OF LOOP IF HAVE PROCESSED ALL EVALUATION POINTS.
C
         IF (JFIRST .GT. NE)  GO TO 5000
C
C     LOCATE ALL POINTS IN INTERVAL.
C
         DO 20  J = JFIRST, NE
            IF (XE(J) .GE. X(IR))  GO TO 30
   20    CONTINUE
         J = NE + 1
         GO TO 40
C
C     HAVE LOCATED FIRST POINT BEYOND INTERVAL.
C
   30    CONTINUE
         IF (IR .EQ. N)  J = NE + 1
C
   40    CONTINUE
         NJ = J - JFIRST
C
C     SKIP EVALUATION IF NO POINTS IN INTERVAL.
C
         IF (NJ .EQ. 0)  GO TO 50
C
C     EVALUATE CUBIC AT XE(I),  I = JFIRST (1) J-1 .
C
C       ----------------------------------------------------------------
        CALL DLIEV  (X(IR-1),X(IR),F(1,IR-1),F(1,IR),
     *              NJ, XE(JFIRST), FE(JFIRST), NEXT, IERC)
C       ----------------------------------------------------------------
         IF (IERC .LT. 0)  GO TO 5005
C
         IF (NEXT(2) .GT. 0)  THEN
C           IN THE CURRENT SET OF XE-POINTS, THERE ARE NEXT(2) TO THE
C           RIGHT OF X(IR).
C
            IF (IR .EQ. N)  THEN
C              THESE ARE ACTUALLY EXTRAPOLATION POINTS.
               IERR = IERR + NEXT(2)
            ELSE
C              WE SHOULD NEVER HAVE GOTTEN HERE.
               GO TO 5005
            ENDIF
         ENDIF
C
         IF (NEXT(1) .GT. 0)  THEN
C           IN THE CURRENT SET OF XE-POINTS, THERE ARE NEXT(1) TO THE
C           LEFT OF X(IR-1).
C
            IF (IR .EQ. 2)  THEN
C              THESE ARE ACTUALLY EXTRAPOLATION POINTS.
               IERR = IERR + NEXT(1)
            ELSE
C              XE IS NOT ORDERED RELATIVE TO X, SO MUST ADJUST
C              EVALUATION INTERVAL.
C
C              FIRST, LOCATE FIRST POINT TO LEFT OF X(IR-1).
               JM1=J-1
               DO 44  I = JFIRST, JM1
                  IF (XE(I) .LT. X(IR-1))  GO TO 45
   44          CONTINUE
C              NOTE:  CANNOT DROP THROUGH HERE UNLESS THERE IS AN ERROR
C                     IN DCHFEV.
               GO TO 5005
C
   45          CONTINUE
C              RESET J.  (THIS WILL BE THE NEW JFIRST.)
               J = I
C
C              NOW FIND OUT HOW FAR TO BACK UP IN THE X-ARRAY.
               IRM1=IR-1
               DO 46  I = 1, IRM1
                  IF (XE(J) .LT. X(I)) GO TO 47
   46          CONTINUE
C              NB:  CAN NEVER DROP THROUGH HERE, SINCE XE(J).LT.X(IR-1).
C
   47          CONTINUE
C              AT THIS POINT, EITHER  XE(J) .LT. X(1)
C                 OR      X(I-1) .LE. XE(J) .LT. X(I) .
C              RESET IR, RECOGNIZING THAT IT WILL BE INCREMENTED BEFORE
C              CYCLING.
               IR = MAX0(1, I-1)
            ENDIF
         ENDIF
C
         JFIRST = J
C
C     END OF IR-LOOP.
C
   50 CONTINUE
      IR = IR + 1
      IF (IR .LE. N)  GO TO 10
C
C  NORMAL RETURN.
C
 5000 CONTINUE
      RETURN
C
C  ERROR RETURNS.
C
 5001 CONTINUE
C     N.LT.2 RETURN.
      IERR = -1
      RETURN
C
 5002 CONTINUE
C     INCFD.LT.1 RETURN.
      IERR = -2
      RETURN
C
 5003 CONTINUE
C     X-ARRAY NOT STRICTLY INCREASING.
      IERR = -3
      RETURN
C
 5004 CONTINUE
C     NE.LT.1 RETURN.
      IERR = -4
      RETURN
C
 5005 CONTINUE
C     ERROR RETURN FROM DCHFEV.
C   *** THIS CASE SHOULD NEVER OCCUR ***
      IERR = -5
      RETURN
C------------- LAST LINE OF DPCHFE FOLLOWS -----------------------------
      END
      SUBROUTINE DLIEV (X1,X2,F1,F2,NE, XE, FE, NEXT, IERR)
C
C-----------------------------------------------------------------------
C
C
C
C-----------------------------------------------------------------------
C
C  CALLING SEQUENCE:
C
C           CALL  DLIEV  (X1,X2,F1,F2,NE, XE, FE, NEXT, IERR)
C
      INTEGER  NE, NEXT(2), IERR
      DOUBLE PRECISION  X1, X2, F1, F2,  XE(NE), FE(NE)
C
C   PARAMETERS:
C
C     X1,X2 -- (INPUT) ENDPOINTS OF INTERVAL OF DEFINITION
C           (ERROR RETURN IF  X1.EQ.X2 .)
C
C     F1,F2 -- (INPUT) VALUES OF FUNCTION AT X1 AND X2, RESPECTIVELY.
C
C
C     NE -- (INPUT) NUMBER OF EVALUATION POINTS.  (ERROR RETURN IF
C           NE.LT.1 .)
C
C     XE -- (INPUT) REAL ARRAY OF POINTS AT WHICH THE FUNCTIONS ARE TO
C           BE EVALUATED.  IF ANY OF THE XE ARE OUTSIDE THE INTERVAL
C           @X1,X2@, A WARNING ERROR IS RETURNED.
C
C     FE -- (OUTPUT) REAL ARRAY OF VALUES OF THE FUNCTION
C
C     NEXT -- (OUTPUT) INTEGER ARRAY INDICATING NUMBER OF EXTRAPOLATION
C           POINTS:
C            NEXT(1) = NUMBER OF EVALUATION POINTS TO LEFT OF INTERVAL.
C            NEXT(2) = NUMBER OF EVALUATION POINTS TO RIGHT OF INTERVAL.
C
C     IERR -- (OUTPUT) ERROR FLAG.
C           NORMAL RETURN:
C              IERR = 0  (NO ERRORS).
C           "RECOVERABLE" ERRORS:
C              IERR = -1  IF NE.LT.1 .
C              IERR = -2  IF X1.EQ.X2 .
C                (OUTPUT ARRAYS HAVE NOT BEEN CHANGED IN EITHER CASE.)
C
C  FORTRAN INTRINSICS USED:  DMAX1, DMIN1.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
C  DECLARE LOCAL VARIABLES.
C
      INTEGER  I
      DOUBLE PRECISION   DELTA, H, X, XMI, XMA, ZERO
      DATA  ZERO /0.0D0/
C
C  VALIDITY-CHECK ARGUMENTS.
C
      IF (NE .LT. 1)  GO TO 5001
      H = X2 - X1
      IF (H .EQ. ZERO)  GO TO 5002
C
C  INITIALIZE.
C
      IERR = 0
      NEXT(1) = 0
      NEXT(2) = 0
      XMI = DMIN1(ZERO, H)
      XMA = DMAX1(ZERO, H)
C
C  COMPUTE SLOPE
C
      DELTA = (F2 - F1)/H
C
C  EVALUATION LOOP.
C
      DO 500  I = 1, NE
         X = XE(I) - X1
         FE(I) = F1 + X*DELTA
C          COUNT EXTRAPOLATION POINTS.
         IF ( X.LT.XMI )  THEN
            NEXT(1) = NEXT(1) + 1
         ELSE IF ( X.GT.XMA )  THEN
            NEXT(2) = NEXT(2) + 1
         ENDIF
  500 CONTINUE
C
C  NORMAL RETURN.
C
      RETURN
C
C  ERROR RETURNS.
C
 5001 CONTINUE
C     NE.LT.1 RETURN.
      IERR = -1
      RETURN
C
 5002 CONTINUE
C     X1.EQ.X2 RETURN.
      IERR = -2
      RETURN
C------------- LAST LINE OF DCHFEV FOLLOWS -----------------------------
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      END
