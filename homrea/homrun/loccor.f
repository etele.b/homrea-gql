!--------------------------------------------------------------------------------------
! Written by Alexander Neagos
!--------------------------------------------------------------------------------------
      module local_coordinates
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C
C
      type coordinate_type
      double precision, allocatable,dimension(:,:)     :: coor_values
      end type coordinate_type
C
C
      type vertice_type
      integer, allocatable,dimension(:)     :: vertice_values
      end type vertice_type
C
C
      contains
C
C
      subroutine alloc_loccoor(ndim,nrnei,coor)
      type (coordinate_type)                            :: coor
      allocate(coor%coor_values(ndim,nrnei))
      end subroutine alloc_loccoor
C
C
      subroutine alloc_vertval(nrnei,vertval)
      type (vertice_type)                               :: vertval
      allocate(vertval%vertice_values(nrnei))
      end subroutine alloc_vertval
C
C
      subroutine loccor(info,onlygrad,numnei,nvert,ndim,nrbou,neq,y,
     1            theta,iret,vertnr,nrnei,vertice_vals,evec,
     1            local_coordinates)
      DOUBLE PRECISION, DIMENSION(NEQ,NVERT)            ::y
      INTEGER, DIMENSION(NDIM,NVERT)                    ::theta
      INTEGER, DIMENSION(nvert)                         ::iret
      INTEGER, DIMENSION(nrbou)                         ::vertnr
      INTEGER, DIMENSION(nrbou)                         ::nrnei
      INTEGER, DIMENSION(nrbou)                         ::evec
      INTEGER, DIMENSION(ndim,nvert)                    ::minmaxthet
      INTEGER, DIMENSION(ndim)                         ::checkminmax
      INTEGER, DIMENSION(ndim)                         ::dummythet1
      INTEGER, DIMENSION(ndim)                         ::dummythet2
      type(coordinate_type),dimension(nrbou)      :: local_coordinates
      type(vertice_type),dimension(nrbou)         :: vertice_vals
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:)       ::svds
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:,:)     ::deltatheta
      DOUBLE PRECISION, DIMENSION(NDIM,NDIM)              ::SVDU
      DOUBLE PRECISION,DIMENSION(5*NDIM)                 ::SVDWORK
      DOUBLE PRECISION,DIMENSION(ndim)                   ::meantheta
      DOUBLE PRECISION,ALLOCATABLE,DIMENSION(:,:)       ::uplus
      INTEGER, DIMENSION(15**(NDIM)-1)                   :: VERTICES
      logical                                          :: dosvd,onlygrad
      dosvd=.true.
      nrb=0
      isum=0
      evec=0.0d0
C***********************************************************************
C     decrease dimension by 1 if desired
C***********************************************************************
      if (info.eq.-1) then
      ndimm=ndim-1
      else
      ndimm=ndim
      end if
C***********************************************************************
C     decrease dimension by 1 if desired
C***********************************************************************
      allocate(uplus(ndimm,ndim))
C***********************************************************************
C     determine minmaxtheta 
C       minmaxtheta(kk,l)  = -1 if vertex is at the neg. b. in kk-dir
C       minmaxtheta(kk,l)  =  1 if vertex is at the pos. b. in kk-dir
CTODO seems to make sense only for regular mesh
C***********************************************************************
      minmaxthet=2
      if (info.eq.-1)then
        do ll=1,nvert
            if (iret(ll).eq.info)then
            do kk=1,ndim
                if(theta(kk,ll).eq.minval(theta(kk,1:nvert)))then
                    minmaxthet(kk,ll)=-1
                else if(theta(kk,ll).eq.maxval(theta(kk,1:nvert)))then
                    minmaxthet(kk,ll)=1
                end if
            end do
            end if
        end do
      end if
C***********************************************************************
C A. Neagos
C Search for boundary vertices and their neighbours
C on boundary
C***********************************************************************
      do l=1,nvert
        nnn=numnei
          if (iret(l).le.info)then
  444       continue
            call neigh_vert2(info,onlygrad,l,nnn,nvert,ndim,theta,
     1                      vertices,ineigh,iret,minmaxthet)
          if (onlygrad)then
            if (ineigh.lt.2*ndimm)then
            nnn=nnn+1
            go to 444
            end if
          else
            if (ineigh.lt.(2*numnei+1)**ndimm-1)then
            nnn=nnn+1
            go to 444
            end if
          end if
            nrb=nrb+1
            nrnei(nrb)=ineigh
            vertnr(nrb)=l
            call alloc_vertval(ineigh,vertice_vals(nrb))
            vertice_vals(nrb)%vertice_values(:)=
     1                                 vertices(1:ineigh)
          end if
      end do
C***********************************************************************
C A. Neagos
C Start loop
C***********************************************************************
      do l=1,nrb
C***********************************************************************
C A. Neagos
C Do SVD if not enough neighbours or if desired
C***********************************************************************
*
      if(dosvd )then
C***********************************************************************
C A. Neagos
C Mean values for local SVD
C***********************************************************************
      allocate(deltatheta(ndim,nrnei(l)+1))
      meantheta=theta(:,vertnr(l))
      do k=1,nrnei(l)
            meantheta=meantheta+
     1      theta(:,vertice_vals(l)%vertice_values(k))
      end do
      meantheta=meantheta/real(nrnei(l)+1)
C***********************************************************************
C A. Neagos
C Perform SVD
C***********************************************************************
      deltatheta(:,1)=meantheta-theta(:,vertnr(l))
      do k=1,nrnei(l)
        deltatheta(:,k+1)=meantheta-
     1                    theta(:,vertice_vals(l)%vertice_values(k))
      end do
      allocate(svds(nrnei(l)+1))
      call dgesvd('A','N',ndim,nrnei(l)+1,deltatheta,NDIM,SVDS,SVDU,
     1            NDIM,SVDV,1,SVDWORK,5*NEQ,INFSVD)
      deallocate(deltatheta)
      deallocate(SVDS)
*      hier: letzter SVDU-Vektor zeigt nach au�en.
C
C***********************************************************************
C A. Neagos
C Pseudo-Inverse of 1. vector in U Matrix
C***********************************************************************
      CALL MPPSI(ndim,NDIMM,SVDU(1:ndim,1:NDIMM),uplus(:,:))
C***********************************************************************
C A. Neagos
C Projection on local coordinates using Pseudo-Inverse
C***********************************************************************
      call alloc_loccoor(ndimm,nrnei(l),local_coordinates(l))
      do k=1,nrnei(l)
        local_coordinates(l)%coor_values(:,k)=
     1          MATMUL(UPLUS,(theta(:,vertnr(l))-
     1           theta(:,vertice_vals(l)%vertice_values(k))))
c        write(*,*)'at vert',vertnr(l),
c    1   'neighbour',vertice_vals(l)%vertice_values(k),
c    1   'local coors',
c    1  local_coordinates(l)%coor_values(:,k)
*      read(*,*)
      end do
C
      end if
      end do
      deallocate (uplus)
      end subroutine loccor
      SUBROUTINE neigh_vert2(INFO,onlygrad,IVC,numnei,NVERT,NDIM,ICORD,
     1           NEIGH,INEIGH,IRET,minmaxthet)
!***********************************************************************
!
!     This routine gets the indices of the neighbours
!
!     input:  NVERT: number of vertices
!             NDIM:  dimension
!             ICORD: coordinates of the vertices
!             IVC  : number of the point, to which neighbours are searched
!
!     output: NEIGH: indices of the neighbours
!             IERR : flag
C             only verices with IRET .LE. INFO are taken into account
C             for ONLYGRAD no doiagonal directions are allowed
!
!***********************************************************************
      DIMENSION NEIGH(15**(NDIM)-1),
     1          ICORD(NDIM,NVERT),iDUMMYCORD(NDIM)
!cjb
      DIMENSION ICO(NDIM)
      DIMENSION IRET(NVERT)
      dimension minmaxthet(ndim,nvert)
      logical testvec(NDIM)
      LOGICAL   LOC1,LOC2,onlygrad,checkminmax
!cjb--
      LOGICAL TEST
C***********************************************************************
C     INITIALIZE
C***********************************************************************
      NEIGH=0
      numdum=numnei
      ineigh=0
C***********************************************************************
C
C     Loop over possible neighbours
C
C***********************************************************************
C***********************************************************************
C     allow only stencil star 
C***********************************************************************
      if (onlygrad) then
          do i=1,nvert
            itest=0
            iDUMMYCORD=ICORD(:,IVC)-ICORD(:,i)
            if (i.ne.ivc)then
                do j=1,ndim
                    itest=itest+abs(idummycord(j))
                end do
                if (itest.eq.1 .and. iret(i).le.info)then
                    ineigh=ineigh+1
                    neigh(ineigh)=i
                end if
            end if
          end do
C***********************************************************************
C     allow all surrounding points
C***********************************************************************
      else
          do i=1,nvert
            itest=0
            iDUMMYCORD=ICORD(:,IVC)-ICORD(:,i)
            if (i.ne.ivc)then
C----  distance in all directions is lt numdum
                do j=1,ndim
                    if (abs(idummycord(j)).le.numdum) then
                        itest=itest+1
                    end if
                end do
                if (itest.eq.ndim .and. iret(i).le.info)then
C---- checkminmax seems to be only important for strange casess
                    checkminmax=.true.
                    do kk=1,ndim
                        if(minmaxthet(kk,ivc).eq.-minmaxthet(kk,i))then
                            checkminmax=.false.
                        end if
                    end do
                    if (checkminmax)then
                        ineigh=ineigh+1
                        neigh(ineigh)=i
                    end if
                end if
            end if
          end do
      end if
      RETURN
!C***********************************************************************
!C     end CHKNEI
!C***********************************************************************
      END
      SUBROUTINE check_ervec(INFO,IVC,NVERT,NDIM,ICORD,
     1           IERR,iervec,neigh)
!***********************************************************************
!
!     This routine gets the indices of the neighbours
!
!     input:  NVERT: number of vertices
!             NDIM:  dimension
!             ICORD: coordinates of the vertices
!             IVC  : number of the point, to which neighbours are searched
!
!     output: NEIGH: indices of the neighbours
!             IERR : flag
!
!***********************************************************************
      DIMENSION ICORD(NDIM,NVERT),iDUMMYCORD(NDIM)
!cjb
      DIMENSION ICO(NDIM)
      DIMENSION iervec(nvert)
      DIMENSION NEXT(NDIM)
      LOGICAL   LOC1,LOC2
!cjb--
      LOGICAL TEST
C***********************************************************************
C     INITIALIZE
C***********************************************************************
      NEIGH=0
      IERR = 0
      numdum=1
      if (iervec(ivc).eq.0)go to 123
      ineigh=0
!C***********************************************************************
!C     Loop over possible neighbours
!C***********************************************************************
      itrigger=100000
      do i=1,nvert
        itest=0
        ithetasum=0
        iDUMMYCORD=ICORD(:,IVC)-ICORD(:,i)
            do j=1,ndim
                if (abs(idummycord(j)).le.NUMDUM) then
                    itest=itest+1
                end if
            end do
            if (itest.eq.ndim
     1          .and. iervec(i).eq.0)then
                ineigh=ineigh+1
                do j=1,ndim
                  ithetasum=ithetasum+abs(idummycord(j))
                end do
                if (ithetasum.lt.itrigger)then
C                  write(*,*)'itsum',ithetasum
                  itrigger=ithetasum
                  neigh=i
                end if
            end if
      end do
      if (neigh.eq.0)IERR = -1
      IF(ineigh.lt.1) then
      numdum=numdum+1
      if (numdum.ge.3)then
        write(*,*)'no neighbours found with iervec=0, stop check_ervec!'
        write(*,*)'try less numnei and lower degree polynomial'
C      write(*,*)'numdum',numdum
C      read(*,*)
      go to 130
      end if
      end if
  123 continue
      RETURN
  130 stop
!C***********************************************************************
!C     end CHKNEI
!C***********************************************************************
      END
      SUBROUTINE MPPSI(NEQ,NDIM,PSITH,PSITHP)
C***********************************************************************
C                                                                      *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DOUBLE PRECISION, DIMENSION(NEQ,NDIM)     :: PSITH
      DOUBLE PRECISION, DIMENSION(NDIM,NEQ)     :: PSITHP
      DOUBLE PRECISION, DIMENSION(NDIM,NDIM)    :: TEMGRA
      DOUBLE PRECISION, DIMENSION(NEQ)          :: RW
      INTEGER         , DIMENSION(NEQ)          :: IW
C***********************************************************************
C                                                                      *
C***********************************************************************
      TEMGRA = MATMUL(TRANSPOSE(PSITH),PSITH)
      CALL DGEINV(NDIM,TEMGRA,RW,IW,IERINV)
      PSITHP = MATMUL(TEMGRA,TRANSPOSE(PSITH))
      IF(IERINV.GT.0)  THEN
C       write(*,*)'inierv',IERINV
C         WRITE(*,*) 'ERROR IN MPPSI x'
C       WRITE(*,*) 'Psith+',PSITHP
        PSITHP(:,:) = 0.0d0
C       WRITE(*,*) 'Psith+',PSITHP
C        read(*,*)
      else if (maxval(psithp).gt.1.0d10 .or. minval(psithp).lt.-1.0d10)
     1 then
       psithp = 0.0d0
      ENDIF
      RETURN
      END
      end module local_coordinates
