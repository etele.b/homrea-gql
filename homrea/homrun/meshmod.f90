module meshmod
implicit none


integer                                         ::n_sp,mesh_points,m_old_len
doubleprecision,dimension(:),allocatable        ::mesh_old
doubleprecision                                 ::summe,betrag
doubleprecision,dimension(:),allocatable        ::mesh_new
integer,dimension(:),allocatable                ::vl
doubleprecision,dimension(:,:),allocatable      ::zst_red_rev,zst_1,grd_1

contains

!*****************************************************************************

!Zentrale Subroutine dieses Moduls--> Schnittstelle zwischen allen anderen
!Unterprogrammen

!*****************************************************************************
subroutine mesh_main(p_in,ret_var)
implicit none

integer,intent(in)                          ::p_in
logical,intent(out)                         ::ret_var

integer                                     ::ia_max,id_max
integer                                     ::i
logical                                     ::proof=.true.

mesh_points=p_in
!p_mesh=p_in
allocate(mesh_new(mesh_points))
mesh_new=mesh_old

m_old_len=mesh_points
allocate(vl(m_old_len-1))

ret_var=.false.

do i=2,mesh_points-1

    write(*,*)'di zum GP',i
    write(*,*)di(mesh_new(i+1),mesh_new(i),mesh_new(i-1))
end do


do while(proof)
    i=2
    proof=.false.
    do while(i<=mesh_points-1)
        if(di(mesh_new(i+1),mesh_new(i),mesh_new(i-1))>=2.00000000000000)then
            call ins_point(i,.true.)
            write(*,*)'Gitter von unten nach oben durchlaufen'
            proof=.true.
        else
            i=i+1
        end if

    end do
end do

proof=.true.

do while(proof)
    i=mesh_points-1
    proof=.false.
    do while(i>=2)
        write(*,*)'di zum GP',i
        write(*,*)di(mesh_new(i+1),mesh_new(i),mesh_new(i-1))
        if(di(mesh_new(i+1),mesh_new(i),mesh_new(i-1))<=0.50000000000000)then
            call ins_point(i,.false.)
            proof=.true.
            write(*,*)'Gitter von oben nach unten durchlaufen'
            i=i+1
        end if
        i=i-1
    end do
end do
do i=1,mesh_points
write(*,*)'Mesh_new zum GP',i
write(*,*)mesh_new(i)
end do

allocate(zst_red_rev(n_sp,mesh_points))


call comp_mesh()

write(*,*)'ia_max eingeben(Grenze einzufuegender Punkte, ab der das modifizierte Gitter uebernommen werden soll):'
read(*,*)ia_max
!write(*,*)'id_max eingeben (Grenze zu entfernender Punkte, ab der das modifizierte Gitter uebernommen werden soll):'
!read(*,*)id_max

!if(maxval(vl)>1)then
!        call set_new_mesh()
!        ret_var=.true.
!        write(*,*)'******************************************************'
!        write(*,*)'              GITTER WURDE ANGEPASST!!                                        '
!        write(*,*)'******************************************************'
!        write(*,*)''
!        write(*,*)'Anzahl neuer Gitterpunkte',mesh_points
if(p_points(.true.,m_old_len-1)>ia_max)then
        call set_new_mesh()
        ret_var=.true.
        write(*,*)'******************************************************'
        write(*,*)'              GITTER WURDE ANGEPASST!!                                        '
        write(*,*)'******************************************************'
        write(*,*)''
        write(*,*)'Anzahl neuer Gitterpunkte',mesh_points
!else if(p_points(.false.,m_old_len-1)>id_max)then
!        call set_new_mesh()
!        ret_var=.true.
!        write(*,*)'******************************************************'
!        write(*,*)'              GITTER WURDE ANGEPASST!!                                        '
!        write(*,*)'******************************************************'
!        write(*,*)''
!        write(*,*)'Anzahl neuer Gitterpunkte',mesh_points
end if


return
end subroutine mesh_main

!*****************************************************************************

!Erstellen des neuen Gitters und der zugeh�rigen Funktionswerte

!*****************************************************************************
subroutine set_new_mesh()
implicit none

integer                                 ::i,j,k,m,length
doubleprecision                         ::delta


mesh_new(1)=mesh_old(1)

zst_red_rev(1:n_sp,1)=zst_1(1:n_sp,1)

j=1
i=2

do while(i<=mesh_points)
    if(vl(j)>0)then
        mesh_new(i+vl(j))=mesh_old(j+1)
        zst_red_rev(1:n_sp,i+vl(j))=zst_1(1:n_sp,j+1)

        delta=mesh_new(i+vl(j))-mesh_new(i-1)
            do k=1,vl(j)
                mesh_new(i+k-1)=(delta/(dble(vl(j)+1)))*dble(k)+mesh_new(i-1)

                do m=1,n_sp
                    zst_red_rev(m,i+k-1)=lin_interp(zst_red_rev(m,i+vl(j))&
                    &,zst_red_rev(m,i-1),delta,(delta/(dble(vl(j)+1)))*dble(k))


                end do
            end do

            i=i+vl(j)+1
            j=j+1
    else if(vl(j)==0)then
        mesh_new(i)=mesh_old(j+1)
        zst_red_rev(1:n_sp,i)=zst_1(1:n_sp,j+1)
        j=j+1
        i=i+1
    else
        j=j+1
    end if

end do

write(*,*)'ZUSTAND AUF NEUES GITTER INTERPOLIERT!!!'

end subroutine set_new_mesh

!*****************************************************************************

!Lineare Approximation zwischen zwei bestehenden Gitterpunkten

!*****************************************************************************
doubleprecision function lin_interp(f_i1,f_i,delta_old,delta_new)
implicit none

doubleprecision,intent(in)                  ::f_i1
doubleprecision,intent(in)                  ::f_i
doubleprecision,intent(in)                  ::delta_old
doubleprecision,intent(in)                  ::delta_new

lin_interp=f_i+((f_i1-f_i)/(delta_old))*(delta_new)

return
end function lin_interp
!*****************************************************************************

!Wert des vorerst eingef�gten neuen Gitterpunktes

!*****************************************************************************
doubleprecision function di(psi1,psi,psi_1)
implicit none

doubleprecision,intent(in)               ::psi1
doubleprecision,intent(in)               ::psi
doubleprecision,intent(in)               ::psi_1

di=(psi1-psi)/(psi-psi_1)

return
end function di
!*****************************************************************************

!Einf�gen des neuen Gitterpunktes ins Gitter

!*****************************************************************************
subroutine ins_point(pointi,up_do)
implicit none

integer,intent(in)                              ::pointi
logical,intent(in)                              ::up_do

doubleprecision,dimension(:),allocatable        ::mesh_hp

allocate(mesh_hp(mesh_points))
mesh_hp=mesh_new

mesh_points=mesh_points+1

write(*,*)'ins_point aufgerufen, mesh_points=',mesh_points
write(*,*)'Eingefuegter Punkt:',pointi

deallocate(mesh_new)

allocate(mesh_new(mesh_points))

if(up_do)then
    mesh_new(pointi+1)=dble(0.5)*(mesh_hp(pointi+1)+mesh_hp(pointi))
    mesh_new(1:pointi)=mesh_hp(1:pointi)
    mesh_new(pointi+2:mesh_points)=mesh_hp(pointi+1:mesh_points-1)
else
    mesh_new(pointi)=dble(0.5)*(mesh_hp(pointi)+mesh_hp(pointi-1))
    mesh_new(1:pointi-1)=mesh_hp(1:pointi-1)
    mesh_new(pointi+1:mesh_points)=mesh_hp(pointi:mesh_points-1)
end if

deallocate(mesh_hp)

end subroutine ins_point

!*****************************************************************************

!Vergleichen des neuen und alten Gitters

!*****************************************************************************
subroutine comp_mesh()
implicit none

integer                     ::i,k,j
integer                     ::ml
k=1
j=1
vl=0
do i=1,m_old_len-1
    ml=1
    do
        if(mesh_old(i+1)==mesh_new(j+ml))then
            vl(i)=ml-1
            write(*,*)'VL im Intervall',i
            write(*,*)vl(i)
            k=i
            do
                if(mesh_new(k)==mesh_old(i+1))then
                    j=k
                    exit
                end if
            k=k+1
            cycle
            end do
            exit
        else
            ml=ml+1
            cycle
        end if

    end do
end do

do i=1,m_old_len-1
    if(vl(i)==-1)then
        if(vl(i+1)>0)then
            vl(i)=0
            vl(i+1)=vl(i+1)-1
        end if
    else if(vl(i)>0)then
        if(vl(i+1)==-1)then
            vl(i)=vl(i)-1
            vl(i+1)=0
        end if
    end if
end do

end subroutine comp_mesh

!*****************************************************************************

!Aufsummieren der Punkte zur �berpr�fung, ob neues Gitter verwendet werden
!soll oder altes beibehalten werden kann

!*****************************************************************************

recursive function p_points(cas,p_last)result(i_a_d)
implicit none

logical,intent(in)              ::cas
integer,intent(in)              ::p_last

integer                         ::i_a_d

if(cas)then
    if(p_last>1)then
        i_a_d=max(vl(p_last),0)+p_points(cas,p_last-1)
    else
        i_a_d=max(vl(p_last),0)
    end if
else
    if(p_last>1)then
        i_a_d=-min(vl(p_last),0)+p_points(cas,p_last-1)
    else
        i_a_d=-min(vl(p_last),0)
    end if
end if
end function p_points
doubleprecision function vektornorm(dim1,dim2,punktanzahl,zust)
implicit none

integer,intent(in)                                      ::dim1,dim2,punktanzahl
doubleprecision,dimension(1:dim1,1:dim2),intent(in)     ::zust

integer                                                 ::i=1,j=1



betrag=dble(0)

do j=1,punktanzahl-1
    summe=dble(0)
    do i=1,dim1
        summe=summe+((zust(i,j+1)-zust(i,j))**2)
    end do
    betrag=betrag+sqrt(summe)

end do

vektornorm=betrag

return
end function vektornorm

recursive function integ(punkt,anz_spe,anz_p,zst,gitter_old) result(normiert)
implicit none

integer,intent(in)                                                          ::punkt
integer,intent(in)                                                          ::anz_spe
integer,intent(in)                                                          ::anz_p
doubleprecision,dimension(1:anz_p),intent(in)                               ::zst
doubleprecision,dimension(1:anz_p),intent(in)                               ::gitter_old
!doubleprecision,intent(in)                                                  ::gew


doubleprecision                                                             ::normiert



                if(punkt==2)then
                normiert=(gitter_old(punkt)-gitter_old(punkt-1))*&
                &(zst(punkt)+zst(punkt-1))/dble(2)

                else
                normiert=integ(punkt-1,anz_spe,anz_p,zst,gitter_old)+(gitter_old(punkt)&
                &-gitter_old(punkt-1))*(zst(punkt)+zst(punkt-1))/dble(2)

                end if

end function integ


doubleprecision function diff(zstp_1,zstp,gtp_1,gtp)
implicit none


doubleprecision,intent(in)               ::zstp_1,zstp,gtp_1,gtp

diff=(zstp_1-zstp)/(gtp_1-gtp)

return
end function diff

doubleprecision function diff_2(zst_p1,zst_p,zst_pm1,gt_p1,gt_p,gt_pm1)
implicit none

doubleprecision,intent(in)               ::zst_p1,zst_p,zst_pm1,gt_p1,gt_p,gt_pm1

diff_2=(zst_p1+zst_pm1-2*zst_p)/((gt_p1-gt_p)*(gt_p-gt_pm1))

return
end function diff_2

doubleprecision function diff_zen(zstp_1,zst_pm1,gtp1_,gtp_)
implicit none

doubleprecision,intent(in)               ::zstp_1,zst_pm1,gtp1_,gtp_

diff_zen=(zstp_1-zst_pm1)/((gtp1_-gtp_)*dble(2))

return
end function diff_zen

end module meshmod


