      SUBROUTINE PITRES(N,RPAR,IPAR,Y,F,IFC)
C***********************************************************************
C
C     Interface between 
C
C***********************************************************************
C***********************************************************************
C     Storage organization
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION Y(N),F(N),RPAR(*),IPAR(*)
C***********************************************************************
C     Get dimension of state space 
C***********************************************************************
      NEQ = IPAR(2)
      NC  = IPAR(11)
C***********************************************************************
C     Call function system 
C***********************************************************************
      TAU = Y(N)
      CALL NEWRES(NEQ,NC,TAU,Y,F,RPAR,IPAR,IFC)
      IFC = 0 
      RETURN
C***********************************************************************
C     END
C***********************************************************************
      END
      SUBROUTINE EULRES(N,TIME,Y,F,RPAR,IPAR,IFC)
C***********************************************************************
C
C     Interface
C
C***********************************************************************
C***********************************************************************
C     Storage organization 
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION Y(N),F(N),RPAR(*),IPAR(*)
      PARAMETER (ZERO=0.0D0, ONE=1.0D0)
C***********************************************************************
C     Call of function system 
C***********************************************************************
      NC=IPAR(11)
      TAU = ONE 
      CALL NEWRES(N,NC,TAU,Y,F,RPAR,IPAR,IFC)
      RETURN
C***********************************************************************
C     END OF EULRES
C***********************************************************************
      END
      SUBROUTINE SNLRES(N,Y,F,RPAR,IPAR,IFC)
C***********************************************************************
C
C     Interface 
C
C***********************************************************************
C***********************************************************************
C     Storage organization 
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION Y(N),F(N),RPAR(*),IPAR(*)
      PARAMETER (ZERO=0.0D0, ONE=1.0D0)
C***********************************************************************
C     Call of function system 
C***********************************************************************
      NC=IPAR(11)
      TAU = ONE 
      CALL NEWRES(N,NC,TAU,Y,F,RPAR,IPAR,IFC)
      RETURN
C***********************************************************************
C     END OF SNLRES
C***********************************************************************
      END
      SUBROUTINE SNLJAC(N,M,Y,A,RPAR,IPAR,IFC)
C***********************************************************************
C
C     Set analytical Jacobian to be -I
C
C***********************************************************************
C***********************************************************************
C     Storage organization 
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION A(N,N),Y(N),RPAR(*),IPAR(*)
      PARAMETER (ZERO=0.0D0,ONE=1.0D0)
C***********************************************************************
C     Set Jacobian to -I
C***********************************************************************
      IFC = 0
      A = ZERO 
      DO I=1,N
      A(I,I) = -ONE  
      ENDDO 
      RETURN
C***********************************************************************
C     End of SNLJAC
C***********************************************************************
      END
      SUBROUTINE ALCRES(N,Y,TAU,F,RPAR,IPAR,IFC)
C***********************************************************************
C
C     Interface 
C
C***********************************************************************
C***********************************************************************
C     Storage organization
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION Y(N),F(N),RPAR(*),IPAR(*)
      PARAMETER (ZERO=0.0D0,ONE=1.0D0)
C***********************************************************************
C     Call right hand side 
C***********************************************************************
      NC=IPAR(11)
      CALL NEWRES(N,NC,TAU,Y,F,RPAR,IPAR,IFC)
      RETURN
C***********************************************************************
C     END
C***********************************************************************
      END
      SUBROUTINE ALCJAC(N,N1,T,Y,F,SM,A,RPAR,IPAR,IFC)
C***********************************************************************
C
C     Form analytical Jacobian for ALCON1 
C
C***********************************************************************
C***********************************************************************
C     Storage organization 
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION A(N,N1),Y(N1),F(N),SM(N1),RPAR(*),IPAR(*)
      PARAMETER (ONE=1.0D0,ZERO=0.0D0)
C***********************************************************************
C     Input from COMMON blocks
C***********************************************************************
      INCLUDE 'homdim.f'
      COMMON/BDFDT/DFDTAU(MNEQU)
C***********************************************************************
C     Generate Jacobian  -I,DFDTAU
C***********************************************************************
      A = ZERO
      DO I=1,N
        A(I,I) = -ONE
      ENDDO
      A(1:N,N+1) = DFDTAU(1:N)
C***********************************************************************
C     Scale Jacobian 
C***********************************************************************
      DO 10 K=1,N1
      A(1:N,K)= SM(K)*A(1:N,K)
  10  CONTINUE
C***********************************************************************
C     Regular exit
C***********************************************************************
      RETURN
      END
      SUBROUTINE CRORES(N,Y,F,RPAR,IPAR,IFC)
C***********************************************************************
C
C     Interface
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION Y(N),F(N),RPAR(*),IPAR(*)
      PARAMETER (ZERO=0.0D0,ONE=1.0D0)
C***********************************************************************
C     Input from COMMON blocks
C***********************************************************************
      INCLUDE 'homdim.f'
      COMMON/BEGVS/EVAR(MNEQU),EVAI(MNEQU)
C***********************************************************************
C     Initialize
C***********************************************************************
      NEQ = IPAR(2)
      NC  = IPAR(11)
C***********************************************************************
C     store tau for mandes 
C***********************************************************************
C***********************************************************************
C     Call right hand side 
C***********************************************************************
      TAU  = Y(N)
      CALL NEWRES(NEQ,NC,TAU,Y,F,RPAR,IPAR,IFC)
C***********************************************************************
C     set up additional equation for target value 
C***********************************************************************
      F(N) = log((EVAR(NC)**2+EVAI(NC)**2)/EVAR(1+NC)**2)
      RETURN
C***********************************************************************
C     END
C***********************************************************************
      END
      SUBROUTINE TARRES(N,Y,F,RPAR,IPAR,IFC)
C***********************************************************************
C
C     Determine target point
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION Y(N),F(N),RPAR(*),IPAR(*)
      PARAMETER (ZERO=0.0D0,ONE=1.0D0)
C***********************************************************************
C     Input from COMMON blocks
C***********************************************************************
      INCLUDE 'homdim.f'
      COMMON/BTARG/VTARG,ITARG
      COMMON/BLIMIT/XLIMIT(MNLIM*(MNSPE+2)),ELIMIT(2,MNLIM  ),NLIM
C***********************************************************************
C     Initialize
C***********************************************************************
      NEQ = IPAR(2)
      NC  = IPAR(11)
C***********************************************************************
C     Call right hand side 
C***********************************************************************
      TAU = Y(N)
      CALL NEWRES(NEQ,NC,TAU,Y,F,RPAR,IPAR,IFC)
C***********************************************************************
C     set up additional equation for target value 
C***********************************************************************
      F(N) = DDOT(N-1,XLIMIT(ITARG),NLIM,Y,1) - VTARG
      RETURN
C***********************************************************************
C     END
C***********************************************************************
      END
      SUBROUTINE TARJAC(N,LDN,Y,A,RPAR,IPAR,IFC)
C***********************************************************************
C
C     Jacobian for target point 
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C***********************************************************************
C     Storage organization 
C***********************************************************************
      DIMENSION A(LDN,N),Y(N),RPAR(*),IPAR(*)
      DOUBLE PRECISION, DIMENSION(N)       :: FDUM
      PARAMETER (ONE=1.0D0,ZERO=0.0D0)
C***********************************************************************
C     Input from COMMON blocks
C***********************************************************************
      INCLUDE 'homdim.f'
      COMMON/BDFDT/DFDTAU(MNEQU)
      COMMON/BTARG/VTARG,ITARG
      COMMON/BLIMIT/XLIMIT(MNLIM*(MNSPE+2)),ELIMIT(2,MNLIM  ),NLIM
C***********************************************************************
C     Call the right hand side (use A as a work array) 
C***********************************************************************
      CALL TARRES(N,Y,FDUM,RPAR,IPAR,IFC)
      IFC = 0
C***********************************************************************
C     Copy Jacobian into A
C***********************************************************************
C***********************************************************************
C     Generate Jacobian  -I,DFDTAU
C***********************************************************************
      A = ZERO
      DO I=1,N-1
        A(I,I) = -ONE
      ENDDO
      A(1:N-1,N) = DFDTAU(1:N-1)
      CALL DCOPY(N-1,XLIMIT(ITARG),NLIM,A(N,1),N)
      RETURN
      END
      SUBROUTINE DUMJAC
      STOP
      END
      SUBROUTINE PATRES(NEQ,TIME,SMF,DEL,RPAR,IPAR,IFC)
C***********************************************************************
C
C     
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      COMMON/BEGVS/EVAR(MNEQU),EVAI(MNEQU)
      COMMON/BPMAT/PMATCO(MNEQU*MNEQU)
      DIMENSION SMF(NEQ),DEL(NEQ)
      DIMENSION RPAR(*),IPAR(*)
C***********************************************************************
C     COMMON BLOCKS
C***********************************************************************
      DATA NFILE6/6/
C***********************************************************************
C     WORK ARRAYS
C***********************************************************************
      DIMENSION RW(MNEQU*MNEQU),IW(2*MNEQU)
      LIW=2*MNEQU
      LRW=MNEQU*MNEQU
C***********************************************************************
C     INITIALIZE
C***********************************************************************
      NC=IPAR(11)
C***********************************************************************
C     Call function evaluation
C***********************************************************************
      CALL PATDES(NEQ,TIME,SMF,DEL,RPAR,IPAR,IFC,NC,
     1    EVAR,EVAI,PMATCO,LRW,RW,LIW,IW,NFILE6,IEREXD)
      IFC = 0
      IF(IEREXD.GT.0) IFC = 9
      RETURN
C***********************************************************************
C     END OF
C***********************************************************************
      END
      SUBROUTINE PATDES(NEQ,TIME,SMF,DEL,RPAR,IPAR,IFC,NC,
     1    AR,AI,CROW,LRW,RW,LIW,IW,NFILE6,IERR)
C***********************************************************************
C
C     RESIDUAL AND COEFFICIENTS OF TIME DERIVATIVES
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      COMMON /BTRMA/XTROW(MNEQU*MNEQU),XTCOL(MNEQU*MNEQU)
      COMMON /BSCRS/ERVROW(MNEQU*MNEQU),ERVCOL(MNEQU*MNEQU)
      COMMON/BDCVT/DELCVT(MNCOV)
      COMMON/BMIX/OMEGA,SMFMIX(MNEQU),SMFCHA(MNEQU),VDIR(MNEQU,NDIMM)
      COMMON/BIGENI/SPLIRA,IGENI,LNF
      COMMON/BNDM/NDMAN
      EXTERNAL MASRES,MASJAC
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)       :: SCHUR,XJAC,AVL,AVR
      DOUBLE PRECISION, DIMENSION(NEQ)           :: HELP,VRS
      PARAMETER (ONE = 1.0D0, ZERO = 0.0D0)
C***********************************************************************
C     arrays of dimension NEQ
C***********************************************************************
      DIMENSION SMF(NEQ),DEL(NEQ),AR(NEQ),AI(NEQ)
C***********************************************************************
C     arrays of dimension NEQ,NEQ
C***********************************************************************
      DIMENSION CROW(NEQ,NEQ)
C***********************************************************************
C     work arrays
C***********************************************************************
      DIMENSION IW(LIW),RW(LRW)
C***********************************************************************
C     parameter arrays 
C***********************************************************************
      DIMENSION RPAR(*),IPAR(*)
C***********************************************************************
C     common blocks 
C***********************************************************************
      COMMON/BSCV/CSTART(MNCOV)
      COMMON/BCV/CVCOM(MNCOV)
      COMMON/BBET1/BETOLD
      COMMON/BPATHF/ICHKD,IALSCA
      SAVE
C***********************************************************************
C
C     Initialization 
C 
C***********************************************************************
      ipri = 0
      ierr = 0
C***********************************************************************
C     Check work array
C***********************************************************************
      IF(LRW.LT.NC*NC) GOTO 900
      IF(LIW.LT.NC   ) GOTO 905
C***********************************************************************
C     Check for valid input
C***********************************************************************
C     CALL CHKDOM(0,NEQ,SMF,SMF,INEG,VHIT,VALUE)
C     IF(INEG.GT.0) GOTO 980
C***********************************************************************
C
C     Calculate eigenvector basis 
C
C***********************************************************************
      MDIM = NC
      CALL EGVBAS(1,0,MDIM,RDIM,NEQ,MASRES,MASJAC,TIME,SMF,VRS,
     1            ERVCOL,ERVROW,XTCOL,XTROW,
     1            HELP,DEL,AR,AI,AVR,AVL,XJAC,SCHUR,LIW,IW,LRW,RW,
     1            RPAR,IPAR,IPRI,NFILE6,IERBAS)
      IF(IERBAS.GT.0) GOTO 910
C***********************************************************************
C     Store pathfollowing vector in HELP
C***********************************************************************
      HELP(1:NC) = DELCVT(1:NC)
C***********************************************************************
C     Compute  P * Q_U
C***********************************************************************
      CALL DGEMM('N','N',NC,NC,NEQ,ONE,CROW,NEQ,AVR,NEQ,ZERO,RW,NC)
CU    CALL DGEMM('N','T',NC,NC,NEQ,ONE,CROW,NEQ,CROW,NEQ,ZERO,RW,NC)
C***********************************************************************
C     factorize  P * Q_U
C***********************************************************************
      CALL DGEFA(RW,NC,NC,IW,INFO)
      IF(INFO.NE.0) GOTO 920
C***********************************************************************
C     Compute DEL = Q_U * (P * Q_U)^-1 * HELP
C***********************************************************************
      CALL DGESL(RW,NC,NC,IW,HELP,0)
      CALL DGEMV('N',NEQ,NC,ONE,AVR,NEQ,HELP,1,ZERO,DEL,1)
CU    CALL DGEMV('T',NC,NEQ,ONE,CROW,NEQ,HELP,1,ZERO,DEL,1)
C***********************************************************************
C     Scaling
C***********************************************************************
C     write(*,*) 'BETOLD ',BETOLD
      IF(BETOLD.GT.1.D-8) THEN
      BETNEW = SQRT(DDOT(NEQ,DEL,1,DEL,1))
      SAFFAC = BETOLD/BETNEW
      IF(IALSCA.GT.0) CALL DSCAL(NEQ,SAFFAC,DEL,1)
      ENDIF
C***********************************************************************
C     Regular exit
C***********************************************************************
      RETURN
C***********************************************************************
C
C***********************************************************************
  900 CONTINUE 
      WRITE(NFILE6,*) ' LRW too small in PATDES ' 
      STOP   
  905 CONTINUE 
      WRITE(NFILE6,*) ' LIW too small in PATDES ' 
      STOP   
  910 IERR = 1
      WRITE(NFILE6,*) ' EGVBAS returned to PATDES with an error flag'
      RETURN
  920 IERR = 2
      WRITE(NFILE6,*) ' ERROR IN SOLVING LIN. EQUATION IN PATDES'
      DO 132 I=1,NEQ
      WRITE(NFILE6,*) 'ar,ai',AR(I),AI(I)
  132 CONTINUE
      RETURN
  980 IERR = 9
      RETURN
C***********************************************************************
C     END OF EVBDES
C***********************************************************************
      END
      SUBROUTINE NEWRES(NEQ,NC,TAU,SMF,DEL,RPAR,IPAR,IFC)
C***********************************************************************
C
C     For a given function F this subroutine does the following:
C      
C        C
C        Z_f^-1
C
C
C     
C
C***********************************************************************
C***********************************************************************
C     Storage organisation 
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION SMF(NEQ),DEL(NEQ),RPAR(*),IPAR(*)
      PARAMETER (ZERO=0.0D0,ONE=1.0D0)
C***********************************************************************
C     COMMON BLOCKS
C***********************************************************************
      INCLUDE 'homdim.f'
      COMMON/BDFDT/DFDTAU(MNEQU)
      COMMON /BSCRS/ERVROW(MNEQU*MNEQU),ERVCOL(MNEQU*MNEQU)
      COMMON/BEGVS/EVAR(MNEQU),EVAI(MNEQU)
      COMMON/BSCV/CSTART(MNCOV)
      COMMON/BCV/CVCOM(MNCOV)
      COMMON/BPMAT/PMATCO(MNEQU*MNEQU)
      COMMON /BTRMA/XTROW(MNEQU*MNEQU),XTCOL(MNEQU*MNEQU)
      COMMON/BHEL/QEVB(MNEQU*MNEQU), QEVBI(MNEQU*MNEQU)
      DOUBLE PRECISION, DIMENSION(NC)            :: CVACT,DCVDTA     
C***********************************************************************
C     COMMON BLOCKS
C***********************************************************************
      DATA NFILE6/6/
C***********************************************************************
C     WORK ARRAYS
C***********************************************************************
      DIMENSION RW(MNEQU*MNEQU),IW(2*MNEQU)
      LIW=2*MNEQU
      LRW=MNEQU*MNEQU
C***********************************************************************
C     INITIALIZE
C***********************************************************************
C***********************************************************************
C     Calculate CV  where CV(TAU) = CVO + TAU*(CV-CVO)
C***********************************************************************
      IF(NC.LE.MNCOV) THEN
      CVACT(1:NC) = TAU * CVCOM(1:NC) + (ONE-TAU) * CSTART(1:NC)
      DCVDTA (1:NC) = CVCOM(1:NC) - CSTART(1:NC)
      ENDIF
C***********************************************************************
C     Call function evaluation
C***********************************************************************
      NZEV = IPAR(7)
      CALL NEWDES(NEQ,NC,NZEV,TAU,SMF,DEL,RPAR,IPAR,IFC,
     1    EVAR,EVAI,PMATCO,DFDTAU,CVACT,DCVDTA,ERVROW,ERVCOL,
     1    XTROW,XTCOL,QEVB,LRW,RW,LIW,IW,NFILE6,IEREXD)
C
      IFC = 0
      IF(IEREXD.GT.0) IFC = 9
      RETURN
C***********************************************************************
C     END OF
C***********************************************************************
      END
      SUBROUTINE NEWDES(NEQ,NC,NZEV,TAU,SMF,DEL,RPAR,IPAR,IFC,
     1    AR,AI,CROW,FTAU,CVACT,DCVDTA ,
     1    ERVROW,ERVCOL,YTROW,YTCOL,QEVB,LRW,RW,LIW,IW,NFILE6,IERR)
C***********************************************************************
C
C     RESIDUAL AND COEFFICIENTS OF TIME DERIVATIVES
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)       :: XTOT,XJAC,SCHUR,
     1                                              AVR,AVL,
     1                                              YTROW,YTCOL,
     1                                              ERVROW,ERVCOL,
     1                                              CROW,QEVB,
     1                                              QTEM
      DOUBLE PRECISION, DIMENSION(NEQ)           :: HELP,VRS,SMF,DEL,
     1                                              AR,AI,FTAU
      DOUBLE PRECISION, DIMENSION(NC)            :: CVACT,DCVDTA     
      DOUBLE PRECISION, DIMENSION(NDIMM,NDIMM)   :: VDP 
      DOUBLE PRECISION, DIMENSION(NDIMM*NDIMM)   :: XXX 
      EXTERNAL MASRES,MASJAC
      LOGICAL EQUILI
      PARAMETER (ONE = 1.0D0, ZERO = 0.0D0)
C***********************************************************************
C     work arrays
C***********************************************************************
      DIMENSION IW(LIW),RW(LRW)
C***********************************************************************
C     parameter arrays 
C***********************************************************************
      DIMENSION RPAR(*),IPAR(*)
C***********************************************************************
C     Commons
C***********************************************************************
      COMMON/ILDMPA/IMAILD,ILMILD,IOILDM
      COMMON/BMIX/OMEGA,SMFMIX(MNEQU),SMFCHA(MNEQU),VDIR(MNEQU,NDIMM)
      COMMON/BIGENI/SPLIRA,IGENI,LNF
      COMMON/BNDM/NDMAN
C***********************************************************************
C
C     Initialization 
C 
C***********************************************************************
      IERR = 0
C***********************************************************************
C     Check work array
C***********************************************************************
C***********************************************************************
C
C     Calculate components for parameterization
C
C***********************************************************************
C---- NP: number of parameterization equaitons
      NP  = NC       
C---- NPF, NPL: indices of first and last manifold equation 
      NPF = 1   
      NPL = NP       
C***********************************************************************
C
C     Calculate components for manifold equations
C
C***********************************************************************
C---- NF: number of manifold equations
      MDDD = NDMAN
      NMM = 0
      IF(IGENI.EQ.1) NMM = NEQ -NC 
      IF(IGENI.EQ.2) NMM = 1 
C     IF(SMF(3).LT.4.2) NMM = 1
C     IF(SMF(3).LT.2.4) NMM = NEQ-NC
      NF  = NEQ - NC - NMM
C---- NFF, NFL: indices of first and last manifold equation 
      NFF = NPL + 1   
      NFL = NFF - 1 + NF
      LNF = NF
      EQUILI = .FALSE.
      IF(NF.EQ.NEQ-NZEV) EQUILI = .TRUE.
C***********************************************************************
C
C     Calculate components for auxiliary equations
C
C***********************************************************************
C---- NP: number of parameterization equaitons
      NA  = 0       
      IF(IGENI.GT.0) NA  = NEQ - NC - NF       
C---- NAF, NAL: indices of first and last manifold equation 
      NAF = NFL + 1   
      NAL = NAF -1 + NA
C***********************************************************************
C
C     Calculate eigenvector basis 
C
C***********************************************************************
      MDIM = NC + NA
C***********************************************************************
C
C***********************************************************************
      IPRI = 0
      RDIM = 0
                       ICE  = 1
                       IAJ  = 0
      IF(IMAILD.EQ.1) THEN
C----   use J*J^T
        ICE = 4
C----   use analytical Jacobian
        IAJ = 0
      ENDIF
      IF(IMAILD.EQ.2) THEN
        ICE = 1
        IAJ = 1
      ENDIF
      IF(ILMILD.NE.0) ICE = 0
      CALL EGVBAS(ICE,IAJ,MDIM,RDIM,NEQ,MASRES,MASJAC,TIME,SMF,VRS,
     1            ERVCOL,ERVROW,YTCOL,YTROW,
     1            HELP,DEL,AR,AI,AVR,AVL,XJAC,SCHUR,LIW,IW,LRW,RW,
     1            RPAR,IPAR,IPRI,NFILE6,IERBAS)
      IF(IERBAS.GT.0) GOTO 910
C-Sl- TEST FOR LEFT AND RIGHT EIGENVECTORS
C         YTROW = YTCOL
C-Sl-
      IF(ILMILD.NE.0) YTROW = QEVB  
      IF(EQUILI) THEN 
         YTROW = ERVROW      
         YTCOL = ERVCOL      
      ENDIF
C***********************************************************************
C     Calculate F
C***********************************************************************
      CALL MASRES(NEQ,TIMDUM,SMF,VRS,RPAR,IPAR,IEMR)
C***********************************************************************
C
C     Calculate components for parameterization
C
C***********************************************************************
C***********************************************************************
C     Calculate (CV - C * SMF)  
C***********************************************************************
      HELP(NPF:NPL) = CVACT(NPF:NPL)  - MATMUL(CROW(NPF:NPL,:),SMF(:))
C***********************************************************************
C     Store CROW in scaling matrix 
C***********************************************************************
      XTOT(NPF:NPL,:) =  CROW(NPF:NPL,:)
C***********************************************************************
C
C     Calculate components for manifold equations
C
C***********************************************************************
      IF(NF.NE.0) THEN
C***********************************************************************
C     Calculate -Z~_f * F or for 1st order   -Z~_f * F_y * F 
C***********************************************************************
      IF(IOILDM.EQ.1) VRS(1:NEQ) = MATMUL(XJAC,VRS(1:NEQ))
      HELP(NFF:NFL) = -MATMUL(YTROW(NEQ-NF+1:NEQ,1:NEQ),VRS(1:NEQ))
C***********************************************************************
C     Set up scaling matrix for manifold equations 
C***********************************************************************
      IF(IOILDM.EQ.0) THEN
         XTOT(NFF:NFL,1:NEQ) =  MATMUL(YTROW(NEQ-NF+1:NEQ,1:NEQ),XJAC)
      ELSE IF(IOILDM.EQ.1) THEN
         XTOT(NFF:NFL,1:NEQ) =  MATMUL(YTROW(NEQ-NF+1:NEQ,1:NEQ),XJAC)
         XTOT(NFF:NFL,1:NEQ) =  MATMUL(XTOT(NEQ-NF+1:NEQ,1:NEQ),XJAC)
      ELSE
        WRITE(*,*) 'NOT APPLICABLE OPTION: IOILDM'
        STOP
      ENDIF
C***********************************************************************
C     End of manifold equations 
C***********************************************************************
      ENDIF
C***********************************************************************
C
C     Calculate components for auxiliary equations
C
C***********************************************************************
C***********************************************************************
C     Calculate auxiliary equation
C***********************************************************************
      IF(NA.NE.0) THEN
      NKV = NC + NF
      CALL ORBAS(NEQ,NKV,NEQ,XTOT,NEQ,QTEM,LRW,RW,LIW,IW,-2,IERR)
      IF(IERR.NE.0) THEN
         WRITE(*,*) ' EMERGENCY'
         STOP
      ENDIF
 7788 FORMAT(15(1PE9.2,1X))
C---- Compute M^orth * v v^T  use vrs as work array
C     VDIR(1:2,1:MDDD) = 0
        VDP(1:MDDD,1:MDDD) = MATMUL(TRANSPOSE(VDIR(1:NEQ,1:MDDD)),
     1                VDIR(1:NEQ,1:MDDD))
      CALL DLACPY('A',MDDD,MDDD,VDP,NDIMM,XXX,MDDD)
      CALL DGEINV(MDDD,XXX,RW,IW,IERINV)
      IF(IERINV.NE.0) THEN
         write(*,*) 'NNNNNN',neq
         GOTO 980
      ENDIF
      CALL DLACPY('A',MDDD,MDDD,XXX,MDDD,VDP,NDIMM)
      QTEM(NAF:NAL,1:NEQ) =
     1      QTEM(NAF:NAL,1:NEQ) - MATMUL(
     1       MATMUL(QTEM(NAF:NAL,1:NEQ),VDIR(1:NEQ,1:MDDD))
     1      , MATMUL(VDP(1:MDDD,1:MDDD),TRANSPOSE(VDIR(1:NEQ,1:MDDD)))
     1     )
      XTOT(NAF:NAL,:) = QTEM(NAF:NAL,:) 
      VRS(1:NEQ) =  (SMFMIX(1:NEQ) - SMF(1:NEQ))
C     write(*,*) 'VRSTT',VRS(1:NEQ)
      HELP(NAF:NAL) = MATMUL(XTOT(NAF:NAL,:),VRS(:))
C     write(*,*) 'Pauxa',NAF,NAL,HELP(NAF:NAL)
C     STOP
      ENDIF 
C***********************************************************************
C
C     Factorize  scaling matrix
C
C***********************************************************************
      CALL DGEFA(XTOT,NEQ,NEQ,IW,INFFA)
C     WRITE(*,*) 'zzzzzzzpppppp'
C     DO I=1,NEQ
C     WRITE(*,7788) XTOT(I,1:NEQ)
C     ENDDO
      IF(INFFA.NE.0) GOTO 930
C***********************************************************************
C     calculate FTAU
C***********************************************************************
      FTAU(1:NEQ)       = ZERO                  
      FTAU(NPF:NPL)     = DCVDTA (1:NP) 
      CALL DGESL(XTOT,NEQ,NEQ,IW,FTAU,0)
C***********************************************************************
C     calculate overall residual 
C***********************************************************************
      CALL DGESL(XTOT,NEQ,NEQ,IW,HELP,0)
      DEL(1:NEQ) = HELP(1:NEQ)
C     write(*,*) DEL(1:NEQ),'OOOOOOOOO'
C---- Note: the approximate Jacobian for this system is -I
C***********************************************************************
C     Regular exit
C***********************************************************************
      RETURN
C***********************************************************************
C
C***********************************************************************
  910 IERR = 1
      WRITE(NFILE6,*) ' EGVBAS returned to NEWDES with error:',IERBAS
      RETURN
  920 IERR = 2
      WRITE(NFILE6,*) ' decomposition of N_f failed in NEWDES',  
     1                ' possible reason: zero eigenvalues in N_f'
      DO 132 I=1,NEQ
      WRITE(NFILE6,*) 'ar,ai',AR(I),AI(I)
  132 CONTINUE
      RETURN
  930 IERR = 3
      WRITE(NFILE6,*) ' decomposition of C Z_f failed in NEWDES'
      RETURN
  980 IERR = 9
      RETURN
C***********************************************************************
C     END OF EVBDES
C***********************************************************************
      END
