      SUBROUTINE ANAHIERA(NEQ,NC,NZEV,NVERT,NPROP,PROP,
     1           RPAR,IPAR,LRW,RW,LIW,IW,IERR,NEQL,NEQU,SYMPRP)
C***********************************************************************
C
C     SUBROUTINE TO ANALYZE THE HIERACHICAL STRUCTURE OF A
C     LOWER AND HIGHER HYDROCARBON CHEMICAL SYSTEM TO JUDGE
C     THE CONSISTENCE OF A HIGHER HYDROCARBON ILDM WITH A LOWER 
C     HYDROCARBON ILDM (FOR THE SAME ELEMENT COMPOSITION, 
C     ENTHAPLY AND PRESSURE).
C     
C     IF THE HIGHER HYDROCARBON ILDM IS SAID TO BE ALMOST EQUAL TO
C     THE LOWER HYDROCARBON ILDM, THE LOWER HYDROCARBON ILDM CAN BE 
C     USED INSTEAD
C
C     THE JACOBIANS OF THE LOWER AND THE HIGHER SYSTEM ARE ANALYZED
C     TO CHECK IF G_y, H_y, G AND H ARE ZERO.
C     THE LAMBDA RATIO OF THE FASTEST NOT DECOUPLED EIGENVALUE OF THE 
C     LOWER SYSTEM WITH THE SLOWEST EIGENVALUE OF THE ADDITIONAL
C     REACTIONS IS COMPUTED.
C
C     WRITTEN BY: CHRISTIAN SEIBEL 12.07.01
C
C***********************************************************************
C
C    INPUT:
C    ------
C    NEQL: Number of equations of the starting solution (nspeclow+h,p)
C    NEQU: Number of equations that add for the higher hydrocarbon
C    PROP: Properties of the starting solution
C
C    OUTPUT:
C    -------
C    VERLAM: LAMBDA RATIO OF THE (NC  )-SLOWEST EV OF THE LOWER SYSTEM
C            WITH THE SLOWEST OF THE HIGHER SYSTEM REACTIONS
C    GNORM: NORM OF THE VECTOR OF RATE CONSTANTS G
C    HNORM: NORM OF THE VECTOR OF RATE CONSTANTS H
C    
C    FORT.68: OUTPUT FOR VALUES OF G,H,G_y,H_y THAT ARE NOT ZERO 
C             AND ABOVE A CERTAIN LIMIT. THE POSTITION OF THESE 
C             'BAD' VALUES TELLS YOU IN WHAT SPECIES THERE MIGHT BE
C             AN ERROR.
C             IT ALSO SAYS IF THERE ARE EIGENVALUES IN THE HIGHER SYSTEM
C             THAT ARE SLOWER THAN THE DECOUPLED EIGENVALUES OF THE
C             LOWER SYSTEM.
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      PARAMETER (ONE = 1.0D0, ZERO = 0.0D0)
C***********************************************************************
C     ARRAYS     
C***********************************************************************
      DOUBLE PRECISION, DIMENSION(NEQ)          :: SMF
      DIMENSION RW(LRW),IW(LIW)
      DIMENSION RPAR(*),IPAR(*)
      DIMENSION PROP(NPROP,NVERT)
      DIMENSION RATE(NEQ),RATEL(NEQ)
      DIMENSION XJAC(NEQ,NEQ),XJACLOW(NEQ,NEQ)
      DIMENSION GY(NEQL,NEQL),FY(NEQL,NEQL),
     1          FYGY(NEQL,NEQL),HZ(NEQU,NEQU)
      DIMENSION G(NEQL-2)
      DIMENSION EVFYR(NEQL), EVFYI(NEQL)
      DIMENSION EVFYGYR(NEQL), EVFYGYI(NEQL)
      DIMENSION XLADIF(NEQL)
      DIMENSION EVHZR(NEQU), EVHZI(NEQU)
      DIMENSION VL(NEQ,NEQ), VR(NEQ,NEQ), VS(NEQ,NEQ)
      LOGICAL TW(LTW),TEST
      CHARACTER(LEN=*)  SYMPRP(NPROP)
      COMMON/BLOCOR/IIRAT,IISMF,IISMFP,IIRV,IIPRO,IIPNU,IILAM,NPROOT
C***********************************************************************
C     USEFUL CONSTANTS
C***********************************************************************
      DATA NFIOUT/6/,NFIL68/68/
      TEST = .FALSE.
      WRITE(NFIOUT,*) ' START -- analysis of the lower- and higher-',
     1                'dimensional System'
      ICLEN=LEN(SYMPRP(1))
C***********************************************************************
C     Rename Symbols in SYMPRP
C***********************************************************************
      SYMPRP(IIRV-1+3) = 'RatLam  '//REPEAT(' ',ICLEN-8)
      SYMPRP(IIRV-1+4) = 'GNorm   '//REPEAT(' ',ICLEN-8)
      SYMPRP(IIRV-1+5) = 'HNorm   '//REPEAT(' ',ICLEN-8)
      SYMPRP(IIRV-1+6) = 'GYNorm  '//REPEAT(' ',ICLEN-8)
      SYMPRP(IIRV-1+7) = 'HYNorm  '//REPEAT(' ',ICLEN-8)
      SYMPRP(IIRV-1+8) = 'LNorm   '//REPEAT(' ',ICLEN-8)
C***********************************************************************
C     IPAR(99)=1 : Calculate Jacobian of the higher System
C             =2 : Calculate Jacobian of the lower System by setting 
C                  Rate Constants of additional Reactions = 0
C***********************************************************************
      DO 100 ILOW=1,NVERT
C---- Copy h,p,phi from PROP in SMF
      SMF(1:NEQ) = PROP(IISMF:IISMF-1+NEQ,ILOW)
C***********************************************************************
C     Calculate Jacobian of the higher System with additional 
C     species = zero
C     Achtung Einheiten: RATE [mol/(m^3*s)]
C***********************************************************************
      IPAR(99)=1
      WRITE(*,*) ' WHERE SHOULD THAT BE IMPLEMENTED? ' 
      STOP
      CALL MASJAC(NEQ,TIME,SMF,RATE,XJAC,RPAR,IPAR,IERJAC)
      IF(IERJAC.GT.0) GOTO 995
C***********************************************************************
C     Calculate Jacobian of the lower System with additional 
C     species = zero 
C     and additional reactions = zero
C***********************************************************************
      IPAR(99)=2
      CALL MASJAC(NEQ,TIME,SMF,RATEL,XJACLOW,RPAR,IPAR,IERJAC)
      IF(IERJAC.GT.0) GOTO 995
C---- Calculate G and G_Y
      GY(1:NEQL,1:NEQL) = XJAC(1:NEQL,1:NEQL)-XJACLOW(1:NEQL,1:NEQL)
      G(1:(NEQL-2)) = RATE(1:(NEQL-2))-RATEL(1:(NEQL-2))
C***********************************************************************
C     Check if Elements of G and H are Zero
C***********************************************************************
C---- Output in fort.68
      IF(TEST)THEN
      WRITE (NFIL68,*) 'NUMBER OF POINT:', ILOW
      DO 10 I=1,(NEQL-2)
        IF(DABS(G(I)).GT.1.0D-04) THEN
          WRITE (NFIL68,*) 'G(',I,') NICHT NULL!', G(I)
        ENDIF
 10   CONTINUE
      DO 20 I=(NEQL-1),(NEQ-2)
         IF(DABS(RATE(I)).GT.1.0D-04) THEN
           WRITE (NFIL68,*) 'H(',I-(NEQL-2),') NICHT NULL!', RATE(I)
         ENDIF
 20   CONTINUE
      ENDIF
C***********************************************************************
C     Calculate the norm of G, H and F
C***********************************************************************
C---- Norm of F
      FNORM = ZERO
      FNORM = DSQRT(DDOT(NEQL-2,RATEL,1,RATEL,1))
C---- Norm of G
      GNORM = ZERO
      GNORM = (DSQRT(DDOT(NEQL-2,G,1,G,1)))/FNORM
C---- Norm of H
      SUMDEL = ZERO
      DO 40 I=(NEQL-1),(NEQ-2)
         SUMDEL = SUMDEL + RATE(I)**2
 40   CONTINUE
      HNORM = (DSQRT(SUMDEL))/FNORM
C***********************************************************************
C     Check if Elements of Gy and Hy are Zero
C***********************************************************************
      IF(TEST)THEN
      DO 60 I=1,NEQL
      DO 60 J=1,NEQL
         IF(DABS(GY(I,J)).GT.1.0D+02) THEN
            WRITE (NFIL68,*) 'GY(',I,J,') NICHT NULL!',GY(I,J)
         ENDIF
 60   CONTINUE
      DO 80 I=NEQL+1,NEQ
      DO 80 J=1,NEQL
         IF(DABS(XJAC(I,J)).GT.1.0D+02) THEN
            WRITE (NFIL68,*) 'HY(',I-NEQL,J,') NICHT NULL!',XJAC(I,J)
         ENDIF
 80   CONTINUE
      ENDIF
C***********************************************************************
C     Calculate the norm of Gy and Hy
C***********************************************************************
C---- Norm of FY
      SUMDEL = ZERO
      DO 130 I=1,NEQL
      DO 130 J=1,NEQL 
         SUMDEL = SUMDEL + XJACLOW(I,J)**2
 130  CONTINUE
      FYNORM = DSQRT(SUMDEL)
C---- Norm of GY
      SUMDEL = ZERO
      DO 110 I=1,NEQL
      DO 110 J=1,NEQL
         SUMDEL = SUMDEL + GY(I,J)**2
 110  CONTINUE
      GYNORM = (DSQRT(SUMDEL))/FYNORM
C---- Norm of HY
      SUMDEL = ZERO 
      DO 120 I=NEQL+1,NEQ
      DO 120 J=1,NEQL
         SUMDEL = SUMDEL + XJAC(I,J)**2
 120  CONTINUE
      HYNORM = (DSQRT(SUMDEL))/FYNORM     
C***********************************************************************
C     Calculation of the eigenvalues
C***********************************************************************
      MDIM=IPAR(11)
C---- Eigenvalues of FY
      FY(1:NEQL,1:NEQL) = XJACLOW(1:NEQL,1:NEQL)
      CALL HOMEVC(-1,NEQL,FY,NEQL,MDIM,RDIM,'D','T',
     1     EVFYR,EVFYI,VL,NEQL,VR,NEQL,VS,NEQL,SRCONE,SRCONV,
     1     RW,LRW,IW,LIW,TW,LTW,RPAR,IPAR,NFIOUT,IERR)
      IF(IERR.GT.0) GOTO 991
      SRLAM = EVFYR(NC)
C---- Eigenvalues of HZ
      HZ(1:NEQU,1:NEQU)=XJAC(NEQL+1:NEQ,NEQL+1:NEQ)
      CALL HOMEVC(-1,NEQU,HZ,NEQU,MDIM,RDIM,'D','T',
     1     EVHZR,EVHZI,VL,NEQU,VR,NEQU,VS,NEQU,SRCONE,SRCONV,
     1     RW,LRW,IW,LIW,TW,LTW,RPAR,IPAR,NFIOUT,IERR)
      IF(IERR.GT.0) GOTO 992
      FRLAM = EVHZR(1)
C---- Eigenvalues of FY+GY
      FYGY(1:NEQL,1:NEQL) = XJAC(1:NEQL,1:NEQL)
      CALL HOMEVC(-1,NEQL,FYGY,NEQL,MDIM,RDIM,'D','T',
     1     EVFYGYR,EVFYGYI,VL,NEQL,VR,NEQL,VS,NEQL,SRCONE,SRCONV,
     1     RW,LRW,IW,LIW,TW,LTW,RPAR,IPAR,NFIOUT,IERR)
      IF(IERR.GT.0) GOTO 994
C---- Influence of G_y on the eigenvalues of F_y
      XLADIF(1:NEQL) = 0.0D0
      XLADIF(1:NEQL) = (EVFYR(1:NEQL)-EVFYGYR(1:NEQL))/
     1  (DMAX1(DABS(EVFYR(1:NEQL)),DABS(EVFYGYR(1:NEQL)),1.0D-08))
      SUMDEL = ZERO
      DO 170 I=NZEV+1,NEQL
         SUMDEL = SUMDEL + XLADIF(I)**2
 170  CONTINUE
      XLNORM = DSQRT(SUMDEL)
C***********************************************************************
C     Check for lambda(Hz) < lambda_slow(Fy)
C***********************************************************************
      IF(SRLAM.LE.FRLAM.AND.TEST) WRITE(NFIL68,*) 'EIGENVALUE OF HIGHER 
     1                            SYSTEM SLOWER!', SRLAM, FRLAM         
C---- Verhaeltnis der Eigenwerte
      IF (SRLAM.GE.ZERO) SRLAM = -1.0D-08 
      VERLAM = FRLAM / SRLAM
      PROP(IIRV-1+3,ILOW) = VERLAM
      PROP(IIRV-1+4,ILOW) = GNORM
      PROP(IIRV-1+5,ILOW) = HNORM 
      PROP(IIRV-1+6,ILOW) = GYNORM
      PROP(IIRV-1+7,ILOW) = HYNORM       
      PROP(IIRV-1+8,ILOW) = XLNORM       
 100  CONTINUE
C---- IPAR zuruecksetzen
      IPAR(99)=1
      WRITE(NFIOUT,*) ' END of analysis'
      RETURN
C***********************************************************************
C     Error exits
C***********************************************************************
 991  CONTINUE
      WRITE(NFIOUT,*) 'ERROR - calculation of eigenvalues',
     1                ' of F_Y failed', IERR
      GOTO 999
 992  CONTINUE
      WRITE(NFIOUT,*) 'ERROR - calculation of eigenvalues',
     1                ' of H_Z failed', IERR
      GOTO 999
 994  CONTINUE
      WRITE(NFIOUT,*) 'ERROR - calculation of eigenvalues',
     1                ' of F_Y+G_Y failed', IERR
      GOTO 999
 993  CONTINUE
      WRITE(NFIOUT,*) ILOW, 'LANGSAMERE ZEITSKALA IN HZ!'
      GOTO 999
 995  CONTINUE
      IERR = IERJAC
      WRITE(NFIOUT,996) IERR
 996  FORMAT(' ',' Computation of Jacobian failed, IERR =',I3)
      GOTO 999 
 999  STOP
C***********************************************************************
C     End of -ANAHIERA-
C***********************************************************************
      END
      SUBROUTINE MANSEN(ndim,nc,nzev,neq,SYMPRP,nfile6,rpar,ipar)
c***********************************************************************
c                                                                      *
c  ******************************************************************  *
c  *  ROUTINE FOR THE CALCULATION OF SENSITIVITIES OF MANIFOLDS     *  *
c  ******************************************************************  *
c                                                                      *
c                    20/10/2003 Karin K�nig                            *
c                                                                      *
c***********************************************************************
c                                                                      *
c     INPUT   : neq     = number of equations (nspec+2)                *
c               ndim    = dimension of ILDM                            *
c               nreac   = number of reactions                          *
c               nsens   = number of reactions varied in sensitivity    *
c                         test                                         *
c               ncel1   = number of ILDM cells                         *
c----------------------------------------------------------------------*
c     INTERNAL: 
c               SENS    = array containing the calculated              *
*                         sensitivities                                *
c               PROPS   = new properties (with sensitivities)          *
c               nprops  = number of new properties (nprop+sen.)        *
c               SMF     = state vector of one ILDM point               *
c               nsens   = number of sen. parameters                    *
c               nvert   = number of ILDM points                        *
c               nfisen  = file for TECPLOT output of results           *
c               GPSI    = G_psi for actual state vector                *
c               GPSIM   = product of G_psi and G_psi_transposed        *
c               GPSII   = Moore-Penrose pseudo-inverse of GPSI         *
c               GP      = G_p for the actual state vector, with p      *
c                         as the parameters (rate coefficients)        *
c----------------------------------------------------------------------*
c     OTHER SUBROUTINES NEEDED:                                        *
c               TABIN, EGVBAS, MASRES, SENOUT, DGEMM, DGEINV           *
c----------------------------------------------------------------------*
c     OUTPUT  : fort.55 = file with sensitivity on each point          *
c                         (TECPLOT format), additionally the properties*        c                         of the manifold are contained                *
c                                                                      *
c***********************************************************************
c***********************************************************************
c     STORAGE ORGANISATION
c***********************************************************************
      IMPLICIT double precision(a-h,o-z)
      INCLUDE 'homdim.f'
      PARAMETER(zero=0.0d0,inull=0)
      PARAMETER(one=1.0d0)
c
      DOUBLE PRECISION, DIMENSION(neq,neq) :: SCHUR,XJAC,AVL,AVR
      DOUBLE PRECISION, DIMENSION(neq)         :: VRS,HELP
      DOUBLE PRECISION, DIMENSION(neq-nc) :: HELPG,GUG
      DOUBLE PRECISION, DIMENSION(neq,neq) :: YTROW,YTCOL
      DOUBLE PRECISION, DIMENSION(neq,neq) :: ERVROW,ERVCOL
      DOUBLE PRECISION, DIMENSION(neq) :: DEL,AR,AI
      EXTERNAL MASRES,MASJAC
      CHARACTER*8 SYMPRP(mnprop)
      DIMENSION SMF(neq)
      DOUBLE PRECISION, DIMENSION(neq,neq-nc) :: PSKA
      DOUBLE PRECISION, DIMENSION(neq-nc,neq-nc) :: ZFP,ZFPP
      DOUBLE PRECISION, DIMENSION(neq-nc,neq-nc) :: ZFPA
      DOUBLE PRECISION, DIMENSION(neq-nc,neq-nc) :: GPSIM
      DOUBLE PRECISION, DIMENSION(neq,neq-nc) :: GPSII      
      DIMENSION GPSI(neq-nc,neq)                     
      DOUBLE PRECISION, DIMENSION(neq-nc) ::  WORK1
      INTEGER         , DIMENSION(neq-nc) ::  IWORK        
      DOUBLE PRECISION, DIMENSION(neq,mnrea+2) ::  GP
      DIMENSION PROP(mnprop,nverm)
      DOUBLE PRECISION, DIMENSION(mnprop+(neq)*(mnrea+2)) :: PROPO
      DOUBLE PRECISION, DIMENSION(neq,mnrea) :: SENS
c maximum dimension (mnprop+mnequ,mnrea,nverm) exceeds allowed
c storage space, if dimension is varied also change this in
c the call of subroutine SENOUT and in error exit 500
      DOUBLE PRECISION, DIMENSION(200,100,1000) :: PROPS
      DIMENSION ICORD(ndimm,nverm)
      DIMENSION IVERT(n2dimm*ncelm)
      DIMENSION IRET(nverm),IY(ndimm*ncelm)
      DIMENSION CMAT(mncov*mnequ)
      DOUBLE PRECISION, DIMENSION(neq-nc,neq) :: HELPP,GUGP
      DOUBLE PRECISION, DIMENSION(neq-nc,mnrea+2) :: HELPA
      DOUBLE PRECISION, DIMENSION(neq-nc,mnrea+2) :: GUGA
      DIMENSION IW(2*mnequ),RW(mnequ*mnequ)
      DIMENSION RPAR(*),IPAR(*)
      PARAMETER (LLLIW=2*mnequ,LLLRW=mnequ*mnequ)
c***********************************************************************
      DATA etad/1.D-2/,atol/1.D-6/
      DATA factor/1.D-2/
c***********************************************************************
c     COMMMON BLOCKS
c***********************************************************************
      COMMON/BSENS/LSMAX,nsens,nrsens,nisens,npsens,
     1             IRSENS,IISENS,IPSENS,ISENSI(mnsen)
      COMMON/ILDMOP/IPARA,ISTAOP,MANPAR,NOFORM,ANAFUE,INMAN,lasloo,
     1              ICALTR,MASENS,IUPVER,NTRANS
c***********************************************************************
c     INITIALIZATION
c***********************************************************************
      lrw        = LLLRW  
      liw        = LLLIW  
      GPSI       = zero
      GPSII      = zero
      GP         = zero
      GPSIM      = zero
      PROP       = zero
      SENS       = zero
      PROPS      = zero
      PROPO      = zero
      PSKA       = zero
      ZFP        = zero
      GUG        = zero
      GUGP       = zero
      GUGA       = zero
      ZFPA       = zero
      HELPG      = zero
      HELPA      = zero
      HELPP      = zero
      write(*,*) '****BEGIN OF SUBROUTINE MANSEN****'              
c***********************************************************************
c     INITIALIZATION OF TECPLOT OUTPUT FILE
c***********************************************************************
      nfisen = 55
      REWIND nfisen
c***********************************************************************
c     INPUT OF ILDM TABLE
c***********************************************************************
      nspec = neq  - 2
      write(*,*) '****input of given ILDM table****'
      OPEN(94,status='unknown')
      n2dim = 2**ndim
      CALL IOGPAR(-1,94,NPROP,NCEL1,NCEL2,NVERT,NDIM,N2DIM,
     1                     NSPEC,NREAC,IY,IVERT,ICORD,IRET,PROP,SYMPRP,
     1                     CMAT,mix,NC)
      CLOSE(94,status='keep')
c***********************************************************************
c     SET UP OF ARRAY SYMBOLS
c***********************************************************************
      nprops = neq * nsens
c***********************************************************************
c     SET UP MATRIX PSKA FOR CALCULATION OF G
c***********************************************************************
      neqnc = neq - nc
      ncs = nc + 1
      DO i =ncs,neq
        DO k=1,neqnc
           IF(i-nc.EQ.k) THEN 
             PSKA(i,k) = one
           ELSE
             PSKA(i,k) = zero
           ENDIF
        ENDDO
      ENDDO
c***********************************************************************
c     LOOP OVER ALL ILDM POINTS (1111)
c***********************************************************************
      DO 1111 ipoint=1,nvert
c***********************************************************************
c     COPY STATE VECTOR FROM PROP TO SMF FOR SENSITIVITY CALCULATION
c***********************************************************************
        SMF    = zero
        ndimnb = ndim + 1
        ndimna = ndim + neq
        SMF(1:neq) = PROP(ndimnb:ndimna,ipoint)
c***********************************************************************
c     COPY STATE VECTOR INTO PROPS FOR OUTPUT 
c***********************************************************************
        PROPS(1:nprop,1,ipoint) = PROP(1:nprop,ipoint)
c***********************************************************************
c***********************************************************************
c     CALCULATION OF SENSITIVITY         
c***********************************************************************
c***********************************************************************
c STEP 1: Calculate G = (Z~_f*P)^(-1)*Z~_f * F for the ILDM point
        write(*,*) '****calculation started for ILDM point nr.',
     1                  ipoint,'****'
c calculate Z~_f:            
        mdim   = nc
        ipri   = 0
        ierbas = 0
        rdim = 0
        time = 0
        CALL EGVBAS(1,0,mdim,rdim,nzev,neq,MASRES,MASJAC,time,SMF,
     1              ERVCOL,ERVROW,YTCOL,YTROW,
     1              AR,AI,AVR,AVL,XJAC,SCHUR,
     1              RPAR,IPAR,ipri,nfile6,ierbas)
        IF(ierbas.GT.0) GOTO 100
c
c calculate F:
        iemr = 0
        CALL MASRES(neq,timdum,SMF,VRS,rpar,ipar,iemr)
        IF(iemr.NE.0) GOTO 600
c
        nc    = IPAR(11)
c
c calculate Z~_f*F:
        HELPG(1:neqnc) = MATMUL(YTROW(ncs:neq,1:neq),VRS(1:neq))
c
c calculate Z~_f*P: 
        ZFP(1:neqnc,1:neqnc) = MATMUL(YTROW(ncs:neq,1:neq),
     1                                PSKA(1:neq,1:neqnc))
c       
c calculate (Z~_f*P)^(-1):
        ierr = 0
        CALL DGEINV(neqnc,ZFP,WORK1,IWORK,ierr)
        IF(ierr.NE.0) GOTO 400
c
c calculate G = (Z~_f*P)^(-1)*Z~_f*F:
        GUG(1:neqnc) = MATMUL(ZFP(1:neqnc,1:neqnc),HELPG(1:neqnc))
c
c STEP 2: Perturbation of the components of the 
c         state vector            
c         (step follows ildmlib.f / subroutine HOMJAC)
        DO 101 k = 1,neq
           up = etad * DMAX1(DABS(SMF(k)),atol) 
           w  = SMF(k) 
           SMF(k) = w + up

c STEP 3: Calculation of G on point with perturbated state vector  
c         for a separate perturbation of each component of the
c         state vector
           mdim = nc
           ipri = 0
           ierr = 0

         CALL EGVBAS(1,0,mdim,rdim,nzev,neq,MASRES,MASJAC,time,SMF,
     1                 ERVCOL,ERVROW,YTCOL,YTROW,
     1                 AR,AI,AVR,AVL,XJAC,SCHUR,
     1                 RPAR,IPAR,ipri,nfile6,ierbas)
           IF(ierbas.GT.0) GOTO 100
c
           iemr = 0
           CALL MASRES(neq,timdum,SMF,VRS,RPAR,IPAR,iemr)
           IF(iemr.NE.0) GOTO 600

           nc    = IPAR(11)

           HELPP(1:neqnc,k) = MATMUL(YTROW(ncs:neq,1:neq),
     1                           VRS(1:neq))
c           
           ZFPP(1:neqnc,1:neqnc) = MATMUL(YTROW(ncs:neq,1:neq),
     1                                PSKA(1:neq,1:neqnc))
c
           ierr = 0
           CALL DGEINV(neqnc,ZFPP,WORK1,IWORK,ierr)
           IF(ierr.NE.0) GOTO 400
c
           GUGP(1:neqnc,k) = MATMUL(ZFPP(1:neqnc,1:neqnc),
     1                            HELPP(1:neqnc,k))

           SMF(k) = w
 

c STEP 4: Calculation of G_psi for the ILDM point using the results
c         for the non-perturbated and perturbated state vectors,
c         G_psi contains the solutions for perturbations of each single
c         component of the state vector (matrix with dimensions
c         (neq-nc) x neq) 

           GPSI(1:neqnc,k) = (GUGP(1:neqnc,k) - GUG(1:neqnc)) / up
             
  101      CONTINUE

c STEP 5: Calculation of the pseudo-inverse of G_psi   

           GPSIM(1:neqnc,1:neqnc) = MATMUL(GPSI(1:neqnc,1:neq),
     1                              TRANSPOSE(GPSI(1:neqnc,1:neq)))
c
           ierr = 0
           CALL DGEINV(neqnc,GPSIM,WORK1,IWORK,ierr)
           IF(ierr.NE.0) GOTO 400
c
           GPSII(1:neq,1:neqnc) = MATMUL(TRANSPOSE(GPSI(1:neqnc,1:neq)),
     1             GPSIM(1:neqnc,1:neqnc))


c STEP 6: Calculation of perturbated parameters and G for perturbated
c         parameters
c loop over all parameters:
           DO 102 i = 1, nsens  

c perturbation of the parameters:
             RPAR(i) = RPAR(i) + factor  
c in MECHJA RPAR is only used, when nsens.ne.0
             nsens = IPAR(4)
             IF (nsens.EQ.0) GOTO 300
c calculate Z~_f:
             mdim = nc
             ipri = 0
             ierbas = 0
      CALL EGVBAS(1,0,mdim,rdim,nzev,neq,MASRES,MASJAC,time,SMF,
     1                   ERVCOL,ERVROW,YTCOL,YTROW,
     1                   AR,AI,AVR,AVL,XJAC,SCHUR,
     1                   RPAR,IPAR,ipri,nfile6,ierbas)
             IF(ierbas.GT.0) GOTO 100
c
             iemr = 0
             CALL MASRES(neq,timdum,SMF,VRS,RPAR,IPAR,iemr)
             IF(iemr.NE.0) GOTO 600
c
             nc    = IPAR(11)
             HELPA(1:neqnc,i) = MATMUL(YTROW(ncs:neq,1:neq),
     1                                  VRS(1:neq))
c
             ZFPA(1:neqnc,1:neqnc) = MATMUL(YTROW(ncs:neq,1:neq),
     1                                      PSKA(1:neq,1:neqnc))
c
             ierr = 0
             CALL DGEINV(neqnc,ZFPA,WORK1,IWORK,ierr)
             IF(ierr.NE.0) GOTO 400
c
             GUGA(1:neqnc,i) = MATMUL(ZFPA(1:neqnc,1:neqnc),
     1                                HELPA(1:neqnc,i))

c
c reset of perturbation:
             RPAR(i) = RPAR(i) - factor


c STEP 7: Calculation of G_p for each parameter with the original G for
c         non-pertubated parameters and the new calculated G
c         for the pertubated parameters
          
             GP(1:neqnc,i) = (GUGA(1:neqnc,i) - GUG(1:neqnc))
     1                         / factor
c
c STEP 8: Calculate sensitivity as a negative product of the pseudo-
c         inverse of G_psi and G_p

             SENS(1:neq,i) = - MATMUL(GPSII(1:neq,1:neqnc),
     1                                    GP(1:neqnc,i))

c alternative calculation: SENS above has to be commented, so does
c the 102 loop below DGEMM-call
c 102 CONTINUE
c            CALL DGEMM('n','n',neq,nsens,neqnc,
c    1                   one,GPSII,mnequ,GP,neq,zero,SENS,neq)
c            SENS(1:neq,nsens) = - MATMUL(GPSII(1:neq,1:neqnc),
c    1                                    GP(1:neqnc,nsens))
c***********************************************************************
c End of loop over all parameters
  102       CONTINUE
c***********************************************************************
c set up array with new properties for output (properties of the 
c ILDM point plus sensitivities), first part of array with properties
c was defined earlier on
              nprop2 = nprop + neq
              IF(nprop2.GT.200.OR.nsens+1.GT.100.OR.nvert.GT.1000) 
     1          GOTO 500
              PROPS(1:neq,2:nsens+1,ipoint) = 
     1          SENS(1:neq,1:nsens)
c***********************************************************************
c     END OF LOOP OVER ALL ILDM POINTS
c***********************************************************************
      SMF   = zero
      HELP  = zero
      HELPG = zero
      HELPA = zero
      HELPP = zero
      GP    = zero
      GPSI  = zero
      GPSIM = zero
      GPSII = zero
 1111 CONTINUE
c***********************************************************************
c     OUTPUT FOR TECPLOT
c***********************************************************************
c array with 3 dim. PROPS must be reduced to a 2 dim. array,
c if dimension of PROPS is adjusted, the call must also be corrected!
             npropp = nprop + neq * nsens 
             OPEN(nfisen,status='unknown')
             CALL SENOUT(nfisen,npropp,SYMPRP,nvert,PROPS,
     1                   ICORD,ndim,n2dim,npropp,IVERT,nprop2,
     1                   nsens,nprop,neq,ncel1)
             CLOSE(nfisen,status='keep')
c***********************************************************************
c     REGULAR EXIT
c     END OF - MANSEN -
c***********************************************************************
      RETURN
c***********************************************************************
c     ERROR EXITS
c***********************************************************************
  100 ierr = 1
      WRITE(nfile6,*) ' EGVBAS returned with error:',ierbas
      STOP
  300 WRITE(nfile6,*) ' ERROR in MECHJA, RPAR is not used as
     1                  nsens is equal 0: ',nsens
      STOP
  400 WRITE(nfile6,*) ' ERROR in matrix inversion'
      STOP
  500 WRITE(nfile6,*) ' ERROR in mansen.f: dimensions of array PROPS
     1                  are exceeded:'
      WRITE(nfile6,*) ' given dimensions of PROPS: (200,100,1000)'
      WRITE(nfile6,*) ' real dimensions of PROPS: 
     1                  (',nprop2,nsens,nvert,')'
      STOP
  600 WRITE(nfile6,*) ' ERROR in masres'
      STOP
c***********************************************************************
      END SUBROUTINE MANSEN
c
      SUBROUTINE SENOUT(nfisen,npropp,SYMPRP,nvert,PROPS,
     1                  ICORD,ndim,n2dim,ncell,IVERT,nprop2,
     1                  nsens,nprop,neq,ncel1)
c***********************************************************************
c                                                                      c
c     ROUTINE FOR THE TECPLOT OUTPUT OF THE CALCULATED SENSITIVITIES   c
c                                                                      c
c***********************************************************************
      IMPLICIT DOUBLE PRECISION(a-h,o-z)
      INCLUDE 'homdim.f'
      PARAMETER(zero=0.0d0)
      PARAMETER(one=1.0d0)

      DOUBLE PRECISION, DIMENSION (200,100,1000) :: PROPS
      DIMENSION PROPO(npropp,nvert)
      DIMENSION IVERT(n2dim,ncel1),ICORD(ndim,nvert)
      CHARACTER(LEN=*) SYMPRP(mnprop)
      CHARACTER*8 TABSYM(ndimm),TABCVS(ndimm)
c***********************************************************************
c     INITIALIZATION
c***********************************************************************
      PROPO = zero
c***********************************************************************
c array with 3 dim. PROPS must be reduced to a 2 dim. array PROPO
c by combination of the first two dimensions of PROPS as the first
c dimension of PROPO
      DO 10 i=1,nvert
          PROPO(1:nprop,i) = PROPS(1:nprop,1,i)
          ik = zero    
          ii = one
        DO 11 k=2,nsens+1
          nprop1 = nprop + 1
          neqnc1 = nprop1 + neq * (ii - 1)    
          neqnc2 = nprop + neq + neq * ik
          PROPO(neqnc1:neqnc2,i) = PROPS(1:neq,k,i)
          ik = ik + one
          ii = ii + one
  11    CONTINUE
 10   CONTINUE


c output of data into file nfisen (fort.55)

      DO i=1,ndim
        WRITE(TABSYM(i),1)i
        WRITE(TABCVS(i),2)i
      ENDDO
c***********************************************************************
c     SET UP OF ARRAY FOR OUTPUT OF SYMBOLS
c***********************************************************************
      WRITE(nfisen,5) (TABSYM(i),i=1,ndim),
     1     (TABCVS(i),i=1,ndim),
     1     (SYMPRP(I),I=1,nprop)
      WRITE(nfisen,51)   (I,I=1,neq*nsens)
      IF(ndim.EQ.1) WRITE(nfisen,6) nvert,ncel1
      IF(ndim.EQ.2) WRITE(nfisen,6) nvert,ncel1
      IF(ndim.EQ.3) WRITE(nfisen,7) nvert,ncel1
      IF(ndim.GE.4) WRITE(nfisen,8) nvert,ncel1
      DO 20 i=1,nvert
         WRITE(nfisen,4) (FLOAT(ICORD(j,i)),j=1,ndim),
     1        zero,zero,
     1        (PROPO(k,i),k=1,npropp)
 20   CONTINUE
      IF(ndim.LE.3) THEN
         DO 30 i=1,ncel1
            IF(ndim.EQ.1) THEN
      WRITE(nfisen,3) IVERT(1,i),IVERT(2,i),IVERT(2,i),IVERT(1,i)
            ELSE IF(ndim.EQ.2) THEN
      WRITE(nfisen,3) IVERT(1,i),IVERT(2,i),IVERT(4,i),IVERT(3,i)
            ELSE IF(ndim.EQ.3) THEN
      WRITE(nfisen,3) IVERT(1,i),IVERT(2,i),IVERT(4,i),IVERT(3,i),
     1                  IVERT(5,i),IVERT(6,i),IVERT(8,i),IVERT(7,i)
            ELSE
              STOP
            ENDIF
   30    CONTINUE
      ELSE
c***********************************************************************
c     ERROR EXIT
c***********************************************************************
        WRITE(*,*) ' ERROR IN SENOUT '
        WRITE(*,*) ' TECPLOT OUTPUT NOT POSSIBLE BECAUSE OF NDIM =',NDIM
      ENDIF
c***********************************************************************
c     FORMAT STATEMENTS
c***********************************************************************
    1 FORMAT('Tabco',I3)
    2 FORMAT('Tabcv',I3)
    3 FORMAT(10I8)
    4 FORMAT(7(1PE10.3,1X))
    5 FORMAT('VARIABLES =',/,7('"',A,'"',','))
   51 FORMAT(7('"dydp',I4,'"',:','))
    6 FORMAT('ZONE N=',I5,', E=',I5,', ET= QUADRILATERAL, F=FEPOINT')
    7 FORMAT('ZONE N=',I5,', E=',I5,', ET= BRICK, F=FEPOINT')
    8 FORMAT('ZONE N=',I6,', E=',I6)
c***********************************************************************
c     REGULAR EXIT OF SUBROUTINE SENOUT
c     END OF -SENOUT-
c***********************************************************************
      RETURN
c***********************************************************************
      END SUBROUTINE SENOUT
