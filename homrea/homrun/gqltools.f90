module gqltools
implicit none
integer, private  :: n, nc, ne, nvert, ana, nr, rpoi, nrad, ncor, ncon, cond, &
         & nspec, itdel, verb, td
integer, private, allocatable :: icref(:)
real, private :: avar, bvar
double precision, private :: tdelref, dis, backtim, tign, &
         detam, detbm, vola, volb, volab, fav, alpha, eq, sp
double precision, private, parameter :: rgas = 8.3141, patm = 101235
double precision, private, allocatable :: smfin(:,:), y(:), yi(:), smfmax(:),  &
         q(:,:) , qi(:,:), itdelref(:), rate(:)
character(len=8), private, allocatable :: snam(:)
public globql
contains
    type opt
        character(len=10) :: blockname
        integer, allocatable :: iopt(:)
        double precision, allocatable :: ropt(:)
        character(len=10), allocatable :: sopt(:), optnames(:)
        
    end type opt
    subroutine globql(job,nin,xmol,symprp,rpar,ipar,ierr)

!*************************************************************************************
!
!   subroutine for global analysis, gql calculation and integration...
!       input:
!            job -
!            nr  -
!            ncons
!
!*************************************************************************************
        integer :: i, ni, job, ierr, nin, ndat, imaild,ilmild,ioildm, ipar(*), iref,&
                io, ic, minns, nfas
        integer, allocatable :: ierrs(:)
        double precision :: rpar(*), xmol(*), tig, tigf, tigs, tdel, &
                ql(nin,nin), qil(nin,nin),smf(nin)
        double precision, allocatable :: stem(:,:), smfp(:)
        character(len=12) :: gqlfil
        character(len=8), dimension(nin) :: symprp
        character(len=6) :: datfil, reffil
        character(len=20) :: filnam
        logical :: tw(1000)
        common/ildmpa/imaild,ilmild,ioildm
        ierr = 0
        ilmild = 1
        imaild = 3
        n = nin
        nspec = n - 2 !For now...
        nc = 7! For now... later should be calculated maybe
        ncon = ipar(7)
        eq = -1.d0
        allocate(stem(n+1,1000000),rate(nspec))
        allocate(ierrs(n-ncon-1))
        allocate(smfp(n))
        call retra(0,'',0,21,stem,nin)

        call locana(nin,stem(2:n+1,1:nin),stem(1,1:nin),'DET',rpar,ipar)
        call tecout(nin,2001,symprp,'detailed',8,stem(1:n+1,1:nvert))
        nvert = nin
        ierr = 0
        !Parameters will be read from gql configuration file
        call redconf(ierr)
        call timdel(td,nin,stem(1:n+1,1:nvert),tig,iref)

        if (ana == 2) then
                ana= 1
        else if (ana == 4) then
                ana = 5
        else
                iref = nvert
        end if

        tign = tig
        allocate(smfin(n,nvert))
        allocate(y(n),yi(n),smfmax(n))!,y(n),yi(n))
        allocate(q(n,n),qi(n,n))
        allocate(snam(nspec))
        snam = symprp(3:n)
        smfin = stem(2:n+1,1:nvert)
        !y = smfin(:,nvert)
        yi = smfin(:,1)
        tig = 0.d0
        
        call masres(n,tig,yi,y,rpar,ipar,ierr)
       
        write(*,*) 'yi', yi
        write(*,*) 'f ', y
        read(*,*)
        allocate(itdelref(n-1),icref(n-1))

        !Get the maxmimum values of the trajectory
        smfmax = (/ (maxval(abs(smfin(i,1:iref))),i=1,n) /)
        !Do analysis
        if (ana>0) then
            call globana1(job,symprp,iref,ipar,rpar,xmol,ierr)
            ana = 0
        end if
        !Subsequent integration
        write(*,*) 'n = ', n, 'ncon = ', ncon, ' nfas = n - ncon - ns'
        write(*,*) 'ns = '
        read(*,*) i

        ndat = i
        nfas = n - ncon - ndat
        if (ana==0) then
        !Determine the GQL #
            ndat = i

            nfas = n - ncon - i
            i = i + ncon
            write(datfil,'(a,i2.2)') 'GQL_',ndat
            open(unit =2003,file = datfil,form='unformatted',access='sequential')
            write(*,*) '  NF = ',nfas,' || File name: ', datfil
            write(*,*) '=================================================='
            !Now open and read GQL
            rewind 2003
            call rmat(2003,q,qi)
            close(2003)
        else if (ana==-1) then
            ! Read reference point from binary file
            ndat = i
            nfas = n - ncon - i
            i = i + ncon
            write(datfil,'(a,i2.2)') 'REF_',ndat
            open(unit=2003,file=datfil,form='unformatted',access='sequential')
            write(*,*) '  NF = ', nfas, ' || File name ', datfil
            write(*,*) '=================================================='
            !Now open and read GQL
            rewind 2003
            call repoi(2003,smfp)
            write(*,*) smfp
            read(*,*)
            close(2003)
        else if (ana==-2) then
            ! Read reference point from ASCII file
            write(*,*) 'Enter file-name to read reference point'
            read(*,*) filnam
            open(unit=2003,file=trim(filnam),status='old')
            call reapoi(2003, smfp)
            write(*,*) smfp
            read(*,*)
            close(2003)
        else if (ana==-3) then
            ! Read basis from ASCII file
            
            write(*,*) 'Enter file-name to read GQL basis'
            read(*,*) filnam
            open(unit=2003,file=trim(filnam),status='old')
            call reapoi(2003,q)
            write(*,*) q
            read(*,*)
            close(2003)
        else if (ana==-4) then
            !Read point exported from matlab
            write(*,*) 'Enter file name for reading point exported from matlab'
            read(*,*) filnam
            open(unit=2003,file=trim(filnam),status='old')
            call matlabpoi(2003,0,smfp,q,ierr)
        end if

        if (ana<0) then
            ic = 1
            minns = n-ncon
            do i=ncon+1,n-1
                write(datfil,'(a,i2.2)') 'GQL_',i-ncon
                open(unit=3000+i,file=datfil,form='unformatted',access='sequential')
                rewind 3000+i
            end do
            call detbas(ndat,smfp,symprp,tigf,tigs,ipar,rpar)
            if (maxval(ierrs)<0) then
                write(*,*) 'Something went wrong - integration with basis given failed'
                stop
            else
                i =  minns+ncon
            end if
        end if
        call intpoi(i,tdel,symprp,ipar,rpar,ierr)
        write(*,*) 'TDEL = ',tdel

        i = 0
        call retra(1,'fa.bin',6,i,stem,nin)
        call tecout(nin,4000,symprp,'fast.tec',8,stem)

        i = 0
        call retra(1,'sl.bin',6,i,stem,nin)
        call tecout(nin,4001,symprp,'slow.tec',8,stem)

        return

        end

    subroutine redconf(ierr)
        integer :: ierr, io
        open(unit=2004,file='./gql.conf',iostat=io)
        if ( io /= 0) then
            ierr = 1
            write(*,*) 'Error Reading file, IO = ', io
            return
        end if
        verb =1
        read(2004,*) ana  ! =1 > global analysis + integration
        if (ana>0) then
            read(2004,*) nr !Number of reference points
        end if
        read(2004,*) nrad ! # species for ignition
        read(2004,*) ncor ! # species for correction
        read(2004,*) td  ! =0 => igntition time; =1 => half-life time 
        return
    end subroutine redconf

    type :: opt




    end type opt
    subroutine setopt
        integer :: ierr, io
        character(len=80) :: line

        ioptnames(1) = 'pointgen'
        ioptnames(2) = 'Analysis.ns1'
        ioptnames(1) = 'Analysis.pointgen'
        ioptnames(1) = 'Analysis.pointgen'
        ioptnames(1) = 'Analysis.pointgen'
        ioptnames(1) = 'Analysis.pointgen'
        ioptnames(1) = 'Analysis.pointgen'
        ioptnames(1) = 'Analysis.pointgen'



        open(unit= 2004, file= './gql.conf', status= 'old', iostat = io)
        read(2004, *, iostat=io) line
        alllines: do while (io == 0)
            if ( line(1) == '#') cycle alllines
            if ( trim(line) == 'Analysis' ) then
                analysis: do while (line(1:3) /= 'End')
                    read(2004, *, iostat=io) line

                    if (io /= 0) exit alllines

                    if (line(1:9) == 'pointgen') then
                        read(line,fmt) iopt(1)
                    else if (line(1:2) == 'ns') then
                        read(line,fmt) iopt(2:3)
                    end if
                end do
            else if ( trim(line) == 'Integration' ) then
                integration: do while(line(1:3) /=)
                    read(2004, *, iostat=io) line

                    if (io /= 0) exit alllines

                    if(line(`1:4`) == 'traj')  then
                        read(*,line) sopt(1)
                    end if
                end do

            else if ( trim(line) == 'LocalAnalysis' ) then
                locan: do while(line(1:))

            else if( trim(line) == 'Output' ) then

            else
                read(2004, *, iostat=io) line
            end if
        end do
        



    end subroutine setopt
    subroutine globana1(job,symprp,iref,ipar,rpar,xmol,ierr)
    integer :: job,i,k,ir,ierr,iref, ic,ne, ipar(*), io, minns
    integer :: ierrs(n-ncon-1)
    double precision :: tigf, tigs, s
    double precision, dimension(n) :: smf0, smf, del0, rpar(*), xmol(*),&
            smfp
    double precision, dimension(n,n) :: a, b
    character(len=6) :: datfil
    character(len=8), dimension(n) :: symprp
    character(len=9) :: progform

                !Note that the first entry in smfin is the time

    s = 9.9d99
    itdelref = 9.9d99
    tdelref = 9.9d99
    !Progress file

    open(2002,file='live',status='new',iostat=io)
    if (io>0) then
            write(*,*) 'Previous calculation was forcefully terminated. &
                   & You might want to clean the working directory and restart &
                   & homrea to avoid errors...'
            read(*,*)
    end if
    close(2002)

    write(progform,'(a,i3,a)') '(',n-ncon-1,'(i3))'
    !Open a file to save good and bad points
    smf0 = 0
    call wpointfiles(0, smf0,symprp,0,2006,2007)
    detbm = product(smfmax)/2
    do i=ncon+1,n-1
        write(datfil,'(a,i2.2)') 'GQL_',i-ncon
        open(unit=3000+i,file=datfil,form='unformatted',access='sequential')
        rewind 3000+i
    end do

    !Open progress file
    open(unit=5000,file='prog.out')
    call maxsource(smf0,ipar,rpar,ierr)
    rewind(5000)
    detam = product(smf0)/2
    fav = sqrt(sum((/(smf0(i)**2,i=1,n) /)))

    close(5000)
    !Get some data about element conservation and similar

    ic = 0
    ir = 0

    rpoi = 0

    allpoints: do while(ir<nr)
        write(*,*) 'ic = ',ic,'ir = ',ir,'/',nr
        open(2002,file='live',status='old',iostat=k)
        if (k>0) exit allpoints
        close(2002)
        ic = ic + 1
        ierrs = 0
        if (job==1) then
            dimensions: do i=1,n
                if (ana == 1) then
                    call randomp(job,smf,del0,ipar,rpar,xmol,ierr)
                    if (ierr>0) cycle allpoints
                else if (ana == 5) then
                    call conpoi(smf,del0,iref,ipar,rpar,xmol,ierr)
                    if (ierr>0) cycle allpoints
                else if (ana == 3 .or. ana==2) then
                    call trapoi(ana,smf,del0,ne,ipar,rpar,xmol,ierr)
                    if (ierr>0) cycle allpoints
                end if

                rpoi = 1234
                b(:,i) = smf
                a(:,i) = del0
            end do dimensions
            call calcgql1(0,a,b,ic,ipar,rpar,ierrs)

            if (ierrs(1)>3) then
                    cycle allpoints
            else
                    ir = ir + 1
            end if
        else if (job==2) then
            if (ana == 1) then
                call randomp(rpoi,smf,del0,ipar,rpar,xmol,ierr)
                if (ierr>0) cycle allpoints
            else if (ana == 2 .or. ana == 3) then
                call trapoi(ana,smf,del0,ne,ipar,rpar,xmol,ierr)
                if (ierr>0) cycle allpoints
            else if (ana == 5) then
                    call conpoi(smf,del0,iref,ipar,rpar,xmol,ierr)
                    if (ierr>0) cycle allpoints
            end if

            rpoi = 1234
            smfp = smf
            minns = n-ncon
            call calcgql2(smf,symprp,tigs,tigf,ic,minns,ipar,rpar,ierrs)
            open(unit=5000,file='prog.out')
            write(5000,progform) ierrs
            close(5000)
            if (maxval(ierrs)<0) then
                    cycle allpoints
            else
                    s = 0.0d0
                    call wpointfiles(1,smfp,symprp,minns,2006,0)
                    ir = ir + 1
            end if
        end if
    end do allpoints


    open(unit=2002,file='live',iostat=io)
    if (io==0) close(2002,status='delete')
    close(5000)
    close(2006)
    close(2007)
    do i=ncon+1,n-1
        close(3000+i)
    end do

    ! Write statistics
    open(unit=1133,file='stat.md')
    write(1133,*) '# Global analysis stats'
    write(1133,*) 'n = ', n, ', ncon', ncon
    write(1133,*) 'Generated GQLs from ', ic, 'reference points; kept ', ir
    write(1133,*) ' '
    write(1133,*) '## Tdel over specific i '
    write(1133,*) ' '
    do i=ncon+1,n-1
        write(1133,*) '- ns = ',i-ncon,' best tdel = ',itdelref(i), ', ic',icref(i)
    end do
    close(1133)

    write(*,*) '# Global analysis stats'
    write(*,*) 'n = ', n, ', ncon', ncon
    write(*,*) 'Generated GQLs from ', ic, 'reference points; kept ', ir
    write(*,*) ' '
    write(*,*) '## Tdel over specific i '
    write(*,*) ' '
    do i=ncon+1,n-1
        if (itdelref(i)<1000) then
            write(*,*) '- ns = ',i-ncon,' best tdel = ',itdelref(i), ', ic',icref(i)
        end if
    end do
    return
    end subroutine globana1


    subroutine gql1(ni,nci,a,b,ql,qli,ipar,rpar,ierr)
        integer :: i,ierr,ipvt(n),iw(3*n),ncc,mdim,rdim,ni, nci
        integer :: nnn, nn, ipar(*)
        double precision :: gap, srconv, srcone, rw(n*n), s, rpar(*),&
            volal, volbl, volabl
        double precision, dimension(ni) :: ar,ai,z, gapar
        double precision, dimension(ni,ni) :: a,b,invb,ql,qli,trow,tcol,avr
        logical :: tw(1000)
        !-Ask jobcon not used!
        ierr = 0
        ncc = nci
        ql = b
        call dgeinv(ni,ql,z,ipvt,ierr)
        if (ierr/=0) goto 901
        invb = ql

        ql = a
        nnn=ni*ni
        nn = 3*ni
        mdim = nci
        rdim = 0
        ierr = 1
        ar = 0.d0
        ai = 0.d0
        call homevc(1,ni,ql,ni,mdim,rdim,'S','S',ar,ai,trow,ni,tcol,ni,avr,&
                & ni,srcone,srconv,rw,nnn,iw,nn,tw,1000,rpar,ipar,6,ierr)
        if (ierr>0) goto 902
        !calculate determinant of matrix a
        s = 0
        gap = 1
        do i=1,ni
                s = ar(i)
                if (ai(i)/=0) then
                        s = dsqrt(ar(i)**2+ai(i)**2)
                end if
                gap = gap * s
        end do
        volal = gap

        a = matmul(a,invb)
        invb = a ! F*B^-1
        ql = b
        ierr = 1
        mdim = nci
        rdim = 0
        call homevc(1,ni,ql,ni,mdim,rdim,'S','S',ai,ai,trow,ni,tcol,ni,avr,ni,srcone,srconv,&
                &  rw,nnn,iw,nn,tw,1000,rpar,ipar,6,ierr)
        if (ierr>0) goto 903
        !Calculate determinant of matrix b
        s = 0
        gap = 1
        do i=1,ni
                s = ar(i)
                if (ai(i)/=0) then
                        s = dsqrt(ar(i)**2+ai(i)**2)
                end if
                gap = gap*s
        end do
        volbl = gap
        !Schurr Decomposition
        ql = invb
        mdim = nci
        rdim = 0
        ierr = 1
        call homevc(1,ni,ql,ni,mdim,rdim,'S','S',ar,ai,trow,ni,tcol,ni,avr,ni,srcone,srconv,&
                &  rw,nnn,iw,nn,tw,1000,rpar,ipar,6,ierr)
        if (ierr>0) goto 904
        ql = trow
        qli = tcol

        !CHECK
        invb = matmul(trow,tcol)
        write(*,*) '+++CHECK SCHUR VECTORS+++'
        write(*,*) 'SUM OF (Q*Q^(-1)(I,I)) = (~N = ',ni,')=',&
                &  sum((/(invb(i,i),i=1,ni)/))

        !Calculate det(ab)
        s = 0
        gap = 1
        do i=1,ni
                s = ar(i)
                if (ai(i)/=0) then
                        s = dsqrt(ar(i)**2+ai(i)**2)
                endif
                gap = gap*s
        end do
        volabl = gap

        !Calulate gap
        gap = dsqrt((ar(nci)**2+ai(nci)**2)/(ar(nci+1)**2+ai(nci+1)**2))
        do i=1,ni-1
                gapar(i) = dsqrt((ar(i)**2+ai(i)**2)/(ar(i+1)**2+ai(i+1)**2))
        end do
        return
        !Error exists
   900  continue
        write(*,*) 'Error in...'
        ierr = 1
        return
   901  continue
        write(*,*) 'Error in masjac'
        ierr = 1
        return
   902  continue
        write(*,*) 'Error in homevc'
        ierr = 1
        return
   903  continue
        write(*,*) 'Error in...'
        ierr = 1
        return
   904  continue
        write(*,*) 'Error in...'
        return
    end

    subroutine conpoi(smfp,del0,iref,ipar,rpar,xmol,ierr)
    integer :: i, j, ierr, seed, ipar(*), iref
    double precision :: s, rn1, rn2, value, vhit, alpha
    double precision, dimension(*) :: rpar, xmol
    double precision, dimension(n) :: smf1,smf2,smfp,del0
    logical :: keep

    ierr = 0

    call system_clock(count=seed)
    call srand(seed)

    keep = .false.

    do while (.not.keep)
        rn1 = rand()
        rn2 = rand()

        alpha = rand()

        smfp = alpha*smfin(1:n,max(1,nint(rn1*iref)))+(1-alpha)*&
                smfin(1:n,max(1,nint(rn2*iref)))

        !Correct species ncor
        s = sum((/(smfp(j+2)*xmol(j),j=1,ncor-1)/)) &
               &  + sum((/(smfp(j+2)*xmol(j),j=ncor+1,n-2)/))
        smfp(ncor+2) = (1.d+0 - s)/xmol(ncor)
        smfp(1:2) = smfin(1:2,1)

        ierr = 0
        call chkdom(0,n,smfp,smfp,ierr,vhit,value)
        if (ierr/=0) then
            write(*,*) 'chkdom prob'
            cycle
        end if
        ierr = 0
        s = 0.d0
        call masres(n,s,smfp,del0,rpar,ipar,ierr)
        if (ierr>0) then
            write(*,*) 'masres error'
            cycle
        end if
        keep = .true.
    end do

    return
    end subroutine conpoi



    subroutine trapoi(job,smfp,del0,ne,ipar,rpar,xmol,ierr)
        integer :: job, i, j, k, ierr, ne, seed, ipar(*)
        integer, dimension(n-2,ne) :: elenum
        double precision :: cc, s, vhit, value
        double precision, dimension(n) :: rno, smf, del0, temp
        double precision, dimension(n-2) :: rate, smfp
        double precision, dimension(n,n) :: b, a
        double precision, dimension(*) :: xmol, rpar
        logical :: keep

        ierr = 0

        if (rpoi == 0) then
            call system_clock(count=seed)
            call srand(seed)
        else if (rpoi/=1234) then
            call srand(rpoi)
        end if


        keep = .false.
        do while(.not.keep)
            rno = rand()

            if (job==3) then

                    k = nint(rno(1)*nvert)
                    if (k==0) k=1
                    smfp =  smfin(1:n,k)

!                    smfp(ncor) = smfin(ncor,1)
            end if
            smfp(1:2) = smfin(1:2,1)
            !write(*,*) smfp
            !read(*,*)
            ierr = 0
            call chkdom(0,n,smfp,smfp,ierr,vhit,value)
            if (ierr/=0) then
                write(*,*) 'chkdom prob'
                cycle
            end if
            ierr = 0
            s = 0.d0
            call masres(n,s,smfp,del0,rpar,ipar,ierr)
            if (ierr>0) then
                write(*,*) 'masres error'
                cycle
            end if
            !Sort out if the norm of the source term is less than the average

            !if (sqrt(sum(del0**2)) <fav/1000) then
            !    write(*,*) 'source less than avg'
            !    cycle
            !end if
            keep = .true.
        end do
        return
    end subroutine trapoi

    subroutine randomp(job,smf,del0,ipar,rpar,xmol,ierr)
        integer :: job, i, j, ierr, k, clock, ipar(*)
        integer, allocatable :: seed(:)
        double precision :: bm, s, vhit, value, smf(n)
        double precision :: del0(n), rno(n), rpar(*), xmol(*)
        logical :: keep


        call random_seed(size=k)
        allocate(seed(k))

        if (job==0) then
            call system_clock(count=clock)
            seed = clock+37* (/ (i - 1, i = 1, k) /)
            call random_seed(put=seed)
        else if (job /= 1234) then
            seed = job+37* (/ (i - 1, i = 1, k) /)
            call random_seed(put=seed)
        end if
        deallocate(seed)

        do while(.not.keep)
            keep = .false.
            call random_number(rno)
            smf = (/ (rno(j)*smfmax(j),j=1,n) /)
            !Overwrite pressure and enthalpy
            smf(1:2) = yi(1:2)
            !Correct species ncor
            s = sum((/(smf(j+2)*xmol(j),j=1,ncor-1)/)) &
                    &  + sum((/(smf(j+2)*xmol(j),j=ncor+1,n-2)/))
            smf(ncor+2) = (1.d+0 - s)/xmol(ncor)
            s = sum( (/ (smf(j+2)*xmol(j),j=1,n-2) /)  )
            if (s<9.90d-1.or.s>1.01d+0) cycle
            ierr = 0
            call chkdom(0,n,smf,smf,ierr,vhit,value)
            if (ierr/=0) cycle
            ierr = 0
            s = 0.d0
            call masres(n,s,smf,del0,rpar,ipar,ierr)
            if (ierr>0) cycle
            !Sort out if the norm of the source term is less than the average
            !if (sqrt(sum((/((del0(j)**2),j=1,n)/))) <fav/2) cycle
            keep = .true.
        end do
        return
    end subroutine randomp

    subroutine maxsource(amax,ipar,rpar,ierr)
        !Subroutine to calculate the source term range for each equation
        !  along the trajectory
        integer :: i, j, ierr, ipar(*)
        double precision :: a(n), amax(n), rpar(*)

        ierr = 0
        amax = 0.d0
        do i=1,nvert
            call masres(n,0,smfin(1:n,i),a,rpar,ipar,ierr)
            do j=1,n
                amax(j) = max(a(j),amax(j))
            end do
        end do
    end subroutine maxsource

    subroutine ratetra(info,detfil,slofil,tdel,thalf,rating)
    ! Subroutine to calculate the rating of the point.
    ! This is determined by the distance of the point after the slow integration 
    ! from the detailed equilibrium point of the detailed integration.
        integer :: i, info, nin, iref
        double precision :: tigdet, tigslo, thalfdet, thalfslo, rating
        double precision, dimension(n) :: 
        
        double precision, dimension(n,nalot) :: stem

        !Read detailed trajectory
        i = 0
        call retra(1,trim(detfil),len(trim(detfil)),i,stem,nin)

        call timdel(0,nin,stem,tigdet,iref)
        
        !Read slow trajectory
        i = 0
        call retra(1,trim(slofil),len(trim(slofil)),i,stemslo,nislo)

        

    subroutine timdel(info,nin,stem,tig,iref)
    !Subroutine to calculate the ignition time
        integer :: i, nin, iref, info
        double precision ::tig,  slope, stem(n+1,nin), ss, fd, res, &
            ref
        if ( info == 0) then
            ss = 0
            if (nin<1) then
                tig = 0.0d0
            else;
                do i=1,nin-1
                    slope = (stem(nrad+3,i+1)-stem(nrad+3,i))/&
                            max(1.0d-21,stem(1,i+1)-stem(1,i))
                    if (slope>ss) then
                        iref = i
                        ss = slope
                        tig = stem(1,i+1)
                    end if
                end do
            end if
        else if ( info == 1 ) then
            if (eq==-1.d0) then
                sp = stem(nrad+3,1)
                eq = stem(nrad+3,nin)
            end if

            ref = (sp+eq)/2

            tig = 0.d0
            do i = 2, nin-1
                if ( stem(nrad+3,i) <= ref ) then
                    fd = min(-1.0d-21,(stem(nrad+3,i)-stem(nrad+3,i-1)))
                    res = stem(1,i-1)+(stem(1,i)-stem(1,i-1))/fd*&
                        (ref-stem(nrad+3,i-1))
                    tig = res
                    exit
                end if
            end do
        end if
        return
    end subroutine timdel

    subroutine wmat(nfil,mat,mat2)
        double precision, dimension(n,n) :: mat, mat2
        integer i, nfil
        rewind nfil
        write(nfil) mat
        write(nfil) mat2
        return
!   882  format(8(1PE14.7,1X))
    end subroutine wmat

    subroutine repoi(nfil,mat)
        double precision, dimension(n) :: mat
        integer :: nfil
        rewind nfil
        read(nfil) mat
        write(*,*) mat
        return
    end subroutine repoi

    subroutine matlabpoi(nfil1,nfil2,mat1,mat2,ierr)
        include 'homdim2.f90'
        integer :: nfil1, nfil2, i, ierr, info

        double precision ::  c, cl, cp, rho, sspv, cil(mnspe+mnthb+1)
        double precision, dimension(n)          :: mat1
        double precision, dimension(n-1)        :: ry
        double precision, dimension(nspec)      :: ci
        double precision, dimension(n,n)        :: mat2
        double precision, dimension(n-1,n-1)    :: rjac
        common /bsspv/   sspv(20*mnspe)
        if ( nfil1 > 0 ) then
            ! Read point from file
            rewind nfil1
            write(*,*) 'Reading point from file'
            read(nfil1,*) ry(1:20)
            write(*,*) ry
            read(*,*)
            ! Convert to form (h, p, c_i)
            write(*,*) 'Converting point to homrea format'

            write(*,*) 'Enter pressure p = '
            read(*,*) mat1(2)

            info = 5 ! Input: T,P,W where W = specific mole numbers
 
            ierr = 0
            call xcihpw(info,nspec,ry(1),c,ci,rho,mat1(1),mat1(2),ry(3),sspv(1), &
                sspv(nspec),sspv(nspec*15+1),sspv(nspec*18+1),cp,sspv(nspec*19+1),ierr)
            if (ierr>0) then
                write(*,*) 'Something went wrong when calculating state vector.'
                write(*,*) 'Make sure data is consistent...'
                stop
            end if
            write(*,*) 'State vector calculated: ', mat1
            read(*,*)
        end if
        if ( nfil2 > 0 ) then 
            ! Read jacbian from file
            rewind nfil2
            write(*,*) 'Reading jacobian from file'
            do i = 1, n - 1
                read(nfil2,*) rjac(1:n-1,i)
            end do
            write(*,*) rjac
            read(*,*)
        end if

        if ( (nfil1 > 0) .and. (nfil2 > 0) ) then
            ! If both points were read, do a consistence check
            return
        end if

        return
    end subroutine matlabpoi

    subroutine reapoi(nfil,mat)
        integer :: nfil
        double precision, dimension(n) :: mat
        rewind nfil
        read(nfil,*) mat(1)
        return
    end subroutine reapoi

    subroutine rmat(nfil,mat,mat2)
        double precision, dimension(n,n) :: mat, mat2
        integer i, nfil
        rewind nfil
        read(nfil) mat
        read(nfil) mat2
        return
    end subroutine rmat

    subroutine wtra(job,nfil,nin,sym,smf)
        !Write trajectory
        double precision :: smf(n+1,nin)
        character(len=8) :: sym(n)
        integer :: job,i, nfil, nin, ios, k
!***********************************************************************
!     Tecplot output for trajectory result
!***********************************************************************
      if (job>0) then
      WRITE(nfil,831)
      WRITE(nfil,833) ' t(s) ',(sym(i),i=1,n)
      WRITE(nfil,834) NVERT
      end if
      DO i=1,nvert
        WRITE(nfil,832) (smf(k,i),k=1,n+1)
      ENDDO
      RETURN
  831 FORMAT('TITLE= " Results of trajectory or reference points "')
  832 FORMAT(7(1PE10.3,1X))
  833 FORMAT('VARIABLES =',/,7('"',A8,'"',:','))
  834 FORMAT('ZONE I=',I4,', J=1, F=POINT')
!***********************************************************************
!     End of -STTPOUT-
!***********************************************************************
        return
    end subroutine wtra

    subroutine intpoi(nci,tdel,symprp,ipar,rpar,ierr)
        integer :: rdim, nci, mdim, k, i, j, ipar(*), nin, nfas, ierr,&
                ierr2, iref, iw(n*n)
        double precision :: rpar(*), tdel, tend, tigf, tigs, scrone,&
                scronv, rw(n*n), vhit, value
        double precision, dimension(n) :: smf0, del0, ar, ai
        double precision, dimension(n+1,1000000) :: stem
        double precision, dimension(n,n) :: ql, qil, tl, refjac, tr, avr, xjac
       logical, dimension(1000) :: tw
       character(len=9) filnam
       character(len=8), dimension(n) :: symprp

       nfas = n  - nci

       write(*,*) 'n    = ', n
       write(*,*) 'nfas = ', nfas
       write(*,*) 'ns   = ', nci-ncon
       write(*,*) 'nci  = ', nci

       !qi = q
       !call dgeinv(n,qi,rw,iw,ierr)
       write(filnam,'(a,i3.3,a)') 'fa.bin'
       open(unit=121,file=trim(filnam),status='replace',form='unformatted',&
               access='sequential')
       rewind 121
       nin = nvert
       smf0 = yi

       ierr = 0
       ierr2 = 0
       tend = 1.0e+6
       write(*,*) 'Fast integration'
       call intfas(n,nfas,smf0,tend,q,qi,6,ierr2,rpar,ipar,nin)
       close(121)

       if (ierr2>=0) then
               j = 0
               call retra(1,filnam,len(filnam),j,stem,nin)
               call timdel(td,nin,stem,tigf,iref)
       else
               write(*,*) 'INTFAS FAILED'
               ierr = 3
               smf0 = yi
               tigf = 0
       end if

       do j=3,n
           if (smf0(j)<-1.or.smf0(j)>100) then
              write(*,*) 'INCONSISTENT DATA '
              ierr = 4
              smf0 = yi
              tigf = 0
           end if
       end do
       ierr2 = 0

       call chkdom(0,n,smf0,smf0,ierr2,vhit,value)
       if (ierr2/=0) then
           write(*,*) 'CHECKDOM FAILED'
               ierr = 4
           smf0 = yi
            tigf = 0
       end if


       write(*,*) 'nf = ', nfas
       write(*,*) 'smf = ', smf0
       !call staban( nfas, smf0, ipar, rpar, ierr2)
       !if ( ierr2 < 0 ) then
       !    write(*,*) 'Throwing point out in staban'
       !    smf0 = yi
       !    tigf = 0
       !    ierr = 4
       !end if

       ierr2 = 0
       !Open file for slow integration
       write(filnam,'(a,i3.3,a)') 'sl.bin'
       open(unit=123,file=trim(filnam),status='replace',form='unformatted',&
               access='sequential')
       rewind 123

       ierr2 = 0
       tend = 1.0e+6

       nin = nvert
       write(*,*) 'Slow integration'
       call intslo(n,nfas,smf0,tend,q,qi,6,ierr2,rpar,ipar,nin)
       close(123)
       if (ierr2>=0) then
               write(*,*) 'SLOW INTEGRATION SUCCESSFUL !'
               j = 0
               call retra(1,filnam,len(trim(filnam)),j,stem,nin)
               call timdel(td,nin,stem,tigs,iref)
       else
               write(*,*) 'SLOW INTEGRATION FAILED'
               ierr = 5
               return
       end if

       write(*,*) 'tigf = ', tigf, 'tigs = ', tigs, 'tign = ', tign
       tdel = abs((tign-(tigs+tigf))/max(1.d-20,abs(tign)))
       return

100 format (7('"',A,'",'))
    end subroutine intpoi

    subroutine retra(job,fils,fillen,nfil,stem,nin)
        !Read trajectory
        double precision :: stem(n+1,100000)
        integer          :: i, nin, ios,nfil, fillen, job
        character(len=30) :: lin
        character(len=fillen) :: fils
        if ( job == 0 ) then
            if (nfil == 0) then
                nfil = 2000
                open(nfil,file=trim(fils),iostat=ios)
            else
                open(nfil,iostat=ios)
            end if
            if (ios /= 0) then
                write(*,*) 'error opening file ', nfil
                stop
            end if

            rewind nfil

            i = 0
            ios = 0
            do while(ios==0)
                i = i + 1
                read(nfil,882,iostat=ios) stem(1:n+1,i)
            end do
            nin  = i - 1
            close(nfil)
        else
            open(unit=nfil,file=trim(fils),form='unformatted',&
                    access='sequential')
            rewind nfil
            i = 0
            ios = 0
            do while(ios==0)
                i = i + 1
                read(nfil,iostat=ios) stem(1:n+1,i)
            end do
 102        continue
            nin = i - 1

            close(nfil)
        end if
        return
   882  format(8(1PE14.7,1X))
    end subroutine retra

    subroutine calcgql1(job,a,b,ic,ipar,rpar,ierrs)
       integer :: job, i, k, j, ic, nfas, ierr, ir, iw(n*n)
       integer ::  mdim, rdim, nin, ipar(*), ierrs(n-3)
       double precision :: rw(n*n), srcone, srconv, rpar(*), tigf, tigs
       double precision :: vola, volb, volab, detam, detbm, tend, tdel, vhit, value
       double precision, dimension(n) :: smf, smf0, del0, ar, ai
       double precision, dimension(n,n) :: a, b, refjac, atemp, tcol, trow, avr
       double precision, dimension(n,n) :: acon, bcon
       double precision, dimension(n+1,100000) :: stem
       double precision, dimension(n-ncon) :: arcon, aicon
       double precision, dimension(n-ncon,n-ncon) :: are, bre,  qrow, qcol, atim, btim
       double precision, dimension(n-ncon,n-ncon) :: xjacfast,trar,trac,avr2
       logical :: keep, notstostable
       logical, dimension(1000) :: tw
       character(len=8), dimension(n) :: symprp

       ierr = 0
       smf = y
       smf0 = yi

       mdim = 6

       call masjac(n,0.d0,smf,del0,refjac,rpar,ipar,ierr)
       if (ierr > 0 ) then
               ierr = 2
               return
       end if
       mdim = ncon
       atemp = refjac
       ierr = 1
       rdim = 0
       call homevc(1,n,refjac,n,mdim,rdim,'S','S',ar,ai,trow,n,tcol,n,avr,n,&
                srcone,srconv,rw,n*n,iw,n*n,tw,1000,rpar,ipar,6,ierr)
       if (ierr > 0 ) then
               ierr = 2
               return
       end if

       atemp = matmul(trow,atemp)
       atemp = matmul(atemp,tcol)
       write(*,*) 'Check of Schurr vectors'
       write(*,*) '(nc,nc) = ',atemp(ncon,ncon)
       write(*,*) '(nc,nc+1) = ',atemp(ncon,ncon+1)
       write(*,*) '(nc+1,nc) = ',atemp(ncon+1,ncon)
       write(*,*) '(nc+1,nc+1) = ',atemp(ncon+1,ncon+1)
       tcol = trow
       ierr = 0
       call dgeinv(n,tcol,rw,iw,ierr)
       if (ierr > 0 ) then
               ierr = 2
               return
       end if

       acon = a
       bcon = b
       a(1:n-ncon,1:n) =matmul(trow(ncon+1:n,1:n),a)
       are = matmul(a(1:n-ncon,1:n),tcol(1:n,ncon+1:n))
       b(1:n-ncon,1:n) =matmul(trow(ncon+1:n,1:n),b)
       bre = matmul(b(1:n-ncon,1:n),tcol(1:n,ncon+1:n))

       atim = are
       btim = bre

       call gql1(n-ncon,nc-ncon,atim,btim,qrow,qcol,ipar,rpar,ierr)
       if (ierr>0) then
               ierr = 2
               return
       end if

       !-Ask Mean determinant condition

       keep = .false.
       dimensions: do i =ncon+2,n-3
           if (verb==1) write(*,*) 'NC = ', i
           nfas = n - ncon - i
           atim = are
           btim = bre
           call gql1(n-ncon,i,atim,btim,qrow,qcol,ipar,rpar,ierr)
           if (ierr>0) then
                   cycle dimensions
           end if
           q(1:i,1:n) = trow(1:i,1:n)
           q(i+1:n,1:n) =  matmul(qrow,trow(i+1:n,1:n))
           qi = q
           ierr = 0
           call dgeinv(n,qi,rw,iw,ierr)
           if (ierr>0) then
                   cycle dimensions
           end if
           atemp = matmul(q,qi)
           if (verb==1) write(*,*) n, ' ~ ', sum((/(atemp(k,k),k=1,n)/))
           ierr = 0
           smf = y
           ierr = 0
           call intpoi(i,tdel,symprp,ipar,rpar,ierr)
           if(ierr>0) then
                   cycle dimensions
           else
                   keep = .true.
                   if (tdel<itdelref(i)) then
                       itdelref(i) = tdel
                       icref(i) = ic
                       call wmat(3000+i,q,qi)
                   end if
                   if (tdel<tdelref) then
                       tdelref = tdel
                       itdel = i
                   end if
           end if

       end do dimensions
       if (keep) then
           ierr = 0
           if (verb==1) write(*,*) 'Keeping point...'
           return
       else
           ierr = 1
           return
       end if

    end subroutine calcgql1

    subroutine calcgql2(smf,symprp,tigf,tigs,ic,minns,ipar,rpar,ierrs)
       integer :: job, nfas, i, k, j, ierr, ir, iw(3*n), ierrs(n-ncon-1)
       integer ::  mdim, rdim, nin, ipar(*), ierr2, nn, ic, minns, ns
       double precision :: rw(10*n*n), srcone, srconv, rpar(*), tigf, tigs
       double precision :: vola, volb, volab, detam, detbm, tend, tdel, vhit, value
       double precision, dimension(n) :: smf0, smf, del, ar, ai
       double precision, dimension(n,n) :: xjac, refjac, atemp, tcol, trow, avr, ql,qil
       double precision, dimension(n+1,100000) :: stem
       logical :: keep, notsostable
       logical, dimension(1000) :: tw
       character(len=1) :: dir, zev

       character(len=8), dimension(n) :: symprp


       ierrs(1) = 1

       dir = 'S'
       zev = 'S'
       ierr2 = 0
       call masjac(n,0.d0,smf,del,xjac,rpar,ipar,ierr2)
       if (ierr2 > 0) then
           ierrs(1) = 2
           return
       end if


       refjac = xjac
       dimensions: do i=ncon+1,n-1
           tdel = 9.9d9
           nfas = n - i

           ns = i - ncon
           xjac = refjac
           mdim =  i
           ierr2 = 0
           rdim = 0

           ierr2 = 0
           call homevc(1,n,xjac,n,mdim,rdim,dir,zev,ar,ai,trow,n,&
                   tcol,n,avr,n,srcone,srconv,rw,10*n*n,iw,3*n,tw,1000,&
                   rpar,ipar,6,ierr2)
           if (ierr2>0) then
                   ierrs(ns) = -1
                   cycle dimensions
           end if
           q = trow
           qi = tcol


           ! Stability check at equilibrium

           xjac = refjac
           ierr2 = 0

           call staban( nfas, y, ipar, rpar, ierr2)
           if ( ierr2 < 0 ) then
                ierrs(ns) = -1
                cycle dimensions
           end if

           ierr = 0
           smf0 = y
           call intpoi(i,tdel,symprp,ipar,rpar,ierr)
           ierrs(ns) = -ierr
           if (ierr>4) then
               cycle dimensions
           else
               ierrs(ns) = ns
               if (tdel<itdelref(i)) then
                   icref(i) = ic
                   itdelref(i) = tdel
                   call wmat(3000+i,q,qi)
               end if
               if (tdel<tdelref) then
                   tdelref = tdel
                   itdel = i
               end if

               if (ns < minns) then
                       minns = ns
               end if
           end if
       end do dimensions

       return
    end subroutine calcgql2

    subroutine detbas(i, smf, symprp, tigf, tigs, ipar, rpar )
       integer :: job, nfas, i, k, j, ierr, ir, iw(3*n), ierrs(n-ncon-1)
       integer ::  mdim, rdim, nin, ipar(*), ierr2, nn, ic, minns, ns
       double precision :: rw(10*n*n), srcone, srconv, rpar(*), tigf, tigs
       double precision :: vola, volb, volab, detam, detbm, tend, tdel, vhit, value
       double precision, dimension(n) :: smf0, smf, del, ar, ai
       double precision, dimension(n,n) :: xjac, refjac, atemp, tcol, trow, avr, ql,qil
       double precision, dimension(n+1,100000) :: stem
       logical :: keep, notsostable
       logical, dimension(1000) :: tw
       character(len=1) :: dir, zev

       character(len=8), dimension(n) :: symprp


       ierrs(1) = 1

       dir = 'S'
       zev = 'S'
       ierr2 = 0
       mdim = 6
       write(*,*) 'Calculate jacobian'
       call masjac(n,0.d0,smf,del,xjac,rpar,ipar,ierr2)
       if (ierr2 > 0) then
           write(*,*) 'Jacobian could not be calculated'
           stop
       end if
        
       write(*,*) 'Calculate basis'
       nfas = n - i - ncon
       ns = i
       mdim =  i + ncon
       ierr2 = 0
       rdim = 0

       ierr2 = 0
       call homevc(1,n,refjac,n,mdim,rdim,dir,zev,ar,ai,trow,n,&
           tcol,n,avr,n,srcone,srconv,rw,10*n*n,iw,3*n,tw,1000,rpar,ipar,6,ierr2)
       if (ierr2>0) then
           write(*,*) 'Basis could not be calculated'
           stop
       end if
       q = trow
       qi = tcol


       ierr = 0
       smf0 = y
       call intpoi(mdim,tdel,symprp,ipar,rpar,ierr)
       ierrs(ns) = -ierr
       if (ierr>4) then
           write(*,*) 'Integration failed'
           stop
       end if
       write(*,*) 'tdel = ', tdel

       return
    end subroutine detbas


    subroutine staban( nf, smfx, ipar, rpar, ierr)
    integer :: job, i, nc, nf, mdim, rdim, ierr, ipar(*), ierr1, msgfil, ierr2
    integer, dimension((nf+6)*nf) :: iw
    double precision :: srcone, srconv, rpar(*)
    double precision, dimension(nf) :: arf, aif
    double precision, dimension(n) :: smfx, del
    double precision, dimension(n,n) :: xjac
    double precision, dimension(nf,nf) :: xjacfast, trar, trac, avr
    double precision, dimension(nf*(nf+6)) :: rw
    logical, dimension(1000) :: tw
    character(len=1) :: dir, zev
    ! Stability check

    dir = 'S'
    zev = 'S'
    ierr2 = 0
    call masjac(n,0.d0,smfx,del,xjac,rpar,ipar,ierr2)
    if (ierr2 > 0) then
        ierr = -3
        return
    end if
    
    job = -1
    ierr = 0

    nc = n - nf
    mdim =  nc
    rdim = 0

    ierr2 = 0
    xjac = matmul( xjac, qi)
    xjac = matmul( q, xjac)
    xjacfast = xjac(nc+1:n,nc+1:n)
    mdim = 0 !nc+ncon
    rdim = 0
    ierr1 = 1

    msgfil = 6
    arf = 0.d0
    aif = 0.d0
    write(*,*) xjacfast
    call homevc(job,nf,xjacfast,nf,mdim,rdim,'S','S',arf,aif,&
            trar,nf,trac,nf,avr,nf,srcone,srconv,rw,&
            nf*(nf+6),iw,(nf+6)*nf,tw,1000,rpar,ipar,msgfil,ierr1)
    if ( ierr1 > 0 ) then
            ierr = -1
            return
    end if

    do i= 1, nf
        if ( arf(i) > -1.d-10 ) then
            ierr = -2
            return
        end if
    end do

    ierr = 0
    return

    end subroutine


    subroutine wpointfiles(job,smfp,symprp,minns,nfil1,nfil2)
    integer :: job, nfil1, nfil2, minns
    double precision :: tdin
    double precision, dimension(n) :: smfp
    character(len=8), dimension(n) :: symprp

    if (job==0) then
        open(unit=nfil1,file='good_points.tec')
        open(unit=nfil2,file='not_so_good_points.tec')

        rewind nfil1
        rewind nfil2
        write(nfil1,*) 'VARIABLES = '
        write(nfil1,100) 'tdel    ', symprp(1:n)
        write(nfil1,*) 'ZONE I=',nr,' J=1, F=POINT'

        write(nfil2,*) 'VARIABLES = '
        write(nfil2,100) 'tdel    ', symprp(1:n)
        write(nfil2,*) 'ZONE I=100, J=1, F=POINT'
    else if (job==1) then
        write(nfil1,101) real(minns,8), smfp
    else
        close(nfil1)
        close(nfil2)
    end if
    return

100 format (7('"',A,'",'))
101 format (2x,7e11.3)

    end subroutine

    subroutine tecout(nv,nfil,symprp,filnam,sl,stem)
    integer :: i, nv,nfil, sl

    double precision, dimension(n+1,nv) :: stem
    character(len=8), dimension(n) :: symprp

    character(len=sl) :: filnam

    open(unit=nfil,file=trim(filnam))
    write(nfil,*) 'VARIABLES = '
    write(nfil,100) 't       ',symprp(1:n)
    write(nfil,*) 'ZONE I=', nv ,'J=1, F=POINT'
    do i=1,nv
        write(nfil,101) stem(1:n+1,i)
    end do

    close(nfil)
    return
100 format (7('"',A,'",'))
101 format (2x,7e11.3)
    end subroutine

    subroutine locana(np,lhs,time,fsuffix,rpar,ipar)
        integer :: i, j, np, ipar(*), ierr, nre, nm, nrins, nrtab, ninert, &
            info
        integer, allocatable :: stoma(:,:), rever(:), mat(:,:)
        double precision, allocatable :: gr(:)
        double precision :: rpar(*), s, ssp(6)
        double precision, dimension(n) :: smf, del
        double precision, dimension(np) :: time
        double precision, dimension(n,n) :: refjac
        double precision, dimension(n,np) :: lhs

        character(len=3) :: fsuffix     ! Filename suffix
        character(len=4), dimension(8) :: fpref
        character(len=7) :: fname       ! Filenames
        character(len=21) :: sni, snj, snf
        character(len=10) :: sns

        common/bnreac/nre,nm,nrins,nrtab,ninert
        

        allocate(stoma(n-2,nre))
        allocate(rever(nre))
        allocate(gr(nre))
        allocate(mat(6,nre))

  
        ! Write filename prefixes
        fpref(1) = 'LHS_'
        fpref(2) = 'RHS_'
        fpref(3) = 'JAC_'
        fpref(4) = 'SSP_'
        fpref(5) = 'GGR_'
        fpref(6) = 'STO_'
        fpref(7) = 'REV_'
        fpref(8) = 'MAT_'

        ! Write format specifiers
        write(sni,'(a,i4,a)') '(', nre, '(1pe14.6e3,1x))'
        write(snf,'(a,i4,a)') '(', n+1, '(1pe14.6e3,1x))'
        write(snj,'(a,i4,a)') '(', n*n, '(1pe14.6e3,1x))'
        write(sns,'(a,i4,a)') '(', nre, '(i4))'


        ! Open files
        do i = 1, 8
            write(fname,'(2a)') fpref(i), fsuffix
            open( unit=8000+i, file=fpref(i)//trim(LA_short), &
                status='replace', access='sequential', position='append' )
            rewind 8000+i
        end do

        ! Now go through the points...
        do i = 1, np
           ! Get point from LHS
           smf = lhs(:,i) 

           ! Do analysis with a modified version of masjac
           call masjac2(n, nre, smf, del, refjac, gr, ssp, stoma, rever, mat, &
               rpar, ipar, ierr)

           !write(*,*) sp
           !read(*,*)
           ! Write data
           write(8001,snf) time(i), smf
           write(8002,snf) time(i), del
           write(8003,snj) refjac
           write(8004,'(5(1pe14.6e3,1x))') ssp
           write(8005,sni) gr

        end do

        ! Write stochiometric matrix
        do i = 1, n - 2
            write(8006,sns) stoma(i,:)
        end do
        
        ! Write reverse reactions
        write(8007,sns) rever

        ! Write reaction coefficients
        do i = 1, nre
            write(8008,'(6(i4))') mat(:,i)
        end do

        do i = 1, 8
            close(8000+i)
        end do


        return

    end subroutine locana



    subroutine masjac2(neq,nre,y,f,a,g,ssp,stoma,rever,mat,rpar,ipar,ierr)
        include 'homdim2.f90'
        ! Subroutine to calculate data for local analysis
        logical :: detona, pcon, vcon, test, tcon, walli
        integer :: i, ipar(*), nre, ne, ieq, ierr, nh, np, nws, nw, nspec, iex, neq, iprim, &
            nfile6, lrw1, lsmax, nsens, nrsens, nisens, npsens, irsens, iisens, ipsens, isensi, &
            temp1(3,nrins), stoma(neq-2,nreac), rever(nreac), mat(6,nreac)

        double precision :: rpar(*), s, mms, g(nreac), cil(mnspe+mnthb+1), &
            tl, cl, rho, hspec, pl, ssp(5), gr(nreac), wall, smfmix, smfcha, vdir, sspv, omega, &
            hlhh, hinf, stomcom, ss

        double precision :: dgdt(nreac), dodt(neq-2), dxds(1), dtds(1), dpds(1)
        double precision, dimension(nreac,neq-2) :: dgdc, dodc
        double precision, dimension(neq-2) :: si, hi, dsdpsi, fn, yn
        double precision, dimension(neq) :: y, f
        double precision, dimension(neq,neq) :: a
        double precision, dimension(mnrea) :: work1

        common /boptl/   pcon, vcon, tcon, detona, wall
        common /bmix/    omega, smfmix(mnequ), smfcha(mnequ), vdir(mnequ,ndimm)
        common /bsspv/   sspv(20*mnspe)
        common /bhcp/    hlhh(7,mnspe*2), hinf(3,mnspe)

        ! Block information about reaction mechanism

        common /bsens/   lsmax,nsens,nrsens,nisens,npsens,irsens,iisens,&
            ipsens,isensi(mnsen)
        common /stohmat/ stomcom(mnequ,mnrea)

        
        lrw1 = mnrea
        nfile6 = 6
        ierr = 0
        test = .false.

        nspec   = ipar(3)
        ne      = ipar(6)
        ieq     = ipar(10)
        nh      = 1
        np      = 2
        nws     = 2
        nw      = 3

        ! Calculate sparse stochiometric matrix
        stoma = 0
        temp1 = reshape(ireac(nnrvec:nnrvec+3*nrins), [3,nrins])
        do i=1, nrins
            stoma(temp1(1,i),temp1(2,i)) = temp1(3,i)
        end do

        do i=1, nreac
            mat(1:3,i) = ireac(nmatll+3*(i-1):nmatll+3*(i-1)+2)
            mat(4:6,i) = ireac(nmatlr+3*(i-1):nmatlr+3*(i-1)+2)
        end do
        
        ! Reverse reactions
        rever = ireac(nnrvec+3*nrins:nnrvec+3*nrins+nreac)
        ! Calculate mean molar mass and mole fractions
        call trassp(ieq, neq, y, nspec, tl, cl, cil, rho, hspec, pl, sspv, iex, 0, dxds, &
            & 1, dtds, dpds)

        yn(1:neq-2) = cil(1:neq-2)

        ! Calculate molar reaction rates

        iprim = 0

        call mechja(1, nspec, nreac, nm, nrins, nrtab, &
            ireac(nmatll), ireac(nnrvec), rreac(npk), rreac(ntexp), rreac(nexa), &
            rreac(nvt4), rreac(nvt5), rreac(nvt6), ireac(niarrh), &
            rreac(nxzsto), rreac(nratco), &
            cl, cil, tl, f(nw), nrsens, isensi(irsens), rpar(irsens), lrw1, work1, iprim, &
            nfile6, ierr, rreac(nainf), rreac(nbinf), rreac(neinf), rreac(nt3), &
            rreac(nt1), rreac(nt2), rreac(nafa), rreac(nbfa), hlhh, hinf, 1, 1, &
            dgdc, dgdt, dodc, dodt)

        g(1:nre) = work1(1:nre)/rho

        f(nw:nws+nspec) = f(nw:nws+nspec)/rho
        f(nh) = 0.d0
        f(np) = 0.d0
        if (omega /= 0.d0) then
            f(1:neq) = f(1:neq) + omega * (smfmix(1:neq) - y(1:neq))
        endif

        mms = sum(y(3:n))
        ! Calculate entropy

        ss = 0.d0
        do i=1, nspec
            call calhsi(nspec, i, tl, hlhh, hinf, hi(i), si(i), 6)
            if( y(i+2) > 1d-99) then
                si(i) = si(i) - rgas*log(y(i+2)*mms*pl/patm)
                dsdpsi(i) = si(i) -rgas
                ss = ss + si(i)*y(i+2)
            end if
        end do
        ssp(1) = ss 
        ssp(2) = tl
        ssp(3) = rho
        ssp(4) = mms
        !sp(5) = dsdpsi

        !write(*,*) sp
        !read(*,*)
        return

        ! Abnormal end

  910   continue
        write(6,*) 'iex = ', iex
        ierr = 1
        return

    end subroutine masjac2


    subroutine wstchio()
        ! Subroutine to print stochiometric matrix

    end subroutine wstchio


end module
