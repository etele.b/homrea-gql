C***********************************************************************
C     Module for multi-dimensional mesh
C***********************************************************************
      MODULE RDMESH
      USE local_coordinates
      DOUBLE PRECISION                                :: manierr
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:,:,:) :: PSTH
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:,:,:) :: PSTHP
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:,:) :: GRDPS2
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:,:)  :: PSTT1,PSTT2
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:)    :: DETPSITH
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:)    :: SUMW
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:,:)  :: TTTL,
     1                                                  SSSL
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:,:)  :: THETA
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:,:,:)  :: TTTHAL
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:,:)  :: YGR(:,:)
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:,:)  :: XGRPSI(:,:)
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:,:)  :: XSEPSI(:,:)
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:,:)  :: GPMA
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:)    :: GPVAL
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:)    :: GPLIM
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:)  :: TCMIN,TCMAX
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:)    :: GPVE
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:,:)  :: DYI ,DYE, DIFFU,
     1                                                  SOURCE,DIFFU2,
     2                                                  PHI
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:,:,:) :: APPJAC
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:,:) :: SCMA
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:,:) :: ELEMENT
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:)   :: ENTROPY
      INTEGER, ALLOCATABLE, DIMENSION(:)                ::flagdiff
      INTEGER, ALLOCATABLE, DIMENSION(:)                ::flagchem
      INTEGER, ALLOCATABLE, DIMENSION(:)                ::evecbou
      INTEGER, ALLOCATABLE, DIMENSION(:)                ::bouvertnr
      INTEGER, ALLOCATABLE, DIMENSION(:)                ::innernei
      INTEGER, ALLOCATABLE, DIMENSION(:)                ::bounrnei
      integer, ALLOCATABLE, DIMENSION(:,:)              ::icordbou
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:,:,:)   ::CORPBOU   
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:,:,:)   ::CORBOU   
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:)       ::DISBOU   
      INTEGER                                           ::nrbou
      INTEGER, ALLOCATABLE,DIMENSION(:)                 ::neighvecbou
      type(coordinate_type),allocatable,dimension(:)
     1                                   :: loccorbou
      type(vertice_type),allocatable,dimension(:)       :: bouvertnei
      logical                               :: addpoints,redistribute,
     1                     nofdinner,diffbou,expansion
      double precision                      ::higherdiff,lowerdiff,
     1                                        changegrad
      logical                               ::correctmass,setzero
      CONTAINS
C***********************************************************************
C     Set-up mesh
C***********************************************************************
        SUBROUTINE SETMES(NEQ,NDIM,NVERT,ICORD)
         INTEGER NEQ,NDIM,NVERT
         INTEGER,          DIMENSION(NDIM,NVERT)   :: ICORD
          IF(.NOT.ALLOCATED(PSTH))  ALLOCATE (PSTH(NEQ,NDIM,NVERT))
          IF(.NOT.ALLOCATED(PSTHP))  ALLOCATE (PSTHP(NDIM,NEQ,NVERT))
          IF(.NOT.ALLOCATED(GRDPS2))  ALLOCATE (GRDPS2(NEQ,NVERT))
          IF(.NOT.ALLOCATED(DETPSITH))  ALLOCATE (DETPSITH(NVERT))
          IF(.NOT.ALLOCATED(SUMW))  ALLOCATE (SUMW(NVERT))
          IF(.NOT.ALLOCATED(THETA))  ALLOCATE (THETA(NDIM,NVERT))
          IF(.NOT.ALLOCATED(YGR))  ALLOCATE (YGR(NEQ,NVERT))
          IF(.NOT.ALLOCATED(XGRPSI))  ALLOCATE (XGRPSI(NEQ,NVERT))
          IF(.NOT.ALLOCATED(XSEPSI))  ALLOCATE (XSEPSI(NEQ,NVERT))
          IF(.NOT.ALLOCATED(TTTL ))  ALLOCATE (TTTL(NDIM,NVERT))
          IF(.NOT.ALLOCATED(TTTHAL)) ALLOCATE (TTTHAL(NDIM,NDIM,NVERT))
          IF(.NOT.ALLOCATED(SSSL ))  ALLOCATE (SSSL(NDIM,NVERT))
          IF(.NOT.ALLOCATED(GPMA ))  ALLOCATE (GPMA (NDIM,NEQ  ))
          IF(.NOT.ALLOCATED(GPVAL))  ALLOCATE (GPVAL(NDIM  ))
          IF(.NOT.ALLOCATED(GPLIM))  ALLOCATE (GPLIM(NDIM  ))
          IF(.NOT.ALLOCATED(GPVE ))  ALLOCATE (GPVE (NEQ       ))
          IF(.NOT.ALLOCATED(TCMIN))  ALLOCATE (TCMIN(NDIM      ))
          IF(.NOT.ALLOCATED(TCMAX))  ALLOCATE (TCMAX(NDIM      ))
          IF(.NOT.ALLOCATED(DYE))    ALLOCATE (DYE(NEQ,NVERT))
          IF(.NOT.ALLOCATED(DYI))    ALLOCATE (DYI(NEQ,NVERT))
          IF(.NOT.ALLOCATED(DIFFU))    ALLOCATE (DIFFU(NEQ,NVERT))
          IF(.NOT.ALLOCATED(DIFFU2))    ALLOCATE (DIFFU2(NEQ,NVERT))
          IF(.NOT.ALLOCATED(SOURCE))    ALLOCATE (SOURCE(NEQ,NVERT))
          IF(.NOT.ALLOCATED(PHI))    ALLOCATE (PHI(NEQ,NVERT))
          IF(.NOT.ALLOCATED(PSTT1))    ALLOCATE (PSTT1(NEQ,NVERT))
          IF(.NOT.ALLOCATED(PSTT2))    ALLOCATE (PSTT2(NEQ,NVERT))
          IF(.NOT.ALLOCATED(APPJAC)) ALLOCATE (APPJAC(NEQ,NEQ,NVERT))
          IF(.NOT.ALLOCATED(ELEMENT)) ALLOCATE (ELEMENT(NEQ,NVERT))
          IF(.NOT.ALLOCATED(ENTROPY)) ALLOCATE (ENTROPY(NVERT))
          IF(.NOT.ALLOCATED(SCMA)) ALLOCATE (SCMA(NEQ,NEQ))
          IF(.NOT.ALLOCATED(flagchem)) ALLOCATE (flagchem(NVERT))
          IF(.NOT.ALLOCATED(flagdiff)) ALLOCATE (flagdiff(NVERT))
          THETA = dble(ICORD)
        END SUBROUTINE SETMES
        SUBROUTINE DEALLOCMES()
          IF(ALLOCATED(PSTH))  DEALLOCATE (PSTH)
          IF(ALLOCATED(PSTHP))  DEALLOCATE (PSTHP)
          IF(ALLOCATED(GRDPS2))  DEALLOCATE (GRDPS2)
          IF(ALLOCATED(DETPSITH))  DEALLOCATE (DETPSITH)
          IF(ALLOCATED(SUMW))  DEALLOCATE (SUMW)
          IF(ALLOCATED(THETA))  DEALLOCATE (THETA)
          IF(ALLOCATED(YGR))  DEALLOCATE (YGR)
          IF(ALLOCATED(XGRPSI))  DEALLOCATE (XGRPSI)
          IF(ALLOCATED(XSEPSI))  DEALLOCATE (XSEPSI)
          IF(ALLOCATED(TTTL ))  DEALLOCATE (TTTL)
          IF(ALLOCATED(TTTHAL)) DEALLOCATE (TTTHAL)
          IF(ALLOCATED(SSSL ))  DEALLOCATE (SSSL)
          IF(ALLOCATED(GPMA ))  DEALLOCATE (GPMA )
          IF(ALLOCATED(GPVAL))  DEALLOCATE (GPVAL)
          IF(ALLOCATED(GPLIM))  DEALLOCATE (GPLIM)
          IF(ALLOCATED(GPVE ))  DEALLOCATE (GPVE )
          IF(ALLOCATED(TCMIN))  DEALLOCATE (TCMIN)
          IF(ALLOCATED(TCMAX))  DEALLOCATE (TCMAX)
          IF(ALLOCATED(DYE))    DEALLOCATE (DYE)
          IF(ALLOCATED(DYI))    DEALLOCATE (DYI)
          IF(ALLOCATED(DIFFU))    DEALLOCATE (DIFFU)
          IF(ALLOCATED(DIFFU2))    DEALLOCATE (DIFFU2)
          IF(ALLOCATED(SOURCE))    DEALLOCATE (SOURCE)
          IF(ALLOCATED(PHI))    DEALLOCATE (PHI)
          IF(ALLOCATED(PSTT1))    DEALLOCATE (PSTT1)
          IF(ALLOCATED(PSTT2))    DEALLOCATE (PSTT2)
          IF(ALLOCATED(APPJAC)) DEALLOCATE (APPJAC)
          IF(ALLOCATED(ELEMENT)) DEALLOCATE (ELEMENT)
          IF(ALLOCATED(ENTROPY)) DEALLOCATE (ENTROPY)
          IF(ALLOCATED(SCMA)) DEALLOCATE (SCMA)
          IF(ALLOCATED(flagchem)) DEALLOCATE (flagchem)
          IF(ALLOCATED(flagdiff)) DEALLOCATE (flagdiff)
        END SUBROUTINE DEALLOCMES
      END MODULE RDMESH
C***********************************************************************
C
C     Module for Gradient array
C
CBZ==================================================================CBZ
CBZ   module was changed for use with more than 2 dimensions         CBZ
CBZ   2014/02/05                                                     CBZ
CBZ==================================================================CBZ
C***********************************************************************
      MODULE GRAFI
      !INTEGER  :: NL,NK
      integer, allocatable, dimension(:)             :: nverti
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:,:)  :: PSIFI,PGRAFI,
     1                                                  PSECFI
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:)    :: DDDFI
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:)    :: GRASVE
      DOUBLE PRECISION                               :: XLIM,FREB
C -Changed: A.Neagos
C vertices of gradient table might differ
      integer                                        :: nverttab
      CONTAINS
        !SUBROUTINE SETGFI(NEQ)
        SUBROUTINE SETGFI(NEQ,nverttab)
         INTEGER NEQ
          !IF(.NOT.ALLOCATED(PSIFI))   ALLOCATE (PSIFI(NEQ,NL,NK))
          !IF(.NOT.ALLOCATED(PGRAFI))  ALLOCATE (PGRAFI(NEQ,NL,NK))
          !IF(.NOT.ALLOCATED(PSECFI))  ALLOCATE (PSECFI(NEQ,NL,NK))
          !IF(.NOT.ALLOCATED(DDDFI))   ALLOCATE (DDDFI(NL,NK))
          IF(.NOT.ALLOCATED(PSIFI))   ALLOCATE (PSIFI(NEQ,nverttab))
          IF(.NOT.ALLOCATED(PGRAFI))  ALLOCATE (PGRAFI(NEQ,nverttab))
          IF(.NOT.ALLOCATED(PSECFI))  ALLOCATE (PSECFI(NEQ,nverttab))
          IF(.NOT.ALLOCATED(DDDFI))   ALLOCATE (DDDFI(nverttab))
          IF(.NOT.ALLOCATED(GRASVE)) ALLOCATE (GRASVE(NEQ))
        END SUBROUTINE SETGFI
      END MODULE GRAFI

      subroutine ind2sub(nsiz,idx,isub,ndim,iwrk)
!***********************************************************************
!     Convert linear index IDX into multidimensional subscripts ISUB(1:NDIM)
!     ISUB is the NDIM dimensional subscript into an NDIM
!     dimensional array with size NSIZ(1:NDIM) corresponding to IDX
!     IWRK(NDIM) is a temporary work array
!
!     nsiz  i  integer array of size ndim, sizes of array in each
!              dimension
!     idx   i  integer, the resulting linear index
!     isub  o  integer array of size ndim, subscript into array in each
!              dimension
!     ndim  i  integer number of dimensions of array
CBZ==================================================================CBZ
CBZ   taken from gridmod.f90 by R.Schiessl                           CBZ
CBZ   2014/02/05                                                     CBZ
CBZ==================================================================CBZ
!***********************************************************************
      implicit none
      integer nsiz, idx, isub, ndim, iwrk, ndx, i
      dimension nsiz(ndim),isub(ndim),iwrk(ndim)
      ndx=idx
      iwrk(1)=1
      do i=2,ndim
        iwrk(i)=iwrk(i-1)*nsiz(i-1)
      enddo
      ndx=ndx-1
      do i=ndim,1,-1
        isub(i) = int(1.0d0*ndx/iwrk(i)) + 1
        ndx = mod(ndx,iwrk(i))
      enddo
      end subroutine ind2sub

      subroutine sub2ind(nsiz,idx,isub,ndim,iwrk)
!***********************************************************************
!     Convert multidimensional subscripts ISUB(1:NDIM) into linear index IDX
!     IDX is the linear index into an NDIM dimensional array with
!     size NSIZ(1:NDIM) corresponding to the subscript ISUB(1:NDIM)
!     IWRK(1:NDIM) is a work array
!
!     nsiz  i  integer array of size ndim, sizes of array in each
!              dimension
!     idx   o  integer, the resulting linear index
!     isub  i  integer array of size ndim, subscript into array in each
!              dimension
!     ndim  i  integer number of dimensions of array
CBZ==================================================================CBZ
CBZ   taken from gridmod.f90 by R.Schiessl                           CBZ
CBZ   2014/02/05                                                     CBZ
CBZ==================================================================CBZ
!***********************************************************************
      implicit none
      integer nsiz, idx, isub, ndim, iwrk, i
      dimension nsiz(ndim), isub(ndim), iwrk(ndim)
      iwrk(1)=1
      do i=2,ndim
        iwrk(i)=iwrk(i-1)*nsiz(i-1)
      enddo
      idx=1
      do i=1,ndim
        idx = idx + (isub(i)-1)*iwrk(i)
      enddo
      end subroutine sub2ind

      SUBROUTINE INERTM(ISCVAR,NDIM,NEQ,NC,CMAT,
     1           LRV,NPROP,PROP,NVERT,
     1           NCELL,ICORD,IRET,IY,IVERT,N2DIM,SYMPRP,
     1           RV,RPAR,IPAR,LRW,RW,LIW,IW,NDINV,MIX)
C***********************************************************************
C
C     Compute Inertial Manifold (modified Fraser algorithm)
C
C                                  | 0 0 |
C     dY/dt = (Y_theta, Y_theta^o)*|     |*(Y_theta, Y_theta^o)^-1*F(Y)
C                                  | 0 I |
C     or
C
C     dY/dt = (I-Y_theta*Y_theta^+)*F(Y)
C
C     using a semi-implicit procedure
C
C***********************************************************************
      USE RDMESH
      USE GRAFI
      USE IRKC_M
      use get_cormatplus
      use cor_module
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      external          F_E,F_I
      TYPE (IRKC_SOL) :: SOL
      PARAMETER (ONE = 1.0D0, ZERO = 0.0D0)
      CHARACTER(LEN=*)  SYMPRP(NPROP)
      EXTERNAL RESINV,IVALEF,IVARIG,DUMJAC
      LOGICAL GRAF,RLX,CONGRA
C***********************************************************************
C     STORAGE ARRAY FOR TRAJECTORIES
C***********************************************************************
C***********************************************************************
C     Arrays
C***********************************************************************
C---- Input
      DIMENSION HLHH (7,2*(NEQ-2))
      DIMENSION HINF (3,NEQ-2)
      DIMENSION SK (NEQ-2)
      DIMENSION IRET(NVERM),IY(NDIMM*NCELM)
      DIMENSION IVERT(N2DIMM*NCELM)
      DIMENSION PROP(NPROP,NVERM)
      DIMENSION RV(LRV)
      DIMENSION RTOLS(1),ATOLS(1),RTOL(NEQ),ATOL(NEQ*NVERT)
      DIMENSION IJOB(20)
      DIMENSION ICORD(NDIM,NVERM)
      DOUBLE PRECISION, DIMENSION(NEQ,NVERM)     :: Y,DY,Z,ynew
      DOUBLE PRECISION, DIMENSION(NVERM)     :: exppar
C---- Arrays needed for the modified Fraser algorithm
      DOUBLE PRECISION, DIMENSION(NEQ*NVERT)     :: TTY
      DOUBLE PRECISION, DIMENSION(NEQ)           :: X
      DOUBLE PRECISION, DIMENSION(NEQ,NVERT)     :: YTEMP,DYTEMP,
     1                                              DY1
      DOUBLE PRECISION, DIMENSION(NC,NEQ)        :: CMAT
      DIMENSION IUNIT(NDIMM*N2DIMM),IPAIR(NDIMM*N2DIMM)
C---- Work arrays
      DIMENSION RW(LRW),IW(LIW),RPAR(*),IPAR(*)
      DIMENSION DIST(NDIM)
      DOUBLE PRECISION, DIMENSION(NEQ-2,NEQ) :: DXDS
      DOUBLE PRECISION, DIMENSION(NEQ) ::  DTDS, DPDS
      DOUBLE PRECISION, DIMENSION(NEQ) ::  HEL1, HEL2 
      DOUBLE PRECISION, DIMENSION(NVERT) :: MMOLM,WR, WI
      DOUBLE PRECISION, DIMENSION(2,NDIM)        :: DK
      DOUBLE PRECISION :: MASSFR,ENTHALPY
      DOUBLE PRECISION, allocatable, DIMENSION(:,:) ::thet2vec
      DOUBLE PRECISION, allocatable, DIMENSION(:,:) ::thet2vec2
C
C
      INTEGER, DIMENSION(15**(NDIM)-1)                   :: VERTICES
      double precision,dimension(:),allocatable         :: corvect
      double precision,dimension(NEQ,NEQ) :: DUM1
C***********************************************************************
C     Common blocks
C***********************************************************************
      COMMON/BSSPV/SSPV(20*MNSPE)
      COMMON/BLOCOR/IIRAT,IISMF,IISMFP,IIRV,IIPRO,IIPNU,IILAM,NPROOT
      COMMON/BINVA/ZETA,DELTAT,ISTE,NAVERT,NAEQ,NADIM,
     1          IVINF(NVERM),NEIGHP(2,NDIMM*NVERM)
      COMMON/BSCAD/DCOV(NVERM)
      COMMON/BIFIR/IFIRCA
      COMMON/TTTEMP/TEMP1(MNSPE+2,1,NVERM),TEMP2(MNSPE+2,1,NVERM),
     1     TEMP3(MNSPE+2,1,NVERM)
      COMMON/BSOL/IMPLI
      COMMON/BGRX/GRVE(MNLIM*(MNSPE+2)),GRVAL(2,MNLIM  ),NGRV
      COMMON/BGRS/GSVE(MNLIM*(MNSPE+2)),GSVAL(2,MNLIM  ),NGSV
      COMMON/BREDIM/REDIMOPT(20)
      COMMON/ILDMOP/IPARA,ISTAOP,MANPAR,NOFORM,ANAFUE,INMAN,LASLOO,
     1              ICALTR,MASENS,IUPVER,NTRANS
      COMMON/BTTTT/SSCAM((MNSPE+2)*(MNSPE+2)),SCAFA(MNSPE+2)
      COMMON/BCELRA/NNEWCLD,NOLDCLD
C***********************************************************************
C     USEFUL CONSTANTS
C***********************************************************************
      DATA NFIOUT/6/,NFIIN/1002/,NFIPLO/1001/,NFISCD/211/
C---- Maximum number of iterations
      DATA NSTMAX/10/
      DATA RGAS/8.3143D0/
C***********************************************************************
C     Initialize Y
C***********************************************************************
      Y(1:NEQ,1:NVERT) = PROP(NDIM+1:NDIM+NEQ,1:NVERT)
C**********************************************************************
C***********************************************************************
C A.Neagos
C initialize flags
C**********************************************************************
      addpoints=.false.
      redistribute=.false.
      nofdinner=.false.
      diffbou=.false.
      expansion=.false.
      if (iupver.eq.1)then
        redistribute=.true.
      else if(iupver.eq.2)then
        redistribute=.true.
        addpoints=.true.
      end if
      if (inman.eq.2)expansion=.true.
CBUG
CBUG  REDIMDIF=0
CBUG  if(REDIMDIF.eq.1)then
CBUG    diffbou=.true.
CBUG  else if(REDIMDIF.eq.2) then
CBUG    nofdinner=.true.
CBUG  else if(REDIMDIF.eq.3) then
CBUG    diffbou=.true.
CBUG    nofdinner=.true.
CBUG  end if
C***********************************************************************
C A.Neagos
C parameters
C**********************************************************************
      zeta=int(redimopt(1))
      stmax=int(redimopt(2))
      deltat0=redimopt(3)
      freb=int(redimopt(4))
      higherdiff=redimopt(5)
      lowerdiff=redimopt(6)
      changegrad=redimopt(7)
      if (nint(redimopt(8)).gt.0)correctmass=.true.
      if (nint(redimopt(9)).gt.0)setzero=.true.
      if(nint(redimopt(10)).eq.1)then
        diffbou=.true.
      else if(nint(redimopt(10)).eq.2) then
        nofdinner=.true.
      else if(nint(redimopt(10)).eq.3) then
        diffbou=.true.
        nofdinner=.true.
      end if
      if(nofdinner)then
         WRITE(*,*) 'CHEbYchev for inner thrown out'
         STOP
      end if
C**********************************************************************
C     REDIMOPT(11) and 12 missing
C************,**********************************************************
      SCAFA(1) = 1.D-6
      SCAFA(2) = 1.D-6
      SCAFA(3:NEQ) = 1.0D0
C A.Neagos
C correct mass-fraction, general
C**********************************************************************
      if(correctmass)then
      DO I=1,NVERT
      corr=(1.0d0-dot_product(y(3:neq,i),xmol(1:neq-2)))/
     1                        sum(xmol(1:neq-2))
      do j=1,neq-2
        y(j+2,i)=y(j+2,i)-corr
      end do
      ENDDO
      end if
C**********************************************************************
C A.Neagos
C set all species to zero, if negative
C**********************************************************************
      if(setzero)then
      do I=1,NVERT
        do j=3,neq
            if (y(j,i).lt.0.0d0)then
                y(j,i)=0.0d0
            end if
        end do
      end do
      end if
*Neagos-----------------------------------------------
*     correct mass-fraction, select species on which correction is based , in this case nr.4
*      do l=1,nvert
*      x=y(:,l)
*      y(4,l)=x(4)+(1.0d0-dot_product(xmol(1:neq-2),x(3:neq)))/
*     1          xmol(2)
*      end do
*      y(1,:)=y(1,nvert)
C
C
C**********************************************************************
C***********************************************************************
C A.Neagos
C implement relaxed states from homrea calculations
C***********************************************************************
      RLX=.false.
      IF (RLX) CALL RLXDSTS(NEQ,NVERT,Y)
C***********************************************************************
C     Read gradient information
C***********************************************************************
      IFIRCA = 0
C***********************************************************************
C     INITIALIZE
C***********************************************************************
      NUCALL = 0
      NAVERT = NVERT
      NADIM  = NDIM
      NAEQ   = NEQ
      NTOT   = NEQ * NVERT
      WRITE(*,*) 'Old values of NCELL,NVERT',NCELL, NVERT
*Neagos-----------------------------------------------
*     reparametrize orthogonally
*      call orth_rep_2d(neq,ndim,nvert,icord,neighp,y)
C***********************************************************************
C     INITIALIZE
C***********************************************************************
*Neagos-----------------------------------------------
*     rename flags for boundary points
      do i=1,nvert
        if(IRET(i).eq.-1)IRET(i)=0
        if(IRET(i).eq.10)IRET(i)=-1
      end do
      NOLDCLD = MAXVAL(ICORD(NDIM,1:NVERT))-MINVAL(ICORD(NDIM,1:NVERT))
C***********************************************************************
C    Add points 
C***********************************************************************
      if (addpoints)then
C---- Get unit vector
       CALL UNISET(NDIM,N2DIM,IUNIT,IPAIR)
C---- Add new boundary points
*Neagos-----------------------------------------------
*     add new boundary points on top of old ones, redistribution necessary, following, new points have iret=10
      CALL ADDPOI(NDIM,N2DIM,NEQ,NVERT,NCELL,Y,ICORD,IRET,IY,IVERT,
     1            IUNIT,RW,LRW,IW,LIW)
      do i=1,nvert
*        if(IRET(i).eq.-1)IRET(i)=0
        if(IRET(i).ne.0)IRET(i)=-1
      end do
      end if
C***********************************************************************
C     calculate maximum number of cells in last direction
C***********************************************************************
      NNEWCLD = MAXVAL(ICORD(NDIM,1:NVERT))-MINVAL(ICORD(NDIM,1:NVERT))
      WRITE(*,*) 'HHHHHHHHHH',NNEWCLD,NOLDCLD
C***********************************************************************
C    Add points 
C***********************************************************************
      CALL SETMES(NEQ,NDIM,NVERT,ICORD)
      NUCALL = 0
      NAVERT = NVERT
      NADIM  = NDIM
      NAEQ   = NEQ
      NTOT   = NEQ * NVERT
      if (addpoints.or.redistribute)then
      WRITE(NFIOUT,*)    ' redistributing points '
C
C***********************************************************************
C A.Neagos
C redistribution
C**********************************************************************
C 1D
C**********************************************************************
      if (ndim.eq.1)then
      nvertnew=nvert
      call param_bgnl(nvert,nvertnew,neq,y,.false.,0,0,z)
      nvert=nvertnew
      y(:,1:nvert)=z(:,1:nvert)
C**********************************************************************
C 2D
C**********************************************************************
      else if (ndim.eq.2)then
      ithet2=maxval(icord(2,1:nvert))-minval(icord(2,1:nvert))+1
      allocate(thet2vec(neq,ithet2))
      allocate(thet2vec2(neq,ithet2))
      do j=minval(icord(1,1:nvert))+1,maxval(icord(1,1:nvert))-1
        ll=0
        do i=minval(icord(2,1:nvert)),maxval(icord(2,1:nvert))
            do l=1,nvert
                if (icord(1,l).eq.j .and. icord(2,l).eq.i) then
                    ll=ll+1
                    thet2vec(:,ll)=y(:,l)
                end if
            end do
        end do
      call param_bgnl(ithet2,ithet2,neq,thet2vec,.false.,0,0,thet2vec2)
        ll=0
        do i=minval(icord(2,1:nvert)),maxval(icord(2,1:nvert))
            do l=1,nvert
                if (icord(1,l).eq.j .and. icord(2,l).eq.i) then
                    ll=ll+1
                    y(:,l)=thet2vec2(:,ll)
                end if
            end do
        end do
      end do
      elseif (ndim.eq.3)then
C**********************************************************************
C 3D
C**********************************************************************
      ithet2=maxval(icord(ndim,1:nvert))-minval(icord(ndim,1:nvert))+1
      allocate(thet2vec(neq,ithet2))
      allocate(thet2vec2(neq,ithet2))
      do j=minval(icord(1,1:nvert))+1,maxval(icord(1,1:nvert))-1
        do i=minval(icord(2,1:nvert)),maxval(icord(2,1:nvert))
            ll=0
            do k=minval(icord(3,1:nvert)),maxval(icord(3,1:nvert))
                do l=1,nvert
                    if (icord(1,l).eq.j .and. icord(2,l).eq.i
     1                  .and. icord(3,l).eq.k) then
                        ll=ll+1
                        thet2vec(:,ll)=y(:,l)
                    end if
                end do
            end do
      call param_bgnl(ithet2,ithet2,neq,thet2vec,.false.,0,0,thet2vec2)
*      write(*,*)'at',j,i
*      write(*,*)'thet2vec2',thet2vec2
*        ll=0
*        do i=minval(icord(2,1:nvert)),maxval(icord(2,1:nvert))
            ll=0
            do k=minval(icord(3,1:nvert)),maxval(icord(3,1:nvert))
                do l=1,nvert
                    if (icord(1,l).eq.j .and. icord(2,l).eq.i
     1                  .and. icord(3,l).eq.k) then
                        ll=ll+1
                        y(:,l)=thet2vec2(:,ll)
                    end if
                end do
            end do
        end do
      end do
      end if
      deallocate(thet2vec)
      deallocate(thet2vec2)
      write(*,*)'NVERT after addpoi',NVERT
      write(*,*)'NCELL after addpoi',NCELL
      end if
C***********************************************************************
C     Get gradient estimates
C***********************************************************************
       IF(NDIM.NE.NGRV) THEN
          WRITE(*,*) ' NDIM and NGRV not matching , use generic values'
         GPMA = 0
         GPVE = 0
         DO J=1,NDIM
          GPMA(J,2+J) = 1.0
         ENDDO
       ELSE
         DO J=1,NDIM
          GPVAL(J) = GRVAL(1,J)
          GPLIM(J) = GRVAL(2,J)
         DO I=1,NEQ
          GPMA(J,I) = GRVE(I+(J-1)*NEQ)
         ENDDO
         ENDDO
       ENDIF
C***********************************************************************
C     Get gradient estimates
C***********************************************************************
      GRAF = .FALSE.
C***********************************************************************
C***********************************************************************
C     Get gradient SEARCH vector
C***********************************************************************
      IF(NGSV.EQ.0) THEN
C--- assume that GRASVE has been read in XXXIN
        WRITE(*,*) ' search vector ',GRASVE(1:NEQ),
     1             ' read from gradient file'
      ELSE IF(NGSV.EQ.1) THEN
        GRASVE(1:NEQ) = GSVE(1:NEQ)
        WRITE(*,*) ' search vector ',GRASVE(1:NEQ),
     1             ' read from input file'
      ELSE
        write(*,*) ' NGSV ne 0,1 not yet allowed '
        stop
      endif
C***********************************************************************
C     
C***********************************************************************
      if(ZETA.LT.0) GRAF=.TRUE.
C***********************************************************************
C     Begin Iterations
C***********************************************************************
C.set tcmin, tcmax for easy gradient estimate
      DO J=1,NDIM
        AGPMAX = -1.D30
        AGPMIN = +1.D30
        DO L=1,NVERT
          AGPV = DOT_PRODUCT(GPMA(J,1:NEQ),Y(1:NEQ,L))
            IF(AGPV.LT.AGPMIN) AGPMIN = AGPV
            IF(AGPV.GT.AGPMAX) AGPMAX = AGPV
        ENDDO
            TCMIN(J) = AGPMIN
            TCMAX(J) = AGPMAX
      ENDDO
C***********************************************************************
C     INITIALIZE
C***********************************************************************
      IVINF(1:NVERT) = IRET(1:NVERT)
      NSTMAX = INT(STMAX)
      DIST(1:NDIM) = ONE
      WRITE(NFIOUT,8001)
 8001 FORMAT(80('*'),/,'      Start of inertm!      ',/,80('*'))
      IS = 1
      NS = NEQ-2
      IEQ = IPAR(10)
      DO L=1,NVERT
C---- Get neighbours of vertex L
      IMES = 0
      CALL CHKNEI(L,NVERT,NDIM,ICORD,
     1       NEIGHP(1,(L-1)*NDIM+1),IERR,IRET,IMES)
C-Ul
      IF(IRET(L).GE.0.AND.IERR.NE.0) THEN
        IRET(L) = -1
      ENDIF
c-UL  IRET(L) = 0
c-UL  IF(IERR.NE.0) IRET(L) = -1
C---- Get gradients Y_theta (called PSITHE)
      ENDDO
      IVINF(1:NVERT) = IRET(1:NVERT)
*Neagos-----------------------------------------------
* Boundary vertices allocation                                        *
C**********************************************************************
*     write iret into ivinf, used in desinv
      IVINF(1:NVERT) = IRET(1:NVERT)
*Neagos-----------------------------------------------
* if ndim .gt. 1 handle boundary
      if (ndim.gt.1)then
*Neagos-----------------------------------------------
* define neighbour distance
*----------------------------------------------------
      numnei=1
*     count boundary points, nrbou
      nrbou=0
      do l=1,nvert
        if (iret(l).eq.-1)then
            nrbou=nrbou+1
        end if
      end do
C**********************************************************************
C     Allocate arrays for local coordinates at boundary
C**********************************************************************
      if(.not.allocated(bouvertnr))   allocate(bouvertnr(nrbou))
      if(.not.allocated(bounrnei))    allocate(bounrnei(nrbou))
      if(.not.allocated(innernei))    allocate(innernei(nrbou))
      if(.not.allocated(evecbou))     allocate(evecbou(nrbou))
      if(.not.allocated(bouvertnei))  allocate(bouvertnei(nrbou))
      if(.not.allocated(loccorbou))   allocate(loccorbou(nrbou))
      if(.not.allocated(DISBOU))      allocate(DISBOU(NRBOU))
C**********************************************************************
C     Compute local coordinate system at boundaries   
C**********************************************************************
      call loccor(-1,.false.,numnei,nvert,ndim,nrbou,neq,y,icord,
     1            iret,bouvertnr,bounrnei,bouvertnei,
     1            evecbou,loccorbou)
C**********************************************************************
C get points from inside, innernei
C**********************************************************************
      if (1.eq.0)then
      do l=1,nrbou
      dummy=1.0d30
      do j=1,nvert
      if (iret(j).ge.0)then
        distance=sqrt(dot_product(y(3:neq,bouvertnr(l))-y(3:neq,j),
     1                             y(3:neq,bouvertnr(l))-y(3:neq,j)))
        if (distance.lt.dummy) then
        dummy=distance
        innernei(l)=j
        end if
      end if
      end do
      DISBOU(L) = DISTANCE
      end do
      else
      do l=1,nrbou
      do j=1,ndim
        if(neighp(1,(bouvertnr(l)-1)*ndim+j).eq.0) then
          innernei(l)=neighp(2,(bouvertnr(l)-1)*ndim+j)
        else if (neighp(2,(bouvertnr(l)-1)*ndim+j).eq.0) then
          innernei(l)=neighp(1,(bouvertnr(l)-1)*ndim+j)
        end if
      end do
      HEL1(1:NEQ) = (y(1:NEQ,innernei(l)) -y(1:NEQ,bouvertnr(l)))*
     1    SCAFA(1:NEQ)
      distance = SQRT(DOT_PRODUCT(HEL1(1:NEQ),HEL1(1:NEQ)))
      DISBOU(L) = DISTANCE
C
      end do
      end if
      DO L=1,NRBOU
      WRITE(*,*) L,'DISBOUKKKKKKKKKK',
     1      DISBOU(L),ICORD(1:NDIM,bouvertnr(l))
      ENDDO
C***********************************************************************
C A. Neagos                                                            *
C Begin boundary points                            *
C***********************************************************************
        DUM1=0.D0
        DO I=1,NEQ
        DUM1(I,I) = 1.0D0
        ENDDO
        allocate(boundary_points(nrbou))
        If(.not.allocated(CORBOU))
     1        allocate(CORBOU(NDIM-1,3**NDIM,NRBOU))
        If(.not.allocated(CORPBOU)) 
     1   allocate(CORPBOU(3**NDIM,NDIM-1,NRBOU))
         write(*,*) 'SSSSSSS',shape(corbou)
         write(*,*) 'SSSSSSS',shape(corpbou)
        do l=1,nrbou
            allocate(boundary_points(l)%nfunc(ndim-1))
            allocate(boundary_points(l)%icord(ndim-1,1+bounrnei(l)))
        end do
        do l=1,nrbou
            NUMBVE  = bounrnei(l)
            boundary_points(l)%nfunc(:)=3
            boundary_points(l)%ndim=ndim-1
            boundary_points(l)%neq=neq
            boundary_points(l)%nvert=bounrnei(l)+1
            boundary_points(l)%npoint=1
            boundary_points(l)%icord(:,1)=0.0d0
            do i=1,bounrnei(l)
                boundary_points(l)%icord(:,i+1)=
     1                             loccorbou(l)%coor_values(:,i)
            end do
C            write(*,*) 'pppppppppp',l,'pp',
c    1            boundary_points(l)%icord(:,1:boundary_points(l)%nvert)
            call boundary_points(l)%get_cormat_plus()
            call boundary_points(l)%allocate_dynamic_data()
            call boundary_points(l)%allocate_psth()
            if(diffbou)then
            call boundary_points(l)%allocate_diff()
            end if
           CORBOU(1:NDIM-1,1:NUMBVE,L) =
     1                  boundary_points(l)%icord(1:NDIM-1,2:NUMBVE+1)
             CORPBOU (1:NUMBVE,1:NDIM-1,L)=0
             NDIMM1=NDIM-1
           CALL MPPSX(NDIMM1,NUMBVE,CORBOU(1:NDIMM1,1:NUMBVE,L),
     1        CORPBOU (1:NUMBVE,1:NDIMM1,L))
C            write(*,*) 'iiiii',CORPBOU(1:NUMBVE,1:NDIM-1,L)
        end do
        end if
C***********************************************************************
C     Begin Iterations
C***********************************************************************
      TIME = ZERO
C***********************************************************************
C A.Neagos                                                             *
C Herausschreiben des Headers f�r ERROR.dat                            *
C***********************************************************************
*      OPEN (UNIT=7,FILE="ERROR.dat")
*      WRITE(7,*)'VARIABLES='
*      WRITE(7,*)'"manierr","errmax","timestep","Iteration Step","theta1"
*     1           ,"theta2"'
C***********************************************************************
      IF(NDIM.GT.1) GOTO  1500
      TEND = redimopt(2)
      if(deltat0.le.0) tend=0
      if(tend.le.0) goto 123
C***********************************************************************
C     EULREL
C***********************************************************************
      IF(0.EQ.1) THEN
      H = DELTAT0
      KFLAG = 1
      TOL = 1.D-3
      HMAX = 1.D3
 1111 ICALL = 1
      IMPLI = 0
      tol = 1.e-2
      IFIRCA = 0
      CALL EULREL(NTOT,RESINV,TIME,Y,TEND,TOL,HMAX,H,KFLAG,ICALL,
     1      RPAR,IPAR)
      IF(TIME.LT.0.99999D0*TEND) GOTO 1111
      ENDIF
C***********************************************************************
C     IRKC
C***********************************************************************
c     IF(1.EQ.0) THEN
c     IFIRCA = 0
c     IMPLI=1
c     DO II=1,NVERT
c     TTY(1+(II-1)*NEQ:II*NEQ) = Y(1:NEQ,II)
c     ENDDO
c     SOL = IRKC_SET(TIME,TTY,TEND,NPDES=15,RE=1.D-2,AE=1.D-6)
c1110 continue
c     CALL IRKC(SOL,F_E,F_I)
c     TIME= SOL%T
c     TTY = SOL%Y
c     DO II=1,NVERT
c     Y(1:NEQ,II) = TTY(1+(II-1)*NEQ:II*NEQ)
c     ENDDO
c      write(*,*) 'kkkkkkkk',SOL%DONE,TIME
C      IF(.Not.SOL%DONE) goto 1110
c     ENDIF
C***********************************************************************
C     LIMEX
C***********************************************************************
      RTOL= 1.d-4
      ATOL= 1.D-8
      NZC= NTOT
      NZV= 0
      IJOB = 0
      IJOB(3)=2
      IJOB(4)=NEQ
      IJOB(5)=NVERT
      IJOB(1)=1
      IJOB(7)=4
      H = DELTAT0
      ICALL = 0
 111  CONTINUE
      IFFF = 0
      HMAX = 3.D+20
      IFIRCA = 0
      IMPLI=1
C     ICALL = 0
      CALL XLIMEX(NTOT,NZC,NZV,IVALEF,IVARIG,DUMJAC,
     *      TIME,Y,DY,TEND,RTOL,ATOL,RTOLS(1),ATOLS(1),
     *      HMAX,H,IJOB,LRW,RW,LIW,IW,ICALL,RPAR,IPAR)
       ICALL = 2
      IF(IJOB(7).LT.0) THEN
        H = DELTAT0
        IFFF = IFFF+1
        ICALL = 1
c       Y = YTEMP
        IF(IFFF.GT.10) GOTO 124
      ENDIF
      IJOB(7) = 4
C222  CONTINUE
      IF(TIME.LT.0.99999D0*TEND) GOTO 111
      goto 103
c***********************************************************************
c     Integration for NDIM >
c***********************************************************************
 1500 CONTINUE
      IFIRCA = 0
      IMPLI = 0
      IF(IS.EQ.1) THEN
        DELTAT = DELTAT0
      ELSE
        DELTAT = DELTAT0
      ENDIF
      TIME = TIME + DELTAT
C***********************************************************************
C     Get first derivatives Y_theta of the manifold
C***********************************************************************
      IF(NSTMAX.EQ.0) GOTO 123
      IF(3.EQ.2) THEN
      ISTE = 2
      CALL RESINV(NTOT,TIME,Y,DYTEMP,RPAR,IPAR,IFC)
      YTEMP(:,:) = Y(:,:) + DELTAT*DYTEMP(:,:)
      ISTE = 1
      NDIV = 10
      DY = DYTEMP
      DO III=1,NDIV
      CALL RESINV(NTOT,TIME,YTEMP,DY1,RPAR,IPAR,IFC)
      DY = DY + DY1 /FLOAT(NDIV)
      YTEMP(:,:) = YTEMP(:,:) + DELTAT*DY1(:,:)/FLOAT(NDIV)
      ENDDO
      ELSE
      ISTE = 0
      CALL RESINV(NTOT,TIME,Y,DY,RPAR,IPAR,IFC)
      ENDIF
C***********************************************************************
C
C     convergence monitor
C
C***********************************************************************
C***********************************************************************
C     determine errors
C***********************************************************************
      LLL=0
      ERRTOT = ZERO
      ERRMAX = ZERO
      DO L=1,NVERT
        ERRL = SQRT(DOT_PRODUCT(DY(3:NEQ,L),DY(3:NEQ,L))/float(NEQ))
        ERRTOT = ERRTOT + ERRL
        IF(ERRL.GT.ERRMAX) LLL = L
        ERRMAX = MAX(ERRMAX,ERRL)
      ENDDO
C***********************************************************************
C     write errors
C***********************************************************************
      IF (LLL.ne.0)then
      WRITE(NFIOUT,8855) IS,manierr,errmax,(ICORD(I,LLL),I=1,NDIM)
      else
      WRITE(*,*)'LLL .eq. 0, look into inertm.f'
      end if
C8854 FORMAT(1(3X,3(1PE10.3,1X)),6(I5))
 8855 FORMAT('I.-step ',I5,', invariance defect=',1pe10.3,
     1    ', errmax=',1pe10.3,' at theta',6(I5))
C***********************************************************************
C     Get the new chemical state Y
C***********************************************************************
C***********************************************************************
C A. Neagos                                                            *
C Zeitschrittweitensteuerung auf Basis von CHKDOM                      *
C 1. Versuch
C***********************************************************************
      Z(1:NEQ,1:NVERT) = Y(1:NEQ,1:NVERT) + DELTAT*DY(1:NEQ,1:NVERT)
      dtempdt=0.0d0
      drhodt=0.0d0
      do i=1,nvert
      CALL TRASSP(IEQ,NEQ,Y(1,I),NS,
     1  temp_n,RW(2),RW(3),rho_n,HDUM,PDUM,SSPV,IEX,1,DXDS,NS,DTDS,DPDS)
*      write(*,*)temp_n,rho_n
*      read(*,*)
      CALL TRASSP(IEQ,NEQ,Z(1,I),NS,
     1temp_n1,RW(2),RW(3),rho_n1,HDUM,PDUM,SSPV,IEX,1,DXDS,NS,DTDS,DPDS)
*      write(*,*)(temp_n1-temp_n)/deltat,(rho_n1-rho_n)/deltat
*      read(*,*)
      dtempdt=dtempdt+abs(temp_n1-temp_n)
      drhodt=drhodt+abs(rho_n1-rho_n)
      end do
      dtempdt=dtempdt/(deltat*nvert)
      drhodt=drhodt/(deltat*nvert)
      call error_dat(ndim,is,deltat0,manierr,dtempdt,drhodt,
     1                 errmax,icord(:,lll))

*      read(*,*)
      Y(1:NEQ,1:NVERT)=Z(1:NEQ,1:NVERT)
C***********************************************************************
C     Check for allowed domain
C***********************************************************************
      JJJ= 1
      IF(1.EQ.2) THEN
      IFLAG = 0
 101  CONTINUE
      DO I=1,NVERT
         CALL CHKDOM(0,NEQ,Y(1,I),Y(1,I),INEG,VHIT,DUMMY)
         IF(INEG.NE.0.AND.IRET(I).EQ.0)THEN
            IFLAG = 99
            WRITE(NFIOUT,*) 'bad vertex', I, INEG
            CALL DELPOI(I,NDIM,N2DIM,NVERT,NEQ,NCELM,NCELL,
     1                 IY,ICORD,IRET,IVERT,Y)
            GOTO 101
         ENDIF
      ENDDO
      IF(IFLAG.EQ.99)THEN
 102     CONTINUE
         DO I=1,NVERT
            IF(IRET(I).EQ.10)THEN
               CALL DELPOI(I,NDIM,N2DIM,NVERT,NEQ,NCELM,NCELL,
     1                    IY,ICORD,IRET,IVERT,Y)
               GOTO 102
            ENDIF
         ENDDO
         CALL ADDPOI(NDIM,N2DIM,NEQ,NVERT,NCELL,Y,
     1               ICORD,IRET,IY,IVERT,IUNIT,RW,LRW,IW,LIW)
      ENDIF
      ENDIF
C***********************************************************************
C     Decision of further iterations
C***********************************************************************
      IS = IS+1
      IF(IS.LE.NSTMAX) GOTO 1500
C***********************************************************************
C     End of Iterations
C***********************************************************************
 123  continue
C
C***********************************************************************
C     Delete boundary points
C***********************************************************************
 103  CONTINUE
C***********************************************************************
C A. Neagos                                                            *
C Berechnung der Summe aller Massenbr�che und der mittleren Molmasse   *
C***********************************************************************
      SUMW=0.0D0
      MASSFR=0.0D0
      DO I=1,NVERT
      DO J=3,NEQ
      MASSFR=Y(J,I)*XMOL(J-2)
      SUMW(I)=SUMW(I)+MASSFR
      END DO
      END DO
C***********************************************************************
C     Compute needed return values
C***********************************************************************
 124  CONTINUE
C***********************************************************************
C A. Neagos                                                            *
C Vorbereitung zur Berechnung der Entropie:
C NASA-Koeffizienten HLHH und Temperaturschranken HINF aus SSPV
C***********************************************************************
      NSPEC=NEQ-2
      DO I=1,NSPEC
      HLHH(1:7,I)=SSPV((NSPEC+1+(I-1)*7):(NSPEC+7+(I-1)*7))
      HLHH(1:7,NSPEC+I)=SSPV((8*NSPEC+(I-1)*7+1):(NSPEC*8+I*7))
      HINF(1:3,I)=SSPV((NSPEC*15+1+3*(I-1)):(NSPEC*15+3+(I-1)*3))
      END DO
C---- Loop over vertices
      open(unit=20,file='Initial_homrea',action='write')
      K=0
      DO I=1,NVERT
C---- Check for allowed domain
         CALL CHKDOM(0,NEQ,Y(1,I),Y(1,I),INEG,VHIT,DUMMY)
         IF(INEG.NE.0)THEN
            WRITE(NFIOUT,*) 'schlechter Punkt', I, 'INEG=',INEG
C           IRET(I) = -10
         ENDIF
C---- Copy Y into PROP
         PROP(IISMF:IISMF-1+NEQ,I) = Y(1:NEQ,I)
C---- Store physical quantities in RV
         CALL TRASSP(IEQ,NEQ,Y(1,I),NS,
     1    RW(1),RW(2),RW(3),DDU,HDUM,PDUM,SSPV,IEX,1,DXDS,NS,DTDS,DPDS)
C***********************************************************************
C A. Neagos                                                            *
C Initialisierung der Entropie und anschlie�ende Berechnung
C***********************************************************************
          ENTROPY(I)=0.0d0
          DO L=1,NSPEC
          CALL CALHSI(NSPEC,L,RW(1),HLHH,HINF,ENTHALPY,SK(L),6)
          IF (RW(2+L).GT.1.0D-99)THEN
          SK(L)=SK(L)-RGAS*DLOG(RW(2+L)/RW(2))
          END IF
          END DO
CBUG
          ENTROPY(I)=DDOT(NSPEC,RW(3:NSPEC+2),1,SK,1)/DDU
C---- Store rates
      CALL MASRES(NEQ,TIME,Y(1,I),PROP(IISMFP:IISMFP-1+NEQ,I),
     1     RPAR,IPAR,IERST)
        IF(IERST.NE.0) GOTO 930
C---- Flag
         PROP(IIRV-1+1,I) = FLOAT(IRET(I))
C---- Temperature
         PROP(IIRV-1+11,I) = RW(1)
C---- Density
         PROP(IIRV-1+12,I) = DDU
C---- Eigenvalues
         PROP(IIRV-1+4:IIRV-1+7,I) = ZERO
C---- Mixture fraction
         PROP(IIRV-1+8,I) = 1.0D0/DTDS(1)
C---- Mean molar mass
         XSUM = ZERO
         XSUM = SUM(Y(3:NEQ,I))
         X = ZERO
         DO j=1,NS
            X(J) = Y(2+J,I) / XSUM
         ENDDO
         XM = ZERO
         DO J=1,NS
            XM = XM + X(J) * SSPV(J)
         ENDDO
         PROP(IIRV-1+13,I) = XM
C---- Negmassf
C        PROP(IIRV-1+10,I) = ZERO
C---- GRADIENT information
         PROP(IIRV-1+15,I) = DCOV(I)
         END DO
C***********************************************************************
C     write data file fort.94
C***********************************************************************
        CALL IOGPAR(1,NFIIN,NPROP,NCELL,NCELL,NVERT,NDIM,N2DIM,
     1            NS,NREAC,IY,IVERT,ICORD,IRET,PROP,SYMPRP,
     2            CMAT,MIX,NC)
C***********************************************************************
C     write tecplot output
C***********************************************************************
      NPROPT =NDIM+2*NEQ+15
      DO I=1,NVERT
      PROP(NPROPT+      1:NPROPT+  NEQ,I) = TEMP1(1:NEQ,1,I)
      PROP(NPROPT+  NEQ+1:NPROPT+2*NEQ,I) = TEMP2(1:NEQ,1,I)
C     PROP(NPROPT+2*NEQ+1:NPROPT+3*NEQ,I) = TEMP3(1:NEQ,1,I)
      ENDDO
      NPROPT =NPROPT+2*NEQ
      CALL XTPOUT(NFIPLO,NPROPT,SYMPRP,NVERT,NPROP,PROP,ICORD,
     1     NDIM,N2DIM,NCELL,IVERT)
C***********************************************************************
C A. Neagos:
C Output von fort.99, Psi, grad_Psi und divgrad_Psi,
C um daraus Anfangsl�sung f�r 2D bzw. 3D-Redim mit Hilfe von
C Prepredim zu erstellen.
C***********************************************************************
      IF (NDIM .EQ. 1) THEN
      CALL XXXOU(INFO,99,NEQ,NPROP,SYMPRP,PROP,NVERT,NDIM)
      else if (ndim.eq.2)then
      CALL XXXOU2(INFO,99,NEQ,NPROP,SYMPRP,PROP,NVERT,NDIM,ICORD)
      END IF
C***********************************************************************
C A.Neagos
C***********************************************************************
C801  FORMAT(7(1PE10.3,1X))
      WRITE(*,*)' '
      WRITE(*, 829)
      WRITE(*,1830)'CONGRATULATION - REDIM SUCCESSFULLY GENERATED '
      WRITE(*, 829)
  829 FORMAT(1X,79('*'))
 1830 FORMAT(1X,10('*'),7X,A45,7X,10('*'))
      RETURN
C***********************************************************************
C     Error exits
C***********************************************************************
C9200 CONTINUE
C     WRITE(NFIOUT,9205)
 9205 FORMAT(' ',' FATAL ERROR do to PCHIP ')
      GOTO 999
 930  CONTINUE
      IERR = IERST
      WRITE(NFIOUT,935) IERR
 935  FORMAT(' ',' ERROR IN COMPUTATION OF SOURCE TERMS:
     1     IERR =',I3,//)
      GOTO 999
 999  WRITE(NFIOUT,998) L
 998  FORMAT(' ','  Error in -INERTM- at point:',I4,/)
CBZ - 05.12.2011
 2223 FORMAT(' ',32('+'),' ABNORMAL END ',32('+'))
      WRITE(NFIOUT,2223)
      WRITE(NFIOUT,2223)
      WRITE(NFIOUT,2223)
CBZ end
      STOP
C***********************************************************************
C     End of -INERTM-
C***********************************************************************
      END
C***********************************************************************
C
C
C
C
C***********************************************************************
      SUBROUTINE REORDER(NDIM,N2DIM,NEQ,NVERT,NCELL,PSI,ICORD,
     1                  IRET,IY,IVERT)
C***********************************************************************
C
C     this routine adds points to the mesh, which fulfill the
C     boundary condition
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION PSI(NEQ,NVERT),PSIDUM(NEQ,NVERT)
      DIMENSION ICORD(NDIM,NVERT),IRET(NVERT),
     1          IY(NDIM,NCELL),IVERT(N2DIM,NCELL)
      DIMENSION ICORDDUM(NDIM,NVERT),IRETDUM(NVERT),
     1          IYDUM(NDIM,NCELL),IVERTDUM(N2DIM,NCELL)
      PARAMETER (ZERO=0.0D0,ONE=1.0D0,TWO=2.0D0)
      DATA NFIOUT/6/
      ll=0
      do j=minval(icord(2,:)),maxval(icord(2,:))
        do i=minval(icord(1,:)),maxval(icord(1,:))
            do l=1,nvert
                if (icord(1,l).eq.i .and. icord(2,l).eq.j) then
                    ll=ll+1
                    ICORDDUM(:,ll)=ICORD(:,l)
                    IRETDUM(ll)=IRET(l)
                    PSIDUM(:,ll)=PSI(:,l)
                end if
            end do
        end do
      end do
      ll=0
      do j=minval(iy(2,:)),maxval(iy(2,:))
        do i=minval(iy(1,:)),maxval(iy(1,:))
            do l=1,ncell
                if (iy(1,l).eq.i .and. iy(2,l).eq.j) then
                    ll=ll+1
                    IYDUM(:,ll)=IY(:,l)
                    IVERTDUM(:,ll)=IVERT(:,l)
                end if
            end do
        end do
      end do
      PSI=PSIDUM
      ICORD=ICORDDUM
      IRET=IRETDUM
      IY=IYDUM
      LAUF = 0
      nl=maxval(icord(1,:))-minval(icord(1,:))+1
      nk=maxval(icord(2,:))-minval(icord(2,:))+1
      DO K=1,nk-1
      DO L=1,nl-1
        LAUF = LAUF + 1
        IVERT(1,LAUF) = L +     NL*(K-1)
        IVERT(2,LAUF) = L + 1 + NL*(K-1)
        IVERT(3,LAUF) = L     + NL*(K  )
        IVERT(4,LAUF) = L + 1 + NL*(K  )
      ENDDO
      ENDDO
C***********************************************************************
C     Initialize
C***********************************************************************

C***********************************************************************
C     Regular exit
C***********************************************************************
      RETURN
C***********************************************************************
C     Error exits
C***********************************************************************
 900  CONTINUE
      WRITE(NFIOUT,905)
 905  FORMAT(' ','  ERROR cannot create new cell ',//)
      GOTO 999
 910  CONTINUE
      WRITE(NFIOUT,915) IN
 915  FORMAT(' ',' ERROR in index of neighbour: IN =',I2,//)
      GOTO 999
 999  WRITE(NFIOUT,998)
 998  FORMAT(' ','  Error in -ADDPOI- ')
      STOP
C***********************************************************************
C     End of -ADDPOI-
C***********************************************************************
      END
C
C
      SUBROUTINE ADDPOI(NDIM,N2DIM,NEQ,NVERT,NCELL,PSI,ICORD,
     1                  IRET,IY,IVERT,IUNIT,RW,LRW,IW,LIW)
C***********************************************************************
C
C     this routine adds points to the mesh, which fulfill the
C     boundary condition
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      DIMENSION PSI(NEQ,*),PSITHE(MNEQU*NDIMM*NVERM)
      DIMENSION ICORD(NDIMM*NVERM),IRET(NVERM),
     1          IY(NDIMM*NCELM),IVERT(N2DIMM*NCELM)
      DIMENSION NEIGH(2*NDIMM),DIST(NDIMM)
      DIMENSION IFOC(NCELM*NDIMM),IYN(NDIMM),IUNIT(NDIMM*N2DIMM)
      DIMENSION RW(LRW),IW(LIW)
      PARAMETER (ZERO=0.0D0,ONE=1.0D0,TWO=2.0D0)
      DATA NFIOUT/6/
C***********************************************************************
C     Initialize
C***********************************************************************
      NCEL3 = NCELL
      NCELO = 0
      INFO = 4
      DIST(1:NDIM) = ONE
C---- Get Gradients of mesh
      DO L=1,NVERT
       IMES = 0
         CALL CHKNEI(L,NVERT,NDIM,ICORD,NEIGH,IERR,IRET,IMES)
         CALL CALGRA(NEQ,NDIM,NEQ,PSI,L,
     1        PSITHE((L-1)*NEQ*NDIM+1),NEIGH,DIST,IERR)
      ENDDO
C***********************************************************************
C     Add new cells / points
C***********************************************************************
      DO LL=1,NCEL3
      NCELO = NCELL
      CALL NEICEL_REDIM(LL,NDIM,NCELL,NCELM,IY,NFIOUT,IERR)
      DO II = NCELO+1,NCELL
      CALL CRECEL(INFO,ICF,NDIM,N2DIM,IY(1+(II-1)*NDIM),
     1           IVERT(1+(II-1)*N2DIM),NVERM,
     1           NVERT,ICORD,IRET,IUNIT,NFIOUT)
       write(*,*) 'oooooooooooo',ii,ncel3, ncell
*      IF(ICF.EQ.0) GOTO 900
      ENDDO
      ENDDO
C---- Get properties of new points
      DO 20 L=1,NVERT
      IF(IRET(L).EQ.10)THEN
       IMES = 0
         CALL CHKNEI(L,NVERT,NDIM,ICORD,NEIGH,IERR,IRET,IMES)
         DO 33 IN=1,2
         DO 32 ID=1,NDIM
         IF(NEIGH((ID-1)*2+IN).EQ.0)THEN
            IF(IN.EQ.1)THEN
               IPOI = NEIGH(ID*2)
               IF(IPOI.EQ.0) STOP
               IF(IRET(IPOI).EQ.-1 .or. IRET(IPOI).EQ.0)THEN
                  INDEX = (IPOI-1)*NEQ*NDIM
                  PSI(1:NEQ,L) = PSI(1:NEQ,IPOI)! -
*     1            PSITHE(INDEX+(ID-1)*NEQ+1:INDEX+ID*NEQ)
               ENDIF
            ELSEIF(IN.EQ.2)THEN
               IPOI = NEIGH((ID-1)*2+1)
               IF(IPOI.EQ.0) STOP
               IF(IRET(IPOI).EQ.-1.or. IRET(IPOI).EQ.0)THEN
                  INDEX = (IPOI-1)*NEQ*NDIM
                  PSI(1:NEQ,L) = PSI(1:NEQ,IPOI)! +
*     1            PSITHE(INDEX+(ID-1)*NEQ+1:INDEX+ID*NEQ)
               ENDIF
            ELSE
               GOTO 910
            ENDIF
         ENDIF
   32    CONTINUE
   33    CONTINUE
      ENDIF
   20 CONTINUE
C***********************************************************************
C     Regular exit
C***********************************************************************
      RETURN
C***********************************************************************
C     Error exits
C***********************************************************************
C900  CONTINUE
C     WRITE(NFIOUT,905)
C905  FORMAT(' ','  ERROR cannot create new cell ',//)
C     GOTO 999
 910  CONTINUE
      WRITE(NFIOUT,915) IN
 915  FORMAT(' ',' ERROR in index of neighbour: IN =',I2,//)
      GOTO 999
 999  WRITE(NFIOUT,998)
 998  FORMAT(' ','  Error in -ADDPOI- ')
      STOP
C***********************************************************************
C     End of -ADDPOI-
C***********************************************************************
      END
      SUBROUTINE RESINV(N,TIME,Y,DY,RPAR,IPAR,IFC)
C***********************************************************************
C
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
C***********************************************************************
C     Arrays
C***********************************************************************
      COMMON/BHJEUL/HJ
      COMMON/BSOL/IMPLI
      COMMON/BINVA/ZETA,DELTAT,ISTE,NAVERT,NAEQ,NADIM,
     1          IVINF(NVERM),NEIGHP(2,NDIMM*NVERM)
C---- Input
C---- Arrays needed for the modified Fraser algorithm
      DOUBLE PRECISION, DIMENSION(N)             :: Y,DY
C---- Work arrays
      DIMENSION RPAR(*),IPAR(*)
      COMMON/BSCAD/DCOV(NVERM)
      COMMON/BERV/ERVRO((MNSPE+2)**2),ERVCO((MNSPE+2)**2),NZVT
C***********************************************************************
C     get numbers
C***********************************************************************
      IEQ    = IPAR(10)
      NSPEC  = IPAR(3)
      ITRM   = IPAR(13)
C***********************************************************************
C     Call of DESINV
C***********************************************************************
      CALL DESINV(NADIM,NAEQ,IEQ,NSPEC,NAVERT,DELTAT,ZETA,Y,DY,
     1          IVINF,NEIGHP,RPAR,IPAR,ERVRO,ERVCO,NZVT,
     1          DCOV,ITRM,ISTE,IRDES,IMPLI)
C***********************************************************************
C     check return code and return
C***********************************************************************
      IF(IRDES.NE.0) IFC = 9
      RETURN
      END
      SUBROUTINE DESINV(NDIM,NEQ,IEQ,NSPEC,NVERT,DELTAT,ZETA,Y,
     1      DY,IRET,NEIGH,RPAR,IPAR,ERVROW,ERVCOL,NZEV,
     1  DCOV,ITRM,ISTE,IRDES,IMPLI)
C***********************************************************************
C
C     Compute Invariant Manifold
C
C                                  | 0 0 |
C     dY/dt = (Y_theta, Y_theta^o)*|     |*(Y_theta, Y_theta^o)^-1*F(Y)
C                                  | 0 I |
C     or
C
C     dY/dt = (I-Y_theta*Y_theta^+)*F(Y)
C
C     using a semi-implicit procedure
C
C***********************************************************************
      USE RDMESH
      USE GRAFI
      use cor_module
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      PARAMETER (ONE = 1.0D0, ZERO = 0.0D0,SMALL=1.D-30)
C***********************************************************************
C     Arrays
C***********************************************************************
C---- INPUT
      LOGICAL CONGRA,TEST
C***********************************************************************
C A. Neagos
C Polynomial fit array allocation
C***********************************************************************
      INTEGER, DIMENSION(NDIM-1,N2DIMM*N2DIMM)          :: COORDINATES
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:,:,:)   ::psialpha
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:,:,:)   ::psialphap
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:,:,:)   ::projecbou
      DOUBLE PRECISION, DIMENSION(1,NEQ)                ::PSTH_IN_P
      DOUBLE PRECISION, DIMENSION(NEQ,1)                ::PSTH_IN
      DOUBLE PRECISION, DIMENSION(NDIM)                 ::GRADTHETA
      DOUBLE PRECISION, DIMENSION(NEQ)                  ::DELTAPSI
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)               ::QC,HHH
C***********************************************************************
C
C***********************************************************************
      INTEGER,          DIMENSION(NVERT)         :: IRET
      LOGICAL         , DIMENSION(NDIM,NVERT)    :: LGRAPF,LGRAPB
      DOUBLE PRECISION, DIMENSION(2,NDIM)        :: DK
      DOUBLE PRECISION, DIMENSION(NEQ)      :: SMFS,VRS,DIFF,DIFF2
      DOUBLE PRECISION, DIMENSION(NEQ)           :: VRSL
      DOUBLE PRECISION, DIMENSION(NEQ)           :: TBPSMF,TBPDIF
      DOUBLE PRECISION, DIMENSION(NEQ)           :: HEL1
      DOUBLE PRECISION, DIMENSION(NEQ,nvert)           :: dumgrad
      DOUBLE PRECISION, DIMENSION(NEQ,NDIM,NVERT) ::PSITHR
      DOUBLE PRECISION, DIMENSION(NEQ,NDIM,NVERT) :: CEGRA
      DOUBLE PRECISION, DIMENSION(NEQ,NDIM)       :: UPWGRA
      DOUBLE PRECISION, DIMENSION(NEQ,NDIM)       :: TEMPPSITH
      DOUBLE PRECISION, DIMENSION(NDIM,NEQ)       :: TEMPPSITHP
      DOUBLE PRECISION, DIMENSION(NEQ,3**NDIM)       :: DELPSI
      DOUBLE PRECISION, DIMENSION(NDIM,NEQ)       :: UPWGRP
      DOUBLE PRECISION, DIMENSION(NEQ,NDIM,NDIM,NVERT) :: DPTHAL
      DOUBLE PRECISION, DIMENSION(NDIM,NEQ,NDIM,NVERT) :: PTPHAL
      DOUBLE PRECISION, DIMENSION(NDIM,NEQ,NVERT)  :: PSITHP,PSTPC
      DOUBLE PRECISION, DIMENSION(NEQ,NVERT)     :: Y,DY
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ,NVERT)       :: PROJEC
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)       ::XJAC,WA1
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)       :: SCAMA
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ,NVERT) :: DIFMA,PROJ
      DOUBLE PRECISION, DIMENSION(NVERT)            :: NDY,NSRCDIFF
C     DOUBLE PRECISION, DIMENSION(NSPEC,NSPEC)   :: DSS
C     DOUBLE PRECISION, DIMENSION(NSPEC)         :: DES,DSE,DSP
CSTO  DOUBLE PRECISION, DIMENSION(NSPEC,NSPEC)   :: projspec
CSTO  DOUBLE PRECISION, DIMENSION(Ndim,NSPEC)   :: psithpspec
      DOUBLE PRECISION, DIMENSION(NEQ,NDIM,NDIM,NVERT) :: SECLOC
      DOUBLE PRECISION, DIMENSION(NDIM)          :: DIST,TTT,
     1                                             DIVTHE,DIVGRA
     1
C     DOUBLE PRECISION, DIMENSION(NDIM,NEQ)      :: PA
      DOUBLE PRECISION, DIMENSION(NSPEC,NEQ)      :: DMATS
      DOUBLE PRECISION, DIMENSION(NEQ)            :: DMATE
      DOUBLE PRECISION, DIMENSION(NEQ,NDIM)       :: DGRAXI
      DOUBLE PRECISION, DIMENSION(NVERT)            :: RHO,DDDG
      INTEGER, DIMENSION(2,NDIM,NVERT)           :: NEIGH
C---- Work arrays
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)       :: ERVROW,ERVCOL
      DOUBLE PRECISION, DIMENSION(NEQ*NEQ)         :: RW
      INTEGER,          DIMENSION(NEQ)           :: IW
      DOUBLE PRECISION, DIMENSION(NVERT)        :: DCOV
      DOUBLE PRECISION, DIMENSION(NDIM,NEQ)      :: GRPRO
      LOGICAL LIMGRA,UPWI,FREEB, both
      DOUBLE PRECISION, DIMENSION(NEQ,NZEV)       :: CSQ
      DOUBLE PRECISION, DIMENSION(NEQ,NVERT)     :: DPSTGT
      DOUBLE PRECISION, DIMENSION(NEQ,NDIM,NVERT)     :: DPSTGTF
      DOUBLE PRECISION, DIMENSION(NEQ,NDIM,NVERT)     :: DPSTGTC
C***********************************************************************
C     Arrays
C***********************************************************************
C---- Work arrays
      DIMENSION RPAR(*),IPAR(*)
C---- Dummy arrays
      COMMON/BIFIR/IFIRCA
      COMMON/BTTTT/SSCAM((MNSPE+2)*(MNSPE+2)),SCAFA(MNSPE+2)
      DATA NFIOUT/6/
      COMMON/TTTEMP/TEMP1(MNSPE+2,1,NVERM),TEMP2(MNSPE+2,1,NVERM),
     1     TEMP3(MNSPE+2,1,NVERM)
      COMMON/BCELRA/NNEWCLD,NOLDCLD
************************************************************************
C A. Neagos
C COMMON Block mit EMAT (Matrix der Erhaltungsgroessen)
C***********************************************************************
       COMMON/BICMAT/COMAT(MNCOV*MNEQU),EMAT((MNSPE+2)*MNCON)
      SAVE
C***********************************************************************
C     INITIALIZE
C***********************************************************************
***********************************************************************
C A. Neagos                                                            *
C                                                                      *
C �berpr�fung der Elementerhaltung, globale Variable ELEMENT wird in   *
C grtheout.f herausgeschrieben                                         *
C                                                                      *
C***********************************************************************
      DO I=1,NEQ
      DO J=1,NZEV
      CSQ(I,J) = EMAT(I+(J-1)*NEQ)
      ENDDO
      ENDDO
      DO I=1,NVERT
      ELEMENT(1:NZEV,I)=MATMUL(TRANSPOSE(CSQ(:,:)),Y(:,I))
      END DO
      CALL ORBAS(NEQ,NZEV,NEQ,CSQ,NEQ,QC,NEQ*(NEQ+1)
     1           ,QF,NEQ,IW,2,IERR)
      IF(IERR.GT.0) WRITE(*,*) 'IERR ORBAS= ', IERR
      HHH=QC
      CALL DGEINV(NEQ,QC,RW,IW,IERR)
C***********************************************************************
C Scaling Matrix
C***********************************************************************
CUM   THIS WAS THE BUG
      IF(IFIRCA.EQ.0) THEN
        SCAMA=0.D0
        DO I=1,NEQ
          SCAMA(I,I) = SCAFA(I)**2
        ENDDO
        CALL DCOPY(NEQ*NEQ,SCAMA,1,SSCAM,1)
      ELSE
        CALL DCOPY(NEQ*NEQ,SSCAM,1,SCAMA,1)
      ENDIF
C***********************************************************************
      IDIFVE=     1
      UPWI= .FALSE.
      FREEB = .false.
      IF(FREB.GT.0) FREEB = .TRUE.
      DK = 1.0D0
      IRDES = 0
      LRW=NEQ*NEQ
      LIW = NEQ
      IADDSE=    1
      IF(ABS(ZETA).LT.SMALL) IADDSE = 0
      IF(ISTE.EQ.1) IADDSE = 0
      IF(ISTE.EQ.2) IADDSE = -1
      IF(ZETA.LT.-100.D0)   IADDSE=   -1
      DIST(1:NDIM) = ONE
      DCOV = 0
C***********************************************************************
C
C     Get gradient estimates
C
C***********************************************************************
        COEFF = abs(ZETA)
      IF(ZETA.LT.0.AND.ZETA.GT.-2) THEN
        CONGRA = .FALSE.
        IF(IFIRCA.EQ.0) THEN
* A.Neagos
* old version of gradient procedure
cLE1    CALL EVGR2(NDIM,NEQ,NVERT,Y,YGR,dumgrad,XSEPSI,DCOV)
* new version
         CALL EVGR3(NDIM,NEQ,NVERT,NEQ,scama,Y,YGR,dumgrad,XSEPSI,DCOV)
        ENDIF
      ELSE
        CONGRA = .TRUE.
      ENDIF
C***********************************************************************
C A. Neagos:                                                           *
C Ver�nderung der Gradienten durch Multiplikation mit St�rung
C***********************************************************************
      do l=1,nvert
        xgrpsi(:,l)=changegrad*dumgrad(:,l)
      end do
C***********************************************************************
C
C
C     Calculate transport coefficients at center nodes
C
C
C***********************************************************************
      DO L=1,NVERT
        CALL GETDIC(ITRM,ieq,neq,Y(1,L),nspec,rho(L),
     1          DMATS,DMATE,XLA,VIS,DDDG(L),RPAR,IPAR,ierr)
        if(ierr.ne.0) goto 999
        DIFMA(1,:,L)     = DMATE
        DIFMA(2,:,L)     = ZERO
        DIFMA(3:NEQ,:,L) = DMATS(1:NSPEC,:)
      ENDDO
C***********************************************************************
C A. Neagos
C Start boundary handling
C***********************************************************************
      if(ndim.gt.1)then
      if(.not.allocated(psialpha))
     1    allocate(psialpha(neq,ndim,nrbou))
      if(.not.allocated(psialphap))
     1    allocate(psialphap(ndim,neq,nrbou))
      if(.not.allocated(projecbou))allocate(projecbou(neq,neq,nrbou))
C***********************************************************************
C A. Neagos
C compute psth and/or diffusion on boundary
C***********************************************************************
C
      do i=1,nrbou
        IVC = bouvertnr(i)
        boundary_points(i)%difma(:,:,1)=difma(:,:,bouvertnr(i))
        boundary_points(i)%psi(:,1)=y(:,bouvertnr(i))
        boundary_points(i)%xgrpsi(:,1)=xgrpsi(:,bouvertnr(i))
        DIFF2=0 
          NUMBVE = BOUNRNEI(I) 
        do j=1,bounrnei(i)
          boundary_points(i)%difma(:,:,j+1)=
     1                    difma(:,:,bouvertnei(i)%vertice_values(j))
          boundary_points(i)%psi(:,j+1)=
     1                    y(:,bouvertnei(i)%vertice_values(j))
          boundary_points(i)%xgrpsi(:,j+1)=
     1                    xgrpsi(:,bouvertnei(i)%vertice_values(j))
          boundary_points(i)%scama=scama
          DELPSI(1:NEQ,J) = boundary_points(i)%psi(:,j+1)-
     1         boundary_points(i)%psi(:,1) 
          IF(J.EQ.1) DIFF2(1:neq)= DIFF2(1:neq)-
     1                    y(:,bouvertnei(i)%vertice_values(j))
          IF(J.EQ.2) DIFF2(1:neq)= DIFF2(1:neq)+
     1                    y(:,bouvertnei(i)%vertice_values(j))
        end do
          TEMPPSITH(1:NEQ,1:NDIM-1) = 
     1      MATMUL(DELPSI(1:NEQ,1:NUMBVE),
     1     CORPBOU(1:NUMBVE,1:NDIM-1,I))
          call boundary_points(i)%compute_psth()
c       write(*,*) 'lllllll',TEMPPSITH(1:NEQ,1:NDIM-1)    
c       write(*,*) 'kkkkkkl',
c    1      shape(boundary_points(i)%psth)
c       write(*,*) 'kkkkkkl',boundary_points(i)%psth(:,:,1:1)
c       write(*,*) 'kkkkkkk',Diff2(1:neq)/Diff2(1)
c       write(*,*) 'kkkkkkk'                
C***********************************************************************
C A. Neagos
C compute diffusion on boundary if necessary
C***********************************************************************
      if(diffbou)then
          call boundary_points(i)%compute_diffusion()
      end if
C***********************************************************************
C A.Neagos: Project xgrpsi onto boundary
C***********************************************************************
          xgrpsi(:,bouvertnr(i))=matmul(
     1         matmul(boundary_points(i)%psth(:,:,1),
     1                boundary_points(i)%psthp(:,:,1)),
     1         xgrpsi(:,bouvertnr(i)))
C        write(*,*) '888888888888',xgrpsi(:,bouvertnr(i))
      CALL GENPSI(NEQ,NDIM-1,NEQ,SCAMA,TEMPPSITH(1:NEQ,1:NDIM-1),
     1       TEMPPSITHP(1:NDIM-1,1:NEQ))
          xgrpsi(:,bouvertnr(i))=matmul(TEMPPSITH(1:NEQ,1:NDIM-1),
     1        matmul(TEMPPSITHP(1:NDIM-1,1:NEQ),xgrpsi(:,bouvertnr(i))))
C        write(*,*) '999999999999',xgrpsi(:,bouvertnr(i))
C***********************************************************************
C A.Neagos: proj. operator onto normal subspace of boundary
C***********************************************************************
      projecbou(:,:,i)=-MATMUL(boundary_points(i)%psth(:,:,1),
     1                         boundary_points(i)%psthp(:,:,1))
      do jj=1,neq
      projecbou(jj,jj,i)=one+projecbou(jj,jj,i)
      end do
      end do
      end if
C***********************************************************************
C A. Neagos
C compute psth and/or diffusion on inner
C***********************************************************************
C***********************************************************************
C***********************************************************************
C
C
C     Calculate gradients
C
C
C***********************************************************************
      CALL  DIRFLA(NDIM,NVERT,LGRAPF,LGRAPB,NEIGH,IERR)
C***********************************************************************
C     Calculate Gradients at half nodes (forward)
C***********************************************************************
      DO I=1,NVERT
        CALL HALNO(NEQ,NDIM,NEQ,Y,NVERT,I,PSITHR(1,1,I),NEIGH,DK,IERR)
      ENDDO
C***********************************************************************
C     Calculate gradients at center nodes
C***********************************************************************
      DO I=1,NVERT
      CALL CENGRA(NEQ,NDIM,NVERT,I,PSITHR,CEGRA(1,1,I),NEIGH,DK,IERR)
C***********************************************************************
C A. Neagos:                                                           *
C Korrektur von psitheta
C***********************************************************************
      if (correctmass)then
      do k=1,ndim
      corr=dot_product(cegra(3:neq,k,i),xmol(1:nspec))/
     1                        sum(xmol(1:nspec))
      do j=1,neq-2
        cegra(j+2,k,i)=cegra(j+2,k,i)-corr
      end do
      end do
      end if
      ENDDO
      PSTH=CEGRA
C***********************************************************************
C     Calculate Pseudo-inverse at center nodes
C***********************************************************************
      DO I=1,NVERT
      IERR=0
C--- buggy
C     PSITHP(1,1,I)=0.0d0
      CALL GENPSI(NEQ,NDIM,NEQ,SCAMA,CEGRA(1,1,I),PSITHP(1,1,I))
      psthp(:,:,i)=psithp(:,:,i)
      ENDDO
C***********************************************************************
C     Loop over vertices: D * psitheta at half nodes
C***********************************************************************
      DO L=1,NVERT
      CALL HALFS(NEQ,NDIM,NVERT,L,PSITHR,LGRAPF,CEGRA,DIFMA,
     1 NEQ,SCAMA,DPTHAL(1,1,1,L),PTPHAL(1,1,1,L),NEIGH,DK,IERR)
      ENDDO
C---- DPTHAL(:,:,j,i) is the Matrix D psitheta at the half node of l in
C                    positive j-direction
C---- PTPHAL(:,:,j,i) is the Matrix psitheta+ at the half node of l in
C                    positive j-direction
C***********************************************************************
C
C     Loop over vertices:
C     calculate reduced coordinate gradient
C
C***********************************************************************
C     DO L= 1, NVERT
C***********************************************************************
C     get projection
C***********************************************************************
C     CALL GRAPRO(L,NEQ,NDIM,GPMA,CEGRA(1,1,L),GRPRO)
C     ENDDO
C***********************************************************************
C
C     Block for analytical gradient => zeta=1
C
C***********************************************************************
      IF(IFIRCA.EQ.0.AND.CONGRA) THEN
      DO L=1,NVERT
        IGRAE = 1
        TTT=GPVAL
        CALL GETDIS(IGRAE,NEQ,NDIM,COEFF,Y(1,L),GPMA,TTT,TCMIN,TCMAX,
     1              SCAMA,CEGRA(1,1,L),DIVTHE,DIVGRA,DETPSITH(L))
        TTTL(:,L) =DIVTHE
        SSSL(:,L) =DIVGRA
      ENDDO
      IF(IDIFVE.LT.0) THEN
      DO L=1,NVERT
        CALL HAXTTL(NEQ,NDIM,NVERT,L,TTTL,NEIGH,IERR,TTTHAL(1,1,L))
      ENDDO
      ENDIF
      ENDIF
C***********************************************************************
C
C     Block for tabulated variables => zeta=-1
C
C***********************************************************************
      IF(IFIRCA.EQ.0.AND.(.NOT.CONGRA)) THEN
      DO L=1,NVERT
C***********************************************************************
C     calculate local gradients tttl
C***********************************************************************
C***********************************************************************
C A. Neagos:                                                           *
C INFO bestimmt, auf welche Art grad_theta berechnet wird. Siehe       *
! C Subroutine GETGRN.
C***********************************************************************
CTODO
CLe1
      INFO = 1
      CALL GETGRN(INFO,LIMGRA,NEQ,NDIM,XGRPSI(1,L),GPMA,GPVAL,GPLIM,
     1      CEGRA(1,1,L),PSITHP(1,1,L),TTTL(1,L),DETPSITH(L))
      IF(LIMGRA) WRITE(*,*) '   grid ',L
      IF (NDIM .EQ. 1) THEN
      GRDPS2(:,L)=MATMUL(CEGRA(:,:,L),TTTL(:,L))
      END IF
C---- psitheta+*GRAPSI  at half nodes
C***********************************************************************
C     calculate gradient at half nodes
C***********************************************************************
      IF(IDIFVE.LT.0) THEN
      CALL HALFTTL(NEQ,NDIM,NVERT,L,XGRPSI,PTPHAL,NEIGH,IERR,
     1      TTTHAL(1,1,L))
      ENDIF
C---- TTTHAL(:,j,L) is the TTT at the half node of l in
C                    positive j-direction
      ENDDO
      ENDIF
C***********************************************************************
C A. Neagos:                                                           *
C zusaetzliche Version f�r Diffusion: (D*Psi_theta*grad(theta))_theta, genannt DPSTGTC
C Muss f�r Diffusion noch mit grad(theta) multipliziert werden
C***********************************************************************
CUM
      IF(1.EQ.0) THEN
* Neagos: Calculate DPSTGT
      do l=1,nvert
*        call HDIFMA(NEQ,NDIM,NVERT,L,
*     1      DIFMA,DIFMAM,NEIGH,DK,IERR)
      call HDPSTGT(NEQ,NDIM,NVERT,L,CEGRA,
     1      DIFMA,tttl,DPSTGT(:,l),NEIGH,DK,IERR)
*      DPSTGT(:,l)=matmul(matmul(DIFMAM,psithr(:,:,l)),tttl(:,l))
      end do
      DO I=1,NVERT
        CALL HALNO(NEQ,NDIM,NEQ,DPSTGT,NVERT,I,
     1         DPSTGTF(1,1,I),NEIGH,DK,IERR)
      ENDDO
      DO I=1,NVERT
      CALL CENGRA(NEQ,NDIM,NVERT,I,DPSTGTF,DPSTGTC(1,1,I),NEIGH,DK,IERR)
      ENDDO
      ENDIF
C***********************************************************************
C
C     calculate divgrad
C
C***********************************************************************
      IF(IFIRCA.EQ.0) THEN
      DO L=1,NVERT
C***********************************************************************
C     calculate second derivative from tttl
C***********************************************************************
      IF(IDIFVE.EQ.1) THEN
      CALL GSDE1(NDIM,NVERT,L,TTTL,SSSL(1,L),NEIGH,DK,IERR)
C***********************************************************************
C     calculate second derivative from grapsi and tttl
C***********************************************************************
      ELSE IF(IDIFVE.EQ.2) THEN
      CALL GSDER(NEQ,NDIM,NVERT,L,XGRPSI,DGRAXI,NEIGH,DK,IERR)
      SSSL(1:NDIM,L) =
     1 MATMUL(PSITHP(:,:,L),MATMUL(DGRAXI(1:NEQ,1:NDIM),TTTL(1:NDIM,L)))
C--- result is psitheta+ grad(psi)_theta grad theta
C***********************************************************************
C     calculate psitheta+ * XSEPSI
C***********************************************************************
      ELSE IF(IDIFVE.EQ.3) THEN
C-diese zeile nicht kommentieren und in newhe, letzte zeile
c     dann funkt. difffl.
          SSSL(1:NDIM,L) = MATMUL(PSITHP(:,:,L),XSEPSI(1:NEQ,L))
      ELSE IF(IDIFVE.LT.0) THEN
      ELSE
        WRITE(*,*) 'IDIFVE=',IDIFVE,' not yet available'
        STOP
      ENDIF
      ENDDO
      ENDIF
  779 continue
*
C***********************************************************************
C A. Neagos
C Build local orthogonal coordinates with one coordinate vector
C pointing inside the manifold
C***********************************************************************
      if(ndim.gt.1)then
      do l=1,nrbou
      deltapsi(:)=y(:,innernei(l))-y(:,bouvertnr(l))
C***********************************************************************
C A. Neagos
C Project onto tangential space of REDIM
C***********************************************************************
      DELTAPSI(:)=MATMUL(MATMUL(CEGRA(:,:,BOUVERTNR(L)),
     1            PSITHP(:,:,BOUVERTNR(L))),DELTAPSI)
C***********************************************************************
C A. Neagos
C Project onto normal space of boundary
C***********************************************************************
      deltapsi(:)=matmul(projecbou(:,:,l),deltapsi)
      psialpha(:,1,l)=deltapsi(:)
      psialpha(:,2:ndim,l)=boundary_points(l)%psth(:,:,1)
      psth(:,1,bouvertnr(l))=deltapsi(:)
      psth(:,2,bouvertnr(l))=boundary_points(l)%psth(:,1,1)
      CALL GENPSI(NEQ,NDIM,NEQ,SCAMA,psialpha(:,:,l),psialphap(:,:,l))
*      write(*,*)'at',bouvertnr(l)
*      write(*,*)'bound vert nr',l
*      write(*,*)'innernei',innernei(l)
*      write(*,*)'delta',deltapsi(:)
*      write(*,*)'psthbou',boundary_points(l)%psth(:,:,1)
*      read(*,*)
      end do
      end if

C***********************************************************************
C A. Neagos
C Diffusion based on finite differences
C***********************************************************************
      iibou=0
      dy=0.0d0
      DO 1001 L=1,NVERT
C***********************************************************************
C
C     Calculate diffusion term
C
C***********************************************************************
C***********************************************************************
C     no diffusion
C***********************************************************************
      IF(IADDSE.EQ.0) THEN
        DIFF(:) = 0.D0
      ELSE IF(IDIFVE.LT.0) THEN
C***********************************************************************
C
C     Diffusion via consecutive derivative
C
C***********************************************************************
C---- check what happens for boundary points
      CALL ALTHES(NEQ,NDIM,
     1     NVERT,L,NEIGH,DPTHAL,TTTL(1,L),TTTHAL,DIFF)
      DIFF(:) = DIFF(:) /RHO(L)
      ELSE
C***********************************************************************
C
C     Diffusion via Hessian 
C
C***********************************************************************
      INFHES = 0
      IF(IDIFVE.EQ.3.OR.IDIFVE.EQ.2) INFHES=1
C***********************************************************************
C     calculate Hessian 
C***********************************************************************
      CALL NEWHES(INFHES,NEQ,NDIM,SCAMA,NVERT,L,
     1    LGRAPF(1,L),NEIGH,CEGRA(1,1,L),
     1   DPTHAL,PTPHAL,SECLOC(1,1,1,L))
C***********************************************************************
C       store info for output
C***********************************************************************
        IF (NDIM.EQ.2)THEN
          PSTT1(:,L)=SECLOC(:,1,1,L)
          PSTT2(:,L)=SECLOC(:,2,2,L)
        ELSE
          PSTT1(:,L)=SECLOC(:,1,1,L)
          PSTT2=0.0D0
        END IF
C***********************************************************************
C     Diffusion
C***********************************************************************
C---- set gradient in boundary direction to zero
C***********************************************************************
C     Not needed, if gradients are projected onto boundary
C***********************************************************************
*       DO K=1,NDIM
*       DO II=1,2
*       IF(NEIGH(II,K,L).EQ.0) TTTL(K,L) = 0.0D0
*       ENDDO
*       ENDDO
C---- Second derivative
        DIFF = 0
!************* DEBUG ****************
Coriginal:
C***********************************************************************
C    ontribution of psi_tt
C***********************************************************************
        IF((.NOT.CONGRA).AND.(.NOT.LIMGRA)) THEN
          DO I=1,NDIM
          DO J=1,NDIM
            DIFF(:) = DIFF(:) + TTTL(I,L)*SECLOC(:,I,J,L)*TTTL(J,L)
          ENDDO
          ENDDO
        ELSE
          DO I=1,NDIM
          DIFF(:) = DIFF(:) + TTTL(I,L)*SECLOC(:,I,I,L)*TTTL(I,L)
          ENDDO
        ENDIF
        DIFFU(:,L)=DIFF(:)/rho(l)
C***********************************************************************
C     calculate contribution of non-equal diffusivities
C***********************************************************************
      DIFF2(:)=MATMUL(DIFMA(:,:,L),MATMUL(CEGRA(:,:,L),SSSL(1:NDIM,L)))
      DIFF(:) = (DIFF(:) + DIFF2(:))/RHO(L)
C***********************************************************************
C     end of calculation of diffusion 
C***********************************************************************
      ENDIF
C***********************************************************************
C     store diffusion for outputusion 
C***********************************************************************
      DIFFU2(:,L)=DIFF2/RHO(L)
      DIFFU(:,L)=DIFF(:)
C***********************************************************************
C
C     Compute source terms F (called SMFS)
C
C***********************************************************************
      IERST = ZERO
      IF(IADDSE.NE.-1) THEN
         CALL CHKDOM(0,NEQ,Y(1,L),Y(1,L),INEG,VHIT,DUMMY)
         CALL CHKUDO(NEQ,Y(1,L),IRCH)
        IOSD=0
         IF(IRCH.NE.0) THEN
           IOSD=1
         ENDIF
         IF(INEG.NE.0.OR.IOSD.NE.0)THEN
           SMFS = 0.0D0
         ELSE
          CALL MASRES(NEQ,TIME,Y(1,L),SMFS,RPAR,IPAR,IERST)
         ENDIF
        IF(IERST.NE.0) GOTO 930
      ELSE
        SMFS = ZERO
      ENDIF
      SOURCE(1:NEQ,L)=0.0D0
      SOURCE(1:NEQ,L)=SMFS(1:NEQ)
C***********************************************************************
C     Calculate overall rate t
C***********************************************************************
      PHI(:,L)= SMFS(:) + DIFF(:)
C***********************************************************************
C
C     Compute  projection matrix P (called PROJEC)
C
C***********************************************************************
      if (nofdinner)then
          STOP 
      else IF(UPWI) THEN
C---- Compute -PSI_teta*PSI^-1_teta
      CALL UPWIND(NEQ,NDIM,NVERT,L,PSITHR,
     1   LGRAPF,CEGRA(1,1,L),SMFS,UPWGRA,NEIGH,DK,IERR)
      CALL GENPSI(NEQ,NDIM,NEQ,SCAMA,UPWGRA,UPWGRP)
      PROJEC(:,:,L)  =  - MATMUL(UPWGRA,UPWGRP)
      else
      PROJEC(:,:,L)  =  - MATMUL(CEGRA(:,:,L),PSITHP(:,:,L))
cle1
      CALL GRAPRO(L,NEQ,NDIM,GPMA,CEGRA(1,1,L),GRPRO)
      PROJEC(:,:,L)  =  - MATMUL(CEGRA(:,:,L),GRPRO(:,:))
      ENDIF
*C
*C---- Compute (I-PSI_teta*PSI^-1_teta)
      DO I=1,NEQ
        PROJEC(I,I,L)  = ONE + PROJEC(I,I,L)
      ENDDO
      keepelements=0
      if(keepelements.eq.1)then
      CALL PROJEC_SLAVA(NEQ,NDIM,NZEV,SCAMA,ERVCOL,
     1                  CEGRA(1,1,L),PSITHP(1,1,L),PROJEC(:,:,L),IERR)
      end if
C***********************************************************************
C
C     Block for expanding boundary 
C
C***********************************************************************
      if (iret(l).eq.-1 .and. ndim .gt.1)then
      iibou=iibou+1
        if(diffbou)then
          diff(:)=boundary_points(iibou)%diff(:,1)/rho(l)
          diffu(:,l)=diff(:)
         end if
      end if
      IF (EXPANSION) THEN
C***********************************************************************
C     set parameters
C***********************************************************************
      DUMMYDIFF    = 0.0D0
      DUMMYSRC     = 0.0D0
      FLAGDIFF(L)  = 0
      FLAGCHEM(L)  = 0
      DY(:,L)      = 0.0D0
C***********************************************************************
C
C     Handle only boundary points
C
C***********************************************************************
      IF (IRET(L).EQ.-1)THEN
C---  PSTH_IN is a vector represented as a matrix (1,NEQ)
      PSTH_IN(:,1)=Y(:,INNERNEI(IIBOU))-Y(:,BOUVERTNR(IIBOU))
C---  calculate normed distance
      HEL1(1:NEQ) = PSTH_IN(1:NEQ,1) * SCAFA(1:NEQ)
      distance = SQRT(DOT_PRODUCT(HEL1(1:NEQ),HEL1(1:NEQ)))
      CALL GENPSI(NEQ,1,NEQ,SCAMA,PSTH_IN,PSTH_IN_P)
      projec(:,:,l)=matmul(projecbou(:,:,iibou),
     1              matmul(cegra(:,:,l),psithp(:,:,l)))
C***********************************************************************
C A. Neagos:                                                           *
C if F points to outside, lower diffusion influence
C***********************************************************************
      TBPSMF(:) = MATMUL(PROJEC(:,:,L),SMFS(:))
      TBPDIF(:) = MATMUL(PROJEC(:,:,L),DIFF(:))
      PARSMF = DOT_PRODUCT(psth_in_p(1,:),TBPSMF(:))
      PARDIF = DOT_PRODUCT(psth_in_p(1,:),TBPDIF(:))
      dummysrc  =dot_product(psialphap(1,:,iibou),TBPSMF(:))
      dummydiff =dot_product(psialphap(1,:,iibou),TBPDIF(:))
c     dummysrc  =PARSMF 
c     dummydiff =PARDIF 
        if (dummysrc.lt.0.0d0 .and. dummydiff.lt.0.0d0)then
            flagchem(l)=1
            flagdiff(l)=1
            dy(:,l)= (PARSMF + HIGHERDIFF*PARDIF) *psth_in(:,1)
        else if (dummysrc.lt.0.0d0)then
          if (lowerdiff*dummydiff+dummysrc.lt.0.0) then
            flagchem(l)=1
            dy(:,l)= (PARSMF + LOWERDIFF*PARDIF) *psth_in(:,1)
          end if
        else if (dummydiff.lt.0.0d0) then
          if (higherdiff*dummydiff+dummysrc.lt.0.0) then
            flagdiff(l)=1
            dy(1:NEQ,l)= (PARSMF + HIGHERDIFF*PARDIF) *psth_in(1:NEQ,1)
          end if
        end if
      FACBDI= DBLE(NNEWCLD)/DBLE(NOLDCLD) + 2.0
   
      DISGLO = MAXVAL(DISBOU)
      DISORB = MAX(DISBOU(IIBOU),0.1D0*DISGLO)
      IF(DISTANCE.GT.FACBDI*DISORB) DY(1:NEQ,L) = 0.D0
       IF(iibou.eq.27) THEN
      write(*,*) L,iibou,bouvertnr(iibou),
     1       'OOOOOOOOOOOO',DISTANCE,FACBDI,DISORB 
       endif
C***********************************************************************
C   
C***********************************************************************
       end if
************************************************************************
      if(minval(y(3:neq,l)).lt.-1.0d-8)then
      DY(1:NEQ,L)=0.0d0
      end if
C***********************************************************************
C       END of expansion     
C***********************************************************************
      else
C***********************************************************************
C       No expansion, take regular expression 
C***********************************************************************
      DY(1:NEQ,L) = MATMUL(PROJEC(1:NEQ,1:NEQ,L),SMFS(:)+DIFF(:))
      end if
C***********************************************************************
C       END of boundary point
C***********************************************************************
      TEMP1(1:NEQ,1,L) =   SMFS(1:NEQ)
      TEMP2(1:NEQ,1,L) =   DIFF(1:NEQ)
      TEMP1(1:NEQ,1,L) =   MATMUL(PROJEC(1:NEQ,1:NEQ,L),SMFS(1:NEQ))
      TEMP2(1:NEQ,1,L) =   MATMUL(PROJEC(1:NEQ,1:NEQ,L),DIFF(1:NEQ))
C***********************************************************************
C
C     preparation for point implicit procedure
C     Calculation of   dY = dt * (I - dt * P * F_psi)^-1 * P * F
C
C***********************************************************************
      IF(IMPLI.LE.0) THEN
C***********************************************************************
C     Calculate Jacobian F_Y (called XJAC)
C***********************************************************************
      IERBAS = 0
      XJAC(1:NEQ,1:NEQ) = ZERO
      IF(IADDSE.GE.0.AND.INEG.EQ.0.AND.IOSD.EQ.0) THEN
        VRS(1:NEQ) = SMFS(1:NEQ)
        CALL MASJAC(NEQ,TIME,Y(1,L),VRS,XJAC,RPAR,IPAR,IERBAS)
        IF(IERBAS.GT.0) GOTO 920
      ENDIF
C***********************************************************************
C     Get (I - dt * P * F_psi)^-1
C***********************************************************************
C--- koennte effizienter gemacht werden
      IF(IADDSE.GE.0) THEN
        APPJAC(:,:,L) = MATMUL(PROJEC(:,:,L),XJAC)
        WA1 = - DELTAT * APPJAC(:,:,L)
        DO I=1,NEQ
          WA1(I,I) = ONE + WA1(I,I)
        ENDDO
        CALL DGEINV(NEQ,WA1,RW,IW,IERINV)
        IF(IERINV.NE.0) GOTO 900
      ENDIF
      ENDIF
C***********************************************************************
C     Make implicit with respect to chemistry
C***********************************************************************
      IF(IMPLI.EQ.0.AND.IADDSE.GE.0)  DY(:,L) =  MATMUL(WA1,DY(:,L))
C***********************************************************************
C
C     Do final checks
CTODO
C     this is not very general
C
C***********************************************************************
      if (theta(1,l).eq.maxval(theta(1,1:nvert))) then
      dy(:,l)=0.0d0
      end if
      if (theta(1,l).eq.minval(theta(1,1:nvert))) then
      dy(:,l)=0.0d0
      end if
C***********************************************************************
C     End of loop over vertices
C***********************************************************************
C***********************************************************************
C     Do not expand corners    
CTODO
C***********************************************************************
      if (expansion)then
      if (theta(ndim,l).ne.maxval(theta(ndim,1:nvert)) .and.
     1 theta(ndim,l).ne.minval(theta(ndim,1:nvert)) ) dy(:,l)=0.0d0
      end if
C***********************************************************************
C A. Neagos:                                                           *
C Calculation of invariance defect
C***********************************************************************
      NSRCDIFF(l)=SQRT(DOT_PRODUCT(DIFF(1:NEQ)+smfs(1:NEQ),
     1 MATMUL(SCAMA(1:NEQ,1:NEQ),DIFF(1:NEQ)+smfs(1:NEQ))))
      NDY(l)=SQRT(DOT_PRODUCT(DY(:,l),
     1 MATMUL(SCAMA(1:NEQ,1:NEQ),DY(:,l))))
C***********************************************************************
C     End of loop over vertices
C***********************************************************************
 1001 CONTINUE
      manierr=sum(ndy)/sum(NSRCDIFF)
      IFIRCA = 1
C-Ulri
      DO L=1,NVERT
        IF(.NOT.FREEB) THEN
           IF(IRET(L).ne.0) DY(:,L) = ZERO
         ENDIF
      ENDDO
      PHI(:,:)=DY(:,:)
      RETURN
C***********************************************************************
C     Error exits
C***********************************************************************
 900  CONTINUE
      IERR = IERINV
      WRITE(NFIOUT,905) IERR
 905  FORMAT(' ','  ERROR IN INVERSION OF MATRIX: IERR =',I3,//)
      GOTO 999
 920  CONTINUE
      IERR = IERBAS
      WRITE(NFIOUT,925) IERR
 925  FORMAT(' ',' ERROR IN COMPUTATION OF JACOBIAN: IERR =',I3,//)
      GOTO 999
 930  CONTINUE
      IERR = IERST
      WRITE(NFIOUT,935) IERR
 935  FORMAT(' ',' ERROR IN COMPUTATION OF SOURCE TERMS:
     1     IERR =',I3,//)
      GOTO 999
 999  WRITE(NFIOUT,998) L
 998  FORMAT(' ','  Error in -INERTM- at point:',I4,/)
      IRDES = 1
      RETURN
C***********************************************************************
C     End of -INERTM-
C***********************************************************************
      END
      SUBROUTINE XXXIN(INFO,NFISTO,NEQ,NSPEC,NVERM,NCELM,
     1   NPROP,PROP,SYMB,SYMBT,
     1   NVERT,NCELL,ICORD,IRET,IY,IVERT,NDIM,IEQ,ITRM)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *             PROGRAM FOR THE INPUT OF PROFILES             *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
CBZ==================================================================CBZ
CBZ   subroutine was changed for use with more than 2 dimensions     CBZ
CBZ   also change line ~270ff in evbres.f                            CBZ
CBZ   2014/02/05                                                     CBZ
CBZ==================================================================CBZ
C***********************************************************************
C     I.: TYPES
C***********************************************************************
      USE GRAFI
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      CHARACTER(LEN=*)  SYMBT(NEQ),SYMB(NEQ)
      CHARACTER(LEN=4) NNNN
!      CHARACTER(LEN=76) MMMM
      character(LEN=20)                 :: frmt !added BZ
      DIMENSION PROP(NPROP,*)
      LOGICAL EQUSYM,INIVAL
      integer, dimension(ndim)          :: ncelli,ndum !added BZ
      integer, dimension(ndim)          :: isub,iwrk,twos !added BZ
      INTEGER, DIMENSION(NDIM,NCELM)    :: IY
      INTEGER, DIMENSION(NDIM,NVERM)    :: ICORD
      INTEGER, DIMENSION(2**NDIM,NCELM) :: IVERT
      INTEGER, DIMENSION(NVERM)         :: IRET
      DOUBLE PRECISION, DIMENSION(NSPEC,NSPEC) :: DSS
      DOUBLE PRECISION, DIMENSION(NSPEC)       :: DES,DSE,DSP
      COMMON/BLOCOR/IIRAT,IISMF,IISMFP,IIRV,IIPRO,IIPNU,IILAM,NPROOT
CBZ      COMMON/BINDL/INDLO1,INDLO2
      integer, dimension(neq)           :: indlo !added BZ
      DATA MSGFIL/6/
C***********************************************************************
C     default values
      if(.not.allocated(nverti)) allocate (nverti(ndim))
C***********************************************************************
      nverti=1
      iret=0
      twos=2
C***********************************************************************
C     flag for saving initial values
C***********************************************************************
      INIVAL = .FALSE.
      IF(INFO.EQ.0) INIVAL = .TRUE.
CBZ old
!C***********************************************************************
!C     input of header
!C***********************************************************************
!      IF(.NOT.ALLOCATED(GRASVE)) ALLOCATE (GRASVE(NEQ))
!      NSPEC=NEQ-2
!      IF(NDIM.GT.2) THEN
!        WRITE(*,*) ' NDIM .GT. 2 in XXXIN'
!        STOP
!      ENDIF
!      REWIND NFISTO
!      READ(NFISTO,101,END=910) INDLO1,INDLO2
!      IF(INDLO1.EQ.0.AND.INDLO2.EQ.0) THEN
!        READ(NFISTO,102) GRASVE(1:NEQ),(XDUM,I=1,NEQ)
!      ELSE
!        GRASVE = 0
!        GRASVE(INDLO1) = 1.0
!        GRASVE(INDLO2) = 1.0
!      ENDIF
! 101  FORMAT(1X,I4,I5)
! 102  FORMAT(1(3X,7(1PE20.13,1X)))
CBZ old end
C***********************************************************************
C
C     input of symbols and check for consitency
C
C***********************************************************************
      REWIND NFISTO
   1  READ(NFISTO,103,END=910) NNNN
      IF(NNNN.NE.'VARI') THEN
        GOTO 1
      ENDIF
C     READ(NFISTO,104) SYMBT(1:NEQ)
      READ(NFISTO,*) SYMBT(1:NEQ)
      DO I=1,NEQ
CTODO
        IF(.NOT.EQUSYM(8,ADJUSTL(SYMBT(I)),ADJUSTL(SYMB(I)))) THEN
          goto 911
        ENDIF
      ENDDO
 103  FORMAT(A4)
C104  FORMAT(7(1X,A,1X,:1X))
 104  FORMAT(7(A,:1X))
CBZ*********************************************************************
CBZ   input number of vertices
CBZ*********************************************************************
CBZ new
      rewind nfisto
   2  read(nfisto,105,end=200) NNNN
      if(NNNN.ne.'NDIM') then
        goto 2
      endif
      ! read NDIM block (new)
      backspace nfisto
      read(nfisto,106,end=910) ndimtab
      write(frmt,107) ndimtab
      read(nfisto,frmt) nverti(1:min(ndimtab,ndim))
      goto 300
      ! read ZONE block (compatibility with old files)
 200  continue
      REWIND NFISTO
   3  READ(NFISTO,103,END=910) NNNN
      IF(NNNN.NE.'ZONE') THEN
        GOTO 3
      ENDIF
      BACKSPACE NFISTO
      READ(NFISTO,108,END=910) nverti(1:min(3,ndim))
      goto 300
 105  format(1x,a4)
 106  format(5x,i4)
 107  format('(1x,',i0,'(i4,1x))')
 108  FORMAT(7X,I4,4X,I4,4X,I4)
      ! check array size
 300  continue
      nverttab=product(nverti) ! total number of vertices
      write(*,*)'nverttab',nverttab
      IF(nverttab.GT.NVERM) THEN
        goto 912
      ENDIF
CBZ new end
CBZ old
!  11  READ(NFISTO,90,END=910) NNNN,MMMM
!   90 FORMAT(A4,A76)
!      IF(NNNN.NE.'ZONE') THEN
!        GOTO 11
!      ENDIF
!      BACKSPACE NFISTO
!      READ(NFISTO,842,END=910) NL,NK
!  842 FORMAT(7X,I4,4X,I4)
!   91 FORMAT(A4,3X,I4)
!      IF(NL*NK.GT.NVERM) THEN
!        WRITE(*,*) ' too many vertices',NL,NK,NL*NK,NVERM
!        STOP
!      ENDIF
CBZ old end
C***********************************************************************
C     set arrays
C***********************************************************************
      CALL SETGFI(NEQ,nverttab)
!      CALL SETGFI(NEQ)
CBZ*********************************************************************
CBZ   input of gradient search vectors
CBZ*********************************************************************
CBZ new
      REWIND NFISTO
  33  read(nfisto,105,end=332) NNNN
      if(NNNN.ne.'GRSV') then
        goto 33
      endif
      ! read GRSV block (new)
      backspace nfisto
      read(nfisto,106,end=910) ngrsv
 331  continue
      write(frmt,107) ngrsv
      read(nfisto,frmt) indlo(1:ngrsv)
      goto 333
      ! if GRSV not found (compatibility with old files)
 332  continue
      rewind nfisto
      ngrsv=2
      goto 331
      ! read/create gradient search vector
 333  continue
      IF(all(indlo(1:ngrsv).EQ.0)) THEN
        READ(NFISTO,102) GRASVE(1:NEQ),(XDUM,I=1,NEQ)
      ELSE
        GRASVE = 0
        do i=1,ngrsv
          GRASVE(indlo(i)) = 1.0
        enddo
      ENDIF
 102  FORMAT(1(3X,7(1PE20.13,1X)))
CBZ new end
CBZ*********************************************************************
CBZ   input of data zones
CBZ*********************************************************************
CBZ new
      ! search first ZONE
      rewind nfisto
   4  read(nfisto,103,end=910) NNNN
      if(NNNN.ne.'ZONE') then
        goto 4
      endif
      backspace nfisto
      ! read data zones
      DO L=1,nverttab
        ! check for ZONE header and skip it
        if(mod((l-1)+nverti(1),nverti(1)).eq.0) then
          read(nfisto,103,end=910) NNNN
          backspace nfisto
          if(NNNN.eq.'ZONE') then
            ! check number of grid points in ZONE header
            read(nfisto,108,end=910) ndum(1:min(2,ndim))
            if(any(ndum(1:min(2,ndim)).ne.nverti(1:min(2,ndim)))) then
              goto 913
            endif
          endif
        endif
        ! read ZONE
        READ(NFISTO,102,END=910) PSIFI(1:NEQ,l),PGRAFI(1:NEQ,l)
     1                                         ,PSECFI(1:NEQ,l)
        ! transport matrices
c       call TRAPRO(ITRM,ieq,neq,PSIFI(1:NEQ,l),nspec,
c    1    DSS,DSE,DSP,DES,DEE,DEP,XLA,VIS,DDDG,ierr)
CBUG  dddg is not calculated
        dddfi(l)=dddg
      ENDDO
CBZ new end
CBZ old
!C***********************************************************************
!C     vertex properties
!C***********************************************************************
!  844 FORMAT(1(3X,7(1PE20.13,1X)))
!      DO L=1,NL*NK
!      IK = (L-1)/NL + 1
!      IL = L - (IK-1) * NL
!C2n     READ(NFISTO,844,END=910) PROP(IISMF:IISMF-1+NEQ,L)
!CAA     READ(NFISTO,844,END=910) PROP(IISMF:IISMF-1+2*NEQ,L)
!CEQ     READ(NFISTO,844,END=910) PSIFI(1:NEQ,IL,IK),PGRAFI(1:NEQ,IL,IK)
!        READ(NFISTO,844,END=910) PSIFI(1:NEQ,IL,IK),PGRAFI(1:NEQ,IL,IK)
!     1                                           ,PSECFI(1:NEQ,IL,IK)
!c       PGRAFI(1:NEQ,IL,IK) = 0.1 * PGRAFI(1:NEQ,IL,IK)
!c       write(*,*) IL,IK,PSIFI(1:NEQ,IL,IK),'/',PGRAFI(1:NEQ,IL,IK),'/',
!c    1     PSECFI(1:NEQ,IL,IK)
!C***********************************************************************
!C     Trasnsport matrices
!C***********************************************************************
!C-   call for Le=1
!      call TRAPRO(ITRM,ieq,neq,PSIFI(1:NEQ,IL,IK),nspec,
!     1    DSS,DSE,DSP,DES,DEE,DEP,XLA,VIS,DDDG,ierr)
!      IF(INIVAL) THEN
!           PROP(IISMF:IISMF-1+NEQ,L)= PSIFI(1:NEQ,IL,IK)
!           PROP(IISMF+NEQ:IISMF-1+2*NEQ,L) = PGRAFI(1:NEQ,IL,IK)
!           PROP(IISMF+2*NEQ,L) = DDDG
!      ENDIF
!      DDDFI(IL,IK) = DDDG
!      ENDDO
CBZ old end
CBZ*********************************************************************
CBZ   save initial values and calculate general coordinates
CBZ*********************************************************************
CBZ new
      IF(INIVAL) THEN
        ! number of cells
        nvert=nverttab
        do l=1,ndim
          ncelli(l)=max(1,nverti(l)-1)
        enddo
        ncell=product(ncelli)
           WRITE(*,*) 'NCELL=',NCELL,'tttttttttt'
        IF(NCELL.GT.NCELM) THEN
           WRITE(*,*) 'NCELL=',NCELL,' > NCELM=',NCELM
           STOP
        ENDIF
        ! loop over all vertices
        do l=1,nvert
          ! save initial values
          PROP(IISMF:IISMF-1+NEQ,L) = PSIFI(1:NEQ,l)
          PROP(IISMF+NEQ:IISMF-1+2*NEQ,L) = PGRAFI(1:NEQ,l)
          PROP(IISMF+2*NEQ,L) = dddfi(l)
          ! coordinates of vertex
          call ind2sub(nverti,l,isub,ndim,iwrk)
          icord(:,l)=isub
          ! boundary flag
          if(any(isub.eq.1).or.any(isub.eq.nverti)) then
            iret(l)=-1
          endif
        enddo
        ! loop over all cells
        do l=1,ncell
          ! coordinates of cell
          call ind2sub(ncelli,l,isub,ndim,iwrk)
          iy(:,l)=isub
          do k=1,2**ndim
            ! coordinates of surrounding vertices
            call ind2sub(twos,k,isub,ndim,iwrk)
            isub=isub+iy(:,l)-1
            call sub2ind(nverti,idx,isub,ndim,iwrk)
            ivert(k,l)=idx
          enddo
        enddo
      ENDIF
c      do l=1,ncell
c      write(*,*) l,'iiiiiivert',ivert(1:2**ndim,l)
c      ENDDO
CBZ new end
CBZ old
!C***********************************************************************
!C     IY
!C***********************************************************************
!      IF(INIVAL) THEN
!      NVERT = NL*NK
!      NCELL  = (NL-1)*(NK-1)
!      IF(NK.EQ.1) NCELL = NL-1
!      IF(NK.GT.1) THEN
!      LAUF = 0
!      DO K=1,NK-1
!      DO L=1,NL-1
!        LAUF = LAUF + 1
!        IY(1,LAUF) = L
!        IY(2,LAUF) = K
!C       IVERT(1,LAUF) = LAUF
!C       IVERT(2,LAUF) = LAUF + 1
!C       IVERT(3,LAUF) = LAUF + NL
!C       IVERT(4,LAUF) = LAUF + NL + 1
!        IVERT(1,LAUF) = L +     NL*(K-1)
!        IVERT(2,LAUF) = L + 1 + NL*(K-1)
!        IF(NDIM.GT.1) THEN
!        IVERT(3,LAUF) = L     + NL*(K  )
!        IVERT(4,LAUF) = L + 1 + NL*(K  )
!        ENDIF
!      ENDDO
!      ENDDO
!      ELSE
!      LAUF = 0
!      DO L=1,NL-1
!        LAUF = LAUF + 1
!        IY(1,LAUF) = L
!        IVERT(1,LAUF) = L
!        IVERT(2,LAUF) = L + 1
!      ENDDO
!      ENDIF
!C***********************************************************************
!C     IRET,ICORD
!C***********************************************************************
!      LAUF = 0
!      IRET(1:NL*NK) = 0
!      IF(NK.GT.1) THEN
!      DO K=1,NK
!      DO L=1,NL
!        LAUF = LAUF + 1
!        ICORD(1,LAUF) = L
!        ICORD(2,LAUF) = K
!!         IF(K.EQ.1.OR.K.EQ.NK.OR.L.EQ.1.OR.L.EQ.NL) IRET(LAUF) = -1
!C-Ul DIFFusion
!c       IF(          K.EQ.NK                     ) IRET(LAUF) = -1
!c       IF(K.EQ.1           .OR.L.EQ.1.OR.L.EQ.NL) IRET(LAUF) =  1
!      ENDDO
!      ENDDO
!      ELSE
!      DO L=1,NL
!        LAUF = LAUF + 1
!        ICORD(1,LAUF) = L
!        IF(L.EQ.1.OR.L.EQ.NL) IRET(LAUF) = -1
!      ENDDO
!      ENDIF
!      ENDIF
CBZ old end
C***********************************************************************
C     normal exit
C***********************************************************************
      RETURN
C***********************************************************************
C     error exits
C***********************************************************************
 910  continue
      write(msgfil,210)
 210  format(' END OF FILE DETECTED DURING INPUT OF FORT.77')
      stop
 911  continue
      write(msgfil,211) symbt(i),symb(i)
 211  format(' SYMBOLS NOT MATCHING IN FORT.77: ',a,1x,a)
      stop
 912  continue
      write(msgfil,212) product(nverti(1:ndim)),nverm
 212  format(' TOO MANY VERTICES IN FORT.77: ',i0,' > ',i0)
      stop
 913  continue
      write(msgfil,213) ndum(1:2),nverti(1:2)
 213  format(' NUMBER OF GRID POINTS MISMATCH IN FORT.77: ',
     1        i0,'x',i0,' .ne. ',i0,'x',i0)
      stop
C***********************************************************************
C     End of -XXXIN-
C***********************************************************************
      END
      SUBROUTINE EVGR2(NDIM,NEQ,NVERT,Y,YGR,XGRPSI,XSECSI,DCOV)
C***********************************************************************
C
C     Compute Inertial Manifold (modified Fraser algorithm)
C
C
C***********************************************************************
CBZ==================================================================CBZ
CBZ   subroutine was changed for use with more than 2 dimensions     CBZ
CBZ   also change line ~270ff in evbres.f                            CBZ
CBZ   2014/03/24                                                     CBZ
CBZ==================================================================CBZ
      USE GRAFI
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      PARAMETER (ONE = 1.0D0, ZERO = 0.0D0,SMALL=1.D-30)
      LOGICAL TEST
CBZ      COMMON/BINDL/INDLO1,INDLO2
C***********************************************************************
C     Arrays
C***********************************************************************
      DOUBLE PRECISION, DIMENSION(NEQ,NVERT)    :: Y,YGR,XGRPSI,XSECSI
      DOUBLE PRECISION, DIMENSION(NVERT)        :: DCOV
      integer, dimension(ndim)          :: ivert,isub,iwrk,threes !added BZ
      logical                           :: kriging !added BZ
C***********************************************************************
C     default values
C***********************************************************************
      kriging=.false.
      threes=3
        TEST =.False.
C***********************************************************************
C     Loop over vertices
C***********************************************************************
      DO I=1,NVERT
        DIST = 1.D30
        IL = 1
C        search next neighbour
        DO L=1,nverttab
          XXX = DOT_PRODUCT(GRASVE(:)*(PSIFI(:,L)-Y(:,I)),
     1                      GRASVE(:)*(PSIFI(:,L)-Y(:,I)))
          IF(XXX.LT.DIST) THEN
            IL = L
            DIST = XXX
          ENDIF
        ENDDO
c       write(*,*) I,' taken from vertex ' , IL
        DCOV(I) = DDDFI(IL)
        !---------------------------------------------------------------
        ! no Kriging
        !---------------------------------------------------------------
        IF(.not. kriging) THEN
          YGR(1:NEQ,I) = Y(:,I)-PSIFI(1:NEQ,IL)
          XGRPSI(1:NEQ,I) = PGRAFI(1:NEQ,IL)
          XSECSI(1:NEQ,I) = PSECFI(1:NEQ,IL)
c       write(*,*) XGRPSI(1:NEQ,I) ,'OOOOOOOOOOOOO'
        !---------------------------------------------------------------
        ! Kriging
        !---------------------------------------------------------------
        ELSE
          SUMWEI = 0.0D0
          YGR   (1:NEQ,I) = 0.0D0
          XGRPSI(1:NEQ,I) = 0.0D0
          XSECSI(1:NEQ,I) = 0.0D0
          ! coordinates of vertex
          call ind2sub(nverti,IL,ivert,ndim,iwrk)
          do k=1,3**ndim
CBZ          DO II=-1,1
CBZ            DO JJ=-1,1
            ! coordinates of surrounding vertices
            call ind2sub(threes,k,isub,ndim,iwrk)
            isub=isub+ivert-2
            call sub2ind(nverti,ILT,isub,ndim,iwrk)
CBZ            ILT = IL+II
CBZ            IKT = IK+JJ
            if(all(isub.ge.1).and.all(isub.le.nverti)) then
CBZ            IF(ILT.GE.1.AND.ILT.LE.NL.AND.
CBZ     1         IKT.GE.1.AND.IKT.LE.NK) THEN
              XXX = DOT_PRODUCT(GRASVE(:)*(PSIFI(:,ILT)-Y(:,I)),
     1                          GRASVE(:)*(PSIFI(:,ILT)-Y(:,I)))
              XXX = MAX(XXX,1.D-20)
              WEIT = 1.0D0 / XXX
              SUMWEI = SUMWEI + WEIT
              YGR(1:NEQ,I) = YGR(1:NEQ,I) +
     1                       WEIT *(Y(:,I)-PSIFI(1:NEQ,ILT))
              XGRPSI(1:NEQ,I) = XGRPSI(1:NEQ,I) +
     1                          WEIT *PGRAFI(1:NEQ,ILT)
              XSECSI(1:NEQ,I) = XSECSI(1:NEQ,I) +
     1                          WEIT *PSECFI(1:NEQ,ILT)
            endif
CBZ            ENDIF
          enddo
CBZ            ENDDO
CBZ          ENDDO
          YGR(1:NEQ,I) = YGR(1:NEQ,I) / SUMWEI
          XGRPSI(1:NEQ,I) = XGRPSI(1:NEQ,I) / SUMWEI
          XSECSI(1:NEQ,I) = XSECSI(1:NEQ,I) / SUMWEI
        ENDIF
      ENDDO
C***********************************************************************
C     End of -INERTM-
C***********************************************************************
      END
      SUBROUTINE EVGR3(NDIM,NEQ,NVERT,NEQS,
     1         scama,Y,YGR,XGRPSI,XSECSI,DCOV)
      USE GRAFI
C***********************************************************************
C
C     Written by A. Neagos
C     Get gradients from gradient manifold
C
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      PARAMETER (ONE = 1.0D0, ZERO = 0.0D0,SMALL=1.D-30)
      LOGICAL TEST
      COMMON/BINDL/INDLO1,INDLO2
C***********************************************************************
C     Arrays
C***********************************************************************
C---- Input
      DOUBLE PRECISION, DIMENSION(NEQ,NVERT)     :: Y,YGR,XGRPSI,XSECSI
      DOUBLE PRECISION, DIMENSION(NVERT)        :: DCOV
      DOUBLE PRECISION, DIMENSION(NEQ)     :: DDY
      DOUBLE PRECISION, DIMENSION(NEQS,neq)     :: scama
      DOUBLE PRECISION, allocatable,DIMENSION(:,:)     :: psth_grad
      DOUBLE PRECISION, allocatable,DIMENSION(:,:)     :: d_grad
      DOUBLE PRECISION, allocatable,DIMENSION(:,:)     :: psth_grad_p
      DOUBLE PRECISION, allocatable,DIMENSION(:)     :: dtheta
      integer, dimension(ndim)         :: ivert,isub,isubdum,iwrk,threes
      integer, dimension(ndim)         :: isubf,isubb
C***********************************************************************
C     Loop over vertices
C***********************************************************************
CRASH
      if (size(nverti).eq.3)then
        ndim_gr=3
        if (nverti(3).eq.1) ndim_gr=2
      end if
      if (size(nverti).eq.2)then
        ndim_gr=2
      if (nverti(2).eq.1) ndim_gr=1
      end if
      if (size(nverti).eq.1)then
                          ndim_gr=1
      end if
      if (.not.allocated(psth_grad))allocate(psth_grad(neq,ndim_gr))
      if (.not.allocated(d_grad))allocate(d_grad(neq,ndim_gr))
      if (.not.allocated(psth_grad_p))allocate(psth_grad_p(ndim_gr,neq))
      if (.not.allocated(dtheta))allocate(dtheta(ndim_gr))
*C
*      IF(ndim_gr.eq.2) THEN
*C-Ulrich
        do i=1,nvert
        DIST = 1.D30
        DO L=1,nverttab
          DDY  = Y(:,I)-PSIFI(:,L)
          YYY  =  sqrt(DOT_PRODUCT(DDY(3:neq),DDY(3:neq)))
*          write(*,*)'dist before',dist
            IF(YYY.lt.dist) THEN
              IL = L
              dist=yyy
            ENDIF
        ENDDO
*        write(*,*)'nearest grad p',il,'at redim p',i
*        read(*,*)
        YGR(1:NEQ,I) =    Y(:,I)-PSIFI(1:NEQ,IL)
        call ind2sub(nverti,il,isub,ndim,iwrk)
        isubf=isub
        isubb=isub
        do ii=1,ndim_gr
            if(isub(ii).gt.1)then
                isubb(ii)=isub(ii)-1
            end if
            if(isub(ii).lt.nverti(ii))then
                isubf(ii)=isub(ii)+1
            end if
        call sub2ind(nverti,lf,isubf,ndim,iwrk)
        call sub2ind(nverti,lb,isubb,ndim,iwrk)
        psth_grad(:,ii)=psifi(:,lf)-psifi(:,lb)
        d_grad(:,ii)=PGRAFI(1:NEQ,lf)-PGRAFI(1:NEQ,lb)
        end do
C
        call genpsi(neq,ndim_gr,NEQS,scama,psth_grad,psth_grad_p)
        dtheta=matmul(psth_grad_p,ygr(:,i))
*        write(*,*)'dtheta',dtheta
*        read(*,*)
        XGRPSI(:,i)=PGRAFI(1:NEQ,il)+matmul(d_grad,dtheta)
*        write(*,*)'XGRPSI(:,i)',XGRPSI(:,i)
        end do
      RETURN
C***********************************************************************
C     End of -INERTM-
C***********************************************************************
      END
      subroutine GETDIC(ITRM,ieq,neq,SMF,nspec,rho,
     1          DMATS,DMATE,XLA,VIS,DDDG,RPAR,IPAR,ierr)
c************************************************************************
c                                                                       *
c     Subroutine for preparation of ILDM output                         *
c                                                                       *
c     input:
c                    ndim,Y,F,RV,Zs,(Zs)**-1 for each ILDM point        *
c            nspec = number of species                                  *
c            neq   = number of equations (nspec+2)                      *
c            DIST  = distance between ILDM Points in each direction     *
c            SYMPRP= symbols for the head of the data file              *
c                                                                       *
c    output: PROPN = desired property field  contains                   *
c                                                                       *
c                    for fix  coordinates(ipara=1):                     *
c                    ndim,Y,CF,RV,CZs(Zs)**-1,DYeta,Dheta,flag          *
c                                                                       *
c                    for gen. coordinates(ipara=2):                     *
c                    ndim,Y,Y+F,RV,Y+Zs(Zs)**-1,DYtheta,Dheta,flag      *
c                                                                       *
c************************************************************************
      implicit double precision(a-h,o-z)
      PARAMETER(ZERO=0.D0)
      include 'homdim.f'
c************************************************************************
c     Variables
c************************************************************************
      logical test
      common/BSSPV/SSPV(20*mnspe)
      dimension RPAR(*),IPAR(*)
c************************************************************************
c     Arrays
c************************************************************************
      dimension CI(NSPEC)
      DOUBLE PRECISION, DIMENSION(NSPEC)          :: DSE,DSP,DES
      DOUBLE PRECISION, DIMENSION(NSPEC,NSPEC)    :: DSS
      DOUBLE PRECISION, DIMENSION(NSPEC,NEQ)      :: DMATS,DXDS
      DOUBLE PRECISION, DIMENSION(NEQ)            :: DMATE,DTDS,DPDS,
     1                                               SMF
c************************************************************************
c     Initialize
c************************************************************************
      test  = .false.
c************************************************************************
c     Calculate transport properties
c************************************************************************
      call TRAPRO(ITRM,ieq,neq,SMF,nspec,DSS,DSE,DSP,DES,DEE,DEP,
     1     XLA,VIS,DDDG,ierr)
      if(ierr.gt.0) THEN
        WRITE(*,*)  ' error calling TRAPRO'
        RETURN
      ENDIF
c************************************************************************
c     Calculate transformation matrices from h,p,phi to t,p,x
c************************************************************************
      infder = 1
!       write(*,*)'IEQ',ieq
!       read(*,*)
      call TRASSP(ieq,neq,SMF,nspec,t,c,ci,rho,h,p,SSPV,ierr,
     1                  infder,DXDS,NSPEC,DTDS,DPDS)
      if (ierr .ne. 0) goto 930
      if(ieq.ne.1) GOTO 910
C************************************************************************
C     Calculate diffusion flux formulation
C     j   = DMATS * grad(h,p,phi)
C     j_q = DMATE * grad(h,p,phi)
C************************************************************************
C************************************************************************
C A. Neagos:                                                            *
C DXDS steht hier f�r die Ableitung der Massenbr�che nach den spezifi-  *
C schen Molzahlen: dw/dPsi. Anwendung, wenn FUSO=ITRM=4                 *
C Vgl. �nderungen in physchem.f/DIFMA!                                  *
C************************************************************************
      IF (ITRM .EQ. 4) THEN
      DXDS(:,1:NEQ) = ZERO
      DO I=1,NSPEC
      DXDS(I,I+2)=XMOL(I)
      END DO
      END IF
C************************************************************************
      CALL TRADIF(neq,nspec,SMF,
     1     DSS,DSE,DSP,DES,DEE,DEP,DXDS,DTDS,DMATS,DMATE,ierr)
      IF(IERR.NE.0) GOTO 920
C************************************************************************
C A. Neagos
C Die Diffusionsmatrix wird hier durch die molaren Massen geteilt, damit
C aus der Massenstromdichte die Stromdichte der spezifischen Molzahlen
C entsteht
C************************************************************************
       do i=1,NSPEC
       DMATS(I,:) = - DMATS(I,:)*XMINV(i)
      ENDDO
       DMATE  = - DMATE
      return
C************************************************************************
C     Error exits
C************************************************************************
  900 continue
      write(*,901)
  901 format(' Error in matrix inversion ')
      goto 999
  910 continue
         write(*,*) 'Check GETDIC'
      goto 999
  920 continue
         write(*,*) Ierr
         write(*,*) '-TRADIF- does not yet include pressure influence'
      goto 999
  930 continue
         write(*,*)'ERROR IN TRASSP, CALCULATION OF ENTHALPIES'
         RETURN
  999 continue
      write(*,998)
  998 format(3(' ****   Error in - GETSYS -  ****',/))
      stop
c************************************************************************
c     End of - PREPRO -
c************************************************************************
      end
      SUBROUTINE IVALEF(NZC,B,IR,IC)
C***********************************************************************
C
C     DEFINITION OF LEFT HAND SIDE (SEE -ULIMEX-)
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION B(*),IR(*),IC(*)
      PARAMETER(ONE=1.0D0)
C***********************************************************************
C
C***********************************************************************
      DO I=1,NZC
        IR(I)=I
        IC(I)=I
      ENDDO
      B(1:NZC) = ONE
      RETURN
C***********************************************************************
C     END OF FASLEF
C***********************************************************************
      END
      SUBROUTINE IVARIG (NEQ,NZV,TIME,Y,F,BV,IR,IC,RPAR,IPAR,IFC,IFP)
C***********************************************************************
C
C     DEFINITION OF RIGHT HAND SIDE (SEE -ULIMEX-)
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION Y(NEQ),F(NEQ),BV(*),IR(*),IC(*)
      DIMENSION RPAR(*),IPAR(*),IFP(*)
C***********************************************************************
C     CALL OF PDESYS
C***********************************************************************
      CALL RESINV(NEQ,TIME,Y,F,RPAR,IPAR,IFC)
      RETURN
C***********************************************************************
C     END OF FCN
C***********************************************************************
      END
      SUBROUTINE NEWHES(INFHES,NEQ,NDIM,SCAMA,
     1     NVERT,IVC,LGRAPF,NEIGH,PSITH,DPTHAL,PTPHAL,SECPSI)
C***********************************************************************
C                                                                      *
C                                                                      *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      PARAMETER(ONE=1.0D0,TWO=2.0D0,ZERO=0.0D0)
      DIMENSION NEIGH(2,NDIM,NVERT)
      LOGICAL         , DIMENSION(NDIM)                :: LGRAPF
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)             :: SCAMA
      DOUBLE PRECISION, DIMENSION(NEQ,NDIM)            :: PSITH
      DOUBLE PRECISION, DIMENSION(NEQ,NDIM)            :: PSITHB,PSITHF
      DOUBLE PRECISION, DIMENSION(NDIM,NEQ)            :: PSITPB,PSITPF
      DOUBLE PRECISION, DIMENSION(NEQ,NDIM,NDIM)       :: SECPSI
      DOUBLE PRECISION, DIMENSION(NEQ,NDIM,NDIM,NVERT) :: DPTHAL
      DOUBLE PRECISION, DIMENSION(NDIM,NEQ,NDIM,NVERT) :: PTPHAL
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)             :: TEMPF,TEMPB
      LOGICAL TEST
C***********************************************************************
C
C***********************************************************************
      TEST = .FALSE.
C***********************************************************************
C
C***********************************************************************
      DO I=1,NDIM
C-Nea
       IF(.NOT.LGRAPF(I)) THEN
       SECPSI(:,:,I) = 0.0D0
       ELSE
C-Nea
C--- backwards
      IRIG = NEIGH(2,I,IVC)
      PSITHF(:,:) = DPTHAL(:,:,I,IVC)
      PSITPF(:,:) = PTPHAL(:,:,I,IVC)
C      PSITHF(:,:) = DPTHAL(:,:,I,IRIG)
C      PSITPF(:,:) = PTPHAL(:,:,I,IRIG)
!      ILEF = NEIGH(1,I,IVC)
C-Nea
      ILEF = NEIGH(1,I,IVC)
C-Nea
      IF(ILEF.NE.0) THEN
        PSITHB(:,:) = DPTHAL(:,:,I,ILEF)
        PSITPB(:,:) = PTPHAL(:,:,I,ILEF)
      ELSE
C-Nea
C        PSITHF(:,:) = DPTHAL(:,:,I,IRIG)
C-Nea
        PSITHB(:,:) = DPTHAL(:,:,I,IVC)
        PSITPB(:,:) = PTPHAL(:,:,I,IVC)
      ENDIF
C***********************************************************************
C     get derivatives
C***********************************************************************
      IF(INFHES.EQ.1) THEN
        TEMPB = MATMUL(PSITHB,PSITPB)
        TEMPF = MATMUL(PSITHF,PSITPF)
        SECPSI(:,:,I) = MATMUL((TEMPF-TEMPB),PSITH(:,:))
      ELSE
        SECPSI(:,:,I) = (PSITHF-PSITHB)
      ENDIF
      ENDIF
      ENDDO
      RETURN
C***********************************************************************
C     END OF
C***********************************************************************
      END
      SUBROUTINE PROJEC_SLAVA(NEQ,NDIM,NC,SCAMA,ERVCOL,
     1                        PSITH,PSITHP,PROJEC,IERR)
C***********************************************************************
C                                                                      *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      DOUBLE PRECISION, DIMENSION(NEQ,NDIM)     :: PSITH
      DOUBLE PRECISION, DIMENSION(NDIM,NEQ)     :: PSITHP
      DOUBLE PRECISION, DIMENSION(2*NDIM,2*NDIM)     :: DUMMY
      DOUBLE PRECISION, DIMENSION(NC+2*NDIM,NEQ)     :: PSTP
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)      :: SCAMA
C-Sl-
      DOUBLE PRECISION, DIMENSION(NEQ,NC+NDIM):: ZC
      DOUBLE PRECISION, DIMENSION(NEQ,2*NDIM)   :: ZCC
      DOUBLE PRECISION, DIMENSION(NC+NDIM,NEQ)   :: ZCS
      DOUBLE PRECISION, DIMENSION(NEQ,NDIM)     :: C
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)      :: QC,QTEST,ERVCOL
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)      :: PROJEC
      DOUBLE PRECISION, DIMENSION(NEQ*(NEQ+1))  :: QF
C-Sl-
      DOUBLE PRECISION, DIMENSION(NDIM,NDIM)    :: TEMGRA
      DOUBLE PRECISION, DIMENSION(NEQ)          :: RW
      INTEGER         , DIMENSION(NEQ)          :: IW
      DOUBLE PRECISION, DIMENSION(NEQ,NC)       :: CSQ
      COMMON/BICMAT/COMAT(MNCOV*MNEQU),EMAT((MNSPE+2)*MNCON)
C***********************************************************************
C                                                                      *
C***********************************************************************
C-Sl- Inserted
      DO I=1,NEQ
      DO J=1,NC
      CSQ(I,J) = EMAT(I+(J-1)*NEQ)
      ENDDO
      ENDDO
      ZC=0.0d0
      ZC(1:NEQ,1:NDIM) = PSITH(1:NEQ,1:NDIM)
      ZC(1:NEQ,ndim+1:NC+ndim) = CSQ(1:NEQ,1:NC)
C---- Find orthogonal Basis
      CALL ORBAS(NEQ,NDIM+NC,NEQ,ZC,NEQ,QC,NEQ*(NEQ+1)
     1           ,QF,NEQ,IW,2,IERR)
      IF(IERR.GT.0) WRITE(*,*) 'IERR ORBAS= ', IERR
      CALL DGEINV(NEQ,QC,RW,IW,IERR)
      IF(IERR.GT.0)THEN
C      WRITE(*,*) 'IERR DGEINV= ', IERR
      END IF
C
      PSITHP = QC(1:NDIM,1:NEQ)
      ZCS(1:NDIM+NC,1:NEQ) = QC(1:NDIM+NC,1:NEQ)
      PROJEC=0.0D0
      PROJEC=-MATMUL(ZC,ZCS)
      DO I=1,NEQ
      PROJEC(I,I)=1.0D0+PROJEC(I,I)
      END DO
C-Sl- End Inserted
      RETURN
      END
C
      SUBROUTINE GENPSI(NEQ,NDIM,NEQS,SCAMA,PSITH,PSITHP)
C***********************************************************************
C                                                                      *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DOUBLE PRECISION, DIMENSION(NEQ,NDIM)     :: PSITH
      DOUBLE PRECISION, DIMENSION(NDIM,NEQ)     :: PSITHP
      DOUBLE PRECISION, DIMENSION(NEQS,NEQ)      :: SCAMA
      DOUBLE PRECISION, DIMENSION(NDIM,NDIM)    :: TEMGRA,SVDU,SVDV
      DOUBLE PRECISION, DIMENSION(NDIM)         :: SVDS 
      DOUBLE PRECISION, DIMENSION(NEQ)          :: RW
      DOUBLE PRECISION, DIMENSION(10*NDIM)      :: RW2
      INTEGER         , DIMENSION(NEQ)          :: IW
C***********************************************************************
C                                                                      *
C***********************************************************************
      LRW2=10*NDIM
      RTOL=1.D-10
      ATOL=1.D-10
      IF(NEQS.EQ.NEQ) THEN
         TEMGRA = MATMUL(TRANSPOSE(PSITH),MATMUL(SCAMA,PSITH))
         CALL DGEINV(NDIM,TEMGRA,RW,IW,IERINV)
         IF(IERINV.GT.0) GOTO 99
c     CALL DGESVD('A','A',NDIM,NDIM,TEMGRA,NDIM,SVDS,SVDU,
c    1            NDIM,SVDV,NDIM,RW2,LRW2,INFSVD)
c        IF(INFSVD.NE.0) THEN
c           WRITE(*,*) ' error svd in genpsi '
c           stop
c        ENDIF
c        SINMA = MAXVAL(SVDS(1:NDIM))
c        DO I=1,NDIM
c          IF(ABS(SVDS(I)).GT.RTOL*SINMA.AND.
c    1        ABS(SVDS(I)).GT.ATOL) THEN
c            SVDS(I) = 1.D0/SVDS(I)
c          ELSE
c            SVDS(I) =0.0D0
c          ENDIF
c        ENDDO 
c        write(*,*) 'ZZZZZZZZZ',SVDS
c        SVDU=TRANSPOSE(SVDU) 
c        SVDV=TRANSPOSE(SVDV)
c        DO I=1,NDIM
c          SVDV(1:NDIM,I) = SVDV(1:NDIM,I) * SVDS(I)
c        ENDDO
c        TEMGRA = MATMUL(SVDV,SVDU)
         PSITHP = MATMUL(MATMUL(TEMGRA,TRANSPOSE(PSITH)),SCAMA)
      ELSEIF(NEQS.EQ.NDIM) THEN
         TEMGRA = MATMUL(SCAMA,PSITH)
         CALL DGEINV(NDIM,TEMGRA,RW,IW,IERINV)
         PSITHP = MATMUL(TEMGRA,SCAMA)
      ELSEIF(NEQS.EQ.1) THEN
         TEMGRA = MATMUL(TRANSPOSE(PSITH),PSITH)
         CALL DGEINV(NDIM,TEMGRA,RW,IW,IERINV)
         IF(IERINV.GT.0) GOTO 99
         PSITHP = MATMUL(TEMGRA,TRANSPOSE(PSITH))
      ELSE
         GOTO 99
      ENDIF
      RETURN
  99  CONTINUE
C     WRITE(*,*) 'ERROR in GENPSI',NEQS,NDIM,NEQ
      PSITHP(:,:) = 0.0d0
C     STOP
      RETURN
C***********************************************************************
C                                                                      *
C***********************************************************************
      END
      SUBROUTINE HALNO(NEQ,NDIM,LDPSI,PSI,NVERT,IVC,GRAPF,
     1      NEIGH,DK,IERR)
C***********************************************************************
C                                                                      *
C     CALCULATE GRADIENT GRADH                                         *
C                                                                      *
C          NEQ  : number of state space variables                      *
C          NDIM : dimension of the manifold                            *
C          neigh: indices of the neighbours                            *
C          d    : distance in the NDIM directions                      *
C                                                                      *
C          OUTPUT:                                                     *
C          GRAPSI: GRADIENT D(PSI)/DTHETA                              *
C                                                                      *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION NEIGH(2,NDIM,NVERT),DK(2,NDIM)
      DIMENSION PSI(LDPSI,*),GRAPF(NEQ,NDIM)
C***********************************************************************
C     LOOP OVER DIMENSIONS
C***********************************************************************
      DO K=1,NDIM
      IRIG = NEIGH(2,K,IVC)
      ILEF = NEIGH(1,K,IVC)
!      IF(ILEF.NE.0) THEN
      IF(IRIG.NE.0) THEN
        GRAPF(1:NEQ,K)  = (PSI(1:NEQ,IRIG)-PSI(1:NEQ,IVC))/DK(2,K)
!        GRAPF(1:NEQ,K)  = (PSI(1:NEQ,IVC)-PSI(1:NEQ,ILEF))/DK(2,K)
      ELSE
        GRAPF(1:NEQ,K)  = 0.0d0
      ENDIF
      ENDDO
      RETURN
C***********************************************************************
C     END
C***********************************************************************
      END
      SUBROUTINE CENGRA(NEQ,NDIM,NVERT,IVC,GRAPF,CEGRA,
     1      NEIGH,DK,IERR)
C***********************************************************************
C
C     CALCULATE GRADIENT GRADH
C
C          NVERT: NUMBER OF VERTICES
C          NDIM : DIMENSION OF THE ILDM
C          IVC:   Index of the vertex
C          NEIGH: INDICES OF THE NEIGHBOURS (NUMBERS OF POINTS)
C          D    : DISTANCES IN NDIM DIMENSIONS (1:left, 2: right)
C          GRAPF: gradients at half nodes +1/2
C          LGRAPF: Flags for the different directions
C
C          OUTPUT:
C          CEGRA: gradients at the center point
C          GRAPSI: GRADIENT D(PSI)/DTHETA
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION NEIGH(2,NDIM,NVERT)
      DIMENSION GRAPF(NEQ,NDIM,NVERT),CEGRA(NEQ,NDIM)
      DOUBLE PRECISION, DIMENSION(2,NDIM)    ::  DK
      LOGICAL FLALEF, FLARIG
C---- the second NDIM-direction denotes the direction of the half node
C***********************************************************************
C     LOOP OVER DIMENSIONS
C***********************************************************************
      DO K=1,NDIM
      ILEF   = NEIGH(1,K,IVC)
      IRIG   = NEIGH(2,K,IVC)
      FLALEF = ILEF.NE.0
      FLARIG = IRIG.NE.0
C---- this vertex exists, check if it has neighbours in positive directions
      IF(FLALEF.AND.FLARIG) THEN
        CEGRA(:,K) =
     1    (DK(1,K) * GRAPF(:,K,IVC) + DK(2,K) * GRAPF (:,K,ILEF) )
     1     / (DK(1,K)+DK(2,K))
      ELSE IF(FLARIG.AND.(.NOT.FLALEF)) THEN
        CEGRA(:,K) = GRAPF(:,K,IVC)
      ELSE IF(FLALEF.AND.(.NOT.FLARIG)) THEN
        CEGRA(:,K) = GRAPF(:,K,ILEF)
      ELSE
        WRITE(6,*) 'ERROR in cengra, jjjj NO NEIGHBOURS IN DIR.',K ,IVC,
     1     NEIGH(1,:,IVC),'ll',NEIGH(2,:,IVC),FLALEF,FLARIG,ILEF
        STOP
      ENDIF
      ENDDO
      RETURN
C***********************************************************************
C     END
C***********************************************************************
      END
      SUBROUTINE HALFS(NEQ,NDIM,NVERT,IVC,GRAPF,LGRAPF,CEGRA,
     1      DMAT,NEQS,SCAMA,PTEF,PTEFP,NEIGH,DK,IERR)
C***********************************************************************
C                                                                      *
C     CALCULATE GRADIENT GRADH                                         *
C                                                                      *
C          NVERT: NUMBER OF VERTICES                                   *
C          NDIM : DIMENSION OF THE ILDM                                *
C          NEIGH: INDICES OF THE NEIGHBOURS (NUMBERS OF POINTS)        *
C          D    : DISTANCES IN NDIM DIMENSIONS                         *
C                                                                      *
C          OUTPUT:                                                     *
C          GRAPSI: GRADIENT D(PSI)/DTHETA                              *
C                                                                      *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION NEIGH(2,NDIM,NVERT)
      DIMENSION GRAPF(NEQ,NDIM,NVERT),CEGRA(NEQ,NDIM,NVERT)
      DIMENSION PTEF(NEQ,NDIM,NDIM),PTEFP(NDIM,NEQ,NDIM)
      LOGICAL, DIMENSION(NDIM,NVERT)    ::  LGRAPF
      DOUBLE PRECISION, DIMENSION(2,NDIM)    ::  DK
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)   :: DMATEM
      DOUBLE PRECISION, DIMENSION(NEQS,NEQ)   :: SCAMA
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ,NVERT) :: DMAT
      PARAMETER (HALF=0.5D0)
C---- the second NDIM-direction denotes the direction of the half node
C***********************************************************************
C     handle the regular terms
C***********************************************************************
      DO K=1,NDIM
      IF(LGRAPF(K,IVC)) THEN
         PTEF(:,K,K) = GRAPF(:,K,IVC)
C         PTEF(:,K,K) = CEGRA(:,K,IVC)
      ELSE
         PTEF(:,K,K) = CEGRA(:,K,IVC)
      ENDIF
      ENDDO
C***********************************************************************
C     LOOP OVER DIMENSIONS
C***********************************************************************
      DO 20 K=1,NDIM
      IRIG = NEIGH(2,K,IVC)
      DO 10 J=1,NDIM
      IF(K.EQ.J) GOTO 10
      IF(IRIG.NE.0) THEN
        PTEF(:,J,K) = HALF * (CEGRA(:,J,IVC) + CEGRA(:,J,IRIG))
      ELSE
        PTEF(:,J,K) = CEGRA(:,J,IVC)
      ENDIF
   10 CONTINUE
      CALL GENPSI(NEQ,NDIM,NEQS,SCAMA,PTEF(1,1,K),PTEFP(1,1,K))
      IF(IRIG.NE.0) THEN
      DMATEM = HALF * (DMAT(:,:,IVC) + DMAT(:,:,IRIG))
      ELSE
      DMATEM = DMAT(:,:,IVC)
      ENDIF
      PTEF(:,:,K) = MATMUL(DMATEM,PTEF(:,:,K))
   20 CONTINUE
      RETURN
C***********************************************************************
C     END
C***********************************************************************
      END
      SUBROUTINE HDPSTGT(NEQ,NDIM,NVERT,IVC,CEGRA,
     1      DMAT,tttl,PTEFGT,NEIGH,DK,IERR)
C***********************************************************************
C                                                                      *
C     CALCULATE GRADIENT GRADH                                         *
C                                                                      *
C          NVERT: NUMBER OF VERTICES                                   *
C          NDIM : DIMENSION OF THE ILDM                                *
C          NEIGH: INDICES OF THE NEIGHBOURS (NUMBERS OF POINTS)        *
C          D    : DISTANCES IN NDIM DIMENSIONS                         *
C                                                                      *
C          OUTPUT:                                                     *
C          GRAPSI: GRADIENT D(PSI)/DTHETA                              *
C                                                                      *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION NEIGH(2,NDIM,NVERT)
      DIMENSION CEGRA(NEQ,NDIM,NVERT),tttl(ndim,nvert),tttm(ndim)
      DIMENSION PTEF(NEQ,NDIM),PTEFGT(NEQ)
      DOUBLE PRECISION, DIMENSION(2,NDIM)    ::  DK
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)   :: DMATEM
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ,NVERT) :: DMAT
      PARAMETER (HALF=0.5D0)
C---- the second NDIM-direction denotes the direction of the half node
C***********************************************************************
C     handle the regular terms
C***********************************************************************
C***********************************************************************
C     LOOP OVER DIMENSIONS
C***********************************************************************
      DO 20 K=1,NDIM
      IRIG = NEIGH(2,K,IVC)
      IF(IRIG.NE.0) THEN
        PTEF(:,K) = CEGRA(:,k,IVC)!HALF * (CEGRA(:,k,IVC) + CEGRA(:,k,IRIG))
      ELSE
        PTEF(:,K) = CEGRA(:,k,IVC)
      ENDIF
      IF(IRIG.NE.0) THEN
      DMATEM = HALF * (DMAT(:,:,IVC) + DMAT(:,:,IRIG))
      tttm(k)=half*(tttl(k,ivc)+tttl(k,irig))
      ELSE
      DMATEM = DMAT(:,:,IVC)
      tttm(k)=tttl(k,ivc)
      ENDIF
      PTEF(:,K) = MATMUL(DMATEM,PTEF(:,k))

   20 CONTINUE
      ptefgt=matmul(PTEF,tttm)
      RETURN
C***********************************************************************
C     END
C***********************************************************************
      END
      SUBROUTINE HDIFMA(NEQ,NDIM,NVERT,IVC,
     1      DMAT,DMATEM,NEIGH,DK,IERR)
C***********************************************************************
C                                                                      *
C     CALCULATE GRADIENT GRADH                                         *
C                                                                      *
C          NVERT: NUMBER OF VERTICES                                   *
C          NDIM : DIMENSION OF THE ILDM                                *
C          NEIGH: INDICES OF THE NEIGHBOURS (NUMBERS OF POINTS)        *
C          D    : DISTANCES IN NDIM DIMENSIONS                         *
C                                                                      *
C          OUTPUT:                                                     *
C          GRAPSI: GRADIENT D(PSI)/DTHETA                              *
C                                                                      *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION NEIGH(2,NDIM,NVERT)
      DIMENSION GRAPF(NEQ,NDIM,NVERT),CEGRA(NEQ,NDIM,NVERT)
      DIMENSION PTEF(NEQ,NDIM,NDIM),PTEFP(NDIM,NEQ,NDIM)
      LOGICAL, DIMENSION(NDIM,NVERT)    ::  LGRAPF
      DOUBLE PRECISION, DIMENSION(2,NDIM)    ::  DK
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)   :: SCAMA,DMATEM
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ,NVERT) :: DMAT
      PARAMETER (HALF=0.5D0)
C---- the second NDIM-direction denotes the direction of the half node
C***********************************************************************
C     handle the regular terms
C***********************************************************************
C***********************************************************************
C     LOOP OVER DIMENSIONS
C***********************************************************************
      DO K=1,NDIM
      IRIG = NEIGH(2,K,IVC)
      IF(IRIG.NE.0) THEN
      DMATEM = HALF * (DMAT(:,:,IVC) + DMAT(:,:,IRIG))
      ELSE
      DMATEM = DMAT(:,:,IVC)
      ENDIF
      end do
      RETURN
C***********************************************************************
C     END
C***********************************************************************
      END
      SUBROUTINE GETDIS(INFO,NEQ,NDIM,COEFF,Y,PCON,GPVAL,TCMIN,TCMAX,
     1      SCAMA,PSITH,DIVTHE,DIVGRA,DETPST)
C***********************************************************************
C                                                                      *
C                                                                      *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      PARAMETER(ONE=1.0D0,TWO=2.0D0,ZERO=0.0D0,PI=3.141592653589793846)
      DOUBLE PRECISION                        ::DETPST
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)    :: SCAMA
      DOUBLE PRECISION, DIMENSION(NEQ,NDIM)   :: PSITH
      DOUBLE PRECISION, DIMENSION(NDIM,NEQ)   :: PCON
      DOUBLE PRECISION, DIMENSION(NDIM)       :: CHI,DIVTHE,DIVGRA
      DOUBLE PRECISION, DIMENSION(NDIM,NDIM)  :: TEMP
      DOUBLE PRECISION, DIMENSION(NDIM)       :: RW,GPVAL
      DOUBLE PRECISION, DIMENSION(NDIM)       :: TCMIN,TCMAX
      DOUBLE PRECISION, DIMENSION(NEQ)        :: Y
      INTEGER         , DIMENSION(NDIM)       :: IW
C***********************************************************************
C     Get gradient estimate in terms of reference coordinates
C***********************************************************************
      A = COEFF
      DIVTHE = 0
      DIVGRA = 0
      IF(INFO.EQ.1) THEN
        DIVTHE(:) = GPVAL(:)
        DIVGRA(:) = 0
      ELSE
        XLIL =0.05
          DO J=1,NDIM
           CHI(J) =(DOT_PRODUCT(PCON(J,1:NEQ),Y(1:NEQ))-TCMIN(J))/
     1         (TCMAX(J)-TCMIN(J))
          ENDDO
        CHI(1) = MIN(MAX(CHI(1),XLIL),1.D0-XLIL)
        HHH = MAX(MIN(dierfc(TWO*CHI(1)),1.D30),-1.D30)
        DIVTHE(1) = SQRT(A/(TWO*PI))*EXP(-HHH**2)
        DIVGRA(1) =  SQRT(TWO*A) * HHH
      ENDIF
C***********************************************************************
C     TRANSFORM in terms of reduced coordinates
C***********************************************************************
      TEMP   = MATMUL(PCON,PSITH)
C***********************************************************************
C A. Neagos:                                                           *
C Determinante der zu invertierenden Matrix zur Berechnung von         *
C grad_theta                                                           *
C***********************************************************************
      IF (NDIM.EQ.2)THEN
      DETPST = TEMP(1,1)*TEMP(2,2)-TEMP(1,2)*TEMP(2,1)
      ELSE
      DETPST=TEMP(1,1)
      ENDIF
      CALL DGEINV(NDIM,TEMP,RW,IW,IERINV)
      IF(IERINV.NE.0) THEN
!         WRITE(*,*) 'TOOOOOOOOO Error'
c       WRITE(*,*) PCON
c       WRITE(*,*) PSITH
C       STOP
        TEMP = 0
      ENDIF
      DIVTHE = MATMUL(TEMP,DIVTHE)
      DIVGRA = MATMUL(TEMP,DIVGRA)
C***********************************************************************
C
C***********************************************************************
      RETURN
C***********************************************************************
C     END OF
C***********************************************************************
      END
      function dierfc(y)
      implicit DOUBLE PRECISION (a - h, o - z)

      parameter (
     &    qa = 9.16461398268964d-01,
     &    qb = 2.31729200323405d-01,
     &    qc = 4.88826640273108d-01,
     &    qd = 1.24610454613712d-01,
     &    q0 = 4.99999303439796d-01,
     &    q1 = 1.16065025341614d-01,
     &    q2 = 1.50689047360223d-01,
     &    q3 = 2.69999308670029d-01,
     &    q4 = -7.28846765585675d-02)
      parameter (
     &    pa = 3.97886080735226000d+00,
     &    pb = 1.20782237635245222d-01,
     &    p0 = 2.44044510593190935d-01,
     &    p1 = 4.34397492331430115d-01,
     &    p2 = 6.86265948274097816d-01,
     &    p3 = 9.56464974744799006d-01,
     &    p4 = 1.16374581931560831d+00,
     &    p5 = 1.21448730779995237d+00,
     &    p6 = 1.05375024970847138d+00,
     &    p7 = 7.13657635868730364d-01,
     &    p8 = 3.16847638520135944d-01,
     &    p9 = 1.47297938331485121d-02,
     &    p10 = -1.05872177941595488d-01,
     &    p11 = -7.43424357241784861d-02)
      parameter (
     &    p12 = 2.20995927012179067d-03,
     &    p13 = 3.46494207789099922d-02,
     &    p14 = 1.42961988697898018d-02,
     &    p15 = -1.18598117047771104d-02,
     &    p16 = -1.12749169332504870d-02,
     &    p17 = 3.39721910367775861d-03,
     &    p18 = 6.85649426074558612d-03,
     &    p19 = -7.71708358954120939d-04,
     &    p20 = -3.51287146129100025d-03,
     &    p21 = 1.05739299623423047d-04,
     &    p22 = 1.12648096188977922d-03)
      z = y
      if (y .gt. 1) z = 2 - y
      w = qa - log(z)
      u = sqrt(w)
      s = (qc + log(u)) / w
      t = 1 / (u + qb)
      x = u * (1 - s * (0.5d0 + s * qd)) -
     &    ((((q4 * t + q3) * t + q2) * t + q1) * t + q0) * t
      t = pa / (pa + x)
      u = t - 0.5d0
      s = (((((((((p22 * u + p21) * u + p20) * u +
     &    p19) * u + p18) * u + p17) * u + p16) * u +
     &    p15) * u + p14) * u + p13) * u + p12
      s = ((((((((((((s * u + p11) * u + p10) * u +
     &    p9) * u + p8) * u + p7) * u + p6) * u + p5) * u +
     &    p4) * u + p3) * u + p2) * u + p1) * u + p0) * t -
     &    z * exp(x * x - pb)
      x = x + s * (1 + x * s)
      if (y .gt. 1) x = -x
      dierfc = x
      end
      SUBROUTINE GSDER(NEQ,NDIM,NVERT,IVC,GRAXI,DGRAXI,NEIGH,DK,IERR)
C***********************************************************************
C                                                                      *
C     CALCULATE GRADIENT GRADH                                         *
C                                                                      *
C          NVERT: NUMBER OF VERTICES                                   *
C          NDIM : DIMENSION OF THE ILDM                                *
C          NEIGH: INDICES OF THE NEIGHBOURS (NUMBERS OF POINTS)        *
C          D    : DISTANCES IN NDIM DIMENSIONS                         *
C                                                                      *
C                                                                      *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION NEIGH(2,NDIM,NVERT)
      DIMENSION GRAXI(NEQ,NVERT),DGRAXI(NEQ,NDIM)
      DOUBLE PRECISION, DIMENSION(2,NDIM)    ::  DK
      LOGICAL FLALEF, FLARIG
C---- the second NDIM-direction denotes the direction of the half node
C***********************************************************************
C     LOOP OVER DIMENSIONS
C***********************************************************************
      DO K=1,NDIM
      ILEF = NEIGH(1,K,IVC)
      IRIG = NEIGH(2,K,IVC)
      FLALEF = .NOT.(ILEF.EQ.0)
      FLARIG = .NOT.(IRIG.EQ.0)
      IF(FLALEF.AND.FLARIG) THEN
        DGRAXI(:,K) =
     1  (  (DK(1,K) / DK(2,K)) * (GRAXI(:,IRIG) - GRAXI(:,IVC) )
     1   +(DK(2,K) / DK(1,K)) * (GRAXI(:,IVC ) - GRAXI(:,ILEF)))
     1     / (DK(1,K)+DK(2,K))
      ELSE IF(FLARIG.AND.(.NOT.FLALEF)) THEN
        DGRAXI(:,K) = (GRAXI(:,IRIG) - GRAXI(:,IVC)  ) / DK(2,K)
      ELSE IF(FLALEF.AND.(.NOT.FLARIG)) THEN
        DGRAXI(:,K) = (GRAXI(:,IVC ) - GRAXI(:,ILEF) ) / DK(1,K)
      ELSE
        WRITE(6,*) ' FATAL ERROR, jjjj NO NEIGHBOURS IN DIR.',K ,IVC,
     1     NEIGH(1,:,IVC),'ll',NEIGH(2,:,IVC),FLALEF,FLARIG,ILEF
        STOP
      ENDIF
      ENDDO
      RETURN
C***********************************************************************
C     END
C***********************************************************************
      END
      SUBROUTINE UPWIND(NEQ,NDIM,NVERT,IVC,GRAPF,LGRAPF,CEGRA,
     1   F,UPWGRA,NEIGH,DK,IERR)
C***********************************************************************
C
C     get upwind direction
C
C          NVERT: NUMBER OF VERTICES
C          NDIM : DIMENSION OF THE ILDM
C          IVC:   Index of the vertex
C          NEIGH: INDICES OF THE NEIGHBOURS (NUMBERS OF POINTS)
C          D    : DISTANCES IN NDIM DIMENSIONS (1:left, 2: right)
C          GRAPF: gradients at half nodes +1/2
C          LGRAPF: Flags for the different directions
C          F     : source term
C
C          OUTPUT:
C          UPWGRA  GRADIENT D(PSI)/DTHETA  in upwind direction
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION NEIGH(2,NDIM,NVERT)
      DIMENSION GRAPF(NEQ,NDIM,NVERT),CEGRA(NEQ,NDIM)
      DIMENSION UPWGRA(NEQ,NDIM),F(NEQ)
      LOGICAL, DIMENSION(NDIM,NVERT)    ::  LGRAPF
      DOUBLE PRECISION, DIMENSION(2,NDIM)    ::  DK
      DOUBLE PRECISION, DIMENSION(NDIM)     ::  COSA
      LOGICAL FLALEF, FLARIG,DOUP
C---- the second NDIM-direction denotes the direction of the half node
C***********************************************************************
C     LOOP OVER DIMENSIONS
C***********************************************************************
      DOUP = .true.
C***********************************************************************
C     No upwind
C***********************************************************************
      IF(.NOT.(DOUP)) THEN
      UPWGRA = CEGRA
      ELSE
C***********************************************************************
C     UPWIND
C***********************************************************************
      COSA= MATMUL(TRANSPOSE(CEGRA),F)
      DO K=1,NDIM
      IF(COSA(K).GT.0.D0) THEN
C---- take backward difference
        ILEF = NEIGH(1,K,IVC)
          IF(ILEF.NE.0) THEN
            UPWGRA(:,K)  = GRAPF(:,K,ILEF)
c              write(*,*) 'hhhh',IVC,k,'backward'
          ELSE
            UPWGRA(:,K)  = CEGRA(:,K)
c              write(*,*) 'hhhh',IVC,k,'central'
          ENDIF
      ELSE
C---- take forward
        FLARIG = LGRAPF(K,IVC)
          IF(FLARIG) THEN
            UPWGRA(:,K)  = GRAPF(:,K,IVC)
c              write(*,*) 'hhhh',IVC,k,'forward'
          ELSE
            UPWGRA(:,K)  = CEGRA(:,K)
c              write(*,*) 'zzzz',IVC,k,'central'
          ENDIF
      ENDIF
      ENDDO
C***********************************************************************
C
C***********************************************************************
      ENDIF
      RETURN
C***********************************************************************
C     END
C***********************************************************************
      END
      SUBROUTINE GRAPRO(lll,NEQ,NDIM,PCON,PSITH,GRPRO)
C***********************************************************************
C                                                                      *
C                                                                      *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DOUBLE PRECISION, DIMENSION(NEQ,NDIM)   :: PSITH
      DOUBLE PRECISION, DIMENSION(NDIM,NEQ)   :: PCON,GRPRO
      DOUBLE PRECISION, DIMENSION(NDIM,NDIM)  :: TEMP
      DOUBLE PRECISION, DIMENSION(NDIM)       :: RW
      INTEGER         , DIMENSION(NDIM)       :: IW
C***********************************************************************
C
C***********************************************************************
      TEMP   = MATMUL(PCON,PSITH)
      CALL DGEINV(NDIM,TEMP,RW,IW,IERINV)
      IF(IERINV.NE.0) THEN
c       WRITE(*,*) 'OOOOOZZZnO Error'
c       DO I=1,NDIM
c       WRITE(*,*) Psith(:,i)
c       ENDDO
        TEMP = 0
      ENDIF
      GRPRO  = MATMUL(TEMP,PCON)
C***********************************************************************
C
C***********************************************************************
      RETURN
      END
      SUBROUTINE GSDE1(NDIM,NVERT,IVC,TTT,DGRAXI,NEIGH,DK,IERR)
C***********************************************************************
C                                                                      *
C     CALCULATE GRADIENT GRADH                                         *
C                                                                      *
C          NVERT: NUMBER OF VERTICES                                   *
C          NDIM : DIMENSION OF THE ILDM                                *
C          NEIGH: INDICES OF THE NEIGHBOURS (NUMBERS OF POINTS)        *
C          D    : DISTANCES IN NDIM DIMENSIONS                         *
C                                                                      *
C                                                                      *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION NEIGH(2,NDIM,NVERT)
      DIMENSION TTT(NDIM,NVERT),DGRAXI(NDIM)
      DOUBLE PRECISION, DIMENSION(2,NDIM)    ::  DK
      DOUBLE PRECISION, DIMENSION(NDIM,NDIM) ::  XTE
      LOGICAL FLALEF, FLARIG
C---- the second NDIM-direction denotes the direction of the half node
C***********************************************************************
C     LOOP OVER DIMENSIONS
C***********************************************************************
      DO K=1,NDIM
      ILEF = NEIGH(1,K,IVC)
      IRIG = NEIGH(2,K,IVC)
      FLALEF = .NOT.(ILEF.EQ.0)
      FLARIG = .NOT.(IRIG.EQ.0)
      IF(FLALEF.AND.FLARIG) THEN
        XTE(:,K) =
     1  (  (DK(1,K) / DK(2,K)) * (TTT(:,IRIG) - TTT(:,IVC) )
     1   +(DK(2,K) / DK(1,K)) * (TTT(:,IVC ) - TTT(:,ILEF)))
     1     / (DK(1,K)+DK(2,K))
      ELSE IF(FLARIG.AND.(.NOT.FLALEF)) THEN
        XTE(:,K) = (TTT(:,IRIG) - TTT(:,IVC)  ) / DK(2,K)
      ELSE IF(FLALEF.AND.(.NOT.FLARIG)) THEN
        XTE(:,K) = (TTT(:,IVC ) - TTT(:,ILEF) ) / DK(1,K)
      ELSE
        WRITE(6,*) ' FATAL ERROR, jjjj NO NEIGHBOURS IN DIR.',K ,IVC,
     1     NEIGH(1,:,IVC),'ll',NEIGH(2,:,IVC),FLALEF,FLARIG,ILEF
        STOP
      ENDIF
      ENDDO
      DGRAXI = MATMUL(XTE,TTT(:,IVC))
      RETURN
C***********************************************************************
C     END
C***********************************************************************
      END
      SUBROUTINE GETGRN(INFO,LIMGRA,NEQ,NDIM,
     1          XGRPS,GPM,GPVL,GPLM,PSITH,PSITHP,GRAT,DETPST)
C***********************************************************************
C                                                                      *
C                                                                      *
C***********************************************************************
!       USE RDMESH
!       USE GRAFI
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      PARAMETER(ONE=1.0D0,TWO=2.0D0,ZERO=0.0D0)
      LOGICAL LIMGRA
      DOUBLE PRECISION                        ::DETPST
      DOUBLE PRECISION, DIMENSION(NEQ)        :: XGRPS
      DOUBLE PRECISION, DIMENSION(NEQ,NDIM)   :: PSITH
      DOUBLE PRECISION, DIMENSION(NDIM,NEQ)   :: PSITHP,GPM
      DOUBLE PRECISION, DIMENSION(NDIM,NDIM)  :: TEMP
      DOUBLE PRECISION, DIMENSION(NDIM)       :: GRAT
      DOUBLE PRECISION, DIMENSION(NDIM)       :: RW
      DOUBLE PRECISION, DIMENSION(NDIM)       :: GRDI,GPLM,GPVL
      INTEGER         , DIMENSION(NDIM)       :: IW
C***********************************************************************
C     calculate gradients of gradient directions
C***********************************************************************
      GRDI   = MATMUL(GPM(:,:),XGRPS(:))
C     WRITE(*,*) 'jjjjjjjjjj',GRDI,GPLM
C***********************************************************************
C     ccheck for limit
C***********************************************************************
      LIMGRA = .FALSE.
      DO J=1,NDIM
      IF(ABS(GRDI(J)).LT.ABS(GPLM(J))) THEN
          WRITE(*,*) 'xlim active',GRDI(J)
         GRDI(J)=GPLM(J)
c        WRITE(*,*) 'IIIIIIIII GRDI set to ', GPLIM(J)
         LIMGRA = .TRUE.
      ENDIF
      ENDDO
C***********************************************************************
C     get gradient in terms of reduced coordinates
C***********************************************************************
      IF(INFO.EQ.0) THEN
      GRAT   =  MATMUL(PSITHP(:,:),XGRPS(:))
      ELSE
      TEMP   = MATMUL(GPM(:,:),PSITH(:,:))
      IF (NDIM.EQ.2) THEN
      DETPST = TEMP(1,1)*TEMP(2,2)-TEMP(1,2)*TEMP(2,1)
      ELSE
      DETPST = 0.0D0
      END IF
      CALL DGEINV(NDIM,TEMP,RW,IW,IERINV)
      IF(IERINV.NE.0) THEN
c       WRITE(*,*) 'zzzFFFOOOO Error'
c       WRITE(*,*) temp ,'PPPPPPPP'
c       WRITE(*,*) GPMA(1,:)
c       WRITE(*,*) GPMA(2,:)
c       WRITE(*,*) Psith(:,1)
c       WRITE(*,*) Psith(:,2)
C         WRITE(*,*)'INVERSE ERROR GETDIS'
!         STOP
        TEMP=0
      ENDIF
      GRAT   = MATMUL(TEMP(1:NDIM,1:NDIM),GRDI(:))
      ENDIF
      RETURN
C***********************************************************************
C     END OF
C***********************************************************************
      END
      SUBROUTINE HALFTTL(NEQ,NDIM,NVERT,IVC,XGRPSI,PTEFP,NEIGH,IERR,
     1      TTTHAL)
C***********************************************************************
C                                                                      *
C     CALCULATE GRADIENT GRADH                                         *
C                                                                      *
C                                                                      *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION NEIGH(2,NDIM,NVERT)
      DIMENSION XGRPSI(NEQ,NVERT)
      DIMENSION PTEFP(NDIM,NEQ,NDIM,NVERT)
      DIMENSION TTTHAL(NDIM,NDIM)
      DOUBLE PRECISION, DIMENSION(NEQ)   :: GRATEM
      PARAMETER (HALF=0.5D0)
C***********************************************************************
C     LOOP OVER DIMENSIONS
C***********************************************************************
      DO 20 K=1,NDIM
      IRIG = NEIGH(2,K,IVC)
      IF(IRIG.NE.0) THEN
      GRATEM = HALF * (XGRPSI(:,IVC) + XGRPSI(:,IRIG))
      ELSE
      GRATEM = 0.0D0
      ENDIF
      TTTHAL(:,K) = MATMUL(PTEFP(:,:,K,IVC),GRATEM(:))
   20 CONTINUE
      RETURN
C***********************************************************************
C     END
C***********************************************************************
      END
      SUBROUTINE ALTHES(NEQ,NDIM,NVERT,IVC,NEIGH,DPTHAL,TTTL,TTTHAL,
     1                  DIFF)
C***********************************************************************
C                                                                      *
C                                                                      *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      PARAMETER(ONE=1.0D0,TWO=2.0D0,ZERO=0.0D0)
      DIMENSION NEIGH(2,NDIM,NVERT)
      DOUBLE PRECISION, DIMENSION(NEQ)                 :: DIFF
      DOUBLE PRECISION, DIMENSION(NEQ,NDIM,NDIM,NVERT) :: DPTHAL
      DOUBLE PRECISION, DIMENSION(NDIM,NDIM,NVERT)     :: TTTHAL
      DOUBLE PRECISION, DIMENSION(NDIM)                :: TTTL
      LOGICAL TEST
C***********************************************************************
C
C***********************************************************************
      DIFF(:) = ZERO
      DO I=1,NDIM
          ILEF = NEIGH(1,I,IVC)
          IRIG = NEIGH(2,I,IVC)
        IF((ILEF.NE.0).AND.(IRIG.NE.0))      THEN
        DIFF(:) = DIFF(:) + TTTL(I)*
     1     (MATMUL(DPTHAL(:,:,I,IVC),TTTHAL(:,I,IVC))
     1    -MATMUL(DPTHAL(:,:,I,ILEF),TTTHAL(:,I,ILEF)))
        ENDIF
      ENDDO
      RETURN
C***********************************************************************
C     END OF ALTHES
C***********************************************************************
      END
      SUBROUTINE HAXTTL(NEQ,NDIM,NVERT,IVC,TTTL,NEIGH,IERR,TTTHAL)
C***********************************************************************
C                                                                      *
C     CALCULATE GRADIENT GRADH                                         *
C                                                                      *
C                                                                      *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION NEIGH(2,NDIM,NVERT)
      DIMENSION TTTL(NDIM,NVERT)
      DIMENSION TTTHAL(NDIM,NDIM)
      DIMENSION GRATEM(NDIM)
      PARAMETER (HALF=0.5D0)
C***********************************************************************
C     LOOP OVER DIMENSIONS
C***********************************************************************
      DO 20 K=1,NDIM
      IRIG = NEIGH(2,K,IVC)
      IF(IRIG.NE.0) THEN
      GRATEM = HALF * (TTTL(:,IVC) + TTTL(:,IRIG))
      ELSE
      GRATEM = TTTL(:,IVC)
      ENDIF
      TTTHAL(:,K) = GRATEM
   20 CONTINUE
      RETURN
C***********************************************************************
C     END
C***********************************************************************
      END
      SUBROUTINE XXXOU(INFO,NFISTO,NEQ,NPROP,SYMPRP,PROP,NVERT,NDIM)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *             PROGRAM FOR THE OUtPUT OF PROFILES            *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C***********************************************************************
C     I.: TYPES
C***********************************************************************
      USE GRAFI
      USE RDMESH
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      CHARACTER(LEN=4) NNNN
      CHARACTER(LEN=76) MMMM
      CHARACTER(LEN=*) SYMPRP(NPROP)
      DIMENSION PROP(NPROP,*)
      COMMON/BLOCOR/IIRAT,IISMF,IISMFP,IIRV,IIPRO,IIPNU,IILAM,NPROOT
      DATA MSGFIL/6/
C***********************************************************************
C     Write symbols of species and species gradients (G)
C***********************************************************************
  834 FORMAT('#',I4)
  836 FORMAT('TITLE= "Output                             "')
  837 FORMAT('VARIABLES =',/,7('"',A,'"',:','))
  838 FORMAT(7('"',A,'"',','))
  839 FORMAT(7('"',A,'"',:','))
  843 FORMAT('ZONE I=',I4,', J=',I4,', F=POINT')
      write(*,*) 'NNNNNN',NL,1
C***********************************************************************
C     vertex properties
C***********************************************************************
  844 FORMAT(1(3X,7(1PE20.13,1X)))
      WRITE(NFISTO,834) NEQ
      WRITE(NFISTO,836)
      WRITE(NFISTO,837) (SYMPRP(IISMF-1+I),I=1,NEQ)
      WRITE(NFISTO,838) (SYMPRP(IISMF-1+I)//'G',I=1,NEQ)
      WRITE(NFISTO,839) (SYMPRP(IISMF-1+I)//'S',I=1,NEQ)
      WRITE(NFISTO,843) NVERT,1
*      DO L=1,NL*NK*NNM
*      IM = (L-1)/NL/NK + 1
*      IK = (L-1)/NL + 1 - (IM-1)*NK
*      IL = L - (IK-1)* NL-(IM-1)*NK*NL
*      PSIFI(1:NEQ,IL,IK,IM)=PROP(IISMF:IISMF-1+NEQ,L)
*      WRITE(NFISTO,844,ERR=910) PSIFI(1:NEQ,IL,IK,IM),XGRPSI(1:NEQ,L)
*     1                                           ,XSEPSI(1:NEQ,L)
*      ENDDO
      DO L=1,NVERT
      WRITE(NFISTO,844)(PROP(IISMF-1+I,L),I=1,NEQ),
     1              (XGRPSI(I,L),I=1,NEQ),(XSEPSI(I,L),I=1,NEQ)
      END DO
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
 910  CONTINUE
      WRITE(MSGFIL,911)
 911  FORMAT(1H ,' END OF FILE DETECTED DURING INPUT OF PROFILES ')
      STOP
C***********************************************************************
C
C***********************************************************************
      END
      SUBROUTINE XXXOU2(INFO,NFISTO,NEQ,NPROP,SYMPRP,PROP,NVERT,NDIM,
     1                  ICORD)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *             PROGRAM FOR THE OUtPUT OF PROFILES            *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C***********************************************************************
C     I.: TYPES
C***********************************************************************
      USE GRAFI
      USE RDMESH
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      CHARACTER(LEN=4) NNNN
      CHARACTER(LEN=76) MMMM
      CHARACTER(LEN=*) SYMPRP(NPROP)
      DIMENSION PROP(NPROP,*)
      DIMENSION ICORD(NDIM,NVERT)
      DOUBLE PRECISION, DIMENSION(NEQ,NVERT) ::  XXX,XXXGR,XXXSE
      DOUBLE PRECISION, DIMENSION(NEQ,NVERT) ::  YYY,YYYGR,YYYSE
      COMMON/BLOCOR/IIRAT,IISMF,IISMFP,IIRV,IIPRO,IIPNU,IILAM,NPROOT
      DATA MSGFIL/6/
C***********************************************************************
C     Write symbols of species and species gradients (G)
C***********************************************************************
  834 FORMAT('#',I4)
  836 FORMAT('TITLE= "Output                             "')
  837 FORMAT('VARIABLES =',/,7('"',A,'"',:','))
  838 FORMAT(7('"',A,'"',','))
  839 FORMAT(7('"',A,'"',:','))
  843 FORMAT('ZONE I=',I4,', J=',I4,', F=POINT')
      write(*,*) 'NNNNNN',NL,NK
C***********************************************************************
C     vertex properties
C***********************************************************************
  844 FORMAT(1(3X,7(1PE20.13,1X)))
* Output of Psi and Psitheta for determination of INSFLA profile distances
      OPEN(66,STATUS='UNKNOWN')
      WRITE(66,837)(SYMPRP(IISMF-1+I),I=1,NEQ)
      WRITE(66,444)'NDIM =',NDIM
      WRITE(66,444)'NEQ  =',NEQ
      WRITE(66,444)'NVERT=',NVERT
      DO L=1,NVERT
      WRITE(66,844)(PROP(IISMF-1+I,L),I=1,NEQ),
     1              ((PSTH(I,J,L),I=1,NEQ),J=1,NDIM)
      END DO
  444 FORMAT(A6,I5)
!
!
      WRITE(NFISTO,834) NEQ
      WRITE(NFISTO,836)
      WRITE(NFISTO,837) (SYMPRP(IISMF-1+I),I=1,NEQ)
      WRITE(NFISTO,838) (SYMPRP(IISMF-1+I)//'G',I=1,NEQ)
      WRITE(NFISTO,839) (SYMPRP(IISMF-1+I)//'S',I=1,NEQ)
      WRITE(NFISTO,843) maxval(icord(1,:))-minval(icord(1,:))+1,
     1                  maxval(icord(2,:))-minval(icord(2,:))+1
      do i=minval(icord(2,:)),maxval(icord(2,:))
      DO L=1,nvert
        if (icord(2,l).eq.i)then
      WRITE(NFISTO,844,ERR=910) PROP(IISMF:IISMF-1+NEQ,L),
     1                                             XGRPSI(1:NEQ,l)
     1                                           ,XSEPSI(1:NEQ,l)
      end if
*        if (icord(2,l).eq.2)then
*      if (icord(1,l).eq.1 .and.icord(2,l).eq.1) then
*      WRITE(NFISTO,844,ERR=910) PROP(IISMF:IISMF-1+NEQ,L),
*     1                                             XGRPSI(1:NEQ,l)
*     1                                           ,XSEPSI(1:NEQ,l)
*      end if
*      if (icord(1,l).eq.maxval(icord(1,:)) .and.icord(2,l).eq.
*     1           maxval(icord(2,:))) then
*      WRITE(NFISTO,844,ERR=910) PROP(IISMF:IISMF-1+NEQ,L),
*     1                                             XGRPSI(1:NEQ,l)
*     1                                           ,XSEPSI(1:NEQ,l)
*      end if
      ENDDO
      end do
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
 910  CONTINUE
      WRITE(MSGFIL,911)
 911  FORMAT(1H ,' END OF FILE DETECTED DURING INPUT OF PROFILES ')
      STOP
C***********************************************************************
C
C***********************************************************************
      END
      SUBROUTINE DIRFLA(NDIM,NVERT,LGRAPF,LGRAPB,NEIGH,IERR)
C***********************************************************************
C                                                                      *
C     GET boundary information                                         *
C                                                                      *
C          NDIM                : dimension of the manifold
C          NVERT               : number of vertices
C          NEIGH(2,NDIM,NVERT) : indices of the neighbours
C                                                                      *
C      OUTPUT:                                                     *
C        LGRAPF(NDIM,NVERT)
C        LGRAPF(K,I)  = true if vertex I has a right neighbour
C            in k-direction
C        LGRAPB(NDIM,NVERT)
C        LGRAPB(K,I)  = true if vertex I has a left  neighbour
C            in k-direction
C                                                                      *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION NEIGH(2,NDIM,NVERT)
      LOGICAL, DIMENSION(NDIM,NVERT)    ::  LGRAPF,LGRAPB
C***********************************************************************
C     LOOP OVER DIMENSIONS
C***********************************************************************
      LGRAPF(:,:) = .FALSE.
      LGRAPB(:,:) = .FALSE.
      DO IVC = 1,NVERT
        DO K=1,NDIM
          ILEF = NEIGH(1,K,IVC)
          IRIG = NEIGH(2,K,IVC)
          IF(ILEF.NE.0) LGRAPB(K,IVC) = .TRUE.
          IF(IRIG.NE.0) LGRAPF(K,IVC) = .TRUE.
      ENDDO
      ENDDO
      RETURN
C***********************************************************************
C     END
C***********************************************************************
      END
      SUBROUTINE F_E(N,TIME,Y,DY)
C***********************************************************************
C
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
C***********************************************************************
C     Arrays
C***********************************************************************
      COMMON/BINVA/ZETA,DELTAT,ISTE,NAVERT,NAEQ,NADIM,
     1          IVINF(NVERM),NEIGHP(2,NDIMM*NVERM)
C----
      DOUBLE PRECISION, DIMENSION(N)             :: Y,DW
C---- Work arrays
      COMMON/BPARM/RPAR(MNRPAR),IPAR(MNIPAR)
      COMMON/BSCAD/DCOV(NVERM)
      COMMON/BERV/ERVRO((MNSPE+2)**2),ERVCO((MNSPE+2)**2),NZVT
C***********************************************************************
C     Initializations
C***********************************************************************
      IEQ    = IPAR(10)
      NSPEC  = IPAR(3)
      ITRM   = IPAR(13)
      IMPLI = -1
      CALL DESINV(NADIM,NAEQ,IEQ,NSPEC,NAVERT,DELTAT,ZETA,Y,DW,
     1          IVINF,NEIGHP,RPAR,IPAR,ERVRO,ERVCO,NZVT,
     1          DCOV,ITRM,ISTE,IRDES,IMPLI)
CBUG   DYE not defined
      DY=DYE
      IF(IRDES.NE.0) IFC = 9
      RETURN
      END
      SUBROUTINE F_I(ICURR,NPDES,TIME,Y,DY,WANT_JAC,JAC)
C***********************************************************************
C
C
C***********************************************************************
      USE RDMESH
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      LOGICAL WANT_JAC
C***********************************************************************
C     Arrays
C***********************************************************************
      DOUBLE PRECISION, DIMENSION(NPDES)          :: Y,DY
      DOUBLE PRECISION, DIMENSION(NPDES,NPDES)    :: JAC
C***********************************************************************
C     Call of DESINV
C***********************************************************************
      IACT = ((ICURR-1)/NPDES) +1
      IMPLI = -1
      DY(:)    = DYI(:,IACT)
C      if(want_jac) then
      JAC(:,:) = APPJAC(:,:,IACT)
C      end if
      RETURN
      END
      subroutine error_dat(ndim,is,deltat,manierr,terr,rhoerr,
     1                     errmax,icord)
C------------------------------------------------------------------
C A. Neagos
C Error output REDIM integration
C------------------------------------------------------------------
        double precision manierr,errmax,deltat,rhoerr,terr
        integer is,icord(ndim)
        character(LEN=11) thet(ndim)
        do i=1,ndim
            write(thet(i),'(A7,I3,A1)')',"theta',i,'"'
        end do
        if (is.eq.1)then
        open(unit=7,status='replace',file="ERROR.dat",action='write')
        WRITE(7,*)'VARIABLES='
        WRITE(7,*)'"Integration step","time","timestep",'
     1              ,'"invariance defect","temperature error"',
     1               '"density error","errmax"',(thet(i),i=1,ndim)
        end if
        WRITE(7,8854) IS,is*deltat,deltat,manierr,terr,rhoerr,
     1                errmax,(icord(i),i=1,ndim)
 8854   FORMAT(I5,6(1PE12.5,1X),6(I5))
      end subroutine error_dat
      SUBROUTINE MPPSX(NEQ,NDIM,PSITH,PSITHP)
C***********************************************************************
C                                                                      *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DOUBLE PRECISION, DIMENSION(NEQ,NDIM)     :: PSITH
      DOUBLE PRECISION, DIMENSION(NDIM,NEQ)     :: PSITHP
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)    :: TEMGRA
      DOUBLE PRECISION, DIMENSION(NEQ)          :: RW
      INTEGER         , DIMENSION(NEQ)          :: IW
C***********************************************************************
C                                                                      *
C***********************************************************************
      TEMGRA = MATMUL(PSITH,TRANSPOSE(PSITH))
      CALL DGEINV(NEQ,TEMGRA,RW,IW,IERINV)
      PSITHP = MATMUL(TRANSPOSE(PSITH),TEMGRA)
      IF(IERINV.GT.0)  THEN
        write(*,*)'inierv',IERINV
          WRITE(*,*) 'ERROR IN MPPSI x'
      ENDIF
      RETURN
      END
