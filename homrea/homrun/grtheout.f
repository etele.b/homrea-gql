      SUBROUTINE GRTHEOUT(NEQ,NPROPN,SYMPRP,NVERT,PROPN,
     1     ICORD,NDIM,N2DIM,NCELL,IVERT)
C***********************************************************************
C    Subroutine for additional output to fort.86
C
C    Integer-variables and -arrays:
C    -----------------------------
C    NEQ   : Number of independent variables
C    NDIM  : Dimension of the REDIM
C    NPROPN: Number of variables in fort.86
C    NVERT : Number of vertices
C    NUMPRO: Total number of variables in new generated file fort.88
C    ICORD : Number of coordinates (array)
C
C    Real-arrays:
C    ------------
C    PROPN   : Output-Vector in fort.86
C    PROPERT : Output-vector in fort.88
C    DYI     : Matrix of projected source term
C    DYE     : Matrix of projected diffusion term
C    SOURCE  : Matrix of source term
C    DIFFU   : Matrix of diffusion term
C    DIFFU2  : Matrix of second part of diffusion term including
C              div(grad(theta)))
C    PSTH  : Vector of first derivative of PSI in theta
C    PSTT1   : Vector of second derivative of PSI in theta1 direction
C    PSTT2   : Vector of second derivative of PSI in theta2 direction
C    NORMVEC : Matrix containing normal vectors at every grid point
C    PHI     : Sum of diffusion and source term
C    NRM_SRC : Scalar product of normal vector and source term vector
C    NRM_DIFF: Scalar product of normal vector and diff. term vector
C    NRM_PHI : Scalar product of normal vector and Phi
C    SCPSTH  : Scalar product of PSTH(1) and PSHT(2)
C    NNORMVEC: Euclidian norm of NORMVEC
C    NSRC    : Euclidian norm of SRC
C    NDIFFU  : Euclidian norm of DIFFU
C    NDIFFU2  : Euclidian norm of DIFFU2
C    NPHI    : Euclidian norm of PHI
C    NPSTH1  : Euclidian norm of PSITH(1)
C    NPSTH2  : Euclidian norm of PSITH(2)
C    NPSTT1  : Euclidian norm of PSTT(1)
C    NPSTT2  : Euclidian norm of PSTT(2)
C    ANG_SRC : Angle between source term vector and normal vector
C    ANG_DIFF: Angle between diff. term vector and normal vector
C    ANG_PHI : Angle between Phi and normal vector
C    ANGPSTH : Angle between PSTH(1) and PSHT(2)
C    TTTL    : Matrix containing gradient vectors at each grid point
C    DETPSITH: Determinant of Psi_theta
C    SCMA   : Scaling matrix
C
C    Variables DYI, DYE, DIFFU, DIFFU2, SOURCE, TTTL, PSTH, PSTT1, PSTT2
C    and DETPSITH are located in module RDMESH!!!
C
C
C    Character-arrays:
C    -----------------
C    SYMPRP : Symbols for header in fort.86
C    SYMPROP: Symbols for header in fort.88
C***********************************************************************

C***********************************************************************
C     Variables
C***********************************************************************
      USE RDMESH
      USE GRAFI
      implicit double precision(a-h,o-z)
      include 'homdim.f'
      INTEGER                                :: NDIM
      INTEGER                                :: NPROPN
      INTEGER                                :: NVERT
      INTEGER                                :: NEQ,NI
C***********************************************************************
C     Arrays
C***********************************************************************
      DOUBLE PRECISION, DIMENSION(NPROPN,NVERT)       :: PROPN
      DOUBLE PRECISION, ALLOCATABLE,DIMENSION(:,:)    :: NORMVEC
!       DOUBLE PRECISION, ALLOCATABLE,DIMENSION(:,:)    :: PHI
      DOUBLE PRECISION, ALLOCATABLE,DIMENSION(:,:)    :: PSTH1
      DOUBLE PRECISION, ALLOCATABLE,DIMENSION(:,:)    :: PSTH2
      CHARACTER(LEN=*), DIMENSION(NPROPN)              :: SYMPRP
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:,:)   :: PROPERT
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:)     :: NRM_SRC
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:)     :: NRM_DIFF
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:)     :: NRM_PHI
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:)     :: SCPSTH
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:)     :: ANG_SRC
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:)     :: ANG_DIFF
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:)     :: ANG_PHI
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:)     :: ANGPSTH
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:)     :: NNORMVEC
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:)     :: NSRC
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:)     :: NDIFFU
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:)     :: NDIFFU2
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:)     :: NPHI
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:)     :: NPSTH1
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:)     :: NPSTH2
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:)     :: NPSTT1
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:)     :: NPSTT2
      CHARACTER(LEN=LSYMB), ALLOCATABLE, DIMENSION(:) :: SYMPROP
      INTEGER, DIMENSION(NDIM,NVERT)                  :: ICORD
C***********************************************************************
C     Initialize
C***********************************************************************
      NUMPRO = NPROPN+14*NEQ+19+10+NDIM
      NI=NPROPN+3

      IF(.NOT.ALLOCATED(PROPERT))  ALLOCATE (PROPERT(NUMPRO,NVERT))
      IF(.NOT.ALLOCATED(SYMPROP)) ALLOCATE (SYMPROP(NUMPRO))
      IF(.NOT.ALLOCATED(NRM_SRC)) ALLOCATE (NRM_SRC(NVERT))
      IF(.NOT.ALLOCATED(NRM_DIFF)) ALLOCATE (NRM_DIFF(NVERT))
      IF(.NOT.ALLOCATED(NRM_PHI)) ALLOCATE (NRM_PHI(NVERT))
      IF(.NOT.ALLOCATED(SCPSTH)) ALLOCATE (SCPSTH(NVERT))
      IF(.NOT.ALLOCATED(ANG_SRC)) ALLOCATE (ANG_SRC(NVERT))
      IF(.NOT.ALLOCATED(ANG_DIFF)) ALLOCATE (ANG_DIFF(NVERT))
      IF(.NOT.ALLOCATED(ANG_PHI)) ALLOCATE (ANG_PHI(NVERT))
      IF(.NOT.ALLOCATED(ANGPSTH)) ALLOCATE (ANGPSTH(NVERT))
      IF(.NOT.ALLOCATED(NNORMVEC)) ALLOCATE (NNORMVEC(NVERT))
      IF(.NOT.ALLOCATED(NSRC)) ALLOCATE (NSRC(NVERT))
      IF(.NOT.ALLOCATED(NDIFFU)) ALLOCATE (NDIFFU(NVERT))
      IF(.NOT.ALLOCATED(NDIFFU2)) ALLOCATE (NDIFFU2(NVERT))
      IF(.NOT.ALLOCATED(NPHI)) ALLOCATE (NPHI(NVERT))
      IF(.NOT.ALLOCATED(NPSTH1)) ALLOCATE (NPSTH1(NVERT))
      IF(.NOT.ALLOCATED(NPSTH2)) ALLOCATE (NPSTH2(NVERT))
      IF(.NOT.ALLOCATED(NPSTT1)) ALLOCATE (NPSTT1(NVERT))
      IF(.NOT.ALLOCATED(NPSTT2)) ALLOCATE (NPSTT2(NVERT))
      IF(.NOT.ALLOCATED(NORMVEC))  ALLOCATE (NORMVEC(NEQ,NVERT))
!       IF(.NOT.ALLOCATED(PHI))  ALLOCATE (PHI(NEQ,NVERT))
      IF(.NOT.ALLOCATED(PSTH1))  ALLOCATE (PSTH1(NEQ,NVERT))
      IF(.NOT.ALLOCATED(PSTH2))  ALLOCATE (PSTH2(NEQ,NVERT))
C***********************************************************************
C     Calculate output variables
C***********************************************************************
      DO I=1,NVERT
      NORMVEC(:,I)=DYI(:,I)+DYE(:,I)
!       PHI(:,I)=SOURCE(:,I)+DIFFU(:,I)
      END DO
      DO I=1,NVERT
      NRM_SRC(I)= DOT_PRODUCT(NORMVEC(1:NEQ,I),
     1 MATMUL(SCMA(1:NEQ,1:NEQ),SOURCE(1:NEQ,I)))
      END DO

      DO I=1,NVERT
      NRM_DIFF(I)= DOT_PRODUCT(NORMVEC(1:NEQ,I),
     1 MATMUL(SCMA(1:NEQ,1:NEQ),DIFFU(1:NEQ,I)))
      END DO

      DO I=1,NVERT
      NRM_PHI(I)= DOT_PRODUCT(NORMVEC(1:NEQ,I),
     1 MATMUL(SCMA(1:NEQ,1:NEQ),PHI(1:NEQ,I)))
      END DO

      IF (NDIM.EQ.2)THEN
      DO I=1,NVERT
      SCPSTH(I)= DOT_PRODUCT(PSTH(1:NEQ,1,I),
     1 MATMUL(SCMA(1:NEQ,1:NEQ),PSTH(1:NEQ,2,I)))
      END DO
      END IF

      DO I=1,NVERT
      NNORMVEC(I)=SQRT(DOT_PRODUCT(NORMVEC(1:NEQ,I),
     1 MATMUL(SCMA(1:NEQ,1:NEQ),NORMVEC(1:NEQ,I))))
      IF (NNORMVEC(I) .LT. 1.0D-8) THEN
      NNORMVEC(I) = 1.0D-8
      END IF
      END DO
      DO I=1,NVERT
      NSRC(I)=SQRT(DOT_PRODUCT(SOURCE(1:NEQ,I),
     1 MATMUL(SCMA(1:NEQ,1:NEQ),SOURCE(1:NEQ,I))))
      IF (NSRC(I) .LT. 1.0D-8) THEN
      NSRC(I) = 1.0D-8
      END IF
      END DO
      DO I=1,NVERT
      NDIFFU(I)=SQRT(DOT_PRODUCT(DIFFU(1:NEQ,I),
     1 MATMUL(SCMA(1:NEQ,1:NEQ),DIFFU(1:NEQ,I))))
      IF (NDIFFU(I) .LT. 1.0D-8) THEN
      NDIFFU(I) = 1.0D-8
      END IF
      END DO

      DO I=1,NVERT
      NDIFFU2(I)=SQRT(DOT_PRODUCT(DIFFU2(1:NEQ,I),
     1 MATMUL(SCMA(1:NEQ,1:NEQ),DIFFU2(1:NEQ,I))))
      IF (NDIFFU2(I) .LT. 1.0D-8) THEN
      NDIFFU2(I) = 1.0D-8
      END IF
      END DO

      DO I=1,NVERT
      NPHI(I)=SQRT(DOT_PRODUCT(PHI(1:NEQ,I),
     1 MATMUL(SCMA(1:NEQ,1:NEQ),PHI(1:NEQ,I))))
      IF (NPHI(I) .LT. 1.0D-8) THEN
      NPHI(I) = 1.0D-8
      END IF
      END DO

      DO I=1,NVERT
      NPSTH1(I)=SQRT(DOT_PRODUCT(PSTH(1:NEQ,1,I),
     1 MATMUL(SCMA(1:NEQ,1:NEQ),PSTH(1:NEQ,1,I))))
      IF (NPSTH1(I) .LT. 1.0D-8) THEN
      NPSTH1(I) = 1.0D-8
      END IF
      END DO

      IF (NDIM.EQ.2)THEN
      DO I=1,NVERT
      NPSTH2(I)=SQRT(DOT_PRODUCT(PSTH(1:NEQ,2,I),
     1 MATMUL(SCMA(1:NEQ,1:NEQ),PSTH(1:NEQ,2,I))))
      IF (NPSTH2(I) .LT. 1.0D-8) THEN
      NPSTH2(I) = 1.0D-8
      END IF
      END DO
      ENDIF

      DO I=1,NVERT
      NPSTT1(I)=SQRT(DOT_PRODUCT(PSTT1(1:NEQ,I),
     1 MATMUL(SCMA(1:NEQ,1:NEQ),PSTT1(1:NEQ,I))))
      IF (NPSTT1(I) .LT. 1.0D-8) THEN
      NPSTT1(I) = 1.0D-8
      END IF
      END DO

      DO I=1,NVERT
      NPSTT2(I)=SQRT(DOT_PRODUCT(PSTT2(1:NEQ,I),
     1 MATMUL(SCMA(1:NEQ,1:NEQ),PSTT2(1:NEQ,I))))
      IF (NPSTT2(I) .LT. 1.0D-8) THEN
      NPSTT2(I) = 1.0D-8
      END IF
      END DO

      DO I=1,NVERT
      ANG_SRC(I)= NRM_SRC(I)/(NNORMVEC(I)*NSRC(I))
      END DO

      DO I=1,NVERT
      ANG_DIFF(I)= NRM_DIFF(I)/(NNORMVEC(I)*NDIFFU(I))
      END DO

      DO I=1,NVERT
      ANG_PHI(I)= NRM_PHI(I)/(NNORMVEC(I)*NPHI(I))
      END DO
      IF (NDIM.EQ.2)THEN
      DO I=1,NVERT
      ANGPSTH(I)= SCPSTH(I)/(NPSTH1(I)*NPSTH2(I))
      END DO
      ELSE
      ANGPSTH=0.0D0
      END IF
C***********************************************************************
C     Write header of fort.88
C***********************************************************************

      DO I=1,NPROPN
      WRITE(SYMPROP(I),'(A)')SYMPRP(I)
      END DO

      do i=1,2
      write(SYMPROP(NPROPN+i),90) i
      ENDDO
   90 FORMAT('gr_th',i3)
      write(SYMPROP(NPROPN+3),91)
   91 FORMAT('DETPSTH ')

      do i=1,NEQ
      write(SYMPROP(NPROPN+3+i),92) i
      ENDDO
   92 FORMAT('DYI  ',i3)

      do i=1,NEQ
      write(SYMPROP(NPROPN+3+NEQ+i),93) i
      ENDDO
   93 FORMAT('DYE  ',i3)

      do i=1,NEQ
      write(SYMPROP(NPROPN+3+2*NEQ+i),94) i
      ENDDO
   94 FORMAT('SOURCE',i2)

      do i=1,NEQ
      write(SYMPROP(NPROPN+3+3*NEQ+i),87) i
      ENDDO
   87 FORMAT('DIFFU',i3)

      do i=1,NEQ
      write(SYMPROP(NPROPN+3+4*NEQ+i),108) i
      ENDDO
  108 FORMAT('DIFFU2',i2)

      do i=1,NEQ
      write(SYMPROP(NPROPN+3+5*NEQ+i),88) i
      ENDDO
   88 FORMAT('PSTH1',i3)

      do i=1,NEQ
      write(SYMPROP(NPROPN+3+6*NEQ+i),89) i
      ENDDO
   89 FORMAT('PSTH2',i3)

      do i=1,NEQ
      write(SYMPROP(NPROPN+3+7*NEQ+i),112) i
      ENDDO
  112 FORMAT('PSTT1',i3)

      do i=1,NEQ
      write(SYMPROP(NPROPN+3+8*NEQ+i),113) i
      ENDDO
  113 FORMAT('PSTT2',i3)

      write(SYMPROP(NPROPN+9*NEQ+4),95)
   95 FORMAT('NRM_SRC ')
      write(SYMPROP(NPROPN+9*NEQ+5),96)
   96 FORMAT('NRM_DIFF')
      write(SYMPROP(NPROPN+9*NEQ+6),97)
   97 FORMAT('NRM_PHI ')
      write(SYMPROP(NPROPN+9*NEQ+7),98)
   98 FORMAT('NNORMVEC')
      write(SYMPROP(NPROPN+9*NEQ+8),99)
   99 FORMAT('NSRC    ')
      write(SYMPROP(NPROPN+9*NEQ+9),100)
  100 FORMAT('NDIFFU  ')
      write(SYMPROP(NPROPN+9*NEQ+10),109)
  109 FORMAT('NDIFFU2 ')
      write(SYMPROP(NPROPN+9*NEQ+11),101)
  101 FORMAT('NPHI    ')
      write(SYMPROP(NPROPN+9*NEQ+12),106)
  106 FORMAT('NPSTH1  ')
      write(SYMPROP(NPROPN+9*NEQ+13),107)
  107 FORMAT('NPSTH2  ')
      write(SYMPROP(NPROPN+9*NEQ+14),110)
  110 FORMAT('NPSTT1  ')
      write(SYMPROP(NPROPN+9*NEQ+15),111)
  111 FORMAT('NPSTT2  ')
      write(SYMPROP(NPROPN+9*NEQ+16),102)
  102 FORMAT('ANG_SRC ')
      write(SYMPROP(NPROPN+9*NEQ+17),103)
  103 FORMAT('ANG_DIFF')
      write(SYMPROP(NPROPN+9*NEQ+18),104)
  104 FORMAT('ANG_PHI ')
      write(SYMPROP(NPROPN+9*NEQ+19),105)
  105 FORMAT('ANGPSTH ')
      do i=1,6
      write(SYMPROP(NPROPN+9*NEQ+19+i),114) i
      ENDDO
  114 FORMAT('ELEM ',i3)
      do i=1,NEQ
      write(SYMPROP(NPROPN+9*NEQ+19+6+i),115) i
      ENDDO
  115 FORMAT('PHI',i3)
      do i=1,NEQ
      write(SYMPROP(NPROPN+10*NEQ+19+6+i),116) i
      ENDDO
  116 FORMAT('DY',i3)
      do i=1,NEQ
      write(SYMPROP(NPROPN+11*NEQ+19+6+i),117) i
      ENDDO
  117 FORMAT('GRPSI',i3)
      write(SYMPROP(NPROPN+12*NEQ+19+7),118)
  118 FORMAT('SUMW    ')
      write(SYMPROP(NPROPN+12*NEQ+19+8),119)
  119 FORMAT('flagdiff')
      write(SYMPROP(NPROPN+12*NEQ+19+9),120)
  120 FORMAT('flagchem')
      do i=1,NEQ
      write(SYMPROP(NPROPN+12*NEQ+19+9+i),121) i
      ENDDO
  121 FORMAT('PSTP1',i3)
      do i=1,NEQ
      write(SYMPROP(NPROPN+13*NEQ+19+9+i),122) i
      ENDDO
  122 FORMAT('PSTP2',i3)
      do i=1,ndim
      write(SYMPROP(NPROPN+14*NEQ+19+9+i),123) i
      ENDDO
  123 FORMAT('theta',i3)
      write(SYMPROP(NPROPN+14*NEQ+19+10+ndim),124)
  124 FORMAT('ENTROPY ')
C***********************************************************************
C     A.Neagos: Write additional variables
C               into internal file PROPERT.
C***********************************************************************
      DO I=1,NPROPN
      PROPERT(I,:)=PROPN(I,:)
      END DO
      DO I=1,NDIM
      PROPERT(NPROPN+I,:)=TTTL(I,:)
      END DO
      IF (NDIM.EQ.1)THEN
      PROPERT(NPROPN+2,:)=0.0D0
      END IF
      PROPERT(NPROPN+3,:)=DETPSITH(:)
!        WRITE(*,*)size(DIFFU)
      DO I=1,NEQ
      PROPERT(NI+I,:)=DYI(I,:)
      END DO
      DO I=1,NEQ
      PROPERT(NPROPN+NEQ+3+I,:)=DYE(I,:)
      END DO
      DO I=1,NEQ
      PROPERT(NPROPN+3+2*NEQ+I,:)=SOURCE(I,:)
      END DO
      DO I=1,NEQ
      PROPERT(NPROPN+3+3*NEQ+I,:)=DIFFU(I,:)
      END DO
      DO I=1,NEQ
      PROPERT(NPROPN+3+4*NEQ+I,:)=DIFFU2(I,:)
      END DO
      DO I=1,NEQ
      PROPERT(NPROPN+3+5*NEQ+I,:)=PSTH(I,1,:)
      END DO
      IF (NDIM.ge.2)THEN
      DO I=1,NEQ
      PROPERT(NPROPN+3+6*NEQ+I,:)=PSTH(I,2,:)
      END DO
      ELSE
      PROPERT(NPROPN+3+6*NEQ+I,:)=0.0D0
      END IF
      DO I=1,NEQ
      PROPERT(NPROPN+3+7*NEQ+I,:)=PSTT1(I,:)
      END DO
      DO I=1,NEQ
      PROPERT(NPROPN+3+8*NEQ+I,:)=PSTT2(I,:)
      END DO
      PROPERT(NPROPN+9*NEQ+4,:)= NRM_SRC(:)
      PROPERT(NPROPN+9*NEQ+5,:)= NRM_DIFF(:)
      PROPERT(NPROPN+9*NEQ+6,:)= NRM_PHI(:)
      PROPERT(NPROPN+9*NEQ+7,:)= NNORMVEC(:)
      PROPERT(NPROPN+9*NEQ+8,:)= NSRC(:)
      PROPERT(NPROPN+9*NEQ+9,:)= NDIFFU(:)
      PROPERT(NPROPN+9*NEQ+10,:)= NDIFFU2(:)
      PROPERT(NPROPN+9*NEQ+11,:)= NPHI(:)
      PROPERT(NPROPN+9*NEQ+12,:)= NPSTH1(:)
      PROPERT(NPROPN+9*NEQ+13,:)= NPSTH2(:)
      PROPERT(NPROPN+9*NEQ+14,:)= NPSTT1(:)
      PROPERT(NPROPN+9*NEQ+15,:)= NPSTT2(:)
      PROPERT(NPROPN+9*NEQ+16,:)= ANG_SRC(:)
      PROPERT(NPROPN+9*NEQ+17,:)= ANG_DIFF(:)
      PROPERT(NPROPN+9*NEQ+18,:)= ANG_PHI(:)
      PROPERT(NPROPN+9*NEQ+19,:)= ANGPSTH(:)
      DO I=1,6
      PROPERT(NPROPN+9*NEQ+19+I,:)=ELEMENT(I,:)
      END DO
      DO I=1,NEQ
      PROPERT(NPROPN+9*NEQ+19+6+I,:)=PHI(I,:)
      END DO
      DO I=1,NEQ
      PROPERT(NPROPN+10*NEQ+19+6+I,:)=NORMVEC(I,:)
      END DO
      DO I=1,NEQ
      PROPERT(NPROPN+11*NEQ+19+6+I,:)=XGRPSI(I,:)
      END DO
      PROPERT(NPROPN+12*NEQ+19+7,:)=SUMW
      PROPERT(NPROPN+12*NEQ+19+8,:)=flagdiff
      PROPERT(NPROPN+12*NEQ+19+9,:)=flagchem
      DO I=1,NEQ
      PROPERT(NPROPN+12*NEQ+19+9+I,:)=PSTHP(1,I,:)
      END DO
      DO I=1,NEQ
      if (ndim.eq.2)then
      PROPERT(NPROPN+13*NEQ+19+9+I,:)=PSTHP(2,I,:)
      else
      PROPERT(NPROPN+13*NEQ+19+9+I,:)=0.0d0
      end if
      END DO
      DO I=1,ndim
      PROPERT(NPROPN+14*NEQ+19+9+I,:)=theta(i,:)
      END DO
      PROPERT(NPROPN+14*NEQ+19+10+ndim,:)=ENTROPY
C***********************************************************************
C     A.Neagos: Write output of SYMPROP and PROPERT into fort.88 by
C               calling XTPOUT, subr. in evbres.f
C***********************************************************************
      NFINEA = 88
      OPEN(NFINEA,STATUS='UNKNOWN')
      CALL XTPOUT(NFINEA,NUMPRO,SYMPROP,NVERT,NUMPRO,PROPERT,
     1     ICORD,NDIM,N2DIM,NCELL,IVERT)
      CLOSE(NFINEA,STATUS='KEEP')
      END SUBROUTINE GRTHEOUT
