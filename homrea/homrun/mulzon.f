
      SUBROUTINE MULIN1(NFILE3,NFILE6,NOPT,NSYMB,MULTIZ,NS,XU,AFRAMU,
     1                  POI)
C**********************************************************************C
C     This programm read the input data from fort.3 needed for the     C
C     engine cycle computation.                                        C
C     Also check the input data of completeness.                       C
C                                                                      C
C**********************************************************************C
C**********************************************************************C
C     I. STORAGE ORGANIZATION                                          C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      CHARACTER*6 SYMB
      CHARACTER(LEN=*)  NSYMB(*)
      DIMENSION XU(MNSPE),POI(3,MNPOI),NOPT(*),AFRAMU(MNPOI)
      DOUBLE PRECISION, DIMENSION(MNZONE+1):: ALPHA
      COMMON/BSURR/TSURR,OVOL,ALPHA
      COMMON/MULINT/TR(MNPOI),VR(MNPOI),XR(MNZONE,MNSPE)
      PARAMETER (ZERO=0.0D0,ONE=1.0D0)
C**********************************************************************C 
      ALPHA(1:MULTIZ+1) = ZERO
      TR(1) = POI(3,1)
      XR(1,1:NS)=XU(1:NS)
      VR(1) = ONE
      IF(NOPT(32).GE.1) THEN	
        READ(NFILE3,800) SYMB
        IF(SYMB.NE.'MULTIZ') GOTO 910
        DO L=1,MULTIZ
          READ(NFILE3,800) SYMB
          IF(SYMB.NE.'REACTO') GOTO 910
          READ(NFILE3,801) TR(L)
          READ(NFILE3,802) (XR(L,I),I=1,NS)
        ENDDO
        READ(NFILE3,802) (VR(L),L=1,MULTIZ)
      ENDIF
      IF (NOPT(4).GE.1) THEN
        READ(NFILE3,802) (ALPHA(L),L=1,MULTIZ+1)
      ENDIF
      RETURN
C**********************************************************************C
C     VIII. FORMAT STATEMENTS                                          C
C**********************************************************************C
  800 FORMAT(1X,6A)
  801 FORMAT(12X,1PE11.4)
  802 FORMAT(12X,5(1PE11.4,1X))
C***********************************************************************
C     IV: ERROR EXITS
C***********************************************************************  
  920 WRITE(NFILE6,921)
  921 FORMAT(6X,'ERROR - INPUT DATA INCOMPLETE')
  910 CONTINUE
      WRITE(NFILE6,995) '          ERROR in -MULIN1-           '
      WRITE(NFILE6,995) '  CANNOT READ FROM FORT.3, CHECK IT   '
      STOP
  995 FORMAT(15('+'),4X,A38,4X,15('+'))
C***********************************************************************
C     END OF -MULIN1-
C***********************************************************************
      END

      SUBROUTINE MULZON(NCA,NEQ,NUZD,NSPEC,NSYMB,TIME,S,SP,F,RPAR,
     1                  IPAR,SMIN,SMAX,NTA,TSO,NFILE6,NFILE8,NFIL10,
     1                  KINTE,NSC,KONSPE,NSENS,CINT,IINT,LRW,
     1                  RWORK,LIW,IWORK,LSWORK,SWORK,LISWOR,ISWORK)
C***********************************************************************
C
C     .-----------------------------------------------------------.
C     |                                                           |
C     |     PROGRAM (INTEGRATION PART)  FOR THE SIMULATION OF     |
C     | CHEMICAL REACTION WITH VARIABLE TEMPERATURE AND PRESSURE  |
C     |                                                           |
C     '-----------------------------------------------------------'
C
C**********************************************************************C
C**********************************************************************C
C                                                                      C
C     A: STORAGE ORGANIZATION                                          C
C                                                                      C
C**********************************************************************C
C**********************************************************************C
C     I: TYPES OF VARIABLES
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      EXTERNAL MULRES,MULLEF,MULRIG
      LOGICAL TSO,TEST
      LOGICAL PCON,VCON,TCON,DETONA
      INTEGER NWALL,NCOMPLX
      COMMON/BOPTL/PCON,VCON,TCON,DETONA,NWALL,NCOMPLX,NPSR
      COMMON/LOCSM/NW,NWS,NU,NV,ND,NG,NP,NEM,NUZ,NSP
      CHARACTER(LEN=*)  NSYMB(*)
C**********************************************************************C
C     II: ARRAYS
C**********************************************************************C
      DIMENSION S(NEQ,NUZD),SP(NEQ,NUZD),F(NEQ,NUZD),SMIN(NEQ,NUZD),
     1        SMAX(NEQ,NUZD)
      DIMENSION KONSPE(NSC)
      DIMENSION RPAR(*),IPAR(*)
C---- WORKING ARRAYS
      DIMENSION RWORK(LRW),IWORK(LIW)
      DIMENSION SWORK(LSWORK),ISWORK(LISWOR)
C---- CONTROL PARAMETERS FOR SOLVERS
      DIMENSION ATOL(1),RTOL(1),ATOLS(1),RTOLS(1)
      DIMENSION INFO(15),IJOB(20),CINT(10),ICSEN(6)
C**********************************************************************C
C     IV: DATA STATEMENTS                                              C
C**********************************************************************C
      DATA ZERO/0.0D0/,ALMOST/0.999999999D0/
C***********************************************************************
C**********************************************************************C
C
C     A:  INITIALIZE
C
C***********************************************************************
      WRITE(NFILE6,8100)
 8100 FORMAT(1X,/,1X,79('*'),/,1X,4('*'),5X,
     1  ' SUBSYSTEM   INTEGRATION     (VERSION OF 09/01/2005)         ',
     1  5X,4('*'),/,1X,79('*'))
      NUZ=NUZD
C***********************************************************************
C     I. INITIALIZE
C***********************************************************************
      TBEG     = CINT(1)
      TEND     = CINT(2)
      TBEG     = CINT(3)
      TSTEPS   = CINT(4)
      ATOL(1)  = CINT(5)
      RTOL(1)  = CINT(6)
      ATOLS(1) = CINT(7)
      RTOLS(1) = CINT(8)
      STEP     = CINT(9)
C***********************************************************************
C     V: SET UP OUTPUT CONTROL PARAMETERS
C***********************************************************************
      IF(TEND-TBEG.LT.1.D-20) RETURN
      DT=(TEND-TBEG)/DABS(TSTEPS)
      NCOU=0
      NTA =0
      INCOU= -DMIN1(TSTEPS,0.D0) + 0.1D0
                       TOUT = TBEG
      IF(TBEG.EQ.ZERO) TOUT = TBEG + DT
      IF(TIME.GE.TOUT) TOUT = TIME
C**********************************************************************C
C 
C**********************************************************************C
                     TEST=.FALSE.
      IF(KINTE.GE.4) TEST=.TRUE.
C***********************************************************************
C     IV.: SET INITIAL PROFILES
C***********************************************************************
      ICSEN(1) = 0        
      CALL MULIVA(NEQ,NUZD,TIME,RPAR,IPAR,S,SP,F,NFILE6)
C***********************************************************************
C     Open Storage File 8 as scratch file
C***********************************************************************
      CALL OPNFIL(-1,NFILE8,'f8',IERR)
C***********************************************************************
C       INITIALIZE ARRAYS
C***********************************************************************
      SMIN = S
      SMAX = S
C***********************************************************************
C     VII: OUTPUT OF VALUES BEFORE BEGIN OF INTEGRATION
C***********************************************************************
      IF(TBEG.LT.1.0D-12) THEN
        NTA=NTA+1
        CALL MULOU1(NTA,NSPEC,NSYMB,TIME,NFILE6,NFILE8,S,NEQ,NUZ)
      ENDIF
C***********************************************************************
C     III.: SET INTEGRATION CONTROL PARAMETERS FOR LIMEX
C***********************************************************************
      ICALL    = 0
      IJOB(1:20) = 0
C---- block tridiagonal structure
C_Alex
      IF (NUZ.EQ.1) THEN  
        IJOB(3) = 0
      ELSE
        IJOB(3) = 2
      ENDIF
      IJOB(4) = NEQ
      IJOB(5) = NUZ
      NZV = 2*NUZ
      NZC = (NEQ-3) * NUZ
      NEQT = NEQ * NUZ
      IF(KINTE.GE.3) IJOB(7)=4
      H        = STEP
      HMAX     = 1.D10
C***********************************************************************
C     IV: SET INTEGRATION CONTROL PARAMETERS FOR DASSL
C***********************************************************************
      INFO(1:15)=0
      INFO(8) = 1
      INFO(3) = 1
C---- banded structure
C_Alex
      IF (NUZ.EQ.1) THEN  
        INFO(6) = 0
      ELSE
        INFO(6) = 1
      ENDIF
      IWORK(1) = 2*NEQ-1
      IWORK(2) = 2*NEQ-1
      RWORK(3) = STEP
 3000 CONTINUE
C**********************************************************************
C     CONTROL PARAMETERS FOR SENSITIVITY ANALYSIS
C**********************************************************************
      ICSEN(1:6)=0
C***********************************************************************
C     VIII: CALL INTEGRATOR AND CHECK RETURN CODE
C***********************************************************************
      IF(IINT.EQ.0) CALL UDASSL(MULRES,NEQT,TIME,S,SP,TEND,INFO,ICSEN,
     1      RTOL,ATOL,RTOLS,ATOLS,IDID,SWORK,LSWORK,
     1      ISWORK,LISWOR,RWORK,LRW,IWORK,LIW,RPAR,IPAR)
      IF(IINT.EQ.1)  CALL XLIMEX(NEQT,NZC,NZV,MULLEF,MULRIG,DUMJAC,
     1      TIME,S,SP,TEND,RTOL,ATOL,RTOLS(1),ATOLS(1),
     1      HMAX,H,IJOB,LRW,RWORK,LIW,IWORK,ICALL,RPAR,IPAR)
      ICALL=2
C***********************************************************************
C     IX: CHECK RETURN CODE AND MONITOR INTEGRATION
C***********************************************************************
      IF(IINT.EQ.1) IDID=IJOB(7)
      IF(IDID.LT.0) GOTO 5000
      IJOB(7)=0
      IF(KINTE.GE.3) IJOB(7)=4
      IF(KINTE.GE.3.AND.IINT.EQ.0)
     1    WRITE(NFILE6,7610) TIME,RWORK(3),RWORK(7),IWORK(14),IWORK(15)
C***********************************************************************
C       UPDATE MAXIMUM VALUES
C***********************************************************************
      SMIN = MIN(S,SMIN)
      SMAX = MAX(S,SMAX)
C***********************************************************************
C    XI: CONTROL OUTPUT
C***********************************************************************
C---- DECIDE IF OUTPUT IS DESIRED
      IF(TIME.GE.(ALMOST*TEND)) THEN
        ILAS=1
        GOTO 3299 
      ELSE
        ILAS = 0
      ENDIF
      IF(TSTEPS.GT.0.D0.AND.TIME.LT.(TOUT-1.D-14*TOUT)) GOTO 3500
          NCOU=NCOU+1
      IF(TSTEPS.LE.0.D0) THEN
        IF(TIME.LT.TBEG) THEN
          NCOU=0
          GOTO 3500
        ENDIF
        IF(NCOU.NE.INCOU) THEN
          GOTO 3500
        ELSE
          NCOU=0
        ENDIF
      ENDIF
 3299 CONTINUE
C---- INCREMENT NUMBER OF OUTPUT AND CALL OUTPUT
      NTA=NTA+1
      CALL MULOU1(NTA,NSPEC,NSYMB,TIME,NFILE6,NFILE8,S,NEQ,NUZ)
C***********************************************************************
C     DETERMINE NEXT TIME FOR OUTPUT
C***********************************************************************
 3300 CONTINUE
      TOUT=TOUT+DT
      IF(TIME.GE.TOUT) GOTO 3300
C***********************************************************************
C     TERMINATION OF LOOP
C***********************************************************************
 3500 CONTINUE
C---- LEAVE LOOP IF TIME = TEND
      IF(TIME.GE.ALMOST*TEND) GO TO 5000
      GO TO 3000
C***********************************************************************
C***********************************************************************
C
C     D: FINAL OUTPUT
C
C***********************************************************************
C***********************************************************************
 5000 CONTINUE
C***********************************************************************
C     XII: OUTPUT OF TABLES AND PLOTS
C***********************************************************************
      N1 = 1
      N2 = N1 + (NEQ + 2)
      N3 = N2 + (NEQ + 2)
      N4 = N3 + (NTA * NUZ)
      N5 = N4 + (NTA * NUZ)
      N6 = N5 + (NTA * NUZ)
      N7 = N6 + (NTA * NUZ)
      N8 = N7 + (NTA * NUZ) *NEQ
      N9 = N8 + NTA*NEQ
      NRWA=N9-1
      IF(NRWA.GT.LRW) GOTO 930
      NDIM = NEQ*NUZ 
CCHRIS
      CALL MULOU2(S,NCA,NSPEC,NDIM,NEQ,NSYMB,NFILE8,NFILE6,TSO,
     1  NTA,RWORK(N1),RWORK(N2),RWORK(N3),RWORK(N4),RWORK(N5),RWORK(N6),
     1  RWORK(N7),RWORK(N8),SMIN,SMAX,RPAR,IPAR,NUZ)
CHRISEND
C***********************************************************************
C     Close Storage file
C***********************************************************************
      CALL CLSFIL(NFILE8)
C***********************************************************************
C     XII: OUTPUT OF IGNITION DELAY TIME
C***********************************************************************
C***********************************************************************
C          PERFORM ANALYSIS OF THE REACTION MECHANISM
C***********************************************************************
      RETURN
C***********************************************************************
C***********************************************************************
C
C     E: ERROR EXITS
C
C***********************************************************************
C***********************************************************************
  930 CONTINUE
      WRITE(NFILE6,931) NRWA,LRW
  931 FORMAT('1',1X,'ERROR - NEEDED REAL WORK SPACE (',I7,') > ',
     1              'SUPPLIED REAL WORK SPACE(',I7,') IN TRAJ')
      GOTO 9999
 9900 CONTINUE
      WRITE(NFILE6,9901) IDID
 9901 FORMAT('1',5X,'ERROR - IDID =',I3,' RETURN CODE OF DASSL/LIMEX')
      GOTO 9999
 9999 CONTINUE
 9998 FORMAT('0',32('+'),' ABNORMAL END ',32('+'))
      WRITE(NFILE6,9998)
      WRITE(NFILE6,9998)
      WRITE(NFILE6,9998)
      WRITE(NFILE6,8010)
      WRITE(NFILE6,8010)
      STOP
C***********************************************************************
C***********************************************************************
C
C     F: FORMAT STATEMENTS
C
C***********************************************************************
C***********************************************************************
 8010 FORMAT('0',2X)
C 829 FORMAT('0',79('*'))
C1829 FORMAT('0',132('*'))
 7610 FORMAT(
     1 '   TIME                                            ',1PE12.5,/,
     1 '   STEP SIZE H TO BE ATTEMPTED ON THE NEXT STEP :  ',1PE12.5,/,
     1 '   STEPSIZE USED ON THE LAST SUCCESSFUL STEP :     ',1PE12.5,/,
     1 '   TOTAL NUMBER OF ERROR TEST FAILURES SO FAR :    ',8X,I4,/,
     1 '   TOTAL NUMBER OF CONVERGENCE TEST FAILURES SO FAR :',6X,I4)
C***********************************************************************
C     END OF -MULZON-
C***********************************************************************
      END

      SUBROUTINE HORMUC(NE,NS,NSYMB,NEQ,MULTIZ,NFILE6,NOPT,TIME,S,CINT)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C     INPUT DATA IN SI UNITS (IF NOT SPECIFIED)
C
C***********************************************************************
C     CHAPTER I: STORAGE ORGANIZATION, INITIAL VALUES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      CHARACTER(LEN=*) NSYMB(*)
      PARAMETER (NWA = 5)
      LOGICAL TEST,DETONA,PCON,VCON,TCON
      INTEGER NWALL,NCOMPLX
      DOUBLE PRECISION, DIMENSION(NS)                    :: CPI,HMOL
      DOUBLE PRECISION, DIMENSION(NS+NWA,MNZONE)         :: S
      DOUBLE PRECISION, DIMENSION(MNZONE)                :: VI0
      COMMON/BOPTL/PCON,VCON,TCON,DETONA,NWALL,NCOMPLX,NPSR
CASSA
      COMMON/SPOPT1/NEXCET,NENGIN
      COMMON/MULINT/TR(MNPOI),VR(MNPOI),XR(MNZONE,MNSPE)
      DIMENSION H0(MNZONE),XMOLM(MNZONE),C0(MNZONE),RHO(MNZONE)
      DIMENSION U0(MNZONE),W0(MNZONE,NS),HMOLL(MNZONE,NS)
CASSA
      COMMON/BDIM/NSMAX,NRMAX,NCRMAX,NPOMAX,NELMAX
      COMMON/BINIT/P0,T,V0,H,XU(MNSPE)
      COMMON/BHCP/HLHH(7,MNSPE*2),HINF(3,MNSPE)
      COMMON/LOCSM/NW,NWS,NU,NV,ND,NG,NP,NEM,NUZ,NSP
      DIMENSION NOPT(*),CINT(10)
      DATA ZERO/0.0D0/,RGAS/8.3143D0/
C**********************************************************************
C     CHAPTER II: INPUT OF DATA
C**********************************************************************
      IF(TCON) GOTO 910
      TEST=.FALSE.
      NUZ = MULTIZ
      IF(NUZ.GT.MNZONE) GOTO 912
      NSP = NS
C***********************************************************************
C     CALCULATE VOLUME OF THE ENGINE COMBUSTION CHAMBER;               *
C     (A.Schubert 10/2005)                                             *
C***********************************************************************
      IF(NENGIN .GT. 0) THEN
        CALL CALVOL(NFILE6,TIME,V0,VPR)
      ENDIF 
C***********************************************************************
C     CHAPTER III.: SET INITIAL PROFILES FOR VARIABLES
C***********************************************************************      
      DO L=1,NUZ
        T0 = TR(L)
C---- mole fractions
        DO J=1,NS
          XU(J) = XR(L,J)
        ENDDO
C---- mean molar mass
        XMOLM(L) = DOT_PRODUCT(XMOL(1:NS),XU(1:NS))
C---- CALCULATE NEW MASS FRACTION AND ENTHALPY FOR ENGINE AND EGR
C---- total concentration
        C0(L) = P0/(RGAS*T0)
C---- density
        RHO(L) = C0(L) * XMOLM(L)
C---- calculate molar enthalpy
        CALL CALHCP(NS,XU,T0,HLHH,HINF,CPI,CPTOT,HMOL,IEHCP)
C---- specific enthalpy
        H0(L) = DOT_PRODUCT(XU(1:NS),HMOL(1:NS)) / XMOLM(L)
C---- specific internal energy
        U0(L) = H0(L) - P0 / RHO(L)
C---- mass fractions
        W0(L,1:NS) = XU(1:NS) * XMOL(1:NS) / XMOLM(L)
      ENDDO
C---- Volumes
      DO L=1,NUZ
        VI0(L)=VR(L)*V0
      ENDDO
C***********************************************************************
C     set up locations 
C***********************************************************************
      NW  = 1
      NWS = 0
      NU  = NS + NW
      NV  = NU + 1
      ND  = NV + 1
      NG  = ND + 1
      NP  = NG + 1
      NEQ = NP
      NEM = NEQ
C***********************************************************************
C     CHECK LENGTH OF SOLUTION VECTOR
C***********************************************************************
      IF ((NEQ*NUZ) .GT. (MNEQU*(MNSEN+1))) GOTO 915
C***********************************************************************
C     store in S
C***********************************************************************
      GADD      = ZERO
      DO L=1,NUZ
        S(1:NS,L) = W0(L,1:NS)
        S(NU,L)   = U0(L)
        S(NV,L)   = VI0(L)
        S(ND,L)   = RHO(L)
        GADD      = GADD + RHO(L)*S(NV,L)*RGAS*TR(L)/XMOLM(L)
        S(NG,L)   = GADD
        S(NP,L)   = P0
      ENDDO
C***********************************************************************
C     Regular exit
C***********************************************************************
  100 CONTINUE
      RETURN
C***********************************************************************
C     Error exits 
C***********************************************************************
  910 CONTINUE
      WRITE(NFILE6,911)
  911 FORMAT(6X,'ERROR - Multi zone model not available for given',
     1       ' temperature profile')
      GOTO 999
  912 CONTINUE
      WRITE(NFILE6,913) 'INCREASE DIMENSION OF MNAUTO IN "HOMDIM.f"'
      WRITE(NFILE6,914) 'NUZ =',NUZ,'GREATER THAN MNZONE = ',MNZONE
  913 FORMAT(13('+'),5X,A42,5X,13('+'))
  914 FORMAT(13('+'),8X,A5,1X,I4,1X,A21,1X,I4,7X,13('+'))
      GOTO 999
 915  CONTINUE
      WRITE(NFILE6,913) 'DIMENSION OF SOLUTION VECTOR IS TOO SMALL'
      WRITE(NFILE6,913) 'INCREASE MNSEN '
  999 CONTINUE
      WRITE(NFILE6,998)
  998 FORMAT(3('   ++++++      ERROR IN HORMUC      ++++++   ',/))
      STOP
C***********************************************************************
C     END OF -HORMUC-
C***********************************************************************
      END

      SUBROUTINE MULIVA(NEQ,NUZD,TIME,RPAR,IPAR,S,SP,F,NFILE6)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *      SET UP INITIAL VALUES                                *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C     NEQ           = NUMBER OF DEPENDENT VARIABLES
C     NUZD          = number of reactors
C     TIME          = ACTUAL TIME
C
C     OUTPUT :
C
C     S(I,1)        = INITIAL VALUES OF DEPENDENT VARIABLES
C     SP( )         = INITIAL VALUES FOR FIRST DERIVATIVES
C
C
C***********************************************************************
C 
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      DIMENSION RPAR(*),IPAR(*)
      DOUBLE PRECISION, DIMENSION(NEQ,NUZD)   ::S,SP,F
      DOUBLE PRECISION, DIMENSION(1) :: DXDS,DTDS,DPDS
C***********************************************************************
C     CHAPTER V.: MAKE INITIAL VALUES FOR VARIABLES CONSISTENT
C                 AND INITIALIZE REACTION RATES
C***********************************************************************
      SP = 0
      ICRES=0
      NSPEC = IPAR(3)
      CALL MULDES(NEQ,NUZD,NSPEC,TIME,S,F,RPAR,IPAR)
      SP=F
C***********************************************************************
C     CHAPTER VI.: CONS. OF INIT. VAL OF "INIT. VALUE. SENSITIVITIES"
C***********************************************************************
      RETURN
C***********************************************************************
C     END OF -MULIVA-
C***********************************************************************
      END

      SUBROUTINE MULRES(TIME,S,SP,F,IRES,RPAR,IPAR)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *            INTERFACE FOR CONSERVATION EQUATIONS           *
C     *              (ADAPTED FOR SUBROUTINE -DASSL-)             *
C     *                                                           *
C     *               U.MAAS                                      *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C     INPUT/OUTPUT: SEE SUBROUTINE -DASSL-
C
C***********************************************************************
C***********************************************************************
C     CHAPTERI: STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      COMMON/LOCSM/NW,NWS,NU,NV,ND,NG,NP,NEM,NUZ,NSP
      DIMENSION S(*),SP(*),F(*),RPAR(*),IPAR(*)
      DOUBLE PRECISION, DIMENSION(2*NUZ)     :: BV
      INTEGER,          DIMENSION(2*NUZ)     :: IR,IC
C***********************************************************************
C     CHAPTER II: CALL OF -MULDES-
C***********************************************************************
      NSPEC = IPAR(3)
      CALL MULDES(NEM,NUZ,NSPEC,TIME,S,F,RPAR,IPAR)
      DO L=1,NUZ
        IAD = (L-1)*NEM
        F(IAD+NV) = SP(IAD+NV) - F(IAD+NV)
        F(IAD+NU) = SP(IAD+NU) - F(IAD+NU) 
        DO I=1,NSP
          F(IAD+NWS+I) = SP(IAD+NWS+I) - F(IAD+NWS+I)
        ENDDO
      ENDDO
C***********************************************************************
C     add variable entries
C***********************************************************************
      NZZ=2*NUZ
      CALL MULBV(NZZ,NEM,NUZ,S,BV,IR,IC)
      DO I=1,NZZ
        F(IR(I)) = F(IR(I)) + SP(IC(I)) * BV(I)
      ENDDO
      RETURN
C***********************************************************************
C     END OF -MULRES-
C***********************************************************************
      END

      SUBROUTINE MULRIG(NEQNEQ,NZV,TIME,S,F,BV,IR,IC,RPAR,IPAR,IFC,IFP)
C***********************************************************************
C
C     DEFINITION OF RIGHT HAND SIDE (SEE -ULIMEX-)
C
C***********************************************************************
C***********************************************************************
C     I.I.: TYPES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      COMMON/LOCSM/NW,NWS,NU,NV,ND,NG,NP,NEM,NUZ,NSP
      DIMENSION S(*),F(*),BV(*),IR(*),IC(*)
      DIMENSION RPAR(*),IPAR(*)
      DIMENSION IFP(*)
C***********************************************************************
C     CALL OF -MULDES-
C***********************************************************************
      NSPEC = IPAR(3)
      CALL MULDES(NEM,NUZ,NSPEC,TIME,S,F,RPAR,IPAR)
      CALL MULBV(NZV,NEM,NUZ,S,BV,IR,IC)
      RETURN
C***********************************************************************
C     END OF -MULRIG-
C***********************************************************************
      END

      SUBROUTINE MULLEF (NZC,B,IR,IC)
C***********************************************************************
C
C     DEFINITION OF LEFT HAND SIDE (SEE -ULIMEX-)
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      COMMON/LOCSM/NW,NWS,NU,NV,ND,NG,NP,NEM,NUZ,NSP
      DIMENSION B(*),IR(*),IC(*)
      DATA ONE/1.0D0/
C***********************************************************************
C
C***********************************************************************
      LAUF = 0
      DO L=1,NUZ
        NADD = NEM*(L-1)
        LAUF = LAUF + 1
        IR(LAUF) = NADD + NV
        IC(LAUF) = NADD + NV
        LAUF = LAUF + 1
        IR(LAUF) = NADD + NU
        IC(LAUF) = NADD + NU
        DO I=1,NSP
          LAUF = LAUF + 1
          IR(LAUF) = NADD + NWS + I
          IC(LAUF) = NADD + NWS + I
        ENDDO
      ENDDO
      DO I=1,NZC
        B(I)= ONE
      ENDDO
        IF(LAUF.NE.NZC) THEN
           WRITE(*,*) 'Schwachkopf MULLEF',LAUF,NZC
           STOP
        ENDIF
      RETURN
C***********************************************************************
C     END OF -MULLEF-
C***********************************************************************
      END

      SUBROUTINE MULDES(NEMD,NUZD,NSPEC,TIME,S,F,RPAR,IPAR)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *           RESIDUALS OF THE CONSERVATION EQUATIONS         *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C                                                                      *
C     INPUT/OUTPUT: SEE  -DASSL AND LIMEX -                            *
C                                                                      *
C***********************************************************************
C                                                                      *
C***********************************************************************
C     I.I.: TYPES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      LOGICAL PCON,VCON,TEST,STOP,TCON,DETONA
      INTEGER NWALL,NCOMPLX
C***********************************************************************
C     I.II.: DIMENSIONS
C***********************************************************************
      DIMENSION S(NEMD,NUZD),F(NEMD,NUZD),RPAR(*),IPAR(*)
C**********************************************************************C
C     I.IV.: NON ARRAY OR FIXED ARRAY COMMON BLOCKS
C**********************************************************************C
      COMMON/BOPTL/PCON,VCON,TCON,DETONA,NWALL,NCOMPLX,NPSR
      COMMON/SPOPT1/NEXCET,NENGIN
      COMMON/BSURR/TSURR,OVOL,ALPHA
      COMMON/LOCSM/NW,NWS,NU,NV,ND,NG,NP,NEM,NUZ,NSP
C***********************************************************************
C     BLOCK FOR INFORMATION ABOUT THE REACTION MECHANISM
C***********************************************************************
      COMMON/BICRES/ICRES
      DIMENSION WORK1(MNREA)
      DOUBLE PRECISION, DIMENSION(NSPEC+2)   :: SMF,SMFP
      DOUBLE PRECISION, DIMENSION(0:NUZD+1)  :: TLL
      DOUBLE PRECISION, DIMENSION(MNZONE+1):: ALPHA
      DOUBLE PRECISION, DIMENSION(NUZ)     :: VL,RHOM 
C***********************************************************************
      DATA NFILE6/06/
      DATA LRW1/MNREA/
C**********************************************************************C
C     I.V.: DATA STATEMENTS
C**********************************************************************C
      DATA ZERO/0.0D0/,ONE/1.0D0/,RGAS/8.3143D0/
C***********************************************************************
C
C     CHAPTER II: INITIALIZATION                                       *
C
C***********************************************************************
      STOP=.TRUE.
      TEST=.FALSE.
C***********************************************************************
C                                                                      *
C     CHAPTER II: CALCULATION OF FUNCTION                              *
C                                                                      *
C***********************************************************************
C---- VOLUME
      IF(NENGIN .EQ. 0) THEN
         CALL HORVAR(NFILE6,2,TIME,VGES,VPR)
      ELSE
        CALL CALVOL(NFILE6,TIME,VGES,VPR)
      ENDIF
C***********************************************************************
C
C***********************************************************************
      RHOM = S(ND,1:NUZ)
      VL   = S(NV,1:NUZ)
      DO L=1,NUZ
        RHOL = S(ND,L)
        USP  = S(NU,L)
        VG   = S(NG,L)
        PL   = S(NP,L)
C***********************************************************************
C     species mass fraction residuals
C***********************************************************************
        SMF(1)         = USP  + PL / RHOL
        SMF(2)         = RHOL
        SMF(3:NSPEC+2) = S(NW:NWS+NSPEC,L)
        CALL MULORE(NSPEC,NEMD,TIME,SMF,SMFP,XMOLML,TL,RPAR,IPAR,IERR,
     1              NFILE6)
        TLL(L) = TL
C***********************************************************************
C
C***********************************************************************
        XNL = (RHOL/XMOLML) * VL(L) * RGAS * TL
C---- XMCHA  CHANGE OF MASS of reactor per time 
        XMCHA  = ZERO
C---- volume
        F(NV,L) = XMCHA / RHOL
C---- energy
        F(NU,L) = ZERO
C---- species
        F(NW:NWS+NSPEC,L) = SMFP(3:NSPEC+2)
C---- density (equation of state)
        F(ND,L) = RHOL - XMOLML * PL / (RGAS * TL)
C---- equation to obtain pressure equation
        IF(L.EQ.1) THEN
          F(NG,L) = VG - XNL
        ELSE
          F(NG,L) = VG - (S(NG,L-1) + XNL)
        ENDIF
        IF(L.EQ.NUZ) THEN
          F(NP,L) = VG / VGES - S(NP,L)
        ELSE
          F(NP,L) = S(NP,L+1) - S(NP,L)
        ENDIF
      ENDDO
C***********************************************************************
C
C***********************************************************************
      TLL(0)      = TSURR
      TLL(NUZ+1)  = TSURR
C---- QCHA  CHANGE OF heat per mass per time
C SET ALPHA FOR THE WALLS
      DO L=1,NUZ
C SET ALPHA(NR.Z) TO A CONSTANT AS LONG AS IT DOESN'T COME FROM INPUT
        QCHA = ZERO
        IF(NWALL.GT.0) THEN
          CALL CALQCH(NWALL,NENGIN,TIME,QCHA,TLL,TSURR,OVOL,
     1              S(NP,L),VL,L,NUZ,ALPHA,RHOM)
        ENDIF
        F(NU,L) = F(NU,L) + QCHA
      ENDDO
C***********************************************************************
C     CHAPTER III: DEFINITION OF THE EQUATIONS
C***********************************************************************
      RETURN
C***********************************************************************
C     END OF -MULDES-
C***********************************************************************
      END

      SUBROUTINE MULORE(NSPEC,NEQ,TIME,Y,F,XMOLM,TL,RPAR,IPAR,IERR,
     1                  NFILE6)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *           RESIDUALS OF THE CONSERVATION EQUATIONS         *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C***********************************************************************
C
C     CHAPTER I: STORAGE ORGANIZATION                                  *
C
C***********************************************************************
C***********************************************************************
C     I.I.: TYPES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      LOGICAL DETONA,PCON,VCON,TEST,TCON
      INTEGER NWALL,NCOMPLX
C***********************************************************************
C     I.II.: DIMENSIONS
C***********************************************************************
      DIMENSION Y(NSPEC+2),F(NSPEC+2),RPAR(*),IPAR(*)
      DIMENSION CIL(MNSPE+MNTHB+1)
      DIMENSION DGDC(1),DGDT(1),DODC(1),DODT(1)
      DOUBLE PRECISION, DIMENSION(1) :: DXDS,DTDS,DPDS
C**********************************************************************C
C     I.III.: ARRAY COMMON BLOCKS
C**********************************************************************C
C**********************************************************************C
C     I.IV.: NON ARRAY OR FIXED ARRAY COMMON BLOCKS
C**********************************************************************C
      COMMON/LOCSM/NW,NWS,NU,NV,ND,NG,NP,NEM,NUZ,NSP
      COMMON/BOPTL/PCON,VCON,TCON,DETONA,NWALL,NCOMPLX,NPSR
      COMMON/BMIX/OMEGA,SMFMIX(MNEQU),SMFCHA(MNEQU),VDIR(MNEQU,NDIMM)
      COMMON/BSSPV/SSPV(20*MNSPE)
      COMMON/BHCP/HLHH(7,MNSPE*2),HINF(3,MNSPE)
C***********************************************************************
C     BLOCK FOR INFORMATIONS ABOUT THE REACTION MECHANISM
C***********************************************************************
      COMMON/BSENS/LSMAX,NSENS,NRSENS,NISENS,NPSENS,
     1       IRSENS,IISENS,IPSENS,ISENSI(MNSEN)
      DIMENSION WORK1(MNREA)
      DATA LRW1/MNREA/
C**********************************************************************C
C     I.V.: DATA STATEMENTS
C**********************************************************************C
      DATA ZERO/0.0D0/
C***********************************************************************
C                                                                      *
C     CHAPTER II: CALCULATION OF FUNCTION                              *
C                                                                      *
C***********************************************************************
      IERR = 0
      TEST = .FALSE.
C***********************************************************************
C     II.2.1: CALC. MASS FRACTION, TEMPERATURE, PRESSURE, VOLUME       *
C***********************************************************************
      NSPEC = IPAR(3)
      NE    = IPAR(6)
C***********************************************************************
C     CALCULATE MEAN MOLAR MASS AND MOLE FRACTIONS                     *
C***********************************************************************
      IF(TCON)      GOTO 999     
      CALL TRASSP(4,NEQ,Y,NSPEC,TL,CL,CIL,RHO,HSPEC,PL,SSPV,IEX,
     1      0,DXDS,1,DTDS,DPDS)

      IF(IEX.GT.0) GOTO 910
      XMOLM = RHO/CL
C***********************************************************************
C     II.2.4: CALCULATON OF MOLAR REACTION RATES                       *
C***********************************************************************
      IPRIM = 0
      CALL MECHJA(0,NSPEC,NREAC,NM,NRINS,NRTAB,
     1  IREAC(NMATLL),IREAC(NNRVEC),RREAC(NPK),RREAC(NTEXP),RREAC(NEXA),
     1  RREAC(NVT4),RREAC(NVT5),RREAC(NVT6),IREAC(NIARRH),
     1  RREAC(NXZSTO),RREAC(NRATCO),
     1  CL,CIL,TL,F(3),NRSENS,ISENSI(IRSENS),RPAR(IRSENS),
     1  LRW1,WORK1,IPRIM,NFILE6,IERR,
     1  RREAC(NAINF),RREAC(NBINF),RREAC(NEINF),RREAC(NT3),
     1  RREAC(NT1),RREAC(NT2),RREAC(NAFA),RREAC(NBFA),HLHH,HINF,
     1     1,1,DGDC,DGDT,DODC,DODT)
C***********************************************************************
C     III.2: P GIVEN
C***********************************************************************
      F(3:2+NSPEC)=XMOL(1:NSPEC)*F(3:2+NSPEC)/RHO
      F(1)= ZERO
      F(2)= ZERO
C***********************************************************************
C     REGULAR EXIT
C***********************************************************************
  490 CONTINUE
      RETURN
C***********************************************************************
C     Abnormal End
C***********************************************************************
  910 CONTINUE
      WRITE(6,*) ' IEX',IEX
      IERR = 1
      RETURN
 2221 FORMAT('0',2X)
  999 CONTINUE
 2222 WRITE(NFILE6,2221)
 2223 FORMAT('0',32('+'),' ABNORMAL END ',32('+'))
      WRITE(NFILE6,2223)
      WRITE(NFILE6,2223)
      WRITE(NFILE6,2223)
      WRITE(NFILE6,2221)
      WRITE(NFILE6,2221)
      STOP
C***********************************************************************
C     END OF -MULORE-
C***********************************************************************
      END

      SUBROUTINE MULBV(NZ,NEMT,NUZT,S,B,IR,IC)
C***********************************************************************
C
C     DEFINITION OF BV
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      COMMON/LOCSM/NW,NWS,NU,NV,ND,NG,NP,NEM,NUZ,NSP
      DIMENSION S(NEMT,NUZT),B(*),IR(*),IC(*)
C***********************************************************************
C
C***********************************************************************
      I= 0
      DO L=1,NUZ
        IAD = NEM*(L-1)
        I=I+1
        IR(I)=NV+IAD
        IC(I)=ND+IAD
        B(I)=S(NV,L)/S(ND,L)
        I=I+1
        IR(I)=NU+IAD
        IC(I)=ND+IAD
        B(I)=-S(NP,L)/S(ND,L)**2
      ENDDO 
      IF(I.NE.NZ) THEN
         WRITE(*,*) 'Schwachkopf MULBV'
         STOP
      ENDIF
      RETURN
C***********************************************************************
C     END OF -MULBV-
C***********************************************************************
      END

      SUBROUTINE MULOU1(NTA,NSPEC,NSYMB,TIME,NFILE6,NFILE8,S,NEQ,NUR)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *    PROGRAM FOR THE PRINTOUT OF RESULTS AT THE END OF A    *    *
C     *                       TIME STEP                           *    *
C     *           LAST CHANGE:  OCT , 2005 / SCHUBERT             *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C                                                                      *
C
C***********************************************************************
C
C
C
C***********************************************************************
C     I: STORAGE
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      COMMON/BSSPV/SSPV(20*MNSPE)
      LOGICAL MOLEFR,MASSFR,LPLO
      CHARACTER(LEN=*) NSYMB(*)
      DIMENSION S(NEQ,NUR)
      DIMENSION PML(NTA,NUR),VML(NTA,NUR),TEMPS(NTA,NUR),CML(NTA,NUR),
     1          HML(NTA,NUR)
      DIMENSION TM(NTA)
      COMMON/BOUT/KONINT,KONTAB,KONPLO,KONSEN,KONMEC,KONANA,KONRED,
     1  KONSPE(MNSPE),KONSID(MNSPE),NSC,
     1  ICRIT,KOGSEN,KONRST,MOLEFR,MASSFR,LPLO
      COMMON/LOCSM/NW,NWS,NU,NV,ND,NG,NP,NEM,NUZ,NSP
      COMMON/SPOPT1/NEXCET,NENGIN
      DOUBLE PRECISION, DIMENSION(NSPEC)             :: CI
      DOUBLE PRECISION, DIMENSION(NSPEC+2)           :: SMF
      DOUBLE PRECISION, DIMENSION(1) :: DXDS,DTDS,DPDS
C***********************************************************************
C     II: CHECK FOR OUTPUT
C***********************************************************************
      IF(NTA.EQ.0) RETURN
C***********************************************************************
C     III: DETERMINE T, P, AND TOTAL C
C***********************************************************************
C_ASchubert 21.10.2005
      IF(NENGIN.GE.1) THEN
        CALL ENGOUT(NENGIN,NFILE6)
      ENDIF
C_Aend 
      WRITE(NFILE6,810) TIME,SUM(S(NV,1:NUR))
      DO L=1,NUR   
        NSP2 = NSPEC + 2
        RHOL= S(ND,L)
        PL  = S(NP,L)
        VL  = S(NV,L)
        HL  = S(NU,L) + PL/RHOL
        SMF(1)         = HL
        SMF(2)         = RHOL
        SMF(3:2+NSPEC) = S(NW:NWS+NSPEC,L)
        CALL TRASSP(4,NSP2,SMF,NSPEC,T,CTOT,CI,RHODUM,H,P,
     1       SSPV,IERR,0,DXDS,1,DTDS,DPDS)
        PBA=1.0D-5*PL
        TEMPS(NTA,L) = T
        PML(NTA,L) = S(NP,L)*1.0D-5
        CML(NTA,L) = CTOT
        VML(NTA,L) = S(NV,L)
        HML(NTA,L) = HL
C***********************************************************************
C     IV: OUTPUT OF INTERMEDIATE RESULTS (C, T) ON NFILE6
C***********************************************************************
C---- NO OUTPUT FOR OPTION -SHORT-
        IF(KONINT.NE.0) THEN
          WRITE(NFILE6,811) L
          CALL UPDSPI(KONINT,NSC,KONSPE,NSCXXX,KONSID,NSPEC)
          NSCXXX=NSCXXX-1
C---- WRITE RESULTS
          WRITE(NFILE6,817) PBA,T,VL,HL,CTOT,
     1       (NSYMB(KONSID(I)),S(KONSID(I),L),I=1,NSCXXX) 
        ENDIF
      ENDDO
C***********************************************************************
C     V: OUTPUT ON NFILE8 (SOLUTION AND LOCAL SENSITIVITIES)
C***********************************************************************
      IF(NTA.EQ.1) THEN
        OPEN(UNIT=NFILE8,FILE='fort.8')
        REWIND NFILE8
      ENDIF
      TM(NTA) = TIME
      WRITE(NFILE8,820) NTA
      DO L=1,NUR
        WRITE(NFILE8,822)TM(NTA),TEMPS(NTA,L),PML(NTA,L),CML(NTA,L),
     1                  VML(NTA,L),HML(NTA,L)
        WRITE(NFILE8,823)S(1:NEQ,L)
      ENDDO
      RETURN
C***********************************************************************
C     VI: FORMAT STATEMENTS
C***********************************************************************
  810 FORMAT(1X,79('*'),/,' Results at t =',1PE9.2,
     1  ' s, overall volume = ',1PE9.2,', mass fractions used',/,
     1  1X,79('*'))
  811 FORMAT(1X,79('-'),/,' Reactor',I3)
  817 FORMAT(1X,'P =',1PE9.2,' bar, T =',1PE9.2,' K, ',
     1       'V =',1PE9.2,', H =',1PE9.2,' J/kg, c =',1PE9.2,/,
     1       1(3(1X,A,':',1PE9.2,1X),1X,A,':',1PE9.2))
C  818 FORMAT(1X,1PE9.2,3X,1PE9.2,1X)
  820 FORMAT('>TIM',I3)
  822 FORMAT(6(1X,1PE12.5))
  823 FORMAT(6(1X,1PE12.5))
C***********************************************************************
C   END OF -MULOU1-
C***********************************************************************
      END

Cchris
      SUBROUTINE MULOU2(S,NCA,NSPEC,NDIM,NEQ,NSYMB,NFILE8,NFILE6,TSO,
     1     NTS,HELP,HL,T,TEMP,CTOT,PTOT,V,VP,SMIN,SMAX,RPAR,IPAR,NUR)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *         PROGRAM FOR THE FINAL PRINTOUT OF RESULTS         *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C     OUTPUT :
C
C     LIST OF ALL RESULTS OF THE TIME-DEPENDENT INTEGRATION
C
C***********************************************************************
C***********************************************************************
C     CHAPTER I: STORAGE ORGANIZATION
C***********************************************************************
C***********************************************************************
C     I.1: TYPES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      LOGICAL TSO
      LOGICAL MOLEFR,LPLO,MASSFR
C***********************************************************************
C     I.2: ARRAYS
C***********************************************************************
      CHARACTER(LEN=*)  NSYMB(NEQ)
      CHARACTER*28 HEADER
      DIMENSION T(NTS),TEMP(NTS,NUR),CTOT(NTS,NUR),PTOT(NTS,NUR),
     1          VOR(NTS,NUR),ENTH(NTS,NUR),RM(NTS,NUR)
      DIMENSION SPR(NTS,NUR,NEQ)
      DIMENSION ARTEMP(NTS),ARC(NTS),ARSH(NTS),AMASS(NTS)
      DIMENSION ARWI(NTS,NSPEC)
      DIMENSION S(NEQ,NUR)
      DIMENSION V(NTS,NEQ),VP(NTS,NEQ),SMIN(NEQ,NUR),SMAX(NEQ,NUR)
      DIMENSION HELP(NEQ+2),HL(NEQ+2)
      DIMENSION RPAR(*),IPAR(*)
      DIMENSION SUMWIM(NTS),RHO(NTS)
C***********************************************************************
C     I.3: COMMON BLOCKS
C***********************************************************************
      COMMON/BSSPV/SSPV(20*MNSPE)
      COMMON/BDIM/NSMAX,NRMAX,NCRMAX,NPOMAX,NELMAX
      COMMON/BOUT/KONINT,KONTAB,KONPLO,KONSEN,KONMEC,KONANA,KONRED,
     1  KONSPE(MNSPE),KONSID(MNSPE),NSC,
     1  ICRIT,KOGSEN,KONRST,MOLEFR,MASSFR,LPLO
      COMMON/BSENS/LSMAX,NSENS,NRSENS,NISENS,NPSENS,
     1       IRSENS,IISENS,IPSENS,ISENSI(MNSEN)
      COMMON/SPOPT1/NEXCET,NENGIN
      DOUBLE PRECISION, DIMENSION(1) :: DXDS,DTDS,DPDS
C***********************************************************************
C     I.4: INITIALIZATION
C***********************************************************************
      DATA ZERO/0.0D0/
      DATA RGAS/8.3143D0/
C***********************************************************************
      T    = ZERO
      TEMP = ZERO
      PTOT = ZERO
      CTOT = ZERO
      VOR  = ZERO
      SPR  = ZERO
      ENTH = ZERO
CMMag Initialization of header string
      HEADER = '                            '
C***********************************************************************
C
C     INPUT AND OUTPUT OF CONCENTRATIONS
C
C***********************************************************************
C***********************************************************************
C     INPUT OF CONCENTRATIONS
C***********************************************************************
      WRITE(NFILE6,*)
      IFIRST = 1
      ILAST  = NEQ
CBZ from M.Magar version
!      CALL SPEARY(NDIM,IFIRST,ILAST,NFILE8,NTS,NUR,T,TEMP,PTOT,CTOT,
!     1            VOR,SPR,ENTH)
      CALL SPEARY(NEQ,NDIM,IFIRST,ILAST,NFILE8,NTS,NUR,T,TEMP,PTOT,
     1             CTOT,VOR,SPR,ENTH)
CBZ end
C***********************************************************************
C     INPUT OF RATES
C***********************************************************************
      IIOPL = 2
      CALL AVADAT(NTS,NEQ,T,TEMP,CTOT,PTOT,NUR,NSPEC,TIME,VOR,SPR,
     1           ENTH,ARTEMP,ARC,ARSH,ARWI,AMASS,RM,HEADER)
      CALL UPDSPI(KONPLO,NSC,KONSPE,NSCXXX,KONSID,NEQ)      
      CALL LISTOU(NCA,NEQ,IIOPL,NSCXXX,KONSID,NTS,HEADER,'  t(s)  ',
     1     'T(K)    ','conc.   ','p(bar)  ','V(m^3)  ','H(J/kg) ',
     1     'Reactor ','   m%   ',
     1     NSYMB,PAR,T,TEMP,CTOT,PTOT,V,NUR,NSPEC,TIME,VOR,SPR,
     1            ENTH,ARTEMP,ARC,ARSH,ARWI,AMASS,RM,RPAR,IPAR)
      RETURN
C***********************************************************************
C   END OF -MULOU2-
C***********************************************************************
      END

      SUBROUTINE LISTOU(NCA,NVAR,ICON,NSC,KONS,NTS,PSYMB,TSYMB,
     1  S1,S2,S3,S4,S5,S6,S7,NSYMB,PAR,T,V1,V2,V3,V,NUR,NSPEC,
     1  TIME,VOR,SPR,ENTH,ARTEMP,ARC,ARSH,ARWI,AMASS,RM,RPAR,IPAR)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *         PROGRAM FOR THE OUTPUT OF LISTINGS                *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C     OUTPUT :
C
C***********************************************************************
C***********************************************************************
C     CHAPTER I: STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      CHARACTER*28 PSYMB
      CHARACTER(LEN=*),INTENT(IN):: NSYMB(*),TSYMB,S1,S2,S3,S4,S5,S6,S7
      DIMENSION T(NTS),V(NTS,*),KONS(NSC)
      DIMENSION V1(NTS,NUR),V2(NTS,NUR),V3(NTS,NUR),VOR(NTS,NUR),
     1          ENTH(NTS,NUR),AMASS(NTS),RM(NTS,NUR)
      DIMENSION SPR(NTS,NUR,*)
      DIMENSION ARTEMP(NTS),ARC(NTS),ARSH(NTS),ARWI(NTS,NSPEC)
      DIMENSION SMF(MNEQU),CI(MNSPE)
      DIMENSION RATES(MNSPE),WORK1(MNREA)
      DATA LRW1/MNREA/
      COMMON/BSSPV/SSPV(20*MNSPE)
      COMMON/BSENS/LSMAX,NSENS,NRSENS,NISENS,NPSENS,
     1       IRSENS,IISENS,IPSENS,ISENSI(MNSEN)
      COMMON/BHCP/HLHH(7,MNSPE*2),HINF(3,MNSPE)
      COMMON/SPOPT1/NEXCET,NENGIN
      DIMENSION RPAR(*),IPAR(*)      
      DIMENSION DGDC(1,1),DGDT(1),DODC(1,1),DODT(1)
      DOUBLE PRECISION, DIMENSION(1) :: DXDS,DTDS,DPDS
C***********************************************************************              
      DATA NFIL28/28/,NFIL29/29/,NFIL38/38/,NFIL39/39/
      DATA NFIL48/48/,NFIL49/49/
C---- Maximum Number of Outputs
CBZ
      !DATA NOP/50/
      NOP = NSPEC
CBZ end
C***********************************************************************
C     II: OUTPUT
C***********************************************************************
      NCOL = MIN(NOP,NSPEC)
C***********************************************************************
C     WRITE IN TECPLOT FORMAT
C***********************************************************************
C     II.1: OUTPUT OF HEADING
C***********************************************************************
      WRITE(NFIL28,830) PSYMB
      WRITE(NFIL28,831) TSYMB,S1,S2,S3,S4,S5,(NSYMB(K),K=1,NCOL)
      WRITE(NFIL28,835) NTS
C----- Avaraged ONE Zone!
      DO J=1,NTS
         WRITE(NFIL28,832) T(J),ARTEMP(J),ARC(J),V3(J,1),
     1          SUM(VOR(J,1:NUR)),ARSH(J),(ARWI(J,K),K=1,NCOL)
      ENDDO
C***********************************************************************
C     II.2: OUTPUT OF TABLES
C***********************************************************************
      IF(NENGIN.EQ.0) THEN
        WRITE(NFIL48,840) PSYMB
        WRITE(NFIL48,831) TSYMB,S6,S7,S1,S2,S3,S4,S5,(NSYMB(K),K=1,NCOL)
C----
        WRITE(NFIL49,840) PSYMB
        WRITE(NFIL49,831) TSYMB,S6,S7,S1,S2,S3,S4,S5,(NSYMB(K),K=1,NCOL)
        DO J=1,NTS
C----- Single Zones!
          WRITE(NFIL48,835) NUR
          DO I=1,NUR
           DI = I
           WRITE(NFIL48,832) T(J),DI,RM(J,I)/AMASS(J),V1(J,I),V2(J,I),
     1           V3(J,I),VOR(J,I),ENTH(J,I),(SPR(J,I,K),K=1,NCOL)
          ENDDO
        ENDDO
        DO I=1,NUR
          WRITE(NFIL49,835) NTS
          DO J=1,NTS
            DI = I
            WRITE(NFIL49,832) T(J),DI,RM(J,I)/AMASS(J),V1(J,I),V2(J,I),
     1           V3(J,I),VOR(J,I),ENTH(J,I),(SPR(J,I,K),K=1,NCOL)
          ENDDO
        ENDDO
      ENDIF
C***********************************************************************
C     WRITE IN GNUPLOT FORMAT
C***********************************************************************
C     II.1: OUTPUT OF HEADING
C***********************************************************************
      WRITE(NFIL29,81) NTS,PSYMB
      WRITE(NFIL29,82)
C***********************************************************************
C     II.2: OUTPUT OF TABLES
C***********************************************************************
C --- OUTPUT OF AVERAGE VALUES
      WRITE(NFIL29,84) TSYMB,S1,S2,S3,S4,S5,(NSYMB(K),K=1,NCOL)
      DO M=1,NTS
        WRITE(NFIL29,85) T(M), ARTEMP(M), ARC(M),V3(M,1),
     1        SUM(VOR(M,1:NUR)),ARSH(M),(ARWI(M,I),I=1,NCOL)
      ENDDO
      WRITE(NFIL29,89)
C***********************************************************************
C     II.3: ADDITIONAL OUTPUT FOR ENGINE CYCLE COMPUTATION             *
C     (A.Schubert 10/2005)                                             *
C***********************************************************************
      IF(NENGIN.GE.1) THEN
        CALL ENGOU1(NC,S1,S2,S3,S4,S5,S6,S7,T,V,SPR,NSYMB,PSYMB,KONS,
     1       V1,VOR,V2,V3,ENTH,NENGIN,ICON,ARTEMP,ARC,ARSH,ARWI,RM,
     1       AMASS,NVAR,NSC,NSPEC,NTS,NUR,NFIL38,NFIL39,NFIL48,NFIL49,
     1       NFILE6,NOP)
C***********************************************************************
C     II.4: OUTPUT OF "EXHAUST" OF THE LAST TIMESTEP  
C     (A.Schubert 10/2005)
C***********************************************************************
        CALL EGROUT(NSYMB,NTS,NSPEC,NUR,ARTEMP,ARC,ARWI,AMASS,
     1              V3,VOR,NFILE6)
      ENDIF
C***********************************************************************
C     III. REGULAR EXIT
C***********************************************************************
      RETURN
C***********************************************************************
C     VI: FORMAT STATEMENTS
C***********************************************************************
   81 FORMAT('%>DATA',I5,A28)
   82 FORMAT(3X,A,6X,1PE25.18)
   83 FORMAT(6(1X,1PE12.5))
   84 FORMAT(1('%',1X,2X,A,1X,63(2X,A,1X)))
   85 FORMAT(1(1X,64(1PE10.2E3,1X)))
   86 FORMAT('%REACTOR :',I3)
   89 FORMAT('%<    ',75X)
  830 FORMAT('TITLE= "Output from MULZON -Average Values-',
     1       A28,'"') 
  831 FORMAT('VARIABLES =',/,7('"',A,'"',',':))
  832 FORMAT(1(3X,7(1PE10.3,1X)))
  835 FORMAT('ZONE I=',I4,', J=1, F=POINT')
  840 FORMAT('TITLE= "Output from MULZON -Reactorwise-',
     1       A28,'"')
C***********************************************************************
C     END OF -LISTOU-
C***********************************************************************
      END

CBZ from M.Magar version
!      SUBROUTINE SPEARY(NDIM,IFIRST,ILAST,NFILE8,NTS,NUR,T,TEMP,PTOT,
!     1                  CTOT,VOR,SPR,ENTH)
      SUBROUTINE SPEARY(NEQ,NDIM,IFIRST,ILAST,NFILE8,NTS,NUR,T,TEMP,PTOT
     1                  ,CTOT,VOR,SPR,ENTH)
CBZ end
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *         PROGRAM FOR THE INPUT OF SPECIFIC ARRAYS          *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C     OUTPUT :
C
C***********************************************************************
C***********************************************************************
C     CHAPTER I: STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION T(NTS),TEMP(NTS,NUR),CTOT(NTS,NUR),PTOT(NTS,NUR),
     1          VOR(NTS,NUR),ENTH(NTS,NUR)
CBZ from M.Magar version
!      DIMENSION SPR(NTS,NUR,*)
	  DIMENSION SPR(NTS,NUR,NEQ)
CBZ end
      COMMON/SPOPT1/NEXCET,NENGIN
C***********************************************************************
C     CHAPTER II: INPUT OF ARRAYS
C***********************************************************************      
      WRITE(*,*) 'PPPPP',NDIM ,IFIRST,ILAST
      OPEN(UNIT=NFILE8,FILE='fort.8',ERR=90)
      REWIND NFILE8
CBZ from M.Magar version
!      NDOA=NDIM/NUR
CBZ end
C---- STARTING AT FIRST ENTRY, ENDING AT LAST ENTRY
CBZ from M.Magar version
!      IF(IFIRST.EQ.1.AND.ILAST.EQ.NDOA) THEN
      IF(IFIRST.EQ.1.AND.ILAST.EQ.NEQ) THEN
CBZ end
        DO M=1,NTS
          READ(NFILE8,*)
          DO L=1,NUR
            READ(NFILE8,81) T(M),TEMP(M,L),PTOT(M,L),CTOT(M,L),
     1                      VOR(M,L),ENTH(M,L)
CBZ from M.Magar version
!            READ(NFILE8,82) (SPR(M,L,I),I=1,NDOA)
            READ(NFILE8,82,ERR=90) (SPR(M,L,I),I=1,NEQ)
CBZ end
          ENDDO
        ENDDO
      ENDIF
      RETURN
C***********************************************************************
C     CHAPTER VI: FORMAT STATEMENTS
C***********************************************************************
   81 FORMAT(6(1X,1PE12.5))
   82 FORMAT(6(1X,1PE12.5))
   83 FORMAT(6(1X,1PE12.5))
   84 FORMAT('DAT:',5(1X,1PE12.5))
   90 CONTINUE
      WRITE(*,*) ' ERROR IN OPENING NFILE8'
      STOP
C***********************************************************************
C     END OF -SPEARY-
C***********************************************************************
      END

      SUBROUTINE AVADAT(NTS,NEQ,T,RT,RC,RP,NUR,NSPEC,TIME,VOR,SPR,
     1                  ENTH,ARTEMP,ARC,ARSH,ARWI,ARM,RM,HEADER)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     * PROGRAM FOR THE OUTPUT OF AVERAGE VALUES OF ALL REACTORS  *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C     OUTPUT :
C
C***********************************************************************
C***********************************************************************
C     CHAPTER I: STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      CHARACTER*28 HEADER
      LOGICAL MOLEFR,MASSFR
      DIMENSION T(NTS),RT(NTS,NUR),RC(NTS,NUR),
     1          RP(NTS,NUR),VOR(NTS,NUR),ENTH(NTS,NUR),SPRWIM(NUR)
      DIMENSION SPR(NTS,NUR,NEQ),RMI(NTS,NUR,NSPEC)
      DIMENSION RM(NTS,NUR),XMOLM(NTS,NUR)
      DIMENSION ASPM(NTS,NSPEC),ARWI(NTS,NSPEC)
      DIMENSION ARM(NTS),ARTEMP(NTS),ARC(NTS),ARSH(NTS)
      DIMENSION WI(NSPEC)
      COMMON/BINIT/P0,T0,V0,H0,XU(MNSPE)
      COMMON/BHCP/HLHH(7,MNSPE*2),HINF(3,MNSPE)
      COMMON/SPOPT1/NEXCET,NENGIN
      COMMON/LOCSM/NW,NWS,NU,NV,ND,NG,NP,NEM,NUZ,NSP 
      COMMON/BSSPV/SSPV(20*MNSPE)
      COMMON/BOUT/KONINT,KONTAB,KONPLO,KONSEN,KONMEC,KONANA,KONRED,
     1            KONSPE(MNSPE),KONSID(MNSPE),NSC,ICRIT,KOGSEN,KONRST,
     1            MOLEFR,MASSFR,LPLO
      DOUBLE PRECISION, DIMENSION(NSPEC)             :: CI
      DOUBLE PRECISION, DIMENSION(NSPEC+2)           :: SMF
      DOUBLE PRECISION, DIMENSION(1) :: DXDS,DTDS,DPDS
      DATA ZERO/0.0D0/
C***********************************************************************
C     CALCULATION OF AVERAGE VALUES
C***********************************************************************
C---- LEAVE MASS FRACTIONS
      IF(MASSFR) THEN
        HEADER='      Mass fractions       '
      ELSE
        IF(MOLEFR) THEN
          HEADER='      Mole fractions       '
        ENDIF 
        IF(.NOT.MOLEFR) THEN
          HEADER=' Concentrations (mol/m**3) '
        ENDIF
      ENDIF
      DO M=1,NTS
      ARWIM = ZERO
      SPRWIM(1:NUR)  = ZERO
C--- CALCULATE OVERALL MASS
        DO L=1,NUR
          XMOLM(M,L) = 1/SUM(SPR(M,L,1:NSPEC)/XMOL(1:NSPEC))
          RM(M,L) = RC(M,L)*VOR(M,L)*XMOLM(M,L)
          DO I=1,NSPEC
            RMI(M,L,I) = SPR(M,L,I)*RM(M,L)
          ENDDO
        ENDDO
        ARM(M) = SUM(RM(M,1:NUR)) 
C--- CALCULATE OVERALL MASS FRACTION
        DO I=1,NSPEC
C---- LEAVE MASS FRACTIONS
          ASPM(M,I) = SUM(RMI(M,1:NUR,I))
          WI(I) = ASPM(M,I)/ARM(M)
        ENDDO  
C--- CALCULATE OVERALL TEMPERATURE
        NSP2 = NSPEC + 2
        RHO = ARM(M)/SUM(VOR(M,1:NUR))
        HGES = SUM(RM(M,1:NUR)*ENTH(M,1:NUR))
        H = HGES/ARM(M)
        SMF(1) = H
        SMF(2) = RHO
        SMF(3:2+NSPEC) = WI
        CALL TRASSP(4,NSP2,SMF,NSPEC,TEMP,C,CI,RHODUM,H,P,
     1              SSPV,IERR,0,DXDS,1,DTDS,DPDS)
        ARTEMP(M) = TEMP
        ARC(M)    = C
        ARSH(M)   = H
C---- TRANSFORM INTO MOLE FRACTIONS
        DO I=1,NSPEC
C---- LEAVE MASS FRACTIONS
          ARWI(M,I) = WI(I)
          IF(.NOT.MASSFR) THEN
            ARWIM = ARWIM + (ARWI(M,I)/XMOL(I))
            DO N=1,NUR
              SPRWIM(N) = SPRWIM(N) + (SPR(M,N,I)/XMOL(I))
            ENDDO
          ENDIF
        ENDDO
        IF(.NOT.MASSFR) THEN
          DO L = 1,NSPEC
            ARWI(M,L) = (ARWI(M,L)/XMOL(L))/ARWIM
            DO N=1,NUR
              SPR(M,N,L) = (SPR(M,N,L)/XMOL(L))/SPRWIM(N)
            ENDDO
          ENDDO
C---- TRANSFORM INTO CONCENTRATIONS
          IF(.NOT.MOLEFR) THEN 
            DO L = 1,NSPEC
              ARWI(M,L) = ARWI(M,L)*ARC(M)
              DO N=1,NUR
                SPR(M,N,L) = (SPR(M,N,L)*RC(M,N))
              ENDDO
            ENDDO
          ENDIF
        ENDIF
      ENDDO
C***********************************************************************
      RETURN
C***********************************************************************
C     CHAPTER II: FORMAT STATEMENTS
C***********************************************************************
C***********************************************************************
C     END OF -AVARE-
C***********************************************************************
      END

      SUBROUTINE CALQCH(NWALL,NENGIN,TIME,QCHA,TLL,TSURR,OVOL,
     1                  PACT,VL,L,NUZ,ALPHA,RHOM)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *    PROGRAM FOR THE ELOSS CALCULATION USING DIFFERENT      *
C     *                   APPROXIMATIONS:                         *
C     *                                                           *
C     *   1       : STANDARD CALCULATION                          *
C     *   2       : HOHENBERG                                     *
C     *   3       : WOSCHNI                                       *
C     *   4       : HAJIREZA                                      *
C     *   5       : BARGENDE                                      *
C     *   6       : WAGNER                                        *
C     *                                                           *
C     *         (by S. Stuerenburg 26.01.2006)
C     *************************************************************
C
C***********************************************************************
C
C     INPUT  : NWALL     : NOPT(4)
C              NENGIN    : NOPT(5)
C              TIME      :
C              TSURR     : WALL TEMPERATURE
C              TLL       : ARRAY OF TEMPERATUR IN ZONES
C              PACT      : PRESSURE IN ZONES (SAME FOR ALL REACTORS)
C              VL        : VOLUME OF ZONE L
C              L         : NUMBER OF ACTUAL ZONE
C              NUZ       : NUMBER OF ZONES
C
C
C     OUTPUT : QCHA      : HEAT TRANSFER COEFFICIENT
C
C***********************************************************************
C
C     VARIABLES:
C
C                   VPIST : mean piston velocity         [m/s]
C                   OVOL  : surface-volume ration        [1/m]
C                   CONST : constant in WOSCHNI equation
C                   RGAS  : universal gas constant       [J/(g*K)
C
C***********************************************************************
C***********************************************************************
C CHAPTER I: STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DATA ZERO/0.0D0/,ALMOST/0.999999999D0/,RGAS/8.3143D0/
      DATA PI/3.14159265359D0/
      DOUBLE PRECISION, DIMENSION(0:NUZ+1):: TLL
      DOUBLE PRECISION, DIMENSION(NUZ+1):: ALPHA
      DOUBLE PRECISION, DIMENSION(NUZ)  :: VL,RHOM
C***********************************************************************
C CHAPTER II: CALCULATION OF ELOSS DEPENDING ON CHOSEN APROXIMATION
C***********************************************************************
C---- NEWTON / WAGNER CALCULATION POSSIBLE WITHOUT ENGINE OPTION:
      IF(NWALL.EQ.1) THEN
C***********************************************************************
C     1. USING STANDARD CALCULATION
C***********************************************************************
        QCHA = -ALPHA(L)*(TLL(L) - TLL(L-1))
     1         -ALPHA(L+1)*(TLL(L) - TLL(L+1))
      ENDIF
C---- OTHER CALCULATION ONLY POSSIBLE WITH ENGINE OPTION
      IF(NENGIN.NE.0) THEN
C---- CALCULATION OF CONST AND VPIST
        CALL CALWAL(NWALL,NFILE6,TIME,OVOL,VPIST,CONST)
C***********************************************************************
C     2. HOHENBERG
C***********************************************************************
        IF(NWALL.EQ.2) THEN
          PBAR=PACT/1.0D5
          C1=130.0
          C2=1.4
          CWOSCH=C1*(VL(L)**(-0.06))*(PBAR**0.8)*(TLL(L)**(-0.4))*
     1           (TLL(L)**(-0,1625)*((VPIST+C2)**0.8))
          QCHA  = +CWOSCH*OVOL*(TLL(L) - TLL(0))
        ENDIF

        IF(NWALL.EQ.3.OR.NWALL.EQ.4.OR.NWALL.EQ.5) THEN
          PACT=PACT
C***********************************************************************
C     3. WOSCHNI
C***********************************************************************
          IF (NWALL.EQ.3) THEN
C---- pressure in kPa
          PBAR=PACT/1.0D2
            VELORAT = 0.5
            VRATE=VELORAT/VPIST
C---- for gas exchange period
CASSA            C1=6.18+0.417*VRATE
CASSA            C2=0
C---- for rest of cycle
            C1=2.28+0.308*VRATE
C---- for compression period
            C2=0.0
C---- for combustion and expansion period
CASSA            C2 = 3.24*1.0D-3
C---- displacement volume
            VDEPLA = 1.0D0
C---- reference points start of compression
            TCOMST = 1.0D0
            PCOMST = 1.0D0
            VCOMST = 1.0D0
C---- motored pressure
            PMOT = PBAR
            CVOL = ((VDEPLA*TCOMST)/(PCOMST*VCOMST))*(PBAR - PMOT)
            AVGVPI=(C1*VPIST+C2*CVOL)
            CONST = (CONST**(-0.2))*3.26
            TVALUE = TLL(L)**(-0.55)
          ENDIF
C***********************************************************************
C     4. HAJIREZA
C***********************************************************************
          IF (NWALL.EQ.4) THEN
            PBAR=PACT/1.0D5
            C1 = 2.28
            AVGVPI = (C1*VPIST)
            TVALUE = TLL(L)**(-0.53)
          ENDIF
C***********************************************************************
C     5. BARGENDE
C***********************************************************************
          IF (NWALL.EQ.5) THEN
            PBAR=PACT
Cassa assamption
            VELORAT=3.0
            VRATE=0.5
            C1=6.18+0.417*VELORAT
            AVGVPI = (C1*VPIST)
            CONST = (CONST**(-0.2))*0.13
            TVALUE = TLL(L)**(-0.53)
          ENDIF
C Calculation of QCHA basing on the already calculated values TVALUE
C PBAR and AVGVPI
          CWOSCH= CONST*(PBAR**0.8)*TVALUE*(AVGVPI**0.8)
          QCHA  = CWOSCH*OVOL*(TLL(L) - TLL(0))
        ENDIF
      ENDIF
C***********************************************************************
C     1. WAGNER (not yet)
C***********************************************************************
      IF(NWALL.EQ.6) THEN
C        XMEMOL=ZERO
C        DO 210 I=1,NSPEC
C          XMEMOL = XMEMOL + CIL(I)/CL*XMOL(I)
C  210   CONTINUE
C        TRCM=TIME
C        IF(TIME.LT.1.0D-19) TRCM=1D-9
C        PACT   = CL * (RGAS*TL)
c        HELP1  = 8*RGAS*TL/(PI*XMEMOL)
C        COEFMO = VPIST
c        HELP2  = COEFMO*6.02205D23
C        HECAAL = 1/(3*SQRT(2.0))*(CPL-RGAS)*SQRT(HELP1)/HELP2
C        HELP3  = (((2.0*PI**2)/3.0)**2+4/PI*(CONST**2)/
C     1           ((HECAAL/((CPL-RGAS)*(PACT/(RGAS*TL))))*TRCM))
C        QCHA = OVOL*HECAAL/CONST*SQRT(HELP3)*(TL-TSURR)
         WRITE(*,*)'NOT IMPLEMENTED YET !!!!!'
         STOP
      ENDIF
C***********************************************************************
C     7. RCM-Modell
C***********************************************************************
      IF(NWALL.EQ.7) THEN
        RTOT = 0.04D0
        VIN = 0.0D0
        DO I = 1,L-1
        VIN = VIN+VL(I)
        ENDDO
                VOUT = 0.0D0
        DO I = 1,L
        VOUT = VOUT+VL(I)
        ENDDO
                VTOT = 0.0D0
        DO I = 1,NUZ
        VTOT = VTOT+VL(I)
        ENDDO
        HTOT = VTOT/(PI*RTOT**2)
c       write(*,*) 'QTTA',HTOT ,VTOT,PI
        AL = 2.0D0*PI*(RTOT/(HTOT*PI))**(2.0D0/3.0D0)*
     1  VIN**(2.0D0/3.0D0)*(1.0D0+HTOT/RTOT)
        ALF = 2.0D0*PI*(RTOT/(HTOT*PI))**(2.0D0/3.0D0)*
     1  VOUT**(2.0D0/3.0D0)*(1.0D0+HTOT/RTOT)
        QCHA = (-ALPHA(L)*AL*(TLL(L) - TLL(L-1))
     1         -ALPHA(L+1)*ALf*(TLL(L) - TLL(L+1)))/(RHOM(L)*VL(L))
C       write(*,*) 'QCHA',QCHA,VL(L),RHOM(L),ALPHA(L),AL,SLF,TLL(L)
c       write(*,*) 'QTTA',HTOT 
      ENDIF
C**********************************************************************C
C     REGULAR EXIT
C**********************************************************************C
      RETURN
C***********************************************************************
C     END OF -CALQCH-
C***********************************************************************
      END
