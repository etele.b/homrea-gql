      SUBROUTINE AUTPAR(IADD,N,NZEV,SMF,AROW,ACOL,
     1           NC,COQO,TAUCA,LIW,IW,LRW,RW,IPAR,RPAR,MSGFIL)
C***********************************************************************
C     automatical Parameterization of the first cell 
C***********************************************************************
C
C  Input:
C
C    N              : dimension of the state space
C
C    NZEV           : number of conserved quantities
C
C    CSQ(N,NZEV)    : matrix for conserved quantities
C                     column partition are the vectors
C
C    NC           : number of controlling  quantities
C
C    COQO(NC,N)    : matrix for controling quantities
C                     row partition are the vectors
C
C    ACOL(n,n)      : transformation matrix for the state space
C                     to separate conserved and non-conserved quantities
C                     first NC column vectors are the vectors of the
C                     conserved scalars, the remaining vectors make
C                     up the orthogonal complement
C
C    AROW(N,N)      : inverse of ACOL
C
C
C    MSGFIL         : fortran unit for error messages
C
C
C    IADD           : flag for parameterization
C                     IADD=1: only one additional direction 
C                             will be calulated 
C                     IADD=0: all irections will be calculated
C
C  Output:
C
C
C    CSQ(LCSQ)      : matrix for conserved quantities
C                     column partition are the vectors
C
C     Work arrays:
C
C       RW(LRW), IW(LIW)   real*8 and integer work space (checked)
C         ,COQ(NC,N)
C
C***********************************************************************
C     STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      PARAMETER (ONE = 1.0D0, ZERO = 0.0D0)
      EXTERNAL MASRES,MASJAC
      LOGICAL PARMAN
C***********************************************************************
C     Arrays           
C***********************************************************************
      DIMENSION AROW(N,N),ACOL(N,N)
      DIMENSION COQO(NC,N)
      DIMENSION SMF(N),VRS(N),AR(N),AI(N),
     1          AVR(N,N),AVL(N,N),XJAC(N,N),
     1          SCHUR(N,N)
      DIMENSION PMAT(NC-NZEV,N)   
      DIMENSION RPAR(*),IPAR(*),RW(LRW),IW(LIW),HELP(N),DEL(N)
      DOUBLE PRECISION, DIMENSION(NC,N)      :: TAUCA
      DOUBLE PRECISION, DIMENSION((NC-NZEV),(NC-NZEV)) :: HM1  
      DOUBLE PRECISION, DIMENSION(N,N)     :: XTCDUM,XTRDUM
      DOUBLE PRECISION, DIMENSION(NC,N)  :: COQ  
C***********************************************************************
C     Common blocks 
C***********************************************************************
      COMMON/ILDMOP/IPARA(1),ISTAOP,MANPAR,NOFORM,ANAFUE,INMAN,lasloo,
     1              ICALTR,MASENS,IUPVER,NTRANS
C***********************************************************************
C     INITIAL CHECKS
C***********************************************************************
      IF(LRW.LT.N) GOTO 910
      IF(LIW.LT.N) GOTO 920
                      PARMAN = .FALSE.
      IF(MANPAR.GE.1) PARMAN = .TRUE.
C***********************************************************************
C     Initialize COQ 
C***********************************************************************
      COQ = COQO
C***********************************************************************
C
C     Automatic parameterization                
C
C***********************************************************************
      IF(.NOT.PARMAN) THEN
         MDIM = NC
         RDIM = 0.D0
         CALL EGVBAS(1,0,MDIM,RDIM,NZEV,N,MASRES,MASJAC,TIME,SMF,
     1               ACOL,AROW,XTCDUM,XTRDUM,
     1               AR,AI,AVR,AVL,XJAC,SCHUR,
     1               RPAR,IPAR,IPRI,MSGFIL,IERBAS)
         IF(IERBAS.GT.0) GOTO 900
C***********************************************************************
C     Compute parameterization of reactive variables: PMAT
C***********************************************************************
         NFAST = N-NC
         CALL PARMAT(IADD,N,NC,NZEV,NFAST,COQ,AVR,
     1               PMAT,LRW,RW,LIW,IW,MSGFIL)
         COQ(NZEV+1:NC,1:N) = PMAT(1:(NC-NZEV),1:N)
         HM1=MATMUL(COQ(NZEV+1:NC,1:N),TRANSPOSE(COQO(NZEV+1:NC,1:N)))
         IH=NC-NZEV
   88 FORMAT(15(1X,1PE9.2))
         CALL DGEINV(IH,HM1,RW,IW,INFO)
         IF(INFO.NE.0) THEN
           WRITE(*,*) ' ERROR in AUTPAR'
           STOP
         ENDIF
         COQ(NZEV+1:NC,1:N) = MATMUL(HM1(1:IH,1:IH),COQ(NZEV+1:NC,1:N))
C---- new
      ENDIF
C***********************************************************************
C     Calculate matrix for conserved variable tabulation
C***********************************************************************
      TAUCA = COQ
C***********************************************************************
C     Solution exit             
C***********************************************************************
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
  900 CONTINUE
      WRITE(MSGFIL,*) ' EGVBAS returned to AUTPAR with an error flag'
      IERR = 6
      GOTO 999
  910 CONTINUE
      IERR = 7
      GOTO 999
  920 CONTINUE
      IERR = 8
      GOTO 999
C---- VECTORS OF CONSERVED SCALERS LINEARLY INDEPENDENT
  930 CONTINUE
      IERR = 1
      GOTO 999
C---- ERROR IN INVERSION OF MATRIX
  940 CONTINUE
      IERR = 2
      GOTO 999
C---- VECTORS OF CONSERVED SCALERS LINEARLY INDEPENDENT
  950 CONTINUE
      IERR = 3
      GOTO 999
C---- ERROR IN INVERSION OF MATRIX
  960 CONTINUE
      WRITE(MSGFIL,*) ' ierr =',IERR
      IERR = 4
      GOTO 999
C---- ERROR IN INVERSION OF MATRIX
  970 CONTINUE
      IERR = 5
      GOTO 999
  999 WRITE(MSGFIL,998) IERR
  998 FORMAT(' ','  Error in -AUTPAR-, IERR=',I3)
      STOP
C***********************************************************************
C     END OF -AUTPAR-
C***********************************************************************
      END
      SUBROUTINE PARMAT(IADD,NEQ,NC,NZEV,NFAST,COQ,SCHUR,
     1                  PMAT,LRW,RW,LIW,IW,MSGFIL)
C***********************************************************************
C     STORAGE ORGANIZATION                                 
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      DIMENSION COQ(NC,NEQ),SCHUR(NEQ,NEQ)
      DIMENSION PMAT(NC-NZEV,NEQ)   
      DIMENSION RW(LRW),IW(LIW)
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)      :: QMAT
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)      :: WMAT 
C***********************************************************************
C     Compute orthonormal matrix (PMAT) of Z_f and P_c            
C***********************************************************************
C---- copy SCHUR into WMAT:
      WMAT(1:NFAST,1:NEQ)       = TRANSPOSE(SCHUR(1:NEQ,NC+1:NEQ))
C---- copy COQ into WMAT:
      WMAT(NFAST+1:NFAST+NZEV,1:NEQ) = COQ(1:NZEV,1:NEQ)
         NCONT = NFAST+NZEV
      IF(IADD.EQ.1)THEN
         NCONT = NFAST+NC - 1    
         WMAT(NFAST+NZEV+1:NFAST+NC-1,1:NEQ) = COQ(NZEV+1:NC-1,1:NEQ)
      ENDIF
      IERR=0   
      CALL ORBAS(NEQ,NCONT,NEQ,WMAT,NEQ,QMAT,LRW,RW,LIW,IW,-2,IERR)
      IF(IERR.GT.0) GOTO 900
      PMAT(1:(NC-NZEV),1:NEQ) = QMAT(NFAST+NZEV+1:NEQ,1:NEQ)
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
  900 CONTINUE
      WRITE(MSGFIL,998) IERR
  998 FORMAT(' ','  Error in -PARMAT- IERR=',I3)
      STOP
C***********************************************************************
C     END OF -PARMAT-
C***********************************************************************
      END
      SUBROUTINE INPDAT(NDIM,N2DIM,NCEL2,NVLOW,NEQ,NC,LRV,NSPECLOW,
     1       IY,IVERT,ICORD,IRET,NPROP,PROP,SYMPRP,SYMLOW,CMAT)
C***********************************************************************
C
C     This Routine reads a starting solution:              
C      
C     task:
C
C     istaop = 1: transform lower dimensional ILDM into higher 
C                 dimensional ILDM with respect to the number of 
C                 reaction progress variables
C     istaop > 1: transform lower dimensional ILDM into higher 
C                 dimensional ILDM with respect to the number of 
C                 chemical species included in the reaction mechanism
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      PARAMETER (ONE = 1.0D0, ZERO = 0.0D0)
      CHARACTER*4 STRING
      CHARACTER(LEN=*) SYMLOW(NPROP),SYMPRP(NPROP)
      DATA NFIOUT/6/,NFITAB/93/
C***********************************************************************
C     Arrays for higher dimension
C***********************************************************************
      DIMENSION IRET(NVERM),IY(NDIMM*NCELM)
      DIMENSION IVERT(N2DIMM*NCELM)
      DIMENSION ICORD(NDIMM*NVERM)
      DIMENSION PROP(NPROP,NVERM) 
C***********************************************************************
C     Arrays for starting solution
C***********************************************************************
      DIMENSION IRETLOW(NVERM),IYLOW(NDIMM*NCELM)
      DIMENSION IVERTLOW(N2DIMM*NCELM)
      DIMENSION ICORDLOW(NDIMM*NVERM)
      DIMENSION PROPLOW(MNPROP*NVERM)
      DIMENSION CMAT(*)                 
C***********************************************************************
C     Auxiliary arrays 
C***********************************************************************
      DIMENSION NEIGH(2*NDIMM)
C***********************************************************************
C     Common blocks                 
C***********************************************************************
      COMMON/ILDMOP/IPARA(1),ISTAOP,MANPAR,NOFORM,ANAFUE,INMAN,lasloo,
     1              ICALTR,MASENS,IUPVER,NTRANS
      COMMON/LOWSYS/NREACLOW
C***********************************************************************
C     Check                          
C***********************************************************************
      IF(NDIM.EQ.1.AND.ISTAOP.EQ.1) GOTO 950
C***********************************************************************
C     Read starting solution and Symbols
C***********************************************************************
      WRITE(NFIOUT,*) 'start -INPDAT-'
      REWIND NFITAB
 50   READ(NFITAB,55) STRING
      IF(STRING.NE.'>TAB') GOTO 50
      READ(NFITAB,65) NCEL1,NCEL2,NVLOW,NDLOW,NPRLOW,NSPECLOW,NREACLOW
 55   FORMAT(20A4)
 65   FORMAT(10I8)
      WRITE(NFIOUT,*)'NCEL1,NCEL2,NVLOW,NDLOW,NPRLOW,NSPECLOW,NREACLOW',
     1                NCEL1,NCEL2,NVLOW,NDLOW,NPRLOW,NSPECLOW,NREACLOW
      N2DLOW = 2**NDLOW
      CALL IOGPAR(-1,NFITAB,NPRLOW,NCEL1,NCEL2,NVLOW,NDLOW,N2DLOW,
     1            NSPECLOW,NREACLOW,IYLOW,IVERTLOW,ICORDLOW,
     1            IRETLOW,PROPLOW,SYMLOW,CMAT,MIX,NC)
C     IF(NCEL1.NE.NCEL2) GOTO 920
C***********************************************************************
C     Delete boundary points with flag .ne. 0              
C***********************************************************************
C-02  IF(NDLOW.EQ.1.OR.INMAN.EQ.1)THEN
      IF(NDLOW.EQ.989898)THEN
 100     CONTINUE
         DO 101 IP=1,NVLOW
         IMES = 0 
         CALL CHKNEI(IP,NVLOW,NDLOW,ICORDLOW,NEIGH,IERR,IRETLOW,IMES)
         DO 101 IN=1,2*NDLOW
         IF(NEIGH(IN).EQ.0.AND.IRETLOW(IP).NE.0)THEN
            CALL DELPOI(IP,NDLOW,N2DLOW,NVLOW,NPRLOW,NCELM,NCEL2,
     1           IYLOW,ICORDLOW,IRETLOW,IVERTLOW,PROPLOW)
C Karin Koenig, bug
c           GOTO 100
            GOTO 101
Cend Karin Koenig
         ENDIF
 101     CONTINUE
      ENDIF
C***********************************************************************
C     Fit format of starting solution               
C***********************************************************************
C---- IRETLOW --> IRET
      IRET(1:NVLOW) = 0
C---- Set Prop to zero
      PROP = ZERO
C---- PROPLOW --> PROP  
      NEQL = 2+NSPECLOW
cjoerg      DO ISYML = NDLOW+1,NDLOW+NEQL
      DO ISYML = NDLOW+1,2*NDLOW+2*NEQL+LRV
         ICOUNT = NDIM
cjoerg         DO ISYM = NDIM+1,NDIM+NEQ
         DO ISYM = NDIM+1,2*NDIM+2*NEQ+LRV
            ICOUNT = ICOUNT+1
            IF(SYMLOW(ISYML).EQ.SYMPRP(ISYM))THEN
               DO I=1,NVLOW
                  ILAUF = (I-1)*NPRLOW
                  PROP(ISYM,I) = PROPLOW(ILAUF+ISYML)
               ENDDO
               GOTO 150
            ENDIF
         ENDDO
cjoerg         IF(ICOUNT.EQ.NDIM+NEQ) GOTO 940
         IF(ICOUNT.EQ.2*NDIM+NEQ+LRV) GOTO 940
 150     CONTINUE
      ENDDO
 810  FORMAT(A4)
 819  FORMAT(I8)
      IF(ISTAOP.EQ.1)THEN
C***********************************************************************
C     Transform lower dimension ---> higher dimension
C
C     in terms of the number of RPVs
C***********************************************************************
C---- Check
         IF(NDIM.NE.NDLOW+1) GOTO 925
C---- IYLOW --> IY
         DO I=1,NCEL2
            ILAUF = (I-1)*NDIM
            ILAU1 = (I-1)*NDLOW
            IY(ILAUF+1:ILAUF+NDLOW) = IYLOW(ILAU1+1:ILAU1+NDLOW)
         ENDDO
C---- ICORDLOW --> ICORD
         DO I=1,NVLOW
            ILAUF = (I-1)*NDIM
            ILAU1 = (I-1)*NDLOW
            ICORD(ILAUF+1:ILAUF+NDLOW) = ICORDLOW(ILAU1+1:ILAU1+NDLOW)
         ENDDO
C---- IVERTLOW --> IVERT
         DO I=1,NCEL2
            ILAUF = (I-1)*N2DIM
            ILAU1 = (I-1)*N2DLOW
            IVERT(ILAUF+1:ILAUF+N2DLOW) = IVERTLOW(ILAU1+1:ILAU1+N2DLOW)
         ENDDO
      IRET(NVLOW+1:NVLOW+NVLOW) = 10
      ELSEIF(ISTAOP.GE.2)THEN
C***********************************************************************
C     Transform lower dimension ---> higher dimension
C
C     in terms of the number of species of the reaction mechanism
C***********************************************************************
C---- Check
         IF(NDIM.NE.NDLOW) GOTO 930
C---- IYLOW --> IY
         IY(1:NDIM*NCEL2) = IYLOW(1:NDIM*NCEL2)
C---- ICORDLOW --> ICORD
         ICORD(1:NDIM*NVLOW) = ICORDLOW(1:NDIM*NVLOW)
C---- IVERTLOW --> IVERT
         IVERT(1:N2DIM*NCEL2) =IVERTLOW(1:N2DIM*NCEL2)
      ENDIF
C***********************************************************************
C     Regular end
C***********************************************************************
      WRITE(NFIOUT,*) 'end -INPDAT-'
      RETURN 
C***********************************************************************
C     Error exits   
C***********************************************************************
  900 CONTINUE
      GOTO 999
  910 CONTINUE
      WRITE(NFIOUT,911)
  911 FORMAT(/,' Error during input of symlow')
      GOTO 999
  920 CONTINUE
      WRITE(NFIOUT,921) NCEL1,NCEL2
  921 FORMAT(/,' ncel1 =', I4,' and ncel2 =', I4,/,
     1       ' No difference allowed!!!!!!!!!!!!')
      GOTO 999
  925 CONTINUE
      WRITE(NFIOUT,926) NDLOW,NDIM
  926 FORMAT(/,' Dimensions of starting solution and',
     1       ' new ILDM do not fit',
     1       /,' NDLOW:',I2,' <--> NDIM:',I2)  
      GOTO 999
  930 CONTINUE
      WRITE(NFIOUT,931) NDIM,NDLOW
  931 FORMAT(/,' ndim =',I3,' not equal ndlow =',I3)
      GOTO 999
  940 CONTINUE
      WRITE(NFIOUT,941) SYMLOW(ISYML)
  941 FORMAT(/,' symbol ',A,' not found in symprp')
      GOTO 999
  950 CONTINUE
      WRITE(NFIOUT,951)
  951 FORMAT(/,' No starting solution available')
      GOTO 999
  999 WRITE(NFIOUT,998)
  998 FORMAT(/,3((3('*')),' Error in -INPDAT- ',(3('*')),/),/)
      STOP
C***********************************************************************
C     End of -INPDAT-                 
C***********************************************************************
      END
