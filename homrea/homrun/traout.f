C>    traout
      SUBROUTINE HOROUR(NEQ,NSPEC,NREAT,INFO,ILAS,NFIL10)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *        PROGRAM FOR OUTPUT OF THE REACTION RATES           *    *
C     *                                                           *    *
C     *           LAST CHANGE:  OCT 01, 1990 / MAAS               *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C                                                                      *
C     INPUT  :
C
C     NREAT           = NUMBER OF REACTIONS
C
C***********************************************************************
C
C
C
C***********************************************************************
C     I: STORAGE
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      COMMON/BRATES/TIMRAT,RATTEM(MNREA),RATINT(MNREA)
      COMMON/BICRES/ICRES
C***********************************************************************
C     OUTPUT OF RATES
C***********************************************************************
      IF((INFO.EQ.3.AND.ILAS.EQ.1).OR. INFO .EQ.4 ) THEN
         WRITE(NFIL10,81) TIMRAT
         WRITE(NFIL10,85) (RATTEM(J),J=1,NREAT)
      ENDIF
       IF((INFO.EQ.1.AND.ILAS.EQ.1).OR.
     1    (INFO.EQ.2)                          .OR.
     1    (INFO.EQ.3.AND.ILAS.EQ.1).OR.
     1    (INFO.EQ.4)                                   )  THEN
         WRITE(NFIL10,82) TIMRAT
         WRITE(NFIL10,85) (RATINT(J),J=1,NREAT)
      ENDIF
   81 FORMAT('           Rates','(mol/m**3*s)',' at Time =',E11.4)
   82 FORMAT('Integrated Rates','(mol/m**3)  ',' at Time =',E11.4)
   85 FORMAT(6(1X,1PE12.5))
C***********************************************************************
C     REGULAR EXIT
C***********************************************************************
      RETURN
C***********************************************************************
C     END OF -UPD   -
C***********************************************************************
      END
      SUBROUTINE UPDINR(NEQ,NSPEC,NREAT,TIME,S,SP,F,IRES,RPAR,IPAR)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *        PROGRAM FOR UPDATING THE INTEGRATED RATES          *    *
C     *                                                           *    *
C     *           LAST CHANGE:  OCT 01, 1990 / MAAS               *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C                                                                      *
C     INPUT  :
C
C     NREAT           = NUMBER OF REACTIONS
C
C***********************************************************************
C
C
C
C***********************************************************************
C     I: STORAGE
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      COMMON/BRATES/TIMRAT,RATTEM(MNREA),RATINT(MNREA)
      COMMON/BICRES/ICRES
      DIMENSION S(NEQ),SP(NEQ),F(NEQ),RPAR(*),IPAR(*)
      DATA HALF/0.5D0/
C***********************************************************************
C     UPDATE INTEGRATED RATES
C***********************************************************************
      DELTAR=TIME-TIMRAT
      DO 10 J=1,NREAT
      RATINT(J)=RATINT(J)+HALF*DELTAR*RATTEM(J)
   10 CONTINUE
      ICRES=1
      CALL HORRES(TIME,S,SP,F,IRES,RPAR,IPAR)
      ICRES=0
      DO 20 J=1,NREAT
      RATINT(J)=RATINT(J)+HALF*DELTAR*RATTEM(J)
   20 CONTINUE
C***********************************************************************
C     REGULAR EXIT
C***********************************************************************
      RETURN
C***********************************************************************
C     END OF -UPD   -
C***********************************************************************
      END

      SUBROUTINE HOROU1(NTA,NSPEC,NSYMB,TIME,NFILE6,NFILE8,S,SP,NEQ)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *    PROGRAM FOR THE PRINTOUT OF RESULTS AT THE END OF A    *    *
C     *                       TIME STEP                           *    *
C     *           LAST CHANGE:  DEC 18, 1989 / MAAS               *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C                                                                      *
C     INPUT  :
C
C     NTA             = NUMBER OF TIME STEP
C     NSPEC           = NUMBER OF SPECIES
C     NSYMB(2,NSPEC+4)= SPECIES SYMBOLS
C     TIME            = TIME
C     NFILE6          = UNIT FOR OUTPUT OF LISTING
C     NFILE8          = UNIT FOR OUTPUT FOR POST-PROCESSING
C     S (NEQ,*)       = DEPENDENT VARIABLE'S ARRAY (SEE -DASSL-) +
C                       LOCAL SENSITIVITY COEFFICIENTS
C     NEQ             = NUMBER OF DEPENDENT VARIABLES
C
C     OUTPUT :
C
C     RESULTS OF TIME DEPENDENT INTEGRATION
C
C***********************************************************************
C
C
C
C***********************************************************************
C     I: STORAGE
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      COMMON/BSSPV/SSPV(20*MNSPE)
      LOGICAL DETONA,PCON,VCON,TCON,WALL
      LOGICAL MOLEFR,MASSFR,LPLO
      CHARACTER(LEN=*)  NSYMB(*)
      DIMENSION S(NEQ,*),SP(NEQ,*)
      COMMON/BOPTL/PCON,VCON,TCON,DETONA,NWALL,NCOMPLX,NPSR
      COMMON/BDIM/NSMAX,NRMAX,NCRMAX,NPOMAX,NELMAX
      COMMON/LOCS0D/NW,NWS,NP,NK,NI,NH
      COMMON/BSENS/LSMAX,NSENS,NRSENS,NISENS,NPSENS,
     1       IRSENS,IISENS,IPSENS,ISENSI(MNSEN)
      COMMON/BOUT/KONINT,KONTAB,KONPLO,KONSEN,KONMEC,KONANA,KONRED,
     1  KONSPE(MNSPE),KONSID(MNSPE),NSC,
     1  ICRIT,KOGSEN,KONRST,MOLEFR,MASSFR,LPLO
      COMMON/BSDIR/SPARAL(MNREA),SPERPE(MNREA)
      DIMENSION SPSCAL(MNSPE),SENHEL(MNEQU)
      DOUBLE PRECISION, DIMENSION(1) :: DXDS,DTDS,DPDS
C***********************************************************************
C     Get length of symbols
C***********************************************************************
      NSYLEN=LEN(NSYMB(1))
C***********************************************************************
C     II: CHECK FOR OUTPUT
C***********************************************************************
      IF(NTA.EQ.0) RETURN
C***********************************************************************
C     III: DETERMINE T, P, AND TOTAL C
C***********************************************************************
      IF(.NOT.DETONA) THEN
C---- TEMPERATURE
        IF(.NOT.TCON) STE=S(NEQ,1)
        IF(TCON) CALL HORVAR(NFILE6,3,TIME,STE,TPR)
C---- CONCENTRATION
        CTOT=0.0D0
        DO 10 I=1,NSPEC
          CTOT=CTOT+S(I,1)
   10   CONTINUE
C---- PRESSURE
        PBA=1.0D-5*CTOT*STE*8.3143D0
      ELSE
        P     = S(NP,1)
        RHOV  = S(NK,1)
        V     = (S(NI,1) - P) / RHOV
        RHO = RHOV / V
        H   = S(NH,1) - V**2/2.0D0
        SENHEL(1) = H
        SENHEL(2) = P
        CALL DCOPY(NSPEC,S(NW,1),1,SENHEL(3),1)
        CALL TRASSP(3,NEQ,SENHEL,NSPEC,T,CTOT,SPSCAL,RHODUM,H,P,
     1       SSPV,IERR,0,DXDS,1,DTDS,DPDS)
        PBA = P / 1.0D5
      ENDIF
C***********************************************************************
C     IV: OUTPUT OF INTERMEDIATE RESULTS (C, T) ON NFILE6
C***********************************************************************
C---- NO OUTPUT FOR OPTION -SHORT-
      IF(KONINT.EQ.0) GO TO 200
      WRITE(NFILE6,811)
      CALL UPDSPI(KONINT,NSC,KONSPE,NSCXXX,KONSID,NEQ)
      IF(.NOT.TCON) NSCXXX=NSCXXX-1
C---- WRITE RESULTS
      IF(MOLEFR) THEN
      WRITE(NFILE6,818) TIME,PBA,STE,
     1       (NSYMB(KONSID(I)),S(KONSID(I),1)/CTOT,I=1,NSCXXX)
      ELSE IF(DETONA) THEN
      WRITE(NFILE6,818) TIME,PBA,STE,
     1       (NSYMB(KONSID(I)),S(KONSID(I),1),I=1,NSCXXX)
      ELSE
      WRITE(NFILE6,814) TIME,PBA,STE,
     1       (NSYMB(KONSID(I)),S(KONSID(I),1)*1.D-6,I=1,NSCXXX)
      ENDIF
C     IF(NSENS.LE.0) GOTO 200
C     GOTO 200
C     DO 110 JP=1,NSENS
C     WRITE(NFILE6,815) ISENSI(JP),
C    1  (NSYMB(KONSID(I)),S(KONSID(I),JP+1),I=1,NSCXXX)
C 110 CONTINUE
  200 CONTINUE
C***********************************************************************
C     V: OUTPUT ON NFILE8 (SOLUTION AND LOCAL SENSITIVITIES)
C***********************************************************************
c         WRITE(88,821) NTA,TIME,STE,PBA,CTOT
c         WRITE(88,822) (S(I,1),I=1,NEQ)
      IF(NTA.EQ.1) THEN
        OPEN(UNIT=NFILE8,FILE='fort.8')
        REWIND NFILE8
      ENDIF
      NSEP1=NSENS+1
      IF(KOGSEN.NE.0) NSEP1=1
      WRITE(NFILE8,821) NTA,TIME,STE,PBA,CTOT
      WRITE(NFILE8,822) ((S(I,JJ),I=1,NEQ),JJ=1,NSEP1)
C***********************************************************************
C     CALCULATE COMPONENTS OF SENSITIVITY VECTOR PARALLEL AND PERP. TO T
C***********************************************************************
      IF(1.GT.0) GOTO 4343
C     WRITE(6,*) '  --------------- TIME=',TIME,' ----------------'
C---- SCALE THE TRAJECTORY VECTOR AND NORM IT
      SPNF = 0.D0
      DO 310 I=1,NEQ
      SPSCAL(I)=SP(I,1)/DMAX1(1.D-10,DABS(S(I,1)))
      SPNF = SPNF + SPSCAL(I) * SPSCAL(I)
  310 CONTINUE
      SPNF =DSQRT(SPNF)
      DO 320 I=1,NEQ
      SPSCAL(I)=SPSCAL(I)/SPNF
  320 CONTINUE
C---- SPSCAL IS NOW THE NORMED VECTOR OF  1/S * DSP/DT
      IF(NRSENS.GT.0) THEN
      DO 390 J = 1, NRSENS
      DO 330 I = 1, NEQ
      SENHEL(I)=S(I,J+IRSENS)
      SENHEL(I)=SENHEL(I)/DMAX1(1.D-10,DABS(S(I,1)))
  330 CONTINUE
      PARCOM = 0.0D0
      DO 340 I = 1, NEQ
      PARCOM = PARCOM + SENHEL(I) * SPSCAL(I)
  340 CONTINUE
      PERCOM= 0.0D0
      DO 350 I = 1, NEQ
      HPERP =  SENHEL(I) - PARCOM * SPSCAL(I)
      PERCOM = HPERP * HPERP + PERCOM
  350 CONTINUE
      IF(DABS(PARCOM).LT.1.D-2.AND.DABS(PERCOM).LT.1.D-2) THEN
      WRITE(6,*) ' REACTION',J,' NOT SENSITIVE '
      ELSE
      IF(DABS(PARCOM).GE.1.D1*DABS(PERCOM)) THEN
      WRITE(6,*) ' REACTION',J,' PARALLEL'
      ELSE
      IF(DABS(PARCOM).LE.1.D-1*DABS(PERCOM)) THEN
      WRITE(6,*) ' REACTION',J,' PERPENDICULAR '
      ELSE
      WRITE(6,*) ' REACTION',J,' MIXED         '
      ENDIF
      ENDIF
      ENDIF
C     WRITE(6,*) ' PARALLEL:',PARCOM,'  PERPEND.:',PERCOM
  390 CONTINUE
      ENDIF
 4343 CONTINUE
      RETURN
C***********************************************************************
C     VI: FORMAT STATEMENTS
C***********************************************************************
  811 FORMAT(' ',79('-'))
  814 FORMAT(' ','  *** c (mol/cm**3) at ',
     1               't =',1PE9.2,' s,  ',
     1       'P =',1PE9.2,' bar,  T =',1PE9.2,' K ***  ',/,
     1       1(3(1X,A,':',1PE9.2,1X),1X,A,':',1PE9.2))
  817 FORMAT(' ','  *** Mass fractions at ',
     1               'x =',1PE9.2,' m,  ',
     1       'P =',1PE9.2,' bar,  T =',1PE9.2,' K ***',/,
     1       'V =',1PE9.2,' m/s,  H =',1PE9.2,' J/kg,  c =',1PE9.2,/,
     1       1(3(1X,A,':',1PE9.2,1X),1X,A,':',1PE9.2))
  818 FORMAT(' ','  *** Mole fractions at ',
     1               't =',1PE9.2,' s,  ',
     1       'P =',1PE9.2,' bar,  T =',1PE9.2,' K ***',/,
     1       1(3(1X,A,':',1PE9.2,1X),1X,A,':',1PE9.2))
C 815 FORMAT('0','   Sensitivity Coefficients with Respect',
C    1       ' to Reaction ',I3,7X,/,
C    1       1(3(1X,A,':','   ',1PE10.3,3X)))
CMag in 821 I3 -> I5 and 6x -> 4x
CBZ - 1.08.2011
CBZ precision of time increased
CBZ  821 FORMAT('>TIM',I5,4X,4(1X,1PE12.5))
  821 FORMAT('>TIM',I5,4X,1X,ES23.15E3,3(1X,1PE12.5))
CBZ end
  822 FORMAT(6(1X,1PE12.5))
C***********************************************************************
C     END OF -HOROU1-
C***********************************************************************
      END
      SUBROUTINE HORIO2(NDIM,IFIRST,ILAST,NFILE8,NTS,T,TEMP,PTOT,CTOT,V)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *         PROGRAM FOR THE INPUT OF SPECIFIC ARRAYS          *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C
C     OUTPUT :
C
C
C***********************************************************************
C
C
C
C***********************************************************************
C     CHAPTER I: STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION T(NTS),TEMP(NTS),CTOT(NTS),PTOT(NTS),V(NTS,*)
C***********************************************************************
C     CHAPTER II: INPUT OF ARRAYS
C***********************************************************************
      WRITE(6,*) NDIM ,IFIRST,ILAST
      OPEN(UNIT=NFILE8,FILE='fort.8',ERR=90)
      REWIND NFILE8
      NN= ILAST - IFIRST + 1
      ILAPL1=ILAST+1
      IFIMI1=IFIRST-1
C---- STARTING AT FIRST ENTRY, ENDING AT LAST ENTRY
      IF(IFIRST.EQ.1.AND.ILAST.EQ.NDIM) THEN
      DO 10 L=1,NTS
      READ(NFILE8,81) T(L),TEMP(L),PTOT(L),CTOT(L)
      READ(NFILE8,82) (V(L,I),I=1     ,NDIM )
   10 CONTINUE
      ENDIF
C---- STARTING AT FIRST ENTRY, ENDING NOT AT LAST ENTRY
      IF(IFIRST.EQ.1.AND.ILAST.NE.NDIM) THEN
      DO 20 L=1,NTS
      READ(NFILE8,81) T(L),TEMP(L),PTOT(L),CTOT(L)
      READ(NFILE8,82) (V(L,I),I=1     , ILAST),
     1                  (DUMMY, I=ILAPL1,NDIM)
   20 CONTINUE
      ENDIF
C---- STARTING NOT AT FIRST ENTRY, ENDING AT LAST ENTRY
      IF(IFIRST.NE.1.AND.ILAST.EQ.NDIM) THEN
      DO 30 L=1,NTS
      READ(NFILE8,81) T(L),TEMP(L),PTOT(L),CTOT(L)
      READ(NFILE8,82) (DUMMY ,I=1      ,IFIMI1),
     1                (V(L,I-IFIRST+1),I=IFIRST, NDIM )
   30 CONTINUE
      ENDIF
C---- STARTING NOT AT FIRST ENTRY, ENDING NOT AT LAST ENTRY
      IF(IFIRST.NE.1.AND.ILAST.NE.NDIM) THEN
      DO 40 L=1,NTS
      READ(NFILE8,81) T(L),TEMP(L),PTOT(L),CTOT(L)
      READ(NFILE8,82) (DUMMY ,I=1      ,IFIMI1),
     1                (V(L,I-IFIRST+1),I=IFIRST ,ILAST),
     1                (DUMMY ,I=ILAPL1,NDIM)
   40 CONTINUE
      ENDIF
      RETURN
C***********************************************************************
C     CHAPTER VI: FORMAT STATEMENTS
C***********************************************************************
CBZ - 1.08.2011
CBZ precision of time increased
CBZ   81 FORMAT(13X,5(1X,1PE12.5))
   81 FORMAT(13X,1X,ES23.15E3,4(1X,1PE12.5))
CBZ end
   82 FORMAT(6(1X,1PE12.5))
   90 CONTINUE
      WRITE(*,*) ' error in opening NFILE8'
      STOP
C***********************************************************************
C     END OF -HORIO2-
C***********************************************************************
      END

      SUBROUTINE STOPLO(NCA,NVAR,ICON,NSC,KONS,NTS,
     1  PSYMB,TSYMB,S1,S2,S3,NSYMB,PAR,T,V1,V2,V3,V,NFIL28,NFIL29)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *         PROGRAM FOR THE OUTPUT OF LISTINGS                *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C
C     OUTPUT :
C
C
C***********************************************************************
C
C
C
C***********************************************************************
C     CHAPTER I: STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      CHARACTER*28 PSYMB
      CHARACTER(LEN=*)  NSYMB(*),S1,S2,S3,TSYMB
      DIMENSION T(NTS),V(NTS,*),KONS(NSC),V1(NTS),V2(NTS),V3(NTS)
      DIMENSION SMF(MNEQU),CI(MNSPE)
      DIMENSION RATES(MNSPE),WORK1(MNREA)
      DATA LRW1/MNREA/
      COMMON/BSSPV/SSPV(20*MNSPE)
      COMMON/BSENS/LSMAX,NSENS,NRSENS,NISENS,NPSENS,
     1       IRSENS,IISENS,IPSENS,ISENSI(MNSEN)
      COMMON/BHCP/HLHH(7,MNSPE*2),HINF(3,MNSPE)
      COMMON/BPARM/RPAR(MNRPAR),IPAR(MNIPAR)
      COMMON/SPOPT1/NEXCET,NENGIN
      DIMENSION  DGDC(1,1),DGDT(1),DODC(1,1),DODT(1)
      DOUBLE PRECISION, DIMENSION(1) :: DXDS,DTDS,DPDS
      DOUBLE PRECISION, DIMENSION(MNSPE) :: VTT
C***********************************************************************
C     WRITE IN TECPLOT FORMAT
C***********************************************************************
C-----------------------------------------------------------------------
C     II.2: OUTPUT OF TABLES
C-----------------------------------------------------------------------
      IF(ICON.EQ.1) THEN
        IF(NCA.EQ.1) WRITE(NFIL28,837)
     1         TSYMB,S1,S2,S3,(NSYMB(KONS(K)),K=1,NSC)
        WRITE(NFIL28,842) NTS
        DO 130 J=1,NTS
         WRITE(NFIL28,844)T(J),V1(J),V2(J),V3(J),(V(J,KONS(K)),K=1,NSC)
  130   CONTINUE
      ELSE
        NSPEC = NVAR-1
C---- Tecplot output
        IF(0.EQ.0)THEN
C        IF(1.EQ.0)THEN
C---- Tecplot output without rates in units of [mol/kg]
          IF(NCA.EQ.1)
     1       WRITE(NFIL28,837) TSYMB,S1,S2,S3,(NSYMB(K),K=1,NSPEC)
          WRITE(NFIL28,842) NTS
          DO 140 J=1,NTS
          VTT(1:NSPEC) = (1/SSPV(1:NSPEC))*V(J,1:NSPEC)
C-Sl Changing of NVAR to NSPEC
          WRITE(NFIL28,844) T(J),V1(J),V2(J),V3(J),(VTT(K),K=1,NSPEC)
C-Sl
  140     CONTINUE
        ELSE
C---- Tecplot output with rates in units of [mol/kg], [mol/(kg*s)]
          IF(NCA.EQ.1) WRITE(NFIL28,837) TSYMB,S1,S2,S3,'RHO     ',
     1    'hspec   ',' p      ',(NSYMB(K),K=1,NVAR-1)
          IF(NCA.EQ.1) WRITE(NFIL28,838) (NSYMB(K),K=1,NVAR-1)
          WRITE(NFIL28,842) NTS
          DO 150 J=1,NTS
C---- Calculate concentrations
             PRESS = V3(J)*1.0E5
             NEQ = NSPEC+2
             TEMPT = V1(J)
             TEMPC = V2(J)
             CI(1:NSPEC) = V(J,1:NSPEC)
             CALL TRASSP(-3,neq,smf,nspec,tempt,tempc,CI,
     1                    rho,enth,press,sspv,ierr,0,DXDS,1,DTDS,DPDS)
C---- Calculate reaction rates
             CALL MECHJA(0,nspec,nreac,nm,nrins,nrtab,
     1  IREAC(NMATLL),IREAC(NNRVEC),RREAC(NPK),RREAC(NTEXP),RREAC(NEXA),
     1  RREAC(NVT4),RREAC(NVT5),RREAC(NVT6),IREAC(NIARRH),
     1            RREAC(NXZSTO),RREAC(NRATCO),
     1            tempc,ci,tempt,rates,NRSENS,ISENSI(IRSENS),RPAR,
     1            LRW1,WORK1,IPRIM,NFILE6,IERR,
     1  RREAC(NAINF),RREAC(NBINF),RREAC(NEINF),RREAC(NT3),
     1  RREAC(NT1),RREAC(NT2),RREAC(NAFA),RREAC(NBFA),HLHH,HINF,
     1  1,1,DGDC,DGDT,DODC,DODT)
C---- Scale rates in units of [mol/(kg*s)]
c            RATES(1:NSPEC) = RATES(1:NSPEC)/RHO
             WRITE(nfil28,844) t(j),V1(J),V2(J),V3(J),rho,
     1            SMF(1:2+NSPEC),RATES(1:NSPEC)/RHO
  150     CONTINUE
C---- Write output for number of species
C         NFIL27 = 27
C         WRITE(nfil27,839)
C         WRITE(nfil27,840) NSPEC
        ENDIF
      ENDIF
 837  FORMAT('VARIABLES =',/,7('"',A,'"',',':))
 838  FORMAT(7('"r',A,'"',:','))
 839  FORMAT('Number of species:')
 840  FORMAT(I4)
 842  FORMAT('ZONE I=',I4,', J=1, F=POINT')
 844  FORMAT(1(3X,7(1PE10.3,1X)))
C***********************************************************************
C     WRITE IN GNUPLOT FORMAT
C***********************************************************************
C-----------------------------------------------------------------------
C     II.1: OUTPUT OF HEADING
C-----------------------------------------------------------------------
      WRITE(NFIL29,81) NTS,PSYMB
      WRITE(NFIL29,82)
C-----------------------------------------------------------------------
C     II.2: OUTPUT OF TABLES
C-----------------------------------------------------------------------
      IF(ICON.EQ.1) THEN
          NCOL=MIN0(17,NSC)
          WRITE(NFIL29,84) TSYMB,S1,S2,S3,(NSYMB(KONS(K)),K=1,NCOL)
          DO 30 J=1,NTS
          WRITE(NFIL29,85)T(J),V1(J),V2(J),V3(J),(V(J,KONS(K)),K=1,NCOL)
   30     CONTINUE
      ELSE
          NCOL=MIN0(17,NVAR)
          WRITE(NFIL29,84) TSYMB,S1,S2,S3,(NSYMB(K),K=1,NCOL)
          DO 40 J=1,NTS
          WRITE(NFIL29,85) T(J),V1(J),V2(J),V3(J),(V(J,K),K=1,NCOL)
   40     CONTINUE
      ENDIF
      WRITE(NFIL29,89)
C***********************************************************************
C     II.4: MODEL OF AN EXOTHERMIC CENTER AND ITS DYNAMIC BEHAVIOUR  -
C           OUTPUT OF RESULTS TO FILE 'fort.33'
C           (06.02.03 / O.Maiwald)
C***********************************************************************
      IF(NEXCET .GE.1) THEN
        CALL EXCOUT(NFILE6,NTS,V1,V3,T)
      ENDIF
      RETURN
C***********************************************************************
C     CHAPTER VI: FORMAT STATEMENTS
C***********************************************************************
   81 FORMAT('>DATA',I5,A28)
   82 FORMAT(3X,A,6X,1PE25.18)
   84 FORMAT(1(1X,2X,A,1X,20(2X,A,1X)))
   85 FORMAT(1(1X,25(1PE10.2E3,1X)))
   89 FORMAT('<    ',75X)
C***********************************************************************
C     END OF -STOPLO-
C***********************************************************************
      END

      SUBROUTINE HORIOU(NSPEC,NDIM,NEQ,NSYMB,NFILE8,NFILE6,TSO,
     1     NTS,HELP1,HELP2,T,TEMP,CTOT,PTOT,V,SMIN,SMAX)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *         PROGRAM FOR THE FINAL PRINTOUT OF RESULTS         *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C
C     OUTPUT :
C
C     LIST OF ALL RESULTS OF THE TIME-DEPENDENT INTEGRATION
C
C***********************************************************************
C
C
C
C***********************************************************************
C     CHAPTER I: STORAGE ORGANIZATION
C***********************************************************************
C***********************************************************************
C     I.1: TYPES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      LOGICAL TSO,NICE,EQUSYM
      LOGICAL MOLEFR,MASSFR,LPLO
C***********************************************************************
C     I.2: ARRAYS
C***********************************************************************
      CHARACTER(LEN=*) NSYMB(NEQ)
      CHARACTER(LEN=16) XSYMB,SDES,SYPL(2),S2
      CHARACTER*28 HEADER
      COMMON/BSSPV/SSPV(20*MNSPE)
      DIMENSION T(NTS),CTOT(NTS),TEMP(NTS),PTOT(NTS)
      DIMENSION V(NTS,NEQ)
      DIMENSION SMIN(NEQ),SMAX(NEQ)
      DIMENSION HELP1(NEQ),HELP2(NEQ)
      DIMENSION KONSP(MNEQU)
      COMMON/BOUT/KONINT,KONTAB,KONPLO,KONSEN,KONMEC,KONANA,KONRED,
     1  KONSPE(MNSPE),KONSID(MNSPE),NSC,
     1  ICRIT,KOGSEN,KONRST,MOLEFR,MASSFR,LPLO
      DOUBLE PRECISION, DIMENSION(1) :: DXDS,DTDS,DPDS
      PARAMETER(ZERO = 0.0D0)
      DATA NZHD/28/
C***********************************************************************
C     INPUT OF VALUES
C***********************************************************************
      LENCHE=MIN(16,LEN(NSYMB))
      NICE = .TRUE.
      IFIRST = 1
      ILAST  = NEQ
      CALL HORIO2(NDIM,IFIRST,ILAST,NFILE8,NTS,T,TEMP,PTOT,CTOT,V)
C***********************************************************************
C     TRANSFORM INTO OTHER UNITS
C***********************************************************************
C---- TRANSFORM INTO MOLE FRACTIONS
      IF(MOLEFR) THEN
      HEADER='      Mole fractions        '
      S2='   c            '
      DO 10 L=1,NTS
      CTOINV=1.0D0/CTOT(L)
      CALL DSCAL(NSPEC,CTOINV,V(L,1),NTS)
   10 CONTINUE
      YSCA = 1.0D0
      ELSE
C---- TRANSFORM INTO MASS FRACTIONS
      IF(MASSFR)THEN
      HEADER='      Mass fractions        '
      S2='h(J/kg)         '
      DO 40 L=1,NTS
      CALL DCOPY(NSPEC,V(L,1),NTS,HELP1,1)
      CALL TRASSP(-3,NEQ,HELP2,NSPEC,TEMP(L),CTOT(L),
     1       HELP1,RHO,HSPEC,PL,SSPV,IERR,0,DXDS,1,DTDS,DPDS)
      CALL DCOPY(NSPEC,HELP2(3),1,V(L,1),NTS)
      CTOT(L)=HSPEC
   40 CONTINUE
      YSCA = 1.0D0
      ELSE
C---- LEAVE CONCENTRATIONS
      HEADER=' Concentrations (mol/m**3) '
      S2='   c            '
      YSCA = 1.0D0
      ENDIF
      ENDIF
C***********************************************************************
C     DECIDE ABOUT X-AXIS
C***********************************************************************
      NUCALL = 0
  100 CONTINUE
      WRITE(6,*)  ' ENTER X-AXIS ("TIME" OR SPECIES SYMBOL)'
      READ(5,830) SDES
      WRITE(6,*)  SDES,' CHOSEN'
      IF(EQUSYM(3,SDES,'END')) RETURN
  830 FORMAT(A)
C---- CHECK SYMBOL
      ITI=0
      IF(EQUSYM(4,SDES,'TIME')) ITI=1
      ICOSP=0
      DO 110 I=1,NEQ
      IF(EQUSYM(LENCHE,NSYMB(I),SDES)) ICOSP=I
  110 CONTINUE
      IF(ITI.EQ.0.AND.ICOSP.EQ.0) THEN
      WRITE(6,*) ' INVALID ENTRY '
      GOTO 100
      ENDIF
C*********************************************************************--
C     II.4: PLOT OF CONCENTRATIONS OR MOLE FRACTIONS
C*********************************************************************--
        XSYMB='                '
      IF(ITI.NE.0) THEN
        TMIN=T(1)
        TMAX=T(NTS)
        XSYMB='t(s)            '
      ELSE
        TMIN=0.0D0
        TMAX=0.0D0
C       TMAX=SMAX(ICOSP)
        XSYMB=NSYMB(ICOSP)
      ENDIF
C*********************************************************************--
C     ASK FOR SPECIES TO BE PLOTTED
C*********************************************************************--
      IF(.NOT.NICE) THEN
  200 CONTINUE
      WRITE(6,*)  ' ENTER SPECIES TO BE PLOTTED (SYMBOLS)'
      READ(5,831) SYPL(1),SYPL(2)
      WRITE(6,*)  SYPL(1),SYPL(2)
      IF(SYPL(1).EQ.'END ') GOTO 100
  831 FORMAT(A,/,A)
C---- CHECK SYMBOL
      ICOPL=0
      IF(NEQ.EQ.NSPEC+1.AND.SYPL(1).EQ.'T               ') THEN
       KONSP(1)=NEQ
       ICOPL=ICOPL+1
      ENDIF
      IF(NEQ.EQ.NSPEC+1.AND.SYPL(2).EQ.'T               ') THEN
       KONSP(2)=NEQ
       ICOPL=ICOPL+1
      ENDIF
      DO 210 I=1,NEQ
      IF(EQUSYM(LENCHE,NSYMB(I),SYPL(1))) THEN
       KONSP(1)=I
       ICOPL=ICOPL+1
      ENDIF
      IF(EQUSYM(LENCHE,NSYMB(I),SYPL(2))) THEN
        KONSP(2)=I
        ICOPL=ICOPL+1
      ENDIF
  210 CONTINUE
      IF(ICOPL.EQ.0) THEN
      WRITE(6,*) ' INVALID ENTRY '
      GOTO 200
      ENDIF
C***********************************************************************
C
C***********************************************************************
      ELSE
      ICOPL=0
      WRITE(6,*)  ' ENTER SPECIES TO BE PLOTTED (SYMBOLS), blank to end'
  561 CONTINUE
      READ(5,832) SYPL(1)
  832 FORMAT(A)
      WRITE(6,*)  SYPL(1)
      IF(EQUSYM(LENCHE,SYPL(1),'        ').AND.ICOPL.EQ.0) GOTO 100
      IF(EQUSYM(LENCHE,SYPL(1),'        ')) GOTO 315
      IF(EQUSYM(3,SYPL(1),'END')) GOTO 100
C---- CHECK SYMBOL
      IF(NEQ.EQ.NSPEC+1.AND.EQUSYM(8,SYPL(1),'T       ')) THEN
         ICOPL=ICOPL+1
         KONSP(ICOPL)=NEQ
        GOTO 561
      ENDIF
      DO 212 I=1,NEQ
      IF(EQUSYM(LENCHE,NSYMB(I),SYPL(1))) THEN
        ICOPL=ICOPL+1
        KONSP(ICOPL)=I
        GOTO 561
      ENDIF
  212 CONTINUE
      WRITE(6,*) ' INVALID ENTRY '
      GOTO 561
  315 CONTINUE
      ENDIF
C*********************************************************************--
C     II.4: PLOT
C*********************************************************************--
      write(6,*) ' enter 0 for TSO'
      read(*,*) ICIN
                    TSO = .FALSE.
      IF(ICIN.GT.0) TSO = .TRUE.
      write(6,*) ' enter file number '
      read(*,*) ICIN
      IF(ITI.NE.0) THEN
C----       PLOT OF TEMPORAL PROFILES
      CALL PLOTOU(NUCALL,NOPL,ICOPL,KONSP,ICIN,LPLO,TSO,'N',NZHD,HEADER,
     1  NTS,T,NTS,1,TMIN,TMAX,XSYMB,1.0D0,
     1  NEQ,V,NTS,1,0,HELP1,HELP2,NSYMB,YSCA)
      ELSE
C----       PLOT OF TRAJECTORIES
      CALL PLOTOU(NUCALL,NOPL,ICOPL,KONSP,ICIN,LPLO,TSO,'N',NZHD,HEADER,
     1  NTS,V(1,ICOSP),NTS,1,TMIN,TMAX,XSYMB,1.0D0,
     1  NEQ,V,NTS,1,0,HELP1,HELP2,NSYMB,YSCA)
      ENDIF
      GOTO 100
C***********************************************************************
C     END OF -HORIOU-
C***********************************************************************
      END
      SUBROUTINE HOROU2(NCA,NSPEC,NDIM,NEQ,NSYMB,NFILE8,NFILE6,TSO,
     1     NTS,HELP,HL,T,TEMP,CTOT,PTOT,V,VP,SMIN,SMAX,RPAR,IPAR,NGQL)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *         PROGRAM FOR THE FINAL PRINTOUT OF RESULTS         *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C
C     OUTPUT :
C
C     LIST OF ALL RESULTS OF THE TIME-DEPENDENT INTEGRATION
C
C***********************************************************************
C
C
C
C***********************************************************************
C     CHAPTER I: STORAGE ORGANIZATION
C***********************************************************************
C***********************************************************************
C     I.1: TYPES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      LOGICAL TSO
      LOGICAL MOLEFR,LPLO,MASSFR
      COMMON/BSSPV/SSPV(20*MNSPE)
C***********************************************************************
C     I.2: ARRAYS
C***********************************************************************
      CHARACTER(LEN=*)  NSYMB(NEQ)
      CHARACTER*8 TSYMB,S2
      CHARACTER*28 HEADER
      DIMENSION T(NTS),CTOT(NTS),TEMP(NTS),PTOT(NTS)
      DIMENSION V(NTS,NEQ),VP(NTS,NEQ),SMIN(NEQ),SMAX(NEQ)
      DIMENSION HELP(NEQ+2),HL(NEQ+2)
      DIMENSION RPAR(*),IPAR(*)
C***********************************************************************
C     I.3: COMMON BLOCKS
C***********************************************************************
      COMMON/BDIM/NSMAX,NRMAX,NCRMAX,NPOMAX,NELMAX
      COMMON/BOUT/KONINT,KONTAB,KONPLO,KONSEN,KONMEC,KONANA,KONRED,
     1  KONSPE(MNSPE),KONSID(MNSPE),NSC,
     1  ICRIT,KOGSEN,KONRST,MOLEFR,MASSFR,LPLO
      COMMON/BSENS/LSMAX,NSENS,NRSENS,NISENS,NPSENS,
     1       IRSENS,IISENS,IPSENS,ISENSI(MNSEN)
      DOUBLE PRECISION, DIMENSION(1) :: DXDS,DTDS,DPDS
      DATA NZHD/28/
C***********************************************************************
C     I.4: INITIALIZATION
C***********************************************************************
      DATA TSYMB/'t(s)    '/
C***********************************************************************
C
C     INPUT AND OUTPUT OF CONCENTRATIONS
C
C***********************************************************************
C***********************************************************************
C     INPUT OF CONCENTRATIONS
C***********************************************************************
      IFIRST = 1
      ILAST  = NEQ
      CALL HORIO2(NDIM,IFIRST,ILAST,NFILE8,NTS,T,TEMP,PTOT,CTOT,V)
      NFIL28 = 28
      NFIL29 = 29
C***********************************************************************
C     Input of Rates
C***********************************************************************
C     DO I=1,NTS
C       HELP(1:NEQ) = V(I,1:NEQ)
C       CALL  HORDES(NEQ,T(I),HELP,HL,RPAR,IPAR)
C       VP(I,1:NEQ) = HL(1:NEQ)
C     ENDDO
C***********************************************************************
C     Input of Rates
C***********************************************************************
      IIOPL = 2
C***********************************************************************
C     Output for ILDM
C***********************************************************************
      IF(KONRED.NE.0.or.NGQL.NE.0) THEN
      NFIL21 = 21
      NFIL22 = 22
      REWIND NFIL21
      DO L=1,NTS
      CALL DCOPY(NSPEC,V(L,1),NTS,HELP,1)
      HELP(1:NSPEC) =  V(L,1:NSPEC)
      NNN = NSPEC + 2
      IEQ = IPAR(10)
      CALL TRASSP(-IEQ,NNN,HL,NSPEC,TEMP(L),CTOT(L),
     1       HELP,RHO,HSPEC,PL,SSPV,IERR,0,DXDS,1,DTDS,DPDS)
      WRITE(NFIL21,882) T(L),HL(1:NSPEC+2)
      IF(L.EQ.NTS) THEN
        WRITE(NFIL22,8811) 'H(T0)   ',HL(1)
        WRITE(NFIL22,8811) 'P       ',HL(2)
        DO I=1,NSPEC
        WRITE(NFIL22,8811) NSYMB(I),HL(2+I)
        ENDDO
      ENDIF
 8811 FORMAT(A,':',1PE10.3)
 882  FORMAT(8(1PE14.7,1X))
      ENDDO
      ENDIF
C***********************************************************************
C     TRANSFORM INTO OTHER UNITS
C***********************************************************************
C---- TRANSFORM INTO MOLE FRACTIONS
      IF(MOLEFR) THEN
         HEADER='      Mole fractions        '
         S2=' conc.  '
         DO L=1,NTS
            V(L,1:NSPEC) = V(L,1:NSPEC) / CTOT(L)
         ENDDO
         YSCA = 1.0D0
      ELSE
C---- TRANSFORM INTO MASS FRACTIONS
      IF(MASSFR)THEN
         HEADER='      Mass fractions        '
         S2='h(J/kg) '
         DO L=1,NTS
            CALL DCOPY(NSPEC,V(L,1),NTS,HELP,1)
C
            NNN = NSPEC + 2
            CALL TRASSP(-3,NNN,HL,NSPEC,TEMP(L),CTOT(L),
     1           HELP,RHO,HSPEC,PL,SSPV,IERR,0,DXDS,1,DTDS,DPDS)
            CALL DCOPY(NSPEC,HL(3),1,V(L,1),NTS)
            CTOT(L)=HSPEC
         ENDDO
         YSCA = 1.0D0
      ELSE
C---- LEAVE CONCENTRATIONS
         HEADER=' Concentrations (mol/m**3) '
         S2=' conc.  '
         YSCA =1.D0
      ENDIF
      ENDIF
C---- WRITE OUTPUT
      CALL UPDSPI(KONPLO,NSC,KONSPE,NSCXXX,KONSID,NEQ)
      NNAA = NSPEC +1
      CALL STOPLO(NCA,NNAA,IIOPL,NSCXXX,KONSID,NTS,
     1            HEADER,TSYMB,'  T(K)  ',S2,' P(BAR) ',NSYMB,
     1            PAR,T,TEMP,CTOT,PTOT,V,NFIL28,NFIL29)
C***********************************************************************
C     OUTPUT OF TABLES AND PLOTS
C***********************************************************************
      IF(KONTAB.GT.0.OR.KONPLO.GT.0) THEN
        IF(     TSO)    WRITE(NFILE6,8030)
        IF(.NOT.TSO)    WRITE(NFILE6,8020)
        WRITE(NFILE6,8131) HEADER
      ENDIF
      CALL UPDSPI(KONTAB,NSC,KONSPE,NSCXXX,KONSID,NEQ)
      CALL HOROUL(NSCXXX,KONSID,NFILE6,TSO,
     1         NTS,T,TSYMB,1.0D0,NEQ,V,NSYMB,YSCA)
      NUCALL = 0
      CALL UPDSPI(KONPLO,NSC,KONSPE,NSCXXX,KONSID,NEQ)
      ITFI = 0
      TMIN = T(1)
      TMAX = T(NTS)
      CALL PLOTOU(NUCALL,NOPL,
     1       NSCXXX,KONSID,ITFI,LPLO,TSO,'N',NZHD,HEADER,
     1       NTS,T,NTS,1,TMIN,TMAX,TSYMB,1.0D0,
     1       NEQ,V,NTS,1,0,HL,HELP,NSYMB,YSCA)
C***********************************************************************
C
C     INPUT AND OUTPUT OF SENSITIVITIES (WITH RESP. TO RATE COEFF)
C
C***********************************************************************
      IF(KONSEN.NE.0.AND.NRSENS.GT.0) THEN
C***********************************************************************
C     INPUT OF SENSITIVITIES
C***********************************************************************
      ILHELP=IRSENS+NRSENS-1
      DO 190 JPAR=IRSENS,ILHELP
      IFIRST = NEQ + (JPAR-1) * NEQ + 1
      ILAST  = NEQ + (JPAR-1) * NEQ + NEQ
      CALL HORIO2(NDIM,IFIRST,ILAST,NFILE8,NTS,T,TEMP,PTOT,CTOT,V)
C***********************************************************************
C     NORMALIZATION OF LOCAL SENSITIVITY COEFFICIENTS
C***********************************************************************
      CALL UPDSPI(KONSEN,NSC,KONSPE,NSCXXX,KONSID,NEQ)
      IF(NSCXXX.GT.0) THEN
      DO I=1,NSCXXX
C Alexander Schubert 17.03.2005
        IF (SMAX(KONSID(I)).GT.0.0D0) THEN
          DO L=1,NTS
            V(L,KONSID(I)) = V(L,KONSID(I)) / SMAX(KONSID(I))
          ENDDO
        ELSE
          V(1:NTS,KONSID(I)) = 0.0D0
          WRITE(NFILE6,8132) NSYMB((KONSID(I)))
        ENDIF
Cend Alexander Schubert
      ENDDO
      ENDIF
C***********************************************************************
C     OUTPUT OF SENSITIVITIES
C***********************************************************************
      IF(     TSO) WRITE(NFILE6,8030)
      IF(.NOT.TSO) WRITE(NFILE6,8020)
                   WRITE(NFILE6,8133) ISENSI(JPAR)
      CALL UPDSPI(KONTAB,NSC,KONSPE,NSCXXX,KONSID,NEQ)
      IF(KONTAB.NE.0) CALL HOROUL(NSCXXX,KONSID,NFILE6,TSO,
     1                            NTS,T,TSYMB,1.0D0,NEQ,V,NSYMB,1.0D0)
      NUCALL = 0
      CALL UPDSPI(KONPLO,NSC,KONSPE,NSCXXX,KONSID,NEQ)
      IF(KONPLO.NE.0)  CALL PLOTOU(NUCALL,NOPL,NSCXXX,KONSID,NFILE6,
     1  LPLO,TSO,'N',6,'header',
     1  NTS,T,NTS,1,T(1),T(NTS),TSYMB,1.0D0,
     1   NEQ,V,NTS,1,0,HL,HELP,NSYMB,1.0D0)
  190 CONTINUE
      ENDIF
C***********************************************************************
C     END IN/OUT SENSITIVITIES WITH RESPECT TO RATE COEFFICIENTS
C***********************************************************************
C***********************************************************************
C
C     INPUT AND OUTPUT OF SENSITIVITIES (WITH RESP. TO INIT. VALUES)
C
C***********************************************************************
      IF((NISENS.GT.0).AND.(.NOT.KOGSEN.NE.0)) THEN
C     IF(NISENS.GT.0) THEN
C***********************************************************************
C     INPUT OF SENSITIVITIES
C***********************************************************************
      ILHELP=IISENS+NISENS-1
      DO 290 JPAR=IISENS,ILHELP
      IFIRST = NEQ + (JPAR-1) * NEQ + 1
      ILAST  = NEQ + (JPAR-1) * NEQ + NEQ
      WRITE(*,*) 'HHHTTHHH' ,IFIRST,ILAST
      CALL HORIO2(NDIM,IFIRST,ILAST,NFILE8,NTS,T,TEMP,PTOT,CTOT,V)
      WRITE(*,*) 'HHHHHHHH'
C***********************************************************************
C     OUTPUT OF SENSITIVITIES
C***********************************************************************
      WRITE(NFILE6,8030)
      WRITE(NFILE6,8233) NSYMB(ISENSI(JPAR))
      CALL UPDSPI(KONTAB,NSC,KONSPE,NSCXXX,KONSID,NEQ)
      IF(KONTAB.NE.0)  CALL HOROUL(NSCXXX,KONSID,NFILE6,TSO,
     1                          NTS,T,TSYMB,1.0D0,NEQ,V,NSYMB,1.0D0)
      NUCALL = 0
      CALL UPDSPI(KONPLO,NSC,KONSPE,NSCXXX,KONSID,NEQ)
      IF(KONPLO.NE.0)  CALL PLOTOU(NUCALL,NOPL,NSCXXX,KONSID,NFILE6,
     1    LPLO,TSO,'N',6,'header',
     1     NTS,T,NTS,1,T(1),T(NTS),TSYMB,1.0D0,
     1     NEQ,V,NTS,1,0,HL,HELP,NSYMB,1.0D0)
  290 CONTINUE
      ENDIF
C***********************************************************************
C     END IN/OUT  SENSITIVITIES WITH RESPECT TO INITIAL VALUES
C***********************************************************************
  500 CONTINUE
      RETURN
C***********************************************************************
C     CHAPTER VI: FORMAT STATEMENTS
C***********************************************************************
 8131 FORMAT(' ',20X,3('-'),A28,3('-'))
 8132 FORMAT(6X,3('-'),' Species ',A,' too low concentration,',
     1      ' not good for Normalized Local Sensitivities ',1X,3('-'))
 8133 FORMAT('0',5X,3('-'),' Normalized Local Sensitivities ',
     1      'with Respect to Reaction ',I3,1X,3('-'))
 8233 FORMAT('0',4X,3('-'),' Local Sensitivities ',
     1      'with Respect to Initial Value of ',A,1X,3('-'))
 8020 FORMAT('1 ')
C8010 FORMAT('  ')
 8030 FORMAT('  ',/,'  ',/,'  ')
C***********************************************************************
C     END OF -HOROU2-
C***********************************************************************
      END
      SUBROUTINE HORIDE(INFO,SYM,TSO,TNEW,VNEW,NFILE6,TAU,TTT)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *         PROGRAM FOR THE PRINTOUT OF IGNITION DELAY TIME   *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C     INPUT  :
C       INFO       TASK
C                     0  INITIALIZE
C                     1  UPDATE
C                     2  PRINT
C       SYM        CHARACTER*8 SYMBOL OF VARIABLE CONSIDERED
C       TSO        .true. : output  80 characters per row
C                  .false.: output 133 characters per row
C       TNEW       new value for independent variable
C       VNEW       new value for   dependent variable
C       NFILE6     FORTRAN UNIT FOR OUTPUT
C       TTT        POINT OF MAXIMUM SLOPE
C       TAU        EXTRAPOLATION MAXIMUM SLOPE TO ORDINATE
C
C                |
C                |                      *****
C                |                   *
C                |               *
C                |            *
C                |           *
C                |         *
C                |      *
C                |*****---I---I------------------
C                       TAU  TZZ
C***********************************************************************
C
C
C
C***********************************************************************
C     CHAPTER I: STORAGE ORGANIZATION
C***********************************************************************
C***********************************************************************
C     I.1: TYPES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      LOGICAL TSO
      CHARACTER(LEN=*) SYM
C***********************************************************************
C     X: STORE MAXIMUM AND MINIMUM VALUES OF TIME DERIVATIVES
C***********************************************************************
      COMMON/BIDETI/TMIN,TMAX,DERMIN,DERMAX,TAUMIN,TAUMAX,
     1   TLAS,VLAS,VBEG,TIGC,NC1
      DATA ZERO/0.0D0/,ONE/1.D0/,BIG/1.D30/
C***********************************************************************
C     DECIDE ABOUT TASK
C***********************************************************************
      IF(INFO.EQ.1) GOTO 100
      IF(INFO.EQ.2) GOTO 200
C***********************************************************************
C      SET INITIAL VALUE FOR DETERMINATION OF MAXIMUM DECAY RATES
C***********************************************************************
        NC1=0
        TLAS    =   ZERO
        VLAS    =   VNEW
        VBEG    =   VNEW
        TMAX    =   ZERO
        TMIN    =   ZERO
        TAU     =   ZERO
        TTT     =   ZERO
        DERMIN  =   BIG
        DERMAX  = - BIG
        TAUMIN  = - ONE
        TAUMAX  = - ONE
      RETURN
C***********************************************************************
C      SET INITIAL VALUE FOR DETERMINATION OF MAXIMUM DECAY RATES
C***********************************************************************
  100 CONTINUE
C
      NC1 = NC1+1
      IF(NC1.EQ.1) TIGC=TNEW
C
      DERIV= (VNEW-VLAS)/(TNEW-TLAS)
C---- EVALUATE INCREASING PROFILE
      IF((DERIV.GT.DERMAX).AND.(DERIV.GT.ZERO)) THEN
        TMAX   = TNEW
        DERMAX = DERIV
        TAUMAX = TNEW-(1.0/DERMAX)*(VNEW-VBEG)
C     write(6,*) 'decreasing,tmax,dermax,taumax',tmax,taumax,dermax
      ENDIF
C---- EVALUATE DECREASING PROFILE
      IF((DERIV.LT.DERMIN).AND.(DERIV.LT.ZERO)) THEN
        TMIN   = TNEW
        DERMIN = DERIV
        TAUMIN = TNEW-(ONE/DERMIN)*(VNEW-VBEG)
C     write(6,*) 'increasing,tmin,dermin,taumin',tmin,taumin,dermin
      ENDIF
C---- EVALUATE INDUCTION TIME AND TIME OF MAXIMUM (MINIMUM) SLOPE
      IF((TAUMIN.LE.0.0D0).AND.(TAUMAX.LE.0.0D0)) THEN
        TAU=-1.0D0
      ENDIF
      IF((TAUMIN.GT.0.0D0).AND.(TAUMAX.LE.0.0D0)) THEN
        TAU=TAUMIN
        TTT=TMIN
      ENDIF
      IF((TAUMIN.LE.0.0D0).AND.(TAUMAX.GT.0.0D0)) THEN
        TAU=TAUMAX
        TTT=TMAX
      ENDIF
      IF((TAUMIN.GT.0.0D0).AND.(TAUMAX.GT.0.0D0)) THEN
        IF(TAUMIN.LT.TAUMAX) THEN
           IF(DABS(DERMIN/DERMAX).GT.1.D-3) THEN
             TAU=TAUMIN
             TTT=TMIN
           ELSE
             TAU=TAUMAX
             TTT=TMAX
           ENDIF
        ELSE
           IF(DABS(DERMAX/DERMIN).GT.1.D-3) THEN
             TAU=TAUMAX
             TTT=TMAX
           ELSE
             TAU=TAUMIN
             TTT=TMIN
           ENDIF
        ENDIF
      ENDIF
      TLAS=TNEW
      VLAS=VNEW
C
      RETURN
C***********************************************************************
C     II.7 OUTPUT OF INDUCTION TIME
C***********************************************************************
  200 CONTINUE
      IF(TAU.GE.0.0D0) THEN
      WRITE(NFILE6,82)
      WRITE(NFILE6,81)
C---- TEND TO SMALL
      IF(TTT.GE.0.9*TNEW)  THEN
        WRITE(NFILE6,85) TRIM(SYM)
      ELSE
C---- TEND TO LARGE
        IF(TTT.LE.2.0*TIGC) THEN
          WRITE(NFILE6,86) TRIM(SYM)
        ELSE
C---- TEND GOOD
          WRITE(NFILE6,87) TAU,TRIM(SYM),TTT
        ENDIF
      ENDIF
      WRITE(NFILE6,81)
      ENDIF
      RETURN
C***********************************************************************
C     CHAPTER VI: FORMAT STATEMENTS
C***********************************************************************
   82 FORMAT('1',1X)
   81 FORMAT(' ',1X,/,' ',1X)
   85 FORMAT('0',4X,A,' profile not good for t(ign) determination ',
     F       ' ==> increase t(end)')
   86 FORMAT('0',4X,A,' profile not good for t(ign) determination ',
     F       ' ==> decrease t(end)')
   87 FORMAT(' ','.',77('-'),/,
     1       ' ','|',77(' '),/,
     1       ' ','|      t(ign) = ',1PE9.2,' (from slope of ',
     1              A,' profile at ',1PE9.2,' S)     ',/,
     1       ' ','|',77(' '),/,
     1       ' ','''',77('-'))
C***********************************************************************
C     END OF -HORIDE-
C***********************************************************************
      END
      SUBROUTINE HOROGS(NSPEC,NREAT,NEQ,NSYMB,NFILE6,TSO,
     1       KGSEN,NSC,KONSPE,NVC,SMAX,SMIN)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *         PROGRAM FOR OUTPUT OF GLOBAL SENSITIVITIES        *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C       NSPEC              NUMBER OF SPECIES
C       NREAT              NUMBER OF REACTIONS
C       NEQ                NUMBER OF EQUATIONS
C       NSYMB(NEQ)         CHARACTER*8 SPECIES SYMBOLS
C       NFILE6             FORTRAN UNIT FOR OUTPUT
C       TSO               .true. : output  80 characters per row
C                         .false.: output 133 characters per row
C       KGSEN              control parameter (only 1 makes sense)
C       NSC                number of species considered for output
C       KONSPE(NSC)        pointer for "output species"
C       NVC                number of parameters
C       SMAX(NSC,NVC)      maximum positive sensitivities
C       SMIN(NSC,NVC)      minimum negative sensitivities
C
C     LIST OF ALL RESULTS OF THE TIME-DEPENDENT INTEGRATION
C
C***********************************************************************
C
C
C
C***********************************************************************
C     CHAPTER I: STORAGE ORGANIZATION
C***********************************************************************
C***********************************************************************
C     I.1: TYPES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      LOGICAL TSO
      PARAMETER(LSTR=255)
C***********************************************************************
C     ARRAYS (FORMAL ARGUMENTS)
C***********************************************************************
      DIMENSION SMAX(NSC,NVC),SMIN(NSC,NVC),KONSPE(NSC)
      CHARACTER(LEN=*) NSYMB(NEQ)
C***********************************************************************
C     ARRAYS (LOCAL VARIABLES)
C***********************************************************************
      CHARACTER*1 NPRI(41)
      CHARACTER(LEN=LSTR) STR
C***********************************************************************
C     BLOCK FOR SENSITIVITY DATA
C***********************************************************************
      COMMON/BSENS/LSMAX,NSENS,NRSENS,NISENS,NPSENS,
     1       IRSENS,IISENS,IPSENS,ISENSI(MNSEN)
C***********************************************************************
C     I.4: INITIALIZATION
C***********************************************************************
      DATA CUTL/0.01D0/
C********************************************************************
C
C     LOOP OVER DESIRED SPECIES
C
C********************************************************************
      LENSYM = LEN(NSYMB(1))
      IF(KGSEN.EQ.0) GOTO 700
      DO 600 K=1,NSC
C********************************************************************
C     IV.1 INITIALIZE, WRITE HEADING
C********************************************************************
      KK = KONSPE(K)
C---- WRITE HEAD OF DRAWING
      WRITE(NFILE6,805)
      WRITE(NFILE6,810) NSYMB(KK)
C***********************************************************************
C
C     SENSITIVITY WITH RESPECT TO THE RATE COEFFICIENTS
C
C***********************************************************************
      IF(NRSENS.EQ.0)     GOTO 200
      IF(NREAT.NE.NRSENS) THEN
        WRITE(NFILE6,911)
        GOTO 200
      ENDIF
C***********************************************************************
C     COMPUTE MAXIMUM SENSITIVITY OVER ALL REACTIONS
C***********************************************************************
      SGMAX=0.0
      DO 110 N=1,NRSENS
      IA=IRSENS-1+N
      SGMAX=DMAX1(DMAX1(DABS(SMAX(K,IA)),DABS(SMIN(K,IA))),SGMAX)
  110 CONTINUE
C***********************************************************************
C     WRITE CO-ORDINATE SYSTEM
C***********************************************************************
      SMAXM=-1.0D0
      SMAXP= 1.0D0
      IF(TSO)      WRITE(NFILE6,821) SMAXM,SMAXP
      IF(.NOT.TSO) WRITE(NFILE6,822) SMAXM,SMAXP
C***********************************************************************
C
C     LOOP OVER REACTIONS
C
C***********************************************************************
      DO 190 N=1,NRSENS
C***********************************************************************
C     CHECK FOR VALUE OF SENSITIVITY
C***********************************************************************
      IA=IRSENS-1+N
      IF(DABS(SMAX(K,IA)).GT.DABS(SMIN(K,IA))) THEN
        CRIT = SMAX(K,IA)
      ELSE
        CRIT=  SMIN(K,IA)
      ENDIF
      IF(DABS(CRIT/SGMAX).LT.CUTL) GO TO 190
C***********************************************************************
C     PREPARE OUTPUT,PRINT
C***********************************************************************
      CALL DEFBAR(LBAR,CRIT,SGMAX,NPRI)
      IF(TSO) THEN
        WRITE(NFILE6,831) N,CRIT,NPRI(1:41)
      ELSE
        IML = NMATLL+(N-1)*3
        IMR = NMATLR+(N-1)*3
        CALL DEFREA(NSPEC,NM,NSYMB,IREAC(IML),IREAC(IMR),LSTR,STR)
        WRITE(NFILE6,832) N,STR,CRIT,NPRI(1:41)
      ENDIF
C***********************************************************************
C     END OF LOOP FOR REACTION N
C***********************************************************************
  190 CONTINUE
      IF(     TSO) WRITE(NFILE6,841)
      IF(.NOT.TSO) WRITE(NFILE6,842)
      WRITE(NFILE6,801)
      WRITE(NFILE6,801)
C***********************************************************************
C
C     SENSITIVITY WITH RESPECT TO THE INITIAL VALUES
C
C***********************************************************************
  200 CONTINUE
      IF(NISENS.EQ.0)     GOTO 300
C     IF(NISENS.NE.NSPEC) THEN
C       WRITE(NFILE6,921)
C       write(*,*) NISENS,NSPE,NEQ
C       GOTO 300
C     ENDIF
C***********************************************************************
C     COMPUTE MAXIMUM SENSITIVITY OVER ALL INITIAL VALUES
C***********************************************************************
      SGMAX=0.0
      DO 210 N=1,NISENS
      IA=IISENS-1+N
      SGMAX=DMAX1(DMAX1(DABS(SMAX(K,IA)),DABS(SMIN(K,IA))),SGMAX)
  210 CONTINUE
C***********************************************************************
C     WRITE CO-ORDINATE SYSTEM
C***********************************************************************
      SMAXM=-1.0D0
      SMAXP= 1.0D0
      IF(TSO)         WRITE(NFILE6,821) SMAXM,SMAXP
      IF(.NOT.TSO)    WRITE(NFILE6,822) SMAXM,SMAXP
C***********************************************************************
C
C     LOOP OVER INITIAL VALUES
C
C***********************************************************************
      DO 290 N=1,NISENS
      IA=IISENS-1+N
      IF(DABS(SMAX(K,IA)).GT.DABS(SMIN(K,IA))) THEN
        CRIT = SMAX(K,IA)
      ELSE
        CRIT=  SMIN(K,IA)
      ENDIF
      IF(DABS(CRIT).LT.CUTL) GO TO 290
C***********************************************************************
C     OUTPUT
C***********************************************************************
      KX=ISENSI(IA)
      CALL DEFBAR(LBAR,CRIT,SGMAX,NPRI)
      IF(TSO) THEN
         WRITE(NFILE6,833) NSYMB(KX),CRIT,NPRI(1:41)
      ELSE
         WRITE(NFILE6,834) NSYMB(KX),CRIT,NPRI(1:41)
      ENDIF
C***********************************************************************
C     END OF LOOP FOR INITIAL VALUES
C***********************************************************************
  290 CONTINUE
      IF(     TSO) WRITE(NFILE6,841)
      IF(.NOT.TSO) WRITE(NFILE6,842)
      WRITE(NFILE6,801)
      WRITE(NFILE6,801)
C***********************************************************************
C     END OF LOOP FOR PROFILE K (SPECIES KK)
C***********************************************************************
  300 CONTINUE
  600 CONTINUE
  700 CONTINUE
C***********************************************************************
C     NORMAL RETURN
C***********************************************************************
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
  911 FORMAT(' ',' Global sensitivity with respect to reactions ',
     1           'cannot be computed',/,' ',' -> missing data')
  921 FORMAT(' ',' Global sensitivity with respect to initial values ',
     1           'cannot be computed',/,' ',' -> missing data')
C***********************************************************************
C     CHAPTER VI: FORMAT STATEMENTS
C***********************************************************************
  801 FORMAT('0',2X)
  805 FORMAT('1',2X)
  806 FORMAT('0',2X,/,'0',2X,/,'0',2X)
  810 FORMAT(' ',19X,'Sensitivity analysis with respect to ',A,/)
  821 FORMAT(' ',31X,1PE9.2,17X,'0',12X,1PE9.2,/,
     1       ' ',37X,'I',19('-'),'I',19('-'),'I',/,
     1       ' ',57X,'I')
  822 FORMAT(' ',91X,1PE9.2,16X,'0',14X,1PE9.2,/,
     1       ' ',96X,'I',19('-'),'I',19('-'),'I',/,
     1       ' ',116X,'I')
  831 FORMAT(' ','Reaction ',I4,' :',2X,'S = ',1PE9.2,7X,41(A1))
  832 FORMAT(' ','Reaction ',I4,':',1X,A60,'S = ',1PE9.2,8X,41(A1))
  833 FORMAT(' ',A,':','S = ',1PE9.2,7X,41(A1))
  834 FORMAT(' ',' Initial value of ',A31,':',
     1           15X,'Sensitivity = ',1PE9.2,8X,41(A1))
  841 FORMAT(' ',57X,'I')
  842 FORMAT(' ',116X,'I')
C***********************************************************************
C     END OF -HOROGS-
C***********************************************************************
      END
      SUBROUTINE DEFREA(NSPEC,NM,NSYMB,MATL,MATR,NC,STR)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *   BUILD A CHARACTER STRING CONTAINING A CHEMICAL REACTION *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C       NSPEC          NUMBER OF SPECIES
C       NM             NUMBER OF THIRD BODIES
C       NSYMB(NSPEC)   SPECIES SYMBOLS
C       MATL(3)        POINTERS FOR LEFT  HAND SIDE OF THE EQUATION
C       MATR(3)        POINTERS FOR RIGHT HAND SIDE OF THE EQUATION
C       NC             LENGTH OF CHARACTER STRING STR
C
C     OUTPUT :
C
C       STR(NC)        CHARACTER STRING CONTAINING THE REACTION
C
C    IF     0 < MAT(I) < NSPEC + 1      : SPECIES MAT(I) REACTS
C    IF NSPEC < MAT(I) < NSPEC + NM + 1 : THIRD BODY MAT(I)-NSPEC REACTS
C    IF         MAT(I) > NSPEC + NM     : NO REACTION PARTNER
C
C***********************************************************************
C
C
C
C***********************************************************************
C     STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      CHARACTER(LEN=*)  NSYMB(NSPEC)
      CHARACTER(LEN=1)  APO
      CHARACTER(LEN=NC)  STR
      DIMENSION MATL(3),MATR(3) ,MATLZ(6),IP(6)
      DATA APO/''''/
C***********************************************************************
C     INITIALIZE
C***********************************************************************
      NSNM =NM+NSPEC
      NSNM1=NM+NSPEC+1
      MATLZ(1)=MATL(1)
      MATLZ(2)=MATL(2)
      MATLZ(3)=MATL(3)
      MATLZ(4)=MATR(1)
      MATLZ(5)=MATR(2)
      MATLZ(6)=MATR(3)
      DO I=1,NC
      STR(I:I) = ' '
      ENDDO
      LENSYM = LEN(NSYMB(1))
      IP(1) = 1
      IP(2) = IP(1) + 1 + LENSYM
      IP(3) = IP(2) + 1 + LENSYM
      IP(4) = IP(3) + 5 + LENSYM
      IP(5) = IP(4) + 1 + LENSYM
      IP(6) = IP(5) + 1 + LENSYM
      ILAST = IP(6) + LENSYM-1
      IF(ILAST.GT.NC) THEN
         DO I=1,NC
            STR(I:I) ='-'
         ENDDO
         RETURN
      ENDIF
C***********************************************************************
C     PREPARE FILLERS
C***********************************************************************
      IF(MATLZ(2).LT.NSNM1.AND.MATLZ(2).NE.0) STR(IP(2):IP(2))='+'
      IF(MATLZ(3).LT.NSNM1.AND.MATLZ(3).NE.0) STR(IP(3):IP(3))='+'
                            STR(IP(4)-3:IP(4))=' -> '
      IF(MATLZ(5).LT.NSNM1.AND.MATLZ(5).NE.0) STR(IP(5):IP(5))='+'
      IF(MATLZ(6).LT.NSNM1.AND.MATLZ(6).NE.0) STR(IP(6):IP(6))='+'
C     IF(MATLZ(5).LT.NSNM1.AND.MATLZ(5).NE.0) STR(41:41)='+'
C     IF(MATLZ(6).LT.NSNM1.AND.MATLZ(6).NE.0) STR(50:50)='+'
C***********************************************************************
C     ADD SYMBOLS
C***********************************************************************
      DO 150 KP=1,6
      INC = IP(KP)
      INDM=MATLZ(KP)
      IF(INDM.GT.0.AND.INDM.LE.NSPEC) THEN
        STR(1+INC:LENSYM+INC)=NSYMB(MATLZ(KP))
      ELSE
        IF(INDM.GT.NSPEC.AND.INDM.LE.NSNM) THEN
        INM=INDM-NSPEC-1
        STR(1+INC:1+INC)='M'
C       IF(INM.GT.0) THEN
C          DO I=1,INM
C          STR(I+1+INC:I+1+INC)=APO
C          ENDDO
C        ENDIF
         ENDIF
      ENDIF
  150 CONTINUE
C***********************************************************************
C     NORMAL RETURN
C***********************************************************************
  101 CONTINUE
      LACT = LEN(TRIM(STR))
      IFBB = INDEX(STR(1:LACT),'  ')
      IF(IFBB.GT.0) THEN
      STR(IFBB:LACT-1) = STR(IFBB+1:LACT)
      STR(LACT:LACT) = ' '
      GOTO 101
      ENDIF
C***********************************************************************
C     NORMAL RETURN
C***********************************************************************
      RETURN
C***********************************************************************
C     END OF -DEFREA-
C***********************************************************************
      END
      SUBROUTINE DEFBAR(LBAR,CRIT,RANGE,NPRI)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *         PROGRAM FOR THE SETUP OF BARS FOR BAR CHARTS      *
C     *                                                           *
C     *              AUTHOR: U.MAAS                               *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C     INPUT:
C          CRIT:      VARIABLE FOR THE DEFINITION OF THE BAR
C          RANGE:     SCALING VARIABLE
C
C          THE FOLLOWING MUST HOLD :
C                              -1 < CRIT/RANGE < 1
C
C     OUTPUT:
C
C          NPRI:      A CHARACTER*41 STRING FOR A BAR CHART
C
C     EXAMPLES:
C
C          CRIT/RANGE = -0.5
C          NPRI = '          XXXXXXXXXXI                    '
C
C          CRIT/RANGE  = +0.8
C          NPRI = '                    IXXXXXXXXXXXXXXXX    '
C
C          CRIT/RANGE < -1.02
C          NPRI = '<XXXXXXXXXXXXXXXXXXXI                    '
C
C          CRIT/RANGE >  1.02
C          NPRI = '                    IXXXXXXXXXXXXXXXXXXX>'
C
C    NOT YET USED: LBAR
C***********************************************************************
C***********************************************************************
C     CHAPTER I: STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      CHARACTER*1 NPRI(41)
C********************************************************************
C     CHAPTER II: DEFINE BAR
C********************************************************************
      DO 5 I=1,41
      NPRI(I)=' '
    5 CONTINUE
      NPRI(21)='I'
      DO 10 MM=1,20
      M=21-MM
      NPRI(MM+21)=' '
      IF(CRIT/RANGE.GT.( FLOAT(MM)/20.0-0.02)) NPRI(MM+21)='X'
      NPRI(M)=' '
      IF(CRIT/RANGE.LT.(-FLOAT(MM)/20.0+0.02)) NPRI(M    )='X'
   10 CONTINUE
      IF(CRIT/RANGE.GT. 1.02D0)  NPRI(41)='>'
      IF(CRIT/RANGE.LT.-1.02D0)  NPRI( 1)='<'
C***********************************************************************
C     CHAPTER III: NORMAL EXIT
C***********************************************************************
      RETURN
C***********************************************************************
C     END OF -DEFBAR-
C***********************************************************************
      END
      SUBROUTINE UPDVAR(INFO,N1,N2,S,NSC,IP,V)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *             PROGRAM FOR UPDATING AN ARRAY                 *    *
C     *                                                           *    *
C     *           LAST CHANGE:  OCT 01, 1990 / MAAS               *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C                                                                      *
C     INPUT  :
C
C     INFO            = TASK
C                       INFO = -1 : MIN(S,V) --> V
C                       INFO =  0 : S        --> VAL
C                       INFO = +1 : MAX(S,V) --> V
C     N1              = LEADING DIMENSION OF S
C     N2              = SECOND DIMENSION OF S AND V
C     NSC             = LENGTH OF POINTER
C     IP              = POINTER
C
C     OUTPUT :
C
C          V       WITH V(I,J) <----   S(IP(I),J)
C       I = 1,NSC     J=1,N2
C***********************************************************************
C
C
C
C***********************************************************************
C     I: STORAGE
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION S(N1,N2),V(NSC,N2),IP(NSC)
C***********************************************************************
C     INFO=1
C***********************************************************************
      IF(INFO.EQ.1) THEN
      DO 10 I=1,NSC
      DO 10 J=1,N2
      V(I,J)=DMAX1(V(I,J),S(IP(I),J))
   10 CONTINUE
      GOTO 400
      ENDIF
C***********************************************************************
C     INFO=-1
C***********************************************************************
      IF(INFO.EQ.-1) THEN
      DO 20 I=1,NSC
      DO 20 J=1,N2
      V(I,J)=DMIN1(V(I,J),S(IP(I),J))
   20 CONTINUE
      GOTO 400
      ENDIF
C***********************************************************************
C     INFO NEITHER 1 NOR -1
C***********************************************************************
      DO 30 I=1,NSC
      DO 30 J=1,N2
      V(I,J)=S(IP(I),J)
   30 CONTINUE
      GOTO 400
C***********************************************************************
C     REGULAR EXIT
C***********************************************************************
  400 CONTINUE
      RETURN
C***********************************************************************
C     END OF -UPDVAR-
C***********************************************************************
      END
      SUBROUTINE UPDEXT(INFO,N,S,V)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *             PROGRAM FOR UPDATING AN ARRAY                 *    *
C     *                                                           *    *
C     *           LAST CHANGE:  OCT 01, 1990 / MAAS               *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C                                                                      *
C     INPUT  :
C
C     INFO            = TASK
C                       INFO = -1 : MIN(S,V) --> V
C                       INFO =  0 : S        --> VAL
C                       INFO = +1 : MAX(S,V) --> V
C     N               = LEADING DIMENSIONS OF S AND V
C
C     OUTPUT :
C
C          V       WITH V(I) <----   S(I)
C       I = 1,N
C***********************************************************************
C
C
C
C***********************************************************************
C     I: STORAGE
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION S(N),V(N)
C***********************************************************************
C     INFO=1
C***********************************************************************
      IF(INFO.EQ.1) THEN
      DO 10 I=1,N
      V(I)=DMAX1(V(I),S(I))
   10 CONTINUE
      GOTO 400
      ENDIF
C***********************************************************************
C     INFO=-1
C***********************************************************************
      IF(INFO.EQ.-1) THEN
      DO 20 I=1,N
      V(I)=DMIN1(V(I),S(I))
   20 CONTINUE
      GOTO 400
      ENDIF
C***********************************************************************
C     INFO NEITHER 1 NOR -1
C***********************************************************************
      DO 30 I=1,N
      V(I)=S(I)
   30 CONTINUE
      GOTO 400
C***********************************************************************
C     REGULAR EXIT
C***********************************************************************
  400 CONTINUE
      RETURN
C***********************************************************************
C     END OF -UPDEXT-
C***********************************************************************
      END
      SUBROUTINE HOROUL(NSC,KONS,NFILE6,TSO,
     1        NTS,X,XSYMB,XSCA,NF,Y,YSYMB,YSCA)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *         PROGRAM FOR THE OUTPUT OF LISTINGS                *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C      NSC            number of functions to plot
C      KONS(NSC)      pointers for variables (Y(l,KONS(I)) is plotted
C      NFILE6         fortran unit number for output
C      TSO            .true. : output  80 characters per row
C                     .false.: output 133 characters per row
C      NTS            number of points to be plotted
C      X(NTS)         x-values
C      XSYMB          character*8 symbol for x-axis
C      NF             number of functions in Y
C      Y(NTS,NF)      y-values
C      YSYMB          character*8 symbol for y-axis
C      XSCA,YSCA      scaling factor for the labels (for convenience)
C
C
C***********************************************************************
C
C
C
C***********************************************************************
C     STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      LOGICAL TSO
      PARAMETER (LSEMA=48,NCB=10,NCT=6)
      CHARACTER(LEN=*)  XSYMB,YSYMB(NF)
      CHARACTER(LEN=15)  FILLC
C---- this assumes that species name is not longer than 48 characters
      CHARACTER(LEN=LSEMA)  SEPA
      DIMENSION X(NTS),Y(NTS,*),KONS(*)
C***********************************************************************
C     RETURN FOR ICON = 0
C***********************************************************************
      FILLC(1:15) = '               '
      SEPA(1:LSEMA) = '------------------------------------------------'
      IF(NSC.EQ.0) GOTO 100
C***********************************************************************
C     Get length of character
C***********************************************************************
      LSYM =LEN(YSYMB(1))
      LSYMT = LEN(XSYMB)
      LCHAO = 8
      NSIN = 2+LCHAO+1
      NFIL = 2+LCHAO-10
      NFILT = 2+LCHAO-10
      LSEP=MIN(NSIN,LSEMA)
C***********************************************************************
C     RETURN FOR ICON = 0
C***********************************************************************
                    NCM = NCB
      IF(TSO)       NCM = NCT
      WRITE(NFILE6,800)
C***********************************************************************
C     OUTPUT OF TABLE OF CONCENTRATIONS OF ONLY MARKED SPECIES
C***********************************************************************
      DO 40 I=1,NSC,NCM
      NCOL=MIN0(NCM-1+I,NSC)
        WRITE(NFILE6,821) XSYMB,(YSYMB(KONS(K))(1:8),K=I,NCOL)
        IF(LSYM.GT.8) THEN
        DO II=LCHAO+1,LSYM,LCHAO
         ICL = MIN(II+7,LSYM)
          WRITE(NFILE6,821) REPEAT(' ',LSYMT),
     1    (YSYMB(KONS(K))(II:ICL),K=I,NCOL)
        ENDDO
        ENDIF
        WRITE(NFILE6,811) (SEPA(1:LSEP),II=I,NCOL+1)
        DO J=1,NTS
        IF(NFIL.GT.0) THEN
        WRITE(NFILE6,824) FILLC(1:NFILT),XSCA*X(J),
     1      (FILLC(1:NFIL),YSCA*Y(J,KONS(K)),K=I,NCOL)
        ELSE
        WRITE(NFILE6,823) XSCA*X(J),(YSCA*Y(J,KONS(K)),K=I,NCOL)
        ENDIF
        ENDDO
        WRITE(NFILE6,800)
   40 CONTINUE
C***********************************************************************
C     REGULAR EXIT
C***********************************************************************
      WRITE(NFILE6,800)
  100 CONTINUE
      RETURN
C***********************************************************************
C     CHAPTER VI: FORMAT STATEMENTS
C***********************************************************************
  800 FORMAT(' ',1X)
  810 FORMAT(' ', 77('-'))
  811 FORMAT(' ',11(A))
  821 FORMAT(' ',11(1X,A,1X,'|'))
  812 FORMAT(' ', 77('-'))
  822 FORMAT(' ',121('-'))
  823 FORMAT(' ',11(1PE10.3,'|'))
  824 FORMAT(' ',11(A,1PE10.3,'|'))
C***********************************************************************
C     END OF -HOROUL-
C***********************************************************************
      END
      SUBROUTINE UPDSPI(KONT,NSC,KONSPE,NSCXXX,KONSID,NEQ)
      DIMENSION KONSPE(*),KONSID(*)
C***********************************************************************
C
C
C
C***********************************************************************
      IF(KONT.EQ.0) THEN
        NSCXXX = 0
      ELSE IF(KONT.EQ.1) THEN
        CALL ICOPY(NSC,KONSPE,1,KONSID,1)
        NSCXXX = NSC
      ELSE
        DO 10 I=1,NEQ
        KONSID(I) = I
   10 CONTINUE
        NSCXXX = NEQ
      ENDIF
C***********************************************************************
C     END OF - UPDSPI -
C***********************************************************************
      RETURN
      END
