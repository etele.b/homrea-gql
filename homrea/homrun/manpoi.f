C***********************************************************************
C
C     Module for calculation of manifold points
C       only communication via
C                          COMMON/BINFM/RVMR(5)
C                            these are stored in RV(11)-RV(15) for
C                            convenience 
C      RPAR and IPAR not touched!
C
C***********************************************************************
C***********************************************************************
C
C     Module for communication 
C
C***********************************************************************
      MODULE EGVSC
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      PARAMETER(MNCV=10,MNSS=100)
      DOUBLE PRECISION,SAVE,DIMENSION(MNCV)      :: CSTART,CVCOM,DELCVT
      DOUBLE PRECISION,SAVE,DIMENSION(MNCV*MNSS) :: CROW
      DOUBLE PRECISION,SAVE,DIMENSION(MNSS*MNSS) :: XTROW,XTCOL,
     1                                              ERROW,ERCOL,
     1                                              COTM
      DOUBLE PRECISION,SAVE,DIMENSION(MNSS)      :: DFDTAU,LAMR,LAMI
      DOUBLE PRECISION,SAVE                      :: EVRS,EVRF,EVIS,EVIF
      DOUBLE PRECISION,SAVE                      :: VTARG
      DOUBLE PRECISION,SAVE                      :: BETOLD  
      INTEGER,         SAVE                      :: ITARG
      INTEGER,         SAVE                      :: LNF,IGENI
      INTEGER,         SAVE                      :: NCT,NZEVT,NDIMT
      INTEGER,         SAVE                      :: NMAILD,NLMILD,NOILDM
      END MODULE EGVSC
C***********************************************************************
C
C     Interface between 
C
C***********************************************************************
      SUBROUTINE MANPOI(JOB,NEQ,Y,YP,NC,CV,CVP,LRV,RV,IRET,
     1     LIW,IWORK,LRW,RWORK,RPAR,IPAR,
     1     NZEV,NDIM,LGENI,ICHKD,IMAILD,ILMILD,IOILDM,
     1     BETOLD2,PREVEC,PMATCO,ERVROW,ERVCOL,QEVB,
     1     AR2,AI2,XTRD,XTCD)
C***********************************************************************
C
C     ************************************************************
C     *                                                          *
C     *    Interface for the Determination of points of the      *
C     *            Low-dimensional manifold                      *
C     *                                                          *
C     ************************************************************
C
C***********************************************************************
C      Common block for user communication:
C     
C                          COMMON/BINFM/RVMR(5)
C                            these are stored in RV(11)-RV(15)
C
C
C      Input values:
C
C        JOB           : control parameter  JOB=1: update weird points
C        NEQ           : dimension of the state space (ns+2)
C        NC            : number of controling variables
C        AR,AI         : eigenvalues
C        CV(NC)        : controlling variables
C                        points of the manifold are determined
C                        subject to the constraint  CM * Y = CVAR
C      * Y(NEQ)        : initial guess for the state space
C      * RPAR(*)       : real parameter array
C      * IPAR(*)       : integer parameter array
C
C      Work arrays:
C
C        IWORK(LIW)    : integer work array
C        RWORK(LRW)    : real work array
C        LIW           : length of integer work array
C        LRW           : length of real work array
C
C
C
C      Output values:
C
C        Y(NEQ)      : point on the manifold
C        YP(NEQ)     : corresponding rate of change
C        RV(LRV)       : information for further use
C                        RV( 1) : return code
C                        RV( 2) : local temperature
C                        RV( 3) : local density or local pressure
C                        RV( 4) : eigenvalue NC   (real part)
C                        RV( 5) : eigenvalue NC   (imaginary part)
C                        RV( 6) : eigenvalue NC+1 (real part)
C                        RV( 7) : eigenvalue NC+1 (imaginary part)
C                        RV(11) - RV(15) returns from MASRES
C        note: IWORK, RWORK can be used arbitrarily in your calling
C              routine, internal check for storage is performed
C              never change RPAR or IPAR, this leads to fatal errors
C              it is necessary, that at least one call to the subroutine
C              lowman had been performed in order to initialize RPAR and
C              IPAR
C
C***********************************************************************
      USE EGVSC
C***********************************************************************
C     STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      PARAMETER (ONE=1.0D0,ZERO=0.0D0)
      EXTERNAL MASRES,MASJAC
C***********************************************************************
C     Arguments       
C***********************************************************************
      DOUBLE PRECISION, DIMENSION(NEQ)       :: Y,YP,PREVEC,AR2,AI2
      DOUBLE PRECISION, DIMENSION(NEQ*NEQ)   :: ERVROW,ERVCOL,QEVB,
     1                                          XTRD,XTCD
      DOUBLE PRECISION, DIMENSION(LRV)       :: RV         
      DOUBLE PRECISION, DIMENSION(NC)        :: CV,CVP     
      DOUBLE PRECISION, DIMENSION(NC*NEQ)   :: PMATCO    
      DIMENSION RPAR(*),IPAR(*)
C***********************************************************************
C     Work arrays  
C***********************************************************************
      DIMENSION RWORK(LRW),IWORK(LIW) 
C***********************************************************************
C     Work arrays  
C***********************************************************************
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)   :: AVL,AVR,SCHUR,XJAC
C***********************************************************************
C     Common Block for user communication with masres
C***********************************************************************
      COMMON/BINFM/RVMR(5)
C***********************************************************************
C     Common Blocks
C***********************************************************************
      DATA NFILE6/06/
C***********************************************************************
C
C     initial preparations
C
C***********************************************************************
      IGENI = LGENI
      NZEVT = NZEV
      NDIMT = NDIM
      NCT = NC 
      RV= 0
      TIME = 1.D36
      IPRI = 2
C***********************************************************************
C
C     Store information for local variables
C
C***********************************************************************
C***********************************************************************
C     Store information for scalars
C***********************************************************************
      NMAILD = IMAILD
      NLMILD = ILMILD
      NOILDM = IOILDM
      BETOLD = BETOLD2
C***********************************************************************
C     Store information for global arrays 
C***********************************************************************
      ERROW(1:NEQ*NEQ)  = ERVROW(1:NEQ*NEQ)
      ERCOL(1:NEQ*NEQ)  = ERVCOL(1:NEQ*NEQ)
      COTM(1:NEQ*NEQ)   = QEVB(1:NEQ*NEQ)
      CROW(1:NC*NEQ)    = PMATCO(1:NEQ*NC) 
C***********************************************************************
C
C     call the manifold routine
C
C***********************************************************************
C***********************************************************************
C     compute the whole reaction space
C***********************************************************************
      IF(NC.EQ.NEQ) THEN
      RV(1:LRV) = 0
      MDIM = NC
      RDIM = 0 
      IPRI = 0 
      CALL EGVBAS(1,0,MDIM,RDIM,NZEVT,NEQ,MASRES,MASJAC,TIME,Y,
     1            ERVCOL,ERVROW,XTRD,XTCD,                              
     1            AR2,AI2,AVR,AVL,XJAC,SCHUR,
     1            RPAR,IPAR,IPRI,NFILE6,IERBAS)
      RV( 1) = FLOAT(IERBAS)
C     IRET = -50 
      IRET = IERBAS
C---- USED for return codes
      INEG = 50 
      GOTO 5000
C***********************************************************************
C
C     compute the low-dimensional manifold
C
C***********************************************************************
      ELSE IF(NC.GE.NZEVT) THEN
         JOBM = ICHKD    
         IPRI = 2 
         CALL EGVSOL(JOBM,NEQ,NC,NZEVT,
     1        Y,CV,PMATCO,PREVEC,
     1        LIW,IWORK,LRW,RWORK,
     1        RPAR,IPAR,IPRI,NFILE6,NFILE6,IERR,INEG,iegtag)
C***********************************************************************
C     store back eigenvalue and eigenspace information
C***********************************************************************
        AR2(1:NEQ)      = LAMR(1:NEQ)
        AI2(1:NEQ)      = LAMI(1:NEQ)
        XTRD(1:NEQ*NEQ) = XTROW(1:NEQ*NEQ)
        XTCD(1:NEQ*NEQ) = XTCOL(1:NEQ*NEQ)
        IRET = IERR
        RV = 0 
        RV( 1) = FLOAT(IRET)
        RV( 2) = IEGTAG 
        RV( 4) = AR2(NC)
        RV( 5) = AI2(NC)
        RV( 6) = AR2(1  +NC)
        RV( 7) = AI2(1  +NC)
        RV( 8) = (AR2(1+NC)-AR2(NC))/MIN(AR2(1+NC),-1.D0)
        RV( 9) = MAX(MAXVAL(AR2(1:NC)),ZERO)/MAX(ABS(AR2(NC+1)),1.D-10)
        IF(INEG.NE.0) THEN
          RV(10) = FLOAT(-INEG) 
        ELSE
          RV(10) = FLOAT(LNF) 
      ENDIF
C***********************************************************************
C     
C***********************************************************************
      ELSE
        GOTO 900
      ENDIF
C***********************************************************************
C     compute needed return values
C***********************************************************************
 5000 CONTINUE
      CALL MASRES(NEQ,TIME,Y,YP,RPAR,IPAR,IEMR)
C---- COMPUTE LOCAL CHANGE OF parameters
      CALL DGEMV('N',NC,NEQ,ONE,PMATCO,NC,YP,1,ZERO,CVP,1)
      RV(11:15) = RVMR(1:5)
C***********************************************************************
C     Regular exit
C***********************************************************************
      RETURN
C***********************************************************************
C     Error exits
C***********************************************************************
 900  CONTINUE
      WRITE(NFILE6,901)
 901  FORMAT(' ',' fatal error detected ')
      GOTO 999
 999  CONTINUE
      WRITE(NFILE6,998)
 998  FORMAT(' ',' error occurred in -MANPOI- ')
      STOP
C***********************************************************************
C     END OF -MANPOI-
C***********************************************************************
      END
      SUBROUTINE EGVSOL(IDUU,NEQ,NC,NZEV,SMF,CV,PMATCO,PREVEC,
     1       LIW,IWORK,LRW,RWORK,RPAR,IPAR,
     1       IPRI,NFILE6,NFIERR,IERRCO,INEG,IERR)
C***********************************************************************
C
C     ************************************************************
C     *                                                          *
C     *   Subroutine for the determination of the state space    *
C     *                                                          *
C     ************************************************************
C
C
C***********************************************************************
      USE EGVSC
C***********************************************************************
C     STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL CHKNEG,GETDOM
      PARAMETER  (ONE=1.0D0,ZERO=0.0D0)
C***********************************************************************
C     actual parameters
C***********************************************************************
      DIMENSION SMF(NEQ)
      DIMENSION IWORK(LIW),RWORK(LRW)
      DIMENSION CV(NC)
      DIMENSION RPAR(*),IPAR(*)
      DIMENSION PMATCO(NC,NEQ),PREVEC(NEQ)
C***********************************************************************
C     local variables 
C***********************************************************************
      IF(NC.GT.MNCV) GOTO 900
C***********************************************************************
C
C     Initialization of controls for solvers
C
C***********************************************************************
      IPRI = 3
                     GETDOM = .FALSE.
      IF(IDUU.GE.2)  GETDOM = .TRUE.
                     CHKNEG = .FALSE.
      IF(IDUU.GE.1)  CHKNEG = .TRUE.
C***********************************************************************
C
C
C     Initialization of pointers and flags
C
C
C***********************************************************************
C---- Set error flag 
      IERR=0
C***********************************************************************
C
C     Initialize the variables
C
C***********************************************************************
C---- calculate current values of the controlling variables
      CALL DGEMV('N',NC,NEQ,ONE,PMATCO,NC,SMF,1,ZERO,CSTART,1)
      DELCVT(1:NC)  = CV(1:NC)-CSTART(1:NC)
C---- initialize CVCOM
      CVCOM(1:NC) = CV(1:NC)
C---- DELCVT and CSTART are fixed during egvsol 
C***********************************************************************
C     Check whether starting values are within the domain
C***********************************************************************
      IF(GETDOM) THEN
        CALL CHKDOM(0,NEQ,SMF,SMF,INEG,VHIT,DUMMY)
        IF(INEG.NE.0) WRITE(6,*) ' WHY DID THIS HAPPEN? Nr.1'
      ENDIF
C***********************************************************************
C     decide about task
C***********************************************************************
C     GOTO 1000
      ICMETH = 2
      IF(ICMETH.EQ.1) THEN
        GOTO 1000
      ELSE 
        GOTO 2000
      ENDIF
C***********************************************************************
C
C     CONTINUATION METHODS ALCON AND PITCON
C
C***********************************************************************
 1000 CONTINUE
                 INFO = 0
      IF(GETDOM) INFO = 1
      CALL EGVCOA(INFO,NEQ,NC,SMF,
     1       LIW,IWORK,LRW,RWORK,RPAR,IPAR,IPRI,NFILE6,INEG,IERR)
C***********************************************************************
C     Check return code
C***********************************************************************
      IF(IERR.LE.0) THEN 
        IERRCO = 0
      ELSE IF(IERR.EQ.1.OR.IERR.EQ.2) THEN
        IERRCO = -1
      ELSE
        IERRCO =  5 
      ENDIF
      GOTO 5000
C***********************************************************************
C
C     Block for own continuation method
C
C***********************************************************************
 2000 CONTINUE
                 INFO = 0
      IF(GETDOM) INFO = 1
      CALL EGVCON(IDUU,
     1    INFO,NEQ,NC,NZEV,SMF,PREVEC,PMATCO,
     1       LIW,IWORK,LRW,RWORK,RPAR,IPAR,1,NFILE6,INEG,IERR)
C***********************************************************************
C     Check return code 
C***********************************************************************
      IF(IERR.EQ.0) THEN
        IERRCO =  0
      ELSE IF(IERR.EQ.1.OR.IERR.EQ.2) THEN
        IERRCO = -1
      ELSE IF(IERR.EQ.3.OR.IERR.EQ.5.OR.IERR.EQ.7) THEN
        IERRCO =  1
      ELSE IF(IERR.EQ.8.OR.IERR.EQ.9) THEN
        IERRCO =  5
      ELSE IF(IERR.EQ.-1) THEN
        IERRCO = -2
        WRITE(6,*) 'ierrco',ierrco
      ELSE IF(IERR.EQ.10) THEN
        WRITE(*,*) 'WHY DID THIS HAPPEN? Nr.2'
        IERRCO = -3
      ELSE
        WRITE(6,850)
        STOP
      ENDIF
      GOTO 5000
C***********************************************************************
C
C     ITERATIVE SOLUTION
C
C***********************************************************************
 5000 CONTINUE
      IF(CHKNEG.AND.IERRCO.EQ.0) THEN
        CALL CHKDOM(0,NEQ,SMF,SMF,INEG,VHIT,DUMMY)
        IF(INEG.NE.0) THEN
          IERRCO = -1
        ENDIF
      ENDIF
      IF(IPRI.GT.0) WRITE(NFILE6,840) IERRCO
C***********************************************************************
C     REGULAR EXIT
C***********************************************************************
      RETURN
C***********************************************************************
C     FORMAT STATEMENTS
C***********************************************************************
  840 FORMAT(' ','   Analysis completed with flag',I3)
  850 FORMAT(' Unknown flag ',/,
     1       ' Error in -EGVSOL- ')
C***********************************************************************
C     Error exits     
C***********************************************************************
  900 CONTINUE
      WRITE(*,*) ' NC > MNCV in EGVSOL, fatal error'
      STOP
C***********************************************************************
C     END OF -EGVSOL-
C***********************************************************************
      END
      SUBROUTINE TARGET(N,Y,RPAR,IPAR,NFILE6,IERR)
C***********************************************************************
C
C
C***********************************************************************
C***********************************************************************
C     STORAGE ORGANIZATION
C***********************************************************************
      USE EGVSC
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      EXTERNAL TARRES,TARJAC
C***********************************************************************
C     Passed Arguments 
C***********************************************************************
      DIMENSION Y(N)
      DIMENSION RPAR(*),IPAR(*)
C***********************************************************************
C     Local Variables 
C***********************************************************************
      DOUBLE PRECISION, DIMENSION(N+1)   :: XSCAL
      PARAMETER (LIW=1000,LRW=10000,MNEQU=1000)
      DIMENSION IWORK(LIW),RWORK(LRW)
C***********************************************************************
C     Solver Control 
C***********************************************************************
      DIMENSION ICONLE(50,3),INTNLE(50,3),REANLE(50,3),
     1          ATONLE(3),EPSTE(3),SCINI(3),ICOSO(50)
C***********************************************************************
C     Constants
C***********************************************************************
      PARAMETER  (ONE=1.0D0,ZERO=0.0D0)
C***********************************************************************
C     Initial values
C***********************************************************************
      DATA ICONLE/150*0/,INTNLE/150*0/,REANLE/150*ZERO/
C***********************************************************************
C     Controls for NLEQ1 fast version
C***********************************************************************
C---- Simplified Jacobian supplied
      ICONLE(3,1) = 1
C---- printing of messages
      ICONLE(11,1) = 1
      ICONLE(13,1) = 0
C---- do ordinary newton iteration
CNNN was decommented
C     ICONLE(33,1) = 1
C---- onestep mode
      ICONLE(2,1)  = 1
C---- Maximum number of Newton Steps (this might be increased)
      INTNLE(31,1) = 30
CNNN was decommented
C     INTNLE(31,1) = 10
C---- Required accuracy
      ATONLE(1)    = 1.D-5
      SCINI(1)     = 1.D-2
C_TOL EPSTE(1)     = 1.D+0
      EPSTE(1)     = 1.D-6
C***********************************************************************
C     Controls for NLEQ1 in case of previous failure
C***********************************************************************
C---- feedback ist ein dreck  ---> macht ganze konvergenz kaputt!!!
      ICONLE(3,2) = 2
C---- printing of messages
C     ICONLE(11,2) = 1
C     ICONLE(13,2) = 2
      ICONLE(11,2) = 1
      ICONLE(13,2) = 2
C---- do ordinary newton iteration
      ICONLE(33,2) = 1
C---- Maximum number of Newton Steps
      INTNLE(31,2) = 30
C---- onestep mode
      ICONLE(2,2) = 1
C---- initial damping factor
      REANLE(21,2) = 1.00
C---- minimum damping factor
      REANLE(22,2) = 0.001
C---- Required accuracy
      ATONLE(2)    = 1.D-4
      SCINI(2)     = 1.D-2
      EPSTE(2)     = 1.D+0
C***********************************************************************
C     
C***********************************************************************
      IUSECO = 1
      ITRIED = 0
  100 CONTINUE
C***********************************************************************
C     Preparations for Newton Solver
C***********************************************************************
      CALL ICOPY(50,ICONLE(1,IUSECO),1,ICOSO,1)
      CALL ICOPY(50,INTNLE(1,IUSECO),1,IWORK,1)
      CALL DCOPY(50,REANLE(1,IUSECO),1,RWORK,1)
      ATT = ATONLE(IUSECO)
      CALL SMSET(N,EPSTE(IUSECO),SCINI(IUSECO),Y,XSCAL)
C***********************************************************************
C     Call of Newton Solver
C***********************************************************************
  444 CONTINUE
      CALL NLEQ1(N,TARRES,TARJAC,Y,XSCAL,ATT,ICOSO,IERR,
     $      LIW,IWORK,LRW,RWORK,RPAR,IPAR)
      IF(IERR.LT.0) GOTO 444
      write(6,*) 'ierr in target point',ierr
      IF(IERR.GT.0) THEN
        IF(ITRIED.EQ.0) THEN
        ITRIED = 1
        IUSECO = 2
        GOTO 100
        ENDIF
      ENDIF
      RETURN
C***********************************************************************
C     END OF -EGUSOL-
C***********************************************************************
      END
      SUBROUTINE COREGV(INFO,NEQ,NC,SMFOLD,SMFNEW,
     1       XSCAL,LIW,IWORK,LRW,RWORK,RPAR,IPAR,IPRI,MSGFIL,IEFFO,IERR)
C***********************************************************************
C
C     ************************************************************
C     *                                                          *
C     *   Corrector step for the determination of the manifold   *
C     *                                                          *
C     ************************************************************
C
C
C***********************************************************************
C***********************************************************************
C     STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      EXTERNAL SNLRES,SNLJAC,EULRES
      LOGICAL GETDOM
C***********************************************************************
C     Input and output arrays
C***********************************************************************
      DIMENSION SMFOLD(NEQ),SMFNEW(NEQ),XSCAL(NEQ+1)
C***********************************************************************
C     Work arrays
C***********************************************************************
      DOUBLE PRECISION, DIMENSION(NEQ) :: SMFBNL 
      DIMENSION IWORK(LIW),RWORK(LRW)
C***********************************************************************
C     Arrays for user communication 
C***********************************************************************
      DIMENSION RPAR(*),IPAR(*)
C***********************************************************************
C     Arrays for solver control
C***********************************************************************
      DIMENSION ICOSO(50)
      DIMENSION ICONLE(50,3),INTNLE(50,3),REANLE(50,3)
      DIMENSION ATONLE(3),EPSTE(3),SCINI(3)
      DIMENSION ATOLIM(1),RTOLIM(1)
C***********************************************************************
C     Constants
C***********************************************************************
      PARAMETER  (ONE=1.0D0,ZERO=0.0D0)
C***********************************************************************
C     Initial values
C***********************************************************************
      DATA ICONLE/150*0/,INTNLE/150*0/,REANLE/150*ZERO/
C***********************************************************************
C
C     Initialization of controls for solvers
C
C***********************************************************************
                    GETDOM = .FALSE.
      IF(INFO.GT.0) GETDOM = .TRUE.
      NX = NEQ-NC
C***********************************************************************
C     Controls for NLEQ1 fast version
C***********************************************************************
      MNTRY = 2
      IF(IEFFO.EQ.0) MNTRY = 1
C***********************************************************************
C     Controls for NLEQ1 fast version
C***********************************************************************
C---- Simplified Jacobian supplied
      ICONLE(3,1) = 1
C---- Simplified Jacobian is the identity 
C     ICONLE(4,1) = 1    funktioniert nur, wenn A als band matrix 
C     ICONLE(6,1) = 0
C     ICONLE(7,1) = 0
C---- printing of messages
      ICONLE(11,1) = 1
      ICONLE(13,1) = 0
C---- multistep mode
      ICONLE(2,1)  = 1
C---- Maximum number of Newton Steps (this might be increased)
      INTNLE(31,1) = 20
C---- initial damping factor
C     REANLE(21,1) = 1.00
C---- minimum damping factor
C     REANLE(22,1) = 0.001 
C---- Required accuracy
      ATONLE(1)    = 1.D-4
      SCINI(1)     = 1.D-1
      EPSTE(1)     = 1.D-1
      EPS          = 1.D-3
C***********************************************************************
C     Controls for NLEQ1 in case of previous failure
C***********************************************************************
C---- feedback ist ein dreck  ---> macht ganze konvergenz kaputt!!!
      ICONLE(3,2) = 2
C---- printing of messages
      ICONLE(11,2) = 1
      ICONLE(13,2) = 0
C                    ^ was 3
C---- Maximum number of Newton Steps
C-was 
      INTNLE(31,2) = 30
C---- multistep mode
      ICONLE(2,2) = 1
cjoergb ****
C---- rel. pert. for jacobian in N1INT
C     REANLE(26,2) = 1.D-4
C---- min. pert. for jacobian in N1INT
C     REANLE(27,2) = 1.D-5
cjoergb ****
C---- initial damping factor
      REANLE(21,2) = 1.00
C---- minimum damping factor
      REANLE(22,2) = 0.001 
C---- Required accuracy
      ATONLE(2)    = 1.D-4
      SCINI(2)     = 1.D-1
      EPSTE(2)     = 1.D-1
      EPS          = 1.D-3
C***********************************************************************
C     Controls for LIMEX
C***********************************************************************
C---- tolerances
      RTOLIM(1) = 1.D-3
      ATOLIM(1) = 1.D-7
C***********************************************************************
C
C
C     Initialization of pointers and flags
C
C
C***********************************************************************
C---- Set error flag 
      IERR=0
C***********************************************************************
C
C     Initialize the variables
C
C***********************************************************************
C---- copy SMFOLD into SMFNEW
      CALL DCOPY(NEQ,SMFOLD,1,SMFNEW,1)
C---- copy SMFOLD into SMFBNL
      CALL DCOPY(NEQ,SMFOLD,1,SMFBNL,1)
C***********************************************************************
C
C     Check whether starting values are within the domain
C
C***********************************************************************
      IF(GETDOM) THEN
        CALL CHKDOM(0,NEQ,SMFOLD,SMFOLD,INEG,VHIT,DUMMY)
        IF(INEG.NE.0) GOTO 9700
      ENDIF 
C***********************************************************************
C     Output of values before iteration
C***********************************************************************
      IF(IPRI.GE.3) WRITE(MSGFIL,8011) (SMFOLD(I),I=1,NEQ)
      GOTO 4000
C***********************************************************************
C
C
C
C     Perform Corrector Step using Pseudo-Time integration 
C
C
C
C***********************************************************************
 3000 CONTINUE
C***********************************************************************
C     INITIALIZE VARIABLES
C***********************************************************************
      ICALL=0
      TEND   = 1.D3
      TIMDUM = 0.D0
      HZ     = 1.D-4
      HMAXZ  = 1.D20
      NZC=1
      NZV=NX*NEQ
      H = 0.001
      HZ= 0.01
      HZ= 0.01
C***********************************************************************
C     CALL INTEGRATOR
C***********************************************************************
      NITEUL = 0
 3200 CONTINUE
      NITEUL = NITEUL + 1 
C---- approximate Jacobian
      KFLAX = 0
      ICALX = 1
      RTOLX = 1.D-3
      HMAXZ  = 1.D20
      CALL EULREL(NEQ,EULRES,TIMDUM,SMFNEW,
     1   TEND,RTOLX,HMAXZ,HZ,KFLAX,ICALX,RPAR,IPAR)
      IF(KFLAX.LT.0.OR.NITEUL.GT.300) THEN     
         WRITE(MSGFIL,*) ' error in eulrel'
         GOTO 9400
      ENDIF
C---- CHECK  ETURN CODE
      IF(TIMDUM.LE.0.999D0*TEND) GOTO 3200
      GOTO 9000
C***********************************************************************
C     END OF ONE TIME INTEGRATION
C***********************************************************************
C***********************************************************************
C
C     Perform Corrector Step using Newton's method
C
C     Back iteration using simplified Jacobian:
C     zuerst wird die vereinfachte version genommen. bei problemen
C     wird dann auf die bessere umgeschaltet.
C     Wenn gleich die richtige Jacobimatrix genommen wird, kann es
C     zu Problemen mit reduktion des Daempfungsfaktors kommen.
C     Allerdings nur selten. Normalerweise werden 2-3 Iterationen
C
C***********************************************************************
 4000 CONTINUE 
      ITRIED = 1
      NITER  = 0
C***********************************************************************
C
C***********************************************************************
 4500 CONTINUE
C***********************************************************************
C     Preparations for Newton Solver
C***********************************************************************
      CALL ICOPY(50,ICONLE(1,ITRIED),1,ICOSO,1)
      CALL ICOPY(50,INTNLE(1,ITRIED),1,IWORK,1)
      CALL DCOPY(50,REANLE(1,ITRIED),1,RWORK,1)
      ATT = ATONLE(ITRIED)
      CALL SMSET(NEQ,EPSTE(ITRIED),SCINI(ITRIED),SMFBNL,XSCAL)
C***********************************************************************
C
C     Newton Solver Loop
C
C***********************************************************************
 4550 CONTINUE
      CALL NLEQ1(NEQ,SNLRES,SNLJAC,SMFNEW,XSCAL,ATT,ICOSO,IERR,
     $      LIW,IWORK,LRW,RWORK,RPAR,IPAR)
      NITER = NITER + 1
 5000 CONTINUE
C***********************************************************************
C
C     Check return codes .GE. 0
C
C***********************************************************************
C***********************************************************************
C     successful iteration
C***********************************************************************
      IF(IERR.EQ.0) THEN
        GOTO 9000
C***********************************************************************
C     successful step, but not yet terminated
C***********************************************************************
      ELSE IF(IERR.LT.0) THEN
        CALL DCOPY(NEQ,SMFNEW,1,SMFBNL,1)
C     IF(GETDOM) THEN
C     CALL CHKDOM(0,NEQ,SMFNEW,SMFNEW,INEG,VHIT,DUMMY)
C     IF(INEG.NE.0) THEN
C      write(6,*) ' outside domain during nleq1'
C       GOTO 9800 
C     ENDIF
C     ENDIF
        GOTO 4550
C***********************************************************************
C     fatal errors
C***********************************************************************
      ELSE IF(IERR.EQ.1.OR.IERR.GT.5) THEN
        CALL DCOPY(NEQ,SMFBNL,1,SMFNEW,1)
        GOTO 9800
C***********************************************************************
C     recoverable errors (convergence problems), continue with newton
C***********************************************************************
      ELSE 
        ITRIED = ITRIED + 1
C---- give it a further try
        IF(ITRIED.LE.MNTRY) GOTO 4500
C---- stepsize too small or too many iterations 
        IF(IERR.EQ.2.OR.IERR.EQ.3) THEN
          GOTO 9400
        ELSE 
          GOTO 9100
        ENDIF
      ENDIF
C***********************************************************************
C
C     Returns      
C
C***********************************************************************
C***********************************************************************
C     Sucessful iteration (SMFBNL=SMFNEW=final result)
C***********************************************************************
 9000 CONTINUE
      IERR = 0
      RETURN
C***********************************************************************
C     Conv. problems, but solution obtained (SMFBNL=SMFNEW=final result)
C***********************************************************************
 9100 CONTINUE
      IF(IPRI.GE.1) WRITE(MSGFIL,9101) 
      IF(IPRI.GE.2) WRITE(MSGFIL,8011) (SMFOLD(I),I=1,NEQ)
      IF(IPRI.GE.2) WRITE(MSGFIL,8300) (SMFNEW(I),I=1,NEQ)
 9101 FORMAT(' ',' Convergence problems, but solution obtained')
      IERR = 1
      RETURN    
C***********************************************************************
C     Conv. problems, solution not obtained (SMFBNL=SMFNEW=final result)
C***********************************************************************
 9400 CONTINUE
      IF(IPRI.GE.1) WRITE(MSGFIL,9401) 
      IF(IPRI.GE.2) WRITE(MSGFIL,8011) (SMFOLD(I),I=1,NEQ)
      IF(IPRI.GE.2) WRITE(MSGFIL,8300) (SMFNEW(I),I=1,NEQ)
 9401 FORMAT(' ',' Convergence problems, solution not yet obtained')
      IERR = 2
      RETURN    
C***********************************************************************
C     Newton method failed (SMFBNL = SMFNEW = output of last succ. step)
C***********************************************************************
 9800 CONTINUE
      IF(IPRI.GE.1) WRITE(MSGFIL,9801) IERR
 9801 FORMAT(' ',' Fatal error (ierr=',I3,') during newton iteration ')
      IERR = 3
      RETURN    
C***********************************************************************
C     Conv. problems, but solution obtained (SMFBNL=SMFNEW=final result)
C***********************************************************************
 9700 CONTINUE
      IF(IPRI.GE.1) WRITE(MSGFIL,9701) 
 9701 FORMAT(' ',' Variables outside allowed domain on entry to COREGV')
      IERR = 9
      RETURN    
C***********************************************************************
C     Fatal errors that should not happen
C***********************************************************************
 9900 CONTINUE
      WRITE(MSGFIL,9998)
 9998 FORMAT(' Fatal error in COREGV, this should not happen')
      STOP 
C***********************************************************************
C
C     FORMAT STATEMENTS
C
C***********************************************************************
 8011 FORMAT(' Values before the iteration       ',/,7(1X,1PE10.3))
 8012 FORMAT(' Aimed values of cont. variables   ',/,7(1X,1PE10.3))
 8300 FORMAT(' Values after iteration in COREGV  ',/,7(1X,1PE10.3))
C***********************************************************************
C     END OF -COREGV-
C***********************************************************************
      END
      SUBROUTINE EGVCON(IDUU,INFO,NEQ,NC,NZEV,SMF,PREVEC,PMATCO,
     1      LIW,IWORK,LRW,RWORK,RPAR,IPAR,IPRI,MSGFIL,INEG,IERR)
C***********************************************************************
C
C     ************************************************************
C     *                                                          *
C     *                                                          *
C     *                                                          *
C     *                                                          *
C     *                                                          *
C
C***********************************************************************
C***********************************************************************
C     STORAGE ORGANIZATION
C***********************************************************************
      USE EGVSC
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL ONESTE,PAVAIL,TEST,GETDOM,INTPRE,STERED,EVSEP
      EXTERNAL PATRES,EULRES
C***********************************************************************
C     Inp/Out
C***********************************************************************
      DIMENSION SMF(NEQ),PREVEC(NEQ),PMATCO(*)
      DIMENSION IWORK(LIW),RWORK(LRW)
      DIMENSION RPAR(*),IPAR(*)
C***********************************************************************
C     Local Arrays 
C***********************************************************************
      DOUBLE PRECISION, DIMENSION(NEQ)   :: SMFBEU,SMFHEL,SMFNEW
      DOUBLE PRECISION, DIMENSION(NEQ+1) :: XSCAL,SMFTAR 
      DIMENSION RTOL(1)
C***********************************************************************
C     Constants 
C***********************************************************************
      PARAMETER  (ONE=1.0D0,ZERO=0.0D0)
C***********************************************************************
C     Data 
C***********************************************************************
      DATA RTOL(1)/1.D-3/
cjoergb      DATA FACMIN/1.D-3/
      DATA FACMIN/2.D-4/
      DATA REDFAC/0.25D0/
C***********************************************************************
C
C     Initialization of controls for solvers
C
C***********************************************************************
      TEST=.FALSE.
      INTPRE = .FALSE.
C     beide versionen funktionieren gleich
C
C     IF(INFO.GT.0)   INTPRE=.TRUE.
C     INTPRE = .TRUE.
                       GETDOM = .TRUE.
      IF(INFO.EQ.0)    GETDOM = .FALSE.
      STERED = .TRUE.
      IF(IDUU.LE.1) FACMIN = 1.0D-1
C***********************************************************************
C
C
C     Initialization of pointers and flags
C
C
C***********************************************************************
C---- Set error flag 
      IERR=0
      INEG = 0
C***********************************************************************
C
C     Check whether starting values are within the domain
C
C***********************************************************************
      IF(GETDOM) THEN
      CALL CHKDOM(0,NEQ,SMF,SMF,INEG,VHIT,DUMMY)
      IF(INEG.NE.0) GOTO 9010 
      ENDIF
C***********************************************************************
C
C     Initialize the variables
C
C***********************************************************************
C---- save SMF
      CALL DCOPY(NEQ,SMF,1,SMFBEU,1)
C---- calculate current values of the controlling variables
C***********************************************************************
C
C     Initialize pathfollowing variables
C
C***********************************************************************
      FAC = 1.0
      TAU    = ZERO
      TAUOLD = ZERO
      TEND   = ONE
      H      = TEND - TAU
C***********************************************************************
C     Jump directly to newton solver if norm of DELCVT is small        
C***********************************************************************
      SUMDEL = DNRM2(NC,DELCVT,1)
      IF(SUMDEL.LT.1.D-12) THEN
        TAU = TEND
        GOTO 4400
      ENDIF
C***********************************************************************
C     SET CONTROLS FOR SOLVER
C***********************************************************************
      ICALL  = -1
C***********************************************************************
C
C     Start loop for pathfollowing
C
C***********************************************************************
      FAC = 1.0
      ONESTE = .TRUE.
      IJ4200 = 0
 4200 CONTINUE
      HMAX = 1.D30
C***********************************************************************
C     
C***********************************************************************
      IF(IJ4200.GT.0.AND.(.NOT.INTPRE)) THEN
        PREVEC(1:NEQ) = (SMF(1:NEQ)-SMFBEU(1:NEQ)) / (TAU-TAUOLD)
      ENDIF
C     write(*,*) smfbeu(1:neq),'ooooooooooo'
C***********************************************************************
C     Save values befor predictor-step
C***********************************************************************
      TAUOLD = TAU
      CALL DCOPY(NEQ,SMF,1,SMFBEU,1)
C***********************************************************************
C
C
C     Perform predictor step
C
C
C***********************************************************************
C***********************************************************************
C
C     Predictor
C
C***********************************************************************
C     Check if a predictor estimate is available
c***********************************************************************
      SNPRE = SQRT(DOT_PRODUCT(PREVEC(1:NEQ),PREVEC(1:NEQ)))
      PAVAIL = SNPRE.GT.1.D-30 
C***********************************************************************
C     predict according to parameterization matrix
C***********************************************************************
      FAC = MIN(FAC,TEND-TAU)
      IF(.NOT.INTPRE.AND.PAVAIL) THEN
        TAU = TAU + FAC
        FACOLD = FAC
C         CALL PATRES(NEQ,TDDD,SMF,PREVEC,RPAR,IPAR,IFC)
          CALL DAXPY(NEQ,FAC,PREVEC,1,SMF,1)
          CVCOM(1:NC) = TAU * DELCVT(1:NC)  + CSTART(1:NC)
      ELSE
C***********************************************************************
C     predict according to approximate integration
C***********************************************************************
      KFLAG = 0
      TINT = TAU + FAC 
      H = FAC
      IXCALL = 0
      write(*,*) 'EULEX as predictor'
      CALL EULEX(NEQ,PATRES,TAU,SMF,TINT,RTOL(1),HMAX,H,KFLAG,IXCALL,
     1      RPAR,IPAR)
C     write(*,*) 'EULEX done        ',SMF(1:NEQ)
      IF(KFLAG.LT.0) THEN     
        GOTO 9140
        CALL DCOPY(NEQ,SMFBEU,1,SMF,1)
        FAC = FAC *REDFAC 
        IF(FAC.GT.FACMIN) THEN
           TAU = TAUOLD
           IF(IPRI.GE.1) WRITE(MSGFIL,8110) TAUOLD,TAU+FAC
           IJ4200 = 0
           GOTO 4200
        ENDIF
      ENDIF
      ICALL =  1
      ENDIF
C***********************************************************************
C
C     Check result of predictor 
C
C***********************************************************************
      IF(GETDOM) THEN
        CALL CHKDOM(1,NEQ,SMFBEU,SMF,INEG,VHIT,ALPHA)
        IF(INEG.GT.0) VTH = VHIT      
C***********************************************************************
C     wrong value in variables before predictor, this is fatal
C***********************************************************************
        IF(INEG.LT.0) THEN 
          GOTO 9010
C***********************************************************************
C     values outside of domain after predictor 
C***********************************************************************
        ELSE IF(INEG.GT.0) THEN
          ONESTE = .FALSE.
C***********************************************************************
C     use linear interpolation at target point 
C***********************************************************************
          IF(.NOT.STERED) THEN
C---- target value already in SMF (after call to chkdom)  
            TAU = TAUOLD + ALPHA * (TAU-TAUOLD)
            IF(IPRI.GE.1) WRITE(MSGFIL,8105) TAU
C***********************************************************************
C           no progress made, seems to be a target point
C***********************************************************************
              TAU = TAUOLD + ALPHA * (TAU-TAUOLD)
              IF(ALPHA.LT.1.D-2) THEN
                CALL DCOPY(NEQ,SMF,1,SMFTAR,1)   
                SMFTAR(NEQ+1) = TAU
                N1 = NEQ+1 
                VTARG = VTH 
CRRRRRRRRRRRR
                write(*,*)'call of -TARGET- eith INEG =',INEG 
c
                INEGT = INEG
                ITARG = INEG
                CALL TARGET(N1,SMFTAR,RPAR,IPAR,MSGFIL,IERR)
                IF(IERR.NE.0) THEN
                  CALL DCOPY(NEQ,SMFBEU,1,SMF,1)
                  GOTO 9210
                ELSE 
                  CALL DCOPY(NEQ,SMFTAR,1,SMF,1)
                  TAUTAR = SMFTAR(NEQ+1)
                  CALL CHKDOM(0,NEQ,SMF,SMF,INEG,VHIT,ALPHA)
                  IF(INEG.GT.0) THEN
                    CALL DCOPY(NEQ,SMFBEU,1,SMF,1)
                    GOTO 9230
                  ELSE
                    INEG = INEGT
                    GOTO 9430
                  ENDIF
                ENDIF
              ENDIF
C***********************************************************************
C     reduce stepsize of predictor
C***********************************************************************
          ELSE 
            FAC = FAC *REDFAC 
C***********************************************************************
C     reduction possible
C***********************************************************************
            IF(FAC.GT.FACMIN) THEN
              TAU = TAUOLD
              CALL DCOPY(NEQ,SMFBEU,1,SMF,1)
              IF(IPRI.GT.1) WRITE(MSGFIL,8110) TAUOLD,TAU+FAC
              IJ4200 = 0
              GOTO 4200
            ELSE
C***********************************************************************
C     further reduction not allowed. last chance: target point
C***********************************************************************
              IF(IPRI.GE.1) WRITE(MSGFIL,8115)
              TAU = TAUOLD + ALPHA * (TAU-TAUOLD)
              CALL DCOPY(NEQ,SMF,1,SMFTAR,1)
C---             something that works is last successful step
              CALL DCOPY(NEQ,SMFBEU,1,SMFTAR,1)
              SMFTAR(NEQ+1) = TAUOLD 
              N1 = NEQ+1 
              VTARG = VTH  
C             write(6,*) 'smffff',(smftar(i),i=1,NEQ+1)
              write(6,*) 'INEG =',INEG
CRRRRRRRRRRR
              INEGT = INEG
              ITARG = INEG 
              CALL TARGET(N1,SMFTAR,RPAR,IPAR,MSGFIL,IERR)
              IF(IERR.NE.0) THEN
                CALL DCOPY(NEQ,SMFBEU,1,SMF,1)
                GOTO 9210
              ELSE 
                CALL DCOPY(NEQ,SMFTAR,1,SMF,1)
                TAUTAR = SMFTAR(NEQ+1)
                CALL CHKDOM(0,NEQ,SMF,SMF,INEG,VHIT,ALPHA)
                IF(INEG.GT.0) THEN
                  CALL DCOPY(NEQ,SMFBEU,1,SMF,1)
                  GOTO 9230
                ELSE
                  INEG = INEGT
                  GOTO 9430
                ENDIF
              ENDIF
            ENDIF
          ENDIF
        ELSE 
        ENDIF
C---- if we are here, the predictor step was successful
      ENDIF
C***********************************************************************
C
C     store results after the predictor step
C
C***********************************************************************
C----- faster version, newton only at the end of one integration
      IF(ONESTE.AND.TAU.LT.TEND) GOTO 4200
      CALL DGEMV('N',NC,NEQ,ONE,PMATCO,NC,SMF,1,ZERO,CVCOM,1)
      IF(TEST) THEN
        IF(IPRI.GE.1) WRITE(MSGFIL,8013) TAU,( CVCOM(I),I=1,NC)
      ENDIF
 8013 FORMAT(' Predicted values of cont. variables at tau=',1PE10.3,/,
     1     7(1X,1PE10.3))
C***********************************************************************
C     Iterate back
C***********************************************************************
 4400 CONTINUE
C***********************************************************************
C
C     Newton Solver Loop
C
C***********************************************************************
      IEFFO = 1
      IF(IDUU.LE.1) IEFFO = 0
C                   IEFFO = 0
          CALL DCOPY(NEQ,SMF,1,SMFHEL,1)
      CALL COREGV(1,NEQ,NC,SMF,SMFNEW,
     1        XSCAL,LIW,IWORK,LRW,RWORK,RPAR,IPAR,2,MSGFIL,IEFFO,IERR)
C***********************************************************************
C     Check for eigenvalue separation   
C***********************************************************************
      EVSEP = .FALSE.
      IF(EVSEP.AND.NC.GT.NZEV) THEN
        IF(IPRI.GE.2) WRITE(MSGFIL,9608) 'Slow',EVRS,EVIS
 9608 FORMAT(1X,A4,' eigenvalue = ',1PE10.3,' + i * ',1PE10.3)
        IF(IPRI.GE.2) WRITE(MSGFIL,9608) 'Fast',EVRF,EVIF
         IF(DABS(EVRF/EVRS).LT.1.2D0) THEN
C   
C         CALL DCOPY(NEQ,SMFNEW,1,SMF,1)
C         GOTO 9435
C   
          N1 = NEQ+1
         CALL DCOPY(NEQ,SMFNEW,1,SMFTAR,1)
          SMFTAR(NEQ+1) = TAU
         CALL CROSS(N1,SMFTAR,RPAR,IPAR,MSGFIL,IERR)
                IF(IERR.NE.0) THEN
                  CALL DCOPY(NEQ,SMFBEU,1,SMF,1)
                  GOTO 9210
                ELSE 
                  CALL DCOPY(NEQ,SMFTAR,1,SMF,1)
                  TAUTAR = SMFTAR(NEQ+1)
                  CALL CHKDOM(0,NEQ,SMF,SMF,INEG,VHIT,ALPHA)
                  IF(INEG.GT.0) THEN
                    CALL DCOPY(NEQ,SMFBEU,1,SMF,1)
                    GOTO 9230
                  ELSE
                    GOTO 9430
                  ENDIF
                ENDIF
         ENDIF
      ENDIF
C***********************************************************************
C     Check for error
C***********************************************************************
      IF(IERR.GT.1) THEN
         TAU = TAUOLD
         CALL DCOPY(NEQ,SMFBEU,1,SMF,1)
         IF(FAC.GT.FACMIN) THEN
            FAC = FAC * REDFAC 
            ONESTE = .FALSE.
            IF(IPRI.GE.1) WRITE(MSGFIL,*)
     1      ' stepsize reduction due to corrector, tauold =',tauold,
     1      '  taunew=',TAU+FAC
            IJ4200 = 0
            GOTO 4200
         ELSE
            IF(IPRI.GE.1) WRITE(MSGFIL,*) 
     1      ' stepsize red. terminated in corrector'
            GOTO 9510
         ENDIF
      ENDIF 
C***********************************************************************
C     Check for fast eigenvalue becoming too slow
C***********************************************************************
      IF(1.EQ.0) THEN
      IF(NC.GT.NZEV) THEN
      EVLIM = -0.D5
        IF(EVRF.GT.EVLIM) THEN
          IF(IPRI.GE.1) WRITE(MSGFIL,*) 
     1       ' fast eigenvalue too small'
          CALL DCOPY(NEQ,SMFNEW,1,SMF,1)
          GOTO 9440
        ENDIF  
      ENDIF
      ENDIF
C***********************************************************************
C     Check for negative mass fractions 
C***********************************************************************
      IF(GETDOM) THEN
        IF(IPRI.GE.3) WRITE(MSGFIL,9602)
     1                    'SMFBEU',(SMFBEU(I),I=1,NEQ)
        IF(IPRI.GE.3) WRITE(MSGFIL,9602) 
     1                    'SMFNEW  ',(SMFNEW(I),I=1,NEQ)
        CALL DCOPY(NEQ,SMFNEW,1,SMFTAR,1)
        CALL CHKDOM(1,NEQ,SMFBEU,SMFTAR,INEG,VHIT,ALPHA)
        IF(INEG.GT.0) VTH = VHIT      
        IF(INEG.GT.0) THEN
          IF(FAC.GT.FACMIN) THEN
          IF(IPRI.GE.1) WRITE(MSGFIL,9602) 'SMFBEU  ',
     1          (SMFBEU(I),I=1,NEQ)
          IF(IPRI.GE.1) WRITE(MSGFIL,9602) 'SMFHEL  ',
     1          (SMFHEL(I),I=1,NEQ)
          IF(IPRI.GE.1) WRITE(MSGFIL,9602) 'SMFNEW  ',
     1          (SMFNEW(I),I=1,NEQ)
          ONESTE = .FALSE.
          TAU = TAUOLD
          CALL DCOPY(NEQ,SMFBEU,1,SMF,1)
          FAC = FAC * REDFAC 
            IF(IPRI.GE.1) WRITE(MSGFIL,*)
     1       ' NLEQ1 crossed domain, returned to predictor '
              IJ4200 = 0
            GOTO 4200
          ENDIF 
          write(6,*) 'alpha',alpha
          IF(IPRI.GE.1) WRITE(MSGFIL,*) 
     1       ' NLEQ1 crossed domain, SMF adjusted'
          write(6,*) 'ineg',ineg
          IF(IPRI.GE.3) WRITE(MSGFIL,9602) 'SMFTAR  ',
     1          (SMFTAR(I),I=1,NEQ)
          SMFTAR(NEQ+1) = (TAUOLD + ALPHA * (TAU-TAUOLD))
          CALL DCOPY(NEQ,SMFBEU,1,SMFTAR,1)
          write(6,*) tauold,tauold,'tau',smftar(neq+1)
          N1 = NEQ+1
          VTARG = VTH   
          INEGT = INEG
          ITARG = INEG
          CALL TARGET(N1,SMFTAR,RPAR,IPAR,MSGFIL,IERR)
          IF(IERR.NE.0) THEN
            CALL DCOPY(NEQ,SMFBEU,1,SMF,1)
            GOTO 9410
          ELSE 
            CALL DCOPY(NEQ,SMFTAR,1,SMF,1)
            TAUTAR = SMFTAR(NEQ+1)
          ENDIF
          CALL CHKDOM(0,NEQ,SMF,SMF,INEG,VHIT,ALPHA)
          IF(INEG.GT.0) THEN
            GOTO 9420
C-neutarget
            CALL DCOPY(NEQ,SMFNEW,1,SMF,1)
          ELSE 
            INEG = INEGT
            GOTO 9430
          ENDIF 
        ENDIF
      ENDIF
C***********************************************************************
C
C     sucessful exit from NLEQ1
C
C***********************************************************************
 4900 CONTINUE
      CALL DCOPY(NEQ,SMFNEW,1,SMF,1)
C***********************************************************************
C     Continue loop if TEND has not yet been reached
C***********************************************************************
      IJ4200 = IJ4200 + 1
      IF(TAU.LT.TEND) THEN
        GOTO 4200
        FAC = FAC*4.0D0
      ENDIF
      IERR = 0
      RETURN
C***********************************************************************
C
C     ERROR EXITS
C
C***********************************************************************
 9140 CONTINUE
      IF(IPRI.GE.1) WRITE(MSGFIL,9141) 
      IF(IPRI.GE.2) WRITE(MSGFIL,9601) (SMF(I),I=1,NEQ)
 9141 FORMAT(' ',' Predictor failed ,',/, 
     1           ' SMF contains values before last successful',
     1           ' predictor step')
      IERR = 9
      RETURN    
 9210 CONTINUE
      IF(IPRI.GE.1) WRITE(MSGFIL,9211) 
      IF(IPRI.GE.2) WRITE(MSGFIL,9601) (SMF(I),I=1,NEQ)
 9211 FORMAT(' ',' Predictor crossed domain repeatedly, but a ',
     1           'target point could not be found',/,
     1           ' SMF contains values before last successful',
     1           ' predictor step')
      IERR = 8
      RETURN    
 9510 CONTINUE
      IF(IPRI.GE.1) WRITE(MSGFIL,9511) 
      IF(IPRI.GE.2) WRITE(MSGFIL,9601) (SMF(I),I=1,NEQ)
 9511 FORMAT(' ',' Corrector failed and no further stepsize',
     1           ' reduction was possible',/, 
     1           ' SMF contains values before last successful',
     1           ' predictor-corrector step')
      IERR = 7
      RETURN    
 9230 CONTINUE
      IF(IPRI.GE.1) WRITE(MSGFIL,9231) TAUTAR,INEG
      IF(IPRI.GE.2) WRITE(MSGFIL,9601) (SMF(I),I=1,NEQ)
 9231 FORMAT(' ',' Target point found at tau=',1PE10.3,
     1           ' with INEG=',I3,
     1           'but new values outside domain, too',/,
     1           ' SMF contains values before last successful',
     1           ' predictor-corrector step')
      IERR = 5
      RETURN
 9410 CONTINUE
      IF(IPRI.GE.1) WRITE(MSGFIL,9411) 
      IF(IPRI.GE.2) WRITE(MSGFIL,9601) (SMF(I),I=1,NEQ)
 9411 FORMAT(' ',' Corrector crossed domain, but target point,',
     1           ' determination failed, SMF updated to output',
     1           ' before predictor')
      IERR = 3
      RETURN
 9420 CONTINUE
      IF(IPRI.GE.1) WRITE(MSGFIL,9421) TAUTAR
      IF(IPRI.GE.2) WRITE(MSGFIL,9601) (SMF(I),I=1,NEQ)
 9421 FORMAT(' ',' Corrector crossed domain at tau=',1Pe10.3,
     1           ', target point found',
     1           ' but target point outside of domain, too.',/,
     1           ' SMF contains values before target search')
C-newtarget
C    1           ' SMF contains target point')
      IERR = 2
      RETURN
 9430 CONTINUE
      IF(IPRI.GE.1) WRITE(MSGFIL,9431) TAUTAR,INEG
      IF(IPRI.GE.2) WRITE(MSGFIL,9601) (SMF(I),I=1,NEQ)
 9431 FORMAT(' ',' Predictor or corrector crossed domain at tau=,',
     1           1PE10.3,' with INEG=',I3,', target point found',/,
     1           ' SMF contains target point')
      IERR = 1
      RETURN
C9435 CONTINUE
C     IF(IPRI.GE.1) WRITE(MSGFIL,9436) TAUTAR
C     IF(IPRI.GE.2) WRITE(MSGFIL,9601) (SMF(I),I=1,NEQ)
C9436 FORMAT(' ',' eigenvalue separation to small at tau=,',
C    1           1PE10.3,'  ',/,
C    1           ' SMF contains last successful pred/cor step')
C     IERR = 1
C     RETURN
 9440 CONTINUE
      IF(IPRI.GE.1) WRITE(MSGFIL,9441) 
      IF(IPRI.GE.2) WRITE(MSGFIL,9601) (SMF(I),I=1,NEQ)
 9441 FORMAT(' ',' Fast eigenvalue too slow ')
      IERR = -1
      RETURN
C***********************************************************************
C     
C***********************************************************************
 9010 CONTINUE
      IF(IPRI.GE.1) WRITE(MSGFIL,9011) INEG
 9011 FORMAT('Input values to EGVCON outside allowed domain',
     1    ' according to constraint',I3)
      IERR = 10
      RETURN
C***********************************************************************
C     FORMAT STATEMENTS
C***********************************************************************
 8110 FORMAT(' stepsize reduction due to predictor,',
     1       ' tauold =',1PE10.3,'  taunew=',1PE10.3)
 8011 FORMAT(' Starting values of cont. variables',/,7(1X,1PE10.3))
 9601 FORMAT(' Result after pathfollowing:       ',/,7(1X,1PE10.3))
 9602 FORMAT(' Current value of ',A,':'          ,/,7(1X,1PE10.3))
 8012 FORMAT(' Aimed values of cont. variables   ',/,7(1X,1PE10.3))
  897 FORMAT(A24,1PE9.2,A7,1PE9.2,A1)
 8105 FORMAT(' values of predictor corr. according to lin. interp.',
     1       1PE12.5)
 8115 FORMAT(' predictor stepsize termination, last chance: target  ')
C***********************************************************************
C     END OF -EGVCON-
C***********************************************************************
      END
      SUBROUTINE EGVCOA(INFO,NEQ,NC,SMF,
     1       LIW,IWORK,LRW,RWORK,RPAR,IPAR,IPRI,MSGFIL,INEG,IERR)
C***********************************************************************
C
C     ************************************************************
C     *                                                          *
C     *                                                          *
C     *                                                          *
C     *                                                          *
C     *                                                          *
C
C***********************************************************************
C***********************************************************************
C     STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      EXTERNAL PITRES,ALCRES,DENSLV
      LOGICAL GETDOM,USEPIT
      DIMENSION SMF(NEQ)
      DOUBLE PRECISION, DIMENSION(NEQ) :: SMFNEW,XSCAL
      DIMENSION IWORK(LIW),RWORK(LRW)
C***********************************************************************
C     Solver controls      
C***********************************************************************
      DIMENSION RPAR(*),IPAR(*)
C***********************************************************************
C     Solver controls      
C***********************************************************************
      DIMENSION ICOALC(10),ICOPIT(10) 
      DIMENSION RCOALC(10),RCOPIT(10) 
      DIMENSION ICOSO(20)
C***********************************************************************
C     Constants            
C***********************************************************************
      PARAMETER  (ONE=1.0D0,ZERO=0.0D0)
C***********************************************************************
C
C
C     Initialization of solver control variables
C
C
C***********************************************************************
      GETDOM = .FALSE.
      IF(INFO.GT.0) GETDOM = .TRUE.
      USEPIT = .FALSE.
C***********************************************************************
C     Continuation method alcon
C***********************************************************************
      DO 110 I=1,10
        ICOALC(I) = 0
        RCOALC(I) = ZERO
  110 CONTINUE
C---- PRINT
      ICOALC(1) =  1
C---- MAX NO OF STEPS
      ICOALC(2) =  50
C---- pathfollowing direction
      ICOALC(3) =  1
C---- CHECK critical points
C     ICOALC(4) = 1
C---- CHECK turning point and bifurcation points
      ICOALC(4) = 3
C---- Analytical approximate Jacobian
      ICOALC(10) = 1
C---- Numerical Jacobian
C     ICOALC(10) = 0
C***********************************************************************
C     Continuation method Pitcon
C***********************************************************************
      DO 120 I=1,10
        ICOPIT(I) = 0
        RCOPIT(I) = ZERO
  120 CONTINUE
      ICOPIT(1) = 0
C---- INITIAL INDEX FOR PATHFOLLOWING PARAMETER
      ICOPIT(2) = NEQ + 1
C---- DO NOT FIX CONTINUATION PARAMETER
      ICOPIT(3) = 0
C---- OUTPUT
      ICOPIT(7) = 3
      ICOPIT(9) = 1
C---- INDEX OF TARGET POINT
      ICOPIT(5) = 0
      RCOPIT(1) = 1.0D-3
      RCOPIT(2) = 1.0D-0
      RCOPIT(5) = 1.0D-2* DABS(TAUMIN-TAUMAX)
      RCOPIT(6) = ONE
C***********************************************************************
C
C
C     Initialization of pointers and flags
C
C
C***********************************************************************
C---- Set error flag 
      IERR=0
C***********************************************************************
C
C     Check whether starting values are within the domain
C
C***********************************************************************
      IF(GETDOM) THEN
      CALL CHKDOM(0,NEQ,SMF,SMF,INEG,VHIT,DUMMY)
      IF(INEG.NE.0) GOTO 9010 
      ENDIF
C***********************************************************************
C
C     Initialize the variables
C
C***********************************************************************
C---- save SMF
      CALL DCOPY(NEQ,SMF,1,SMFNEW,1)
C***********************************************************************
C     Setup initial scaling
C***********************************************************************
C---- INITIAL SCALING
      ATT = 1.D-0
      CALL SMSET(NEQ,ONE,ATT,SMF,XSCAL)
C***********************************************************************
C
C     Initialize pathfollowing variables
C
C***********************************************************************
      TAU    =  ZERO 
      TAUMIN = -ONE   
      TAUMAX =  ONE   
C***********************************************************************
C
C     Continuation method ALCON
C
C***********************************************************************
      EPS =1.D-3
      CALL ICOPY(10,ICOALC,1,ICOSO,1)
      write(6,*) ' alcon1 called'
      CALL ALCON1 (ALCRES,NEQ,SMFNEW,XSCAL,TAU,
     1   TAUMIN,TAUMAX,EPS,ICOSO,RWORK,LRW,IWORK,LIW,RPAR,IPAR)
      IERR = ICOSO(5)
C***********************************************************************
C     Check error code
C***********************************************************************
      IF(IERR.EQ.0) THEN
        IF(DABS(TAU-TAUMAX).GT.1.D-5) GOTO 9210
        CALL DCOPY(NEQ,SMFNEW,1,SMF,1)
        GOTO 6000
      ELSE IF(IERR.EQ.-1) THEN
        CALL DCOPY(NEQ,SMFNEW,1,SMF,1)
        GOTO 9430
      ELSE IF(IERR.EQ.-9) THEN
        GOTO 9410
      ELSE IF(IERR.GT.0) THEN
        GOTO 9140
      ENDIF
C***********************************************************************
C
C     Continuation method PITCON 
C
C***********************************************************************
C     DO 220 I=1,LIW
C     IWORK(I) = 0
C 220 CONTINUE
C     DO 230 I=1,LRW
C     RWORK(I) = ZERO
C 230 CONTINUE
C     numerical determination of the jacobian 
C     IWORK(9) = 1
C     CALL ICOPY(10,ICOPIT,1,IWORK,1)
C     CALL DCOPY(10,RCOPIT,1,RWORK,1)
C     NOVA = NEQ + 1
C2100 CONTINUE
C     SMFNEW(NOVA) = TAU
C     CALL PITCON(JAC,RPAR,PITRES,IERROR,IPAR,IWORK,LIW,NOVA,RWORK,
C    *LRW,SMFNEW,DENSLV)
C     IERR = IWORK(1)
C     TAU = SMFNEW(NOVA)
C     WRITE(6,*) '  INFO  FLAG FROM PITCON', IWORK(1), ' TAU =',TAU
C     WRITE(6,*) '  ERROR FLAG FROM PITCON', IERROR
C     IF(IERR.NE.3) GOTO 2100 
 6000 CONTINUE 
      IERR = 0
      RETURN
C***********************************************************************
C
C     ERROR EXITS
C
C***********************************************************************
 9140 CONTINUE
      IF(IPRI.GE.1) WRITE(MSGFIL,9141) 
      IF(IPRI.GE.2) WRITE(MSGFIL,9601) (SMF(I),I=1,NEQ)
 9141 FORMAT(' ',' Pathfollowing failed ')   
      IERR = 9
      RETURN    
 9210 CONTINUE
      IF(IPRI.GE.1) WRITE(MSGFIL,9211) 
      IF(IPRI.GE.2) WRITE(MSGFIL,9601) (SMF(I),I=1,NEQ)
 9211 FORMAT(' ',' Pathfollowing failure, possibly a turning ',
     1           ' point was overjumped')
      IERR = 8
      RETURN    
 9410 CONTINUE
      IF(IPRI.GE.1) WRITE(MSGFIL,9411) 
      IF(IPRI.GE.2) WRITE(MSGFIL,9601) (SMF(I),I=1,NEQ)
 9411 FORMAT(' ',' Corrector crossed domain, but target point,',
     1           ' determination failed, SMF updated to output',
     1           ' of corrector')
      IERR = 3
      RETURN
 9430 CONTINUE
      IF(IPRI.GE.1) WRITE(MSGFIL,9431) 
      IF(IPRI.GE.2) WRITE(MSGFIL,9601) (SMF(I),I=1,NEQ)
 9431 FORMAT(' ',' Predictor or corrector crossed domain',
     1           ', target point found',/,
     1           ' SMF contains target point')
      INEG = 999
      IERR = 1
      RETURN
C***********************************************************************
C     
C***********************************************************************
 9010 CONTINUE
      IF(IPRI.GE.1) WRITE(MSGFIL,9011) 
 9011 FORMAT('Input values to EGVCON outside allowed domain')
      IERR = 10
      RETURN
C***********************************************************************
C     FORMAT STATEMENTS
C***********************************************************************
 8011 FORMAT(' Starting values of cont. variables',/,7(1X,1PE10.3))
 9601 FORMAT(' Result after pathfollowing:       ',/,7(1X,1PE10.3))
 8012 FORMAT(' Aimed values of cont. variables   ',/,7(1X,1PE10.3))
C***********************************************************************
C     END OF -EGVCON-
C***********************************************************************
      END
      SUBROUTINE CROSS(N,Y,RPAR,IPAR,NFILE6,IERR)
C***********************************************************************
C
C
C***********************************************************************
C***********************************************************************
C     STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      EXTERNAL CRORES,JAC
      DIMENSION Y(N)
      PARAMETER (LIW=1000,LRW=10000)
      DIMENSION IWORK(LIW),RWORK(LRW)
      DOUBLE PRECISION, DIMENSION(N+1) :: XSCAL  
      DIMENSION RPAR(*),IPAR(*)
C---- LOCAL
      DIMENSION ICONLE(50,3),INTNLE(50,3),REANLE(50,3),
     1          ATONLE(3),EPSTE(3),SCINI(3),ICOSO(50)
C***********************************************************************
C     Constants
C***********************************************************************
      PARAMETER  (ONE=1.0D0,ZERO=0.0D0)
C***********************************************************************
C     Initial Values 
C***********************************************************************
      DATA ICONLE/150*0/,INTNLE/150*0/,REANLE/150*ZERO/
C***********************************************************************
C     Controls for NLEQ1 in case of previous failure
C***********************************************************************
C---- feedback ist ein dreck  ---> macht ganze konvergenz kaputt!!!
      ICONLE(3,1) = 2
C---- printing of messages
      ICONLE(11,1) = 1
      ICONLE(13,1) = 3
C---- Maximum number of Newton Steps
      INTNLE(31,1) = 30
C---- onestep mode
      ICONLE(2,1) = 0
C---- initial damping factor
      REANLE(21,1) = 1.00
C---- minimum damping factor
      REANLE(22,1) = 0.001
C---- Required accuracy
      ATONLE(1)    = 1.D-4
      SCINI(1)     = 1.D+0
C***********************************************************************
C     
C***********************************************************************
      IUSECO = 1
C***********************************************************************
C     Preparations for Newton Solver
C***********************************************************************
      CALL ICOPY(50,ICONLE(1,IUSECO),1,ICOSO,1)
      CALL ICOPY(50,INTNLE(1,IUSECO),1,IWORK,1)
      CALL DCOPY(50,REANLE(1,IUSECO),1,RWORK,1)
      ATT = ATONLE(IUSECO)
      CALL SMSET(N,EPSTE(IUSECO),SCINI(IUSECO),Y,XSCAL)
C***********************************************************************
C     Call of Newton Solver
C***********************************************************************
      CALL NLEQ1(N,CRORES,JAC,Y,XSCAL,ATT,ICOSO,IERR,
     $      LIW,IWORK,LRW,RWORK,RPAR,IPAR)
      write(6,*) 'ierr in crossing point',ierr
      RETURN
C***********************************************************************
C     END OF -CROSS-
C***********************************************************************
      END
      SUBROUTINE PITRES(N,RPAR,IPAR,Y,F,IFC)
C***********************************************************************
C
C     Interface between  PITCON and  NEWRES
C
C***********************************************************************
C***********************************************************************
C     Storage organization
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION Y(N),F(N),RPAR(*),IPAR(*)
C***********************************************************************
C     Call function system 
C***********************************************************************
      NEQ = N - 1
      TAU = Y(N)
      CALL NEWRES(NEQ,TAU,Y,F,RPAR,IPAR,IFC)
      IFC = 0 
      RETURN
C***********************************************************************
C     END
C***********************************************************************
      END
      SUBROUTINE EULRES(N,TIME,Y,F,RPAR,IPAR,IFC)
C***********************************************************************
C
C     Interface
C
C***********************************************************************
C***********************************************************************
C     Storage organization 
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION Y(N),F(N),RPAR(*),IPAR(*)
      PARAMETER (ZERO=0.0D0, ONE=1.0D0)
C***********************************************************************
C     Call of function system 
C***********************************************************************
      TAU = ONE 
      CALL NEWRES(N,TAU,Y,F,RPAR,IPAR,IFC)
      RETURN
C***********************************************************************
C     END OF EULRES
C***********************************************************************
      END
      SUBROUTINE SNLRES(N,Y,F,RPAR,IPAR,IFC)
C***********************************************************************
C
C     Interface 
C
C***********************************************************************
C***********************************************************************
C     Storage organization 
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION Y(N),F(N),RPAR(*),IPAR(*)
      PARAMETER (ZERO=0.0D0, ONE=1.0D0)
C***********************************************************************
C     Call of function system 
C***********************************************************************
      TAU = ONE 
      CALL NEWRES(N,TAU,Y,F,RPAR,IPAR,IFC)
      RETURN
C***********************************************************************
C     END OF SNLRES
C***********************************************************************
      END
      SUBROUTINE SNLJAC(N,M,Y,A,RPAR,IPAR,IFC)
C***********************************************************************
C
C     Set analytical Jacobian to be -I
C
C***********************************************************************
C***********************************************************************
C     Storage organization 
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION A(N,N),Y(N),RPAR(*),IPAR(*)
      PARAMETER (ZERO=0.0D0,ONE=1.0D0)
C***********************************************************************
C     Set Jacobian to -I
C***********************************************************************
      IFC = 0
      A = ZERO 
      DO I=1,N
      A(I,I) = -ONE  
      ENDDO 
      RETURN
C***********************************************************************
C     End of SNLJAC
C***********************************************************************
      END
      SUBROUTINE ALCRES(N,Y,TAU,F,RPAR,IPAR,IFC)
C***********************************************************************
C
C     Interface 
C
C***********************************************************************
C***********************************************************************
C     Storage organization
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION Y(N),F(N),RPAR(*),IPAR(*)
C***********************************************************************
C     Call right hand side 
C***********************************************************************
      CALL NEWRES(N,TAU,Y,F,RPAR,IPAR,IFC)
      RETURN
C***********************************************************************
C     END
C***********************************************************************
      END
      SUBROUTINE ALCJAC(N,N1,T,Y,F,SM,A,RPAR,IPAR,IFC)
C***********************************************************************
C
C     Form analytical Jacobian for ALCON1 
C
C***********************************************************************
C***********************************************************************
C     Storage organization 
C***********************************************************************
      USE EGVSC
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION A(N,N1),Y(N1),F(N),SM(N1),RPAR(*),IPAR(*)
      PARAMETER (ONE=1.0D0,ZERO=0.0D0)
C***********************************************************************
C     Generate Jacobian  -I,DFDTAU
C***********************************************************************
      A = ZERO
      DO I=1,N
        A(I,I) = -ONE
      ENDDO
      A(1:N,N+1) = DFDTAU(1:N)
C***********************************************************************
C     Scale Jacobian 
C***********************************************************************
      DO 10 K=1,N1
      A(1:N,K)= SM(K)*A(1:N,K)
  10  CONTINUE
C***********************************************************************
C     Regular exit
C***********************************************************************
      RETURN
      END
      SUBROUTINE CRORES(N,Y,F,RPAR,IPAR,IFC)
C***********************************************************************
C
C     Interface
C
C***********************************************************************
      USE EGVSC
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION Y(N),F(N),RPAR(*),IPAR(*)
C***********************************************************************
C     Call right hand side 
C***********************************************************************
      TAU  = Y(N)
      NEQ = N-1
      CALL NEWRES(NEQ,TAU,Y,F,RPAR,IPAR,IFC)
C***********************************************************************
C     set up additional equation for target value 
C***********************************************************************
      F(N) = log((EVRS**2+EVIS**2)/EVRF**2)
      RETURN
C***********************************************************************
C     END
C***********************************************************************
      END
      SUBROUTINE TARRES(N,Y,F,RPAR,IPAR,IFC)
C***********************************************************************
C
C     Determine target point
C
C***********************************************************************
      USE EGVSC
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION Y(N),F(N),RPAR(*),IPAR(*)
      DOUBLE PRECISION, DIMENSION(N-1)     ::  ATARG     
      PARAMETER (ZERO=0.0D0,ONE=1.0D0)
C***********************************************************************
C     Call right hand side 
C***********************************************************************
      NEQ = N - 1
      TAU = Y(N)
      CALL NEWRES(NEQ,TAU,Y,F,RPAR,IPAR,IFC)
C***********************************************************************
C     set up additional equation for target value 
C***********************************************************************
      CALL GETTAR(N-1,ITARG,ATARG)
      F(N) = DOT_PRODUCT(ATARG(1:N-1),Y(1:N-1)) - VTARG
      RETURN
C***********************************************************************
C     END
C***********************************************************************
      END
      SUBROUTINE TARJAC(N,LDN,Y,A,RPAR,IPAR,IFC)
C***********************************************************************
C
C     Jacobian for target point 
C
C***********************************************************************
      USE EGVSC
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C***********************************************************************
C     Storage organization 
C***********************************************************************
      DIMENSION A(LDN,N),Y(N),RPAR(*),IPAR(*)
      DOUBLE PRECISION, DIMENSION(N)       :: FDUM
      DOUBLE PRECISION, DIMENSION(N-1)     :: ATARG 
      PARAMETER (ONE=1.0D0,ZERO=0.0D0)
C***********************************************************************
C     Call the right hand side (use A as a work array) 
C***********************************************************************
      CALL TARRES(N,Y,FDUM,RPAR,IPAR,IFC)
      IFC = 0
C***********************************************************************
C     Generate Jacobian  -I,DFDTAU
C***********************************************************************
      A = ZERO
      DO I=1,N-1
        A(I,I) = -ONE
      ENDDO
      A(1:N-1,N) = DFDTAU(1:N-1)
      CALL GETTAR(N-1,ITARG,ATARG)
      CALL DCOPY(N-1,ATARG,1,A(N,1),N)
      RETURN
C***********************************************************************
C  
C***********************************************************************
      END
      SUBROUTINE DUMJAC
C***********************************************************************
C  
C***********************************************************************
      STOP
      END
      SUBROUTINE PATRES(NEQ,TIME,SMF,DEL,RPAR,IPAR,IFC)
C***********************************************************************
C
C     
C
C***********************************************************************
      USE EGVSC
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION SMF(NEQ),DEL(NEQ)
      DIMENSION RPAR(*),IPAR(*)
      DATA NFILE6/6/
C***********************************************************************
C     WORK ARRAYS
C***********************************************************************
      DOUBLE PRECISION, DIMENSION(NEQ*NEQ) ::RW
      INTEGER,          DIMENSION(2*NEQ)   ::IW
      LIW=2*NEQ
      LRW=NEQ*NEQ
C***********************************************************************
C     Call function evaluation
C***********************************************************************
      CALL PATDES(NEQ,TIME,SMF,DEL,RPAR,IPAR,IFC,NCT,
     1    LAMR,LAMI,LRW,RW,LIW,IW,NFILE6,IEREXD)
      IFC = 0
      IF(IEREXD.GT.0) IFC = 9
      RETURN
C***********************************************************************
C     END OF
C***********************************************************************
      END
      SUBROUTINE PATDES(NEQ,TIME,SMF,DEL,RPAR,IPAR,IFC,NC,
     1    AR,AI,LRW,RW,LIW,IW,NFILE6,IERR)
C***********************************************************************
C
C     RESIDUAL AND COEFFICIENTS OF TIME DERIVATIVES
C
C***********************************************************************
      USE EGVSC
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      EXTERNAL MASRES,MASJAC
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)       :: SCHUR,XJAC,AVL,AVR
      DOUBLE PRECISION, DIMENSION(NEQ)           :: HELP
      PARAMETER (ONE = 1.0D0, ZERO = 0.0D0)
C***********************************************************************
C     arrays of dimension NEQ
C***********************************************************************
      DIMENSION SMF(NEQ),DEL(NEQ),AR(NEQ),AI(NEQ)
C***********************************************************************
C     work arrays
C***********************************************************************
      DIMENSION IW(LIW),RW(LRW)
C***********************************************************************
C     parameter arrays 
C***********************************************************************
      DIMENSION RPAR(*),IPAR(*)
      SAVE
C***********************************************************************
C
C     Initialization 
C 
C***********************************************************************
      IF(IGENI.GT.0) THEN
         WRITE(*,*) ' PATDES cannot handle yet auxiliary equations'
         STOP
      ENDIF
      ipri = 0
      ierr = 0
C***********************************************************************
C     Check work array
C***********************************************************************
      IF(LRW.LT.NC*NC) GOTO 900
      IF(LIW.LT.NC   ) GOTO 905
C***********************************************************************
C
C     Calculate eigenvector basis 
C
C***********************************************************************
      MDIM = NC
      RDIM = 0
      CALL EGVBAS(1,0,MDIM,RDIM,NZEVT,NEQ,MASRES,MASJAC,TIME,SMF,
     1            ERCOL,ERROW,XTCOL,XTROW,
     1            AR,AI,AVR,AVL,XJAC,SCHUR,
     1            RPAR,IPAR,IPRI,NFILE6,IERBAS)
      IF(IERBAS.GT.0) GOTO 910
C***********************************************************************
C     Store pathfollowing vector in HELP
C***********************************************************************
      HELP(1:NC) = DELCVT(1:NC)
C***********************************************************************
C     Compute  P * Q_U
C***********************************************************************
      CALL DGEMM('N','N',NC,NC,NEQ,ONE,CROW,NC,AVR,NEQ,ZERO,RW,NC)
C***********************************************************************
C     factorize  P * Q_U
C***********************************************************************
      CALL DGEFA(RW,NC,NC,IW,INFO)
      IF(INFO.NE.0) GOTO 920
C***********************************************************************
C     Compute DEL = Q_U * (P * Q_U)^-1 * HELP
C***********************************************************************
      CALL DGESL(RW,NC,NC,IW,HELP,0)
      CALL DGEMV('N',NEQ,NC,ONE,AVR,NEQ,HELP,1,ZERO,DEL,1)
C***********************************************************************
C     Scaling
C***********************************************************************
      IF(BETOLD.GT.1.D-8) THEN
      BETNEW = SQRT(DDOT(NEQ,DEL,1,DEL,1))
      SAFFAC = BETOLD/BETNEW
C---- allow scaling  (comment for no scaling allowed)
      CALL DSCAL(NEQ,SAFFAC,DEL,1)
      ENDIF
C***********************************************************************
C     Regular exit
C***********************************************************************
      RETURN
C***********************************************************************
C
C***********************************************************************
  900 CONTINUE 
      WRITE(NFILE6,*) ' LRW too small in PATDES ' 
      STOP   
  905 CONTINUE 
      WRITE(NFILE6,*) ' LIW too small in PATDES ' 
      STOP   
  910 IERR = 1
      WRITE(NFILE6,*) ' EGVBAS returned to PATDES with an error flag'
      RETURN
  920 IERR = 2
      WRITE(NFILE6,*) ' ERROR IN SOLVING LIN. EQUATION IN PATDES'
      DO 132 I=1,NEQ
      WRITE(NFILE6,*) 'ar,ai',AR(I),AI(I)
  132 CONTINUE
      RETURN
  980 IERR = 9
      RETURN
C***********************************************************************
C     END OF EVBDES
C***********************************************************************
      END
      SUBROUTINE NEWRES(NEQ,TAU,SMF,DEL,RPAR,IPAR,IFC)
C***********************************************************************
C
C     For a given function F this subroutine does the following:
C      
C        C
C        Z_f^-1
C
C
C     
C
C***********************************************************************
C***********************************************************************
C     Storage organisation 
C***********************************************************************
      USE EGVSC
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION SMF(NEQ),DEL(NEQ),RPAR(*),IPAR(*)
      PARAMETER (ZERO=0.0D0,ONE=1.0D0)
      DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:)   :: CVACT,DCVDTA     
      DATA NFILE6/6/
C***********************************************************************
C     WORK ARRAYS
C***********************************************************************
      DOUBLE PRECISION, DIMENSION(NEQ*NEQ) ::RW
      INTEGER,          DIMENSION(2*NEQ)   ::IW
      LIW=2*NEQ
      LRW=NEQ*NEQ
C***********************************************************************
C     INITIALIZE
C***********************************************************************
      IF(.NOT.ALLOCATED(CVACT))  ALLOCATE (CVACT(NCT)) 
      IF(.NOT.ALLOCATED(DCVDTA)) ALLOCATE (DCVDTA(NCT)) 
C***********************************************************************
C     Calculate CV  where CV(TAU) = CVO + TAU*(CV-CVO)
C***********************************************************************
      CVACT(1:NCT) = TAU * CVCOM(1:NCT) + (ONE-TAU) * CSTART(1:NCT)
      DCVDTA (1:NCT) = CVCOM(1:NCT) - CSTART(1:NCT)
C***********************************************************************
C     Call function evaluation
C***********************************************************************
      CALL NEWDES(NEQ,NCT,NZEVT,NDIMT,TAU,SMF,DEL,RPAR,IPAR,IFC,
     1    LAMR,LAMI,DFDTAU,CVACT,DCVDTA,CROW,ERROW,ERCOL,
     1    XTROW,XTCOL,COTM,IGENI,LNF,
     1    NMAILD,NLMILD,NOILDM,
     1    LRW,RW,LIW,IW,NFILE6,IEREXD)
C
      EVRS = LAMR(NCT)
      EVRF = LAMR(NCT+1)
      EVIS = LAMI(NCT)
      EVIF = LAMI(NCT+1)
      IFC = 0
      IF(IEREXD.GT.0) IFC = 9
      RETURN
C***********************************************************************
C     END OF
C***********************************************************************
      END
      SUBROUTINE NEWDES(NEQ,NC,NZEV,NDIM,TAU,SMF,DEL,RPAR,IPAR,IFC,
     1    AR,AI,FTAU,CVACT,DCVDTA,CROW,
     1    ERVR,ERVC,YTROW,YTCOL,COM,IGENI,LNF,
     1    NMAILD,NLMILD,NOILDM,
     1    LRW,RW,LIW,IW,NFILE6,IERR)
C***********************************************************************
C
C     RESIDUAL AND COEFFICIENTS OF TIME DERIVATIVES
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)       :: XTOT,XJAC,SCHUR,
     1                                              AVR,AVL,
     1                                              YTROW,YTCOL,
     1                                              ERVR,ERVC,COM,QTEM
      DOUBLE PRECISION, DIMENSION(NEQ)           :: HELP,VRS,SMF,DEL,
     1                                              AR,AI,FTAU,SMFMI
      DOUBLE PRECISION, DIMENSION(NC)            :: CVACT,DCVDTA     
      DOUBLE PRECISION, DIMENSION(NDIM,NDIM)   :: VDP
      DOUBLE PRECISION, DIMENSION(NEQ,NDIM)      :: VDI 
      DOUBLE PRECISION, DIMENSION(NC,NEQ)        :: CROW
      EXTERNAL MASRES,MASJAC
      LOGICAL EQUILI
      PARAMETER (ONE = 1.0D0, ZERO = 0.0D0)
C***********************************************************************
C     work arrays
C***********************************************************************
      DIMENSION IW(LIW),RW(LRW)
C***********************************************************************
C     parameter arrays 
C***********************************************************************
      DIMENSION RPAR(*),IPAR(*)
C***********************************************************************
C
C     Initialization 
C 
C***********************************************************************
      IERR = 0
C***********************************************************************
C
C     Calculate components for parameterization
C
C***********************************************************************
C---- NP: number of parameterization equaitons
      NP  = NC       
C---- NPF, NPL: indices of first and last manifold equation 
      NPF = 1   
      NPL = NP       
C***********************************************************************
C
C     Calculate components for manifold equations
C
C***********************************************************************
C---- NF: number of manifold equations
      NMM = 0
      IF(IGENI.EQ.1) NMM = NEQ -NC 
      IF(IGENI.EQ.2) NMM = 1 
C     IF(SMF(3).LT.4.2) NMM = 1
C     IF(SMF(3).LT.2.4) NMM = NEQ-NC
      NF  = NEQ - NC - NMM
C---- NFF, NFL: indices of first and last manifold equation 
      NFF = NPL + 1   
      NFL = NFF - 1 + NF
      LNF = NF
      EQUILI = .FALSE.
      IF(NF.EQ.NEQ-NZEV) EQUILI = .TRUE.
C***********************************************************************
C
C     Calculate components for auxiliary equations
C
C***********************************************************************
C---- NA: number of auxiliary equations
      NA  = 0       
      IF(IGENI.GT.0) NA  = NEQ - NC - NF       
C---- NAF, NAL: indices of first and last manifold equation 
      NAF = NFL + 1   
      NAL = NAF -1 + NA
C***********************************************************************
C
C     Calculate eigenvector basis 
C
C***********************************************************************
      MDIM = NC + NA
C***********************************************************************
C
C***********************************************************************
      IPRI = 0
      RDIM = 0
                       ICE  = 1
                       IAJ  = 0
      IF(NMAILD.EQ.1) THEN
C----   use J*J^T
        ICE = 4
C----   use analytical Jacobian
        IAJ = 0
      ENDIF
      IF(NMAILD.EQ.2) THEN
        ICE = 1
        IAJ = 1
      ENDIF
      IF(NLMILD.NE.0) ICE = 0
      CALL EGVBAS(ICE,IAJ,MDIM,RDIM,NZEV,NEQ,MASRES,MASJAC,TIME,SMF,
     1            ERVC,ERVR,YTCOL,YTROW,
     1            AR,AI,AVR,AVL,XJAC,SCHUR,
     1            RPAR,IPAR,IPRI,NFILE6,IERBAS)
      IF(IERBAS.GT.0) GOTO 910
      IF(NLMILD.NE.0) YTROW = COM  
      IF(EQUILI) THEN 
         YTROW = ERVR      
         YTCOL = ERVC      
      ENDIF
C***********************************************************************
C     Calculate F
C***********************************************************************
      CALL MASRES(NEQ,TIMDUM,SMF,VRS,RPAR,IPAR,IEMR)
C***********************************************************************
C
C     Calculate components for parameterization
C
C***********************************************************************
C***********************************************************************
C     Calculate (CV - C * SMF)  
C***********************************************************************
      HELP(NPF:NPL) = CVACT(NPF:NPL)  - MATMUL(CROW(1:NC,:),SMF(:))
C***********************************************************************
C     Store CROW in scaling matrix 
C***********************************************************************
      XTOT(NPF:NPL,:) =  CROW(1:NC,:)
C***********************************************************************
C
C     Calculate components for manifold equations
C
C***********************************************************************
      IF(NF.NE.0) THEN
C***********************************************************************
C     Calculate -Z~_f * F or for 1st order   -Z~_f * F_y * F 
C***********************************************************************
      IF(NOILDM.EQ.1) VRS(1:NEQ) = MATMUL(XJAC,VRS(1:NEQ))
      HELP(NFF:NFL) = -MATMUL(YTROW(NEQ-NF+1:NEQ,1:NEQ),VRS(1:NEQ))
C***********************************************************************
C     Set up scaling matrix for manifold equations 
C***********************************************************************
      IF(NOILDM.EQ.0) THEN
         XTOT(NFF:NFL,1:NEQ) =  MATMUL(YTROW(NEQ-NF+1:NEQ,1:NEQ),XJAC)
      ELSE IF(NOILDM.EQ.1) THEN
         XTOT(NFF:NFL,1:NEQ) =  MATMUL(YTROW(NEQ-NF+1:NEQ,1:NEQ),XJAC)
         XTOT(NFF:NFL,1:NEQ) =  MATMUL(XTOT(NEQ-NF+1:NEQ,1:NEQ),XJAC)
      ELSE
        WRITE(*,*) ' NOT APPLICABLE'
        STOP
      ENDIF
C***********************************************************************
C     End of manifold equations 
C***********************************************************************
      ENDIF
C***********************************************************************
C
C     Calculate components for auxiliary equations
C
C***********************************************************************
C***********************************************************************
C     Calculate auxiliary equation
C***********************************************************************
      IF(NA.NE.0) THEN 
      NKV = NC + NF
      CALL ORBAS(NEQ,NKV,NEQ,XTOT,NEQ,QTEM,LRW,RW,LIW,IW,-2,IERR)
      IF(IERR.NE.0) THEN
         WRITE(*,*) ' EMERGENCY'
         STOP
      ENDIF
C---- Compute M^orth * v v^T  use vrs as work array
      CALL GETVDI(NEQ,NDIM,VDI,SMFMI)
      VDP = MATMUL(TRANSPOSE(VDI),VDI)
      CALL DGEINV(NDIM,VDP,RW,IW,IERINV)
      IF(IERINV.NE.0) THEN
         write(*,*) 'NNNNNN',neq
         GOTO 980
      ENDIF
C----- auswertung keinen einfluss auf ergebnis
      QTEM(NAF:NAL,1:NEQ) =
     1      QTEM(NAF:NAL,1:NEQ) - MATMUL(
     1       MATMUL(QTEM(NAF:NAL,1:NEQ),VDI(1:NEQ,1:NDIM))
     1      , MATMUL(VDP(1:NDIM,1:NDIM),TRANSPOSE(VDI(1:NEQ,1:NDIM)))
     1     )
C     do i = naf,nal
C     write(*,*) 'iiii',QTEM(I,1:NEQ)
C     enddo
      XTOT(NAF:NAL,:) = QTEM(NAF:NAL,:) 
      VRS(1:NEQ) =  (SMFMI(1:NEQ) - SMF(1:NEQ))
      HELP(NAF:NAL) = MATMUL(XTOT(NAF:NAL,:),VRS(:))
C     write(*,*) 'jjjj',HELP(NAF:NAL)
      ENDIF 
C***********************************************************************
C
C     Factorize  scaling matrix
C
C***********************************************************************
      CALL DGEFA(XTOT,NEQ,NEQ,IW,INFFA)
      IF(INFFA.NE.0) GOTO 930
C***********************************************************************
C     calculate FTAU
C***********************************************************************
      FTAU(1:NEQ)       = ZERO                  
      FTAU(NPF:NPL)     = DCVDTA (1:NP) 
      CALL DGESL(XTOT,NEQ,NEQ,IW,FTAU,0)
C***********************************************************************
C     calculate overall residual 
C***********************************************************************
      CALL DGESL(XTOT,NEQ,NEQ,IW,HELP,0)
      DEL(1:NEQ) = HELP(1:NEQ)
C---- Note: the approximate Jacobian for this system is -I
C***********************************************************************
C     Regular exit
C***********************************************************************
      RETURN
C***********************************************************************
C
C***********************************************************************
  910 IERR = 1
      WRITE(NFILE6,*) ' EGVBAS returned to NEWDES with error:',IERBAS
      RETURN
  920 IERR = 2
      WRITE(NFILE6,*) ' decomposition of N_f failed in NEWDES',  
     1                ' possible reason: zero eigenvalues in N_f'
      DO 132 I=1,NEQ
      WRITE(NFILE6,*) 'ar,ai',AR(I),AI(I)
  132 CONTINUE
      RETURN
  930 IERR = 3
      WRITE(NFILE6,*) ' decomposition of C Z_f failed in NEWDES'
      DO I=1,NC
      write(*,*) INFFA,' CRRRRR', CROW(I,1:NEQ)      
      ENDDO 
      RETURN
  980 IERR = 9
      RETURN
C***********************************************************************
C     END OF EVBDES
C***********************************************************************
      END
