!--------------------------------------------------------------------------------------
! Written by Alexander Neagos
! several modules for the object oriented calculation of functions
! function types: polynomial, chebyshev polynomials, chebyshev cosinus, gauss, normed gauss
!--------------------------------------------------------------------------------------
module abstract_mod
!
    type,abstract :: base
        double precision, allocatable, dimension(:,:)   :: param_array
        double precision, allocatable, dimension(:,:)   :: value_array
        integer                                         :: nfunc
        double precision                                :: x
        double precision                                :: xmin,xmax
        contains
            procedure(allocate_them),deferred, pass(self)       :: allocate_all
            procedure(set_para),deferred,pass(self)             :: set_parameters
            procedure(func_val),deferred,pass(self)             :: get_func_val
            procedure(dfunc_val),deferred,pass(self)            :: get_dfunc_val
            procedure(ddfunc_val),deferred,pass(self)           :: get_ddfunc_val
            procedure                                           :: get_values
            procedure                                           :: scale_x
            procedure                                           :: rescale_y
            procedure                                           :: deallocate_all
    end type base
!
    abstract interface
        subroutine allocate_them(self)
            import          :: base
            class (base)    :: self
        end subroutine allocate_them
    end interface
!
    abstract interface
        subroutine set_para(self)
            import          :: base
            class (base)    :: self
        end subroutine set_para
    end interface
!
    abstract interface
    function func_val(self) result(res)
        import              :: base
        class (base)        :: self
        double precision,dimension(self%nfunc)    :: res
    end function func_val
    end interface
!
    abstract interface
    function dfunc_val(self) result(res)
        import              :: base
        class (base)        :: self
        double precision,dimension(self%nfunc)    :: res
    end function dfunc_val
    end interface
!
    abstract interface
    function ddfunc_val(self) result(res)
        import              :: base
        class (base)        :: self
        double precision,dimension(self%nfunc)    :: res
    end function ddfunc_val
    end interface
!
    contains
    subroutine scale_x(self,t,tmin,tmax)
        class (base)        :: self
        double precision    :: t,tmin,tmax
        self%x = ((t-tmin)/(tmax-tmin))*(self%xmax-self%xmin)+self%xmin
    end subroutine scale_x
!
    subroutine get_values(self)
        class (base)        :: self
            self%value_array(1,:)=self%get_func_val()
            self%value_array(2,:)=self%get_dfunc_val()
            self%value_array(3,:)=self%get_ddfunc_val()
    end subroutine get_values
!
    subroutine rescale_y(self,tmin,tmax)
        class (base),intent(inout)        :: self
        double precision                  :: tmin,tmax
        do i=1,self%nfunc
            self%value_array(2,i)=self%value_array(2,i)*((self%xmax-self%xmin)/(tmax-tmin))
            self%value_array(3,i)=self%value_array(3,i)*((self%xmax-self%xmin)/(tmax-tmin))**2
        end do
    end subroutine rescale_y
!
    subroutine deallocate_all(self)
        class (base)        :: self
        if (allocated(self%param_array)) deallocate(self%param_array)
        if (allocated(self%value_array)) deallocate(self%value_array)
    end subroutine deallocate_all
!
end module abstract_mod
!
module poly_mod
    use abstract_mod
    type,extends(base) :: poly
        contains
        procedure,pass(self) :: allocate_all => allocate_all_poly
        procedure,pass(self) :: set_parameters => set_parameters_poly
        procedure,pass(self) :: get_func_val => get_func_val_poly
        procedure,pass(self) :: get_dfunc_val => get_dfunc_val_poly
        procedure,pass(self) :: get_ddfunc_val => get_ddfunc_val_poly
    end type poly
    !
    contains
    !
        subroutine allocate_all_poly(self)
            class (poly) :: self
            if (allocated(self%param_array)) deallocate(self%param_array)
            allocate(self%param_array(self%nfunc,self%nfunc))
            if (allocated(self%value_array)) deallocate(self%value_array)
            allocate(self%value_array(3,self%nfunc))
            self%param_array=0.0d0
            self%value_array=0.0d0
        end subroutine allocate_all_poly
!
        subroutine set_parameters_poly(self)
            class (poly) :: self
            integer :: i
            !self%param_array=0.0d0
            do i=1,self%nfunc
                self%param_array(i,i)=1.0d0
            end do
            self%xmin=-1.0d0
            self%xmax=1.0d0
        end subroutine set_parameters_poly
!
        function get_func_val_poly(self) result (res)
         class (poly)           :: self ;
         double precision,dimension(self%nfunc) :: res ;
         double precision :: dumres
         integer :: i,j
         res=0.0d0
         do i=1,self%nfunc
            do j=1,self%nfunc
            dumres=self%param_array(i,j)*self%x**dble(j-1);
            res(i)=res(i)+dumres
            end do
         end do
       end function get_func_val_poly
!
       function get_dfunc_val_poly(self) result (res)
         class (poly)            :: self ;
         double precision,dimension(self%nfunc) :: res
         double precision :: dumres
         integer :: i,j
         res=0.0d0
         do i=1,self%nfunc
            do j=1,self%nfunc
                if ((j-1).eq.0 .and. self%x.eq.0.0d0) then
                dumres= 0.0d0
                else
                dumres = self%param_array(i,j)*dble(j-1)*self%x**(dble(j-1)-1.0d0);
                end if
                res(i)=res(i)+dumres
            end do
         end do
!
       end function get_dfunc_val_poly
!
       function get_ddfunc_val_poly(self) result (res)
         class (poly)               :: self ;
         double precision,dimension(self%nfunc) :: res
         integer :: i,j
         res=0.0d0
         do i=1,self%nfunc
            do j=1,self%nfunc
                if ((j-1).le.1 .and. self%x.eq.0.0d0) then
                dumres= 0.0d0
                else
                dumres = self%param_array(i,j)*(dble(j-1)**2-dble(j-1))*self%x**(dble(j-1)-2.0d0);
                end if
                res(i)=res(i)+dumres
            end do
         end do
       end function get_ddfunc_val_poly
!
end module poly_mod
!
module chebyshev_mod
    use poly_mod
    type,extends(poly) :: chebyshev
        contains
        procedure,pass(self) :: set_parameters => set_parameters_chebyshev
    end type chebyshev
    contains
        subroutine set_parameters_chebyshev(self)
            class (chebyshev) :: self
            integer :: i
            self%param_array=0.0d0
            call chebyshev_func(self%nfunc,self%param_array)
            self%xmin=-1.0d0
            self%xmax=1.0d0
        end subroutine set_parameters_chebyshev
        subroutine chebyshev_func(d,current)
            integer :: i,j,d
            double precision, dimension(d,d),intent(out) :: current
            current=0.0d0
            if (d.eq.1) then
                current(1,1)=1.0d0
            else if (d.eq.2) then
                current(1,1)=1.0d0
                current(2,2)=1.0d0
        !        write(*,*)current
            else if (d.gt.2) then
                current(1,1)=1.0d0
                current(2,2)=1.0d0
                do i=3,d
                    do j=1,d-1
                        current(i,j+1)=2*current(i-1,j)
                    end do
                    current(i,:)=current(i,:)-current(i-2,:)
                end do
            else
                write(*,*)'d=',d,'not available'
            end if
        end subroutine chebyshev_func
!!! subroutine for cheb. params. here!
end module chebyshev_mod
module chebyshev_cos_mod
    use abstract_mod
    type,extends(base) :: chebyshev_cos
        contains
        procedure,pass(self) :: allocate_all => allocate_all_chebyshev_cos
        procedure,pass(self) :: set_parameters => set_parameters_chebyshev_cos
        procedure,pass(self) :: get_func_val => get_func_val_chebyshev_cos
        procedure,pass(self) :: get_dfunc_val => get_dfunc_val_chebyshev_cos
        procedure,pass(self) :: get_ddfunc_val => get_ddfunc_val_chebyshev_cos
    end type chebyshev_cos
    !
    contains
    !
        subroutine allocate_all_chebyshev_cos(self)
            class (chebyshev_cos) :: self
            if (.not. allocated(self%param_array)) allocate(self%param_array(1,self%nfunc))
            if (.not. allocated(self%value_array)) allocate(self%value_array(3,self%nfunc))
            self%param_array=0.0d0
            self%value_array=0.0d0
        end subroutine allocate_all_chebyshev_cos
!
        subroutine set_parameters_chebyshev_cos(self)
            class (chebyshev_cos) :: self
            integer :: i
            !self%param_array=0.0d0
            do i=1,self%nfunc
                self%param_array(1,i)=dble(i-1)
            end do
            self%xmin=-1.0d0
            self%xmax=1.0d0
        end subroutine set_parameters_chebyshev_cos
!
        function get_func_val_chebyshev_cos(self) result (res)
         class (chebyshev_cos)           :: self ;
         double precision,dimension(self%nfunc) :: res ;
         double precision :: dumres
         integer :: i,j
         res=0.0d0
         do i=1,self%nfunc
            res(i)=Cos(self%param_array(1,i)*ACos(self%x))
         end do
       end function get_func_val_chebyshev_cos
!
       function get_dfunc_val_chebyshev_cos(self) result (res)
         class (chebyshev_cos)            :: self ;
         double precision,dimension(self%nfunc) :: res
         double precision :: xx
         integer :: i,j
         res=0.0d0
         do i=1,self%nfunc
            if (self%x.eq.-1.0d0) then
                xx=self%x+1.0d-14
                res(i)=(self%param_array(1,i)*Sin(self%param_array(1,i)*ACos(xx)))&
                & /Sqrt(1.0d0 - xx**2.0d0)
            else if(self%x.eq.1.0d0) then
                xx=self%x-1.0d-14
                res(i)=(self%param_array(1,i)*Sin(self%param_array(1,i)*ACos(xx)))&
                & /Sqrt(1.0d0 - xx**2.0d0)
            else
                res(i)=(self%param_array(1,i)*Sin(self%param_array(1,i)*ACos(self%x)))&
                & /Sqrt(1.0d0 - self%x**2.0d0)
            end if
         end do
!
       end function get_dfunc_val_chebyshev_cos
!
       function get_ddfunc_val_chebyshev_cos(self) result (res)
         class (chebyshev_cos)               :: self ;
         double precision,dimension(self%nfunc) :: res
         integer :: i,j
         res=0.0d0
         do i=1,self%nfunc
            if (self%x.eq.-1.0d0) then
                res(i)=(-1.0d0)**self%param_array(1,i)*&
                &(self%param_array(1,i)**4-self%param_array(1,i)**2)/3.0d0
            else if(self%x.eq.1.0d0) then
                res(i)=(self%param_array(1,i)**4-self%param_array(1,i)**2)/3.0d0
            else
                res(i)=-((self%param_array(1,i)**2.0d0*Cos(self%param_array(1,i)*ACos(self%x)))&
                &/(1.0d0 - self%x**2.0d0)) +&
                & (self%param_array(1,i)*self%x*Sin(self%param_array(1,i)*ACos(self%x)))&
                &/(1.0d0 - self%x**2.0d0)**1.5d0
            end if
         end do
       end function get_ddfunc_val_chebyshev_cos
!
end module chebyshev_cos_mod
module gauss_module
    use abstract_mod
    type,extends(base) :: gauss
        contains
        procedure,pass(self) :: allocate_all => allocate_all_gauss
        procedure,pass(self) :: set_parameters => set_parameters_gauss
        procedure,pass(self) :: get_func_val => get_func_val_gauss
        procedure,pass(self) :: get_dfunc_val => get_dfunc_val_gauss
        procedure,pass(self) :: get_ddfunc_val => get_ddfunc_val_gauss
    end type gauss
    !
    contains
    !
        subroutine allocate_all_gauss(self)
            class (gauss) :: self
            if (.not. allocated(self%param_array)) allocate(self%param_array(2,self%nfunc))
            if (.not. allocated(self%value_array)) allocate(self%value_array(3,self%nfunc))
        end subroutine allocate_all_gauss
    !
        subroutine set_parameters_gauss(self)
            class (gauss) :: self
            integer :: i
            do i=1,self%nfunc
                self%param_array(1,i)=equi_delta(i)
                self%param_array(2,i)=1.0d0
!               self%param_array(2,i)=1.0d0
            end do
            self%xmin=0.0d0
            self%xmax=equi_delta(self%nfunc)
        end subroutine set_parameters_gauss
    !
        function get_func_val_gauss(self) result (res)
         class (gauss)           :: self ;
         double precision,dimension(self%nfunc) :: res
         integer :: i
         res=0.0d0
         do i=1,self%nfunc
         res(i) = exp(-(self%x-self%param_array(1,i))**2/self%param_array(2,i)**2)
         end do
       end function get_func_val_gauss
    !
       function get_dfunc_val_gauss(self) result (res)
         class (gauss)            :: self ;
         double precision,dimension(self%nfunc) :: res
         integer :: i
         res=0.0d0
         do i=1,self%nfunc
            res(i) = (-2.0d0*(self%x-self%param_array(1,i))/self%param_array(2,i)**2)*&
            & exp(-(self%x-self%param_array(1,i))**2/self%param_array(2,i)**2);
         end do
    !
       end function get_dfunc_val_gauss
    !
       function get_ddfunc_val_gauss(self) result (res)
         class (gauss)               :: self ;
         double precision,dimension(self%nfunc) :: res
         integer :: i
         res=0.0d0
         do i=1,self%nfunc
           res(i) = -2.0d0/(exp((-self%param_array(1,i) + self%x)**2.0d0/self%param_array(2,i)**2.0d0)*&
           &self%param_array(2,i)**2.0d0)&
           & + (4.0d0*(-self%param_array(1,i) + self%x)**2.0d0)/(exp((-self%param_array(1,i) + self%x)**2.0d0/ &
           & self%param_array(2,i)**2.0d0)*self%param_array(2,i)**4.0d0)
         end do
       end function get_ddfunc_val_gauss
!
    recursive function equi_delta(num) result (delta)
     double precision :: delta ;
     integer :: num;
     if (num.eq.1)then
     delta = 0.0d0
     else
     delta = equi_delta(num-1)+0.8325546111576978d0
     end if
    end function equi_delta
!
    recursive function var_delta(lvl) result (delta)
     double precision :: delta ;
     integer :: lvl;
     if (lvl.eq.1)then
     delta = 0.0d0
     else
     delta = 0.8325546111576978d0*sigma(lvl-1)
     end if
    end function var_delta
!
    recursive function sigma(lvl) result (sig)
     double precision :: sig ;
     integer :: lvl;
     if (lvl.eq.1)then
     sig = 1.0d0
     else
     sig = sigma(lvl-1)/2.0d0
     end if
    end function sigma
!
end module gauss_module
module normed_gauss_module
    use gauss_module
    type, extends(gauss)  :: normed_gauss
        contains
        procedure, pass(self)  :: get_values => get_values_normed_gauss
    end type normed_gauss
    contains
    subroutine get_values_normed_gauss(self)
        class (normed_gauss)                :: self
        double precision                    :: dumsumy,dumsumdy,dumsumddy
        double precision,dimension(self%nfunc)   :: dumy, dumdy, dumddy
            self%value_array(1,:)=self%get_func_val()
            self%value_array(2,:)=self%get_dfunc_val()
            self%value_array(3,:)=self%get_ddfunc_val()
        dumy=self%value_array(1,:)
        dumdy=self%value_array(2,:)
        dumddy=self%value_array(3,:)
        dumsumy=sum(self%value_array(1,:))
        dumsumdy=sum(self%value_array(2,:))
        dumsumddy=sum(self%value_array(3,:))
        self%value_array(1,:)=self%value_array(1,:)/dumsumy
        self%value_array(2,:)=self%value_array(2,:)/dumsumy-dumy*dumsumdy/(dumsumy)**2
        self%value_array(3,:)=-2.0d0*dumdy*dumsumdy/(dumsumy)**2+ &
                               & 2.0d0*dumy*(dumsumdy)**2/(dumsumy)**3+&
                               & dumddy/dumsumy-&
                               & dumy*(dumsumddy)/(dumsumy)**2
    end subroutine get_values_normed_gauss
end module normed_gauss_module
!
!
!
      module vector_algebra
      implicit none
      integer,parameter                        :: rk=8
      type value_type
            double precision, allocatable,dimension(:) :: val
      end type value_type
      contains
!
      subroutine multiply_coor(ndim,val_vectors,product_vec)
      integer,intent(in)                                   :: ndim
      type(value_type),dimension(ndim),intent(in)          :: val_vectors
      double precision,dimension(:),allocatable,intent(out)   :: product_vec
      integer                                              :: nn,mm,ii,ll
      double precision,dimension(:),allocatable               :: dummyvect
      double precision,dimension(:),allocatable               :: actvect
      integer                                              :: newdim,actdim
      newdim=1
      do ii=1,ndim
      newdim=newdim*size(val_vectors(ii)%val)
      end do
      allocate(product_vec(newdim))
      product_vec=0.0d0
      newdim=size(val_vectors(1)%val)
      product_vec(1:size(val_vectors(1)%val))=val_vectors(1)%val
!
      if (ndim.ge.2) then
      do ii=2,ndim
      allocate(dummyvect(size(val_vectors(ii)%val)))
      dummyvect=val_vectors(ii)%val
      actdim=newdim*size(val_vectors(ii)%val)
      allocate(actvect(actdim))
      call cartesian_product(newdim,product_vec(1:newdim),size(val_vectors(ii)%val),dummyvect,&
        & actvect(1:actdim))
      newdim=actdim
      product_vec(1:actdim)=actvect(1:actdim)
      deallocate(dummyvect)
      deallocate(actvect)
      end do
      end if
!
      end subroutine multiply_coor
!
!
!
      subroutine cartesian_product(a,x,b,y,z)
      integer, intent(in)                               :: a,b
      double precision,dimension(a),intent(in)             :: x
      double precision,dimension(b),intent(in)             :: y
      double precision,dimension(a*b),intent(out)          :: z
      integer                                           :: nn,mm
      do nn=1,b
      do mm=1,a
!      write(*,*)x(mm),'mal',y(nn),'=',x(mm)*y(nn)
      z((nn-1)*a+mm)=x(mm)*y(nn)
      end do
      end do
      end subroutine cartesian_product
      end module vector_algebra
