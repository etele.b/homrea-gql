      SUBROUTINE GENCEL(INDC,INFO,ICF,NDIM,N2DIM,IY,IVERT,NVERM,NVERT,
     1     ICORD,IRET,IUNIT,IPAIR,NPROP,PROP,PMCOL,PMROW,MIXFRA,INFOD,
     1     NEQ,NZEV,NC,QEVB,ERVROW,ERVCOL,
     1     LRV,RV,RPAR,IPAR,LRW,RW,LIW,IW,
     1     NFIOUT,IERR)
C***********************************************************************
C     Storage organization
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL TEST
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)    ::  QEVB,ERVROW,ERVCOL
      DIMENSION IY(NDIM),IVERT(N2DIM),ICORD(NDIM,*)
      DIMENSION IUNIT(NDIM,N2DIM),IPAIR(NDIM,N2DIM),IRET(*)
      DIMENSION PROP(NPROP,*),SMF(NEQ)
      DIMENSION RV(LRV),RPAR(*),IPAR(*),RW(LRW),IW(LIW)
      DIMENSION PMCOL(NEQ,NC),PMROW(NC,NEQ)
      COMMON/BLOCOR/IIRAT,IISMF,IISMFP,IIRV,IIPRO,IIPNU,IILAM,NPROOT
C***********************************************************************
C     Initialization 
C***********************************************************************
      TEST = .FALSE.
C***********************************************************************
C
C     Create cell
C
C***********************************************************************
      NVERMO = NVERT
      CALL CRECEL(INFO,ICF,NDIM,N2DIM,IY,IVERT,NVERM,NVERT,
     1     ICORD,IRET,IUNIT,NFIOUT)
      NOG = 0
      NOU = 0
      DO I=1,N2DIM
       INDV = IVERT(I)
       IF(IRET(INDV).EQ.0) THEN
       NOG = NOG + 1
       CALL CHKUDO(NEQ,PROP(IISMF,INDV),IRCH)
       IF(IRCH.NE.0) THEN
         WRITE(*,*) 'at least one good vertex not in setup domain',IRCH
         NOU = NOU + 1
C        ICF = 0
       ENDIF
       ENDIF
      ENDDO 
       IF(NOU.EQ.NOG) THEN
         WRITE(*,*) 'all good vertices not in table setup domain'
         ICF = 0
         NVERT = NVERMO
       ENDIF
         IF(ICF.EQ.0) THEN
            WRITE(NFIOUT,819) (IRET(IVERT(I)),I=1,N2DIM)
            GOTO 600
         ELSE
            WRITE(NFIOUT,818) INDC
         ENDIF
 818  FORMAT(1X,60('*'),/,' Cell',I6,' will be created')
 819  FORMAT(1X,' cell has not been created, iflags:',/,10(1X,I2))
C***********************************************************************
C
C     Calculate Cell
C
C***********************************************************************
      CALL CALCEL(INDC,NDIM,N2DIM,NPROP,PROP,IVERT,IRET,ICORD,IUNIT,
     1     IPAIR,IY,PMCOL,PMROW,MIXFRA,INFOD,NEQ,NZEV,NC,
     1     QEVB,ERVROW,ERVCOL,
     1     LRV,RV,RPAR,IPAR,LRW,RW,LIW,IW,IERR,
     1     nonew,NVERM,NVERT)
C***********************************************************************
C
C     delete the cell if cell directions gave problems 
C
C***********************************************************************
      IF(IERR.EQ.-6.OR.IERR.EQ.-5) THEN    
        NVERT = NVERT-NONEW
        ICF = 0
        GOTO 600
      ENDIF
C***********************************************************************
C     Regular exit
C***********************************************************************
 600  RETURN
      END
      SUBROUTINE UPDCEL(INDC,INFO,ICF,NDIM,N2DIM,IY,IVERT,
     1     NVERT,ICORD,IRET,IUNIT,IPAIR,NPROP,PROP,PMCOL,PMROW,MIXFRA,
     1     INFOD,NEQ,NZEV,NC,QEVB,ERVROW,ERVCOL,
     1     LRV,RV,RPAR,IPAR,LRW,RW,LIW,IW,NFIOUT,IERR)
C***********************************************************************
C
C     Update a cell
C
C***********************************************************************
C***********************************************************************
C     Storage organization
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C***********************************************************************
C     Storage organization
C***********************************************************************
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)    ::  QEVB,ERVROW,ERVCOL
      DIMENSION IY(NDIM),IVERT(N2DIM),ICORD(NDIM,*)
      DIMENSION IUNIT(NDIM,N2DIM),IPAIR(NDIM,N2DIM),IRET(*)
      DIMENSION PROP(NPROP,*)
      DIMENSION RV(LRV),RPAR(*),IPAR(*),RW(LRW),IW(LIW)
      DIMENSION PMCOL(NEQ,NC),PMROW(NC,NEQ)
C***********************************************************************
C     Pointer array for PROP
C***********************************************************************
      COMMON/BLOCOR/IIRAT,IISMF,IISMFP,IIRV,IIPRO,IIPNU,IILAM,NPROOT
C***********************************************************************
C     reset flag
C***********************************************************************
      IREP = 0
      DO 10 I=1,N2DIM
         IV = IVERT(I)
         IR = IRET(IV)
         IF(INFO.EQ.2.AND.IR.EQ.-1) THEN
            CALL CHKDOM(0,NEQ,PROP(IISMF:IISMF-1+NEQ,IV),
     1                     PROP(IISMF:IISMF-1+NEQ,IV),IRCH,VHIT,VALUE)
            IF(IRCH.EQ.0) IR = 11
         ENDIF
         IF(IR.LT.-1.OR.IR.GT.0) THEN
            IRET(IV) = 11
            IREP = IREP + 1
         ENDIF
 10   CONTINUE
      IF(IREP.EQ.0) GOTO 600
C***********************************************************************
C     produce test output 
C***********************************************************************
      WRITE(NFIOUT,*) ' trying to repair cell',INDC
C***********************************************************************
C     Set pointers for the vertices
C***********************************************************************
      CALL CALCEL(INDC,NDIM,N2DIM,NPROP,PROP,IVERT,
     1     IRET,ICORD,IUNIT,IPAIR,IY,PMCOL,PMROW,MIXFRA,
     1     INFOD,NEQ,NZEV,NC,
     1     QEVB,ERVROW,ERVCOL,
     1     LRV,RV,RPAR,IPAR,LRW,RW,LIW,IW,IERR,
     1     nonew,NVERM,NVERT)
C***********************************************************************
C     Regular exit
C***********************************************************************
 600  RETURN
      END
      SUBROUTINE CALCEL(INDC,NDIM,N2DIM,NPROP,PROP,IVERT,IRET,ICORD,
     1     IUNIT,IPAIR,IY,PMCOL,PMROW,MIXFRA,INFOD,NEQ,NZEV,NC,
     1     QEVB,ERVROW,ERVCOL,
     1     LRV,RV,RPAR,IPAR,LRW,RW,LIW,IW,IERR,
     1     nonew,NVERM,NVERT)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      LOGICAL TEST,LOCUP
      PARAMETER (ZERO=0.0D0) 
C***********************************************************************
C     ARRAYS                              
C***********************************************************************
      DIMENSION IRET(*),ICORD(NDIM,*),IY(*)
      DIMENSION IVERT(N2DIM),IUNIT(NDIM,N2DIM),IPAIR(NDIM,N2DIM)
      DIMENSION PROP(NPROP,*)
      DIMENSION RV(LRV)
      DIMENSION RPAR(*),IPAR(*)
      DIMENSION RW(LRW),IW(LIW)
      DIMENSION PMCOL(NEQ,NC),PMROW(NC,NEQ)
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)    ::  QEVB,ERVROW,ERVCOL
C***********************************************************************
C     Local arrays 
C***********************************************************************
      INTEGER,          DIMENSION(N2DIM)     :: INV,IHELP
      DOUBLE PRECISION, DIMENSION(NC)        :: CV,CVP,PCOM
      DOUBLE PRECISION, DIMENSION(NEQ)       :: PREDIM1,SMFM,SMFP,
     1                                          AR,AI,SMF
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)   :: XTRDIM1,XTCDIM1
C***********************************************************************
C     Common blocks for communication  with table setup 
C***********************************************************************
      COMMON/BLOCOR/IIRAT,IISMF,IISMFP,IIRV,IIPRO,IIPNU,IILAM,NPROOT
      COMMON/ILDMPA/IMAILD,ILMILD,IOILDM
      COMMON/BICHKD/ICHKD
      COMMON/BIGENI/IGENI,LNF
C***********************************************************************
C     USEFUL CONSTANTS
C***********************************************************************
      NFIOUT = 6 
      IHELP  = 0
C***********************************************************************
C     Loop over the vertices
C***********************************************************************
      TEST   = .FALSE.
C***********************************************************************
C     Check whether this is a corner update
C***********************************************************************
      NONEW = 0
      NOBAD = 0
      DO 5 IV = 1,N2DIM
         INDVER = IVERT(IV)
         IRETIV = IRET(INDVER)
         IF(IRETIV.EQ.10) NONEW = NONEW + 1
C        IF(IRETIV.GT.0.AND.IRETIV.NE.10.AND.IRETIV.NE.11) 
C    1        NOBAD = NOBAD + 1
         IF(IRETIV.NE.0.AND.IRETIV.NE.10.AND.IRETIV.NE.11)
     1        NOBAD = NOBAD + 1
    5 CONTINUE
      LOCUP = .TRUE.
C-UM  IF(NONEW.GE.N2DIM/2) LOCUP =.FALSE.
      IF(NONEW+NOBAD.GE.N2DIM/2) LOCUP =.FALSE.
C***********************************************************************
C     Check whether there is a vertex which is completely o.k.
C***********************************************************************
      DO 10 IV = 1,N2DIM
         INDVER = IVERT(IV)
         IRETIV = IRET(INDVER) 
         IF(IRETIV.EQ.0) THEN
            INDVV = INDVER
            IF(TEST) WRITE(NFIOUT,*) ' starting 
     1           vertex with iflag 0 found '
            GOTO 30
         ENDIF
 10   CONTINUE
C***********************************************************************
C     Check whether there is at least a vertex which is a bit o.k.
C***********************************************************************
      DO 20 IV = 1,N2DIM
         INDVER = IVERT(IV)
         IRETIV = IRET(INDVER)
         IF(IRETIV.LE.0) THEN
            INDVV = INDVER
            IF(TEST) WRITE(NFIOUT,*) ' starting vertex with 
     1                  iflag <0 found '
            GOTO 30
         ENDIF
 20   CONTINUE
C***********************************************************************
C     No valid vertex found for the cell (this should not happen)
C***********************************************************************
      WRITE(NFIOUT,*) ' no valid starting vertex found '
      STOP
 30   CONTINUE
      INDVVO = INDVV
C***********************************************************************
C     update cell directions
C***********************************************************************
      IF(LOCUP) THEN
         write(6,*) ' local update for the cell'
         CALL DIRCEL(INFOD,-1,0,NDIM,N2DIM,IVERT,
     1    IRET,IPAIR,PROP(IISMF,1),NPROP,NEQ,
     1    PMCOL(1,NZEV-MIXFRA+1),NFIOUT,IERR)
         IF(INFOD.EQ.1.AND.IERR.GT.0) THEN 
            WRITE(*,*) ' check update procedure'
            STOP
         ENDIF
         IF(INFOD.GT.0) THEN
            CALL UPDPAR(NC,NZEV,NEQ,PMCOL,PMROW,NFIOUT)
         ENDIF
      ENDIF
C***********************************************************************
C     Loop over vertices
C***********************************************************************
      DO 1000 IV = 1,N2DIM
         INDVER = IVERT(IV)
C***********************************************************************
C     Check whether this node already exists
C***********************************************************************
         IF(IRET(INDVER).LT.10) GOTO 1000
C***********************************************************************
c     find direct neighbours of a vertex (evbaux.f)
C***********************************************************************
         INV(1:N2DIM) = 0
         IORT = 0 
CUUUU    IF(IGENI.NE.0) IORT = -1
         CALL IFNVER(IORT,IV,NDIM,N2DIM,IVERT,IRET,IUNIT,NNV,INV)
         IF(NNV.GT.0) THEN
            IF(INDC.EQ.1)THEN
               J = 1
               DO I=1,N2DIM
                  IF(INV(I).NE.0)THEN
                     IHELP(J) = INV(I)
                     J = J+1
                  ENDIF
               ENDDO
               INDVV = NVERM
               DO I=1,J
                  IF(IHELP(I).GT.0) THEN
                  IF(IVERT(IHELP(I)).LT.INDVV) THEN
                     INDVV = IVERT(IHELP(I))
                  ENDIF
                  ENDIF
               ENDDO
            ELSE
                IF(IGENI.NE.0) THEN
                 AAAMAX = -1
                 DO I=1,NNV
                II    = IVERT(INV(I))
                PCOM(1:NC-NDIM)    = ZERO
                PCOM(NC-NDIM+1:NC) = ICORD(:,INDVER) - ICORD(:,II)
                PREDIM1 = MATMUL(PMCOL,PCOM(1:NC))
                  ISMF = NDIM + 1
                  NDM  = MIXFRA 
         CALL DCOPY(NEQ,PROP(IISMF,II),1,SMF,1)
                  CALL INIVDI(0,NDIM,NDM,NEQ,NZEV,NC,SMF,SMFM,VDIN,
     1     NVERT,INDVER,II,ICORD,IRET,NPROP,PROP,ISMF,NFIOUT,IERR)
         VDIN   = SQRT(DOT_PRODUCT(SMFM-SMF,SMFM-SMF))
                 VNPR= SQRT(DOT_PRODUCT(PREDIM1(1:NEQ),PREDIM1(1:NEQ)))
                 AAA = ABS(DOT_PRODUCT(PREDIM1,SMFM-SMF) /
     1                  (VNPR*VDIN))
              WRITE(*,*) 'iiiiiiiiiiiii',VDIN,VNPR,AAA
                 IF(AAA.GT.AAAMAX) THEN
                   AAAMAX = AAA
                   INDVV = II
                 ENDIF
C----- PREDIM1 would be th secant predictor
              ENDDO
             ELSE
                INDVV    = IVERT(INV(NNV))
             ENDIF
            ENDIF
            write(6,*) ' found',indvv,' ret=',iret(indvv)
         ELSE
C         IF(IVERT(IV).GT.N2DIM) GOTO 1000
            INDVV = INDVVO
            write(6,*) ' not found',indvv,' ret=',iret(indvv)
         ENDIF
         INDVER = IVERT(IV)
         CALL DCOPY(NEQ,PROP(IISMF,INDVV),1,SMF,1)
C***********************************************************************
C     Set pathfollowing vector to the suitable linear combination
C***********************************************************************
      CV(1:NC) = MATMUL(PMROW(1:NC,1:NEQ),SMF(1:NEQ))
      PCOM(1:NC-NDIM)    = ZERO
      PCOM(NC-NDIM+1:NC) = ICORD(:,INDVER) - ICORD(:,INDVV)
      CV(NC-NDIM+1:NC)   = CV(NC-NDIM+1:NC) + PCOM(NC-NDIM+1:NC)
C***********************************************************************
C
C     Get secant predictor vector
C
C***********************************************************************
      IF(INDC.GT.1) THEN
        PREDIM1 = MATMUL(PMCOL,PCOM(1:NC))
      ELSE
        PREDIM1 = 0
      ENDIF
      BETOLD1 = SQRT(DOT_PRODUCT(PREDIM1,PREDIM1))
C***********************************************************************
C     determine the mixing projection
C***********************************************************************
      IF(IGENI.NE.0) THEN
      ISMF = NDIM + 1
      NDM  = MIXFRA 
      CALL INIVDI(1,NDIM,NDM,NEQ,NZEV,NC,SMF,SMFM,VDIN,
     1     NVERT,INDVER,INDVV,ICORD,IRET,NPROP,PROP,ISMF,NFIOUT,IERR)
      IF(IERR.NE.0) GOTO 1001
      ENDIF
C***********************************************************************
C     Set pathfollowing vector to the suitable linear combination
C***********************************************************************
         CALL MANPOI(3,NEQ,SMF,SMFP,NC,CV,CVP,LRV,RV,IRETMN,
     1        LIW,IW,LRW,RW,RPAR,IPAR,
     1        NZEV,NDIM,IGENI,ICHKD,IMAILD,ILMILD,IOILDM,
     1        BETOLD1,PREDIM1,PMROW,
     1        ERVROW,ERVCOL,QEVB,AR,AI,XTRDIM1,XTCDIM1)
C**********************************************************************
C     successful search of point
C**********************************************************************
         IRET(INDVER)   = -12
         IF(IRETMN.EQ.0) IRET(INDVER) = 0
C**********************************************************************
C     error during path finding
C**********************************************************************
         IF(IRETMN.EQ.5) IRET(INDVER) = -12
C**********************************************************************
C     physically unreasonable value during path finding
C**********************************************************************
         IF(IRETMN.EQ.4) IRET(INDVER) = -11
         IF(IRETMN.EQ.3) IRET(INDVER) = -13
         IF(IRETMN.EQ.2) IRET(INDVER) = -9
         IF(IRETMN.EQ.1) IRET(INDVER) = -8
         IF(IRETMN.EQ.-1) IRET(INDVER) = -1
         IF(IRETMN.EQ.-3) IRET(INDVER) = -1
         IF(IRETMN.EQ.-9) IRET(INDVER) = -5
***********************************************************************
C     Check results
C***********************************************************************
         IF(IRET(INDVER).EQ.-12) THEN
            IRET(INDVER) = -4
         ENDIF
C***********************************************************************
C     Check results of SMF
C     re-arrange flag if the cell is outside the desired domain
C***********************************************************************
         IF(IRET(INDVER).EQ.0) THEN
            CALL CHKDOM(0,NEQ,SMF,SMF,IRCH,VHIT,VALUE)
            IF(IRCH.NE.0) THEN
               IRET(INDVER) = -7
            ENDIF
         ENDIF
C**********************************************************************
C     store values 
C**********************************************************************
C-Sl- Stability Analysis-----------------------------------------------
C     CALL STABAN( 0,NEQ,NC,NEQ-NC,QEVB,SMF,TIME,AR,AI,
C    �           RPAR,IPAR,IERR )
C-Sl- End of analysis--------------------------------------------------
         CALL STOVAL(INDVER,NDIM,NC,NEQ,LRV,NPROP,PROP,SMF,SMFP,
     1    RV,AR,AI,XTRDIM1,XTCDIM1,CV,CVP,INDC,IRET) 
c**********************************************************************
 1000 CONTINUE
 1001 CONTINUE
C**********************************************************************
C     Check whether the cell is o.k.
C**********************************************************************
      NZRC = 0
      NTRC = 0
      NNRC = 0
      NPRC = 0
      DO 3100 IV = 1,N2DIM
         INDVER = IVERT(IV)
         IRETCO = IRET(INDVER)
         IF(IRETCO.EQ. 0) NZRC = NZRC + 1
         IF(IRETCO.EQ.10) NTRC = NTRC + 1
         IF(IRETCO.LT. 0) NNRC = NNRC + 1
         IF(IRETCO.GT. 0) NPRC = NPRC + 1
 3100 CONTINUE
C     IF(NTRC.NE.0) THEN
C        WRITE(NFIOUT,*) ' Fatal error in CALCEL '
C        STOP
C     ENDIF
      IF(TEST) THEN
         IF(NZRC.EQ.N2DIM) THEN
            WRITE(NFIOUT,8311) INDC
 8311       FORMAT(' Cell',I6,' is completely o.k.')
         ELSE
            WRITE(NFIOUT,8312) INDC,NZRC,NNRC,NPRC
 8312       FORMAT(' Cell',I6,' has',I4,' nodes with zero     
     1           return codes',/ '               ',I4,
     1           ' nodes with negative return codes',/
     1           '           and ',I4,' nodes with 
     1           positive return codes')
         ENDIF
      ENDIF
C**********************************************************************
C     Regular exit
C**********************************************************************
      RETURN
C***********************************************************************
C     END OF CALCEL
C***********************************************************************
      END
