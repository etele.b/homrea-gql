C  engine.f
      SUBROUTINE ENGIN1(NENGIN,XU,NS,NSYMB,TBEG,TEND,NFILE3,NFILE6)
C**********************************************************************C
C                                                                      C
C     *************************************************************    C
C     *                                                           *    C
C     *           READ INPUTDATA FOR ENGINESIMULATION             *    C
C     *                      FROM FILE fort.3                     *    C
C     *                                                           *    C
C     *                (by A. Schubert, 20.10.2005)               *    C
C     *                                                           *    C
C     *************************************************************    C
C                                                                      C
C**********************************************************************C
C                                                                      C
C     VARIABLES:                                                       C
C           CABEG  = start crank angle      [deg]                      C
C           CAEND  = end crank angle        [deg]                      C
C           VOLUME = cylinder volume        [m^3]                      C
C           COMRAT = compression ratio      [-]                        C
C           DISP   = displacment volume     [m^3]                      C
C           ROD    = connecting rod         [m]                        C
C           BORE   = bore                   [m]                        C
C           STROKE = stroke                 [m]                        C
C           ESPEED = engine speed           [min^(-1)]                 C
C           ROD    = connecting rod         [m]                        C
C           NFUEL  = kind of fuel 1)iso-octane/n-heptane               C
C           AFRATI = air/fuel ratio         [-]                        C
C           RON    = research octane number [-]                        C
C                                                                      C
C**********************************************************************C
C     I. STORAGE ORGANIZATION                                          C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      CHARACTER(LEN=*) NSYMB(MNSPE+MNTHB+1)
      CHARACTER*6 SYMB
      DIMENSION XU(MNSPE)
      COMMON/ENGDA1/ESPEED,DISP,COMRAT,ROD,BORE,STROKE,
     1              CABEG,CAEND,AFRATI,RON,NFUEL
      DATA ZERO/0.0D0/
C**********************************************************************C 
      READ(NFILE3,800) SYMB
      IF(SYMB.NE.'ENGINE') GOTO 910
      READ(NFILE3,801,END=920) NFUEL,ESPEED,CABEG,CAEND
      READ(NFILE3,802) DISP,COMRAT,ROD,BORE
      READ(NFILE3,802) STROKE,AFRATI,RON,DUMMY
C---- COMPUTATION TIMING VARIABLES, UNIT SEC. !!!
      TBEG   = ZERO
      TEND   = (CAEND - CABEG) / (6*ESPEED)
      IF(TEND .LT. ZERO) GOTO 930
      IF(TEND .EQ. ZERO) GOTO 931
      RETURN
C**********************************************************************C
C     VIII. FORMAT STATEMENTS                                          C
C**********************************************************************C
  800 FORMAT(1X,6A)
  801 FORMAT(32X,I3,1X,3(1PE11.4,1X))
  802 FORMAT(24X,4(1PE11.4,1X))
C***********************************************************************
C     IV: ERROR EXITS
C***********************************************************************  
  920 WRITE(NFILE6,921)
  921 FORMAT(6X,'ERROR - INPUT DATA INCOMPLETE')
  910 CONTINUE
      WRITE(NFILE6,995) '          ERROR in -ENGIN1-           '
      WRITE(NFILE6,995) '  CANNOT READ FROM FORT.3, CHECK IT   '
      STOP
  930 CONTINUE
      WRITE(NFILE6,990) '          ERROR in -ENGCON-           '
      WRITE(NFILE6,990) 'COMP. ENDTIME TEND NEGATIVE - CHECK IT'
      STOP
  931 CONTINUE
      WRITE(NFILE6,990) '          ERROR in -ENGCON-           '
      WRITE(NFILE6,990) '     TEND EQUAL TBEG  - CHECK IT      '
      STOP
  990 FORMAT(15('+'),4X,A38,4X,15('+'))
  995 FORMAT(15('+'),4X,A38,4X,15('+'))
C***********************************************************************
C     END OF -ENGIN1-
C***********************************************************************
      END

      SUBROUTINE CALVOL(NFILE6,TIME,VOLUME,VPR)
C**********************************************************************C
C                                                                      C
C     *************************************************************    C
C     *                                                           *    C
C     *           CALCULATION OF TIME DEPENDENT VOLUME            *    C
C     *                    AND TIME DERIVATION                    *    C
C     *                                                           *    C
C     *                 (by O. Maiwald, 20.08.2001)               *    C
C     *                                                           *    C
C     *************************************************************    C
C                                                                      C
C**********************************************************************C
C     This programm calculate the volume curve of an engine as a       C
C     function of the geometry parameters and time.                    C
C                                                                      C
C     Crank angles (CPHI) predefinition:  -180=BDC   0=TDC   +180=BDC  C
C                                                                      C
C**********************************************************************C
C                                                                      C
C     INPUT  : time (sec)                                              C
C                                                                      C
C     OUTPUT : time dependent volume  (m^3)                            C
C                                                                      C
C     VARIABLES:                                                       C
C           CPHI   = crank angle       [deg]                           C
C           VOLUME = cylinder volume   [m^3]                           C
C           SURFAC = cylinder surface  [m^2]                           C
C           VC     = clearance volume  [m^3]                           C
C           RATIO  = ratio of crank radius to connecting rod  [-]      C 
C           SPIST  = piston                                            C
C                                                                      C
C**********************************************************************C

C**********************************************************************C
C     CHAPTER I: STORAGE ORGANIZATION                                  C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      COMMON/ENGDA1/ESPEED,DISP,COMRAT,ROD,BORE,STROKE,
     1              CABEG,CAEND,AFRATI,RON,NFUEL
C**********************************************************************C
C     III. DATA STATEMENTS                                             C
C**********************************************************************C
      DATA PI/3.141592654D0/,ZERO/0.0D0/
C**********************************************************************C
C     CHAPTER II: INITIALIZATION                                       C
C**********************************************************************C 
C**********************************************************************C
C     CHAPTER III: CALCULATION                                         C
C**********************************************************************C
C**********************************************************************C
C     III.1: INSTANTANEOUS CYLINDER VOLUME                             C
C**********************************************************************C
      VC    = DISP/(COMRAT-1)
      RATIO = (STROKE/2)/ROD
      CPHI  = TIME * 6 * ESPEED + CABEG
      PH    = CPHI*PI/180
C---- PISTON MOVEMENT
      TERM1 = 1-COS(PH)
      TERM2 = 1-((RATIO**2)*(SIN(PH))**2)
      IF (TERM1 .LT. ZERO .OR. TERM2 .LT. ZERO) GOTO 190
      SPIST = (STROKE/2)*(TERM1+1/RATIO*(1-SQRT(TERM2)))
C---- CYLINDER VOLUME
      VOLUME= (VC+(PI*(BORE**2)/4*SPIST))
C**********************************************************************C
C     III.2: TIME DERIVATION OF THE CYLINDER VOLUME                    C
C**********************************************************************C
      TERM3 = PI*(BORE**2)/4*STROKE/2 * PI/30*ESPEED
      VPR   = TERM3 * (SIN(PH)+RATIO/2*SIN(2*PH)/SQRT(TERM2))
C**********************************************************************C
C     CHAPTER IV: OUTPUT FOR TESTING                                   C
C**********************************************************************C
      IF (2.EQ.1) THEN
        WRITE(NFILE6,844) TIME,VOLUME
 844    FORMAT(1X,'TIME, VOLUME: ',1PE10.3,5X,1PE9.3)
      ENDIF
C**********************************************************************C
C     CHAPTER V. REGULAR EXIT                                          C
C**********************************************************************C
      RETURN
C**********************************************************************C
C     CHAPTER VI. ERROR EXITS                                          C
C**********************************************************************C
  190 CONTINUE
      WRITE(NFILE6,*) '+++++++++++  ERROR in CALVOL !!!  +++++++++++'
      STOP
C**********************************************************************C
C     END OF -CALVOL-                                                  C
C**********************************************************************C
      END

      SUBROUTINE ENGOUT(NENGIN,NFILE6)
C**********************************************************************C
C                                                                      C
C     *************************************************************    C
C     *                                                           *    C
C     *       OUTPUT FOR ENGINE CYCLE COMPUTATION ON NFILE6       *    C
C     *                                                           *    C
C     *                (by A. Schubert, 20.10.2005)               *    C
C     *                                                           *    C
C     *************************************************************    C
C                                                                      C
C**********************************************************************C
C                                                                      C
C     VARIABLES:                                                       C
C**********************************************************************C
C     I. STORAGE ORGANIZATION                                          C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      COMMON/ENGDA1/ESPEED,DISP,COMRAT,ROD,BORE,STROKE,
     1              CABEG,CAEND,AFRATI,RON,NFUEL
C**********************************************************************C
      IF (NENGIN.GE.1) THEN
        WRITE(NFILE6,81)
      ENDIF
C***********************************************************************
      RETURN
C***********************************************************************
C     VIII. FORMAT STATEMENTS
C***********************************************************************
   81 FORMAT(1X,79('*'),/,1X,25('*'),2X,'ENGINE CYCLE COMPUTATION',
     1       2X,26('*'))
C***********************************************************************
C     IV: ERROR EXITS
C***********************************************************************
C***********************************************************************
C     END OF -ENGOUT-
C***********************************************************************
      END

      SUBROUTINE ENGOU1(NC,S1,S2,S3,S4,S5,S6,S7,T,V,SPR,NSYMB,PSYMB,
     1           KONS,V1,VOR,V2,V3,ENTH,NENGIN,ICON,ARTEMP,ARC,ARSH,
     1           ARWI,RM,AMASS,NVAR,NSC,NSPEC,NTS,NUR,NFIL38,NFIL39,
     1           NFIL48,NFIL49,NFILE6,NOP)
C**********************************************************************C
C                                                                      C
C     *************************************************************    C
C     *                                                           *    C
C     *              OUTPUT FOR ENGINE CYCLE COMPUTATION          *    C
C     *                                                           *    C
C     *                (by A. Schubert, 20.10.2005)               *    C
C     *                                                           *    C
C     *************************************************************    C
C                                                                      C
C**********************************************************************C
C                                                                      C
C     VARIABLES:                                                       C
C**********************************************************************C
C     I. STORAGE ORGANIZATION                                          C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      CHARACTER*28 PSYMB
      CHARACTER(LEN=*) NSYMB(*),S1,S2,S3,S4,S5,S6,S7
      DIMENSION T(NTS),PHI(NTS),V(NTS,*),KONS(NSC),V1(NTS,NUR),
     1          V2(NTS,NUR),V3(NTS,NUR),VOR(NTS,NUR),SPR(NTS,NUR,*),
     1          ENTH(NTS,NUR),RM(NTS,NUR),AMASS(NTS)
      DIMENSION ARTEMP(NTS),ARC(NTS),ARSH(NTS),ARWI(NTS,NSPEC)
      COMMON/ENGDA1/ESPEED,DISP,COMRAT,ROD,BORE,STROKE,
     1              CABEG,CAEND,AFRATI,RON,NFUEL
C***********************************************************************
C     WRITE IN GNUPLOT FORMAT
C*********************************************************************** 
      DUMMY = 0.0
      DO K=1,NTS
        PHI(K) = 6 * T(K) * ESPEED + CABEG
      ENDDO
C----  WRITE HEADER FOR EACH COMPUTATION
      WRITE(NFIL39,81) NTS,PSYMB
      WRITE(NFIL39,82)
      NCOL=MIN0(NOP,NSPEC)
      WRITE(NFIL39,84) ' t(CA)  ',S1,S2,S3,S4,S5,(NSYMB(K),K=1,NCOL)
      DO M=1,NTS
            WRITE(NFIL39,85) PHI(M), ARTEMP(M), ARC(M),V3(M,1),
     1             SUM(VOR(M,1:NUR)),ARSH(M),(ARWI(M,I),I=1,NCOL)
      ENDDO
      WRITE(NFIL39,89)
C***********************************************************************
C     WRITE IN TECPLOT FORMAT
C***********************************************************************
C       OUTPUT OF TITLE
        WRITE(NFIL38,850) PSYMB
C       OUTPUT OF HEADING
        WRITE(NFIL38,851) ' t(CA)  ',S1,S2,S3,S4,S5,(NSYMB(K),K=1,NCOL)
        WRITE(NFIL38,855) NTS
C----
        WRITE(NFIL48,840) PSYMB
        WRITE(NFIL48,831) ' t(CA)  ',
     1     S6,S7,S1,S2,S3,S4,S5,(NSYMB(K),K=1,NCOL)
        WRITE(NFIL49,840) PSYMB
        WRITE(NFIL49,831) ' t(CA)  ',
     1     S6,S7,S1,S2,S3,S4,S5,(NSYMB(K),K=1,NCOL)
C-----
        DO J=1,NTS
         WRITE(NFIL38,852) PHI(J),ARTEMP(J),ARC(J),V3(J,1),
     1         SUM(VOR(J,1:NUR)),ARSH(J),(ARWI(J,K),K=1,NCOL)
C----- Single Zones!
          WRITE(NFIL48,835) NUR
          DO I=1,NUR
           DI = I
           DUMMY = RM(J,I)/AMASS(J)
           WRITE(NFIL48,832) PHI(J),DI,DUMMY,V1(J,I),V2(J,I),
     1           V3(J,I),VOR(J,I),ENTH(J,I),(SPR(J,I,K),K=1,NCOL)
          ENDDO
        ENDDO
        DO I=1,NUR
          WRITE(NFIL49,835) NTS
          DO J=1,NTS
            DI = I
            DUMMY = RM(J,I)/AMASS(J)
            WRITE(NFIL49,832) PHI(J),DI,DUMMY,V1(J,I),V2(J,I),
     1           V3(J,I),VOR(J,I),ENTH(J,I),(SPR(J,I,K),K=1,NCOL)
          ENDDO
        ENDDO
C***********************************************************************
      RETURN
C***********************************************************************
C     VIII. FORMAT STATEMENTS
C***********************************************************************
   81 FORMAT('%DATA',I5,A28)
   82 FORMAT(3X,A,6X,1PE25.18)
   83 FORMAT(6(1X,1PE12.5))
   84 FORMAT('%',1(2X,A,1X,63(2X,A,1X)))
   85 FORMAT(1(1X,64(1PE10.2E3,1X)))
   89 FORMAT('%<    ',75X)
  831 FORMAT('VARIABLES =',/,7('"',A,'"',',':))
  832 FORMAT(1(3X,7(1PE10.3,1X)))
  835 FORMAT('ZONE I=',I4,', J=1, F=POINT')
  840 FORMAT('TITLE= "Output from MULZON -Reactorwise-',
     1       A28,'"')
  850 FORMAT('TITLE= "Output from MULZON -Average Values-',
     1       A28,'"')
  851 FORMAT('VARIABLES =',/,7('"',A,'"',',':))
  852 FORMAT(1(3X,7(1PE10.3,1X)))
  855 FORMAT('ZONE I=',I4,', J=1, F=POINT')
  856 FORMAT('TITLE= "Output from MULZON - Reactorwise - ',
     1       'over crank angle -"',A)
  858 FORMAT(1(3X,8(1PE10.3,1X)))
C***********************************************************************
C     IV: ERROR EXITS
C***********************************************************************
C***********************************************************************
C     END OF -ENGOU1-
C***********************************************************************
      END

      SUBROUTINE EGROUT(NSYMB,NTS,NSPEC,NUR,ARTEMP,ARC,ARWI,AMASS,
     1                  V3,VOR,NFILE6)
C**********************************************************************C
C                                                                      C
C     *************************************************************    C
C     *                                                           *    C
C     *    OUTPUT OF THE SPECIES MASS FRACTIONS TO 'fort.32'      *    C
C     *                                                           *    C
C     *              (by A. Schubert, 25.10.2005)                 *    C
C     *                                                           *    C
C     *************************************************************    C
C                                                                      C
C**********************************************************************C
C                                                                      C
C     INPUT  : species and additional input data                       C
C                                                                      C
C     OUTPUT : - exhaust mole fractions to 'fort.32'                   C
C              - exhaust temperature and pressure of previous calc.    C
C                                                                      C
C     PURPOSE: - output of species mass fractions from the final       C
C                species mass fractions at TEND.                       C
C              - input as exhaust gas recycled (EGR) for the next      C
C                calculation                                           C
C                                                                      C
C     VARIABLES: XSPE   = species mass fraction at time t              C
C                                                                      C
C**********************************************************************C
C**********************************************************************C
C     I: STORAGE ORGANIZATION
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      LOGICAL MOLEFR,LPLO,MASSFR
      CHARACTER(LEN=*) NSYMB(*)
      CHARACTER*28 HEADER
      DIMENSION ARTEMP(NTS),ARC(NTS),ARWI(NTS,NSPEC),V3(NTS,NUR),
     1          VOR(NTS,NUR),AMASS(NTS)
      COMMON/BOUT/KONINT,KONTAB,KONPLO,KONSEN,KONMEC,KONANA,KONRED,
     1  KONSPE(MNSPE),KONSID(MNSPE),NSC,
     1  ICRIT,KOGSEN,KONRST,MOLEFR,MASSFR,LPLO
      DATA NFIL32/32/,ZERO/0.0D0/,SMALL/1.0D-09/
C**********************************************************************C
C     II. OUTPUT OF SPECIES MASS NUMBERS TO 'fort.32'
C**********************************************************************C
C_ASSA: INITIALIZATION:
      HEADER='      Mass fractions        '
      IF(MASSFR)THEN
C---- LEAVE MASS FRACTIONS
      ELSE 
C---- TRANSFORM CONCENTRATIONS INTO MOLE FRACTIONS
        IF(.NOT.MOLEFR) THEN
          DO M=1,NTS
            DO L = 1,NSPEC
              ARWI(M,L) = ARWI(M,L)/ARC(M)
            ENDDO
          ENDDO
        ENDIF
C---- TRANSFORM MOLE FRACTIONS INTO MASS FRACTIONS
        DO M=1,NTS
          ARWIM = ZERO
          DO L = 1,NSPEC
            ARWIM = ARWIM + (ARWI(M,L)*XMOL(L))
          ENDDO
          DO L = 1,NSPEC
            ARWI(M,L) = (ARWI(M,L)*XMOL(L))/ARWIM
          ENDDO
        ENDDO
      ENDIF

C---- CHECKS
      IF(ARTEMP(NTS) .LE. ZERO .OR. V3(NTS,1) .LE. ZERO) GOTO 910
C---- OUTPUT
      OPEN(UNIT=NFIL32,FILE='fort.32',POSITION='REWIND')
      WRITE(NFIL32,860) '>Exhaust',HEADER
      WRITE(NFIL32,870) 'T(egr)  ',ARTEMP(NTS),'[K]  '
      WRITE(NFIL32,870) 'p(egr)  ',V3(NTS,1)  ,'[bar]'
      WRITE(NFIL32,870) 'm(egr)  ',AMASS(NTS) ,'[kg] '
      DO L=1,NSPEC
        IF(ARWI(NTS,L).GT.SMALL) THEN  
          WRITE(NFIL32,870) NSYMB(L),ARWI(NTS,L)
        ENDIF
      ENDDO
      WRITE(NFIL32,890)
C**********************************************************************C
C     IV. FORMAT STATEMENTS
C**********************************************************************C
  800 FORMAT(1PE12.6)
  860 FORMAT(A,15X,A28)
  870 FORMAT(1(A,':',1PE14.6,1X,A5))
  890 FORMAT('<    ',75X)
C**********************************************************************C
C     V. REGULAR EXIT
C**********************************************************************C
      CLOSE(NFIL32)
      RETURN
C**********************************************************************C
C     VI. ERROR EXITS
C**********************************************************************C
  910 CONTINUE
      WRITE(NFILE6,911) 'ERROR in -EGROUT-  (see engine.f)  !!!'
      WRITE(NFILE6,911) 'ERROR in -EGROUT-  (see engine.f)  !!!'
  911 FORMAT(13('+'),5X,A38,5X,13('+'))
      STOP
C***********************************************************************
C     END OF -EGROUT-                                                  *
C***********************************************************************
      END

      SUBROUTINE GESPNU(NFUEL,NS,NSYMB,KFUEL,NFILE6)
C**********************************************************************C
C                                                                      C
C     *************************************************************    C
C     *                                                           *    C
C     *   GET SPECIES NUMBERS FROM THE INITIAL FUEL AIR MIXTURE   *    C
C     *                                                           *    C
C     *              (by A.Schubert, 07.11.2005)                  *    C
C     *                                                           *    C
C     *************************************************************    C
C                                                                      C
C**********************************************************************C
C                                                                      C
C     INPUT  :    NS     : number of species                           C
C                 NSYMB  : species symbols                             C
C                 NFUEL  : 1 = iso-octane/n-heptane                    C
C**********************************************************************C
C**********************************************************************C
C     CHAPTER I: STORAGE ORGANIZATION                                  C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION KFUEL(4) 
      CHARACTER(LEN=*)  NSYMB(*)
      LOGICAL EQUSYM
C**********************************************************************C
C     I.1: DATA STATEMENTS                                             C
C**********************************************************************C
      KFUEL(1:4) = 0
C**********************************************************************C
C     II.1: DETERMINE SPECIES NUMBERS
C**********************************************************************C
      IF (NFUEL .EQ. 1) THEN
        DO K=1,NS
          IF(EQUSYM(8,NSYMB(K),'O2      ')) KFUEL(1)=K
          IF(EQUSYM(8,NSYMB(K),'N2      ')) KFUEL(2)=K
          IF(EQUSYM(8,NSYMB(K),'IC8H18  ')) KFUEL(3)=K
          IF(EQUSYM(8,NSYMB(K),'NC7H16  ')) KFUEL(4)=K
        ENDDO
      ENDIF
C***********************************************************************
C     END OF -GESPNU-
C***********************************************************************
      END

      SUBROUTINE LAMOUT(L,NFUEL,NS,NSYMB,W0,NFILE6)
C**********************************************************************C
C                                                                      C
C     *************************************************************    C
C     *                                                           *    C
C     *         CALCULATE AIR FUEL RATIO IN THE CHAMBER           *    C
C     *                                                           *    C
C     *              (by A.Schubert, 07.11.2005)                  *    C
C     *                                                           *    C
C     *************************************************************    C
C                                                                      C
C**********************************************************************C
C                                                                      C
C     INPUT  :    NS     : number of species                           C
C                 NSYMB  : species symbols                             C
C                 NFUEL  : 1 = iso-octane/n-heptane                    C
C**********************************************************************C
C**********************************************************************C
C     CHAPTER I: STORAGE ORGANIZATION                                  C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      DIMENSION KFUEL(4),W0(NS)
      CHARACTER(LEN=*) NSYMB(*)
      COMMON/LAMDAT/AIFUST
C**********************************************************************C
C     I.1: DATA STATEMENTS                                             C
C**********************************************************************C
C**********************************************************************C
C     CALCULATE ACTUAL AIR FUEL RATIO WITH MASS FRACTIONS
C**********************************************************************C
      CALL GESPNU(NFUEL,NS,NSYMB,KFUEL,NFILE6)
      IF (NFUEL.EQ.1) THEN
        AIFUAC = (W0(KFUEL(1))+W0(KFUEL(2)))/(W0(KFUEL(3))+W0(KFUEL(4)))
      ENDIF
      AIFURA = AIFUAC / AIFUST
C**********************************************************************C
C     OUTPUT TO NFILE6
C**********************************************************************C
      WRITE(NFILE6,800)
      WRITE(NFILE6,801) 'REACTOR ',L
      WRITE(NFILE6,802) 'INITIAL CHAMBER AIR-FUEL-RATIO = ',AIFURA
      WRITE(NFILE6,800)
C**********************************************************************C
C    FORMAT STATEMENTS
C**********************************************************************C
  800 FORMAT(2X,52('*'))
  801 FORMAT(2X,3('*'),1X,A,I3,34X,3('*'))
  802 FORMAT(2X,3('*'),1X,A33,1PE10.2,2X,3('*'))
C***********************************************************************
C     END OF -LAMOUT-
C***********************************************************************
      END
 
      SUBROUTINE CALWAL(NWALL,NFILE6,TIME,OVOL,VPIST,CONST)
C**********************************************************************C
C                                                                      C
C     *************************************************************    C
C     *                                                           *    C
C     *           TIME DEPENDENT SURFACE CALCULATION AND          *    C
C     *        ENGINE SPECIFIC VALUES FOR WALL HEAT TRANSER       *    C
C     *                                                           *    C
C     *                 (by O. Maiwald, 20.12.2001)               *    C
C     *                                                           *    C
C     *************************************************************    C
C                                                                      C
C**********************************************************************C
C     This programm calculate the combustion chamber surface as a      C
C     function of time and engine parameters.                          C
C     Also calculation of the CONST and VPIST variables for the wall   C
C     heat transfer calculation using the WOSCHNI equation             C
C     In Chapter III.3 you can choose two different but similar        C
C     equations for the CONST parameter. The first one lead in         C
C     combination with the complete Woschni equation to a higher heat  C
C     transfer coefficient than the second one.                        C
C                                                                      C
C     Crank angles (CPHI) predefinition:  -180=BDC   0=TDC   +180=BDC  C
C                                                                      C
C**********************************************************************C
C                                                                      C
C     INPUT  : time (sec)                                              C
C              NWALL : WALL Option needed for OVOL calculation         C
C                                                                      C
C     OUTPUT : time dependent volume  (m**3)                           C
C              VPIST : mean piston velocity         [m/s]              C
C              OVOL  : surface-volume ration        [1/m]              C
C              CONST : constant in WOSCHNI equation                    C
C                                                                      C
C     VARIABLES FROM COMMON BLOCK /ENGDA1/                             C
C           COMRAT = compression ratio [1]                             C
C           DISP   = stroke volume     [m**3]                          C
C           ROD    = connecting rod    [m]                             C
C           BORE   = bore              [m]                             C
C           STROKE = stroke            [m]                             C
C           ESPEED = engine speed      [1/min]                         C
C           CABEG  = start crank angle                                 C
C                                                                      C
C     VARIABLES:                                                       C
C           CPHI   = crank angle       [CA]                            C
C           VOLUME = cylinder volume   [m**3]                          C
C           SURFAC = cylinder surface   [m**2]                         C
C           Vc     = clearance volume  [m**3]                          C
C           r      = crank radius      [m]                             C
C           RATIO  = ratio of crank radius to connecting rod  [1]      C
C           APIST  = piston surface    [m**2]                          C
C           SPIST  = piston                                            C
C                                                                      C
C**********************************************************************C

C**********************************************************************C
C     CHAPTER I: STORAGE ORGANIZATION                                  C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      COMMON/ENGDA1/ESPEED,DISP,COMRAT,ROD,BORE,STROKE,
     1              CABEG,CAEND,AFRATI,RON,NFUEL
C**********************************************************************C
C     III. DATA STATEMENTS                                             C
C**********************************************************************C
      DATA PI/3.141592654D0/,ZERO/0.0D0/,SMALL/1.0D-12/,NFIL37/37/
C**********************************************************************C
C     CHAPTER III: CALCULATION                                         C
C**********************************************************************C
      VPIST = ZERO
      CONST = ZERO
C**********************************************************************C
C     III.1: INSTANTANEOUS CYLINDER VOLUME                             C
C**********************************************************************C
      Vc = DISP/(COMRAT-1)
      RATIO = (STROKE/2)/ROD
      CPHI = TIME *6*espeed + CABEG
      PH = CPHI*PI/180
C---- PISTON MOVEMENT
      TERM1 = 1-COS(PH)
      TERM2 = 1-((RATIO**2)*(SIN(PH))**2)
      IF (TERM1 .LT. ZERO .OR. TERM2 .LT. ZERO) GOTO 190
      SPIST = (STROKE/2)*(TERM1+1/RATIO*(1-SQRT(TERM2)))
C---- CYLINDER VOLUME
      VOLUME= (Vc+PI*(BORE**2)/4*SPIST)
C**********************************************************************C
C     III.2: INSTANTANEOUS CYLINDER SURFACE                            C
C**********************************************************************C
      APIST=PI/4*(BORE**2)
      HEIGHT=Vc/APIST
      SURFAC=2*APIST + PI*BORE*(SPIST+HEIGHT)
C---- SURFACE WITHOUT THE PISTON SURFACE
C      SURFAC=PI*D*(SPIST+HEIGHT)
C     OVOL CALCULATION ONLY WHEN USING HOHENBERG OR WOSCHNI
      IF(EPSILON(TIME).LT.TIME) THEN
        IF(NWALL.EQ.2.OR.NWALL.EQ.3) THEN
          OVOL=SURFAC/VOLUME
        ENDIF
      ENDIF
C     OTHERWISE THE OVOL OUT OF THE INPUT FILE IS TAKEN
C**********************************************************************C
C     III.3: CALC. WOSCHNI PARAMETERS FOR ENGINE COMBUSTION SIMULATION C
C            (see different books of Bargende, Spicher, Heywood)       C
C**********************************************************************C
      !IF(TIME.LT.SMALL) VPIST=2*STROKE*espeed/60
      VPIST=2*STROKE*espeed/60
C_ORINIAL_WOSCHNI	IF(TIME.EQ.ZERO .AND. CONST.EQ.-1D0) CONST=3.26*(D**(-0.2))
      !IF(TIME.LT.SMALL) CONST=130.0*(BORE**(-0.2))
      CONST=130.0*(BORE**(-0.2))
C**********************************************************************C
C     CHAPTER IV: OUTPUT TO 'fort.37'                                  C
C**********************************************************************C
      IF(TIME.LT.SMALL) THEN
        OPEN(UNIT=NFIL37,FILE='fort.37')
        WRITE(NFIL37,81) 'WALL HEAT TRANSFER CALCULATION'
        WRITE(NFIL37,82) 'VPIST  :',VPIST
        WRITE(NFIL37,82) 'CONST  :',CONST
        WRITE(NFIL37,83) '    S/V    :  function of time'
        WRITE(NFIL37,83) '<<<<                          '
      END IF
C**********************************************************************C
C     CHAPTER V: FORMAT STATEMENTS                                     C
C**********************************************************************C
   81 FORMAT(1X,A30)
   82 FORMAT(4X,A,1PE12.4)
   83 FORMAT(A30)
C**********************************************************************C
C     CHAPTER VI. REGULAR EXIT                                         C
C**********************************************************************C
      RETURN
C**********************************************************************C
C     CHAPTER VII. ERROR EXITS                                         C
C**********************************************************************C
  190 CONTINUE
      WRITE(NFILE6,*) '+++++++++++  ERROR in CALWAL !!!  +++++++++++'
      STOP
C**********************************************************************C
C     END OF -CALWAL-                                                  C
C**********************************************************************C
      END

