C>     simman
      SUBROUTINE SIMMAN(JOB,NC,NEQ,CMAT,LRW,RW,LIW,IW,
     1   SMF,SMFP,SMFSYM,NFILE6,RPAR,IPAR,LRV,RV)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      CHARACTER(LEN=*) SMFSYM(*)
      DIMENSION SMF(NEQ),SMFP(NEQ)
      DIMENSION RW(LRW),IW(LIW),RPAR(*),IPAR(*)
      DIMENSION RV(LRV)
      DIMENSION TABVAR(MNCOV)
C***********************************************************************
C     Fuel and oxidizer composition
C***********************************************************************
      DIMENSION CMAT(*)
      COMMON/BCHIS/EWF(MNCON,7),SMFF(MNEQU,7),PAT(2,MNTCO),
     1       MIXFRA,MENFRA,MPRFRA
      COMMON/BMIX/OMEGA,SMFMIX(MNEQU),SMFCHA(MNEQU),VDIR(MNEQU,NDIMM)
      COMMON/BTNU/M1,M2,M3,NPROP
      COMMON/BNUE/NUMEQ
C***********************************************************************
C     build up integer workspace
C***********************************************************************
      WRITE(6,*) ' JHDSFHDJKSFHJKSFKAKASDFH'
      NUMEQ  = NEQ
      IEQ    = IPAR(10)
      NS     = IPAR(3)
      NE     = IPAR(6)
      NMM     = NE + 2
C***********************************************************************
C     compute maximum number of vertices
C***********************************************************************
      NVM = 20
      M1 = 11
      IF(MIXFRA.EQ.0) M1 = 1
      m2 = 11
      m3 = 11
      nomax = 500
      nprop = 80
C***********************************************************************
C     build up integer workspace
C***********************************************************************
      IIH= 1
      IIW  = IIH + NVM
      LEFTiw = Liw + 1 - iiw
      if(Leftiw.lt.1) goto 910
C***********************************************************************
C     build up integer workspace
C***********************************************************************
      IXI     = 1
      IEV     = IXI + NE
      IDC     = IEV + NE * NS
      IXV     = IDC + NMM * NMM
      IX      = IXV + NS * NVM
      IY      = IX  + NVM
      IPSIM   = IY  + NVM
      IPHIG   = IPSIM + NEQ
      IPSIG   = IPHIG + NS * M2
      ITSTO   = IPSIG + NEQ * M1 * M2
      IASTO   = ITSTO + NOMAX
      IPSTO   = IASTO + NOMAX
      IRSTO   = IPSTO + NEQ * NOMAX
      IPROP   = IRSTO + NEQ  * NOMAX
      IGRAD   = IPROP + NPROP * M1 * M2 * M3
      ICOLMA  = IGRAD + NEQ * NEQ
      IROWMA  = ICOLMA + NEQ * NEQ
      IPROPI  = IROWMA + NEQ * NEQ
      IRW     = IPROPI + NPROP
      LEFTRW  = LRW + 1 - IRW
      if(leftrw.lt.1) goto 920
C***********************************************************************
C     call subroutine
C***********************************************************************
      CALL SIMMA1(JOB,IEQ,NMM,NC,NEQ,NE,NS,CMAT,TABVAR,
     1   SMF,SMFP,SMFSYM,NFILE6,NPROP,M1,M2,M3,NVM,NOMAX,
     1   IW(IIH),
     1   RW(IXI),RW(IEV),RW(IDC),RW(IXV),RW(IX),RW(IY),RW(IPSIM),
     1   RW(IPHIG),RW(IPSIG),RW(ITSTO),RW(IASTO),RW(IPSTO),
     1   RW(IRSTO),RW(IPROP),RW(IGRAD),RW(ICOLMA),RW(IROWMA),RW(IPROPI),
     1   LEFTRW,RW(IRW),LEFTIW,IW(IIW),RPAR,IPAR)
C***********************************************************************
C     call subroutine
C***********************************************************************
      RETURN
C***********************************************************************
C     error exits
C***********************************************************************
  910 continue
      write(nfile6,*) ' increase iw(liw) by at least ',-leftiw
      goto 999
  920 continue
      write(nfile6,*) ' increase rw(lrw) by at least ',-leftrw
      goto 999
  999 continue
      write(nfile6,*)  ' error in simman'
      END
      SUBROUTINE SIMMA1(JOB,IEQ,NMM,NC,NEQ,NE,NS,CMAT,TABVAR,
     1   SMF,SMFP,SMFSYM,NFILE6,NPROP,M1,M2,M3,NVM,NOMAX,
     1   IH,
     1   XI,EV,DC,XV,X,Y,PSIMIX,PHIG,PSIGEN,
     1   TSTO,ARCSTO,PSISTO,RMSSTO,PROP,GRAD,COLMA,ROWMA,PROPI,
     1   LRW,RW,LIW,IW,RPAR,IPAR)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
C***********************************************************************
C     arrays
C***********************************************************************
      DIMENSION CMAT(NC,NEQ),TABVAR(NC),IPAR(*),RPAR(*)
      DIMENSION RW(LRW),IW(LIW)
      DIMENSION SMF(NEQ),SMFP(NEQ)
      CHARACTER(LEN=*) SMFSYM(NEQ)
C***********************************************************************
C     manifold arrays
C***********************************************************************
      DIMENSION XI(ne),EV(ne,ns),DC(NMM,NMM)
      COMMON/BSSPV/SSPV(20*MNSPE)
      DIMENSION X(NVM),y(NVM),ih(NVM),XV(ns*NVM)
      DIMENSION psimix(NEQ),phig(NS,M2), psigen(neq,m1,m2)
      DIMENSION TSTO(nomax),ARCSTO(nomax),
     1          PSISTO(NEQ,nomax),RMSSTO(NEQ,nomax)
      DIMENSION prop(nprop,m1,m2,m3)
      DIMENSION GRAD(NEQ,NEQ),COLMA(NEQ,NEQ),ROWMA(NEQ,NEQ),PROPI(NPROP)
      DIMENSION C(MNTCO),CI(MNTCO),CIT(MNTCO),CM(MNTCO)
cddddddddddddddddddddddd
C     dimension grah(3,15),PROM(3,15)
C***********************************************************************
C     Fuel and oxidizer composition
C***********************************************************************
      COMMON/BCHIS/EWF(MNCON,7),SMFF(MNEQU,7),PAT(2,MNTCO),
     1       MIXFRA,MENFRA,MPRFRA
      COMMON/BMIX/OMEGA,SMFMIX(MNEQU),SMFCHA(MNEQU),VDIR(MNEQU,NDIMM)
      DOUBLE PRECISION, DIMENSION(1) :: DXDS,DTDS,DPDS
C***********************************************************************
C     USEFUL CONSTANTS
C***********************************************************************
      DATA ZERO/0.0D0/,ONE/1.0D0/
C***********************************************************************
C     V.I: FIRST DERIVATIVES
C***********************************************************************
C---- DFDX GIVES THE DERIVATIVE OF THE FORM DF/DX FOREWARD OR BACKWARD
      DFDX(F1,F2,DX) =  (F2-F1)/DX
C---- DFDXCE GIVES THE CENTRAL DIFFERENCE APPROXIMATION
C     DFDXCE(FL,F0,FR,DXL,DXR)= 0.5D0* (DFDX(F0,FR,DXR)+DFDX(FL,F0,DXL))
C---- DFDXPA GIVES THE DERIVATIVE OF THE FORM DF/DX (PARABOLIC)
C     DFDXPA(FL,F0,FR,DXL,DXR)=
C    1    ( DXR*DFDX(FL,F0,DXL) + DXL*DFDX(F0,FR,DXR) )  /  (DXL+DXR)
C***********************************************************************
C     FIRST EXECUTABLE STATEMENT FOLLOWS
C***********************************************************************
C***********************************************************************
C     SET INITIAL VALUES FOR CONTROLING VARIABLES
C***********************************************************************
       OMEGA = 0.0D0
C***********************************************************************
C
C     COMPUTE manifold
C
C***********************************************************************
      IF(JOB.EQ.0) THEN
C***********************************************************************
C
C     COMPUTE Generator manifold
C
C***********************************************************************
      ndmg = 1
      CALL MANGEN(NDMG,M1,M2,NS,NE,NMM,NEQ,NC,CMAT,TABVAR,
     1     ev,dc,xi,psimix,xv,NVM,x,y,ih,PHIG,PSIGEN,
     1     LRW,RW,LIW,IW,NFILE6,RPAR,IPAR)
C***********************************************************************
C
C     Loop over mixture fraction
C
C***********************************************************************
      DO 190 I1=1,M1
      write(6,*) ' I1 =',i1,' starts'
C***********************************************************************
C
C     Compute equilibrium value
C
C***********************************************************************
C***********************************************************************
C     check for the initial condition with the highest temperature
C***********************************************************************
      TMAX = -1.D20
      DO 110 I2=1,M2
      CALL TRASSP(IEQ,NEQ,PSIGEN(1,I1,I2),NS,T,CDUM,SMFP,RHO,H,P,
     1     SSPV,IERR,0,DXDS,1,DTDS,DPDS)
      write(6,*) I2,' T = ',T
      IF(T.GT.TMAX) THEN
        TMAX = T
        ITM  = I2
      ENDIF
  110 CONTINUE
C***********************************************************************
C     compute trajectory for these initial conditions
C***********************************************************************
      M3DUM = 2
      OMEGA = 0.0D0
C---- use the corresponding properties array as work array
      TEND = 1.D10
      IPR = 0
      CALL RXNPRO(1,M3DUM,NEQ,TEND,PSIGEN(1,i1,ITM),SMFMIX,SMFP,
     1   RPAR,IPAR,LIW,IW,LRW,RW,
     1  NMM,NOMAX,NOUT,TSTO,ARCSTO,PSISTO,RMSSTO,
     1  NPROP,M1,M2,PROP(1,i1,i2,1),IPR,NFILE6)
      write(6,*) (smfmix(i),i=3,ne+2)
C***********************************************************************
C     set omega to the desired value
C***********************************************************************
      OMEGA = 1.0D-7
C***********************************************************************
C
C     COMPUTE Trajectory
C
C***********************************************************************
      if(m1.gt.1) then
        chi =  float(i1-1)/float(m1-1)
      else
        chi = 1.0d0
      endif
      write(6,*) ' neq',neq
      DO 180 I2=1,M2
      write(6,*) ' I2 =',i2,' starts'
      TEND = 1.D10
      IPR = 1
      CALL RXNPRO(3,M3,NEQ,TEND,PSIGEN(1,i1,i2),SMF,SMFP,
     1      RPAR,IPAR,LIW,IW,LRW,RW,
     1  NMM,NOMAX,NOUT,TSTO,ARCSTO,PSISTO,RMSSTO,
     1  NPROP,M1,M2,PROP(1,i1,i2,1),IPR,NFILE6)
  180 CONTINUE
  190 CONTINUE
C***********************************************************************
C     Write properties
C***********************************************************************
      write(57,*) ((((PROP(i,j,k,l),i=1,nprop),j=1,m1),k=1,m2),l=1,m3)
C***********************************************************************
C     read properties
C***********************************************************************
      ELSE
      read(57,*) ((((PROP(i,j,k,l),i=1,nprop),j=1,m1),k=1,m2),l=1,m3)
      ENDIF
C***********************************************************************
C     Compute local basis
C***********************************************************************
      D1 = 1.0D0 / float(m1-1)
      D2 = 1.0D0 / float(m2-1)
      D3 = 1.0D0 / float(m3-1)
      do 290 I1 = 1,M1
      do 280 I2 = 1,M2
      do 270 I3 = 1,M3
C***********************************************************************
C     Compute gradients and store in vectors
C***********************************************************************
      do 210 k = 1,neq
C---- gradient in direction 1
      if(m1.ne.1) then
      if(i1.eq.1) then
      GRA1 = DFDX(PROP(k,i1,i2,i3),PROP(k,i1+1,i2,i3),D1)
      else if(i1.eq.m1) then
      GRA1 = DFDX(PROP(k,i1-1,i2,i3),PROP(k,i1,i2,i3),D1)
      else
      GRA1 = DERMON(
     1   PROP(k,i1-1,i2,i3),PROP(k,i1,i2,i3),PROP(k,i1+1,i2,i3),D1,D1)
      endif
      endif
C---- gradient in direction 2
      if(i2.eq.1) then
      GRA2 = DERMON(
     1   PROP(k,i1,m2-1,i3),PROP(k,i1,1,i3),PROP(k,i1,2,i3),D2,D2)
c     GRA2 = DFDX(PROP(k,i1,i2,i3),PROP(k,i1,i2+1,i3),D2)
      else if(i2.eq.m2) then
      GRA2 = DERMON(
     1   PROP(k,i1,m2-1,i3),PROP(k,i1,1,i3),PROP(k,i1,2,i3),D2,D2)
c     GRA2 = DFDX(PROP(k,i1,i2-1,i3),PROP(k,i1,i2,i3),D2)
      else
      GRA2 = DERMON(
     1   PROP(k,i1,i2-1,i3),PROP(k,i1,i2,i3),PROP(k,i1,i2+1,i3),D2,D2)
      endif
C---- gradient 3
      if(i3.eq.1) then
      GRA3 = DFDX(PROP(k,i1,i2,i3),PROP(k,i1,i2,i3+1),D3)
      else if(i3.eq.m3) then
      GRA3 = DFDX(PROP(k,i1,i2,i3-1),PROP(k,i1,i2,i3),D3)
      else
      GRA3 = DERMON(
     1   PROP(k,i1,i2,i3-1),PROP(k,i1,i2,i3),PROP(k,i1,i2,i3+1),D3,D3)
      endif
      if(m1.ne.1) then
      GRAD(k,1) = GRA3
      GRAD(k,2) = GRA2
      GRAD(k,3) = GRA1
      else
      GRAD(k,1) = GRA3
      GRAD(k,2) = GRA2
      endif
  210 continue
C***********************************************************************
C     compute projection matrix (not in use and not o.k.)
C***********************************************************************
C***********************************************************************
C     compute orthogonal basis
C***********************************************************************
                  NDB = 3
      IF(M1.eq.1) NDB = 2
      CALL ORBAS(NEQ,NDB,NEQ,GRAD,NEQ,COLMA,LRW,RW,LIW,IW,2,IERR)
      IF(IERR.GT.0) THEN
      WRITE(6,*) 'IERR=',IERR
      STOP
      ENDIF
      NN = NEQ * NEQ
      CALL DCOPY(NN,COLMA,1,ROWMA,1)
c---- the following lines can be used instead of the call to dgeinv
      CALL DGEINV(NEQ,ROWMA,RW,IW,IERR)
      IF(ierr.gt.0) then
        write(6,*) ' error in matrix inversion, ierr =',ierr
        stop
      endif
C***********************************************************************
C     compute orthogonal basis
C***********************************************************************
c---- compute rate in the new basis
      CALL DCOPY(NEQ,PROP(NEQ+1,i1,i2,i3),1,SMFP,1)
      DTDT = prop(neq+neq+2,i1,i2,i3)
      CALL DGEMV('N',NEQ,NEQ,one,ROWMA,NEQ,smfp,1,ZERO,SMF,1)
C     write(6,*) ' transformed rates:',i1,i2,i3,(smf(i),i=1,neq)
C---- grad will not be touched in the subroutine
c     store gradients in prop
c     scf =           det
c     call dscal(neq,scf,rowma(1,1),neq)
c     call dscal(neq,scf,rowma(2,1),neq)
c     call dscal(neq,scf,rowma(3,1),neq)
      call dcopy(neq,rowma(1,1),neq,prop(neq+neq+2+2*neq+1,i1,i2,i3),1)
      call dcopy(neq,rowma(2,1),neq,prop(neq+neq+2+  neq+1,i1,i2,i3),1)
      if(ndb.eq.3) then
      call dcopy(neq,rowma(3,1),neq,prop(neq+neq+2      +1,i1,i2,i3),1)
      else
      do 6161 i=1,neq
      prop(neq+neq+2+i,i1,i2,i3) = 0.0d0
 6161 continue
      endif
C***********************************************************************
C
C***********************************************************************
C---- do not forget to dimension prom(ndim,neq) and grah(ndim,neq)
C***********************************************************************
C
C***********************************************************************
  270 CONTINUE
  280 CONTINUE
  290 CONTINUE
      rewind 57
      write(57,*) ((((PROP(i,j,k,l),i=1,nprop),j=1,m1),k=1,m2),l=1,m3)
C***********************************************************************
C
C     Output for post-processing
C
C***********************************************************************
      write(47,*) 'TITLE= "  SIMPLE "'
      if(mixfra.eq.1) then
      write(47,*) 'VARIABLES= CHI, I2, I3, rat1, rat2, rat3,',
     1   (smfsym(i),',',i=1,neq),' retval, T, rho'
      else
      write(47,*) 'VARIABLES=  I2, I3, rat2, rat3,',
     1     (smfsym(i),',',i=1,neq),' retval,T, rho'
      endif
      DO 690 I1=1,M1
C***********************************************************************
C     tecplot output regular grid
C***********************************************************************
      if(1.gt.2) then
      write(6,*) ' neq',neq
      write(47,*) 'ZONE I=',m2,'  J=',m3,' F=BLOCK'
      write(6,*) ' m1,m2,m3 ', m1,m2,m3
      do 428 j=1,m3
      write(47,*) (chi,i=1,m2)
  428 continue
      do 429 j=1,m3
      write(47,*) (float(i-1)/float(m2-1),i=1,m2)
  429 continue
      do 427 j=1,m3
      write(47,*) (float(j-1)/float(m3-1),i=1,m2)
  427 continue
      write(6,*) ' neq',neq
csho  do 430 k=1,neq+NMM+2
      do 430 k=1,neq+NMM+2
      write(6,*) 'hsdfkjhdkhkj'
      do 430 j=1,m3
      write(47,*) (prop(k,i1,i,j),i=1,m2)
  430 continue
C***********************************************************************
C     tecplot output regular grid
C***********************************************************************
      else
      write(47,*) 'ZONE I=',m2*m3,'  J=',(m2-1)*(m3-1),' F=FEPOINT'
      write(6,*) ' m1,m2,m3 ', m1,m2,m3
      do 533 j=1,m3
      do 533 i=1,m2
      if(mixfra.eq.1) then
      write(47,*) chi,float(i-1)/float(m2-1),float(j-1)/float(m3-1),
     1         zero,zero,prop(2*neq+2,i1,i,j),(prop(k,i1,i,j),k=1,neq),
     1         zero,zero,zero
      else
      write(47,*) float(i-1)/float(m2-1),float(j-1)/float(m3-1),
     1         zero,prop(2*neq+2,i1,i,j),(prop(k,i1,i,j),k=1,neq),
     1         zero,zero,zero
      endif
  533 continue
      do 534 j=1,m3-1
      do 534 i=1,m2-1
      iv1 = i + (j-1) * m2
      iv2 = iv1 + 1
      iv3 = iv1 + m2
      iv4 = iv3 + 1
      write(47,*)  iv1,iv2,iv4,iv3
  534 continue
      endif
  690 continue
C***********************************************************************
C
C     integrate rate equations
C
C***********************************************************************
      cm(1)=0.0
      cm(2)=0.0
      cm(3)=0.0
333   write(*,*) ' Enter omega (omega < 0 to end)'
      read(*,*) omega
      if(omega.lt.0.0d0) goto 6767
      write(*,*) ' Enter c(1),c(2),c(3) '
      read(*,*) c(1),c(2),c(3)
      write(*,*) ' Enter cm(1),cm(2),cm(3) '
      read(*,*) cm(1),cm(2),cm(3)
      CALL SIMIPO(3,M1,M2,M3,C,NPROP,PROP,PROPI)
      CALL SIMIPO(3,M1,M2,M3,CM,NPROP,PROP,SMFMIX)
C---- first neq properties are the state space, prop will not be touched
      TEND = 1.0d-2
      IPR = 1
      CALL RXNPRO(2,M3,NEQ,TEND,PROPI,SMF,SMFP,
     1      RPAR,IPAR,LIW,IW,LRW,RW,
     1  NMM,NOMAX,NOUT,TSTO,ARCSTO,PSISTO,RMSSTO,
     1  NPROP,M1,M2,PROP,IPR,NFILE6)
      write(47,*) ' ZONE T=" Det. o =',omega,
     1      ' ", I=',NOUT,' , J= 1, F= POINT'
      do 136 l=1,nout
      if(mixfra.eq.1) then
      write(47,*) tsto(l),zero,zero,zero,zero,zero,
     1         (Psisto(i,l),i=1,neq),zero,zero,zero
      else
      write(47,*) tsto(l),zero,zero,zero,
     1         (Psisto(i,l),i=1,neq),zero,zero,zero
      endif
 136  continue
      CALL RXNSIM(2,3,M3,NEQ,TEND,C,CI,CIT,LIW,IW,LRW,RW,
     1   IPAR,PROP,NOMAX,NOUT,TSTO,NEQ,PSISTO,RMSSTO,IPR,NFILE6)
      write(47,*) ' ZONE T=" Red. o =',omega,
     1      ' ", I=',NOUT,' , J= 1, F= POINT'
      do 336 l=1,nout
      write(6,*) psisto(1,l),psisto(2,l),psisto(3,l)
      CI(1) = psisto(1,l)
      CI(3) = 1.0d0 - dabs(1.0d0 - psisto(3,l))
      if(ci(3).gt.1.0d0) then
        y2h = - psisto(2,l)
        else
        y2h = psisto(2,l)
      endif
      imul = idint(y2h)
      if(y2h .lt.0.0d0) imul = imul -1
      ci(2) = y2h - float(imul)
      CALL SIMIPO(3,M1,M2,M3,CI,NPROP,PROP,PROPI)
      if(mixfra.eq.1) then
      write(47,*) tsto(l),zero,zero,ci(1),ci(2),ci(3),
     1         (propi(i),i=1,neq),zero,zero,zero
      else
      write(47,*) tsto(l),zero,ci(2),ci(3),
     1         (propi(i),i=1,neq),zero,zero,zero
      endif
      write(6,*) tsto(l),(propi(i),i=3,NMM+2)
 336  continue
      goto 333
 6767 continue
C***********************************************************************
C     Compute Dimension of the Table and number of properties
C***********************************************************************
      NDIM = NC - NE - 2 + MIXFRA + MENFRA + MPRFRA
C                       NPROP = NDIM+ 2 *NEQ+LRV
C**********************************************************************
C     Regular exit
C**********************************************************************
      STOP
      END
      subroutine vert( ev,nsf, chi, ne, NMM, ns, nv, xv, nvd )
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
c
c  routine to find the vertices of the polytope in composition space
c  corresponding to the allowed region.
c
c  input:
c       ev(nsf,ns) - element vectors
c       chi(ne)   - element specific mole numbers
c       ne        - number of elements: must be 4
c       ns        - number of species
c       NMM       - number of major species
c       nsf       - first dimension of ev
c       nvd       - second dimension of xv
c
c  returned:
c       nv        - number of vertices
c       xv(ns,nv) - coordinates of vertices (in arbitrary order)
c
c  set  iop=1  to turn on diagnostic output.
c
      parameter ( nsd = 80 )
      dimension a(nsd,nsd), b(nsd), ipvt(nsd)
      dimension ev(nsf,ns), chi(ne), xv(ns,nvd)
c
      data thresh/ 1.d-8 /
      data iop / 1 /
c
      if( ne .ne. 4 ) then
         write(0,*)'vert: coded for ne=4, not ne=',ne
         stop
      endif
c
      if( ns .gt. nsd ) then
         write(0,*)'vert: increase nsd to ns=',ns
         stop
      endif
c
c  initialize
c
      nv     = 0
      apos   = 1.e20
      aneg   = -apos
      azmax  = 0.
      dismin = 1.e20
      dismax = 0.
c     
c  loop over all possible sets of 2 non-zero species
c
      do 100 i1 =    1, NMM
         do 100 i2 = i1+1, NMM
c     
c  set first ne rows to element vectors
c
            do 130 i = 1, ne
               do 130 j = 1, NMM
                  a(i,j) = ev(i,j)
 130           continue
c
c  set remaining rows to species vectors of i1 and i2
c
               do 110 j = 1, NMM
                  a(ne+1,j) = 0.
                  a(ne+2,j) = 0.
 110           continue
               a(ne+1,i1) = 1.
               a(ne+2,i2) = 1.
c     
c     set right hand side
c
               do 120 i = 1, ne
                  b(i) = chi(i)
 120           continue
               b(ne+1) = 0.0d0
               b(ne+2) = 0.0d0
               if(iop.gt.1) then
                  write(6,*) ' pair',i1,i2
                  do 453 i=1,NMM
                     write(6,877) (a(i,j),j=1,NMM),b(i)
 453              continue
 877              format(' ',6(f3.0,1x),'   ',1pe9.2)
               endif
c
c  solve linear system
c
               CALL DGEFA(A,NSD,NMM,IPVT,IER)
c
               if( ier  .gt. 0 ) then
                  if( iop .gt. 1 ) write(6,607)i1,i2,ier
 607   format(1x,'matrix is singular for comb.',2i4,' ier=',i3)
                  go to 100
               endif
c
               CALL DGESL(A,NSD,NMM,IPVT,B,0)
c     
c  examine solution
c
               pmin = 1.e20
               zmax = 0.
c     
               do 140 i = 1, NMM
                  pmin = dmin1( pmin, b(i) )
 140           continue
c
c
c  negative coordinate
c
               if( pmin .lt. -thresh ) then
                  if( iop .ne. 0 )
     1                 write(8,608)i1,i2,(b(i),i=1,NMM)
 608   format(1x,'vertex from',2i3,' has a negative value ',/,
     1                 ' b =',6(1pe9.2,1x))
       go to 100
      endif
c
      apos = dmin1( apos, pmin )
c
c  find minimum distance to other vertices
c
      dist = 1.e20
      do 200 iv = 1, nv
c
         dis = 0.
         do 210 i = 1, NMM
 210        dis = dis + abs( b(i) - xv(i,iv) )
c
 200        dist = dmin1( dist, dis )
c
c  treat coincident vertex
c
            if( dist .lt. thresh ) then
               dismax = dmax1( dismax, dist )
               if( iop .ne. 0 )
     1              write(9,609)i1,i2,dist,pmin,(b(i),i=1,NMM)
 609           format(1x,'vert coincident vertex',2i4,6(1pe13.3))
               go to 100
            endif
c
c  add vertex
c
            dismin = dmin1( dismin, dist )
c
            nv = nv + 1
c
            if( nv .gt. nvd ) then
               write(0,*)'vert: increase nvd to at least nv=,nv'
               stop
            endif
c     
            do 220 i = 1, ns
 220           xv(i,nv) = 0.0d0
               do 225 i = 1, NMM
 225              xv(i,nv) = b(i)
c
 100  continue
c
      if( iop .ne. 0 ) then
         write(6,*)' diagnostic output from vert'
         write(6,*)' number of vertices=',nv
         write(6,*)' smallest coordinate deemed positive=',apos
         write(6,*)' largest coordinate deemed negative=',aneg
         write(6,*)' largest error in specified zero=',azmax
         write(6,*)' largest separation deemed coincident=',dismax
         write(6,*)' smallest separation deemed distinct=',dismin
      endif
c
      return
      end
C DECOMP 11-3-82
C
C FROM COMPUTER METHODS FOR MATHEMATICAL COMPUTATIONS
C BY FORSYTHE, MALCOLM, AND MOLER
C PRENTICE HALL, 1977, P.52-54.
C
C DECOMPOSES A REAL MATRIX BY GAUSSIAN ELIMINATION
C AND ESTIMATES THE CONDITION OF THE MATRIX.
C
C USE SOLVE TO COMPUTE SOLUTIONS TO LINEAR SYSTEMS.
C
C
      SUBROUTINE DECOMP(NDIM,N,A,COND,IPVT,WORK)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C
      INTEGER NDIM, N
      DOUBLE PRECISION A(NDIM,N), COND, WORK(N)
      INTEGER IPVT(N)
C
C
C INPUT..
C
C  NDIM = DECLARED ROW DIMENSION OF THE ARRAY CONTAINING A.
C  N = ORDER OF THE MATRIX.
C  A = MATRIX TO TRIANGULARIZED.
C
C
C OUTPUT..
C
C A CONTAINS AN UPPER TRIANGULAR MATRIX  U  AND A PERMUTED
C   VERSION OF A LOWER TRIANGULAR MATRIX  I-L  SO THAT
C   (PERMUTATION MATRIX)*A = L*U
C
C COND = AN ESTIMATE OF THE CONDITION OF A .
C   FOR THE LINEAR SYSTEM  A * X = B, CHANGES IN  A  AND  B
C   MAY CAUSE CHANGES  COND  TIMES AS LARGE IN  X .
C   IF  COND+1 = COND ,  A  IS SINGULAR TO WORKING PRECISION.
C   COND  IS SET TO  1.0E+32  IF EXACT SINGULARITY IS DETECTED.
C
C IPVT = THE PIVOT VECTOR.
C   IPVT(K) = THE INDEX OF THE K-TH PIVOT ROW
C   IPVT(N) = (-1)**(NUMBER OF INTERCHANGES)
C
C WORK SPACE..  THE VECTOR   WORK  MUST BE DECLARED AND INCLUDED
C   IN THE CALL.  ITS INPUT CONTENTS ARE IGNORED. ITS OUTPUT
C   CONTENTS ARE USUALLY UNIMPORTANT.
C
C THE DETERMINANT OF  A  CAN BE OBTAINED ON OUTPUT BY
C   DET(A) = IPVT(N) * A(1,1) * A(2,2) * ... * A(N,N).
C
C
      DOUBLE PRECISION EK, T, ANORM, YNORM, ZNORM
      INTEGER NM1, I, J, K, KP1, KB, KM1, M
C
      IPVT(N) = 1
      IF( N .EQ. 1) GO TO 80
      NM1 = N - 1
C
C     COMPUTE 1-NORM OF A
C
      ANORM = 0.0
      DO 10 J = 1, N
        T = 0.0
        DO 5 I = 1, N
          T = T + ABS(A(I,J))
 5      CONTINUE
        IF( T .GT. ANORM ) ANORM = T
10    CONTINUE
C
C     GAUSSIAN ELIMINATION WITH PARTIAL PIVOTING
C
      DO 35 K = 1, NM1
        KP1 = K + 1
C
C     FIND PIVOT
C
        M = K
        DO 15 I = KP1, N
          IF( ABS(A(I,K)) .GT. ABS(A(M,K)) ) M = I
15      CONTINUE
        IPVT(K) = M
        IF( M .NE. K) IPVT(N) = -IPVT(N)
        T = A(M,K)
        A(M,K) = A(K,K)
        A(K,K) = T
C
C     SKIP STEP IF PIVOT IS ZERO
C
      IF( T .EQ. 0.0 ) GO  TO 35
C
C     COMPUTE MULTIPLIERS
C
        DO 20 I = KP1, N
          A(I,K) = -A(I,K)/T
20      CONTINUE
C
C     INTERCHANGE AND ELIMINATE BY COLUMNS
C
        DO 30 J = KP1, N
          T = A(M,J)
          A(M,J) = A(K,J)
          A(K,J) = T
          IF( T .EQ. 0.0 ) GO TO 30
          DO 25 I = KP1, N
            A(I,J) = A(I,J) + A(I,K)*T
25        CONTINUE
30      CONTINUE
35    CONTINUE
C
C COND = (1-NORM OF A)*(AN ESTIMATE OF 1-NORM OF A-INVERSE)
C ESTIMATE OBTAINED BY ONE STEP OF INVERSE ITERATION FOR THE
C SMALL SINGULAR VECTOR. THIS INVOLVES SOLVING TWO SYSTEMS
C OF EQUATIONS, (A-TRANSPOSE)*Y = E AND A*Z = Y WHERE  E
C IS A VECTOR OF +1 OR -1 CHOSEN TO CAUSE GROWTH ON  Y .
C ESTIMATE = (1-NORM OF Z)/(1-NORM OF Y)
C
C     SOLVE (A-TRANSPOSE)*Y = E
C
      DO 50 K = 1, N
        T = 0.0
        IF( K .EQ. 1 ) GO TO 45
        KM1 = K - 1
        DO 40 I = 1, KM1
          T = T + A(I,K)*WORK(I)
40      CONTINUE
45      EK = 1.0
        IF( T .LT. 0.0 ) EK = -1.0
        IF( A(K,K) .EQ. 0.0 ) GO TO 90
        WORK(K) = -(EK + T)/A(K,K)
50    CONTINUE
      DO 60 KB = 1, NM1
        K = N - KB
        T = 0.0
        KP1 = K + 1
        DO 55 I = KP1, N
          T = T + A(I,K)*WORK(K)
55      CONTINUE
        WORK(K) = T
        M = IPVT(K)
        IF( M .EQ. K ) GO TO 60
        T = WORK(M)
        WORK(M) = WORK(K)
        WORK(K) = T
60    CONTINUE
C
      YNORM = 0.0
      DO 65 I = 1, N
        YNORM = YNORM + ABS(WORK(I))
65    CONTINUE
C
C     SOLVE A*Z = Y
C
      CALL popsol(NDIM,N,A,WORK,IPVT)
C
      ZNORM = 0.0
      DO 70 I = 1, N
        ZNORM = ZNORM + ABS(WORK(I))
70    CONTINUE
C
C     ESTIMATE CONDITION
C
      COND = ANORM*ZNORM/YNORM
      IF( COND .LT. 1.0 ) COND = 1.0
      RETURN
C
C     1-BY-1
C
80    COND = 1.0
      IF( A(1,1) .NE. 0.0 ) RETURN
C
C     EXACT SINGULARITY
C
90    COND = 1.0E+32
      RETURN
      END
C SOLVE  11-3-82
C
C FROM COMPUTER METHODS FOR MATHEMATICAL COMPUTATIONS
C BY FORSYTHE, MALCOLM, AND MOLER
C PRENTICE HALL, 1977, P.55.
C
C
C SOLUTION OF LINEAR SYSTEM,  A * X = B
C
C DO NOT USE IF DECOMP HAS DETECTED A SINGULARITY
C
C
      SUBROUTINE popsol(NDIM,N,A,B,IPVT)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C
      INTEGER NDIM, N, IPVT(N)
      DOUBLE PRECISION A(NDIM,N), B(N)
C
C
C INPUT..
C  NDIM = DECLARED ROW DIMENSION OF ARRAY CONTAINING A .
C  N = ORDER OF MATRIX.
C  A = TRIAGULARIZED MATRIX OBTAINED FROM DECOMP .
C  B = RIGHT HAND SIDE VECTOR.
C  IPVT = PIVOT VECTOR OBTAINED FROM DECOMP.
C
C OUTPUT..
C  B = SOLUTION VECTOR,  X .
C
C
      INTEGER KB, KM1, NM1, KP1, I, K, M
      DOUBLE PRECISION T
C
C     FORWARD ELIMINATION
C
      IF( N .EQ. 1 ) GO TO 50
      NM1 = N - 1
      DO 20 K = 1, NM1
        KP1 = K + 1
        M = IPVT(K)
        T = B(M)
        B(M) = B(K)
        B(K) = T
        DO 10 I = KP1, N
          B(I) = B(I) + A(I,K)*T
10      CONTINUE
20    CONTINUE
C
C     BACK SUBSTITUTION
C
      DO 40 KB = 1, NM1
        KM1 = N - KB
        K = KM1 + 1
        B(K) = B(K)/A(K,K)
        T = -B(K)
        DO 30 I = 1, KM1
          B(I) = B(I) + A(I,K)*T
30      CONTINUE
40    CONTINUE
50    B(1) = B(1)/A(1,1)
      RETURN
      END
      subroutine orthgs( v, nd, nv )
      implicit DOUBLE PRECISION(a-h,o-z)
c
c  replace the nv vectors (of dimension nd) by the orthonormal
c  set obtained by the gram-schmidt process.
c
c  input:
c       v(nd,nv)  - vectors (must be independent)
c       nd        - dimension of vectors
c       nv        - number of vectors ( .le. nd)
c
c  returned:
c       v(nd,nv)  - orthonormal vectors
c
      dimension v(nd,nv)
c
c  check input
c
      if( nd .lt. 1 ) then
         write(0,*)'ortho called with nd .lt. 1.  nd=',nd
         stop
      endif
c     
      if( nv .lt. 1 ) then
         write(0,*)'ortho called with nv .lt. 1.  nv=',nv
         stop
      elseif( nv .gt. nd ) then
         write(0,*)'ortho called with nv .gt. nd. nv,nd=',nv,nd
         stop
      endif
c
c  loop backward over vectors
c
      do 100 iv = nv, 2, -1
c
c  normalize
c
         call unitv( v(1,iv), nd )
c
c  make all other vectors orthogonal
c
         do 100 i = 1, iv - 1
 100       call ortho2( v(1,i), v(1,iv), nd )
c
c  normalize last vector
c
           call unitv( v(1,1), nd )
c
           return
           end
      subroutine unitv( x, n )
      implicit DOUBLE PRECISION(a-h,o-z)
c
c  normalize the n-vector x to unit length
c
      dimension x(n)
c
c  check input
c
      if( n .lt. 1 ) then
         write(0,*)'unitv called with n < 1.  n=',n
         stop
      endif
c
c  find length
c
      dot = 0.
c
      do 100 i = 1, n
 100     dot = dot + x(i)**2
c
         if( dot .le. 0. ) then
            write(0,*)'unitv called with zero vector'
            stop
         endif
c
         scale = 1./sqrt( dot )
c
         do 200 i = 1, n
 200        x(i) = x(i) * scale
c
            return
            end
      subroutine ortho2( x, u, n )
      implicit DOUBLE PRECISION(a-h,o-z)
c
c  routine to replace the n-vector x with its component that is
c  orthogonal to the unit n-vector u.
c  it is not checked that u has unit length.
c
      dimension x(n), u(n)
c
c  check input
c
      if( n .lt. 1 ) then
         write(0,*)'ortho2 called with n < 1.  n=',n
         stop
      endif
c
c  form dot product
c
      dot = 0.
c
      do 100 i = 1, n
 100     dot = dot + x(i)*u(i)
c     
         a = -dot
c
         do 200 i = 1, n
 200        x(i) = x(i) + a * u(i)
c
            return
            end
      subroutine hull( x, y, n, nh, ih )
      implicit DOUBLE PRECISION(a-h,o-z)
c
c  routine to find the convex hull of the set of points x(i), y(i), i=1,n.
c  the nh hull vertices are found and their indices returned in ih.
c
      dimension x(n), y(n), ih(n)
c
      data small / 1.d-5 /
c
      if( n .lt. 3 ) then
         write(0,*)'hull called with n=',n
c       write(0,*)'hull: stopped'
         nh = n
         do 11 i=1,n
            ih(i) = i
 11      continue
         return
      endif
c
c  find limits
c
      xmin =  1.e20
      xmax = -1.e20
      ymin =  1.e20
      ymax = -1.e20
c
      do 10 i = 1, n
         xmin = dmin1( xmin , x(i) )
         xmax = dmax1( xmax , x(i) )
         ymin = dmin1( ymin , y(i) )
 10      ymax = dmax1( ymax , y(i) )
c
         xsmall = small * ( xmax - xmin )
         ysmall = small * ( ymax - ymin )
         asmall = small * ( xmax - xmin ) * ( ymax - ymin )
c
         if( asmall .eq. 0. ) then
            write(0,*)'hull: asmall=0. stopped'
            stop
         endif
c
c  find lower left vertex
c
         il = 0
         xl = 1.e20
         yl = 1.e20
         do 20 i = 1, n
            if( abs( x(i) - xl ) .lt. xsmall ) then
               if( y(i) .lt. yl ) then
                  xl = x(i)
                  yl = y(i)
                  il = i
               endif
            elseif( x(i) .lt. xl ) then
               xl = x(i)
               yl = y(i)
               il = i
            endif
 20      continue
c     
         nh    = 1
         ih(1) = il
         ilast = il
c
c  start of loop
c
c  find starting guess
c
 99      do 100 i = 1, n
            if( abs( x(i) - x(nh) ) .gt. xsmall .or.
     1           abs( y(i) - y(nh) ) .gt. ysmall ) then
               il = i
               go to 101
            endif
 100     continue
c
         write(0,*)'hull: failed to find starting guess; stopped'
         stop
c
 101     continue
c
c  loop over all vertices to find least angle
c
         do 200 it = 1, n
           call xangle(x(ilast), y(ilast),  x(il), y(il),  x(it), y(it),
     1          asmall, lt )
c     
            if( lt .eq. 1 ) il = it
 200     continue
c
c  if vertex coincident with first - all done
c
         if( abs( x(il) - x(ih(1)) ) .lt. xsmall  .and.
     1        abs( y(il) - y(ih(1)) ) .lt. ysmall ) return
c
         nh = nh + 1
         ih(nh) = il
         ilast  = il
         go to 99
c     
         end
      subroutine xangle( x0, y0,  x1, y1,  x2, y2, asmall, l2 )
      implicit DOUBLE PRECISION(a-h,o-z)
c
c  routine to exdmine the angle 0-1-2.
c  if it is left-turning, l2 = 0
c  if it is right-turning, l2 = 1
c  if it is straight, l2 = 0 or 1 depending on whether 1 or 2 is closest to 0.
c
      dx1 = x1 - x0
      dx2 = x2 - x0
      dy1 = y1 - y0
      dy2 = y2 - y0
c
      a = dx1 * dy2 - dy1 * dx2
c
      if( abs( a ) .lt. asmall ) then
         l2 = 0
         if( dx1**2 + dy1**2 .lt.  dx2**2 + dy2**2 ) l2 = 1
      elseif( a .gt. 0. ) then
         l2 = 0
      else
         l2 = 1
      endif
c     
c**     write(19,900)x0,y0,x1,y1,x2,y2,a,asmall,l2
c**900  format(1p8e9.1,0pi6)
      return
      end
      subroutine ctrans( a, x, n, xt )
      implicit DOUBLE PRECISION(a-h,o-z)
c
c  routine to form transform of vector
c
c  input:
c       a  - direction cosines
c       x  - vector to be transformed
c       n  - vector length
c  output:
c       xt - transformed vector
c
      dimension a(n,n), x(n), xt(n)
c
      do 100 i = 1, n
         xt(i) = 0.
         do 100 j = 1, n
 100        xt(i) = xt(i) + a(j,i) * x(j)
c
      return
      end
      subroutine mixpt( phi, x, n, nh, ih, nhd )
      implicit DOUBLE PRECISION(a-h,o-z)
c
c  input:
c       phi     - point in n-space
c       x       - set of nh points in n-space
c       n       - dimension of space
c       nh      - number of points in x
c       ih      - pointer to points
c       nhd     - dimension for arrays
c
c  function:
c       phi is coincident with one of the points in x.  this point is found,
c       and the pointer array ih is cycled to make this point first
c
      dimension phi(n), x(n,nhd), ih(nhd)
c
      data thresh / 1.d-5 /
c
c  check dimensions  -  last two vectors in x used as work space
c
      if( nh+2 .gt. nhd ) then
         write(0,*)'mixpt: increase nhd to at least',nh+2
         stop
      endif
c
c  loop to find closest point and extremes
c
      nmax = nhd - 1
      nmin = nhd
c
      do 100 i = 1, n
         x(i,nmax) = -1.e20
 100    x(i,nmin) =  1.e20
c
        dismin = 1.e20
c
c  loop over points
c
        do 120 ip = 1, nh
           j = ih(ip)
c
           dist = 0.
           do 140 i = 1, n
              dist      = dist + abs( phi(i) - x(i,j) )
              x(i,nmax) = dmax1( x(i,nmax) , x(i,j) )
 140          x(i,nmin) = dmin1( x(i,nmin) , x(i,j) )
c     
              if( dist .lt. dismin ) then
                 near   = ip
                 dismin = dist
              endif
 120    continue
c
c  determine characteristic distance
c
        if(nh.eq.1) then
           xref = 0.
           do 164 i = 1, n
 164       xref = xref + x(i,1)**2 + phi(i)**2
           xref = 0.01 * dsqrt(xref)
        else
           xref = 0.
           do 160 i = 1, n
 160       xref = xref + x(i,nmax) - x(i,nmin)
         endif
c
c  check for closeness
c
         if( dismin .gt. thresh * xref ) then
            write(0,*)'mixpt: coincident point not found'
            write(0,*)'dismin=',dismin,'thresh=',thresh,'xref=',xref
            stop
         endif
c
c  cycle ih
c
         ishift = near - 1
         do 200 ip = 1, nh
            ito = ip - ishift
            if( ito .lt. 1 ) ito = ito + nh
 200     x(ito,nmax) = ih(ip)
c
         do 220 ip = 1, nh
220      ih(ip) = nint( x(ip,nmax) )
c
         return
         end
      subroutine gcurve( x, ldx ,n, nv, ip, npts, f, nf )
      implicit DOUBLE PRECISION(a-h,o-z)
c
c  in n-space, there is a closed curve which is made up of straight
c  line segments.  x contains the coordinates of the nv vertices.
c  ip points to the vertices in order, with the starting and ending
c  points being coincident (i.e. ip(1) = ip(nv) ).
c  there are npts points, equally spaced around the curve, with the first
c  and last points being coincident with the first vertex.
c  this subroutine returns in f the coordinates of these points.
c
c  input:
c       x(i,iv) - i-coordinate of vertex number iv
c       n       - dimension of space
c       nv      - number of vertices
c       ip      - pointer to vertices in order around the curve
c       npts    - number of equally-spaced points
c       nf      - first dimension of array f
c  output:
c       f       - coordinates of points
c
      parameter ( nseg = 100 )
      dimension darc(nseg), arc(nseg)
      dimension x(ldx,nv), ip(nv), f(nf,npts)
c
c  check input
c
      if( ip(1) .ne. ip(nv) ) then
         write(0,*)'gcurve: pointer does not give closed curve'
         stop
      endif
c
      if( nv .gt. nseg ) then
         write(0,*)'gcurve: increase nseg to at least',nv
         stop
      endif
c
c  darc(j) is the length of the line segment joining vertices (j-1) and j.
c  arc(j) is the arc length between vertex 1 and vertex j.
c
      arc(1)   = 0.
c
      do 100 j = 2, nv
         jj = ip(j)
         jm = ip(j-1)
c
         darc(j)  = 0.
         do 120 i = 1, n
 120     darc(j)  = darc(j) + ( x(i,jj) - x(i,jm) )**2
c
         if( darc(j) .eq. 0. ) then
            write(0,*)'gcurve: zero-length segment'
            if(nv.gt.2) then
               stop
            else
               write(0,*) ' only two points given --> degenerate case '
               j1 = ip(1)
               do 133 i = 1, n
                  do 133 m = 1, npts
 133           f(i,m) = x(i,j1)
               goto 595
            endif
         endif
c
         darc(j)  = sqrt( darc(j) )
         arc(j)   = arc(j-1) + darc(j)
 100  continue
c
c  determine point spacing ds
c
      ds = arc(nv) / float( npts - 1 )
c     
c  loop over points 2 to nv-1
c
      s = 0.
      j = 2
c
      do 200 m = 2, npts-1
         s = s + ds
c
c  find first vertex beyond point
c
 220     if( s .gt. arc(j) ) then
            j = j + 1
            if( j .le. nv ) go to 220
            write(0,*)'gcurve: run over end'
            stop
         endif
c
c  interpolate
c
         jj = ip(j)
         jm = ip(j-1)
         frjm = ( arc(j) - s ) / darc(j)
         frjj = 1. - frjm
c
         do 200 i = 1, n
 200     f(i,m) = frjm * x(i,jm) + frjj * x(i,jj)
c
c  treat first and last points
c
         j1 = ip(1)
         do 300 m = 1, npts, npts-1
            do 300 i = 1, n
 300     f(i,m) = x(i,j1)
c
c  set remaining components of f to zero
c
 595     continue
         if( n .eq. nf ) return
c
         do 400 m = 1, npts
            do 400 i = n+1, nf
 400     f(i,m) = 0.
c
         return
         end
      SUBROUTINE MANGEN(NDMG,M1,M2,NS,NE,NMM,NEQ,NC,CMAT,TABVAR,
     1     ev,dc,xi,psimix,xv,NVM,x,y,ih,phig,PSIGEN,
     1     LRW,RW,LIW,IW,NFILE6,RPAR,IPAR)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
        REAL  gman(11, 11, 3), geql(11,3)
        DOUBLE PRECISION CHI(1)
c
C***********************************************************************
C     manifold arrays
C***********************************************************************
      DIMENSION CMAT(NC,NEQ),RPAR(*),IPAR(*)
      DIMENSION EV(NE,NS),XI(NE),DC(NMM,NMM),TABVAR(NC),PSIMIX(NEQ)
      DIMENSION RW(LRW),IW(LIW)
      DIMENSION X(NVM),Y(NVM),XV(NS,NVM),IH(NVM),PHIG(NS,M2)
      DIMENSION PSIGEN(NEQ,M1,M2)
C***********************************************************************
C     COMMON BLOCKS
C***********************************************************************
      COMMON/BCHIS/EWF(MNCON,7),SMFF(MNEQU,7),PAT(2,MNTCO),
     1       MIXFRA,MENFRA,MPRFRA
C***********************************************************************
C     USEFUL CONSTANTS
C***********************************************************************
      DATA IOP/0/
C***********************************************************************
C     FIRST EXECUTABLE STATEMENT FOLLOWS
C***********************************************************************
C***********************************************************************
C     Compute Dimension of the Table and number of properties
C***********************************************************************
      NDIM = MIXFRA + MENFRA + MPRFRA + NDMG + 1
C***********************************************************************
C     write information
C***********************************************************************
      IF(MIXFRA.EQ.0) WRITE(NFILE6,8831) NDIM
 8831 FORMAT(' constant mixture fraction used for the',I2,
     1                    '-dimensional table setup')
      IF(MIXFRA.EQ.1) WRITE(NFILE6,8832) NDIM
 8832 FORMAT(' mixture fraction is one of the',I2,
     1     ' tabulation coordinates')
      IF(MIXFRA.NE.1.AND.MIXFRA.NE.0) THEN
        WRITE(NFILE6,*) ' wrong numbers for table setup'
        STOP
      ENDIF
C***********************************************************************
C
C     Initialization
C
C***********************************************************************
C***********************************************************************
C     assign elemnt vectors
C***********************************************************************
C---- copy element vectors
      DO 110 J = 1,NE
      DO 110 I = 1,NS
      EV(J,I) = CMAT(2+J,2+I)
  110 CONTINUE
      IF(IOP.GT.2) WRITE(6,*) ((EV(I,j),I=1,ns),j=1,ne)
C***********************************************************************
C     Compute basis for xi-plane
C***********************************************************************
c---- first two vectors are vectors of the controling species
      do 120 iv = 1, 2
      do 120 is = 1, NMM
120   dc(is,iv) = CMAT(2+NE+IV,2+IS)
c---- remainder are element vectors
      do 130 iv = 1, NMM-2
      do 130 is = 1, NMM
130   dc(is,iv+2) = ev(iv,is)
      IF(IOP.GT.2) WRITE(6,*) ((DC(I,j),I=1,NMM),'kk',j=1,NMM)
c---- form orthonormal basis: dc are the direction cosines
      call orthgs( dc, NMM, NMM )
C***********************************************************************
C     Compute minimum phi over mixture fraction for pure mixing
C***********************************************************************
c---- be sure to dimension phimin before un-commenting
c     CHI(1) = 0.0D0
c     CALL CHICOM(3,NEQ,1,CHI(1),PSIMIX)
c     do 533 i=1,neq
c     phimin(i) = PSIMIX(i)
c 533 continue
c     CHI(1) = 1.0D0
c     CALL CHICOM(3,NEQ,1,CHI(1),PSIMIX)
c     do 535 i=1,neq
c     phimin(i) = dmin1(PSIMIX(i),phimin(i))
c 535 continue
c     write(6,*) (phimin(i+2),i=1,NMM)
C***********************************************************************
C
C     Loop over mixture fraction
C
C***********************************************************************
      do 300 i1 = 1, m1
C***********************************************************************
C     compute chi, PSIMIX, specific element mole numbers, etc.
C***********************************************************************
      if(m1.eq.1) then
        chi(1) = 1.0D0
      else
        chi(1) = ( i1 - 1 ) / float( m1 - 1 )
      endif
      NCONS = NE + 2
      CALL CHICOM(2,NCONS,1,CHI(1),TABVAR)
      CALL CHICOM(3,NEQ,1,CHI(1),PSIMIX)
      H = TABVAR(1)
      P = TABVAR(2)
      DO 210 I=1,NE
      XI(I) = TABVAR(I+2)
  210 CONTINUE
C***********************************************************************
C     determine vertices                                               *
C***********************************************************************
      call vert( ev, ne, xi, ne, NMM, ns, nv, xv, NVM)
      IF(IOP.GT.2) then
      do 215 j=1,nv
      write(6,8777) (xv(i,j),i=1,NMM)
 8777 format('  ',6(1pe10.3,2x))
  215 continue
      endif
C***********************************************************************
C     find coordinates of vertices projected onto constant xi plane
C***********************************************************************
      do 230 iv = 1, nv
      call ctrans( dc, xv(1,iv), NMM, RW )
      x(iv) = RW(1)
  230 y(iv) = RW(2)
C***********************************************************************
c  find ordering of hull vertices
C***********************************************************************
      call hull( x, y, nv, nh, ih )
      if(iop.gt.2) then
        write(6,*) (ih(i),i=1,nv)
      do 3413 j=1,nv
      write(6,8777) (xv(i,ih(j)),i=1,NMM)
 3413 continue
      write(6,*) ' psimix'
      write(6,8777) (PSIMIX(2+i),i=1,NMM)
      endif
        if( nv .ne. nh ) write(0,*)'simpler: warning nv .ne. nh',nv,nh
C***********************************************************************
c  find vertex on mixing line, and reorder so that it is ih(1)
C***********************************************************************
        call mixpt( PSIMIX(3), xv, ns, nv, ih, NVM )
C***********************************************************************
c  add mixing line as final point
C***********************************************************************
        nv     = nv + 1
        ih(nv) = ih(1)
      if(iop.gt.2) then
      do 3414 j=1,nv
      write(6,8777) (xv(i,ih(j)),i=1,NMM)
 3414 continue
      endif
C***********************************************************************
c  determine equally-spaced points around the manifold generating curve
C***********************************************************************
      call gcurve( xv,ns, ns, nv, ih, m2, phig,ns )
      if(iop.gt.2) then
      do 2211 j=1,m2
      write(6,878) (phig(i,j),i=1,NMM)
 2211 continue
  878 format(' ',6(1pe9.2,1x))
      endif
C***********************************************************************
c  store psigen
C***********************************************************************
        do 350 i2 = 1, m2
        psigen(1,i1,i2) = H
        psigen(2,i1,i2) = P
      do 350 i = 1, ns
        psigen(2+i,i1,i2) = phig(i,i2)
  350 continue
C***********************************************************************
c  store xi-plane coordinates of mainifold generator
C***********************************************************************
        do 390 i2 = 1, m2
        call ctrans( dc, phig(1,i2), NMM, rw )
        gman(i1,i2,1) = rw(1)
        gman(i1,i2,2) = rw(2)
        gman(i1,i2,3) = rw(3)
        geql(i1,1) = rw(1)
        geql(i1,2) = rw(2)
        geql(i1,3) = rw(3)
  390 continue
c
  300 continue
c
C       call pgman( gman, geql, m1, m2 )
C**********************************************************************
C     Regular exit
C**********************************************************************
      RETURN
      END
      SUBROUTINE RXNPRO(INFO,NP,NEQ,TEND,PSIGEN,PSITRA,DPSIDT,
     1   RPAR,IPAR,LIW,IW,LRW,RW,
     1 NMM,NOM,NOUT,TSTO,ARCSTO,PSISTO,RMSSTO,
     1  NPROP,M1,M2,PROP,IPR,NFILE6)
C***********************************************************************
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      LOGICAL SKIP
      EXTERNAL MASLEF,MASRIG,DUMJAC
      DIMENSION PSIGEN(NEQ),PSITRA(NEQ),DPSIDT(NEQ)
      DIMENSION PSISTO(NEQ,NOM),ARCSTO(NOM),TSTO(NOM),RMSSTO(NEQ,NOM)
      DIMENSION PROP(NPROP,M1,M2,NP)
C***********************************************************************
C     STORAGE FOR WORK ARRAYS AND PARAMETERS
C***********************************************************************
      DIMENSION RPAR(*),IPAR(*),IW(LIW),RW(LRW)
C***********************************************************************
C     STORAGE ARRAY FOR TRAJECTORIES
C***********************************************************************
      DIMENSION IC(2),VC(2)
      DIMENSION IJOB(20),RTOL(1),ATOL(1),RTOLS(1),ATOLS(1)
      COMMON/BMIX/OMEGA,SMFMIX(MNEQU),SMFCHA(MNEQU),VDIR(MNEQU,NDIMM)
C***********************************************************************
C     ERROR TOLERANCES FOR THE INTEGRATOR
C***********************************************************************
      DATA RTOL(1) /1.D-3/,ATOL(1) /1.D-10/,
     1     RTOLS(1)/1.D-3/,ATOLS(1)/1.D-10/
      DATA IC(1)/0/,IC(2)/0/
C---- DFDX GIVES THE DERIVATIVE OF THE FORM DF/DX FOREWARD OR BACKWARD
      DFDX(F1,F2,DX) =  (F2-F1)/DX
C---- DFDXCE GIVES THE CENTRAL DIFFERENCE APPROXIMATION
C     DFDXCE(FL,F0,FR,DXL,DXR)= 0.5D0* (DFDX(F0,FR,DXR)+DFDX(FL,F0,DXL))
C---- DFDXPA GIVES THE DERIVATIVE OF THE FORM DF/DX (PARABOLIC)
      DFDXPA(FL,F0,FR,DXL,DXR)=
     1    ( DXR*DFDX(FL,F0,DXL) + DXL*DFDX(F0,FR,DXR) )  /  (DXL+DXR)
C***********************************************************************
C     Set initial value
C***********************************************************************
      CALL DCOPY(NEQ,PSIGEN,1,PSITRA,1)
C***********************************************************************
C     initialize integration parameters
C***********************************************************************
      NUCNUC = 1
      ICALL    = 0
      DO 10 I=1,20
   10 IJOB(I) = 0
      NZV     = 0
      NZC     = NEQ
      IJOB(1) = 1
      TIME = 0.D0
      H    = 1.D-7
      HMAX = TEND
      IJOB(7) = 0
      IF(LRW.LT.NEQ) GOTO 905
C***********************************************************************
C     store initial values
C***********************************************************************
      NOUT = 1
      TSTO(NOUT) = TIME
      ARCSTO(NOUT) = 0.0D0
      DO 15 I = 1, NEQ
      RMSSTO(I,NOUT) = 0.0D0
  15  CONTINUE
      CALL DCOPY(NEQ,PSITRA,1,PSISTO(1,NOUT),1)
C***********************************************************************
C
C     Integration loop
C
C***********************************************************************
   20 CONTINUE
      CALL XLIMEX(NEQ,NZC,NZV,MASLEF,MASRIG,DUMJAC,
     *      TIME,PSITRA,DPSIDT,TEND,RTOL,ATOL,RTOLS(1),ATOLS(1),
     *      HMAX,H,IJOB,LRW,RW,LIW,IW,ICALL,RPAR,IPAR)
      ICALL = 2
      NOUT = NOUT + 1
      IF(IJOB(7).LT.0) THEN
        WRITE(NFILE6,*) ' ERROR DURING INTEGRATION '
        STOP
      ENDIF
      IJOB(7) = 0
      IF(NOUT.GT.NOM) GOTO 900
C***********************************************************************
C     store results
C***********************************************************************
      IF(INFO.GT.1) THEN
      TSTO(NOUT) = TIME
      CALL DCOPY(NEQ,PSITRA,1,PSISTO(1,NOUT),1)
      CALL DCOPY(NEQ,DPSIDT,1,RMSSTO(1,NOUT),1)
C---- use all species for arclength
      DIFF = 0.0D0
      DO 60 I= 3,NEQ
      DIFF = DIFF + (PSISTO(I,NOUT)-PSISTO(I,NOUT-1))**2
   60 CONTINUE
      DIFF = DSQRT(DIFF)
      ARCSTO(NOUT) = ARCSTO(NOUT-1) + DIFF
      ENDIF
C***********************************************************************
C     continue if time < tend
C***********************************************************************
      IF(TIME.LT.0.9999D0*TEND) GOTO 20
C***********************************************************************
C     print out reults
C***********************************************************************
      IF(IPR.GT.0.AND.INFO.LE.2) THEN
      do 335 l=1,nout
      write(6,*) TSTO(l),(psisto(i+2,l),i=1,NMM)
 335  continue
      ENDIF
C***********************************************************************
C     leave subroutine for info =1
C***********************************************************************
      if(info.le.2) return
C***********************************************************************
C     normalize arclength
C***********************************************************************
      DO 110 L = 1,NOUT
      ARCSTO(L) = ARCSTO(L) / ARCSTO(NOUT)
      IF(IPR.GT.0) THEN
        WRITE(6,82) TSTO(L),ARCSTO(L),(PSISTO(I,L),I=3,7)
      ENDIF
  110 CONTINUE
   82 FORMAT(1x,1pe20.13,7(1x,1PE9.2))
C***********************************************************************
C
C     Interpolate onto equidistant grid
C
C***********************************************************************
      write(nfile6,*) nout,' time steps used for the interpolation'
      IFOL = 1
      IDER = IFOL + NOUT
      IFUN = IDER + NOUT
      INP  = IFUN + NP
      IRW  = INP  + NP
      NWK = LRW - IRW
      IF(NWK.LT.1) GOTO 905
      DO 210 I = 1,NP
      RW(INP-1+I) = FLOAT(I-1)/FLOAT(NP-1)
  210 CONTINUE
c     write(*,*) (rw(inp-1+i),i=1,np)
      SKIP=.FALSE.
      SWITCH = 0.0D0
      INPRO = NPROP*M1*M2
C***********************************************************************
C     INTERPOLATE TIME
C***********************************************************************
      CALL DPCHIC(IC,VC,SWITCH,NOUT,ARCSTO,TSTO,RW(IDER),1,
     1      RW(IRW),NWK,IERR)
      IF(IERR.LT.0) GOTO 920
      CALL DPCHFE(NOUT,ARCSTO,TSTO,RW(IDER),1,SKIP,
     1     NP,RW(INP),RW(IFUN),IERR)
      IF(IERR.LT.0) GOTO 921
      CALL DCOPY(NP,RW(IFUN),1,PROP(NEQ+NEQ+1,1,1,1),INPRO)
c     write(*,*) (rw(ifun-1+i),i=1,np)
c---- this is the current value of the time
C***********************************************************************
C     INTERPOLATE PSIS
C***********************************************************************
      DO 390 I=1,NEQ
      CALL DCOPY(NOUT,PSISTO(I,1),NEQ,RW(IFOL),1)
c     write(*,*) '  old fu',(rw(ifol-1+l),l=1,nout)
c     write(*,*) '   -------------  '
      CALL DPCHIC(IC,VC,SWITCH,NOUT,ARCSTO,RW(IFOL),RW(IDER),1,
     1      RW(IRW),NWK,IERR)
      IF(IERR.LT.0) GOTO 922
      CALL DPCHFE(NOUT,ARCSTO,RW(IFOL),RW(IDER),1,SKIP,
     1     NP,RW(INP),RW(IFUN),IERR)
      IF(IERR.LT.0) GOTO 923
      CALL DCOPY(NP,RW(IFUN),1,PROP(I,1,1,1),INPRO)
c     write(*,*) (rw(ifun-1+l),l=1,np)
  390 CONTINUE
      DO 410 L=1,NP
C       WRITE(6,82) (PROP(I,1,1,L),I=3,8)
  410 CONTINUE
C***********************************************************************
C     INTERPOLATE RATES OF ALL SPECIES
C***********************************************************************
      DO 380 I=1,NEQ
      CALL DCOPY(NOUT,RMSSTO(I,1),NEQ,RW(IFOL),1)
      CALL DPCHIC(IC,VC,SWITCH,NOUT,ARCSTO,RW(IFOL),RW(IDER),1,
     1      RW(IRW),NWK,IERR)
      IF(IERR.LT.0) GOTO 922
      CALL DPCHFE(NOUT,ARCSTO,RW(IFOL),RW(IDER),1,SKIP,
     1     NP,RW(INP),RW(IFUN),IERR)
      IF(IERR.LT.0) GOTO 923
      CALL DCOPY(NP,RW(IFUN),1,PROP(NEQ+I,1,1,1),INPRO)
c     write(*,*) (rw(ifun-1+l),l=1,np)
  380 CONTINUE
CAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
C
C      This is the new block
C
CAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
C***********************************************************************
C     get time derivative
C***********************************************************************
C     set icder = 0 for derivatives using picewise monotonic cubic
C         hermite interpolant
C     set icder .ne. 0 for central difference
C
      ICDER = 0
      IF(ICDER.EQ.0) THEN
      CALL DPCHIC(IC,VC,SWITCH,NOUT,TSTO,ARCSTO,RW(IFOL),1,
     1      RW(IRW),NWK,IERR)
      IF(IERR.LT.0) GOTO 920
      ELSE
      DXM             = TSTO(2) - TSTO(1)
      RW(IFOL)        = DFDX(ARCSTO(2),ARCSTO(1),DXM)
      RW(IFOL-1+NOUT) = 0.0D0
      DO 2311 I = 2,NOUT-1
      DXM = TSTO(I  ) - TSTO(I-1)
      DXP = TSTO(I+1) - TSTO(I)
      RW(IFOL-1+I) = DFDXPA(ARCSTO(I-1),ARCSTO(I),ARCSTO(I+1),DXM,DXP)
 2311 CONTINUE
      ENDIF
C---- rw(IFOL) contains the time derivatives of the arclength at the
C     different points
C
C---- rw(ider) is the time derivative of the arclength
      CALL DPCHIC(IC,VC,SWITCH,NOUT,ARCSTO,RW(IFOL),RW(IDER),1,
     1      RW(IRW),NWK,IERR)
      IF(IERR.LT.0) GOTO 922
      CALL DPCHFE(NOUT,ARCSTO,RW(IFOL),RW(IDER),1,SKIP,
     1     NP,RW(INP),RW(IFUN),IERR)
      CALL DCOPY(NP,RW(IFUN),1,PROP(NEQ+NEQ+2,1,1,1),INPRO)
      do 377 l=1,np
      write(6,82) float(l-1)/float(np-1),
     1     prop(neq+neq+2,1,1,l)
 377  continue
CBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB
C
C      This is the end of the new block
C
CBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB
C   comment old block
C***********************************************************************
C     get time derivatives of arclength
C***********************************************************************
C     CALL DCOPY(NP,PROP(NEQ+NEQ+1,1,1,1),INPRO,RW(IFUN),1)
C     CALL DPCHIC(IC,VC,SWITCH,NP,RW(IFUN),RW(INP),RW(IDER),1,
C    1      RW(IRW),NWK,IERR)
C     CALL DCOPY(NP,RW(IDER),1,PROP(NEQ+NEQ+2,1,1,1),INPRO)
C     IF(IERR.LT.0) GOTO 920
C     IF(IPR.GT.0) THEN
C     do 377 l=1,np
C     write(6,82) float(l-1)/float(np-1),
C    1     prop(neq+neq+2,1,1,l)
C377  continue
C     ENDIF
C***********************************************************************
C     regular end
C***********************************************************************
      RETURN
  900 write(nfile6,*) ' nout > nom ',nout,nom
      stop
  905 write(nfile6,*) ' work array too small'
      stop
  920 write(nfile6,*) ' ierr 1 from dpchic',ierr
      stop
  921 write(nfile6,*) ' ierr 2 from dpchic',ierr
      stop
  922 write(nfile6,*) ' ierr 3 from dpchic',ierr
      stop
  923 write(nfile6,*) ' ierr 4 from dpchic',ierr
      STOP
      END
      SUBROUTINE SIMIPO(NDIM,M1,M2,M3,C,NPROP,PROP,PROPI)
C
C
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION C(NDIM),PROP(NPROP,M1,M2,M3),PROPI(NPROP)
      COMMON/BNUE/NUMEQ
      DATA ONE/1.0D0/
      NEQ = NUMEQ
      IF(NDIM.GT.3) WRITE(6,*) ' SCHWACHKOPF'
C*********************************************************************
C     compute index of cell
C*********************************************************************
      I1 = max(1,min(m1-1,1 + INT(C(1) * dble(m1-1))))
      I2 = max(1,min(m2-1,1 + INT(C(2) * dble(m2-1))))
      I3 = max(1,min(m3-1,1 + INT(C(3) * dble(m3-1))))
c     write(6,*) ' i1,i2,i3',i1,i2,i3
C*****************************************************************
C     compute origin of cell, position in cell  and scale
C*****************************************************************
      cc1 = c(1) * float(m1-1) - float(i1-1)
      cc2 = c(2) * float(m2-1) - float(i2-1)
      cc3 = c(3) * float(m3-1) - float(i3-1)
c     write(6,*) ' cc1,cc2,cc3',cc1,cc2,cc3
C*****************************************************************
C     PERFORM INTERPOLATION
C*****************************************************************
      NPROI = 2 * NEQ + 2
      LEFT  = NPROP - NPROI
C*****************************************************************
C     PERFORM INTERPOLATION for variables 1 through 2*neq+2
C*****************************************************************
      DO 10 K = 1,NPROI
      PROPI(K) = 0.0
   10 CONTINUE
      IF(M1.GT.1) THEN
      FAC = (one-cc1)*(one-cc2)*(one-cc3)
      CALL DAXPY(NPROI,FAC,PROP(1,i1,i2,i3),1,PROPI,1)
      FAC = (    cc1)*(one-cc2)*(one-cc3)
      CALL DAXPY(NPROI,FAC,PROP(1,i1+1,i2,i3),1,PROPI,1)
      FAC = (    cc1)*(    cc2)*(one-cc3)
      CALL DAXPY(NPROI,FAC,PROP(1,i1+1,i2+1,i3),1,PROPI,1)
      FAC = (one-cc1)*(    cc2)*(one-cc3)
      CALL DAXPY(NPROI,FAC,PROP(1,i1,i2+1,i3),1,PROPI,1)
      FAC = (one-cc1)*(one-cc2)*(    cc3)
      CALL DAXPY(NPROI,FAC,PROP(1,i1,i2,i3+1),1,PROPI,1)
      FAC = (    cc1)*(one-cc2)*(    cc3)
      CALL DAXPY(NPROI,FAC,PROP(1,i1+1,i2,i3+1),1,PROPI,1)
      FAC = (    cc1)*(    cc2)*(    cc3)
      CALL DAXPY(NPROI,FAC,PROP(1,i1+1,i2+1,i3+1),1,PROPI,1)
      FAC = (one-cc1)*(    cc2)*(    cc3)
      CALL DAXPY(NPROI,FAC,PROP(1,i1,i2+1,i3+1),1,PROPI,1)
      ELSE
      FAC = (one-cc2)*(one-cc3)
      CALL DAXPY(NPROI,FAC,PROP(1,i1,i2,i3),1,PROPI,1)
      FAC = (    cc2)*(one-cc3)
      CALL DAXPY(NPROI,FAC,PROP(1,i1,i2+1,i3),1,PROPI,1)
      FAC = (one-cc2)*(    cc3)
      CALL DAXPY(NPROI,FAC,PROP(1,i1,i2,i3+1),1,PROPI,1)
      FAC = (    cc2)*(    cc3)
      CALL DAXPY(NPROI,FAC,PROP(1,i1,i2+1,i3+1),1,PROPI,1)
      ENDIF
C*****************************************************************
C     PERFORM lookup for variables 1 through 2*neq+2
C*****************************************************************
      IF(M1.GT.1) THEN
        CALL DCOPY(LEFT,PROP(NPROI+1,i1,i2,i3),1,PROPI(NPROI+1),1)
      ELSE
        CALL DCOPY(LEFT,PROP(NPROI+1,1,i2,i3),1,PROPI(NPROI+1),1)
      ENDIF
      RETURN
      END
      SUBROUTINE RXNSIM(INFO,NDIM,NP,NEQ,TEND,C,CI,CIT,LIW,IW,LRW,RW,
     1   IPAR,RPAR,NOM,NOUT,TSTO,LDSTO,PSISTO,RMSSTO,IPR,NFILE6)
C***********************************************************************
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      EXTERNAL SMMLEF,SIMRIG,DUMJAC
      DIMENSION C(NDIM),CI(NDIM),CIT(NDIM)
      DIMENSION PSISTO(LDSTO,NOM),TSTO(NOM),RMSSTO(LDSTO,NOM)
C***********************************************************************
C     STORAGE FOR WORK ARRAYS AND PARAMETERS
C***********************************************************************
      DIMENSION RPAR(*),IPAR(*),IW(LIW),RW(LRW)
C***********************************************************************
C     STORAGE ARRAY FOR TRAJECTORIES
C***********************************************************************
      DIMENSION IJOB(20),RTOL(1),ATOL(1),RTOLS(1),ATOLS(1)
      COMMON/BMIX/OMEGA,SMFMIX(MNEQU),SMFCHA(MNEQU),VDIR(MNEQU,NDIMM)
C***********************************************************************
C     ERROR TOLERANCES FOR THE INTEGRATOR
C***********************************************************************
      DATA RTOL(1) /1.D-2/,ATOL(1) /1.D-3/,
     1     RTOLS(1)/1.D-3/,ATOLS(1)/1.D-10/
C***********************************************************************
C     Set initial value
C***********************************************************************
      CALL DCOPY(NDIM,C,1,CI,1)
C***********************************************************************
C     initialize integration parameters
C***********************************************************************
      NUCNUC = 1
      ICALL    = 0
      DO 10 I=1,20
   10 IJOB(I) = 0
      NZV     = 0
      NZC     = NDIM
      IJOB(1) = 1
      TIME = 0.D0
      H    = 1.D-7
      HMAX = TEND
      IJOB(7) = 0
      IF(LRW.LT.NEQ) GOTO 905
C***********************************************************************
C     store initial values
C***********************************************************************
      NOUT = 1
      TSTO(NOUT) = TIME
      DO 15 I = 1, NEQ
      RMSSTO(I,NOUT) = 0.0D0
  15  CONTINUE
      CALL DCOPY(NDIM,CI,1,PSISTO(1,NOUT),1)
C***********************************************************************
C
C     Integration loop
C
C***********************************************************************
   20 CONTINUE
      CALL XLIMEX(NDIM,NZC,NZV,SMMLEF,SIMRIG,DUMJAC,
     *      TIME,CI,CIT,TEND,RTOL,ATOL,RTOLS(1),ATOLS(1),
     *      HMAX,H,IJOB,LRW,RW,LIW,IW,ICALL,RPAR,IPAR)
      ICALL = 2
      NOUT = NOUT + 1
      IF(IJOB(7).LT.0) THEN
        WRITE(NFILE6,*) ' ERROR DURING INTEGRATION '
        STOP
      ENDIF
      IJOB(7) = 0
      IF(NOUT.GT.NOM) GOTO 900
C***********************************************************************
C     store results
C***********************************************************************
      IF(INFO.GT.1) THEN
      TSTO(NOUT) = TIME
      CALL DCOPY(NDIM,CI,1,PSISTO(1,NOUT),1)
      CALL DCOPY(NDIM,CIT,1,RMSSTO(1,NOUT),1)
      ENDIF
C***********************************************************************
C     continue if time < tend
C***********************************************************************
      IF(TIME.LT.0.9999D0*TEND) GOTO 20
C***********************************************************************
C
C***********************************************************************
      return
C***********************************************************************
C     regular end
C***********************************************************************
  900 write(nfile6,*) ' nout > nom ',nout,nom
      nout = nom
      return
  905 write(nfile6,*) ' work array too small'
      stop
  920 write(nfile6,*) ' ierr 1 from dpchic'
      stop
  921 write(nfile6,*) ' ierr 2 from dpchic'
      stop
  922 write(nfile6,*) ' ierr 3 from dpchic'
      stop
  923 write(nfile6,*) ' ierr 4 from dpchic'
      STOP
      END
      SUBROUTINE SIMRIG(N,NZV,T,Y,F,BV,IR,IC,RG,IG,IFC,IFCP)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'homdim.f'
      DIMENSION Y(N),F(N),BV(1),IR(1),IC(1),IFCP(1),RG(1),IG(1)
      DIMENSION XPROP(100),pert(100)
      DIMENSION YH(5)
      COMMON/BMIX/OMEGA,SMFMIX(MNEQU),SMFCHA(MNEQU),VDIR(MNEQU,NDIMM)
      COMMON/BTNU/M1,M2,M3,NPROP
      COMMON/BNUE/NUMEQ
C***********************************************************************
C
C***********************************************************************
      IF(NPROP.GT.100) THEN
      WRITE(6,*) ' errr nprop '
      STOP
      ENDIF
CCCCCCCCCCCCCCC
      neq = numeq
C***********************************************************************
C
C***********************************************************************
      DO 5 i=1,n
      yh(i) = y(i)
    5 continue
      if(y(3).gt.1.0d0) then
        y2h = - y(2)
        else
        y2h = y(2)
      endif
      yh(3) = 1.0d0 - dabs(1.0d0 - y(3))
      imul = idint(y2h)
      if(y2h .lt.0.0d0) imul = imul -1
      yh(2) = y2h - float(imul)
c     write(6,*) 'y,yh',y(2),y(3),yh(2),yh(3)
      CALL SIMIPO(3,M1,M2,M3,YH,NPROP,RG,XPROP)
      p1 =0.0d0
      p2 =0.0d0
      p3 =0.0d0
      do 10 i=1,neq
      pert(i)  =  omega *(smfmix(i) - xprop(i))
   10 continue
c     write(6,*) (pert(i),i=1,neq)
c     write(6,*) ' gra1',(xprop(neq+neq+2+neq+i),i=1,neq)
c     write(6,*) ' gra2',(xprop(neq+neq+2+neq+neq+i),i=1,neq)
      do 20 i=1,neq
      p1 = p1 + xprop(neq+neq+2+      i) * pert(i)
      p2 = p2 + xprop(neq+neq+2+  neq+i) * pert(i)
      p3 = p3 + xprop(neq+neq+2+2*neq+i) * pert(i)
   20 continue
C***********************************************************************
C      compute projection of perturbation on manifold                  *
C***********************************************************************
c     write(6,*) 'p1,p2,p3',p1,p2,p3,xprop(32)
      f(1) = p1
      f(2) = p2
      f(3) = XPROP(32) + p3
      RETURN
      END
      SUBROUTINE SMMLEF (NZC,B,IR,IC)
C***********************************************************************
C
C     DEFINITION OF LEFT HAND SIDE (SEE -ULIMEX-)
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION B(NZC),IR(NZC),IC(NZC)
      DO 10 I=1,NZC
      B(I) =1.0D0
      IR(I) =I
      IC(I) =I
   10 CONTINUE
      RETURN
      END
      DOUBLE PRECISION FUNCTION DERMON(F1,F2,F3,DRM,DRP)
C
C-----------------------------------------------------------------------
C
C
      IMPLICIT DOUBLE PRECISION  (A-H,O-Z)
C
C
C  initialize.
C
CVAX  DATA  ZERO /0.0D0/,  THREE /3.0D0/,SMALL /1.0D-70/
      DATA  ZERO /0.0D0/
C***********************************************************************
C     COMPUTE FIRST DERIVATIVE
C***********************************************************************
      DERM    =(F2-F1)/DRM
      DERP    =(F3-F2)/DRP
      HSUM    = DRM + DRP
C***********************************************************************
C
C***********************************************************************
       IF(0.GT.0) THEN
       IF(DPCHST(DERM,DERP) .LE. ZERO)  THEN
C---- SLOPE SWITHES SIGN
          DERMON=ZERO
       ELSE
C-----   USE BRODLIE MODIFICATION OF BUTLAND FORMULA.
          HSUMT3 = HSUM+HSUM+HSUM
          W1 = (HSUM + DRM)/HSUMT3
          W2 = (HSUM + DRP)/HSUMT3
          DMAX  = DMAX1( DABS(DERM), DABS(DERP) )
          DMIN  = DMIN1( DABS(DERM), DABS(DERP) )
          DRAT1 = DERM/DMAX
          DRAT2 = DERP/DMAX
          DER   = DMIN/(W1*DRAT1 + W2*DRAT2)
          DERMON=DER
       ENDIF
       ELSE
          DERMON = DERP
       ENDIF
C
C  normal return.
C
      RETURN
C------ ------ LAST LINE OF DERUPI FOLLOWS -----------------------------
      END
      SUBROUTINE CHICOM(INFO,NUVA,NCHI,CHI,EW)
C**********************************************************************C
C                                                                      C
C     *************************************************************    C
C     *                                                           *    C
C     *                                                           *    C
C     *    PROGRAM FOR THE CALCULATION OF COMPOSITION             *    C
C     *                                                           *    C
C     *                                                           *    C
C     *                                                           *    C
C     *************************************************************    C
C                                                                      C
C**********************************************************************C
C                                                                      C
C                                                                      C
C**********************************************************************C
C**********************************************************************C
C     CHAPTER I: STORAGE ORGANIZATION                                  C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'homdim.f'
      DIMENSION EW(NUVA),CHI(NCHI)
       COMMON/BCHIS/EWF(MNCON,7),SMFF(MNEQU,7),PAT(2,MNTCO),
     1       MIXFRA,MENFRA,MPRFRA
      COMMON/BCVB/CVBACK(MNCON*MNCON)
      DIMENSION HELP(MNCON)
C**********************************************************************C
C     compute actual values of ew
C**********************************************************************C
ccttchi
      write(6,*) 'chicom ----------------------------------'
cc      stop
c      write(6,2) NCHI, CHI(1)
c  2   format(I4,1PE10.3)
ccttchi
      IF(INFO.EQ.1) THEN
      DO 5 I=1,NUVA
      EW(I) = EWF(I,1)
    5 CONTINUE
C**********************************************************************C
C     compute ew from ew of fuel, oxidizer via chi
C**********************************************************************C
      ELSE IF(INFO.EQ.2) THEN
      DO 11 I=1,NUVA
C     EW(I) = EWF(I,1) 
      EW(I) = 0.0D0
      CHI1 = 1.0D0
      DO 10 K=1,NCHI 
      CHI1 = CHI1 - CHI(K)
      EW(I) = EW(I) + CHI(K) * EWF(I,K+1) 
   10 CONTINUE
      EW(I) = EW(I) + CHI1   * EWF(I,1)
   11 CONTINUE
C**********************************************************************C
C     compute phi from values of fuel and oxidizer
C**********************************************************************C
      ELSE IF(INFO.EQ.3) THEN
      DO 16 I=1,NUVA
      EW(I) = 0.0D0 
      CHI1 = 1.0D0
      DO 15 K=1,NCHI 
      CHI1 = CHI1 - CHI(K)
      EW(I) = EW(I) + CHI(K) * SMFF(I,K+1)
   15 CONTINUE
      EW(I) = EW(I) + CHI1   * SMFF(I,1)
   16 CONTINUE
C**********************************************************************C
C     dummy for computation of chi for only one mixture 
      write(6,*) 'tabrou 1: chi for mixture fraction --------'
c      stop
C**********************************************************************C
      ELSE IF(INFO.EQ.-1) THEN
      CHI(1) = 1.0D0    
C**********************************************************************C
C     compute chi from ew           
C**********************************************************************C
      ELSE IF(INFO.EQ.-2) THEN
CCCCCCCCCCCCCCCCCCC
      MFP1 = NCHI+1
      CALL DGEMV('N',MFP1,NUVA,
     1      ONE,CVBACK,MFP1,EW,1,ZERO,HELP,1)
CCCCCCCCCCCCCCCCCCC
      DO 20 II = 1,NCHI
      CHI(II) = HELP(II+1)
   20 CONTINUE
      ENDIF
c      write(6,*) 'chicom ew,chi ,nchi,ncons '
c      write(6,*) EW,CHI
c      write(6,*) NCHI,NUVA
c      write(6,*) 'chicom CVBACK EWF '
c      write(6,*) CVBACK, EWF
C**********************************************************************C
C     END OF
C**********************************************************************C
      END
