module tra_mod
use options
implicit none

type :: tra
    integer :: nvert
    double precision, allocatable :: time(:), d(:,:), yeq(:), yi(:)
    double precision :: th, ti, is

    contains
        procedure :: init     => init_tra
        procedure :: delete   => del
        procedure :: re       => retra
        procedure :: compare  => rate_tra
        procedure :: prop     => tra_prop

        procedure :: igndel   => igndel
        procedure :: halftim  => halftim
        procedure :: intsp    => intsp
        procedure :: minmax   => tra_minmax
        procedure :: mrg      => merge_tra
end type tra



contains
    !
    ! ###             * Initialization routine *                            ###
    !
    subroutine init_tra(t,fname)
        class(tra), intent(inout) :: t
        character(len=*), intent(in) :: fname
        integer :: ix

        call t%re(fname)
        allocate(t%yi(NEQ), t%yeq(NEQ))
        t%yeq = t%d(1:NEQ, t%nvert)
        t%yi = t%d(1:NEQ,1)

        return
    901 continue
        print*, 'Error initializing trajectory ', trim(fname)
        stop
    end subroutine init_tra

    !
    ! ###                   * Trajectory routines *                         ###
    !
    subroutine igndel(t, pos, ig, smf) 
        ! Function, which returns the trajectory ignition
        ! delay time.
        class(tra), intent(inout) :: t
        integer, intent(out) :: pos
        double precision, intent(out) :: ig, smf(NEQ)

        integer :: i 
        double precision :: slope, ss

        ig = 0.d0
        ss = 0.d0
        do i = 1, t%nvert - 1
            slope = (t%d(A_nrad + 2, i + 1) - t%d(A_nrad + 2, i))/&
                max(1.0d-21, t%time(i+1) - t%time(i))
            if (slope > ss) then
                pos = i
                ss = slope
                ig = t%time(i)
                smf = t%d(1:NEQ, i)
            end if
        end do
        t%ti = ig
        return
    end subroutine igndel

    subroutine halftim(t, pos, halft, smf)
        class(tra), intent(inout) :: t
        integer, intent(out) :: pos
        double precision, intent(out) :: halft
        double precision, intent (out) :: smf(NEQ)

        integer :: i
        double precision :: ref, fd


        ref = (t%d(A_nhalf + 2, 1) + t%d(A_nhalf + 2, t%nvert))/2
        halft = 9.9d9

        if (t%d(A_nhalf + 2, t%nvert)/max(-1.0d-21, t%d(A_nhalf + 2, 1))&
            > A_halftol) return
        do i = 2, t%nvert - 1
            if ( t%d(A_nhalf + 2, i) <= ref ) then
                fd = min(-1.0d-21, t%d(A_nhalf + 2, i) &
                    - t%d(A_nhalf + 2, i - 1))
                halft = t%time(i - 1) + (t%time(i) -t%time(i-1)) / fd * &
                    (ref - t%d(A_nhalf+2,i-1))
                pos = i
                smf = t%d(1:NEQ, i)
                exit
            end if
        end do

        t%th = halft

        return
    end subroutine halftim

    !
    ! ###      * Rates and similarity of two trajectories *                 ###
    !
    subroutine intsp(t, isp)
        class(tra), intent(inout) :: t
        double precision, intent(out) :: isp

        integer :: grow, i


        isp = 0.d0
        grow = -1
        if (t%nvert > 1) then
            ! TODO add check if x is realy monotnonous
            do i=2,t%nvert
                isp = isp + (t%d(A_nint(1) + 2, i) - &
                    t%d(A_nint(1) + 2, i - 1)) &
                    *0.5d0*(t%d(A_nint(2) + 2, i) - t%d(A_nint(2) + 2, i - 1 ))
            end do
        end if

        isp = abs(isp)
        t%is = isp

        return

    end subroutine intsp

    subroutine tra_prop(t)
        class(tra), intent(inout) :: t
        integer :: dummy1
        double precision :: dummy2, dummy3(NEQ)

        if (A_tdel /= 0) call t%igndel(dummy1, dummy2, dummy3)

        if (A_thalf /= 0) call t%halftim(dummy1, dummy2, dummy3)


        if (A_intdif /= 0) call t%intsp(dummy2)

    end subroutine tra_prop

    function rate_tra(t_red, t_det) result(rating)
        class(tra), intent(in) :: t_red, t_det
        double precision :: rating

        rating = 0
        if (A_tdel > 0) then
            rating = rating +A_tdel * abs(t_det%ti - t_red%ti)/&
                max(1.d-20, abs(t_det%ti))
        end if
        if(A_thalf > 0) then
            rating = rating + A_thalf * abs(t_det%th - t_red%th)/&
                max(1.d-20, abs(t_det%th)) 
        end if
        if (A_intdif > 0) then
            rating = rating + A_intdif * abs(t_det%is - t_red%is)/&
                max(1.d-20, abs(t_det%is))
        end if
        return
    end function rate_tra

    subroutine tra_minmax(t, smf)
        class(tra), intent(in) :: t
        double precision, intent(out) :: smf(NEQ)
        integer :: i
        do i = 1, NEQ
            smf(i) = maxval(t%d(i,:))
        end do
        return
    end subroutine tra_minmax
    
    subroutine merge_tra(t1, t2)
        !
        ! Merges two trajectories
        !
        class(tra), intent(inout) :: t1
        type(tra), intent(in) :: t2

        double precision :: temp(NEQ, t1%nvert), temptim(t1%nvert)

        temp = t1%d
        temptim = t1%time

        write(*,*) t1%nvert, t2%nvert
        deallocate(t1%d, t1%time)
        allocate(t1%d(NEQ, t1%nvert + t2%nvert), t1%time(t1%nvert + t2%nvert))
        t1%d(1:NEQ, 1:t1%nvert) = temp
        t1%time(1:t1%nvert)  = temptim
        t1%d(1:NEQ, t1%nvert+1:t1%nvert + t2%nvert) = t2%d(1:NEQ, 1:t2%nvert)
        t1%time(t1%nvert+ 1:t1%nvert + t2%nvert) = t2%time(1:t2%nvert)
        t1%nvert = t1%nvert + t2%nvert


        return

    end subroutine merge_tra

    ! 
    ! Initialization routines
    !

    subroutine del(self)
        class(tra) :: self
        ! if allocated
        if (allocated(self%d)) deallocate(self%d)
        if (allocated(self%time)) deallocate(self%time)
        return
    end subroutine del

!
! In-/Output routines
!


! ###           * Read matrix of the size nin x nin *                       ###
    subroutine rmat(nmat, filename, mat1, mat2) 
        integer, intent(in) :: nmat
        character(len=20),intent(in) :: filename
        double precision, dimension(NEQ, NEQ), optional, intent(out) :: &
            mat1, mat2

        integer :: i, io
        if (index(filename, '.mat') /= 0) then
            open(unit = 666, file = trim(filename), iostat = io)
            if (io /= 0) goto 100
           
            rewind 666
            do i = 1, NEQ
                read(666, IO_format, iostat=io) mat1(1:NEQ, i)
            end do
            if (nmat > 1) then
                do i = 1, NEQ
                    read(666, IO_format, iostat=io) mat2(1:NEQ, i)
                end do
            end if
            close(666)
            return
        end if
        if (index(filename, '.bin') /= 0) then
            open(unit = 666, file= trim(filename), &
                form = 'unformatted', access = 'sequential', iostat = io)
            rewind 666
            if (io /= 0) goto 100
            read(666) mat1
            read(666) mat2
            close(666)
            if (io /= 0) goto 101
            return
        end if
        goto 102
        ! Error exists
    100 continue
        write(*,*) 'Error in rmat: File ', filename, 'not found'
        stop
    101 continue
        write(*,*) 'Error in rmat: Matrix size not what expected'
        stop
    102 continue 
        write(*,*) 'Error in rmat: unknown file type'
    end subroutine rmat

! ###           * Write matrix of the size nin x nin *                      ###
    subroutine wmat(nmat, fno, fid, fnam, mat1, mat2, opt)
        integer, intent(in) :: nmat, fno, opt(2)
        character(len=3), intent(in) :: fnam
        character(len=2), intent(in) :: fid
        double precision, optional, dimension(NEQ, NEQ) :: mat1, mat2

        integer :: io, i
        character(len=10) :: ofmt
        character(len=20) :: filename

        if (opt(2) > 0) then
            write(filename,'(a3,a1,a2,i3.3,a4)') fnam, '_', fid, fno, '.bin'
            open( file = trim(filename), unit = 100, status= 'replace', &
                form = 'unformatted', access = 'sequential')
            if (present(mat1)) write(100) mat1
            if (present(mat2)) write(100) mat2
            close(100)
        end if
        filename = ''
        if (opt(1) > 0) then
            write(filename,'(a3,a1,a2,i3.3,a4)') fnam, '_', fid, fno, '.mat'
            open( file = trim(filename), unit = 100, status = 'replace')
            if (present(mat1)) then
                do i = 1, NEQ
                    write(100,*) mat1(1:NEQ, i)
                end do
            end if
            if (present(mat2)) then
                do i = 1, NEQ
                    write(100,*) mat2(1:NEQ, i)
                end do
            end if
        end if
        return
    end subroutine wmat

! ###           * Subroutine to write a point to a file *                   ###
    subroutine wpoi(i1, dp1, name_short, refp, iopar)
        ! Incoming variables
        integer, intent(in) :: i1, iopar(2)
        double precision, intent(in) :: refp(NEQ), dp1
        character(len=3), intent(in) :: name_short

        ! Local variables
        integer :: i, io
        character(len=10) :: ofmt, tecfmt
        character(len=20) :: fname

        if (iopar(1) > 0) then
            fname = 'refp_'//name_short//'.traj'
            open(file = trim(fname), unit = 100, status = 'old', iostat = io, &
                position = 'append')
            if (io > 0) goto 900

            write(100, IO_format) real(i1,8), dp1, refp
            close(100)
        end if

        fname = ' '
        if (iopar(2) > 0) then
            fname = 'refp_'//name_short//'.tec'
            open(file = trim(fname), unit = 100, status = 'old', iostat = io,&
                position = 'append')
            if (io > 0) goto 900

            write(100, 800) real(i1,8), dp1, refp
            close(100)
        end if

        return

    800 format(2x,7e11.3)
        ! Error exists

    900 continue
        print*, 'Error in wpoi: file ', fname, 'not found.'
        stop

    end subroutine wpoi
! ###           * Subroutine to read trajectory from file *                 ###
    subroutine retra(t, fname)
        class(tra), intent(inout) :: t
        character(len=*), intent(in) :: fname

        double precision :: stem(NEQ+1,10000)
        character(len=100) :: test
        integer :: io, i
       
        if (index(fname, '.traj') /= 0) then
            open(unit = 666, file = trim(fname), status = 'old', iostat = io)
            if (io /= 0) goto 201
            i = 0
            do while (io == 0)
                i = i + 1
                read(666,*, iostat = io) stem(1:NEQ+1,i)
            end do
            close(666)
            i = i - 1
            t%nvert = i
            allocate(t%d(NEQ, i), t%time(i))
            t%d = stem(2:NEQ+1, 1:i)
            t%time = stem(1, 1:i)
            return
        end if

        if (index(fname, '.bin') /= 0) then
            open(unit = 666, file = trim(fname), &
                form = 'unformatted', access = 'sequential', iostat = io)
            if (io /= 0) goto 201
            i = 0
            do while (io /= 0)
                i = i + 1
                read(666,iostat = io) stem(NEQ+1,i)
            end do
            close(666)
            t%nvert = i
            t%d = stem(2:NEQ+1, 1:i)
            t%time = stem(1, 1:i)
            return
        end if

        ! Error exists
        print*, 'Error in retra: unknown data type :', trim(fname)
    201 continue
        print*, 'Error in retra: could not open file ', trim(fname)
        stop
    end subroutine retra

! ###           * SUbroutine to write trajectory to file *                  ###
    subroutine wtra(fno, fid, fnam, t, iopt)
        class(tra), intent(in) :: t
        integer, intent(in) :: fno, iopt(2)
        character(len=3), intent(in) :: fnam, fid

        integer :: io, i
        character(len=20) :: outfmt, filename
        character(len=8) :: vars(NEQ+1)

        if (iopt(1) > 0) then
            !Tecplot output
            write(vars(1), '(a)') 't       '
            vars(2:NEQ+1) = SNAM(1:NEQ)
            write(filename, "(a,'_', a, '-', i3.3, '.tec' )") fnam, fid, fno
            open(file = trim(filename), unit = 100, status = 'replace')
            call techead(100, NEQ+1, t%nvert, vars)
            do i =1, t%nvert
                write(100, '(2x, 7e11.3)') t%time(i), t%d(1:NEQ, i)
            end do
            close(100)
        end if
        filename = ''
        if (iopt(2) > 0) then
            ! Trajectory format
            write(filename, "(a, '_', a, '-', i0.3, '.traj')") fnam, fid, fno
            open(file = trim(filename), unit = 100, status = 'replace')
            do i =1, t%nvert
                write(100, IO_format) t%time(i), t%d(1:NEQ, i)
            end do
            close(100)
        end if
        return
    end subroutine wtra

    subroutine techead(nfil, nvar, nvert, vars)
        integer, intent(in) :: nfil, nvar, nvert
        character(len=8), intent(in) :: vars(nvar)

        write(nfil, *) 'VARIABLES = '
        write(nfil, 100) vars(1:nvar)
        write(nfil, *) 'ZONE I=', nvert, 'J=1, F=POINT'

    100 format(7('"',a,'",'))
        return
    end subroutine techead
end module tra_mod

