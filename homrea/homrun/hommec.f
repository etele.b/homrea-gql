C>    hommec
      SUBROUTINE HOMMEC(NSYMB,LIW,IW,LRW,RW,INPFIL,NFIOUT)
C***********************************************************************
C
C     .------------------------------------------------------------.
C     |                                                            |
C     |            Analysis of the Reaction Mechanism              |
C     |                       (24.09.1985)                         |
C     |                                                            |
C     |                     DRIVER ROUTINE                         |
C     '------------------------------------------------------------'
C
C***********************************************************************
C
C           INPUT-PARAMETERS FROM INPFIL
C
C     NSPEC.........NUMBER OF SPECIES
C     NREAC.........NUMBER OF REACTIONS
C     NRINS      ...SPECIES SYMBOLS
C     NM         ...THIRD BODIES
C
C***********************************************************************
C     I: STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT REAL (A-H,O-Z)
      INCLUDE 'homdim.f'
      CHARACTER*1 CW
      CHARACTER(LEN=*) NSYMB(*)
      DIMENSION NOPT(40)
      DIMENSION RW(LRW),IW(LIW),CW(8*(MNSPE+MNTHB+1))
      LCW=8*(MNSPE+MNTHB+1)
C***********************************************************************
C     GET NUMBERS NECESSARY TO BUILD UP WORK SPACE
C***********************************************************************
      REWIND INPFIL
      READ(INPFIL,810) (NOPT(I),I=1,40)
      READ(INPFIL,820) NSPEC,NREAC,NRINS,NM
  810 FORMAT(12X,20I3)
  820 FORMAT(1X,4(4X,I5))
C***********************************************************************
C     CALL OF PROGRAM
C***********************************************************************
      CALL HOMAN1(NSYMB,NOPT,NSPEC,NREAC,NRINS,NM,INPFIL,NFIOUT)
C***********************************************************************
C     REGULAR EXIT
C***********************************************************************
      RETURN
C***********************************************************************
C     END OF DRIVER ROUTINE
C***********************************************************************
      END
      SUBROUTINE HOMAN1(NSYMB,NOPT,NSPEC,NREAC,NRINS,NM,INPFIL,NFIOUT)
C***********************************************************************
C
C     .------------------------------------------------------------.
C     |                                                            |
C     |            Analysis of the Reaction Mechanism              |
C     |                       (24.09.1985)                         |
C     |                                                            |
C     '------------------------------------------------------------'
C
C***********************************************************************
C
C           INPUT-PARAMETERS:
C
C     NSPEC.........NUMBER OF SPECIES
C     NREAC.........NUMBER OF REACTIONS
C     NRINS      ...SPECIES SYMBOLS
C     NM
C
C***********************************************************************
C     I: STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT REAL (A-H,O-Z)
      LOGICAL STOP,TEST
      DIMENSION NOPT(*)
      DIMENSION SRATE(NSPEC),RINTC(NSPEC),SRINT(NSPEC)
      DIMENSION RAMEC(NREAC)
      DIMENSION RRATE(NSPEC,NREAC),RINT(NSPEC,NREAC)
      DIMENSION NSWI(NSPEC)
      DIMENSION NRWI(NREAC),IMPINT(NREAC),IRUECK(NREAC)
      DIMENSION MATL(6,NREAC),NRVEC(3,NRINS)
      CHARACTER(LEN=*) NSYMB(NSPEC+NM+1)
      CHARACTER*16 RATKIN
      CHARACTER*12 UNITS
C***********************************************************************
C     CALCULATION OF ABSOLUTE AND RELATIVE REACTION RATES
C***********************************************************************
      STOP=.TRUE.
      TEST=.FALSE.
C***********************************************************************
C     INPUT OF DATA NECESSARY FOR ARRAY INITIALIZATION
C***********************************************************************
C***********************************************************************
C     INPUT OF INFORMATION DATA
C***********************************************************************
      CALL HOMIN1(NSPEC,NREAC,NRINS,NM,NSYMB,IRUECK,MATL,NRVEC,
     1        INPFIL,NFIOUT,NOPT,TEST)
C    1        INPFIL,NFIOUT,NOPT,TEST,MIW,IFIT)
C***********************************************************************
C     INPUT OF ARRAYS
C***********************************************************************
   10 CONTINUE
      CALL HOMIN2(TIME,NSPEC,NREAC,NRINS,RAMEC,
     1    RATKIN,UNITS,INPFIL,NFIOUT,NOPT,TEST,STOP)
C---- IF END OF DATASET IS REACHED PRINT INTEGRAL RATES AND STOP
      IF(STOP) GOTO 7000
C***********************************************************************
C     OUTPUT OF RATES AND ADD FOR INTEGRALS
C***********************************************************************
      WRITE(NFIOUT,8030) RATKIN,TIME
      CALL HOMCAL(NSPEC,NREAC,NRINS,NSYMB,
     1    SRATE,RINTC,SRINT,RAMEC,RRATE,RINT,
     1     NSWI,NRWI,IMPINT,IRUECK,
     1     NRVEC,RATKIN,UNITS,INPFIL,NFIOUT,NOPT,TEST)
C***********************************************************************
C     END LOOP AND JUMP TO 10
C***********************************************************************
      GOTO 10
C***********************************************************************
C     OUTPUT OF INTEGRALS FOR LAST CALCULATION
C***********************************************************************
 8030 FORMAT('1',79('*'),/,
     1     ' **** ',A16,' of Formation and',
     1     ' Consumption at Time = '
     1     ,1PE11.3,' S  ****'
     1     ,/,1X,79('*'))
 7000 CONTINUE
      RETURN
C***********************************************************************
C     END OF OUTHOM
C***********************************************************************
      END
      SUBROUTINE HOMIN1(NSPEC,NREAC,NRINS,NM,NSYMB,IRUECK,MATL,NRVEC,
     1     INPFIL,NFIOUT,NOPT,TEST)
C***********************************************************************
C***********************************************************************
C
C     **************************************************************
C     *                                                            *
C     *        ELIMINATION OF UNIMPORTANT REACTIONS                *
C     *                    (2.11.1981)                             *
C     *                                                            *
C     **************************************************************
C
C           INPUT-PARAMETERS:
C
C     NS............NUMBER OF SPECIES
C     NR............NUMBER OF REACTIONS
C     NSYMB().......SPECIES SYMBOLS
C     IRUECK(NR)....IRUECK=2 FOR REVERSE REACTION, =1 FOR RESPECTIVE
C                   FORWARD REACTION (THE REVERSE REACTION MUST BE
C                   LOCATED DIRECTLY AFTER THE FORWARD REACTION)
C
C***********************************************************************
C     I: STORAGE ORGANIZATION
C***********************************************************************
      LOGICAL STOP,TEST,TSO
      CHARACTER(LEN=*)  NSYMB(NSPEC+NM+1)
      DIMENSION IRUECK(*)
      DIMENSION NOPT(*)
      DIMENSION MATL(6,*),NRVEC(3,*)
C***********************************************************************
C     INPUT OF DATA FROM UNIT INPFIL
C***********************************************************************
      LSYM=LEN(NSYMB(1))
      READ(INPFIL,8320) (NSYMB(I),I=1,NSPEC)
      DO 110 I=1,NM
      DO  J=1,LSYM
      NSYMB(NSPEC+I)(J:J) = ' '        
      ENDDO
      NSYMB(NSPEC+I)='M       '
  110 CONTINUE
      DO  J=1,LSYM
      NSYMB(NSPEC+NM+1)(J:J)=' '
      ENDDO
      IF(TEST) WRITE(NFIOUT,8320) (NSYMB(I),I=1,NSPEC)
      READ(INPFIL,8330) (IRUECK(I),I=1,NREAC)
      IF(TEST) WRITE(NFIOUT,8331) (IRUECK(I),I=1,NREAC)
      READ(INPFIL,857)
  857 FORMAT(1X)
      READ(INPFIL,856) ((MATL(K,I),K=1,6),I=1,NREAC)
  856 FORMAT(6I5)
  859 FORMAT(3I5)
      READ(INPFIL,858)
  858 FORMAT(1X)
      READ(INPFIL,859) ((NRVEC(K,I),K=1,3),I=1,NRINS)
      READ(INPFIL,858)
C**********************************************************************
C
C**********************************************************************
      WRITE(NFIOUT,8030)
      WRITE(NFIOUT,8035)
       TSO=.TRUE.
      NULL=NSPEC+NM+1
      DO NR=1,NREAC
      CALL MXIMAN(NSYMB,MATL(1,NR),NFIOUT,NR,NULL,STOP,TEST,TSO)
      ENDDO    
      WRITE(NFIOUT,8040)
C**********************************************************************
C
C**********************************************************************
 8320 FORMAT(1X,A)
 8040 FORMAT(9X)
 8330 FORMAT(7X,30I2)
 8331 FORMAT(1X,'IRUECK',30I2)
 8030 FORMAT('1',79('*'),/,
     1     ' ****                    ',
     1     'Analysis of the Chemical Flows',
     1     '                     ****'
     1     ,/,1X,79('*'))
 8035 FORMAT(/,3X,'Reaction Mechanism:'/)
 7000 CONTINUE
      RETURN
      END
      SUBROUTINE HOMIN2(TIME,NSPEC,NREAC,NRINS,RAMEC,
     1    RATKIN,UNITS,INPFIL,NFIOUT,NOPT,TEST,STOP)
C***********************************************************************
C***********************************************************************
C
C     **************************************************************
C     *                                                            *
C     *               INPUT 2                                      *
C     *                                                            *
C     *                                                            *
C     **************************************************************
C
C           INPUT-PARAMETERS:
C
C     NS............NUMBER OF SPECIES
C     NR............NUMBER OF REACTIONS
C     NSYMB(2,NS)...SPECIES SYMBOLS
C     XM(NS)........SPECIES MOLAR MASSES
C     IRUECK(NR)....IRUECK=2 FOR REVERSE REACTION, =1 FOR RESPECTIVE
C                   FORWARD REACTION (THE REVERSE REACTION MUST BE
C                   LOCATED DIRECTLY AFTER THE FORWARD REACTION)
C     SRATE(J)      OVER ALL MOLAR RATE OF FORMATION
C     RRATE(J,K)    MOLAR RATE OF FORMATION IN ITS REACTION NO K
C
C***********************************************************************
C     I: STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT REAL(A-H,O-Z)
      LOGICAL STOP,TEST
      DIMENSION RAMEC(*)
      DIMENSION NOPT(*)
      CHARACTER*16 RATKIN
      CHARACTER*12 UNITS
C***********************************************************************
C     INPUT OF DATA
C***********************************************************************
      STOP=.FALSE.
      READ(INPFIL,8100,END=7000) RATKIN,UNITS,TTTT
 8100 FORMAT(A16,A12,10X,E11.4)
      TIME=TTTT
      READ(INPFIL,8110)  (RAMEC(K),K=1,NREAC)
 8110 FORMAT(6(1X,1PE12.5))
      RETURN
 7000 STOP=.TRUE.
      RETURN
C***********************************************************************
C     END OF HOMIN2
C***********************************************************************
      END
      SUBROUTINE HOMCAL(NS,NR,NRINS,NSYMB,
     1     SRATE,RINTC,SRINT,RAMEC,RRATE,RINT,
     1     NSWI,NRWI,IMPINT,IRUECK,
     1     NRVEC,RATKIN,UNITS,INPFIL,NFIOUT,NOPT,TEST)
C***********************************************************************
C***********************************************************************
C
C     **************************************************************
C     *                                                            *
C     *   CALCULATION OF RATES OF FORMATION AND CONSUMPTION        *
C     *                    (2.11.1981)                             *
C     *                                                            *
C     **************************************************************
C
C           INPUT-PARAMETERS:
C
C     NSPEC.........NUMBER OF SPECIES
C     NRREAC........NUMBER OF REACTIONS
C     NSYMB(2,NS)...SPECIES SYMBOLS
C     IRUECK(NR)....IRUECK=2 FOR REVERSE REACTION, =1 FOR RESPECTIVE
C                   FORWARD REACTION (THE REVERSE REACTION MUST BE
C                   LOCATED DIRECTLY AFTER THE FORWARD REACTION)
C     RRATE(NR,NS)..MOLAR RATE OF FORMATION
C
C***********************************************************************
C     I: STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT REAL(A-H,O-Z)
      LOGICAL TEST,IMPORT
      DIMENSION SRATE(NS),RINTC(NS),SRINT(NS)
      DIMENSION RAMEC(NR)
      DIMENSION RRATE(NS,NR),RINT(NS,NR)
      DIMENSION NSWI(NS)
      DIMENSION NRWI(NR),IMPINT(NR),IRUECK(NR)
      DIMENSION NRVEC(3,NRINS)
      DIMENSION NOPT(*)
      CHARACTER(LEN=*) NSYMB(*)
      CHARACTER*12 KIND
      CHARACTER*16 RATKIN
      CHARACTER*12 UNITS
      DATA XLIMS/0.001/,XLIMR/0.003/
C***********************************************************************
C     CALCULATION OF DETAILED  REACTION RATES
C***********************************************************************
      ISYLEN=LEN(NSYMB(1))
      DO 100 J=1,NS
      DO 100 K=1,NR
 100  RRATE(J,K)=0.0
      DO 2010 J=1,NS
 2010 SRATE(J)=0.0D0
      DO 2100 I=1,NRINS
      NRT=NRVEC(2,I)
      NST=NRVEC(1,I)
      NDT=NRVEC(3,I)
      RRATE(NST,NRT)=     RAMEC(NRT)* FLOAT(NDT)
      SRATE(NST)=SRATE(NST)+ RRATE(NST,NRT)
 2100 CONTINUE
C***********************************************************************
C     CALCULATION OF ABSOLUTE AND RELATIVE REACTION RATES
C***********************************************************************
      RCMAX=0.0
      RFMAX=0.0
      DO 315 I=1,NS
  315 NSWI(I)=2
      DO 316 I=1,NR
  316 IMPINT(I)=2
      NSCHA=-1
 1000 NSCHA=-NSCHA
      IF(NSCHA.LT.0) KIND='Consumption '
      IF(NSCHA.GT.0) KIND='Formation   '
      DO 415 I=1,NS
      RSUM=0.0
      DO 410 M=1,NR
      RINT(I,M)=0.0
C-----ADDITION OF REACTION AND REVERSE REACTION
      IF(IRUECK(M).EQ.0)  RMIT = RRATE(I,M)
      IF(IRUECK(M).EQ.1)  RMIT = RRATE(I,M) + RRATE(I,M+1)
      IF(IRUECK(M).EQ.2)  RMIT = 0.0
C-----ADDITION OF EITHER FORMATION OR CONSUMPTION
      IF((NSCHA.LT.0).AND.(RMIT.LE.0.0))  RINT(I,M)=RMIT
      IF((NSCHA.GT.0).AND.(RMIT.GE.0.0))  RINT(I,M)=RMIT
      RSUM=RSUM+RINT(I,M)
  410 CONTINUE
      RINTC(I)=RSUM
  415 CONTINUE
C***********************************************************************
C     NORM RELATIVE REACTION RATES
C***********************************************************************
      DO 500 K=1,NR
      DO 500 I=1,NS
      IF(RINTC(I).EQ.0.0)  RINT(I,K)=99999.
      IF(RINTC(I).EQ.0.0)  GO TO 500
      RINT(I,K)=RINT(I,K)/RINTC(I)
  500 CONTINUE
C***********************************************************************
C     DETERMINATION OF BY-PRODUCTS(FOR FORMATION AND CONSUMPTION
C***********************************************************************
C-----DETERMINE MAXIMUM RATE
      RXMAX=0.0
      DO 505 I=1,NS
      RXMAX=AMAX1(ABS(RINTC(I)),RXMAX)
  505 CONTINUE
      RXMAX= RXMAX*XLIMS
C-----IMPORTANCE =0 IF FORMATION OR CONSUMPTION LT. XLIMS OF MAX.
      DO 510 I=1,NS
      IF(ABS(RINTC(I)).LT.RXMAX)  NSWI(I)=NSWI(I)-1
  510 CONTINUE
  520 CONTINUE
C***********************************************************************
C     DETERMINE IMPORTANT REACTIONS
C***********************************************************************
      DO 1695 M=1,NR
      IF(IRUECK(M).EQ.2)  IMPINT(M)=IMPINT(M-1)
      IF(IRUECK(M).EQ.2)  GO TO 1695
      IMPORT=.FALSE.
      DO 1693 I=1,NS
      IF(ABS(RINTC(I)).LT.RXMAX)  GOTO 1693
      IMPORT= IMPORT .OR.
     1      (      (ABS(RINT(I,M)).GT. XLIMR)
     1       .AND. (ABS(RINT(I,M)).LT.99997.))
 1693 CONTINUE
      IF(.NOT.IMPORT) IMPINT(M)=IMPINT(M)-1
 1695 CONTINUE
C***********************************************************************
C     OUTPUT OF RELATIVE RATES OF REACTION
C***********************************************************************
  660 FORMAT(5X,'Relative Rates of ',A12,'( % )')
  670 FORMAT(24X,'|',20(A4,'|'))
  690 FORMAT(5X,'Reaction ',I4,6X,'|',20(I4,'|'))
  691 FORMAT(5X,'Reaction ',I4,'+',I4,1X,'|',20(I4,'|'))
      WRITE(6,40)
   40 FORMAT(2X)
      WRITE(6,660) KIND
      WRITE(6,40)
      NROW=20
      IF(NOPT(40).EQ.1) NROW=10
      NANF=1
 700  NEND=NANF+NROW-1
      NACT=NEND
      IF(NS.LE.NEND) NACT=NS
      DO II=1,ISYLEN,4
      ICL = MIN(II+3,ISYLEN)
      WRITE(6,670)  (NSYMB(I)(II:ICL),I=NANF,NACT)
      ENDDO
      WRITE(6,40)
      DO 695 M=1,NR
      IF(IRUECK(M).EQ.2)  GO TO 695
      MPL=M+1
      IF(IRUECK(M).EQ.0)  WRITE(6,690)
     1     M,(INT(100.0*RINT(I,M)),I=NANF,NACT)
      IF(IRUECK(M).EQ.1)  WRITE(6,691)
     1        M,MPL,(INT(100.0*RINT(I,M)),I=NANF,NACT)
  695 CONTINUE
      IF(NACT.EQ.NS) GOTO 763
      NANF=NACT+1
      WRITE(NFIOUT,40)
      GOTO 700
  763 CONTINUE
C***********************************************************************
C     OUTPUT OF ABSOLUTE RATES OF REACTION
C***********************************************************************
      WRITE(6,40)
      WRITE(6,2704) KIND,UNITS
      WRITE(6,40)
      WRITE(6,2706) (NSYMB(I),RINTC(I),I=1,NS)
      WRITE(6,40)
 2704 FORMAT(5X,'Absolute Rates of ',A12,1X,A12,1X,':')
 2706 FORMAT(3(4X,A,':',1PE9.2))
C***********************************************************************
C     GO BACK TO COMPUTE RATES OF CONSUMTION
C***********************************************************************
      IF(NSCHA.GT.0) THEN
      DO I=1,NS
        SRINT(I)=RINTC(I)
      ENDDO
        GOTO 1000
      ENDIF
C***********************************************************************
C
C     FINAL OUTPUT
C
C***********************************************************************
C***********************************************************************
C     OUTPUT OF BY-PRODUCTS AND INERT SPECIES
C***********************************************************************
      WRITE(6,40)
  544 FORMAT(4X,'Species ',A,' is inert or ',
     1      'a by-product (',2PF5.3,' % limit)')
      DO 545 I=1,NS
      IF(NSWI(I).EQ.0) WRITE(6,544) NSYMB(I),XLIMS
  545 CONTINUE
C***********************************************************************
C     OUTPUT OF UNIMPORTANT REACTIONS
C***********************************************************************
      MMM=0
      DO 821 M=1,NR
      IF(IMPINT(M).EQ.0) NRWI(MMM+1)=M
      IF(IMPINT(M).EQ.0) MMM=MMM+1
  821 CONTINUE
      WRITE(6,40)
      IF(MMM.EQ.0)  WRITE(6,55) XLIMR
      IF(MMM.GT.0)  WRITE(6,43) XLIMR,(NRWI(M),M=1,MMM)
   43 FORMAT('0',3X,'Reactions that contribute less than ',2PF5.2,' % ',
     1   'to the formation or consumption',/,4X,'of major species:',/,
     1    1(22X,11(I4,2X)))
   55 FORMAT('0',4X,'All reactions contribute more than ',2PF5.3,1X,
     1  '% to the formation or consumption',/,'of major species ')
C***********************************************************************
C     OUTPUT OF REACTIONS IN PARTIAL EQUILIBRIUM
C***********************************************************************
      SSMAL=1.E-14
      PARLIM = 2.0
      DO M=1,NR
        IF(IRUECK(M).EQ.2) THEN
          IF(ABS(RAMEC(M-1)).GE.SSMAL) THEN
            RATIO=ABS(RAMEC(M)/RAMEC(M-1))
            IF(RATIO.LE.PARLIM.AND.RATIO.GE.1.0/PARLIM) THEN
              WRITE(6,810) M-1,-M,RATIO
            ENDIF
          ENDIF
        ENDIF
      ENDDO
  810 FORMAT(4X,'Reactions',I5,' -',I5,2X,
     1     'are in partial equilibrium (r''/r=',F5.3,')')
C***********************************************************************
C     OUTPUT OF SPECIES IN QUASI-STEADY STATE
C***********************************************************************
      SSMAL=1.E-14
      PARLIM = 2.0
      DO 1811 I=1,NS
      RNET=SRINT(I)+RINTC(I)
      DENO=AMAX1(ABS(SRINT(I)),ABS(RINTC(I)))
      IF(DENO.GT.SSMAL) THEN
         RATIO= ABS(RNET) / DENO
         IF(RATIO.LT.0.3) THEN
            WRITE(6,1812) NSYMB(I),RATIO
         ENDIF
      ENDIF
 1811 CONTINUE
 1812 FORMAT(4X,'Species  ',A,
     1     ' is in quasi-steady state  (ratio=',F6.4,')')
C***********************************************************************
C***********************************************************************
C     REGULAR EXIT
C***********************************************************************
      RETURN
C***********************************************************************
C     END OF OUTMEC
C***********************************************************************
      END
      SUBROUTINE MXIMAN(NSYMB,NSTOE,
     1           NFIOUT,NR,NULL,STOP,TEST,TSO)
C***********************************************************************
C
C     **************************************************************
C     *                                                            *
C     *           PRINTING OUT ACTION OF REACTION EQUATIONS        *
C     *                                                            *
C     **************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C     NSYMB(VAR.)  = SPECIES SYMBOLS       (CHARACTER*8)
C     NSTOE(1)     = SPECIES INDICES
C     NFIOUT       = UNIT OF OUTPUT ON PRINTER
C
C     OUTPUT :
C
C     STOP         = .TRUE. FOR NORMAL END, .FALSE. FOR ABNORMAL END
C
C***********************************************************************
C
C
C***********************************************************************
C     STORAGE ORGANIZATION, INITIAL VALUES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL STOP,TEST,TSO
      CHARACTER(LEN=*) NSYMB(*)
      CHARACTER*2 NSIG(6)
      DIMENSION NSTOE(6),ISTOE(6)
      STOP=.TRUE.
C***********************************************************************
C     ASSIGN SYMBOLS
C***********************************************************************
      ISTOE = NSTOE
      DO I = 1,6
         IF(ISTOE(I).EQ.0) ISTOE(I)=NULL
      ENDDO
C***********************************************************************
C     ASSIGN SYMBOLS
C***********************************************************************
      NSIG(1:6)='  '
      IF(NSTOE(2).NE.NULL)  NSIG(1)='+ '
      IF(NSTOE(3).NE.NULL)  NSIG(2)='+ '
      NSIG(3)='->'
      IF(NSTOE(5).NE.NULL)  NSIG(4)='+ '
      IF(NSTOE(6).NE.NULL)  NSIG(5)='+ '
C***********************************************************************
C     CHECK TASK
C***********************************************************************
      WRITE(NFIOUT,811) NR,(NSYMB(NSTOE(II)),NSIG(II),II=1,6)
  811 FORMAT(1X,I4,':',3X,6(A,A2,2X))
  300 CONTINUE
      RETURN
C***********************************************************************
C     END OF -MXIMAN-
C***********************************************************************
      END
