C>    setmat
      SUBROUTINE SETMAT(MSGFIL,IEQ,NEQ,
     1      NCOQ,COQ,LCOQ,NCSQ,CSQ,LCSQ,NXLQ,XLQ,LXLQ,NULQ,ULQ,LULQ,
     1      NS,NCSS,CSS,NCOS,COS,NXLS,XLS,NULS,ULS)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *    Set up Locations and Matrices for the State Space      *    *
C     *                                                           *    *
C     *                U. Maas / April 11th 1991                  *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C***********************************************************************
C
C  Input:
C
C    MSGFIL         : fortran unit for error messages
C
C    IEQ            : kind of equation system
C                       IEQ = 1:  adiabatic, isobaric, closed system
C                       IEQ = 2:  adiabatic, isochoric, closed system
C                       IEQ = 5   isothermal, isobaric, closed system
C                       IEQ = 6   isothermal, isochoric, closed system
C
C    NS             : number of different species in the reaction system
C
C    NCSS           : number of conserved species quantities
C
C    CSS(NS,NCSS)   : vector for conserved species quantities
C                     CSS(*,j) * w/M is the conserved quantity j
C                     CSS(*,j) are e.g. the element vectors
C
C    NCOS           : number of controlling species quantities
C
C    COS(NS,NCSS)   : vector for controling species quantities
C                     COS(*,j) * w/M is the controlling quantity j
C
C    NXLS           : number of domain limits
C
C    XLS(NS,NXLS)   : vector for domain limits
C                     XLS(*,j) * w/M is the controlling quantity j
C
C    note: the input vectors refer to vectors in the composition space
C          the output matrices refer to the whole state space
C
C  Output:
C
C    NEQ            : dimension of the state space
C
C    NCSQ           : number of conserved quantities
C
C    CSQ(LCSQ)      : matrix for conserved quantities
C                     column partition are the vectors
C
C    NCOQ           : number of controlling  quantities
C
C    COQ(LCOQ)      : matrix for controling quantities
C                     multiplication of COQ with the state vector
C                     will give the controlling variables
C
C    NXLQ           : number of domain limiting vectors
C
C    XLQ(LXLQ)      : matrix for domain limiting
C                     multiplication of XLQ with the state vector
C                     will give the variables to be limited
C
C     IEQ = 1
C         for IEQ = 1 (adiabatic, isobaric, closed system) the first
C         entry of the state space is the specific enthalpy, the second
C         the pressure and the 3rd through 2+ns th the specific mole
C         numbers
C
C     IEQ = 2
C         for IEQ = 2 (adiabatic, isochoric, closed system) the first
C         entry of the state space is the specific enthalpy, the second
C         the density and the 3rd through 2+ns th the specific mole
C         numbers
C
C     IEQ = 5
C         for IEQ = 5 (isothermal, isobaric, closed system) the first
C         entry of the state space is the temperature, the second
C         the pressure and the 3rd through 2+ns th the specific mole
C         numbers
C
C     IEQ = 6
C         for IEQ = 6 (isothermal, isochoric, closed system) the first
C         entry of the state space is the temperature, the second
C         the density and the 3rd through 2+ns th the specific mole
C         numbers
C
C***********************************************************************
C***********************************************************************
C     Storage Organization
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION COQ(LCOQ),CSQ(LCSQ),XLQ(LXLQ),ULQ(LULQ)
      DIMENSION COS(NS,NCOS),CSS(NS,NCSS),XLS(NS,NXLS),ULS(NS,NULS)
      DATA ZERO/0.0D0/,ONE/1.0D0/
C***********************************************************************
C     INITIALIZE
C***********************************************************************
      IF(IEQ.EQ.1.OR.IEQ.EQ.2.OR.IEQ.EQ.5.OR.IEQ.EQ.6) THEN
        NEQ = NS + 2
        NH = 1
        NP = 2
        NWS= 2
        NW = 3
      ELSE
        GOTO 900
      ENDIF
C***********************************************************************
C
C     MATRIX OF VECTORS CORRESPONDING TO CONSERVED QUANTITIES (COL. VEC.
C
C***********************************************************************
      NCSQ = NCSS + 2
C***********************************************************************
C     SET MATRIX TO ZERO
C***********************************************************************
      NEN = NCSQ * NEQ
      IF(NEN.GT.LCSQ) GOTO 910
      CSQ(1:NEN) = ZERO
C***********************************************************************
C     FIRST TWO COLUMNS: ENTHALPY AND PRESSURE OR DENSITY
C***********************************************************************
      CSQ(NH+(1-1)*NEQ) = ONE
      CSQ(NP+(2-1)*NEQ) = ONE
C***********************************************************************
C     COLUMNS 3 THROUGH 2 + NCSS: CONSERVED SPECIES QUANTITIES (ELEMENTS
C***********************************************************************
      IF(NCSS.GT.0) THEN
      DO 20 J=1,NCSS
      DO 20 I=1,NS
      CSQ(NWS+I+(2+J-1)*NEQ) = CSS(I,J)
   20 CONTINUE
      ENDIF
C***********************************************************************
C
C     MATRIX OF CONTROLING VECTORS
C
C***********************************************************************
      NCOQ = NCOS + 2
C***********************************************************************
C     SET MATRIX TO ZERO
C***********************************************************************
      NEN = NCOQ * NEQ
      IF(NEN.GT.LCOQ) GOTO 920
      DO 30 I=1,NEN
      COQ(I) = ZERO
   30 CONTINUE
C***********************************************************************
C     FIRST TWO ROWS: ENTHALPY AND PRESSURE
C***********************************************************************
      COQ(1+(NH-1)*NCOQ) = ONE
      COQ(2+(NP-1)*NCOQ) = ONE
C***********************************************************************
C     ROWS 2+NCSS+1 THROUGH NCOQ: CONTROLLING VECTORS
C***********************************************************************
      IF(NCOS.GT.0) THEN
      DO 40 J=1,NCOS
      DO 40 I=1,NS
      COQ(2+J+(NWS+I-1)*NCOQ) = COS(I,J)
   40 CONTINUE
      ENDIF
C***********************************************************************
C
C     MATRIX OF FOR DOMAIN LIMITS
C
C***********************************************************************
      NXLQ = NXLS + 2
C***********************************************************************
C     SET MATRIX TO ZERO
C***********************************************************************
      NEN = NXLQ * NEQ
      IF(NEN.GT.LXLQ) GOTO 930
      DO 50 I=1,NEN
      XLQ(I) = ZERO
   50 CONTINUE
C***********************************************************************
C     FIRST TWO ROWS: ENTHALPY AND PRESSURE
C***********************************************************************
      XLQ(1+(NH-1)*NXLQ) = ONE
      XLQ(2+(NP-1)*NXLQ) = ONE
C***********************************************************************
C     ROWS 2+NCSS+1 THROUGH NXLQ: CONTROLLING VECTORS
C***********************************************************************
      IF(NXLS.GT.0) THEN
      DO 60 J=1,NXLS
      DO 60 I=1,NS
      XLQ(2+J+(NWS+I-1)*NXLQ) = XLS(I,J)
   60 CONTINUE
      ENDIF
C***********************************************************************
C
C     MATRIX OF FOR DOMAIN LIMITS  for update
C
C***********************************************************************
      NULQ = NULS + 2
C***********************************************************************
C     SET MATRIX TO ZERO
C***********************************************************************
      NEN = NULQ * NEQ
      IF(NEN.GT.LULQ) GOTO 930
      ULQ(1:NEN) = ZERO
C***********************************************************************
C     FIRST TWO ROWS: ENTHALPY AND PRESSURE
C***********************************************************************
      ULQ(1+(NH-1)*NULQ) =  ONE
      ULQ(2+(NP-1)*NULQ) =  ONE
C***********************************************************************
C
C***********************************************************************
      IF(NULS.GT.0) THEN
      DO 70 J=1,NULS
      DO 70 I=1,NS
      ULQ(2+J+(NWS+I-1)*NULQ) = ULS(I,J)
   70 CONTINUE
      ENDIF
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
  900 CONTINUE
      WRITE(MSGFIL,901)
  901 FORMAT(' ','Program not able to handle equation system IEQ=',I3)
      GOTO 999
  910 CONTINUE
      WRITE(MSGFIL,911) NEN,LCSQ
  911 FORMAT(' ','Supplied array for matrix of conserved quantities',
     1           ' not big enough, ',I4,' > ',I4)
      GOTO 999
  930 CONTINUE
      WRITE(MSGFIL,931) NEN,LXLQ
  931 FORMAT(' ','Supplied array for matrix for domain control',
     1           ' not big enough, ',I4,' > ',I4)
      GOTO 999
  920 CONTINUE
      WRITE(MSGFIL,921) NEN,LCOQ
  921 FORMAT(' ','Supplied array for matrix of controlling  quantities',
     1           ' not big enough, ',I4,' > ',I4)
      GOTO 999
  999 CONTINUE
      WRITE(MSGFIL,998)
  998 FORMAT(3(/,' +++++ Error in -SETMAT- +++++'))
      RETURN
C***********************************************************************
C     END OF SETMAT
C***********************************************************************
      END
      SUBROUTINE ORBAS(N,NC,LDC,CM,LDQ,QM,LRW,RW,LIW,IW,INFO,IERR)
C***********************************************************************
C                                                                      *
C     ************************************************************     *
C     *                                                          *     *
C     *    Compute an Orthogonal Basis or Orthogonal Complement  *     *
C     *                                                          *     *
C     *                U. Maas / April 11th 1991                 *     *
C     *                                                          *     *
C     *              Last change:   4/11/1991 U.Maas             *     *
C     *                                                          *     *
C     ************************************************************     *
C                                                                      *
C***********************************************************************
C
C   Input:
C
C    N           : vector length
C
C    NC          : number of known vectors
C
C    LDC         : leading dimension of array CM
C
C    CM(LDC,1)  : matrix of known vectors
C
C    LDQ         : leading dimension of array QM
C
C    LRW         : length of real work array (min.: (NC+1) * N
C
C    LIW         : length of integer work array (min.: 1)
C
C    INFO        : task
C
C   Output:
C
C    IERR        : error flag, ierr = 0 for regular dtermination
C                              ierr = 1 if COV are lin. dependent
C                              ierr = 7 if LRW is too small
C                              ierr = 8 if LIW is too small
C
C    QM(LDQ,1)
C
C                  if INFO > 0 then CM are assumed to be column vectors
C
C                  INFO =-2: the rows of 1,...,NC of QM contain the
C                            row vectors CM, the rows NC+1,...,N
C                            contain the vectors of the orthogonal
C                            complement
C
C                  INFO =-1: the rows of QM contain the vectors
C                            of the orthogonal bais
C
C                  INFO = 1: the columns of QM contain the vectors
C                            of the orthogonal bais
C
C                  INFO = 2: the columns of 1,...,NC of QM contain the
C                            column vectors CM, the columns NC+1,...,N
C                            contain the vectors of the orthogonal
C                            complement
C
C   Work arrays:
C
C      IW(LIW),RW(LRW)
C
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL TEST
      DIMENSION IW(LIW),RW(LRW)
      DIMENSION CM(LDC,*),QM(LDQ,*)
C---- DUMMY ARRAYS
      DIMENSION RD1(1),RD2(1),RD3(1),RD4(1),ID1(1)
C***********************************************************************
C     Check work space
C***********************************************************************
      IERR = 0
      NEEDR = N * NC + N
      NEEDI = 1
      IF(NEEDR.GT.LRW) GOTO 910
      IF(NEEDI.GT.LIW) GOTO 920
C***********************************************************************
C     Store first NC vectors in RW
C***********************************************************************
      DO 10 I=1,NC
      IF(INFO.GT.0) THEN
        CALL DCOPY(N,CM(1,I),1,RW(1+(I-1)*N),1)
      ELSE
        CALL DCOPY(N,CM(I,1),LDC,RW(1+(I-1)*N),1)
      ENDIF
   10 CONTINUE
      IFREE = 1+NC*N
C***********************************************************************
C     perfom qr decomposition and calculate orthogonal basis
C***********************************************************************
C---- PERFORM QR DECOMPOSITION OF THE N * NC MATRIX
      JOB = 0
      CALL DQRDC(RW,N,N,NC,RW(IFREE),ID1,RD1,JOB)
C***********************************************************************
C     calculate the orthogonal basis (rather unelegant, expensive way)
C***********************************************************************
C---- set up unit vectors
      DO 190 I = 1,N
      DO 110 J = 1,N
        QM(J,I) = 0.D0
  110 CONTINUE
        QM(I,I) =1.0D0
C---- compute q
      INFOJ = 0
      CALL DQRSL(RW(1),N,N,NC,RW(IFREE),
     1    QM(1,I),QM(1,I),RD1,RD2,RD3,RD4,10000,INFOJ)
  190 CONTINUE
      IF(INFOJ.NE.0) GOTO 930
      IF(INFO.LT.0)  CALL DTRAD(N,LDQ,QM)
C***********************************************************************
C     store nc vectors cm in matrix
C***********************************************************************
      IF(IABS(INFO).EQ.2) THEN
      DO 210 I=1,NC
      IF(INFO.GT.0) THEN
        CALL DCOPY(N,CM(1,I),1,QM(1,I),1)
      ELSE
        CALL DCOPY(N,CM(I,1),LDC,QM(I,1),LDQ)
      ENDIF
  210 CONTINUE
      ENDIF
C***********************************************************************
C     check orthogonality
C***********************************************************************
      TEST=.FALSE.
      IF(TEST) THEN
C---- check orthogonality with respect to vectors cm
      DO 320 I=1,N
      DO 310 J=1,NC
      IF(INFO.GT.0) CHE=DDOT(N,CM(1,J),1,QM(1,I),1)
      IF(INFO.LT.0) CHE=DDOT(N,CM(J,1),LDC,QM(I,1),LDQ)
      WRITE(6,*) ' +++++ ' , I , J , CHE
  310 CONTINUE
  320 CONTINUE
C---- check overall orthogonality
      DO 330 I=1,N
      DO 330 J=1,N
      IF(INFO.GT.0) CHE=DDOT(N,QM(1,I),1,QM(1,J),1)
      IF(INFO.LT.0) CHE=DDOT(N,QM(I,1),LDQ,QM(J,1),LDQ)
      WRITE(6,*) ' ALL   ' , I , J , CHE
  330 CONTINUE
      ENDIF
C***********************************************************************
C     Regular exit
C***********************************************************************
      RETURN
C***********************************************************************
C     error exits
C***********************************************************************
  910 CONTINUE
      IERR = 7
      RETURN
  920 CONTINUE
      IERR = 8
      RETURN
  930 CONTINUE
      IERR = 1
      RETURN
C***********************************************************************
C     end of -ORBAS-
C***********************************************************************
      END
      SUBROUTINE DTRAD(N,LDM,A)
C***********************************************************************
C                                                                      *
C     ************************************************************     *
C     *                                                          *     *
C     *        Compute Transpose of a Quadratic Matrix           *     *
C     *                                                          *     *
C     ************************************************************     *
C                                                                      *
C***********************************************************************
C
C    Input:
C
C        N        : dimension of the matrix
C        LDM      : leading dimension of the matrix (for convenience)
C        A(LDM,N) : matrix
C
C    Output:
C
C        A(LDM,N) : transpose of A(LDM,N)
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION A(LDM,N)
C***********************************************************************
C     form transpose
C***********************************************************************
      DO 20 I=1,N
      DO 10 J=1,I
      HELP   = A(I,J)
      A(I,J) = A(J,I)
      A(J,I) = HELP
   10 CONTINUE
   20 CONTINUE
      RETURN
C***********************************************************************
C     end of -DTRAD-
C***********************************************************************
      END
      SUBROUTINE PERMAT(INFO,N,NP,A,B)
C***********************************************************************
C
C
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION A(N,N),B(N,N)
C***********************************************************************
C     INITIALIZE
C***********************************************************************
      NF = N - NP
C***********************************************************************
C     ROW PERMUTATION
C***********************************************************************
      IF(INFO.EQ.1) THEN
      DO 130 I=1,NF
      DO 130 J=1,N
      B(I,J) = A(I+NP,J)
  130 CONTINUE
      DO 140 I=1,NP
      DO 140 J=1,N
      B(I+NF,J) = A(I,J)
  140 CONTINUE
      ENDIF
C***********************************************************************
C     COLUMN PERMUTATION
C***********************************************************************
      IF(INFO.EQ.2) THEN
      DO 210 J=1,NF
      DO 210 I=1,N
      B(I,J) = A(I,J+NP)
  210 CONTINUE
      DO 220 J=1,NP
      DO 220 I=1,N
      B(I,J+NF) = A(I,J)
  220 CONTINUE
      ENDIF
C***********************************************************************
C     ROW AND COLUMN PERMUTATION
C***********************************************************************
      IF(INFO.EQ.3) THEN
      DO 310 J=1,NF
      DO 310 I=1,NF
      B(I,J) = A(I+NP,J+NP)
  310 CONTINUE
      DO 320 J=1,NP
      DO 320 I=1,NP
      B(I+NF,J+NF) = A(I,J)
  320 CONTINUE
      DO 330 J=1,NP
      DO 330 I=1,NF
      B(I,J+NF) = A(I+NP,J)
  330 CONTINUE
      DO 340 J=1,NF
      DO 340 I=1,NP
      B(I+NF,J) = A(I,J+NP)
  340 CONTINUE
      ENDIF
C***********************************************************************
C     REGULAR EXIT
C***********************************************************************
      RETURN
C***********************************************************************
C     END OF -PERMAT-
C***********************************************************************
      END
      SUBROUTINE PROMAT(INFO,N,NC,C,AR,AC,NDP,P,LIW,IW,LRW,RW,MSGFIL)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *    Set up Projection Matrix onto the Manifold             *    *
C     *                                                           *    *
C     *                U. Maas / November 4th 1991                *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C
C  Input:
C
C    N              : dimension of the state space
C
C    NC             : number of parameters
C
C    C(NC,N)        : transformation matrix State space ---> parameters
C
C    AR(N,N)        : matrix of invariant subspace
C                     row partition are the vectors
C
C    AC(N,N)        : basis of invariant subspace
C                     column partition are the vectors
C
C    MSGFIL         : fortran unit for error messages
C
C  Output:
C
C    P(NDP,N)       : projection matrix
C                                          I 0
C                             P = C * AC *       * AR  for INFO = 0
C                                          0 0
C
C                                          I 0
C                             P =     AC *       * AR  else
C                                          0 0
C                     where I is a NC*NC idendity matrix
C
C
C     Work arrays:
C
C       RW(LRW), IW(LIW)   real*8 and integer work space (checked)
C
C***********************************************************************
C***********************************************************************
C     STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      DIMENSION AR(N,N),AC(N,N),C(NC,N),P(NDP,N)
      DIMENSION IW(LIW),RW(LRW)
      DATA ZERO/0.0D0/,ONE/1.0D0/
C**********************************************************************C
C     INFO = 0
C**********************************************************************C
      IF(INFO.EQ.0) THEN
         NRW = N * NC
         IF(LRW.LT.NRW) GOTO 910
         NIW = 1
         IF(LIW.LT.NIW) GOTO 920
         IF(NDP.NE.NC)  GOTO 930
C***********************************************************************
C     Compute projection matrix P
C***********************************************************************
         CALL DGEMM('N','N',NC,N,N,ONE,C,NC,AC,N,ZERO,RW,NC)
         CALL DGEMM('N','N',NC,N,NC,ONE,RW,NC,AR,N,ZERO,P,NC)
C**********************************************************************C
C     INFO = 1
C**********************************************************************C
      ELSE
         IF(NDP.NE.N) GOTO 940
         CALL DGEMM('N','N',N,N,NC,ONE,AC,N,AR,N,ZERO,P,N)
      ENDIF
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
  910 CONTINUE
      IERR = 1
      GOTO 999
  920 CONTINUE
      IERR = 2
      GOTO 999
  930 CONTINUE
      IERR = 3
      GOTO 999
  940 CONTINUE
      IERR = 4
      GOTO 999
  999 WRITE(MSGFIL,998) IERR
  998 FORMAT(' ','  Error in -PROMAT-, IERR=',I3)
      STOP
C***********************************************************************
C     END OF -PROMAT-
C***********************************************************************
      END
      SUBROUTINE INILDM(NSPEC,NHPW,IEQ,
     1    NC,CMAT,LCMAT,NCS,EMAT,LEMAT,NLIM,XLIM,LXLIM,
     1    NULIM,UXLIM,LUXLIM,
     1    NE,XRVE,NGVE,XGVE,NLS,XLS,NULS,UXLS,
     1    IFEGV,LIP,IPAR,LRP,RPAR,
     1    LIW,IW,LRW,RW,NFILE6)
C***********************************************************************
C
C     SET UP VARIABLES FOR EIGENVECTOR ANALYSIS
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'homdim.f'
      DIMENSION CMAT(*),EMAT(*),XLIM(*),UXLIM(*),
     1          XLS(NSPEC,NLS),XRVE(NSPEC,NE),XGVE(NSPEC,NGVE),
     1          IPAR(*),RPAR(*),UXLS(NSPEC,NULS)
      DIMENSION IW(LIW),RW(LRW)
C***********************************************************************
C
C     Call SETMAT
C
C***********************************************************************
      CALL SETMAT(NFILE6,IEQ,NHPW,NC,CMAT,LCMAT,NCS,EMAT,LEMAT,
     1      NLIM,XLIM,LXLIM,NULIM,UXLIM,LUXLIM,NSPEC,NE,XRVE,NGVE,XGVE,
     1      NLS,XLS,NULS,UXLS)
      IPAR(2)  = NHPW
      IPAR(11) = NC
C---- three different arrays are allowed in cvar
      INEXT= IFEGV
      IF(INEXT-1.GT.LRP) GOTO 935
C***********************************************************************
C     CALCULATE TRANSFORMATION MATRIX (H,P,WI/MI) -> (H,P,EI,RI)
C***********************************************************************
C---- Will be done in subroutine AUTPAR (starou.f)
C***********************************************************************
C
C     CHAPTER VII: REGULAR EXIT
C
C***********************************************************************
C***********************************************************************
      RETURN
C***********************************************************************
C***********************************************************************
C
C     CHAPTER V: ERROR EXITS
C
C***********************************************************************
  935 CONTINUE
      WRITE(NFILE6,936) INEXT-1,LRP
  936 FORMAT(' ','  NEEDED LENGTH FOR ARRAY RPAR (',I8,') EXCEEDS '
     1          ,'allocated space (',I8,')')
      STOP
  999 CONTINUE
      WRITE(NFILE6,998)
      WRITE(NFILE6,998)
      WRITE(NFILE6,998)
  998 FORMAT(' ','   ++++++      ERROR IN HORIN1      ++++++   ')
      STOP
C***********************************************************************
C     END OF -HORIN1-                                                  *
C***********************************************************************
      END
      SUBROUTINE GENPMA(INPF,NS,NE,XRVE,NGVE,XGVE,ECV,NGVMAX,
     1     NLIM,XLIM,ELIM,NULIM,UXLIM,UELIM,
     1     NGRV,GRVE,GRVAL,NGSV,GSVE,GSVAL,NLIMAX,IERR)
C***********************************************************************
C
C     ***********************************************************
C     *                                                         *
C     * Input of parameterization information for eigenvector    *
C     * analysis                                                *
C     *                                                         *
C     ***********************************************************
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      include 'homdim.f'
      DIMENSION XRVE(NS,NE),XGVE(NS,NGVMAX),ECV(2,NGVMAX)
      DIMENSION XLIM(NS,NLIMAX),ELIM(2,NLIMAX)
      DIMENSION UXLIM(NS,NLIMAX),UELIM(2,NLIMAX)
      DIMENSION GRVE(NS+2,NLIMAX),GRVAL(2,NLIMAX)
      DIMENSION GSVE(NS+2,NLIMAX),GSVAL(2,NLIMAX)
C***********************************************************************
C     initialize
C***********************************************************************
      IERR = 0
      NEQ= NS+2
C***********************************************************************
C     read header
C***********************************************************************
      READ(INPF,81,END=91,ERR=91)
C***********************************************************************
C     read XRVE (matrix with element vectors)
C***********************************************************************
      READ(INPF,82,END=91,ERR=91) ((XRVE(I,J),I=1,NS),J=1,NE)
C***********************************************************************
C     read XGVE (matrix with paramatrization for the first cell)
C***********************************************************************
      READ(INPF,83,END=91,ERR=91) NGVE
      READ(INPF,82,END=91,ERR=91)((XGVE(I,J),I=1,NS),J=1,NGVE)
C***********************************************************************
C     read ECV
C***********************************************************************
      READ(INPF,82,END=91,ERR=91)((ECV(I,J) ,I=1,2),    J=1,NGVE)
C***********************************************************************
C     read XGVE (matrix with paramatrization vectors)
C***********************************************************************
      READ(INPF,83,END=91,ERR=91) NLIM
      IF(NLIM.GT.0) THEN
        READ(INPF,82,END=91,ERR=91)((XLIM(I,J),I=1,NS),J=1,NLIM)
        READ(INPF,82,END=91,ERR=91)((ELIM(I,J) ,I=1,2),    J=1,NLIM)
      ENDIF
C***********************************************************************
C
C***********************************************************************
      READ(INPF,83,END=91,ERR=91) NULIM
      IF(NULIM.GT.0) THEN
      READ(INPF,82,END=91,ERR=91)((UXLIM(I,J),I=1,NS),J=1,NULIM)
      READ(INPF,82,END=91,ERR=91)((UELIM(I,J) ,I=1,2),    J=1,NULIM)
      ENDIF
C***********************************************************************
C
C***********************************************************************
      READ(INPF,83,END=91,ERR=91) NGRV
      IF(NGRV.GT.0) THEN
C     READ(INPF,82,END=91,ERR=91)((GRVE(2+I,J),I=1,NS),J=1,NGRV)
C     READ(INPF,82,END=91,ERR=91)((GRVAL(I,J) ,I=1,2),    J=1,NGRV)
C     GRVE(1:2,1:NGRV) = 0 ! BZ bug, changed NGSV to NGRV
C     GRVE(1:2,1:NGRV) = 0 ! BZ bug, changed NGSV to NGRV
      READ(INPF,82,END=91,ERR=91)((GRVE(I,J),I=1,NEQ),J=1,NGRV)
      READ(INPF,82,END=91,ERR=91)((GRVAL(I,J) ,I=1,2),    J=1,NGRV)
      ENDIF
C***********************************************************************
C
C***********************************************************************
      READ(INPF,83,END=91,ERR=91) NGSV
      IF(NGSV.GT.0) THEN
C     READ(INPF,82,END=91,ERR=91)((GSVE(2+I,J),I=1,NS),J=1,NGSV)
C     READ(INPF,82,END=91,ERR=91)((GSVAL(I,J) ,I=1,2),    J=1,NGSV)
C     GSVE(1:2,1:NGSV) = 0
      READ(INPF,82,END=91,ERR=91)((GSVE(I,J),I=1,NEQ),J=1,NGSV)
      READ(INPF,82,END=91,ERR=91)((GSVAL(I,J) ,I=1,2),    J=1,NGSV)
      ENDIF
C***********************************************************************
C     regular exit
C***********************************************************************
      RETURN
C***********************************************************************
C     error exits
C***********************************************************************
   91 CONTINUE
      IERR = 1
   92 CONTINUE
      IERR = 2
      RETURN
  999 CONTINUE
      STOP
C***********************************************************************
C     format statements
C***********************************************************************
   81 FORMAT(1X)
   82 FORMAT(5(1PE16.9))
   83 FORMAT(I3)
   84 FORMAT(20A5)
   85 FORMAT(I2)
C***********************************************************************
C     end
C***********************************************************************
      END
      SUBROUTINE SPAKOE(NS,NR,MATLL,MATLR,KOEF,MSGFIL)
C***********************************************************************
C     CALCULATE STOICHIOMETRIC MATRIX FOR ORIGINAL VECTORS
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL TEST
      DIMENSION KOEF(NS,NR),MATLL(3,NR),MATLR(3,NR)
C***********************************************************************
C     set coefficients to zero
C***********************************************************************
      DO 10 I=1,NS
      DO 10 J=1,NR
      KOEF(I,J)=0
  10  CONTINUE
C***********************************************************************
C     CALCULATE STOICHIOMETRIC MATRIX FOR ORIGINAL VECTORS
C***********************************************************************
      DO 20 J=1,NR
      DO 20 K=1,3
      ISL=MATLL(K,J)
      ISR=MATLR(K,J)
      IF(ISL.GT.0.AND.ISL.LE.NS) KOEF(ISL,J)=KOEF(ISL,J)-1
      IF(ISR.GT.0.AND.ISR.LE.NS) KOEF(ISR,J)=KOEF(ISR,J)+1
   20 CONTINUE
C***********************************************************************
C
C***********************************************************************
      TEST = .FALSE.
      IF(TEST) THEN
      DO 60 J=1,NR
      WRITE(MSGFIL,*) (KOEF(I,J),I=1,NS)
   60 CONTINUE
      ENDIF
      RETURN
C***********************************************************************
C
C***********************************************************************
      END
      SUBROUTINE INISEN(NEQ,NSENS,NISENS,NRSENS,NPSENS,
     1     IISENS,IRSENS,IPSENS,ISENSI,
     1     NSPEC,KONISE,NREAC,KONRSE,IREV,KONOSE,NSC,KONSPE)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION ISENSI(*),IREV(NREAC),KONSPE(*)
C***********************************************************************
C     II.8 SET UP POINTERS FOR SENSITIVITY ANALYSIS
C***********************************************************************
      IISENS=1
      NISENS=0
C     write(6,*) 'p',iisens,irsens,ipsens,nisens,nrsens,npsens
C***********************************************************************
C     Sensitivity with respect to initial concentrations
C***********************************************************************
      IF(KONISE.EQ.2) THEN
      NISENS=NEQ
      DO 110 I=1,NISENS
      ISENSI(IISENS-1+I)=I
  110 CONTINUE
      ELSE IF(KONISE.EQ.1) THEN
      NISENS = NSC
      DO  I=1,NSC
      ISENSI(IISENS-1+I)= KONSPE(I)
      ENDDO
      ELSE
      ENDIF
C***********************************************************************
C     Sensitivity with respect to rate coefficients
C***********************************************************************
      IRSENS = IISENS+ NISENS
      NRSENS = 0
      DO 130 I=1,NREAC
                         III =   0
        IF(KONRSE.EQ.1)  III =   IREV(I)
        IF(KONRSE.EQ.2)  III = - 1
        IREV(I)=IABS(IREV(I))-1
        IF(III.LT.0) THEN
          NRSENS=NRSENS+1
          ISENSI(IRSENS-1+NRSENS)=I
        ENDIF
  130 CONTINUE
C***********************************************************************
C     Sensitivity with respect to other parameters
C***********************************************************************
      NPSENS=0
      IPSENS=IRSENS+NRSENS
C***********************************************************************
C     compute NSENS
C***********************************************************************
      NSENS=NRSENS+NISENS+NPSENS
C***********************************************************************
C     Regular exit
C***********************************************************************
      RETURN
      END
