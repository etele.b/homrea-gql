subroutine analysis(symprp, rparin, iparin)
    use options
    implicit none
    integer, intent(in) :: iparin(*)
    double precision, intent(in) :: rparin(*)
    character(len=8), intent(in) :: symprp(*)
    character(len=20) :: fname
    fname = 'ana.nml'
    call init(fname, 5555, iparin, rparin, symprp)
    call amain()
end subroutine analysis
!
! ###               * Start of analysis part *                              ###
!
subroutine amain
    ! Incomming variables:
    use atools
    implicit none
    type(tra) :: tdet, tred, somet
    integer :: ns, ierr
    double precision :: q(NEQ, NEQ), qi(NEQ, NEQ), temp(NEQ), test
    character(len=20) :: filename
    ! Read detailed trajectory
    call tdet%init(trim(G_detfile))

    ! Calculate properties of detailed trajectory
    call tdet%prop

    ! Print tecplot version of detailed trajectory if required
    write(*,*) IO_dettec, len(IO_dettec)
    if (len(IO_dettec) > 0) then
        call wtra(0,'000','det',tdet, [1, 0])
    end if
    ! Check if to do analysis
    if (A_pointgen > 0) call global_analysis(tdet)

    ! If pointgen not negative, integrate
    if (A_pointgen == 0) then
        ! Get NS
        if (I_ns == -1) then
            print*, 'NS = '
            read(*,'(I3)') ns
        else
            ns = I_ns
        end if
        ! Read basis
        write(filename, '(a,i3.3,a)') 'gql_#n', ns, '.bin'
        call rmat(2, filename, q, qi) 
        ! Integrate
        call integrate(ns, q, qi, tdet%yi, tdet%yeq, tred, ierr)
        if (ierr > 4) write(IO_errf, *) 'ERROR: Integration failed!'
        !Print trajectory if option is enabled
    end if

    ! Generate output files for local analysis

    if (LA_on > 0) then
        call somet%init(LA_traj)
        call locana_output(somet)
    end if

    return
end subroutine amain

subroutine global_analysis(tdet)
    use atools
    implicit none
    type(tra), intent(inout) :: tdet

    ! Local variables
    integer :: io, ic, ir, pos, minns, ierrs(NEQ-NCON), &
        istat, ierr
    double precision :: smfmax(NEQ), rating_ref(NEQ-NCON), rand(NEQ) 

    istat = 999
    open(istat,file='live', status='new', iostat = io)
    if (io > 0) then
        print*, 'Previous calculation was forcefully terminated. &
            You might want to clean the working directory and &
            restart'
        read(*,*)
    end if
    close(999)

    ic = 0
    ir = 0

    ! Calculate propoerties of detailed trajectory
    call output_init
    ! Initialize random point generator
    call genpoi_init(pos, smfmax, tdet)
  
    minns = 100
    rating_ref = 9.d99
    allpoints: do while(ir < A_nr)
        ! Check if the live file exists, if not, exit loop
        open(istat, file = 'live', status = 'old', iostat = io)
        if (io > 0) exit allpoints
        close(istat)
        ! Number of points tried
        ic = ic + 1
        ! Error array
        ierrs = 0
        ! Get a random point
        call genpoi(pos, smfmax, tdet, rand)
        ! Calculate the basis
        call gqlbas(rand, tdet, rating_ref, minns, ierrs)
    
        ! Check if at least one dimension could be integrated
        if (minval(ierrs(A_ns(1):A_ns(2))) < 5) then
            ir = ir + 1
        end if
        
        ! DO progress statistics
        call stat_prog(istat, ic, ir, minns, rating_ref, ierr)
        if (ierr > 0) exit allpoints
    end do allpoints

    call stat_fin(ic, ir, rating_ref, minns)
    return
end subroutine global_analysis

subroutine locana_output(t)
    use atools
    type(tra), intent(in) :: t
    integer :: i, stoma(NSPEC, NREAC)
    double precision :: smf(NEQ), f(NEQ), jac(NEQ,NEQ), gr(NREAC), ssp(5)
    character(len=4) :: fpref(8)
    character(len=22) :: snf(4)

    ! Write filename prefixes
    fpref(1) = 'LHS_'
    fpref(2) = 'RHS_'
    fpref(3) = 'JAC_'
    fpref(4) = 'SSP_'
    fpref(5) = 'GGR_'
    fpref(6) = 'STO_'
    fpref(7) = 'REV_'
    fpref(8) = 'MAT_'

    ! Write format specifiers
    snf = ''
    write(snf(1),'(a,i4,a)') '(', NREAC, '(1pe14.6e3,1x))' 
    write(snf(2),'(a,i4,a)') '(', NEQ+1, '(1pe14.6e3,1x))'
    write(snf(3),'(a,i4,a)') '(', NEQ*NEQ, '(1pe14.6e3,1x))'
    write(snf(4),'(a,i4,a)') '(', NREAC, '(i4))'

    ! Open files
    do i = 1, 8
       open( unit=8000+i, file=fpref(i)//trim(LA_short), status='replace', &
           access='sequential')
    end do

    do i = 1, t%nvert
        call trares(3, t%d(1:NEQ, i), f, jac = jac, ssp = ssp, g = gr, &
            ierr = ierr)
        write(8001,snf(2)) t%time(i), smf
        write(8002,snf(2)) t%time(i), f
        write(8003,snf(3)) jac
        write(8004,'(5(1pe14.6e3,1x))') ssp
        write(8005,snf(1)) gr
    end do

    call ret_stoma(stoma)
    ! Write stochiometric matrix
    do i = 1, n - 2
        write(8006,snf(4)) stoma(i,:)
    end do
       
    ! Write reverse reactions
    write(8007,snf(4)) ireac(NNRVEC+3*NRINS:NNNRVEC+3*NRINS+NREAC)

    ! Write reaction coefficients
    do i = 1, nre
        write(8008,'(6(i4))') ireac(nmatll+3*(i-1):nmatll+3*(i-1)+2), &
            ireac(nmatlr+3*(i-1):nmatlr+3*(i-1)+1)
    end do


    do i = 1, 8
        close(8000+i)
    end do


    return


end subroutine locana_output


subroutine ana_reac

end subroutine ana_reac
