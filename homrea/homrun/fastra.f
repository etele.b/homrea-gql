      SUBROUTINE INTFAS( NEQ,NF,SMF,TEND,Q,QI,
     1     MSGFIL,IERR,RPAR,IPAR,NVERT )
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      EXTERNAL FASRIG,FASLEF,DUMJAC
C***********************************************************************
C     arrays for solvers
C***********************************************************************
      DIMENSION RTOL(1),ATOL(1),RTOLS(1),ATOLS(1),RPAR(*),IPAR(*)
      DIMENSION IJOB(20)
      DOUBLE PRECISION, DIMENSION(NEQ) :: SMF,SMFP
      DOUBLE PRECISION, DIMENSION(NEQ*NEQ) :: Q , QI
C***********************************************************************
C     STORAGE ARRAY FOR THE TABLE
C***********************************************************************
      DIMENSION RW(25000),IW(5000)
C***********************************************************************
      COMMON/BHEL/QEVB(MNEQU*MNEQU),QEVBI(MNEQU*MNEQU)
      COMMON/LEFCOM/NLNEQ,NLROW
C***********************************************************************
C     STORAGE ARRAY FOR TRAJECTORIES
C***********************************************************************
      DATA LRW /25000/,LIW /5000/
      DATA RTOL(1) /1.0D-3/,ATOL(1) /1.0D-10/
      DATA RTOLS(1)/1.0D-2/,ATOLS(1)/1.0D-9/
C***********************************************************************
C     USEFUL CONSTANTS
C***********************************************************************
      DATA ZERO/0.0D0/,ONE/1.0D0/
C***********************************************************************
C
C     loop over vertices
C
C***********************************************************************
C**********************************************************************
C     store the properties at t=0
C**********************************************************************
C***********************************************************************
C     SET INTEGRATION CONTROL PARAMETERS
C***********************************************************************
      ICALL    = 0
      IJOB(1:20) = 0
      NZV = 0
      NZC = NEQ
      IJOB(1)=1
      IJOB(7)=0
      H    = 1.D-10
      HMAX = 1.D+10
C-Sl- Common Block Defintion--------------------------------------------
      QEVB(1:NEQ*NEQ) = Q(1:NEQ*NEQ)
      QEVBI(1:NEQ*NEQ) = QI(1:NEQ*NEQ)
      NLNEQ = NEQ
      NLROW = NEQ - NF
C***********************************************************************
C
C***********************************************************************
C
C-Sl- Output organization for fast subsystem solution
      NFIL121 = 121
C      REWIND NFIL121
C-Sl- End of opening of the file
C
      IR = 0
      TIME = ZERO
      DO WHILE (TIME.LT.TEND)
C       WRITE(*,*) ' DIMENSIONS : ',NEQ,NZC,NZV,LIW,LRW, IJOB(7)
       CALL XLIMEX(NEQ,NZC,NZV,FASLEF,FASRIG,DUMJAC,
     *      TIME,SMF,SMFP,TEND,RTOL,ATOL,RTOLS(1),ATOLS(1),
     *      HMAX,H,IJOB,LRW,RW,LIW,IW,ICALL,RPAR,IPAR)
      IR = IR + 1
C      WRITE(*,*) 'SMF FAST INTEGRATION : ' , SMF(1:NEQ)
C
C-Sl- Write to fort.121
      WRITE(NFIL121) TIME,SMF(1:NEQ)
 1221 FORMAT(8(1PE14.7,1X))
C-Sl- End of writing
C**********************************************************************
C     check error code
C**********************************************************************
      IERR = IJOB(7)
C
      IF(IJOB(7).LT.0) THEN
C        WRITE(MSGFIL,*) 'IJOB' , IJOB(7), ' !!! STOP forced !!! '
C-Sl- Error exit of fast trajectory calculation
C      STOP
C      READ(*,*)
      GOTO 1000
C-Sl- End of
      ENDIF
C**********************************************************************
C     check current time
C**********************************************************************
      ICALL = 2
      IJOB(7) = 0
      ENDDO
C**********************************************************************
C     Regular exit
C**********************************************************************
 1000 RETURN
      END
      SUBROUTINE FASRIG (NEQ,NZV,TIME,Y,F,BV,IR,IC,RPAR,IPAR,IFC,IFP)
C***********************************************************************
C
C     DEFINITION OF RIGHT HAND SIDE (SEE -ULIMEX-)
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION Y(NEQ),F(NEQ),BV(*),IR(*),IC(*)
      DIMENSION RPAR(*),IPAR(*),IFP(*)
C***********************************************************************
C     CALL OF PDESYS
C***********************************************************************
      CALL FASRES(NEQ,TIME,Y,F,RPAR,IPAR,IEMD)
      RETURN
C***********************************************************************
C     END OF FCN
C***********************************************************************
      END
      SUBROUTINE FASLEF(NZC,B,IR,IC)
C***********************************************************************
C
C     DEFINITION OF LEFT HAND SIDE (SEE -ULIMEX-)
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION B(*),IR(*),IC(*)
      PARAMETER(ONE=1.0D0)
C***********************************************************************
C
C***********************************************************************
      DO I=1,NZC
        IR(I)=I
        IC(I)=I
      ENDDO
      B(1:NZC) = ONE
      RETURN
C***********************************************************************
C     END OF FASLEF
C***********************************************************************
      END
      SUBROUTINE FASRES(NEQ,TIME,SMF,DEL,RPAR,IPAR,IFC)
C***********************************************************************
C
C     For a given function F this subroutine does the following:
C
C        C
C        Z_f^-1
C
C
C***********************************************************************
C***********************************************************************
C     Storage organisation
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION SMF(NEQ),DEL(NEQ),RPAR(*),IPAR(*)
C***********************************************************************
C     COMMON BLOCKS
C***********************************************************************
      INCLUDE 'homdim.f'
      COMMON /BSCRS/ERVROW(MNEQU*MNEQU),ERVCOL(MNEQU*MNEQU)
      COMMON /BEGVS/EVAR(MNEQU),EVAI(MNEQU)
      COMMON /BTRMA/XTROW(MNEQU*MNEQU),XTCOL(MNEQU*MNEQU)
      COMMON/BHEL/QEVB(MNEQU*MNEQU),QEVBI(MNEQU*MNEQU)
      COMMON/LEFCOM/NLNEQ,NLROW
C***********************************************************************
C     DATA Statements
C***********************************************************************
      DATA NFILE6/6/
C***********************************************************************
C     WORK ARRAYS
C***********************************************************************
      DIMENSION RW(MNEQU*MNEQU),IW(2*MNEQU)
      LIW=2*MNEQU
      LRW=MNEQU*MNEQU
C***********************************************************************
C     Call function evaluation
C***********************************************************************
      NZEV = IPAR(7)
C-Sl-
C      NC   = IPAR(11)
      NC   = NLROW
C      WRITE(*,*) 'NC = ', NC, 'NF = ', NEQ - NC
C      READ(*,*)
C-Sl-
      CALL FASDES(NEQ,NC,NZEV,TIME,SMF,DEL,RPAR,IPAR,IFC,
     1    EVAR,EVAI,ERVROW,ERVCOL,XTROW,XTCOL,QEVB,QEVBI,NFILE6,IEREXD)
      IFC = 0
      IF(IEREXD.GT.0) IFC = 9
      RETURN
C***********************************************************************
C     END OF
C***********************************************************************
      END
      SUBROUTINE FASDES(NEQ,NC,NZEV,TIME,SMF,DEL,RPAR,IPAR,IFC,AR,AI,
     1    ERVROW,ERVCOL,YTROW,YTCOL,QEVB,QEVBI,NFILE6,IERR)
C***********************************************************************
C
C     RESIDUAL AND COEFFICIENTS OF TIME DERIVATIVES
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
C***********************************************************************
C     parameter arrays
C***********************************************************************
      DIMENSION RPAR(*),IPAR(*)
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)       :: XJAC,SCHUR,
     1                                              AVR,AVL,
     1                                              YTROW,YTCOL,
     1                                              ERVROW,ERVCOL,
     1                                              QEVB, QEVBI,
     1                                              QTEM
      DOUBLE PRECISION, DIMENSION(NEQ)           :: HELP,VRS,SMF,DEL,
     1                                              AR,AI
      EXTERNAL MASRES,MASJAC
      LOGICAL EQUILI
      PARAMETER (ONE = 1.0D0, ZERO = 0.0D0)
C***********************************************************************
C     work arrays
C***********************************************************************
      INTEGER         , DIMENSION(2*NEQ)         :: IW
      DOUBLE PRECISION, DIMENSION(NEQ*NEQ)       :: RW
C***********************************************************************
C     Commons
C***********************************************************************
      COMMON/ILDMPA/IMAILD,ILMILD,IOILDM
C***********************************************************************
C
C***********************************************************************
      LIW = 2*NEQ
      LRW = NEQ*NEQ
C***********************************************************************
C
C     Initialization
C
C***********************************************************************
      IERR = 0
C***********************************************************************
C     Check work array
C***********************************************************************
C***********************************************************************
C
C     Calculate components for manifold equations
C
C***********************************************************************
      NF   = NEQ - NC
      MDIM = NC
      EQUILI = .FALSE.
      IF(NF.EQ.NEQ-NZEV) EQUILI = .TRUE.
C***********************************************************************
C
C     Calculate eigenvector basis
C
C***********************************************************************
C***********************************************************************
C
C***********************************************************************
      IPRI = 0
      RDIM = 0
                       ICE  = 1
                       IAJ  = 0
      IF(IMAILD.EQ.1) THEN
C----   use J*J^T
        ICE = 4
C----   use analytical Jacobian
        IAJ = 0
      ENDIF
      IF(IMAILD.EQ.2) THEN
        ICE = 1
        IAJ = 1
      ENDIF
      IF(ILMILD.NE.0) ICE = 0
C      CALL EGVBAS(ICE,IAJ,MDIM,RDIM,NEQ,MASRES,MASJAC,TIME,SMF,VRS,
C     1            ERVCOL,ERVROW,YTCOL,YTROW,
C     1            HELP,DEL,AR,AI,AVR,AVL,XJAC,SCHUR,LIW,IW,LRW,RW,
C     1            RPAR,IPAR,IPRI,NFILE6,IERBAS)
C      IF(IERBAS.GT.0) GOTO 910
      YTROW = QEVB
      YTCOL = QEVBI
      IF(EQUILI) THEN
         YTROW = ERVROW
         YTCOL = ERVCOL
      ENDIF
C***********************************************************************
C     Calculate F
C***********************************************************************
      CALL MASRES(NEQ,TIMDUM,SMF,VRS,RPAR,IPAR,IEMR)
C***********************************************************************
C
C     Calculate components for manifold equations
C
C***********************************************************************
C***********************************************************************
C     Calculate -Z~_f * F or for 1st order   -Z~_f * F_y * F
C***********************************************************************
C-Sl-
C      QTEM=MATMUL(YTCOL,YTROW)
C      SS=0.D0
C      DO I=1,NEQ
C        SS =  SS + QTEM(I,I)
C      ENDDO
C        WRITE(*,*) '****FAST SUBSYSTEM****'
C        WRITE(*,*) 'N ~ ', SS
C-Sl-
      HELP(1:NF) = MATMUL(YTROW(NEQ-NF+1:NEQ,1:NEQ),VRS(1:NEQ))
      DEL(1:NEQ) = MATMUL(YTCOL(1:NEQ,NEQ-NF+1:NEQ),HELP(1:NF))
C***********************************************************************
C     Regular exit
C***********************************************************************
      RETURN
C***********************************************************************
C
C***********************************************************************
  910 IERR = 1
      WRITE(NFILE6,*) ' EGVBAS returned to NEWDES with error:',IERBAS
      RETURN
C***********************************************************************
C     END OF FASDES
C***********************************************************************
      END
