      subroutine param_bgnl(p_1,p_red,NEQ,zust_ausg,dimtell,normierung,rev_on,z)!
      use meshmod
      implicit none

      integer, intent(in)                                        ::p_1                   !Anzahl Punkte Ausgangszustand
      integer, intent(in)                                        ::p_red                 !Anzahl Punkte nach Paarametrisierung
      integer, intent(in)                                        ::NEQ              !Anzahl verschiedener Spezies(Zeilen des Zustandvektors)
      doubleprecision,dimension(1:NEQ,1:p_1),intent(in)       ::zust_ausg         !Ausgangszustaende
      logical,intent(in)                                         ::dimtell
      integer,intent(in)                                         ::normierung!
      integer,intent(in)                                         ::rev_on!
      doubleprecision,dimension(1:NEQ,1:p_red),intent(out)    ::z
!     doubleprecision,dimension(1:NEQ,1:p_1),intent(in)            ::grad_aus             !ausgangsgradienten
!     integer,intent(out)                                             ::n_p_rev
!     character(len=11),dimension(1:NEQ),intent(in)                ::spe



      doubleprecision,dimension(1:NEQ,1:p_red) ::zst_red,zst_diff,zst_diff2              !Zustaende nach Parametrisierung
      doubleprecision,dimension(1:NEQ,1:p_1)                       ::help_matr
      doubleprecision,dimension(1:NEQ)                             ::maxima
      doubleprecision,dimension(1:p_red)                              ::gitter
      doubleprecision,dimension(1:p_red)                              ::gitter_norm
      integer                                                         ::i,j,k,hilfszaehler
      doubleprecision                                                 ::delta_s,bglc1,bglc2,s_ges
      logical                                                         ::sc1
      doubleprecision,dimension(1:NEQ)                             ::gew1,gew2
      doubleprecision,dimension(1:2,1:NEQ)                         ::hp
      doubleprecision,dimension(1:p_red)                              ::inv_fkt
      doubleprecision,dimension(2)                                    ::bgln
      integer,dimension(1:NEQ)                                     ::gew_bgln!
!***********************************************************************
!     read weighting
!***********************************************************************
!calpkt=calpkt+1
      open(unit=37,file='gewichtung_bgln.dat',status='unknown',action='read')
      rewind(37)
      do i=1,NEQ!
        read(37,*)gew_bgln(i)!
!       write(*,*)gew_bgln(i)!
      end do
!***********************************************************************
!     compute scaling parameters
!***********************************************************************
      delta_s=dble(0)
      if(normierung==1)then!
          maxima(1:NEQ) = maxval(abs(zust_ausg(1:NEQ,1:p_1)),DIM=2)
          do i=1,NEQ
            if(maxima(i)==dble(0))maxima(i)=dble(1)
          end do
        else
           maxima=1.0d0
      end if!
!***********************************************************************
!     compute help matrix 
!***********************************************************************
      do k=1,p_1
        do i=1,NEQ
          help_matr(i,k)=zust_ausg(i,k)/maxima(i)*dble(gew_bgln(i))!
        end do
      end do
!***********************************************************************
!     compute overall arclength
!***********************************************************************
      s_ges=vektornorm(NEQ,p_1,p_1,help_matr)
!     if (s_ges.lt.1.0d-10)s_ges=1.0d0
      write(*,*)'Gesamtbogenlaenge:',s_ges
!***********************************************************************
!     assign values at 1 and p_1
!***********************************************************************
      zst_red(1:NEQ,1)=zust_ausg(1:NEQ,1)
      zst_red(1:NEQ,p_red)=zust_ausg(1:NEQ,p_1)
      delta_s=s_ges/dble(p_red-1)
      do k=1,p_red
        gitter(k)=dble(k-1)*delta_s
      ENDDO 
!***********************************************************************
!     Loop over points, find next to interpolate
!***********************************************************************
      do k=2,p_red-1
        do i=1,p_1
          hilfszaehler=i
          bglc1=gitter(k)/vektornorm(NEQ,p_1,i,help_matr)
          if(bglc1<dble(1))exit
        end do

!     write(*,*)'Schleifenvariable zum Zustand',k,' :'
!     write(*,*)hilfszaehler

      bgln(1)=vektornorm(NEQ,p_1,hilfszaehler,help_matr)
      bgln(2)=vektornorm(NEQ,p_1,hilfszaehler-1,help_matr)


do j=1,NEQ
zst_red(j,k)=lin_interp(zust_ausg(j,hilfszaehler),zust_ausg(j,hilfszaehler-1),bgln(1)-bgln(2),gitter(k)-bgln(2))
!
end do
end do

do k=2,p_red

    write(*,*)'Reparametrisierter Gitterpunkt ',k
    write(*,*)gitter(k)

end do
if(rev_on==1)then!
if(dimtell)then
write(*,*)'**********************************************************************************'
write(*,*)'Reverteilung! Bitte Gewichtung der Ableitungen waehlen!'
write(*,*)'**********************************************************************************'

open(unit=36, file='gewichtung_al.dat',status='unknown',action='write')

do i=1,NEQ

!write(*,*)'Gewichtung f�r ',spe(i),' angeben:'
write(*,*)'Gewichtung f�r Spezies',i,' angeben:'
write(*,*)'Gewichtung 1. AL'
read(*,*)gew1(i)
write(36,*)gew1(i)
write(*,*)'Gewichtung 2. AL'
read(*,*)gew2(i)
write(36,*)gew2(i)
end do

else
open(unit=36,file='gewichtung_al.dat',status='unknown',action='read')
rewind(36)

do i=1,NEQ
    read(36,*)gew1(i)
    read(36,*)gew2(i)
    if(gew_bgln(i)==0)then!
        gew1(i)=dble(0)!
        gew2(i)=dble(0)!
    end if!
end do


!gew1=1.0d0
!gew2=1.0d0
end if
do k=1,p_red
    do i=1,NEQ
        if(k<p_red)then
            if(k>1)then
            zst_diff(i,k)=abs(diff_zen(zst_red(i,k+1),zst_red(i,k-1),gitter(k+1),gitter(k)))
            zst_diff2(i,k)=abs(diff_2(zst_red(i,k+1),zst_red(i,k),zst_red(i,k-1),gitter(k+1),gitter(k),gitter(k-1)))
            end if
        end if
        if(k==1)then
            zst_diff(i,k)=abs(diff(zst_red(i,k+1),zst_red(i,k),gitter(k+1),gitter(k)))
            zst_diff2(i,k)=abs(diff_2(zst_red(i,k+2),zst_red(i,k+1),zst_red(i,k),gitter(k+2),gitter(k+1),gitter(k)))
        else if(k==p_red)then
            zst_diff(i,k)=abs(diff(zst_red(i,k),zst_red(i,k-1),gitter(k),gitter(k-1)))
            zst_diff2(i,k)=abs(diff_2(zst_red(i,k),zst_red(i,k-1),zst_red(i,k-2),gitter(k),gitter(k-1),gitter(k-2)))
        end if

    end do
end do

gitter_norm(1)=gitter(1)

do k=2,p_red
    do i=1,NEQ
        if(integ(p_red,NEQ,p_red,zst_diff(i,1:p_red),gitter)==dble(0)) then

        else

            gitter_norm(k)=gitter_norm(k)+gew1(i)*(integ(k,NEQ,p_red,zst_diff(i,1:p_red),&
            &gitter))/(integ(p_red,NEQ,p_red,zst_diff(i,1:p_red),gitter))+&
            &gew2(i)*(integ(k,NEQ,p_red,zst_diff2(i,1:p_red),gitter))/(integ&
            &(p_red,NEQ,p_red,zst_diff2(i,1:p_red),gitter))
        end if
    end do

end do

do k=1,p_red
    gitter_norm(k)=gitter_norm(k)*(dble(p_red)/gitter_norm(p_red))
end do

do i=1,p_red
    inv_fkt(i)=dble(i-1)
end do

gitter(1)=gitter_norm(1)
!gitter(p_red)=gitter_norm(p_red)

do i=2,p_red
    do k=2,p_red
        if(gitter_norm(k)>=dble(i-1))then
            gitter(i)=lin_interp(inv_fkt(k),inv_fkt(k-1)&
            &,gitter_norm(k)-gitter_norm(k-1),dble(i-1)-gitter_norm(k-1))
            exit
        end if

    end do
end do


do k=1,p_red
    gitter(k)=gitter(k)*(s_ges/gitter(p_red))
end do



do k=2,p_red

    write(*,*)'Reverteilter Gitterpunkt ',k
    write(*,*)gitter(k)

end do


do k=2,p_red-1

    do i=1,p_1
        hilfszaehler=i

        bglc2=gitter(k)/vektornorm(NEQ,p_1,i,help_matr)

        if(bglc2<=dble(1))exit


    end do

    write(*,*)'Schleifenvariable zum Zustand',k,' :'
    write(*,*)hilfszaehler

    bgln(1)=vektornorm(NEQ,p_1,hilfszaehler,help_matr)
    bgln(2)=vektornorm(NEQ,p_1,hilfszaehler-1,help_matr)





do j=1,NEQ

zst_red(j,k)=lin_interp(zust_ausg(j,hilfszaehler),zust_ausg(j,hilfszaehler-1),bgln(1)-bgln(2),gitter(k)-bgln(2))


end do
end do
end if!

z=zst_red

n_sp=NEQ
allocate(mesh_old(p_red))
mesh_old=gitter
allocate(zst_1(NEQ,p_red))
zst_1=zst_red


!if(calpkt==1)then
!allocate(pkt_bgnln(n_p_rev))
!
!!pkt_bgnln=mesh_new
!end if
!***********************************************************************
!    deallocate arrays
!***********************************************************************
      deallocate(mesh_old)
      deallocate(zst_1)
!***********************************************************************
!    Return   
!***********************************************************************
      return
      end subroutine param_bgnl
