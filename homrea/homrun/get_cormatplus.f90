!--------------------------------------------------------------------------------------
! Written by Alexander Neagos
!--------------------------------------------------------------------------------------
! module for computation of diffusion and psi_theta through sum of weighted functions
!--------------------------------------------------------------------------------------
module get_cormatplus
    use vector_algebra
    implicit none
    type :: integers
        integer                 :: neq,ndim,nvert,npoint,typefunc
    end type integers
!
    type, extends(integers) :: function_data
        type(value_type),allocatable,dimension(:,:)                 :: y,dy,ddy
        double precision, allocatable,dimension(:,:)                :: icord
        integer, allocatable,dimension(:)                           :: nfunc
        integer                                                     :: itotdim
        contains
        procedure                                                   :: get_functions
    end type function_data
!
    type, extends(function_data) :: cormat_data
        double precision, allocatable, dimension(:,:)                :: cormatplus
        double precision, allocatable, dimension(:,:)                :: cormat
        contains
        procedure                                                    :: get_cormat_plus
    end type cormat_data
!
!
    type, extends(cormat_data) :: dynamic_data
        double precision, allocatable, dimension(:,:)               :: coefficients
        double precision, allocatable, dimension(:,:)               :: psi
        double precision, allocatable, dimension(:,:,:)             :: difma
        double precision, allocatable, dimension(:,:,:)             :: psth
        double precision, allocatable, dimension(:,:,:)             :: psthp
        double precision, allocatable, dimension(:,:)               :: xgrpsi
        double precision, allocatable, dimension(:,:,:,:)           :: pstt
        double precision, allocatable, dimension(:,:)               :: gradtheta
        double precision, allocatable, dimension(:,:)               :: scama
        double precision, allocatable, dimension(:,:)               :: diff
        contains
        procedure                                                   :: compute_diffusion
        procedure                                                   :: compute_psth
        procedure                                                   :: allocate_dynamic_data
        procedure                                                   :: allocate_psth
        procedure                                                   :: allocate_diff
    end type dynamic_data
    contains
    subroutine get_functions(self)
!--------------------------------------------------------------------------------------
! subroutine to get function values and first and second derivatives
!--------------------------------------------------------------------------------------
! change to other types of functions by changing module, see file basis functions
!--------------------------------------------------------------------------------------
        use normed_gauss_module
        class(base),allocatable   :: basis
        class(function_data)      :: self
        integer                                         :: i,l,k
!--------------------------------------------------------------------------------------
! - replace basis type through chosen one, e.g. normed gauss
! - corresponding module has to be used
! - change to other types of functions , see file basis functions
!--------------------------------------------------------------------------------------
        allocate(normed_gauss :: basis)
!--------------------------------------------------------------------------------------
! allocate functions
!--------------------------------------------------------------------------------------
        call allocate_functions(self)
!--------------------------------------------------------------------------------------
! compute function values for each dimension
!--------------------------------------------------------------------------------------
        do i=1,self%ndim
!--------------------------------------------------------------------------------------
! number of functions for each dimension
!--------------------------------------------------------------------------------------
            basis%nfunc=self%nfunc(i)
!--------------------------------------------------------------------------------------
! allocation
!--------------------------------------------------------------------------------------
            call basis%allocate_all()
!--------------------------------------------------------------------------------------
! set function parameters, see basis_functions.f90
!--------------------------------------------------------------------------------------
            call basis%set_parameters()
!--------------------------------------------------------------------------------------
! loop over grid
!--------------------------------------------------------------------------------------
            do l=1,self%nvert
!--------------------------------------------------------------------------------------
! scaling from theta to function coordinates, see basis_functions.f90
!--------------------------------------------------------------------------------------
                call basis%scale_x(self%icord(i,l),minval(self%icord(i,:)), &
                & maxval(self%icord(i,:)))
!--------------------------------------------------------------------------------------
! get function values in function coordinates, see basis_functions.f90
!--------------------------------------------------------------------------------------
                call basis%get_values()
!--------------------------------------------------------------------------------------
! rescale first and second derivative, see basis_functions.f90
!--------------------------------------------------------------------------------------
                call basis%rescale_y(minval(self%icord(i,:)),maxval(self%icord(i,:)))
!--------------------------------------------------------------------------------------
! assign function values
!--------------------------------------------------------------------------------------
                self%y(l,i)%val=basis%value_array(1,:)
                self%dy(l,i)%val=basis%value_array(2,:)
                self%ddy(l,i)%val=basis%value_array(3,:)
            end do
!--------------------------------------------------------------------------------------
! deallocation, see basis_functions.f90
!--------------------------------------------------------------------------------------
            call basis%deallocate_all()
        end do
    end subroutine get_functions
!
    subroutine allocate_functions(self)
!--------------------------------------------------------------------------------------
! subrotuine for allocation of function data
!--------------------------------------------------------------------------------------
        class(function_data)       :: self
        integer :: i,l
        if (.not. allocated(self%y)) allocate(self%y(self%nvert,self%ndim))
        if (.not. allocated(self%dy)) allocate(self%dy(self%nvert,self%ndim))
        if (.not. allocated(self%ddy)) allocate(self%ddy(self%nvert,self%ndim))
!        itotdim=1
!
        do l=1,self%nvert
            do i=1,self%ndim
                if (.not.allocated(self%y(l,i)%val))allocate(self%y(l,i)%val(self%nfunc(i)))
                if (.not.allocated(self%dy(l,i)%val))allocate(self%dy(l,i)%val(self%nfunc(i)))
                if (.not.allocated(self%ddy(l,i)%val))allocate(self%ddy(l,i)%val(self%nfunc(i)))
            end do
        end do
        self%itotdim=1
!--------------------------------------------------------------------------------------
! itotdim through multiplication of function number in each direction, needed get_cormatplus
!--------------------------------------------------------------------------------------
        do i=1,self%ndim
        self%itotdim=self%itotdim*size(self%y(1,i)%val)
        end do
    end subroutine allocate_functions
!
    subroutine get_cormat_plus (self)
!--------------------------------------------------------------------------------------
! subrotuine for computation of (pseudo-)inverse of coordinate matrix
!--------------------------------------------------------------------------------------
        class(cormat_data)                               :: self
        type(value_type),dimension(self%ndim)           :: worky
        double precision,dimension(:),allocatable       :: corvect
        double precision, dimension(self%nvert,self%nvert)        :: scama
        integer                                         :: i,l,k
        double precision, dimension(self%nvert)              :: rw
        integer         , dimension(self%nvert)              :: iw
        integer                                         :: ierinv
        call self%get_functions()
!--------------------------------------------------------------------------------------
! cormat & cormatplus & coeff_vec: size(degree1*degree2*...)
!--------------------------------------------------------------------------------------
        if (.not.allocated(self%cormat)) allocate(self%cormat(self%nvert,self%itotdim))
        if (.not.allocated(self%cormatplus)) allocate(self%cormatplus(self%itotdim,self%nvert))
        scama=0.0d0
        self%cormat=0.0d0
        self%cormatplus=0.0d0
!--------------------------------------------------------------------------------------
! scaling not needed, use genpsi
!--------------------------------------------------------------------------------------
        do k=1,self%nvert
            scama(k,k)=1.0d0
        end do
!--------------------------------------------------------------------------------------
! cartesian product, save in cormat, see basis_functions.f90
!--------------------------------------------------------------------------------------
        do k=1,self%nvert
            worky=self%y(k,:)
            call multiply_coor(self%ndim,worky,corvect)
!            write(*,*)corvect
!            read(*,*)
            self%cormat(k,:)=corvect
        end do
!--------------------------------------------------------------------------------------
! Pseudo-Inverse -> Least squares
!--------------------------------------------------------------------------------------
            if (self%itotdim.ne.self%nvert)then
!                write(*,*)'itotdim',self%itotdim,'.ne. nvert',self%nvert
!                read(*,*)
                call genpsi(self%nvert,self%itotdim,self%nvert, &
                &           scama, &
                &           self%cormat(1:self%nvert,1:self%itotdim), &
                &           self%cormatplus)
!--------------------------------------------------------------------------------------
! Inverse-> exact
!--------------------------------------------------------------------------------------
            else
!                write(*,*)'itotdim .eq. nvert'
                self%cormatplus=self%cormat
                call dgeinv(self%nvert,self%cormatplus,rw,iw,ierinv)
            end if
    end subroutine get_cormat_plus
!
    subroutine print_function_values(dynamic_dat,symbols,npoints)
!--------------------------------------------------------------------------------------
! subroutine for printing of function values, only if global functions are used
!--------------------------------------------------------------------------------------
        implicit none
        type(function_data)                             :: func_dat
        type(dynamic_data)                               :: dynamic_dat
        double precision,allocatable,dimension(:,:)     :: ynew
        integer, dimension(dynamic_dat%ndim)             :: npoints
        integer                                         :: i,j,k,l
        integer                                         :: a,b,c
        integer                                         :: newpoints=1
        integer                                         :: LTHET        
        character(LEN=*), dimension(dynamic_dat%neq)         :: symbols
        parameter(LTHET=8)
        character(LEN=LTHET), dimension(dynamic_dat%ndim)        :: thetsym
!
        newpoints=product(npoints(:))
        allocate (ynew(dynamic_dat%neq,newpoints))
        func_dat%nvert=newpoints
        func_dat%ndim=dynamic_dat%ndim
        func_dat%neq=dynamic_dat%neq
        allocate(func_dat%nfunc(func_dat%ndim))
        allocate(func_dat%icord(func_dat%ndim,func_dat%nvert))
        func_dat%nfunc=dynamic_dat%nfunc
        do i=1,func_dat%ndim
            a=newpoints/product(npoints(1:i))
            b=newpoints/product(npoints(i:func_dat%ndim))
        c=product(npoints(1:i))
!            write(*,*)'a',a
!            write(*,*)'b',b
!            write(*,*)'npoints(i)',npoints(i)
            do j=1,a
                do k=1,npoints(i)
                    func_dat%icord(i,(j-1)*c+(k-1)*b+1:(j-1)*c+(k-1)*b+b)=&
                    &(k-1)*((maxval(dynamic_dat%icord(i,:))-minval(dynamic_dat%icord(i,:)))/&
                    &(npoints(i)-1))+minval(dynamic_dat%icord(i,:))
!                    write(*,*)'von',(j-1)*c+(k-1)*b+1,'bis',(j-1)*c+(k-1)*b+b
!                    write(*,*)newcord(i,(j-1)*c+(k-1)*b+1:(j-1)*c+(k-1)*b+b)
                end do
            end do
!        do m=1,newpoints
!        write(*,*)'newcord',m,newcord(:,m)
!        end do
        end do
!        newcord=-1.0d0*newcord
!
        call get_new_yvalues(func_dat,dynamic_dat,ynew)
!
        do k=1,func_dat%ndim
            write(thetsym(k),"('theta',i3)") k
        end do
!
        write(55,10) (thetsym(k),k=1,func_dat%ndim), (symbols(k),k=1,func_dat%neq)
        if (func_dat%ndim.eq.1)then
            write(55,11) npoints(1)
        else if (func_dat%ndim.eq.2)then
            write(55,12) npoints(1),npoints(2)
        else if (func_dat%ndim.eq.3)then
            write(55,13) npoints(1),npoints(2),npoints(3)
        end if
        do l=1,func_dat%nvert
            write(55,14)(func_dat%icord(k,l),k=1,func_dat%ndim),(ynew(k,l),k=1,func_dat%neq)
        end do
        10 format('variables =',/,7('"',a,'"',:','))
        11 format('zone i=',i5,', f=point')
        12 format('zone i=',i5,', j=',i5,', f=point')
        13 format('zone i=',i5,', j=',i5,', k=',i5,', f=point')
        14 format(7(1pe12.5,1x))
    end subroutine print_function_values
!
    subroutine get_new_yvalues(func_dat,dynamic_dat,ynew)
!--------------------------------------------------------------------------------------
! subroutine for calculation of new function values, mainly for output
!--------------------------------------------------------------------------------------
        implicit none
        type(function_data)                             :: func_dat
        type(dynamic_data)                              :: dynamic_dat
        double precision,dimension(:),allocatable         :: corvect
        double precision, dimension(func_dat%neq,func_dat%nvert)      :: ynew
        integer                                         :: k,l
! test if cormatplus has been allocated
        call func_dat%get_functions()
        do k=1,func_dat%neq
            do l=1,func_dat%nvert
                call multiply_coor(func_dat%ndim,func_dat%y(l,:),corvect)
                ynew(k,l) = dot_product(corvect,dynamic_dat%coefficients(k,:))
            end do
        end do
    end subroutine get_new_yvalues
!
    subroutine compute_diffusion_simple(self)
!--------------------------------------------------------------------------------------
! subroutine performs simple form of diffusion calculation
!--------------------------------------------------------------------------------------
    class(dynamic_data)                                :: self
    type(value_type),dimension(self%ndim)             :: worky
    double precision,dimension(:),allocatable         :: corvect
    double precision,dimension(self%itotdim)          :: coeff_vec
    double precision, dimension(self%neq,self%ndim,self%nvert)   :: dpsthloc,dgrpsi
    double precision, dimension(self%neq,self%ndim,self%ndim,self%nvert)  :: dpstt
    double precision, dimension(self%neq,self%neq,self%ndim,self%nvert)  :: difmath
    double precision, dimension(self%ndim,self%neq)             :: dpseugt,&
                                                       & dpseudum
    double precision, dimension(self%neq,self%nvert)            :: diff1,diff2 &
                                                       & ,diff3
    integer     :: i,j,k,l,ii,jj,kk,ll
!--------------------------------------------------------------------------------------
! coefficients: cormatplus*Psi
!--------------------------------------------------------------------------------------
    do k=1,self%neq
        self%coefficients(k,:)=matmul(self%cormatplus,self%psi(k,:))
!--------------------------------------------------------------------------------------
! call chebfit instead in case of chebyshev_cos, fast fourier transform, andrey koksharov
!--------------------------------------------------------------------------------------
!    call chebfit(self%nvert,self%psi(k,:),self%coefficients(k,:))
    end do
!--------------------------------------------------------------------------------------
! compute psi_theta
!--------------------------------------------------------------------------------------
    do k=1,self%neq
        do l=1,self%nvert
            do i=1,self%ndim
                worky=self%y(l,:)
                worky(i)=self%dy(l,i)
                call multiply_coor(self%ndim,worky,corvect)
!--------------------------------------------------------------------------------------
! psi_theta
!--------------------------------------------------------------------------------------
                self%psth(k,i,l)=dot_product(corvect,self%coefficients(k,:))
            end do
        end do
    end do
!--------------------------------------------------------------------------------------
! diff=(D*psi_theta)_theta*grad(theta)
!--------------------------------------------------------------------------------------
      do l=1,self%nvert
          dpsthloc(:,:,l)=&
         & matmul(self%difma(:,:,l),&
         &        self%psth(:,:,l))
          call genpsi(self%neq,self%ndim,self%scama,self%psth(:,:,l),self%psthp(:,:,l))
          self%gradtheta(:,l)= matmul(self%psthp(:,:,l),self%xgrpsi(:,l))
          self%diff(:,l)=matmul(dpsthloc(:,:,l),self%gradtheta(:,l))
      end do
    do k=1,self%neq
        coeff_vec=matmul(self%cormatplus,self%diff(k,:))
!--------------------------------------------------------------------------------------
! call chebfit instead in case of chebyshev_cos, fast fourier transform, andrey koksharov
!--------------------------------------------------------------------------------------
    !            coeff_vec=0.0d0
    !            call chebfit(self%nvert,self%diff(k,:),coeff_vec)
        do l=1,self%nvert
            do i=1,self%ndim
                worky=self%y(l,:)
                worky(i)=self%dy(l,i)
                call multiply_coor(self%ndim,worky,corvect)
                dpsthloc(k,i,l)=dot_product(corvect,coeff_vec)
            end do
        end do
    end do
    do l=1,self%nvert
        self%diff(:,l)=matmul(dpsthloc(:,:,l),self%gradtheta(:,l))
    end do
    end subroutine
!
!
!
    subroutine compute_psth(self)
!--------------------------------------------------------------------------------------
! subroutine computes the derivative of the psi, psi_theta, based on
! local coordinates as well as the pseudo-inverse psi_theta+
!--------------------------------------------------------------------------------------
    class(dynamic_data)                                :: self
    type(value_type),dimension(self%ndim)             :: worky
    double precision,dimension(:),allocatable         :: corvect
    double precision,dimension(self%itotdim)          :: coeff_vec
    double precision, dimension(self%neq,self%ndim,self%nvert)   :: dpsthloc,dgrpsi
    double precision, dimension(self%neq,self%ndim,self%ndim,self%nvert)  :: dpstt
    double precision, dimension(self%neq,self%neq,self%ndim,self%nvert)  :: difmath
    double precision, dimension(self%ndim,self%neq)             :: dpseugt,&
                                                       & dpseudum
    double precision, dimension(self%neq,self%nvert)            :: diff1,diff2 &
                                                       & ,diff3
    integer     :: i,j,k,l,ii,jj,kk,ll
!--------------------------------------------------------------------------------------
! compute coefficients, global variables
!--------------------------------------------------------------------------------------
    do k=1,self%neq
    self%coefficients(k,:)=matmul(self%cormatplus,self%psi(k,:))
!--------------------------------------------------------------------------------------
! call chebfit instead in case of chebyshev_cos, fast fourier transform, andrey koksharov
!--------------------------------------------------------------------------------------
!    call chebfit(self%nvert,self%psi(k,:),self%coefficients(k,:))
    end do
!--------------------------------------------------------------------------------------
! compute psi_theta
!--------------------------------------------------------------------------------------
    do l=1,self%npoint
        do k=1,self%neq
            do i=1,self%ndim
                worky=self%y(l,:)
                worky(i)=self%dy(l,i)
                call multiply_coor(self%ndim,worky,corvect)
!                  write(*,*)'coruuuuuuuu:vect',corvect
                self%psth(k,i,l)=dot_product(corvect,self%coefficients(k,:))
            end do
        end do
        call genpsi(self%neq,self%ndim,self%neq, &
               self%scama,self%psth(:,:,l),self%psthp(:,:,l))
    !      write(*,*)'psith',psth(k,:,10)
    !      read(*,*)
    end do
    end subroutine compute_psth
!
!
!
    subroutine compute_diffusion(self)
!--------------------------------------------------------------------------------------
! subrotuine computes diffusion term, complicated form
!--------------------------------------------------------------------------------------
    class(dynamic_data)                                :: self
    type(value_type),dimension(self%ndim)             :: worky
    double precision,dimension(:),allocatable         :: corvect
    double precision,dimension(self%itotdim)          :: coeff_vec
    double precision, dimension(self%neq,self%ndim,self%npoint)   :: dpsthloc,dgrpsi
    double precision, dimension(self%neq,self%ndim,self%ndim,self%npoint)  :: dpstt
    double precision, dimension(self%neq,self%neq,self%ndim,self%npoint)  :: difmath
    double precision, dimension(self%ndim,self%neq)             :: dpseugt,&
                                                       & dpseudum
    double precision, dimension(self%neq,self%npoint)            :: diff1,diff2 &
                                                       & ,diff3
    integer     :: i,j,k,l,ii,jj,kk,ll
!--------------------------------------------------------------------------------------
! compute psi_theta_theta
!--------------------------------------------------------------------------------------
    do k=1,self%neq
        do l=1,self%npoint
            do i=1,self%ndim
                do j=1,self%ndim
                    worky=self%y(l,:)
                    if (i.eq.j)then
                        worky(i)=self%ddy(l,i)
                    else
                        worky(i)=self%dy(l,i)
                        worky(j)=self%dy(l,j)
                    end if
                    call multiply_coor(self%ndim,worky,corvect)
                    self%pstt(k,i,j,l)=dot_product(corvect,self%coefficients(k,:))
                end do
            end do
        end do
    end do
!--------------------------------------------------------------------------------------
! compute D_theta
!--------------------------------------------------------------------------------------
    do jj=1,self%neq
        do kk=1,self%neq
            coeff_vec=matmul(self%cormatplus,self%difma(kk,jj,:))
!            coeff_vec=0.0d0
!            call chebfit(self%nvert,self%difma(kk,jj,:),coeff_vec)
            do ii=1,self%npoint
                do ll=1,self%ndim
                    worky=self%y(ii,:)
                    worky(ll)=self%dy(ii,ll)
                    call multiply_coor(self%ndim,worky,corvect)
                    difmath(kk,jj,ll,ii)=dot_product(corvect,coeff_vec)
                end do
            end do
        end do
    end do
!--------------------------------------------------------------------------------------
! compute D_theta*psi_theta+D*psi_theta_theta
!--------------------------------------------------------------------------------------
    do l=1,self%npoint
        do j=1,self%ndim
            Dpstt(:,j,:,l)=matmul(difmath(:,:,j,l),self%psth(:,:,l))+&
       &                   matmul(self%difma(:,:,l),self%pstt(:,:,j,l))
        end do
    end do
!--------------------------------------------------------------------------------------
! compute first part of diffusion, grad(theta)*(D_theta*psi_theta+D*psi_theta_theta)*grad(theta)
!--------------------------------------------------------------------------------------
    diff1=0.0d0
    do l=1,self%npoint
    self%gradtheta(:,l)=matmul(self%psthp(:,:,l),self%xgrpsi(:,l))
    do i=1,self%ndim
    do j=1,self%ndim
        diff1(:,l) = diff1(:,l) + self%gradtheta(i,l)*dpstt(:,i,j,l)*self%gradtheta(j,l)
    end do
    end do
    end do
!--------------------------------------------------------------------------------------
! compute derivative of psi_theta+, dpsudum=psi_theta+_theta, and term psi_theta+_theta*grad(theta)
!--------------------------------------------------------------------------------------
    do l=1,self%npoint
        dpseugt=0.0d0
        do i=1,self%ndim
            call dpseudo(self%ndim,self%neq,self%scama,self%psth(:,:,l),self%pstt(:,:,i,l), &
                         & self%psthp(:,:,l),dpseudum)
            dpseugt=dpseugt+dpseudum*self%gradtheta(i,l)
        end do
!--------------------------------------------------------------------------------------
! compute second part of diffusion, (D*psi_theta)*(psi_theta+_theta**grad(theta)*grad(psi))
!--------------------------------------------------------------------------------------
        diff2(:,l)=matmul(matmul(self%difma(:,:,l),self%psth(:,:,l)), &
            &  matmul(dpseugt,self%xgrpsi(:,l)))
    end do
!--------------------------------------------------------------------------------------
! compute thrid part of diffusion,D*psi_theta*psi_theta+*(grad(psi)_theta*grad(theta))
!--------------------------------------------------------------------------------------
    self%pstt=dpstt
    do k=1,self%neq
        coeff_vec=matmul(self%cormatplus,self%xgrpsi(k,:))
        do l=1,self%npoint
            do i=1,self%ndim
                worky=self%y(l,:)
                worky(i)=self%dy(l,i)
                call multiply_coor(self%ndim,worky,corvect)
                dgrpsi(k,i,l)=dot_product(corvect,coeff_vec)
            end do
        end do
    end do
    do l=1,self%npoint
    diff3(:,l)=matmul(matmul(self%difma(:,:,l),self%psth(:,:,l)), &
       &   matmul(self%psthp(:,:,l),matmul(dgrpsi(:,:,l),self%gradtheta(:,l))))
    end do
!--------------------------------------------------------------------------------------
! add diffusion parts
!--------------------------------------------------------------------------------------
    self%diff=diff1+diff2+diff3
    end subroutine
!
!
    subroutine dpseudo(ndim,neq,m,a,aprime,aplus,dpseu)
!--------------------------------------------------------------------------------------
! subroutine computes derivative of matrix aplus, pseudo-inverse of aprime
!--------------------------------------------------------------------------------------
    implicit none
    integer :: ndim,neq,ierinv
    double precision, dimension(neq,ndim)       ::a,aprime
    double precision, dimension(ndim,neq)       ::aplus,dpseu
    double precision, dimension(ndim,ndim)       ::b
    double precision, dimension(neq)          :: rw
    integer         , dimension(neq)          :: iw
    double precision, dimension(neq,neq)          :: m
    b=matmul(transpose(a),matmul(m,a))
    call dgeinv(ndim,b,rw,iw,ierinv)
!
!
    dpseu=matmul(matmul(b,transpose(aprime)),m)- &
    & matmul(matmul(matmul(b,transpose(aprime)),m),matmul(a,aplus))-&
    & matmul(aplus,matmul(aprime,aplus))
!
    end subroutine
!
!--------------------------------------------------------------------------------------
! allocation of necessary arrays
!--------------------------------------------------------------------------------------
    subroutine allocate_dynamic_data(self)
        class(dynamic_data)                   :: self
        allocate(self%coefficients(self%neq,self%itotdim))
        allocate(self%psi(self%neq,self%nvert))
        allocate(self%difma(self%neq,self%neq,self%nvert))
        allocate(self%xgrpsi(self%neq,self%nvert))
        allocate(self%scama(self%neq,self%neq))
    end subroutine allocate_dynamic_data
    subroutine allocate_psth(self)
        class(dynamic_data)                   :: self
        allocate(self%psth(self%neq,self%ndim,self%npoint))
        allocate(self%psthp(self%ndim,self%neq,self%npoint))
    end subroutine allocate_psth
    subroutine allocate_diff(self)
        class(dynamic_data)                   :: self
        allocate(self%pstt(self%neq,self%ndim,self%ndim,self%npoint))
        allocate(self%gradtheta(self%ndim,self%npoint))
        allocate(self%diff(self%neq,self%npoint))
    end subroutine allocate_diff
!--------------------------------------------------------------------------------------
! some functions
!--------------------------------------------------------------------------------------
double precision function funcpara(ii,mini,maxi)
      IMPLICIT NONE
      double precision ii
      double precision maxi,mini
      funcpara= -(((2.0d0*(ii-mini)/(maxi-mini))&
       &            -1.0d0)**2)+1.0d0
      end function funcpara
end module get_cormatplus
module cor_module
    use get_cormatplus
    type(dynamic_data),allocatable,dimension(:)        :: inner_points
    type(dynamic_data),allocatable,dimension(:)        :: boundary_points
end module cor_module
