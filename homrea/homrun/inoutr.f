C>    inoutr
      SUBROUTINE OUTRST(NEQ,TIME,NFISTO,S,SP)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *   PROGRAM FOR THE PRINTOUT OF RESULTS FOR RESTART AND PP. *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C     TIME         = TIME
C     NFISTO       = UNIT FOR OUTPUT
C     NEQ          = NUMBER OF VARIABLES
C     S(NEQ)       = DEPENDENT VARIABLE'S ARRAY
C     SP(NEQ)      = TIME DERIVATIVE OF S
C
C     TO BE USED WITH INPRST
C
C***********************************************************************
C
C
C
C***********************************************************************
C     STORAGE ORGANIZATION AND DEFINITION OF INITIAL VALUES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION S(NEQ),SP(NEQ)
C***********************************************************************
C     OUTPUT FOR RESTART AND POST PROCESSING
C***********************************************************************
      WRITE(NFISTO,81)
      WRITE(NFISTO,82) TIME,TIME
      WRITE(NFISTO,87) (S(I),I=1,NEQ)
      WRITE(NFISTO,87) (SP(I),I=1,NEQ)
      WRITE(NFISTO,89)
      RETURN
C***********************************************************************
C     FORMAT STATEMENTS
C***********************************************************************
   81 FORMAT('>DATA',75X)
   82 FORMAT('> TIME (S) ',1PE10.3,4X,1PE25.18)
   87 FORMAT(5(1PE15.8,1X))
   89 FORMAT('<    ',75X)
C***********************************************************************
C     END OF -OUTRST-
C***********************************************************************
      END
      SUBROUTINE INPRST(NEQ,TIME,NFISTO,S,SP,IERR)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *   PROGRAM FOR THE INPUT OF RESULTS FOR RESTART AND POSTP. *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C     NFISTO       = UNIT FOR OUTPUT
C     NEQ          = NUMBER OF VARIABLES
C
C
C     OUTPUT :
C
C     TIME         = TIME
C     S(NEQ)       = DEPENDENT VARIABLE'S ARRAY
C     SP(NEQ)      = TIME DERIVATIVE OF S
C
C     TO BE USED WITH OUTRST
C
C***********************************************************************
C
C***********************************************************************
C     STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      CHARACTER*4 STR4
      CHARACTER*1 STR1
      DIMENSION S(NEQ),SP(NEQ)
C***********************************************************************
C     INITIALIZATION
C***********************************************************************
      IERR=0
C***********************************************************************
C     INPUT OF VALUES
C***********************************************************************
      READ(NFISTO,81,END=90,ERR=90) STR4
      IF(STR4.NE.'>DAT') GOTO 92
      READ(NFISTO,82,END=90,ERR=90) TIME
      READ(NFISTO,87,END=90,ERR=90) (S(I),I=1,NEQ)
      READ(NFISTO,87,END=90,ERR=90) (SP(I),I=1,NEQ)
      READ(NFISTO,89,END=90,ERR=90) STR1
      IF(STR1.NE.'<') GOTO 95
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
   90 CONTINUE
      IERR=1
      GOTO 99
   92 CONTINUE
      IERR=2
      GOTO 99
   95 CONTINUE
      IERR=3
      GOTO 99
   99 CONTINUE
      RETURN
C***********************************************************************
C     FORMAT STATEMENTS
C***********************************************************************
   81 FORMAT(A4)
   82 FORMAT(25X,1PE25.18)
   87 FORMAT(5(1PE15.8,1X))
   89 FORMAT(A1)
C***********************************************************************
C     END OF -INPRST-
C***********************************************************************
      END
