C>    masres
      SUBROUTINE MASRIG (NEQ,NZV,TIME,Y,F,BV,IR,IC,RPAR,IPAR,IFC,IFP)
C***********************************************************************
C
C     DEFINITION OF RIGHT HAND SIDE (SEE -ULIMEX-)
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION Y(NEQ),F(NEQ),BV(*),IR(*),IC(*)
      DIMENSION RPAR(*),IPAR(*),IFP(*)
C***********************************************************************
C     CALL OF PDESYS
C***********************************************************************
      CALL MASRES(NEQ,TIME,Y,F,RPAR,IPAR,IEMD)
      RETURN
C***********************************************************************
C     END OF FCN
C***********************************************************************
      END
      SUBROUTINE MASLEF(NZC,B,IR,IC)
C***********************************************************************
C
C     DEFINITION OF LEFT HAND SIDE (SEE -ULIMEX-)
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION B(*),IR(*),IC(*)
      PARAMETER(ONE=1.0D0)
C***********************************************************************
C
C***********************************************************************
      DO I=1,NZC
        IR(I)=I
        IC(I)=I
      ENDDO
      B(1:NZC) = ONE
      RETURN
C***********************************************************************
C     END OF MASLEF
C***********************************************************************
      END
      SUBROUTINE MASRES(NEQ,TIME,Y,F,RPAR,IPAR,IERR)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *           RESIDUALS OF THE CONSERVATION EQUATIONS         *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C***********************************************************************
C
C     CHAPTER I: STORAGE ORGANIZATION                                  *
C
C***********************************************************************
C***********************************************************************
C     I.I.: TYPES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      LOGICAL DETONA,PCON,VCON,TEST,TCON
C***********************************************************************
C     I.II.: DIMENSIONS
C***********************************************************************
      DIMENSION Y(NEQ),F(NEQ),RPAR(*),IPAR(*)
      DIMENSION CIL(MNSPE+MNTHB+1)
      DIMENSION DGDC(1),DGDT(1),DODC(1),DODT(1)
      DOUBLE PRECISION, DIMENSION(1) :: DXDS,DTDS,DPDS
C**********************************************************************C
C     I.III.: ARRAY COMMON BLOCKS
C**********************************************************************C
C**********************************************************************C
C     I.IV.: NON ARRAY OR FIXED ARRAY COMMON BLOCKS
C**********************************************************************C
      COMMON/BOPTL/PCON,VCON,TCON,DETONA,NWALL,NCOMPLX,NPSR
      COMMON/BMIX/OMEGA,SMFMIX(MNEQU),SMFCHA(MNEQU),VDIR(MNEQU,NDIMM)
      COMMON/BSSPV/SSPV(20*MNSPE)
      COMMON/BHCP/HLHH(7,MNSPE*2),HINF(3,MNSPE)
C***********************************************************************
C     BLOCK FOR INFORMATIONS ABOUT THE REACTION MECHANISM
C***********************************************************************
      COMMON/BSENS/LSMAX,NSENS,NRSENS,NISENS,NPSENS,
     1       IRSENS,IISENS,IPSENS,ISENSI(MNSEN)
      DIMENSION WORK1(MNREA)
      DATA LRW1/MNREA/
C***********************************************************************
C     BLOCK FOR COMMUNICATION WITH SOLVER
C***********************************************************************
      COMMON/BINFM/RVMR(5)
C**********************************************************************C
C     I.V.: DATA STATEMENTS
C**********************************************************************C
      DATA nfile6/06/
      DATA zero/0.0D0/
C***********************************************************************
C                                                                      *
C     CHAPTER II: CALCULATION OF FUNCTION                              *
C                                                                      *
C***********************************************************************
      IERR = 0
      TEST = .FALSE.
C***********************************************************************
C     II.2.1: CALC. MASS FRACTION, TEMPERATURE, PRESSURE, VOLUME       *
C***********************************************************************
      NSPEC = IPAR(3)
      NE    = IPAR(6)
      IEQ   = IPAR(10)
      NH    = 1
      NP    = 2
      NWS   = 2
      NW    = 3
C***********************************************************************
C     CALCULATE MEAN MOLAR MASS AND MOLE FRACTIONS                     *
C***********************************************************************
      CALL TRASSP(IEQ,NEQ,Y,NSPEC,TL,CL,CIL,RHO,HSPEC,PL,SSPV,IEX,
     1      0,DXDS,1,DTDS,DPDS)
      IF(IEX.GT.0) GOTO 910
C***********************************************************************
C     II.2.4: CALCULATON OF MOLAR REACTION RATES                       *
C***********************************************************************
      IPRIM = 0
      CALL MECHJA(0,NSPEC,NREAC,NM,NRINS,NRTAB,
     1  IREAC(NMATLL),IREAC(NNRVEC),RREAC(NPK),RREAC(NTEXP),RREAC(NEXA),
     1  RREAC(NVT4),RREAC(NVT5),RREAC(NVT6),IREAC(NIARRH),
     2  RREAC(NXZSTO),RREAC(NRATCO),
     3  CL,CIL,TL,F(NW),NRSENS,ISENSI(IRSENS),RPAR(IRSENS),
     1  LRW1,WORK1,IPRIM,NFILE6,IERR,
     4  RREAC(NAINF),RREAC(NBINF),RREAC(NEINF),RREAC(NT3),
     5  RREAC(NT1),RREAC(NT2),RREAC(NAFA),RREAC(NBFA),HLHH,HINF,
     1     1,1,DGDC,DGDT,DODC,DODT)
C***********************************************************************
C     III.2: P GIVEN
C***********************************************************************
      F(NW:NWS+NSPEC)=F(NW:NWS+NSPEC)/RHO
      F(NH)= ZERO
      F(NP)= ZERO
      IF(OMEGA.NE.ZERO) THEN
        F(1:NEQ)=F(1:NEQ) + OMEGA * (SMFMIX(1:NEQ)-Y(1:NEQ))
      ENDIF
C***********************************************************************
C     REGULAR EXIT
C***********************************************************************
  490 CONTINUE
      RVMR(1) = TL
      RVMR(2) = RHO
      IF(IEQ.EQ.2.OR.IEQ.EQ.6) RVMR(2) = PL
      RVMR(3) = RHO/CL
      RVMR(4) = ZERO
      RVMR(5) = ZERO
      RETURN
C***********************************************************************
C     Abnormal End
C***********************************************************************
  910 CONTINUE
      WRITE(6,*) ' IEX',IEX
      IERR = 1
      RETURN
 2221 FORMAT('0',2X)
  999 CONTINUE
 2222 WRITE(NFILE6,2221)
 2223 FORMAT('0',32('+'),' ABNORMAL END ',32('+'))
      WRITE(NFILE6,2223)
      WRITE(NFILE6,2223)
      WRITE(NFILE6,2223)
      WRITE(NFILE6,2221)
      WRITE(NFILE6,2221)
      STOP
C***********************************************************************
C     END OF -DESYS-
C***********************************************************************
      END
      SUBROUTINE MASJAC(NEQ,TIME,Y,F,A,RPAR,IPAR,IERR)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *           RESIDUALS OF THE CONSERVATION EQUATIONS         *    *
C     *               IN MASS FRACTION FORMULATION                *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C***********************************************************************
C
C     CHAPTER I: STORAGE ORGANIZATION                                  *
C
C***********************************************************************
C***********************************************************************
C     I.I.: TYPES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      LOGICAL DETONA,PCON,VCON,TCON
C***********************************************************************
C     I.II.: DIMENSIONS
C***********************************************************************
      DIMENSION Y(NEQ),F(NEQ),A(NEQ,NEQ),RPAR(*),IPAR(*)
C**********************************************************************C
C     I.IV.: NON ARRAY OR FIXED ARRAY COMMON BLOCKS
C**********************************************************************C
      COMMON/BOPTL/PCON,VCON,TCON,DETONA,NWALL,NCOMPLX,NPSR
C***********************************************************************
C     BLOCK FOR INFORMATIONS ABOUT THE REACTION MECHANISM
C***********************************************************************
      COMMON/BSENS/LSMAX,NSENS,NRSENS,NISENS,NPSENS,
     1       IRSENS,IISENS,IPSENS,ISENSI(MNSEN)
C***********************************************************************
C
C***********************************************************************
      NSPEC = IPAR(3)
      IEQ   = IPAR(10)
C***********************************************************************
C     CALCULATE MEAN MOLAR MASS AND MOLE FRACTIONS                     *
C***********************************************************************
      CALL MASJAI(ieq,neq,nspec,nreac,Y,F,A,RPAR,IPAR,ierr)
      RETURN
  999 CONTINUE
      WRITE(*,*) ' ERROR in MASJAC '
      STOP
C***********************************************************************
C
C***********************************************************************
      END
      SUBROUTINE MASJAI(IEQ,NEQ,NSPEC,NREAD,Y,F,A,RPAR,IPAR,IERR)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *           RESIDUALS OF THE CONSERVATION EQUATIONS         *    *
C     *               IN MASS FRACTION FORMULATION                *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C***********************************************************************
C
C     CHAPTER I: STORAGE ORGANIZATION                                  *
C
C***********************************************************************
C***********************************************************************
C     I.I.: TYPES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      PARAMETER (ZERO = 0.0D0, ONE = 1.0D0)
C***********************************************************************
C     I.II.: DIMENSIONS
C***********************************************************************
      DOUBLE PRECISION, DIMENSION(NEQ)            :: Y,F
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)        :: A
      DOUBLE PRECISION, DIMENSION(NSPEC)          :: DODT,RAT,HI,
     1                                               HELP1,HELP2
      DOUBLE PRECISION, DIMENSION(NSPEC+MNTHB+1)  :: CIL
      DOUBLE PRECISION, DIMENSION(NSPEC,NSPEC)    :: DODC
      DOUBLE PRECISION, DIMENSION(NREAD)          :: G,DGDT
      DOUBLE PRECISION, DIMENSION(NREAD,NSPEC)    :: DGDC
      DOUBLE PRECISION, DIMENSION(NSPEC,NREAD)    :: STOICH
      DOUBLE PRECISION, DIMENSION(4,NSPEC)    :: TEST2
      DOUBLE PRECISION, DIMENSION(4,NREAD)    :: TEST1
      DOUBLE PRECISION, DIMENSION(4,NSPEC)    :: EMT
      DIMENSION RPAR(*),IPAR(*)
      DOUBLE PRECISION, DIMENSION(1) :: DXDS,DTDS,DPDS
C**********************************************************************C
C     Common blocks
C**********************************************************************C
      COMMON/BHCP/HLHH(7,MNSPE*2),HINF(3,MNSPE)
      COMMON/BSSPV/SSPV(20*MNSPE)
      COMMON/BMIX/OMEGA,SMFMIX(MNEQU),SMFCHA(MNEQU),VDIR(MNEQU,NDIMM)
C***********************************************************************
C     BLOCK FOR INFORMATIONS ABOUT THE REACTION MECHANISM
C***********************************************************************
      COMMON/BSENS/LSMAX,NSENS,NRSENS,NISENS,NPSENS,
     1       IRSENS,IISENS,IPSENS,ISENSI(MNSEN)
C**********************************************************************C
C     I.V.: DATA STATEMENTS
C**********************************************************************C
      DATA NFILE6/06/
C***********************************************************************
C                                                                      *
C     CHAPTER II: CALCULATION OF FUNCTION                              *
C                                                                      *
C***********************************************************************
      IERR = 0
C***********************************************************************
C     II.2.1: CALC. MASS FRACTION, TEMPERATURE, PRESSURE, VOLUME       *
C***********************************************************************
      NH  = 1
      NP  = 2
      NWS = 2
      NW  = 3
C***********************************************************************
C     CALCULATE MEAN MOLAR MASS AND MOLE FRACTIONS                     *
C***********************************************************************
      CALL TRASSP(ieq,neq,Y,nspec,TL,CL,CIL,RHO,HSPEC,PL,SSPV,iex,
     1         0,DXDS,1,DTDS,DPDS)
      IF(IEX.GT.0) GOTO 910
      HI(1:NSPEC)   = SSPV(18*NSPEC+1:18*NSPEC+NSPEC)
      CP=DOT_PRODUCT(SSPV(19*NSPEC+1:19*NSPEC+NSPEC),Y(NWS+1:NWS+NSPEC))
C---- HI are molar enthalpies
C---- CP is the specific heat capacity
      XMOLM = RHO / CL
C***********************************************************************
C     Calculate Jacobian of the rate equations
C***********************************************************************
      LLL = NREAD
      INFOJ = 1
C***********************************************************************
C A. Neagos: Überprüfung der Elementerhaltung
C***********************************************************************
!       TEST1=0.0D0
!       TEST2=0.0D0
!       STOICH=0.0D0
!       EMT=0.0D0
! !       WRITE(*,*)'NRINS',NRINS
! !       WRITE(*,*)'NREAD',NREAD
! !       WRITE(*,*)'NSPEC*NREAD',NSPEC*NREAD
!       DO I=1,NRINS
!       NS=IREAC(NNRVEC+3*(I-1))
!       NR=IREAC(NNRVEC+3*(I-1)+1)
!       ND=IREAC(NNRVEC+3*(I-1)+2)
!       STOICH(NS,NR)=FLOAT(IREAC(NNRVEC+3*(I-1)+2))
!       END DO
! !       WRITE(*,*)'STOICH',STOICH
!       DO J=1,4
!       DO I=3,NEQ
!       EMT(J,I-2)=EMAT(I+(J+1)*NEQ)
! !       TEST1(J,K)=TEST1(J,K)+ERVCOL(I,2+J)*STOICH(I-2,K)
! !       WRITE(*,*)' '
!       END DO
!       END DO
! !       WRITE(*,*)'EMT H',EMT(1,:)
! !       WRITE(*,*)'EMT C',EMT(2,:)
! !       WRITE(*,*)'EMT O',EMT(3,:)
! !       WRITE(*,*)'EMT N',EMT(4,:)
! !       WRITE(*,*)'EMT*STOICH',MATMUL(EMT,STOICH)
! !       STOP
!       TEST1(1:4,1:NREAD)=MATMUL(EMT,STOICH)
! !       DO I=1,4
! ! !       IF (TEST1(I,I).GT.1.0D0) THEN
! ! !       WRITE(*,*)'SMF AT 50 IN PREPRO',SMF
! ! !       WRITE(*,*)'XJAC AT 50 IN PREPRO',XJAC
! !       WRITE(*,*)'IND.:',I
! !       WRITE(*,*)'EMAT*STOICH IN MASJAI',TEST1(I,:)
! !       WRITE(*,*)'AT POINT',L
! !       DO J=1,NREAD
! !       IF (ABS(TEST1(I,J)) .GT. 1.0D-03 ) THEN
! !       WRITE(*,*)'VALUE NOT EQUAL 0 AT REACTION NR.',J
! !       END IF
! !       END DO
! !       END DO
! !       STOP
C***********************************************************************
C A. Neagos: Überprüfung der Elementerhaltung
C***********************************************************************
      CALL MECHJA(INFOJ,NSPEC,NREAC,NM,NRINS,NRTAB,
     1  IREAC(NMATLL),IREAC(NNRVEC),RREAC(NPK),RREAC(NTEXP),RREAC(NEXA),
     1  RREAC(NVT4),RREAC(NVT5),RREAC(NVT6),IREAC(NIARRH),
     2  RREAC(NXZSTO),RREAC(NRATCO),
     3  CL,CIL,TL,RAT,NRSENS,ISENSI(IRSENS),RPAR(IRSENS),
     1  LLL,G,IPRIM,NFILE6,IERR,
     4  RREAC(NAINF),RREAC(NBINF),RREAC(NEINF),RREAC(NT3),
     5  RREAC(NT1),RREAC(NT2),RREAC(NAFA),RREAC(NBFA),HLHH,HINF,
     1  NSPEC,NREAC,DGDC,DGDT,DODC,DODT)
C***********************************************************************
C A. Neagos: Überprüfung der Elementerhaltung
C***********************************************************************
! !       WRITE(*,*)'TEST222',TEST2
! !       DO K=1,NSPEC
! !       DO J=1,4
! !       DO I=3,NEQ
! !       TEST2(J,K)=TEST2(J,K)+EMAT(I+(J+1)*NEQ)*DODC(I-2,K)
! !       END DO
! !       END DO
! !       END DO
!       TEST2(1:4,1:NSPEC)=MATMUL(EMT,DODC)
! !       TEST3=0.0D0
! !       DO I=3,NEQ
! !       TEST3=TEST3+EMAT(I+3*NEQ)*DODC(I-2,16)
! !       write(*,*)'EMAT LOOP',EMAT(I+3*NEQ)
! !       write(*,*)'DODC LOOP',DODC(I-2,16)
! !       END DO
! !       write(*,*)'TEST3',TEST3
!       WRITE(*,*)'DGDC 16',DGDC(:,16)
! !       WRITE(*,*)'DODC',DODC(:,16)
      DO I=1,NREAD
      DO J=1,NSPEC
      IF (ABS(DGDC(I,J)).GT.1.0D10) THEN
!       WRITE(*,*)'MAX. VALUE OF DGDC FOR SPECIES',J,'IS',DGDC(I,J),
!      1          ' AT REAC. NR.',I
!       WRITE(*,*)'RATE OF REAC. NR.',I,'IS',G(I)
      END IF
!       IF (ABS(DGDC(I,13)).GT.1.0D15) THEN
!       WRITE(*,*)'MAX. VALUE OF DGDC 13',DGDC(I,13),' AT REAC. NR.',I
!       WRITE(*,*)'RATE OF REAC. NR.',I,'IS',G(I)
!       END IF
!       IF (ABS(DGDC(I,16)).GT.1.0D15) THEN
!       WRITE(*,*)'MAX. VALUE OF DGDC 16',DGDC(I,16),' AT REAC. NR.',I
!       WRITE(*,*)'RATE OF REAC. NR.',I,'IS',G(I)
!       END IF
      END DO
      END DO
!       DO I=1,4
! !       IF (TEST1(I,I).GT.1.0D0) THEN
! !       WRITE(*,*)'SMF AT 50 IN PREPRO',SMF
! !       WRITE(*,*)'XJAC AT 50 IN PREPRO',XJAC
!       WRITE(*,*)'IND.:',I
!       WRITE(*,*)'EMAT*DODC IN MASJAI',TEST2(I,:)
!       DO J=1,NSPEC
!       IF (ABS(TEST2(I,J)) .GT. 1.0D-03 ) THEN
!       WRITE(*,*)'VALUE NOT EQUAL 0 AT SPEC. NR.',J
!       END IF
!       END DO
!       END DO
! !       STOP
C***********************************************************************
C A. Neagos: Überprüfung der Elementerhaltung
C***********************************************************************
C***********************************************************************
C
C     Transform for Omega = omega/rho
C
C***********************************************************************
C---- the following transforms only according to the different
C---- right hand side, therefore it does not depend on the type of
C---- the equation system
C***********************************************************************
C     Transform for temperature  rho * dO / dT = do/dt  + o/T
C***********************************************************************
      DODT = DODT +  RAT / TL
C***********************************************************************
C     Transform for concentrations rho*dO/dc = do/dc  - o*(xmol/rho)
C***********************************************************************
      FAC = -  ONE / RHO
      CALL DGER(nspec,nspec,FAC,RAT,1,XMOL,1,DODC,nspec)
C***********************************************************************
C
C     Multiply with transformation matrix
C
C***********************************************************************
C---- now it becomes system dependent
C***********************************************************************
C
C     zero entries
C
C***********************************************************************
      A(1:nws,1:neq) = zero
C***********************************************************************
C
C     Entry dO/dphi
C
C***********************************************************************
      A(nw:nws+nspec,nw:nws+nspec) = DODC
C***********************************************************************
C     add correction  constant pressure and const enth.
C***********************************************************************
C---- not needed for constant density or constant temperature
      IF(IEQ.EQ.1.OR.IEQ.EQ.5) THEN
C---- calculate  rho dO/dc * phi
      HELP1(1:nspec) = MATMUL(DODC,Y(nw:nws+nspec))
C---- add correction
      HELP2(1:nspec) = HI(1:nspec) / (TL *CP ) - XMOLM
      CALL DGER(nspec,nspec,one,HELP1,1,HELP2,1,A(nw,nw),neq)
      ENDIF
C***********************************************************************
C     add correction  for temperature
C***********************************************************************
      IF(IEQ.EQ.1.OR.IEQ.EQ.2) THEN
      FAC = -ONE/(RHO*CP)
C     CALL DGER(NSPEC,NSPEC,FAC,DODT,1,HI,1,A(NW,NW),NEQ)
      A(NW:NWS+NSPEC,NW:NWS+NSPEC) = A(NW:NWS+NSPEC,NW:NWS+NSPEC)
     1  + FAC * MATMUL(RESHAPE(DODT(1:NSPEC),(/NSPEC,1/)),
     1                 RESHAPE(HI(1:NSPEC),  (/1,NSPEC/)))
      ENDIF
C***********************************************************************
C
C     Entry dO/dh  (or dO/dT)
C
C***********************************************************************
C***********************************************************************
C     calculate  rho dO/dc * (dc/dh  / rho)
C***********************************************************************
CUM   IF(IEQ.EQ.1) THEN
      IF(IEQ.EQ.1.OR.IEQ.EQ.5) THEN
      A(NW:NWS+NSPEC,NH) = - MATMUL(DODC,Y(NW:NWS+NSPEC)) / (TL*CP)
      ELSEIF(IEQ.EQ.2) THEN
      ELSEIF(IEQ.EQ.5) THEN
      A(NW:NWS+NSPEC,NH) = - MATMUL(DODC,Y(NW:NWS+NSPEC)) / TL
      ELSEIF(IEQ.EQ.6) THEN
      ELSE
      ENDIF
C***********************************************************************
C     calculate  rho dO/dT * (dT/dh  / rho)
C***********************************************************************
      IF(IEQ.EQ.1.OR.IEQ.EQ.2) THEN
      A(NW:NWS+NSPEC,NH) = A(NW:NWS+NSPEC,NH) + DODT / (RHO*CP)
      ELSEIF(IEQ.EQ.5.OR.IEQ.EQ.6) THEN
      A(NW:NWS+NSPEC,NH) = A(NW:NWS+NSPEC,NH) + DODT / RHO
      ELSE
      ENDIF
C***********************************************************************
C
C     Entry dO/dp
C
C***********************************************************************
C***********************************************************************
C     calculate  rho dO/dc * (dc/dp  / rho)
C***********************************************************************
      IF(IEQ.EQ.1.OR.IEQ.EQ.5) THEN
      A(NW:NWS+NSPEC,NP) = HELP1(1:NSPEC) / PL
      ELSEIF(IEQ.EQ.2.OR.IEQ.EQ.6) THEN
      A(NW:NWS+NSPEC,NP) = HELP1(1:NSPEC) / RHO
      ELSE
      ENDIF
C***********************************************************************
C
C      ADD term for mixing problem
C
C***********************************************************************
      IF(OMEGA.NE.ZERO) THEN
      DO I=1,NEQ
         A(I,I) = A(I,I) - OMEGA
      ENDDO
C      write(*,*)  A(1:NEQ,1:NEQ)
      ENDIF
C***********************************************************************
C     REGULAR EXIT
C***********************************************************************
      RETURN
C***********************************************************************
C     CHAPTER IV: ABNORMAL END
C***********************************************************************
  910 CONTINUE
      WRITE(6,*) ' IEX',IEX
      IERR = 1
      RETURN
  999 CONTINUE
      STOP
C***********************************************************************
C     END OF -DESYS-
C***********************************************************************
      END
      SUBROUTINE GETTAR(NEQ,ITARG,ATARG)
C***********************************************************************
C
C     get limit information
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'homdim.f'
      DIMENSION ATARG(NEQ)
      COMMON/BLIMIT/XLIMIT(MNLIM*(MNSPE+2)),ELIMIT(2,MNLIM  ),NLIM
C***********************************************************************
C     get target vector
C***********************************************************************
      CALL DCOPY(NEQ,XLIMIT(ITARG),NLIM,ATARG,1)
      RETURN
      END
      SUBROUTINE GETVDI(NEQ,NDIM,VDI,SMFMI)
C***********************************************************************
C
C     get mixing direction information
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'homdim.f'
      DIMENSION VDI(NEQ,NDIM),SMFMI(NEQ)
      COMMON/BMIX/OMEGA,SMFMIX(MNEQU),SMFCHA(MNEQU),VDIR(MNEQU,NDIMM)
C***********************************************************************
C     get VDI and SMFMI
C***********************************************************************
      SMFMI(1:NEQ) = SMFMIX(1:NEQ)
      CALL DLACPY('A',NEQ,NDIM,VDIR,MNEQU,VDI,NEQ)
      RETURN
      END
      SUBROUTINE GETNUM(NLIMI)
C***********************************************************************
C
C     get limit information
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'homdim.f'
      COMMON/BLIMIT/XLIMIT(MNLIM*(MNSPE+2)),ELIMIT(2,MNLIM  ),NLIM
C***********************************************************************
C     get target vector
C***********************************************************************
      NLIMI = NLIM
      RETURN
      END
      SUBROUTINE GETSCV(NC,NZEV,SCALD)
C***********************************************************************
C
C     get limit information
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'homdim.f'
      DIMENSION SCALD(NC)
      COMMON/BECVA/ECV(2,MNCOV)
      COMMON/BCHIS/EWF(MNCON,7),SMFF(MNEQU,7),PAT(2,MNTCO),
     1       MIXFRA,MENFRA,MPRFRA
C***********************************************************************
C     get target vector
C***********************************************************************
         SCALD(1:NZEV) =  0.05D0
         DO I = 1,MIXFRA
            SCALD(NZEV-MIXFRA+I) =  PAT(1,1+(I-1)*3)
         ENDDO
         SCALD(NZEV+1:NC) =  ECV(1,NZEV+1:NC)
      RETURN
      END
      SUBROUTINE GETPAC(NC,NZEV,NEQ,MIXFRA,CMA,TTEM)
C***********************************************************************
C
C     get limit information
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'homdim.f'
      DOUBLE PRECISION, DIMENSION(NZEV,MIXFRA)      :: TTEM
      DOUBLE PRECISION, DIMENSION(NC,NEQ)           :: CMA
      COMMON/BCHIS/EWF(MNCON,7),SMFF(MNEQU,7),PAT(2,MNTCO),
     1       MIXFR,MENFRA,MPRFRA
C***********************************************************************
C     get target vector
C***********************************************************************
        DO I=1,MIXFRA
          TTEM(1:NZEV,I) =
     1        MATMUL(CMA(1:NZEV,1:NEQ),SMFF(1:NEQ,I+1)-SMFF(1:NEQ,1))
        ENDDO
      RETURN
      END
      SUBROUTINE CONPAR(N,NZEV,MIXFRA,NC,TAUCA,MSGFIL)
C***********************************************************************
C     automatical Parameterization of the first cell
C***********************************************************************
C
C  Input:
C
C    N              : dimension of the state space
C
C    NZEV           : number of conserved quantities
C
C    NC           : number of controlling  quantities
C
C
C    MSGFIL         : fortran unit for error messages
C
C
C  Output:
C
C
C    TAUCA(NC,N)    : matrix for conserved quantities
C                    matrix for controling quantities
C                     row partition are the vectors
C
C***********************************************************************
C     STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      PARAMETER (ONE = 1.0D0, ZERO = 0.0D0)
C***********************************************************************
C     Arrays
C***********************************************************************
      DOUBLE PRECISION, DIMENSION((NC+1)*N)  :: RW
      INTEGER,          DIMENSION(N)         :: IW
      DOUBLE PRECISION, DIMENSION(NC,N)      :: TAUCA
      DOUBLE PRECISION, DIMENSION(NZEV,NZEV) :: TTEM,TTEX
      DOUBLE PRECISION, DIMENSION(NC,NC)     :: HMAT
      DOUBLE PRECISION, DIMENSION(NC,N)      :: COQ
C***********************************************************************
C
C***********************************************************************
      COMMON/BICMAT/COMAT(MNCOV*MNEQU),EMAT((MNSPE+2)*MNCON)
C***********************************************************************
C
C***********************************************************************
      LRW = (NC+1) * N
      LIW = N
C***********************************************************************
C     Initialize COQ
C***********************************************************************
      DO I=1,NC
      DO J=1,N
      COQ(I,J) = COMAT(I+(J-1)*NC)
      ENDDO
      ENDDO
C***********************************************************************
C
C     Calculate matrix for conserved variable tabulation
C
C***********************************************************************
      IF(MIXFRA.EQ.0) THEN
         CALL DLASET('Full',NC,NC,ZERO,ONE,HMAT,NC)
      ELSE
      CALL GETPAC(NC,NZEV,N,MIXFRA,COQ,TTEM)
         DO j=1,n
         ENDDO
         DO j=1,NZEV
         ENDDO
        CALL ORBAS(NZEV,MIXFRA,NZEV,TTEM,NZEV,TTEX,LRW,RW,LIW,IW,2,IERR)
        TTEM(:,1:NZEV-MIXFRA)      = TTEX(:,MIXFRA+1:NZEV)
        TTEM(:,NZEV-MIXFRA+1:NZEV) = TTEX(:,1:MIXFRA)
        INFO=0
        CALL DGEINV(NZEV,TTEM,RW,IW,INFO)
        IF(INFO.NE.0) GOTO 940
        CALL DLASET('Full',NC,NC,ZERO,ONE,HMAT,NC)
        HMAT(1:NZEV,1:NZEV) = TTEM(1:NZEV,1:NZEV)
      ENDIF
      TAUCA = MATMUL(HMAT,COQ)
C***********************************************************************
C     Solution exit
C***********************************************************************
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
C---- ERROR IN INVERSION OF MATRIX
  940 WRITE(MSGFIL,998) IERR
  998 FORMAT(' ','  Error in -CONPAR-, IERR=',I3)
      STOP
C***********************************************************************
C     END OF -CONPAR-
C***********************************************************************
      END
      SUBROUTINE INIERV(N,NCSQ,AROW,ACOL)
C***********************************************************************
C     automatical Parameterization of the first cell
C***********************************************************************
C
C  Input:
C
C    N              : dimension of the state space
C
C    NCSQ           : number of conserved quantities
C
C    CSQ(N,NCSQ)    : matrix for conserved quantities
C                     column partition are the vectors
C
C
C  Output:
C
C    ACOL(n,n)      : transformation matrix for the state space
C                     to separate conserved and non-conserved quantities
C                     first NCSQ column vectors are the vectors of the
C                     conserved scalars, the remaining vectors make
C                     up the orthogonal complement
C
C    AROW(N,N)      : inverse of ACOL
C
C
C***********************************************************************
C     STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
C***********************************************************************
C     Arrays
C***********************************************************************
      DIMENSION AROW(N,N),ACOL(N,N)
      DIMENSION CSQ(N,NCSQ)
      INTEGER,          DIMENSION(10*N)   :: IW
      DOUBLE PRECISION, DIMENSION(N)      :: RW
      COMMON/BICMAT/COMAT(MNCOV*MNEQU),EMAT((MNSPE+2)*MNCON)
C***********************************************************************
C     INITIAL CHECKS
C***********************************************************************
      LIW = 10*N
      LRW =    N
      NN = N * N
C***********************************************************************
C
C***********************************************************************
!       write(*,*)'NCSQ',NCSQ
!       stop
      DO I=1,N
      DO J=1,NCSQ
      CSQ(I,J) = EMAT(I+(J-1)*N)
      ENDDO
      ENDDO
!      write(*,*)'EMAT',CSQ
C***********************************************************************
C     Compute conserved scalar matrix
C***********************************************************************
C---- AROW used as work array
      CALL ORBAS(N,NCSQ,N,CSQ,N,ACOL,NN,AROW,LIW,IW,1,IERR)
!      write(*,*)'ACOL',ACOL(:,1:NCSQ)
      IF(IERR.GT.0) GOTO 900
      CALL DCOPY(NN,ACOL,1,AROW,1)
      CALL DGEINV(N,AROW,RW,IW,IERR)
      IF(IERR.GT.0) GOTO 910
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
  900 CONTINUE
      WRITE(*,*) ' FATAL error 1  in INIERV '
      STOP
C---- VECTORS OF CONSERVED SCALERS LINEARLY INDEPENDENT
  910 CONTINUE
      WRITE(*,*) ' FATAL error 2  in INIERV '
      STOP
C***********************************************************************
C     END OF -INIERV-
C***********************************************************************
      END
