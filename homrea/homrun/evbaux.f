      SUBROUTINE UNISET(NDIM,N2DIM,IUNIT,IPAIR)
C***********************************************************************
C
C     ************************************************************
C     *                                                          *
C     *   Set generic cell information vectors                   *
C     *                                                          *
C     ************************************************************
C
C***********************************************************************
C
C  Arguments
C  =========
C
C  NDIM   (input) INTEGER
C          dimension of the cell 
C
C  N2DIM   (input) INTEGER
C          2**NDIM
C
C  IUNIT   (output) INTEGER array, dimension (NDIM,N2DIM)
C          For cell vertex I IUNIT(J,I) tells the position of the vertex
C          in the J-direction (0 or 1). Basically IUNIT(J,I) are the  
C          digits of I-1 in the binary system.
C
C  IPAIR   (output) INTEGER array, dimension (NDIM,2,*) (*=N2DIM/2)
C          For direction I IPAIR(I,1,J) and IPAIR(I,2,J) tell the
C          N2DIM/2 vertex pairs in this direction (J=1,2,...,N2DIM/2)
C
C***********************************************************************
C     Storage organization
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL TEST
      DIMENSION IUNIT(NDIM,N2DIM),IPAIR(NDIM,2,N2DIM/2)
C***********************************************************************
C     Initialize 
C***********************************************************************
      TEST = .TRUE.
C***********************************************************************
C
C     Set up Unit vectors
C
C***********************************************************************
      DO I = 1 , N2DIM
        I1 = I - 1
        DO JJ = 1, NDIM
          J = NDIM + 1 - JJ
          J1 = J - 1
          IHH = I1 / 2**J1
          IUNIT(J,I) = IHH
          IF( IHH .GE. 1 ) I1 = I1 - 2**J1
        ENDDO   
      ENDDO   
C***********************************************************************
C
C     Determine vertex pairs
C
C***********************************************************************
C***********************************************************************
C     Loop over dimensions 
C***********************************************************************
      DO 100 K=1,NDIM
C---- Set pointers to zero 
      IPAIR(K,1,1:N2DIM/2) = 0
      IPAIR(K,2,1:N2DIM/2) = 0
      NFP = 0
C---- Loop over vertices
      DO 190 I=1,N2DIM
      DO 180 J=1,I
      DO KK=1,NDIM
C---  positions in other than K-direction differ
      IF(KK.NE.K.AND.(ABS(IUNIT(KK,I)-IUNIT(KK,J)).NE.0)) GOTO 180
C---  positions in K-direction does not differ
      IF(KK.EQ.K.AND.(ABS(IUNIT(KK,I)-IUNIT(KK,J)).NE.1)) GOTO 180
      ENDDO    
C---  if we are here, a vertex pair has been found for direction K
      NFP = NFP+1
      IPAIR(K,1,NFP) = J
      IPAIR(K,2,NFP) = I
  180 CONTINUE
  190 CONTINUE
      IF(NFP.NE.N2DIM/2) GOTO 910
  100 CONTINUE
C***********************************************************************
C     Test output
C***********************************************************************
      IF(TEST) THEN
        DO K=1,NDIM
          WRITE(*,*) ' vertex pairs for direction',K,':'
          DO I=1,N2DIM/2
            WRITE(*,*) IPAIR(K,1,I),' and ',IPAIR(K,2,I)
          ENDDO 
        ENDDO
      ENDIF
C***********************************************************************
C     Regular exit 
C***********************************************************************
      RETURN
C***********************************************************************
C     Error exit
C***********************************************************************
  910 CONTINUE
      WRITE(*,*) ' FATAL ERROR IN UNISET '
      STOP
C***********************************************************************
C     End of UNISET
C***********************************************************************
      END
      SUBROUTINE IFNVER(INFO,I1,NDIM,N2DIM,IVERT,IRET,IUNIT,NNV,INV)
C***********************************************************************
C
C     ************************************************************
C     *                                                          *
C     *    Find the direct neighbours of a vertex                *
C     *                                                          *
C     ************************************************************
C
C  Arguments
C  =========
C
C  INFO    (input) INTEGER
C          information for handling the return flag IRET  
C          INFO <  0 allow vertices with IRET(J) <= 0
C          INFO =  0 allow vertices with IRET(J) = 0
C          INFO =  1 allow all vertices
C
C  I1      (input) INTEGER
C          index of the vertex for which a direct neighbour is saught
C
C  NDIM    (input) INTEGER
C          dimension of the cell
C
C  N2DIM   (input) INTEGER
C          2**NDIM
C
C  IVERT   (input) INTEGER array, dimension (N2DIM)
C          poiters to the N2DIM vertices of the cell
C
C  IRET    (input) INTEGER array, dimension (*)
C          flags of the vertices
C
C  IUNIT   (input) INTEGER array, dimension (NDIM,N2DIM)
C          For cell vertex I IUNIT(J,I) tells the position of the vertex
C          in the J-direction (0 or 1). Basically IUNIT(J,I) are the  
C          digits of I-1 in the binary system.
C
C  NNV     (output) INTEGER 
C          number of direct neighbours (differing only in one direction)
C          found for vertex I1
C
C  INV     (output) INTEGER array, dimension (*)
C          INV returns the NNV direct neighbouring vertices which have  
C          fulfilled the conditions for the return code IRET
C
C***********************************************************************
C***********************************************************************
C     Storage organization
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL TEST
      DIMENSION IVERT(N2DIM),IUNIT(NDIM,N2DIM),IRET(*),INV(N2DIM)
C***********************************************************************
C     Initialize 
C***********************************************************************
      TEST = .FALSE.
      NNV    = 0
      IVI    = IVERT(I1)
      IVRET  = IRET(IVI)
C***********************************************************************
C     loop over all other vertices
C***********************************************************************
      DO 110 J = 1,N2DIM
C---- Set values for vertex J and overjump if vert. has a bad ret.code
      IF(J.EQ.I1) GOTO 110
      IVJ    = IVERT(J)
      JVRET = IRET(IVJ)
      IF(INFO.LT.0.AND.JVRET.GT.0) GOTO 110
      IF(INFO.EQ.0.AND.JVRET.NE.0) GOTO 110
      IFD = 0
      DO 105 K = 1 , NDIM
      IDDT = IUNIT(K,I1) - IUNIT(K,J)
      IF(IDDT.NE.0) THEN
        IF(IFD.EQ.0) THEN
          IFD = IFD + 1
          IVPD = K
          IDD = IDDT
        ELSE
          GOTO 110
        ENDIF
      ENDIF
  105 CONTINUE
      NNV = NNV + 1
      INV(NNV) = J
      IF(TEST) WRITE(6,881) I1,IVRET,J,JVRET,
     1     (IUNIT(K,I1),K=1,NDIM),10,(IUNIT(K,J),K=1,NDIM)
  110 CONTINUE
C***********************************************************************
C     Regular end
C***********************************************************************
      RETURN
C***********************************************************************
C     Format statements
C***********************************************************************
  881 FORMAT(1X,' Vertex pair',I4,'(',I3,') and',I4,'(',I3,')',
     1           5X,8(I1,1X))
C***********************************************************************
C     End         
C***********************************************************************
      END
      SUBROUTINE CRECEL(INFO,ICF,NDIM,N2DIM,IY,IVERT,
     1      NVERTM,NVERT,ICORD,IRET,IUNIT,NFIOUT)
C***********************************************************************
C
C     ************************************************************
C     *                                                          *
C     ************************************************************
C
C  Arguments
C  =========
C
C  INFO    (input) INTEGER
C          task
C          INFO = 0: cell created, if all old vertices have flag = 0
C          INFO = 1: cell created, if at least one old vertex 
C                    has flag = 0
C          INFO = 2: cell created, if all old vertices 
C                    have flag < 0
C          INFO = 3: cell created, if at least one old vertex   
C                    has flag < 0
C          INFO = 4: cell created in any case
C          INFO = 5: cell created, if N2DIM vertices are 
C                    already known
C
C  ICF     (output) INTEGER
C          cell flag
C          ICF = 1: cell has been created
C          ICF = 0: cell has not been created
C
C  NDIM    (input) INTEGER
C          dimension of the cell
C
C  N2DIM   (input) INTEGER
C          2**NDIM
C
C  IY      (input) INTEGER array, dimension (NDIM)
C          coordinates of the cell
C
C  IVERT   (output) INTEGER array, dimension (N2DIM)
C          pointers to the vertices of the cell
C
C  NVERTM  (input) INTEGER
C          maximum number of vertices 
C
C  NVERT   (input/output) INTEGER
C          on input NVERT denotes the number of vertices found 
C          so far, 
C          on output NVERT denotes the new number of vertices, i.e., 
C          the old number plus the vertices of the cell which had not 
C          yet belonged to the old mesh 
C
C  ICORD   (input/output) INTEGER array, dimension (NDIM,NVERTM)
C          on input ICORD denotes the coordinates of the old NVERT
C          vertices,
C          on output ICORD denotes the coordinates of the new NVERT
C          vertices (the old coordinates have not been changed)
C
C  IRET    (input/output) INTEGER array, dimension (NVERTM)
C          on input IRET contains the flags of the vertices found so 
C          far,
C          on output IRET contains these flag, and the flags of the 
C          new vertices are set to 10
C
C  IUNIT   (input) INTEGER array, dimension (NDIM,N2DIM)
C          For cell vertex I IUNIT(J,I) tells the position of the vertex
C          in the J-direction (0 or 1). Basically IUNIT(J,I) are the
C          digits of I-1 in the binary system.
C
C  NFIOUT  (input) INTEGER
C          FORTRAN unit for output messages
C
C***********************************************************************
C***********************************************************************
C     Storage organization
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL SAME,TEST
      DIMENSION IY(NDIM),IVERT(N2DIM),ICORD(NDIM,NVERTM)
      DIMENSION IUNIT(NDIM,N2DIM),IRET(NVERTM)
      INTEGER, DIMENSION(NDIM,N2DIM)     ::  IVC
C***********************************************************************
C     Initialize
C***********************************************************************
      TEST = .TRUE.
C***********************************************************************
C     Calculate the coordinates of the different vertices
C***********************************************************************
      DO I = 1 , N2DIM
      DO J = 1 , NDIM
      IVC(J,I) = IY(J) + IUNIT(J,I)
      ENDDO     
      ENDDO 
C***********************************************************************
C     Check if the vertex already exists
C***********************************************************************
      INV  = 0
      IAD1 = 0
      IAD2 = 0
      IAD3 = 0
      IAD4 = 0
      IAD5 = 0
      DO 40 I = 1 , N2DIM
      DO 30 J = 1 , NVERT
      SAME = .TRUE.
      DO 20 K = 1 , NDIM
      IF(IVC(K,I).NE.ICORD(K,J)) THEN
        SAME = .FALSE.
        GOTO 30
      ENDIF
   20 CONTINUE
C---- if we are here, the vertex has been found
      IVERT(I) = J
                        IAD1 = IAD1 + 1
      IF(IRET(J).EQ.0)  IAD2 = IAD2 + 1
      IF(IRET(J).LT.0)  IAD3 = IAD3 + 1
      IF(IRET(J).GT.0)  IAD4 = IAD4 + 1
      IAD5 = IAD5 + 1
      GOTO 40
   30 CONTINUE
C---- if we are here, the vertex has not been found
C     IF(INFO.EQ.5) GOTO 40
      NVERT = NVERT + 1
      INV = INV + 1
      IF(NVERT.GT.NVERTM) GOTO 910
      IVERT(I) = NVERT
      IRET(NVERT) = 10
      ICORD(1:NDIM,NVERT) = IVC(1:NDIM,I)
   40 CONTINUE
C***********************************************************************
C     check if new vertices have a reasonable neighbouring vertex
C***********************************************************************
C     IAD1 is the total number of vertices found in the previous mesh
C     IAD2 is the number of vertices with flag = 0
C     IAD3 is the number of vertices with flag < 0
C     IAD4 is the number of vertices with flag > 0
C     IAD5 is the number of vertices that have been found
C***********************************************************************
C     set the cell flag to 1
C***********************************************************************
      ICF = 1
C***********************************************************************
C     check the cell
C***********************************************************************
      IF(INFO.EQ.0) THEN
        IF(IAD2.NE.IAD1)      ICF = 0
      ELSE IF(INFO.EQ.1) THEN
        IF(IAD2.EQ.0)         ICF = 0
      ELSE IF(INFO.EQ.2) THEN
        IF(IAD2+IAD3.NE.IAD1) ICF = 0
      ELSE IF(INFO.EQ.3) THEN
        IF(IAD2+IAD3.EQ.0)    ICF = 0
      ELSE IF(INFO.EQ.4) THEN
      ELSE IF(INFO.EQ.5) THEN
        IF(IAD5.NE.N2DIM)     ICF = 0
      ELSE    
       GOTO 900
      ENDIF
C***********************************************************************
C     set back NVERT if the cell won't be created 
C***********************************************************************
      IF(ICF.EQ.0) NVERT = NVERT - INV 
C***********************************************************************
C     Test output
C***********************************************************************
      IF(TEST.AND.ICF.EQ.1) THEN
      WRITE(NFIOUT,811)
      DO I=1,N2DIM
      WRITE(NFIOUT,810) I,IRET(IVERT(I)),IVERT(I),(IVC(J,I),J=1,NDIM)
      ENDDO    
      ENDIF
C***********************************************************************
C     Regular end
C***********************************************************************
      RETURN
C***********************************************************************
C     Error exits
C***********************************************************************
  900 CONTINUE
      WRITE(NFIOUT,901) INFO
  901 FORMAT(' Wrong value of INFO:',I5)
      GOTO 999
  910 CONTINUE
      WRITE(NFIOUT,911) NVERTM
  911 FORMAT(' Number of vertices exceeds ',I5)
      GOTO 999
  999 CONTINUE
      WRITE(NFIOUT,998)
  998 FORMAT(3(' Error in CRECEL ',/))
      STOP
C***********************************************************************
C     Formats 
C***********************************************************************
  811 FORMAT(' Cell handled by CRECEL',/,
     1         ' vertex | flag | vert.no.| coordinates')
  810 FORMAT(2X,I4,1X,I6,1X,I10,3X,12(1X,I4))
C***********************************************************************
C     end of CRECEL 
C***********************************************************************
      END
      SUBROUTINE UPDPAR(NC,NLCO,NEQ,PMCOL,PMROW,NFIOUT)
C***********************************************************************
C
C     ************************************************************
C     *                                                          *
C     *   Update the parametrization matrix                      *
C     *                                                          *
C     *                                                          *
C     ************************************************************
C
C***********************************************************************
C
C
C  Arguments
C  =========
C
C  NC      (input) INTEGER   
C          number of direction vectors
C
C  NLCO    (input) INTEGER   
C          number of direction vectors that are not adjusted 
C
C  NEQ     (input) INTEGER
C          dimension of the direction vectors
C
C  PMCOL   (input) DOUBLE PRECISION array, dimension (NEQ,NC)
C          direction vectors
C
C  PMROW   (input/output) DOUBLE PRECISION array, dimension (NC,NEQ)
C          parametrization vectors.
C          On return the first NLCO rows of PMROW remain unchanged.
C          The remaining NC-NLCO rows are overwritten by 
C          (P^T * P)^{-1} * P^T,
C          where P denotes the matrix formed by the columns 
C          NLCO+1 ... NC of PMCOL            
C
C  LIW     (input) INTEGER
C          length of supplied integer work array
C
C  IW      (work) INTEGER array, dimension (LIW)
C          integer work array
C
C
C***********************************************************************
C     Storage organization
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      PARAMETER (ZERO = 0.0D0, ONE = 1.0D0)
      DIMENSION PMCOL(NEQ,*),PMROW(NC,*)
      DOUBLE PRECISION, DIMENSION(NC-NLCO,NC-NLCO)   :: HM
      DOUBLE PRECISION, DIMENSION(NC-NLCO)           :: HA
C***********************************************************************
C     Initialize
C***********************************************************************
C***********************************************************************
C
C     Calculate the transformation matrix
C
C***********************************************************************
      NNCV = NC-NLCO
      IF(NNCV.NE.0) THEN
        CALL  MPPSIC(NEQ,NNCV,
     1       PMCOL(1,NLCO+1),NEQ,PMROW(NLCO+1,1),NC,IERI)
        IF(IERI.GT.0) GOTO 930 
      ENDIF
C***********************************************************************
C     Regular end
C***********************************************************************
      RETURN
C***********************************************************************
C     Error exits
C***********************************************************************
  930 CONTINUE
      WRITE(NFIOUT,931) 
  931 FORMAT(' Error during matrix inversion in evbaux.f, -UPDPAR- ')
      STOP   
C***********************************************************************
C     End   
C***********************************************************************
      END
      SUBROUTINE DIRCEL(INFO,IFMI,IFMA,NDIM,N2DIM,IVERT,
     1 IRET,IPAIR,PROP,LDPROP,NEQ,PMCOL,NFIOUT,IERR)
C***********************************************************************
C
C     ************************************************************
C     *                                                          *
C     *     Calculate direction vectors of a cell                *
C     *                                                          *
C     ************************************************************
C
C***********************************************************************
C
C     INFO       control flag
C
C
C
C  Arguments
C  =========
C
C  INFO    (input) INTEGER
C          control flag
C           |INFO| = 1:  update the directions using the vectors
C                        of the cell which start at the origin
C           |INFO| = 2:  update the directions using the aithmetic 
C                        mean of the edges 
C
C  IFMI    (input) INTEGER
C          minimum value of IRET which is allowed to consider a vertex
C
C  IFMA    (input) INTEGER
C          maximum value of IRET which is allowed to consider a vertex
C
C  NDIM    (input) INTEGER
C          dimension of the cell
C
C  N2DIM   (input) INTEGER
C          2**NDIM
C
C  IVERT   (input) INTEGER array, dimension (N2DIM)
C          poiters to the N2DIM vertices of the cell
C
C  IRET    (input) INTEGER array, dimension (*)
C          flags of the vertices
C
C  IPAIR   (input) INTEGER array, dimension (NDIM,2,*) (*=N2DIM/2)
C          For direction I IPAIR(I,1,J) and IPAIR(I,2,J) tell the
C          N2DIM/2 vertex pairs in this direction (J=1,2,...,N2DIM/2)
C
C  PROP    (input) DOUBLE PRECISION array (LDPROP,*)
C          expected to contain the NEQ variables used for the update 
C          of the directions 
C
C  LDPROP  (input) INTEGER
C          leading dimension of PROP
C
C  NEQ     (input) INTEGER
C          dimension of the update vectors
C
C  PMCOL   (input/output ) DOUBLE PRECISION array (NEQ,*)
C          contains the updated direction vectors 
C
C  IERR    (ouput) INTEGER
C          error flag, 0 for regular exit
C
C
C***********************************************************************
C     Storage organization
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL TEST
      PARAMETER (ZERO = 0.0D0, ONE = 1.0D0)
      DIMENSION IVERT(N2DIM),IRET(*),IPAIR(NDIM,2,*)
      DIMENSION PMCOL(NEQ,*),PROP(LDPROP,*)
      DOUBLE PRECISION, DIMENSION(NEQ)    :: HELP 
C***********************************************************************
C     Initialize
C***********************************************************************
      TEST = .FALSE.
      IF(TEST) WRITE(NFIOUT,803)
C***********************************************************************
C
C     Update vectors using the corner vectors in the origin
C
C***********************************************************************
      IF(ABS(INFO).EQ.1) THEN
        IERR = 0
        I0 = IVERT(1)
        DO 110 I = 1,NDIM
          IACT = 1+ 2**(I-1)
          I1  = IVERT(IACT)
          IR1 = IRET(I1)
          IF(IR1.LT.IFMI.OR.IR1.GT.IFMA) THEN
            IERR = NDIM
            RETURN
          ENDIF
          PMCOL(1:NEQ,I) = PROP(1:NEQ,I1) - PROP(1:NEQ,I0)
  110   CONTINUE
C***********************************************************************
C
C     Calculate the cell directions based on vertex pairs
C
C***********************************************************************
      ELSE IF(ABS(INFO).EQ.2) THEN
C***********************************************************************
C     Loop over directions
C***********************************************************************
      IERR = 0
      DO 250 K=1,NDIM
        HELP(1:NEQ) = ZERO
        IFGP = 0
        DO 230 J=1,N2DIM/2
          NI = IVERT(IPAIR(K,2,J))
          NJ = IVERT(IPAIR(K,1,J))
          IF(IRET(NI).LE.IFMA.AND.IRET(NJ).LE.IFMA  .AND.
     1     IRET(NI).GE.IFMI.AND.IRET(NJ).GE.IFMI) THEN
          IFGP = IFGP + 1
          HELP(1:NEQ) = HELP(1:NEQ) + PROP(1:NEQ,NI) - PROP(1:NEQ,NJ)
        ENDIF
  230 CONTINUE
      IF(IFGP.EQ.0) THEN
        IERR = IERR + 1
        IF(TEST) WRITE(NFIOUT,801) K
      ELSE
        IF(TEST) WRITE(NFIOUT,802) K,IFGP
        PMCOL(1:NEQ,K) = HELP(1:NEQ) / FLOAT(IFGP)
      ENDIF
  250 CONTINUE
      ELSE
        GOTO 900
      ENDIF
C***********************************************************************
C
C     Check results and produce output for testing
C
C***********************************************************************
C***********************************************************************
C     Output for testing
C***********************************************************************
      IF(TEST) THEN
      DO I=1,NDIM
      DO J=1,I
       SC1 = DDOT(NEQ,PMCOL(1,I),1,PMCOL(1,I),1)
       SC2 = DDOT(NEQ,PMCOL(1,J),1,PMCOL(1,J),1)
       SCP = DDOT(NEQ,PMCOL(1,I),1,PMCOL(1,J),1)
       COSA = SCP / DSQRT(SC1*SC2)
       ANGL = 360.0* DACOS(COSA)/(2.0d0*3.141592d0)
       WRITE(NFIOUT,805) I,J,ANGL
      ENDDO    
      ENDDO    
      ENDIF
C***********************************************************************
C     Regular end
C***********************************************************************
      RETURN
C***********************************************************************
C     Error exits
C***********************************************************************
  900 CONTINUE
      WRITE(NFIOUT,*) ' INFO =',INFO,' not available '   
      GOTO 999
  999 CONTINUE
      WRITE(NFIOUT,998)
  998 FORMAT(3(' Error in DIRCEL ',/))
      STOP
C***********************************************************************
C     Format statements 
C***********************************************************************
  803 FORMAT(' Test output from DIRCEL ')
  805 FORMAT(' ',' Vertex',I3,' and vertex',I3,' form an angle of',F7.2,
     1           ' degrees')
  802 FORMAT(' vector for direction',I2,' updated',
     1                  ' using',I3,' good vertex pairs ')
  801 FORMAT(' no valid vertex pair found for direction',I2)
C***********************************************************************
C     End of DIRCEL
C***********************************************************************
      END
      SUBROUTINE DELCEL(INFO,ICELL,NCELL,NVERT,NDIM,N2DIM,NPROP,
     1   IRET,ICORD,PROP,NEQ,ISMF,
     1   IREC,VREC,IVERT,IY,NCELLN,NVERTN,NFIOUT)
C***********************************************************************
C
C     ************************************************************
C     *                                                          *
C     ************************************************************
C
C  Arguments
C  =========
C
C  INFO    (input) INTEGER
C       INFO = 0:   delete if at least one vertex is bad
C       INFO = 1:   delete if no vertex is bad, no new boundaries, but old b.
C       INFO = 2:   delete if cell has boundary points
C       INFO = 3:   delete if cell has boundary points
C                     boundaryies not determined by flag, but according to
C                     missing neighbouring vertices
C       INFO < 0:   delete if no vertex is bad and vertices doe not pass checks
C
C  ICELL   (input) Index of cell to be analyzed
C
C  NCELL   (input) INTEGER
C          initial number of cells    
C
C  NVERT   (input) INTEGER
C          initial number of vertices 
C
C  NDIM    (input) INTEGER
C
C  N2DIM   (input) INTEGER
C          2**NDIM
C
C  NPROP   (input) INTEGER
C          number of properties in array prop
C
C  IRET    (input/output) INTEGER array, dimension (NVERTM)
C          IRET contains the flags of the vertices found so 
C
C  PROP    (input/output) DOUBLE PRECISION array, 
C          dimension (NPROP,NVERT)
C          PROP contains the properties
C
C  IVERT   (input/output) INTEGER array, dimension (N2DIM,NCELL)
C          pointers to the vertices of the cell
C
C  IY      (input) INTEGER array, dimension (NDIM,NCELL)
C          coordinates of the cells
C
C  ICORD   (input/output) INTEGER array, dimension (NDIM,NVERTM)
C          on input ICORD denotes the coordinates of the old NVERT
C          vertices,
C          on output ICORD denotes the coordinates of the new NVERT
C          vertices (the old coordinates have not been changed)
C
C  NCELLN  (output) INTEGER
C          new  number of cells    
C
C  NVERTN  (output) INTEGER
C          new  number of vertices 
C
C  NFIOUT  (input) INTEGER
C          FORTRAN unit for output messages
C
C***********************************************************************
C***********************************************************************
C     Storage organization
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL SAME,TEST
C---- vertex properties 
      INTEGER, DIMENSION(NVERT)                :: IRET 
      INTEGER, DIMENSION(NDIM,NVERT)           :: ICORD 
      DOUBLE PRECISION, DIMENSION(NPROP,NVERT) :: PROP
C---- cell properties
      INTEGER, DIMENSION(N2DIM,NCELL)          :: IVERT
      INTEGER, DIMENSION(NDIM,NCELL)           :: IY
      DIMENSION NEIGH(2,NDIM)
C***********************************************************************
C     Initialize
C***********************************************************************
      TEST = .FALSE.
      NCELLN = NCELL
      NVERTN = NVERT
C***********************************************************************
C     check vertices
C***********************************************************************
      NBAD  = 0
      NBOUO = 0
      NBOUN = 0
      NOBND = 0
      NOBAV = 0
      NOVFU = 0
      NADCH = 0
      NOCL  = 0
      DO I=1,N2DIM
        IVTEM = IVERT(I,ICELL)
        IRETTE = IRET(IVTEM)
        IF(IRETTE.LT.-1) THEN
          NBAD = NBAD + 1
          CALL CHKUDO(NEQ,PROP(ISMF,IVTEM),IRCH)
          IF(IRCH.EQ.0) NOBND = NOBND + 1
        ENDIF
        IIBO = 0
        IF(IRETTE.EQ.-1) THEN
          IIBO = -1
          NBOUO = NBOUO + 1
          CALL CHKDOM(0,NEQ,PROP(ISMF,IVTEM),PROP(ISMF,IVTEM),
     1              IRCH,VHIT,VALUE)
          IF(IRCH.EQ.0) THEN
            NBOUN = NBOUN + 1 
            IIBO = 1
          ENDIF 
          CALL CHKUDO(NEQ,PROP(ISMF,IVTEM),IRCH)
          IF(IRCH.EQ.0) NOBND = NOBND + 1
        ENDIF
        IF(IIBO.NE.1.AND.PROP(IREC,IVTEM).GT.VREC) NADCH = NADCH + 1
        IF(INFO.EQ.3) THEN
          IMES = 0
          call CHKNEI(IVTEM,nvert,ndim,ICORD,NEIGH,ierr,IRET,IMES)
          IF(IERR.NE.0.AND.IRET(IVTEM).NE.0) NOBAV = NOBAV + 1
        ENDIF
        IF(INFO.EQ.4.OR.INFO.EQ.5) THEN
          CALL CHKUDO(NEQ,PROP(ISMF,IVTEM),IRCH)
          IF(IRCH.EQ.0) NOVFU = NOVFU +1
        ENDIF
      ENDDO 
      WRITE(*,*) NBOUO,' old boundary vertices '
      WRITE(*,*) NBOUN,' new boundary vertices '
      WRITE(*,*) NBAD,'  bad vertices          '
      WRITE(*,*) NOBND,' old boundary verte in update domain      ' 
      WRITE(*,*) NADCH,' vertices not passing additional check   '
      WRITE(*,*) NOFVU,' vertices fulfilling domain condition    '
      IF(INFO.EQ.0.AND.NBAD.GT.0) GOTO 500 
      IF(INFO.EQ.1.AND.NBAD.EQ.0.AND.NBOUN.EQ.0.AND.NBOUO.GT.0) GOTO 500
C---- delete cell if it was a boundary cell and is in ther new domain
      IF(INFO.EQ.2.AND.NOBND.NE.0) GOTO 500
      IF(INFO.LT.0.AND.NBAD.EQ.0.AND.NADCH.GT.0) GOTO 500
      IF(INFO.EQ.3.AND.NOBAV.NE.0) GOTO 500
      IF(INFO.EQ.4.AND.(NBAD.NE.0.OR.NBOUO.NE.0).AND.NOVFU.EQ.N2DIM)
     1     THEN 
        GOTO 500
      ENDIF
      IF(INFO.EQ.5.AND.NOVFU.EQ.N2DIM)
     1     THEN 
        GOTO 500
      ENDIF
      WRITE(*,*)  ' cell ',ICELL,' not deleted'
      GOTO 600
C***********************************************************************
C
C     Delete cell 
C
C***********************************************************************
  500 CONTINUE
      WRITE(*,*)  ' delete cell '
C***********************************************************************
C     check vertices for deletion
C***********************************************************************
      DO I=1,N2DIM
      IVTEM = IVERT(I,ICELL)
C---- check all other cells whether they have this vertex
        DO K=1,NCELL
        IF(K.NE.ICELL) THEN
          DO J=1,N2DIM
            IVTO = IVERT(J,K)
            IF(IVTO.EQ.IVTEM) GOTO 200
          ENDDO
        ENDIF
        ENDDO
C***********************************************************************
C     vertex no longer needed, delete it 
C***********************************************************************
      WRITE(NFIOUT,*) ' Vertex',IVTEM,' will be deleted'
C---- IVTEM: index of vertex to be replaced
C---- simplification: replace by last vertex
      PROP(1:NPROP,IVTEM) = PROP(1:NPROP,NVERTN)
      ICORD(1:NDIM,IVTEM) = ICORD(1:NDIM,NVERTN)
      IRET(IVTEM)         = IRET(NVERTN)
C---- change indices that point to formerly last vertex
        DO K=1,NCELL
        DO J=1,N2DIM
          IF(IVERT(J,K).EQ.NVERTN) IVERT(J,K) = IVTEM 
        ENDDO
        ENDDO
      NVERTN = NVERTN - 1
 200  CONTINUE
C***********************************************************************
C     end of loop for vertex deletion    
C***********************************************************************
      ENDDO 
C***********************************************************************
C     delete cell
C***********************************************************************
      IVERT(:,ICELL) = IVERT(:,NCELLN)
      IY(:,ICELL)    = IY(:,NCELLN)
      NCELLN = NCELL - 1
C***********************************************************************
C     Regular end
C***********************************************************************
      WRITE(*,*) ' Cell',ICELL,' has been deleted'
  600 CONTINUE
      RETURN
C***********************************************************************
C     Error exits
C***********************************************************************
  900 CONTINUE
      WRITE(NFIOUT,901) INFO
  901 FORMAT(' Wrong value of INFO:',I5)
      GOTO 999
  999 CONTINUE
      WRITE(NFIOUT,998)
  998 FORMAT(3(' Error in DELCEL ',/))
      STOP
C***********************************************************************
C     Formats 
C***********************************************************************
  811 FORMAT(' Cell handled by DELCEL',/,
     1         ' vertex | flag | vert.no.| coordinates')
  810 FORMAT(2X,I4,1X,I6,1X,I10,3X,12(1X,I4))
C***********************************************************************
C     end of DELCEL  
C***********************************************************************
      END
      SUBROUTINE MPPSIC(NROW,NCOL,A,LDA,B,LDB,IERR)   
C***********************************************************************
C
C     ************************************************************
C     *                                                          *
C     *   Get PSeudo-Inverse of a matrix with no. of rows        *
C     *   with number of rows larger than number of columns      *
C     *                                                          *
C     ************************************************************
C
C***********************************************************************
C
C  Arguments
C  =========
C
C  NROW   (input) INTEGER
C          number of rows of matrix A
C          number of columns of matrix B
C
C  NCOL   (input) INTEGER
C          number of columns of matrix A
C          number of rows of matrix B
C
C  LDA     (input) INTEGER
C          leading dimension of A
C
C  LDB     (input) INTEGER
C          leading dimension of B
C
C  A       (input) DOUBLE PRECISION, DIMENSION (LDA,NCOL)     
C          Input matrix 
C
C  B       (output) DOUBLE PRECISION, DIMENSION (LDA,NCOL)     
C          Output matrix 
C          B = (A^T * A)^(-1) * A^T
C
C***********************************************************************
C     Storage organization
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION A(LDA,NCOL),B(LDB,NROW),HH(NROW,NROW)
      DOUBLE PRECISION, DIMENSION(NCOL,NCOL)  :: HM
      DOUBLE PRECISION, DIMENSION(NCOL)       :: HV
      INTEGER,          DIMENSION(NCOL)       :: IV
C***********************************************************************
C     Tests      
C***********************************************************************
      IERR = 0 
      IF(NCOL.GT.NROW) THEN
         IERR = -1
         RETURN
      ELSE IF(NROW.GT.LDA) THEN
         IERR = -2
         RETURN
      ELSE IF(NCOL.GT.LDB) THEN
         IERR = -3
         RETURN
      ELSE 
      ENDIF
C***********************************************************************
C     Calculate     
C***********************************************************************
      HM = MATMUL(TRANSPOSE(A(1:NROW,1:NCOL)),A(1:NROW,1:NCOL))
      CALL DGEINV(NCOL,HM,HV,IV,IERI)
      IF(IERI.NE.0) THEN
         IERR = IERI
         RETURN
      ENDIF   
      B(1:NCOL,1:NROW) = MATMUL(HM(1:NCOL,1:NCOL),
     1        TRANSPOSE(A(1:NROW,1:NCOL)))
C***********************************************************************
C     Regular exit 
C***********************************************************************
      RETURN
C***********************************************************************
C     End of UNISET
C***********************************************************************
      END
      SUBROUTINE NEICEL(ICELL,NDIM,NCELL,NCELM,IY,NFIOUT,IERR)
C***********************************************************************
C
C     ************************************************************
C     *
C     *    Create neighbours of a given cell
C     *
C     ************************************************************
C
C***********************************************************************
C
C  Arguments
C  =========
C
C  ICELL  (input) INTEGER
C          index of cell for which neighbours are calculated 
C
C  NDIM   (input) INTEGER
C          dimension of the cell
C
C  NCELL   (input/output) INTEGER
C          number of cells
C          on input NCELL denotes the current number of cells
C          on output NCELL denotes the new number of cells (old
C          number plus neighbours that have been found
C
C  NCELM   (input) INTEGER
C          maximum number of cells
C
C  IY      (input/output) INTEGER array, dimension (NDIM,NCELM)
C          coordinates of the cell
C          On input IY(J,I), I=1,NCELL has to contain the coordinates
C          of the cells. On output these IY have not been changed, but 
C          the neighbours of cell ICELL have been added. Neighbouring 
C          cells of ICELL are cells, for which IY differs by +1 or -1 
C          in one direction only. New cells are created only if they
C          do not yet exist, i.e., if they are not found in the old
C          IY array 
C
C  NFIOUT  (input) INTEGER
C          FORTRAN unit number for output 
C
C  IERR    (output) INTEGER
C          error flag
C
C          IERR = 0 for regular exit
C          IERR = 1 if the number of cells exceeds NCELM
C
C***********************************************************************
C     Storage organization
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL TEST
      DIMENSION IY(NDIM,NCELM)
      INTEGER, DIMENSION(NDIM)     :: IYN
C***********************************************************************
C     Initialize
C***********************************************************************
      TEST = .FALSE.
      IERR = 0
      IADD = 0
C***********************************************************************
C     Loop over positive and negative directions
C***********************************************************************
      DO 101 J=1,2
         DO 100 K=1,NDIM
C***********************************************************************
C     Set coordinates of possible new cell
C***********************************************************************
            IYN(1:NDIM) = IY(1:NDIM,ICELL)
            IF(J.EQ.1) IYN(K) = IYN(K) + 1
            IF(J.EQ.2) IYN(K) = IYN(K) - 1
C***********************************************************************
C     Check whether this cell already exists
C***********************************************************************
            DO 30 I = 1,NCELL
               DO L = 1,NDIM
                  IF(IYN(L).NE.IY(L,I)) GOTO 25
               ENDDO 
C---- if we are here, the cell has been found
               IF(TEST) THEN
                  IF(J.EQ.1) WRITE(NFIOUT,804) ' forward  ',K,I
                  IF(J.EQ.2) WRITE(NFIOUT,804) ' backward ',K,I
               ENDIF
               GOTO 100
 25            CONTINUE
 30         CONTINUE
C***********************************************************************
C     Cell has to be created
C***********************************************************************
            IADD = IADD + 1
C---- check for enough storage
            IF(NCELL+IADD.GT.NCELM) GOTO 910
            IY(1:NDIM,NCELL+IADD) = IYN(1:NDIM)
            IF(TEST) THEN
              IF(J.EQ.1) WRITE(NFIOUT,805) ' forward  ',K,I
              IF(J.EQ.2) WRITE(NFIOUT,805) ' backward ',K,I
            ENDIF
 100     CONTINUE
 101  CONTINUE
      NCELL = NCELL+IADD
C***********************************************************************
C     Test output
C***********************************************************************
      IF(TEST) THEN
         WRITE(NFIOUT,801) ICELL,(IY(I,ICELL),I=1,NDIM)
 801     FORMAT(' NEICELL: considering cell',I7,' IY=',6(1X,I4))
         WRITE(NFIOUT,*) ' Coordinates of new cells:'
         DO 610 I=NCELL-IADD+1,NCELL
            WRITE(NFIOUT,810) I,(IY(J,I),J=1,NDIM)
 610     CONTINUE
      ENDIF
C***********************************************************************
C     Regular end
C***********************************************************************
      RETURN
C***********************************************************************
C     Error exits
C***********************************************************************
  910 CONTINUE
      IERR = 1
      WRITE(NFIOUT,911) NCELM
  911 FORMAT(' Number of cells exceeds ',I5)
      GOTO 999
  999 CONTINUE
      WRITE(NFIOUT,998)
  998 FORMAT(3(' Error in NEICEL ',/))
      STOP
C***********************************************************************
C     FORMAT statements
C***********************************************************************
  804 FORMAT(A10,'neighbouring cell',I6,' found as cell',I6)
  805 FORMAT(A10,'neighbouring cell',I6,' will be created')
  810 FORMAT(5(1X,I3))
C***********************************************************************
C     End of NEICEL
C***********************************************************************
      END
      SUBROUTINE NEICEL_REDIM(ICELL,NDIM,NCELL,NCELM,IY,NFIOUT,IERR)
C***********************************************************************
C
C     ************************************************************
C     *
C     *    Create neighbours of a given cell
C     *
C     ************************************************************
C
C***********************************************************************
C
C  Arguments
C  =========
C
C  ICELL  (input) INTEGER
C          index of cell for which neighbours are calculated
C
C  NDIM   (input) INTEGER
C          dimension of the cell
C
C  NCELL   (input/output) INTEGER
C          number of cells
C          on input NCELL denotes the current number of cells
C          on output NCELL denotes the new number of cells (old
C          number plus neighbours that have been found
C
C  NCELM   (input) INTEGER
C          maximum number of cells
C
C  IY      (input/output) INTEGER array, dimension (NDIM,NCELM)
C          coordinates of the cell
C          On input IY(J,I), I=1,NCELL has to contain the coordinates
C          of the cells. On output these IY have not been changed, but
C          the neighbours of cell ICELL have been added. Neighbouring
C          cells of ICELL are cells, for which IY differs by +1 or -1
C          in one direction only. New cells are created only if they
C          do not yet exist, i.e., if they are not found in the old
C          IY array
C
C  NFIOUT  (input) INTEGER
C          FORTRAN unit number for output
C
C  IERR    (output) INTEGER
C          error flag
C
C          IERR = 0 for regular exit
C          IERR = 1 if the number of cells exceeds NCELM
C
C***********************************************************************
C     Storage organization
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL TEST
      DIMENSION IY(NDIM,NCELM)
      INTEGER, DIMENSION(NDIM)     :: IYN
C***********************************************************************
C     Initialize
C***********************************************************************
      TEST = .FALSE.
      IERR = 0
      IADD = 0
C***********************************************************************
C     Loop over positive and negative directions
C***********************************************************************
      DO 101 J=1,2
* Neagos: Lopp resticted to start from second dimension
         DO 100 K=NDIM,NDIM
*            IF(IY(3,ICELL).gt.1)THEN
C***********************************************************************
C     Set coordinates of possible new cell
C***********************************************************************
            IYN(1:NDIM) = IY(1:NDIM,ICELL)
            IF(J.EQ.1) IYN(K) = IYN(K) + 1
            IF(J.EQ.2) IYN(K) = IYN(K) - 1
C***********************************************************************
C     Check whether this cell already exists
C***********************************************************************
            DO 30 I = 1,NCELL
               DO L = 1,NDIM
                  IF(IYN(L).NE.IY(L,I)) GOTO 25
               ENDDO
C---- if we are here, the cell has been found
               IF(TEST) THEN
                  IF(J.EQ.1) WRITE(NFIOUT,804) ' forward  ',K,I
                  IF(J.EQ.2) WRITE(NFIOUT,804) ' backward ',K,I
               ENDIF
               GOTO 100
 25            CONTINUE
 30         CONTINUE
C***********************************************************************
C     Cell has to be created
C***********************************************************************
            IADD = IADD + 1
C---- check for enough storage
            IF(NCELL+IADD.GT.NCELM) GOTO 910
            IY(1:NDIM,NCELL+IADD) = IYN(1:NDIM)
            IF(TEST) THEN
              IF(J.EQ.1) WRITE(NFIOUT,805) ' forward  ',K,I
              IF(J.EQ.2) WRITE(NFIOUT,805) ' backward ',K,I
*            ENDIF
            ENDIF
 100     CONTINUE
 101  CONTINUE
      NCELL = NCELL+IADD
C***********************************************************************
C     Test output
C***********************************************************************
      IF(TEST) THEN
         WRITE(NFIOUT,801) ICELL,(IY(I,ICELL),I=1,NDIM)
 801     FORMAT(' NEICELL: considering cell',I7,' IY=',6(1X,I4))
         WRITE(NFIOUT,*) ' Coordinates of new cells:'
         DO 610 I=NCELL-IADD+1,NCELL
            WRITE(NFIOUT,810) I,(IY(J,I),J=1,NDIM)
 610     CONTINUE
      ENDIF
C***********************************************************************
C     Regular end
C***********************************************************************
      RETURN
C***********************************************************************
C     Error exits
C***********************************************************************
  910 CONTINUE
      IERR = 1
      WRITE(NFIOUT,911) NCELM
  911 FORMAT(' Number of cells exceeds ',I5)
      GOTO 999
  999 CONTINUE
      WRITE(NFIOUT,998)
  998 FORMAT(3(' Error in NEICEL ',/))
      STOP
C***********************************************************************
C     FORMAT statements
C***********************************************************************
  804 FORMAT(A10,'neighbouring cell',I6,' found as cell',I6)
  805 FORMAT(A10,'neighbouring cell',I6,' will be created')
  810 FORMAT(5(1X,I3))
C***********************************************************************
C     End of NEICEL
C***********************************************************************
      END
      SUBROUTINE DELPOI(IP,NDIM,N2DIM,NVERT,NPROP,NCELM,NCELL,
     1                 IY,ICORD,IRET,IVERT,PROP)
C***********************************************************************
C
C     This Routine deletes points with bad flags  
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DATA NFIOUT/6/
C***********************************************************************
C     Dimension                     
C***********************************************************************
      DIMENSION IRET(NVERT),IY(NDIM,*)
      DIMENSION IVERT(N2DIM,*),ICORD(NDIM,*)
      DIMENSION PROP(NPROP,*)
      DIMENSION IWORK(N2DIM,NCELM),IWORK2(N2DIM*NCELM)
C***********************************************************************
C     Delete the cells with point IP      
C***********************************************************************
      ICP = 0
      DO IC=1,NCELL 
  200    CONTINUE
         DO ID=1,N2DIM
            IF(IVERT(ID,IC).EQ.IP)THEN
C---- Store points of the cell
               ICP = ICP+1
               IWORK(1:N2DIM,ICP) = IVERT(1:N2DIM,IC)
C---- Delete cell in IY and IVERT
               CALL ISHIFT(IC,IY,NDIM,NCELL)
               CALL ISHIFT(IC,IVERT,N2DIM,NCELL)
               NCELL = NCELL-1
               IF(ICP.EQ.N2DIM) GOTO 210
               GOTO 200
            ENDIF
         ENDDO  
      ENDDO
C***********************************************************************
C     Check which points have to be deleted
C***********************************************************************
  210 CONTINUE
C---- List of points, which are used for the cells containing IP
      IWORK2(1:ICP*N2DIM) = 0
      ICOUNT = 0
      DO 222 JJ=1,ICP
      DO 222 II=1,N2DIM
         IADD = 0
         DO 111 LL=1,ICP*N2DIM
            IF(IWORK(II,JJ).NE.IWORK2(LL)) IADD = IADD+1
  111    CONTINUE
         IF(IADD.EQ.ICP*N2DIM)THEN
            ICOUNT = ICOUNT+1
            IWORK2(ICOUNT) = IWORK(II,JJ)
         ENDIF
  222 CONTINUE
C---- Search points for deletion
      DO 250 II=1,ICOUNT
         IDEL = 0
         DO 223 KK=1,N2DIM
         DO 223 LL=1,NCELL
            IF(IWORK2(II).EQ.IVERT(KK,LL))THEN  
               GOTO 250
            ELSE
               IDEL = IDEL+1
            ENDIF
  223    CONTINUE
         IF(IDEL.NE.N2DIM*NCELL) GOTO 900
C---- Delete point in IRET,ICORD,PROP
         CALL ISHIFT(IWORK2(II),IRET,1,NVERT)
         CALL ISHIFT(IWORK2(II),ICORD,NDIM,NVERT)
         CALL ESHIFT(IWORK2(II),PROP,NPROP,NVERT)
C---- Corrector step for IVERT and IWORK2
         DO 224 KK=1,N2DIM
         DO 224 LL=1,NCELL
            IF(IWORK2(II).LT.IVERT(KK,LL))THEN  
               IVERT(KK,LL) = IVERT(KK,LL)-1
               IF(IVERT(KK,LL).LE.0) GOTO 910
            ENDIF
  224    CONTINUE
         INDEX = IWORK2(II)
         DO III=1,ICOUNT
            IF(INDEX.LE.IWORK2(III)) IWORK2(III) = IWORK2(III)-1
         ENDDO
         NVERT = NVERT-1     
  250 CONTINUE
C***********************************************************************
C     Regular end
C***********************************************************************
      RETURN
C***********************************************************************
C     Error exits
C***********************************************************************
  900 CONTINUE
      WRITE(NFIOUT,905) IDEL, N2DIM*NCELL
  905 FORMAT(/,' Error: IDEL:', I5,' not equal N2DIM*NCELL', I5,/)
      GOTO 999
  910 CONTINUE
      WRITE(NFIOUT,915) IVERT(KK,LL)
  915 FORMAT(/,' Error: Entries in IVERT have to be greater than',
     1       I3,/)
      GOTO 999
  999 CONTINUE
  998 FORMAT(' ERROR in -DELPOI-')
      STOP
C***********************************************************************
C     End of -DELPOI-               
C***********************************************************************
      END
      SUBROUTINE ISHIFT(IPOINT,IARR,IND1,IND2)
C***********************************************************************
C
C     Left-Shift of points in Integer-Arrays
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C***********************************************************************
C     Dimension
C***********************************************************************
      DIMENSION IARR(IND1,IND2)
C***********************************************************************
C     Left-Shift
C***********************************************************************
      IARR(1:IND1,IPOINT:IND2-1) = IARR(1:IND1,IPOINT+1:IND2)
      IARR(1:IND1,IND2) = 0
C***********************************************************************
C     Regular end
C***********************************************************************
      RETURN
C***********************************************************************
C     End of -ISHIFT-
C***********************************************************************
      END
      SUBROUTINE ESHIFT(IPOINT,RARR,IND1,IND2)
C***********************************************************************
C
C     Left-Shift of points in Real-Arrays
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      PARAMETER (ZERO = 0.0D0)
C***********************************************************************
C     Dimension
C***********************************************************************
      DIMENSION RARR(IND1,IND2)
C***********************************************************************
C     Left-Shift
C***********************************************************************
      RARR(1:IND1,IPOINT:IND2-1) = RARR(1:IND1,IPOINT+1:IND2)
      RARR(1:IND1,IND2) = ZERO
C***********************************************************************
C     Regular end
C***********************************************************************
      RETURN
C***********************************************************************
C     End of -RSHIFT-
C***********************************************************************
      END
