C>    trajec
      SUBROUTINE TRAJE(NCA,ISTO,NEQ,ND2,NSPEC,NREAC,NSYMB,TIME,S,SP,F,
     1                 RPAR,IPAR,SMIN,SMAX,NTA,TSO,NFILE6,NFILE8,NFIL10,
     1                 NFIL18,KINTE,KFINO,KLSEN,KGSEN,KMANA,NSC,KONSPE,
     1                 NSENS,NRSENS,NISENS,NPSENS,ICRIT,CINT,IINT,
     1                 SENMAX,SENMIN,RATINT,LRW,RWORK,LIW,IWORK,LSWORK,
     1                 SWORK,LISWOR,ISWORK,NGQL)
C***********************************************************************
C
C     .-----------------------------------------------------------.
C     |                                                           |
C     |     PROGRAM (INTEGRATION PART)  FOR THE SIMULATION OF     |
C     | CHEMICAL REACTION WITH VARIABLE TEMPERATURE AND PRESSURE  |
C     |                                                           |
C     '-----------------------------------------------------------'
C
C**********************************************************************C
C**********************************************************************C
C                                                                      C
C     A: STORAGE ORGANIZATION                                          C
C                                                                      C
C**********************************************************************C
C**********************************************************************C
C     I: TYPES OF VARIABLES
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      EXTERNAL HORRES,HORLEF,HORRIG,DUMJAC,DETRES,DETRIG,DETLEF,HOREUL
      LOGICAL TSO,TEST
      LOGICAL PCON,VCON,TCON,DETONA
      INTEGER NWALL,NCOMPLX
      COMMON/BOPTL/PCON,VCON,TCON,DETONA,NWALL,NCOMPLX,NPSR
      COMMON/SPOPT1/NEXCET,NENGIN
      CHARACTER(LEN=*) NSYMB(*)
      COMMON/BDETON/DETVEL,SIGMA,THETA,REY,ETA,TSHO,TUDI,UU,VV,V,SOUND
C**********************************************************************C
C     II: ARRAYS
C**********************************************************************C
      DIMENSION S(NEQ,ND2),SP(NEQ,ND2),F(NEQ),SMIN(NEQ),SMAX(NEQ)
      DIMENSION SENMAX(NSC,ND2),SENMIN(NSC,ND2)
      DIMENSION KONSPE(NSC),RATINT(NREAC)
      DIMENSION RPAR(*),IPAR(*)
C---- WORKING ARRAYS
      DIMENSION RWORK(LRW),IWORK(LIW)
      DIMENSION SWORK(LSWORK),ISWORK(LISWOR)
C---- CONTROL PARAMETERS FOR SOLVERS
      DIMENSION ATOL(1),RTOL(1),ATOLS(1),RTOLS(1)
      DIMENSION INFO(15),IJOB(20),CINT(10),ICSEN(6)
C**********************************************************************C
C     IV: DATA STATEMENTS                                              C
C**********************************************************************C
      DATA ZERO/0.0D0/,ALMOST/0.999999999D0/
C***********************************************************************
C**********************************************************************C
C
C     A:  INITIALIZE
C
C***********************************************************************
      WRITE(NFILE6,8100)
 8100 FORMAT('1',/,1X,79('*'),/,1X,4('*'),5X,
     1  ' SUBSYSTEM   INTEGRATION     (VERSION OF 09/16/1988)         ',
     1  5X,4('*'),/,1X,79('*'))
C***********************************************************************
C     I. INITIALIZE
C***********************************************************************
      TBEG     = CINT(1)
      TEND     = CINT(2)
      TBEG     = CINT(3)
      TSTEPS   = CINT(4)
      ATOL(1)  = CINT(5)
      RTOL(1)  = CINT(6)
      ATOLS(1) = CINT(7)
      RTOLS(1) = CINT(8)
      STEP     = CINT(9)
C***********************************************************************
C     V: SET UP OUTPUT CONTROL PARAMETERS
C***********************************************************************
      IF(TEND-TBEG.LT.1.D-20) RETURN
      DT=(TEND-TBEG)/DABS(TSTEPS)
      NCOU=0
      NTA =0
      INCOU= -MIN(TSTEPS,0.D0) + 0.1D0
                       TOUT = TBEG
      IF(TBEG.EQ.ZERO) TOUT = TBEG + DT
      IF(TIME.GE.TOUT) TOUT = TIME
C**********************************************************************C
C     III. INITIALIZE LOGICAL VARIABLES
C**********************************************************************C
                     TEST=.FALSE.
      IF(KINTE.GE.4) TEST=.TRUE.
C***********************************************************************
C     IV.: SET INITIAL PROFILES
C***********************************************************************
      ICSEN(1) = NSENS
      CALL HORIVA(NEQ,ND2,NSPEC,NFILE6,TIME,RPAR,IPAR,S,SP,F,
     1  NSC,KONSPE,NSENS,ICSEN,KMANA,NFIL10,NREAC,RATINT)
C***********************************************************************
C     VI: SET INITIAL VALUE FOR DETERMINATION OF MAXIMUM DECAY RATES
C***********************************************************************
      IF(ICRIT.GT.0)
     1   CALL HORIDE(0,NSYMB(ICRIT),TSO,TIME,S(ICRIT,1),NFILE6,TAU,TZZ)
C***********************************************************************
C     Open Storage File 8 as scratch file
C***********************************************************************
      CALL OPNFIL(-1,NFILE8,'f8',IERR)
C***********************************************************************
C       INITIALIZE ARRAYS
C***********************************************************************
      CALL UPDEXT(0,NEQ,S,SMIN)
      CALL UPDEXT(0,NEQ,S,SMAX)
      IF(KGSEN.NE.0)THEN
        CALL UPDVAR(0,NEQ,NSENS,S(1,2),NSC,KONSPE,SENMIN)
        CALL UPDVAR(0,NEQ,NSENS,S(1,2),NSC,KONSPE,SENMAX)
      ENDIF
C***********************************************************************
C     VII: OUTPUT OF VALUES BEFORE BEGIN OF INTEGRATION
C***********************************************************************
      IF(TBEG.LT.1.0D-12) THEN
        NTA=NTA+1
        IF(ISTO.GT.0) CALL OUTRST(NEQ,TIME,NFIL18,S,SP)
        CALL HOROU1(NTA,NSPEC,NSYMB,TIME,NFILE6,NFILE8,S,SP,NEQ)
      ENDIF
C***********************************************************************
C     III.: SET INTEGRATION CONTROL PARAMETERS FOR LIMEX
C***********************************************************************
      ICALL    = 0
      DO 2510 I=1,20
 2510 IJOB(I) = 0
      NZV = 0
      NZC = NEQ
      IJOB(1)=1
      IF(DETONA) IJOB(1) = 0
      IJOB(20)=NSENS
      IF(KINTE.GE.3) IJOB(7)=4
      H        = STEP
      HMAX     = 1.D10
C_test
C      HMAX     = 1.D-3
C***********************************************************************
C     IV: SET INTEGRATION CONTROL PARAMETERS FOR DASSL
C***********************************************************************
      DO 2550 I=1,15
 2550 INFO(I)=0
      INFO(8) = 1
      INFO(3) = 1
      INFO(11) = 0
      RWORK(3) = STEP
 3000 CONTINUE
C**********************************************************************
C     CONTROL PARAMETERS FOR SENSITIVITY ANALYSIS
C**********************************************************************
      DO 20 I=1,6
   20 ICSEN(I)=0
      IF(NSENS.GT.0) ICSEN(1) = 1
                     ICSEN(5) = NSENS
C***********************************************************************
C     VIII: CALL INTEGRATOR AND CHECK RETURN CODE
C***********************************************************************
      IF(.NOT.DETONA) THEN
      IF(IINT.EQ.-1) THEN
        ICALL = 1
        KFLAG = 0
        CALL EULEX(NEQ,HOREUL,TIME,S,TEND,RTOL(1),
     1      HMAX,H,KFLAG,ICALL,RPAR,IPAR)
      WRITE(6,*) 'TIME,TEND,H,HMAX',TIME,TEND,H,HMAX
      WRITE(6,*) 'KFLAG',KFLAG
      ENDIF
      IF(IINT.EQ.0) CALL UDASSL(HORRES,NEQ,TIME,S,SP,TEND,INFO,ICSEN,
     1      RTOL,ATOL,RTOLS,ATOLS,IDID,SWORK,LSWORK,
     1      ISWORK,LISWOR,RWORK,LRW,IWORK,LIW,RPAR,IPAR)
      IF(IINT.EQ.1)  CALL XLIMEX(NEQ,NZC,NZV,HORLEF,HORRIG,DUMJAC,
     1      TIME,S,SP,TEND,RTOL,ATOL,RTOLS(1),ATOLS(1),
     1      HMAX,H,IJOB,LRW,RWORK,LIW,IWORK,ICALL,RPAR,IPAR)
      ELSE
      WRITE(6,*) ' UU,VV',UU,VV
      IF(IINT.EQ.0) CALL UDASSL(DETRES,NEQ,TIME,S,SP,TEND,INFO,ICSEN,
     1      RTOL,ATOL,RTOLS,ATOLS,IDID,SWORK,LSWORK,
     1      ISWORK,LISWOR,RWORK,LRW,IWORK,LIW,RPAR,IPAR)
      IF(IINT.EQ.1)  CALL XLIMEX(NEQ,NZC,NZV,DETLEF,DETRIG,DUMJAC,
     1      TIME,S,SP,TEND,RTOL,ATOL,RTOLS(1),ATOLS(1),
     1      HMAX,H,IJOB,LRW,RWORK,LIW,IWORK,ICALL,RPAR,IPAR)
      ENDIF
      ICALL=2
C***********************************************************************
C     IX: CHECK RETURN CODE AND MONITOR INTEGRATION
C***********************************************************************
      IF(IINT.EQ.1) IDID=IJOB(7)
      IF(IDID.LT.0) GOTO 5000
      IJOB(7)=0
      IF(KINTE.GE.3) IJOB(7)=4
      IF(KINTE.GE.3.AND.IINT.EQ.0)
     1    WRITE(NFILE6,7610) TIME,RWORK(3),RWORK(7),IWORK(14),IWORK(15)
C**********************************************************************
C     UPDATE IGNITION DELAY TIME
C**********************************************************************
        IF(ISTO.EQ.3) CALL OUTRST(NEQ,TIME,NFIL18,S,SP)
C**********************************************************************
C     UPDATE IGNITION DELAY TIME
C**********************************************************************
      IF(ICRIT.GT.0)
     1   CALL HORIDE(1,NSYMB(ICRIT),TSO,TIME,S(ICRIT,1),NFILE6,TAU,TZZ)
C***********************************************************************
C       UPDATE MAXIMUM VALUES
C***********************************************************************
      CALL UPDEXT(-1,NEQ,S,SMIN)
      CALL UPDEXT(+1,NEQ,S,SMAX)
      IF(KGSEN.NE.0) THEN
        CALL UPDVAR(-1,NEQ,NSENS,S(1,2),NSC,KONSPE,SENMIN)
        CALL UPDVAR(+1,NEQ,NSENS,S(1,2),NSC,KONSPE,SENMAX)
      ENDIF
C**********************************************************************
C     UPDATE REACTION RATES
C**********************************************************************
      IF(KMANA.NE.0)
     1   CALL UPDINR(NEQ,NSPEC,NREAC,TIME,S,SP,F,IRES,RPAR,IPAR)
C***********************************************************************
C     XI: CONTROL OUTPUT
C***********************************************************************
C---- DECIDE IF OUTPUT IS DESIRED
      IF(TIME.GE.(ALMOST*TEND)) THEN
        ILAS=1
        GOTO 3299
      ELSE
        ILAS = 0
      ENDIF
      IF(TSTEPS.GT.0.D0.AND.TIME.LT.(TOUT-1.D-14*TOUT)) GOTO 3500
          NCOU=NCOU+1
      IF(TSTEPS.LE.0.D0) THEN
        IF(TIME.LT.TBEG) THEN
          NCOU=0
          GOTO 3500
        ENDIF
        IF(NCOU.NE.INCOU) THEN
          GOTO 3500
        ELSE
          NCOU=0
        ENDIF
      ENDIF
 3299 CONTINUE
C---- INCREMENT NUMBER OF OUTPUT AND CALL OUTPUT
      NTA=NTA+1
      CALL HOROU1(NTA,NSPEC,NSYMB,TIME,NFILE6,NFILE8,S,SP,NEQ)
      IF(ISTO.EQ.1) CALL OUTRST(NEQ,TIME,NFIL18,S,SP)
      IF (NEXCET .GE. 1) THEN
        CALL EXCCAL(TIME,NTA)
      ENDIF
C***********************************************************************
C     OUTPUT FOR MECHANISM ANALYSIS
C***********************************************************************
      IF(KMANA .NE.0) CALL HOROUR(NEQ,NSPEC,NREAC,KMANA,ILAS,NFIL10)
C---- DETERMINE NEXT TIME FOR OUTPUT
 3300 CONTINUE
      TOUT=TOUT+DT
      IF(TIME.GE.TOUT) GOTO 3300
C***********************************************************************
C     XII: TERMINATION OF LOOP
C***********************************************************************
 3500 CONTINUE
C---- LEAVE LOOP IF TIME = TEND
      IF(DETONA) THEN
        IF(V.LT.3.0D0) GO TO 5000
      ENDIF
      IF(TIME.GE.ALMOST*TEND) GO TO 5000
      GOTO 3000
C***********************************************************************
C***********************************************************************
C
C     D: FINAL OUTPUT
C
C***********************************************************************
C***********************************************************************
 5000 CONTINUE
C***********************************************************************
C     XII: OUTPUT OF TABLES AND PLOTS
C***********************************************************************
      N1 = 1
      N2 = N1 + (NEQ + 2)
      N3 = N2 + (NEQ + 2)
      N4 = N3 + NTA
      N5 = N4 + NTA
      N6 = N5 + NTA
      N7 = N6 + NTA
      N8 = N7 + NTA*NEQ
      N9 = N8 + NTA*NEQ
      NRWA=N9-1
!CMM SENSITIVITY WITH RESP. TO INITIAL PRESSURE
!CMM FOUND IN FORT.8 AT INDEX NEQ*NSENS+1 to (NEQ+1)*NSENS
!      IF (NSENS.GT.1) THEN
!        NEQ=NEQ+1
!      ENDIF
!CMM
      IF(NRWA.GT.LRW) GOTO 930
                     NDIM = NEQ*(NSENS+1)
      IF(KGSEN.NE.0) NDIM = NEQ
C- --
      IF(3.LT.2) THEN
      CALL HORIOU(NSPEC,NDIM,NEQ,NSYMB,NFILE8,NFILE6,TSO,
     1  NTA,RWORK(N1),RWORK(N2),RWORK(N3),RWORK(N4),RWORK(N5),RWORK(N6),
     1     RWORK(N7),SMIN,SMAX)
      ENDIF
      CALL HOROU2(NCA,NSPEC,NDIM,NEQ,NSYMB,NFILE8,NFILE6,TSO,
     1  NTA,RWORK(N1),RWORK(N2),RWORK(N3),RWORK(N4),RWORK(N5),RWORK(N6),
     1  RWORK(N7),RWORK(N8),SMIN,SMAX,RPAR,IPAR,NGQL)
C***********************************************************************
C     Close Storage file
C***********************************************************************
      CALL CLSFIL(NFILE8)
C***********************************************************************
C     XII: OUTPUT OF IGNITION DELAY TIME
C***********************************************************************
      IF(ICRIT.GT.0)
     1  CALL HORIDE(2,NSYMB(ICRIT),TSO,TIME,S(ICRIT,1),NFILE6,TAU,TZZ)
C***********************************************************************
C     XII: OUTPUT OF GLOBAL SENSITIVITIES FOR REACTIONS
C***********************************************************************
      IF(KGSEN.GT.0)
     1CALL HOROGS(NSPEC,NREAC,NEQ,NSYMB,NFILE6,TSO,KGSEN,NSC,KONSPE,
     1           NSENS,SENMAX,SENMIN)
C***********************************************************************
C          PERFORM ANALYSIS OF THE REACTION MECHANISM
C***********************************************************************
      IF(KMANA.GT.0) THEN
        LRW4=LRW*2
        CALL HOMMEC(NSYMB,LIW,IWORK,LRW4,RWORK,NFIL10,NFILE6)
      ENDIF
      RETURN
C***********************************************************************
C***********************************************************************
C
C     E: ERROR EXITS
C
C***********************************************************************
C***********************************************************************
  930 CONTINUE
      WRITE(NFILE6,931) NRWA,LRW
  931 FORMAT('1',1X,'ERROR - NEEDED REAL WORK SPACE (',I7,') > ',
     1              'SUPPLIED REAL WORK SPACE(',I7,') IN TRAJ')
      GO TO 9999
 9900 CONTINUE
      WRITE(NFILE6,9901) IDID
 9901 FORMAT('1',5X,'ERROR - IDID =',I3,' RETURN CODE OF DASSL/LIMEX')
      GO TO 9999
 9999 CONTINUE
 9998 FORMAT('0',32('+'),' ABNORMAL END ',32('+'))
      WRITE(NFILE6,9998)
      WRITE(NFILE6,9998)
      WRITE(NFILE6,9998)
      WRITE(NFILE6,8010)
      WRITE(NFILE6,8010)
      STOP
C***********************************************************************
C***********************************************************************
C
C     F: FORMAT STATEMENTS
C
C***********************************************************************
C***********************************************************************
 8010 FORMAT('0',2X)
C 829 FORMAT('0',79('*'))
C1829 FORMAT('0',132('*'))
 7610 FORMAT(
     1 '   TIME                                            ',1PE12.5,/,
     1 '   STEP SIZE H TO BE ATTEMPTED ON THE NEXT STEP :  ',1PE12.5,/,
     1 '   STEPSIZE USED ON THE LAST SUCCESSFUL STEP :     ',1PE12.5,/,
     1 '   TOTAL NUMBER OF ERROR TEST FAILURES SO FAR :    ',8X,I4,/,
     1 '   TOTAL NUMBER OF CONVERGENCE TEST FAILURES SO FAR :',6X,I4)
C***********************************************************************
C***********************************************************************
C
C     END OF MAIN PROGRAM
C
C***********************************************************************
C***********************************************************************
      END
      SUBROUTINE JAC(X,Y,YP,Z,CJ,RPAR,IPAR)
      INTEGER IPAR(1)
      DOUBLE PRECISION X, Y(1), YP(1), Z, CJ,RPAR
      STOP
      END
