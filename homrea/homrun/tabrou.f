      SUBROUTINE NSETUP(JOBTSU,NDIM,NC,NEQ,NZEV,MIXFRA,LRV,
     1           IPAR,ICHKD,SCALD,NPROP)
C***********************************************************************
C     STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
C***********************************************************************
C     DIMENSION
C***********************************************************************
      DIMENSION SCALD(MNCOV)
      DIMENSION IPAR(*)
C***********************************************************************
C     COMMON BLOCKS
C***********************************************************************
      COMMON/ILDMOP/IPARA,ISTAOP,MANPAR,NOFORM,ANAFUE,INMAN,lasloo,
     1              ICALTR,MASENS,IUPVER,NTRANS
      COMMON/BLOCOR/IIRAT,IISMF,IISMFP,IIRV,IIPRO,IIPNU,IILAM,NPROOT
C***********************************************************************
C     Data statements
C***********************************************************************
      DATA NFIOUT/6/
C***********************************************************************
C     FIRST EXECUTABLE STATEMENT FOLLOWS
C***********************************************************************
      NS = IPAR(3)
      NE = IPAR(6)
C***********************************************************************
C     Compute Dimension of the Table and number of properties
C***********************************************************************
         IIRAT  = 1
         IISMF  = IIRAT  + NDIM
         IISMFP = IISMF  + NEQ  
         IIRV   = IISMFP + NEQ  
         IIPRO  = IIRV   + LRV
C----  2 means that full projection matrix is calculated
C     IF(ISTAOP.LT.0) IPAR(39) = 0
CUUUU
                      IPAR(39) = 0
      IF(IPAR(39).EQ.0) THEN
         IIPNU  = 0 
      ELSE 
         IF(IPAR(39).GT.0) THEN
           IIPNU  = 2 * NC * NEQ 
         ELSE
           IIPNU  = NEQ * NEQ 
         ENDIF 
      ENDIF
      IILAM = IIPRO + IIPNU     
      IILAS = IILAM + 2 * NEQ
      NPROP = IILAS - 1
C***********************************************************************
C     IPAR(39).LT.0 
C***********************************************************************
C---- Check for enough storage
      IF(NPROP.GT.MNPROP)THEN
         WRITE(NFIOUT,8830) NPROP, MNPROP
         STOP
      ENDIF
 8830 FORMAT(' MNPROP too small:',/,' NPROP = ',I4,' <=> MNPROP = ',I4)
C***********************************************************************
C     write information
C***********************************************************************
      IF(MIXFRA.EQ.0) WRITE(NFIOUT,8831) NDIM
 8831 FORMAT(' constant mixture fraction used for the',I2,
     1                    '-dimensional table setup')
      IF(MIXFRA.EQ.1) WRITE(NFIOUT,8832) NDIM
      IF(MIXFRA.EQ.2) WRITE(NFIOUT,8832) NDIM
 8832 FORMAT(' mixture fraction is one of the',I2,
     1     ' tabulation coordinates')
      IF(MIXFRA.NE.1.AND.MIXFRA.NE.0.AND.MIXFRA.NE.2) THEN
        WRITE(NFIOUT,*) ' wrong numbers for table setup'
        STOP
      ENDIF
C***********************************************************************
C     Checks
C***********************************************************************
C---- check for allowed domain
      ICHKD    = 2
C***********************************************************************
C     Entscheidung feste oder variable Parametrisierung
C***********************************************************************
      CALL GETSCV(NC,NZEV,SCALD)
C***********************************************************************
C     End of NSETUP
C***********************************************************************
      END
C***********************************************************************
C***********************************************************************
C***********************************************************************
C***********************************************************************
C***********************************************************************
C***********************************************************************
C***********************************************************************
C***********************************************************************
C***********************************************************************
C***********************************************************************
C***********************************************************************
C***********************************************************************
      SUBROUTINE TABSYG(NDIM,NEQ,SMFSYM,LRV,RETSYM,NPROP,SYMPRP)
C***********************************************************************
C
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      CHARACTER(LEN=*)  SMFSYM(NEQ),RETSYM(LRV),SYMPRP(NPROP) 
      COMMON/BLOCOR/IIRAT,IISMF,IISMFP,IIRV,IIPRO,IIPNU,IILAM,NPROOT
      COMMON/ILDMOP/IPARA,ISTAOP,MANPAR,NOFORM,ANAFUE,INMAN,lasloo,
     1              ICALTR,MASENS,IUPVER,NTRANS
C***********************************************************************
C 
C***********************************************************************
      ISTO = 0
      ISLEN = LEN(SYMPRP(1))
      DO I=1,ISLEN
      DO J=1,NPROP
      SYMPRP(J)(I:I) = ' '
      ENDDO
      ENDDO
C***********************************************************************
C     assign symbols for rates of the parameters 
C***********************************************************************
      DO I=1,NDIM
      WRITE(SYMPRP(I+IIRAT-1),81) I
      ENDDO
   81 FORMAT(I3,'-Rate')
C***********************************************************************
C     assign symbols for SMF
C***********************************************************************
      SYMPRP(IISMF:IISMF-1+NEQ) =  SMFSYM(1:NEQ)
C***********************************************************************
C     assign symbols for SMFP
C***********************************************************************
      DO I=1,NEQ 
      WRITE(SYMPRP(IISMFP-1+I),83) SMFSYM(I)
      ENDDO 
   83 FORMAT('r',A7)
C***********************************************************************
C     assign symbols for RV  
C***********************************************************************
      SYMPRP(IIRV:IIRV-1+LRV) = RETSYM(1:LRV)
C***********************************************************************
C     assign symbols for projection matrix
C***********************************************************************
      DO I=1,IIPNU
        WRITE(SYMPRP(IIPRO-1+I:IIPRO-1+I),84) I               
      ENDDO 
   84 FORMAT('P ',I6)
C***********************************************************************
C     assign remaining symbols
C***********************************************************************
      DO I=1,NEQ
        WRITE(SYMPRP(IILAM-1+I),188) I
        WRITE(SYMPRP(IILAM-1+I+NEQ),189) I
      ENDDO
 188  FORMAT('la_r',I4)
 189  FORMAT('la_i',I4)
C***********************************************************************
C     Position of species used for the thermal NO-formation
C***********************************************************************
C***********************************************************************
C     assign remaining symbols
C***********************************************************************
      RETURN
      END
      SUBROUTINE STOVAL(INDVER,NDIM,NC,NEQ,LRV,NPROP,PROP,SMF,SMFP,
     1           RV,AR,AI,XROW,XCOL,
     1           CVCEN,CVCENP,INDC,IRET)
c***********************************************************************
c***********************************************************************
c***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C**********************************************************************
C     storage organiztaion
C**********************************************************************
      DIMENSION SMF(NEQ),SMFP(NEQ),RV(LRV)
      DIMENSION PROP(NPROP,*),XROW(NEQ*NEQ),XCOL(NEQ*NEQ)
      DIMENSION AR(NEQ), AI(NEQ)
      DIMENSION CVCEN(NC),CVCENP(NC)
      DIMENSION IRET(*)
      COMMON/BLOCOR/IIRAT,IISMF,IISMFP,IIRV,IIPRO,IIPNU,IILAM,NPROOT
C**********************************************************************
C     store values         
C**********************************************************************
      DO ID0=1,NDIM
         PROP(IIRAT-1+ID0,INDVER) = CVCENP(NC-NDIM+ID0)
      ENDDO
C---- massfractions
      PROP(IISMF:IISMF-1+NEQ,INDVER) = SMF(1:NEQ)
C---- rates
      PROP(IISMFP:IISMFP-1+NEQ,INDVER) = SMFP(1:NEQ)
C---- flags 
      PROP(IIRV:IIRV-1+LRV,INDVER) = RV(1:LRV)
C---- flag
      PROP(IIRV,INDVER) = FLOAT(IRET(INDVER))
C---- slow eigenvectors Z_s
      IF(IIPNU.NE.0) THEN
      PROP(IIPRO:IIPRO-1+NC*NEQ,INDVER) =  XCOL(1:NC*NEQ) 
C---- slow eigenvectors Z_s^(-1)
      DO I=1,NC
         DO J=1,NEQ
            PROP(IIPRO-1+NC*NEQ+(I-1)*NEQ+J,INDVER)=
     1           XROW((J-1)*NEQ+I)
         ENDDO
      ENDDO
      ENDIF
C---- eigenvalues
      PROP(IILAM    :IILAM-1+NEQ,    INDVER) = AR(1:NEQ)
      PROP(IILAM+NEQ:IILAM-1+NEQ+NEQ,INDVER) = AI(1:NEQ)
C***********************************************************************
C     End of stoval
C***********************************************************************
      END
