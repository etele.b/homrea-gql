module options
! *****************************************************************************
! Module for reading and storing options and parameters
! *****************************************************************************
implicit none
public
include 'homdim2.f90'
! General variables used by every routine
integer, protected, save :: NEQ, NSPEC, IEQ, NE, NCON, IPAR(MNIPAR)
double precision, parameter :: RGAS = 8.3141, PATM = 101235 
double precision, save :: RPAR(MNRPAR)
double precision, allocatable, save :: qb(:,:), qbi(:,:)
character(len=8), allocatable, save :: SNAM(:)
character(len=20), save :: G_detfile
! Parameters used by analysis
integer, save :: A_pointgen, A_ns(2), A_staban, A_nr, A_nrad, A_ncor,&
    A_nint(2), A_nhalf
double precision, save :: A_staban_tol, A_tdel, A_thalf, A_intdif,&
    A_rref, A_tol, A_halftol 

! Parameters used by the integration
integer, save :: I_nvert_max, I_checks, I_ns
double precision, save :: I_tend

! Parameters used by local analysis
integer, save :: LA_on
character(len=20) :: LA_traj, LA_short

! Output options
integer, save :: IO_intouti, IO_errf
integer, allocatable, save :: IO_table(:,:)
character(len=20), save :: IO_format, IO_intoutf, IO_statf, IO_dettec


contains
    subroutine init(filename, nfile, iparin, rparin, symprp)
        !
        ! Subroutine to read analysis configuration file and save options
        !
        integer, intent(in) :: nfile, iparin(*)
        double precision, intent(in) :: rparin(*)
        character(len=8), intent(in) :: symprp(NREAC)
        character(len=20), intent(in) :: filename

        integer :: io, info
        character(len=50000) :: c
        logical :: exi
        namelist / General / G_detfile
        namelist / Analysis / A_pointgen, A_nr, A_ns, A_staban, A_staban_tol, &
            A_tol, A_tdel, A_thalf, A_intdif, A_rref, A_nrad, A_ncor, A_nhalf,&
            A_nint, A_halftol
        namelist / Integration / I_tend, I_ns, I_nvert_max, I_checks
        namelist / LAOutput / LA_on, LA_traj, LA_short
        namelist / Output / IO_format, IO_intouti, IO_errf, IO_statf, &
            IO_table, IO_intoutf, IO_dettec

        RPAR(1:MNRPAR) = rparin(1:MNRPAR)
        IPAR(1:MNIPAR) = iparin(1:MNIPAR)
       
        NEQ = IPAR(1) + 1 !PROBLEM
        NSPEC = IPAR(3)
        NE = IPAR(6)
        IEQ = IPAR(10)
        NCON = IPAR(7) 

        

        allocate(IO_table(4, 8))
        allocate(SNAM(NREAC))
        allocate(qb(NEQ, NEQ), qbi(NEQ, NEQ))
        SNAM = symprp(1:NREAC)
       
        ! Open configuration file
        open(unit = 123, file = trim(filename), status = 'old', iostat = io)
        if (io /= 0) goto 901

        call read_nml(123, c, info)
        read(c, General)
        call read_nml(123, c, info)
        read(c, Analysis)
        call read_nml(123, c, info)
        read(c, Integration)
        call read_nml(123, c, info)
        read(c, LAOutput)
        call read_nml(123, c, info)
        read(c, Output)
        close(123)
        return
    901 continue
        print*, 'Error in init_para: Coud not find file : ', trim(filename), io 
        stop
    end subroutine init

    subroutine read_nml(unt, line, info)
        integer, intent(in) :: unt
        character(len=*) :: line
        integer, intent(out) :: info

        integer :: i

        i = 1
        do while(.true.)
            read(unt, '(A)', end = 100) line(i:)
            i = index(line,'/')
            if (i == 0) then
                i = len(trim(line)) + 2
            else
                line(i+1:) = ''
                exit
            end if
        end do
        
        return
    100 continue
        info = 0
        return
    end subroutine read_nml



end module options
