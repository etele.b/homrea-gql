C>    horiva
      SUBROUTINE HORIVA(NEQ,ND2,NSPEC,NFILE6,TIME,RPAR,IPAR,S,SP,F,
     1  NSC,KONSPE,NSE,ICSEN,KONMEC,NFIL10,NREAT,RATINT)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *      SET UP INITIAL VALUES                                *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C     NEQ           = NUMBER OF DEPENDENT VARIABLES
C     NSPEC         = NUMBER OF SPECIES
C     NFILE6        = NUMBER OF OUTPUT FILE FOR LISTING
C     TIME          = ACTUAL TIME
C     ICSEN         = INFORMATION FOR PERFORMANCE OF SENSITIVITY ANAL.
C
C     OUTPUT :
C
C     S(I,1)        = INITIAL VALUES OF DEPENDENT VARIABLES
C     S(I,J)        = INITIAL VALUES OF SENSITIVITY COEFFICIENTS
C     SP( )         = INITIAL VALUES FOR FIRST DERIVATIVES
C     RPAR(1)       = PARAMETERS FOR SENSITIVITY (ALL SET TO ONE)
C     SMAX(1)       = MAXIMUM VALUES OF DEP. VARIABLES(SET TO INIT VAL.)
C
C     INPUT FROM COMMON BLOCK:
C
C         INITIAL VALUES
C
C***********************************************************************
C     CHAPTER I: STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      LOGICAL PCON,VCON,TCON,DETONA
      INTEGER NWALL,NCOMPLX
      COMMON/LOCS0D/NW,NWS,NP,NK,NI,NH
      COMMON/BOPTL/PCON,VCON,TCON,DETONA,NWALL,NCOMPLX,NPSR
      COMMON/BDETON/DETVEL,SIGMA,THETA,REY,ETA,TSHO,TUDI,UU,VV,V,SOUND
      COMMON/BHCP/HLHH(7,MNSPE*2),HINF(3,MNSPE)
      DIMENSION RPAR(*),IPAR(*),S(NEQ,ND2),SP(NEQ,ND2),F(NEQ),ICSEN(*)
      DIMENSION KONSPE(NSC)
      DIMENSION RATINT(NREAT),HI(MNSPE),CPI(MNSPE),HELP(MNSPE+2)
      COMMON/BSSPV/SSPV(20*MNSPE)
      COMMON/BSENS/LSMAX,NSENS,NRSENS,NISENS,NPSENS,
     1       IRSENS,IISENS,IPSENS,ISENSI(MNSEN)
      COMMON/BICRES/ICRES
      DOUBLE PRECISION, DIMENSION(1) :: DXDS,DTDS,DPDS
      DATA RGAS/8.3143D0/,ZERO/0.0D0/,ONE/1.0D0/
      NE = IPAR(6)
C***********************************************************************
C     CHAPTER II.: SET VALUES FOR PARAMETERS TO ONE
C***********************************************************************
      IF(ICSEN(1).EQ.0) GOTO 7
      DO 5 J=1,NSENS
      RPAR(J)=ONE
    5 CONTINUE
    7 CONTINUE
C***********************************************************************
C     CHAPTER III.: SET INITIAL PROFILES FOR VARIABLES
C***********************************************************************
C---- VARIABLES ASSUMED TO BE SET FOR SET FOR REGULAR SYSTEM
      IF(DETONA) THEN
C     DETVEL =  2000.9500D0
      DETVEL =  2000.9500D0
      NW  = 1
      NWS = 0
      NP  = NSPEC + 1
      NK  = NSPEC + 2
      NI  = NSPEC + 3
      NH  = NSPEC + 4
      T0 = S(NSPEC+1,1)
      CALL DCOPY(NSPEC,S,1,F,1)
      CALL CALHCP(NSPEC,S,T0,HLHH,HINF,CPI,CPDEN,HI,IERR)
      HDEN = DDOT(NSPEC,S,1,HI,1)
      IF(IERR.GT.0) THEN
        WRITE(NFILE6,*) ' Error in HORIVA'
        STOP
      ENDIF
      NNN = NSPEC+2
      CALL TRASSP(-3,NNN,HELP,NSPEC,T0,C0,F,RHO0,H0,P0,
     1          SSPV,IERR,0,DXDS,1,DTDS,DPDS)
      CALL DCOPY(NSPEC,HELP(3),1,S(NW,1),1)
      IF(IERR.GT.0) THEN
        WRITE(NFILE6,*) ' Error in HORIVA'
        STOP
      ENDIF
      XMOLM = RHO0 / C0
      H0    = HDEN / RHO0
      CP0 = CPDEN / C0
      CV0 = CP0 - RGAS
      GAMMA0 = CP0 / CV0
      SOUND0 = DSQRT(GAMMA0 * P0 / RHO0)
      XMACH = DETVEL / SOUND0
      WRITE(6,*) ' GAMMA0 = ',GAMMA0
      WRITE(6,*) ' SOUND0 = ',SOUND0
      RHO =   RHO0 * (GAMMA0 + 1)*XMACH**2
     1    / ((GAMMA0-1.0D0)*XMACH**2 +2.0D0)
      P   = P0 *(2.0D0*GAMMA0*XMACH**2/(GAMMA0+1.0D0)-
     1          (GAMMA0 - 1.0D0)/(GAMMA0 + 1.0D0))
      VAU = DETVEL * RHO0 / RHO
      T   = P * XMOLM /(RHO * RGAS)
      H   = H0 + DETVEL**2/2.0D0 - VAU**2/2.0D0
      write(6,*) 'rho,p,vau,t,h', rho,p,vau,t,h
      S(NP,1) = P
      S(NK,1) = RHO * VAU
      S(NI,1) = P + RHO * VAU**2
      S(NH,1) = H + VAU**2/2.0D0
      WRITE(6,*) ' ---- ',(S(I,1),I=1,NEQ)
      DIAMOL = 2.0D0
      TUDI   = 1.D-2
      ETA = 26.7D-7 * DSQRT(XMOLM*T) / DIAMOL**2
      REY = RHO * (DETVEL - VAU) * TUDI / ETA
      TSHO = T
      write(6,*) ' tudi,rey,eta',tudi,rey,eta
      ENDIF
C***********************************************************************
C
C     CHAPTER III.: SET INITIAL VALUES FOR SENSITIVITIES
C
C***********************************************************************
C***********************************************************************
C     CHAPTER III.: SET INITIAL VALUES FOR SENS. WITH RESPECT TO REACT.
C***********************************************************************
      IF(NRSENS.GT.0) THEN
      DO 410 J=1,NRSENS
      DO 410 I=1,NEQ
      S(I,IRSENS+J)=ZERO
  410 CONTINUE
      ENDIF
C***********************************************************************
C     CHAPTER III.: SET INITIAL VALUES FOR SENS. WITH RESPECT TO INIT.V.
C***********************************************************************
      write(6,*) nisens,iisens,nrsens,irsens,npsens,ipsens
      IF(NISENS.GT.0) THEN
      DO 420 J=1,NISENS
      DO 420 I=1,NEQ
      S(I,IISENS+J)=ZERO
      IF(ISENSI(IISENS-1+J).EQ.I) S(I,IISENS+J)=1.D0
  420 CONTINUE
      ENDIF
C***********************************************************************
C
C     CHAPTER V.: MAKE INITIAL VALUES FOR VARIABLES CONSISTENT
C                 AND INITIALIZE REACTION RATES
C***********************************************************************
      DO 30 I= 1,NEQ
      SP(I,1)=ZERO
   30 CONTINUE
                      ICRES=0
      IF(KONMEC.NE.0) ICRES=1
      IF(.NOT.DETONA) THEN
        CALL HORDES(NEQ,TIME,S,F,RPAR,IPAR)
      ELSE
        CALL DETDES(NEQ,NSPEC,TIME,S,F,RPAR,IPAR,IERR)
      ENDIF
                      ICRES=0
      DO 40 I= 1,NEQ
   40 SP(I,1)=-F(I)
C***********************************************************************
C
C     CHAPTER VI.: CONS. OF INIT. VAL OF "REACTION SENSITIVITIES"
C
C***********************************************************************
C***********************************************************************
C     CHAPTER VI.: CONS. OF INIT. VAL OF "REACTION SENSITIVITIES"
C***********************************************************************
      IF(NRSENS.GT.0) THEN
      IF(DETONA) THEN
         WRITE(6,*) ' NOT YET REALIZED FOR DETONATIONS '
         STOP
      ENDIF
C---- CALCULATE OPTIMUM AMOUNT OF VARIATION
C     UROUND=D1MACH(4)
      UROUND=1.D-15
      DEL=DSQRT(UROUND)
      DELINV=ONE/DEL
C---- INITIALIZE SOLUTION AND SENSITIVITIES
      DO 190 J=1,NRSENS
      DO 110 I=1,NEQ
      SP(I,IRSENS+J) = ZERO
  110 CONTINUE
C---- MAKE VARIATION
      SAVE=RPAR(J+IRSENS-1)
      RPAR(J+IRSENS-1)=RPAR(J+IRSENS-1)+DEL
      CALL HORRES(TIME,S,SP,SP(1,IRSENS+J),IRES,RPAR,IPAR)
C---- CALCULATE RELATIVE SENSITIVITIES
      DO 120 I=1,NEQ
      SP(I,IRSENS+J)=SP(I,IRSENS+J)*DELINV
  120 CONTINUE
C---- RESET VARIATION
      RPAR(J+IRSENS-1)=SAVE
  190 CONTINUE
      ENDIF
C***********************************************************************
C     CHAPTER VI.: CONS. OF INIT. VAL OF "INIT. VALUE. SENSITIVITIES"
C***********************************************************************
      IF(NISENS.GT.0) THEN
      IF(DETONA) THEN
         WRITE(6,*) ' NOT YET REALIZED FOR DETONATIONS '
         STOP
      ENDIF
C---- CALCULATE OPTIMUM AMOUNT OF VARIATION
C     UROUND=D1MACH(4)
      UROUND=1.D-15
      DEL=DSQRT(UROUND)
      DELINV=ONE/DEL
C---- INITIALIZE SOLUTION AND SENSITIVITIES
      DO 590 J=1,NISENS
      DO 510 I=1,NEQ
      SP(I,IISENS+J) = ZERO
  510 CONTINUE
C---- CALCULATE COLUMN OF JACOBIAN WITH RESPECT TO VARIABLE THE INIT.
C---  VALUE OF WHICH HAD BEEN PECONSIDERED
C---- MAKE VARIATION
      IPERTU=ISENSI(IISENS-1+J)
      SAVE=S(IPERTU,1)
      VPERT =   DEL * DMAX1(S(IPERTU,1),1.D-7)
      S(IPERTU,1)=S(IPERTU,1)+VPERT
      CALL HORDES(NEQ,TIME,S,F,RPAR,IPAR)
C---- GET SP
      DO 520 I=1,NEQ
      SP(I,IISENS+J)= F(I)/VPERT
  520 CONTINUE
C---- RESET VARIATION
      S(IPERTU,1)=SAVE
  590 CONTINUE
      ENDIF
C***********************************************************************
C
C     CHAPTER IV.: ASSIGN INITIAL VALUES TO SMAX
C
C***********************************************************************
C     DO 20 II=1,NSC
C     I=KONSPE(II)
C     DO 20 J=1,NSE
C     SENMAX(II,J)=S(I,J+1)
C     SENMIN(II,J)=S(I,J+1)
C  20 CONTINUE
C     DO 30 I=1,NEQ
C     SMAX(I,J)=S(I,1)
C     SMIN(I,J)=S(I,1)
C  20 CONTINUE
C**********************************************************************
C     INITIALIZE REACTION RATES
C**********************************************************************
      IF(KONMEC.NE.0) THEN
      CALL POSIT(0,NFIL10,NFILE6,IRETU)
      WRITE(NFIL10,861)
  861 FORMAT('>DATA  ')
      IF(NREAC.NE.NREAT) THEN
        WRITE(6,*) ' fatal error in horiva'
        STOP
      ENDIF
      DO 2791 J=1,NREAT
      RATINT(J)=0.0D0
 2791 CONTINUE
      ENDIF
C***********************************************************************
C     CHAPTER VII.: REGULAR EXIT
C***********************************************************************
  200 CONTINUE
      RETURN
C***********************************************************************
C     END OF -HORIVA-                                                  *
C***********************************************************************
      END
