C>    posdat
      SUBROUTINE POSIT(NCON,NFISTO,MSGFIL,IDAV)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *   Program for the Positioning of a Pointer in a Dataset   *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C    INPUT:
C
C     NCON    : control parameter
C              0,1  position to the first available dataset
C               -1  position to the last available dataset
C                   here dataset means a block of lines, beginning
C                   with the string '>DAT' and ending with '<   '
C                   in any case completeness (occurence of both strings
C                   is checked
C                   on return, the next read or write statement will
C                   begin in the line with sting '>DAT'
C                   if NCON = 0, no check for completeness is performed
C
C     NFISTO       : fortran unit of the file
C
C     MSGFIL       : fortran unit for messages
C                    set MSGFIL to 0 for no output
C
C     IDAV         : flag
C                    IDAV = 1 for successful completion
C                    IDAV = 0 otherwise
C
C
C***********************************************************************
C***********************************************************************
C     storage organization and definition of initial values
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL TEST
      CHARACTER*4 NNNN
C***********************************************************************
C     Initialize
C***********************************************************************
                      TEST=.TRUE.
      IF(MSGFIL.EQ.0) TEST=.FALSE.
      IDAV=1
      IF(NCON.EQ. 0) GOTO  50
      IF(NCON.EQ. 1) GOTO 100
      IF(NCON.EQ.-1) GOTO 300
C***********************************************************************
C
C     position to the first string >dat ,if not found, to the end
C
C***********************************************************************
   50 CONTINUE
      REWIND NFISTO
   60 READ(NFISTO,800,END=70) NNNN
      IF(NNNN.NE.'>DAT') GOTO 60
      BACKSPACE NFISTO
      GOTO 700
   70 IDAV=0
      RETURN
C***********************************************************************
C
C     position to the first string >dat ,if not found, to the end
C
C***********************************************************************
  100 REWIND NFISTO
  110 READ(NFISTO,800,END=120) NNNN
      IF(NNNN.NE.'>DAT') GOTO 110
      GOTO 130
  120 IF(TEST) WRITE(MSGFIL,810)
      IDAV=0
  130 BACKSPACE NFISTO
      GOTO 700
C***********************************************************************
C
C     block for positioning on last dataset
C
C***********************************************************************
  300 CONTINUE
      REWIND NFISTO
C***********************************************************************
C     look if at least one complete dataset is available
C***********************************************************************
 310  READ(NFISTO,800,END=910) NNNN
      IF(NNNN.NE.'>DAT') GOTO 310
 320  READ(NFISTO,800,END=920) NNNN
      IF(NNNN.NE.'<   ') GOTO 320
C***********************************************************************
C     POSITION TO THE END
C***********************************************************************
 350  CONTINUE
 355  READ(NFISTO,800,END=360) NNNN
      GOTO 355
 360  CONTINUE
      BACKSPACE NFISTO
      BACKSPACE NFISTO
C---- POSITION: LAST LINE OF DATASET
C***********************************************************************
C     search for last "<   " in dataset (actual position is at the end
C***********************************************************************
 400  CONTINUE
 410  READ(NFISTO,800) NNNN
      IF(NNNN.EQ.'<   ') GOTO 420
      BACKSPACE NFISTO
      BACKSPACE NFISTO
      GOTO 410
 420  CONTINUE
      BACKSPACE NFISTO
C***********************************************************************
C     search for last ">DAT"
C***********************************************************************
 450  CONTINUE
 460  READ(NFISTO,800) NNNN
      IF(NNNN.EQ.'>DAT') GOTO 470
      BACKSPACE NFISTO
      BACKSPACE NFISTO
      GOTO 460
 470  CONTINUE
      BACKSPACE NFISTO
      GOTO 700
C***********************************************************************
C     successful exit
C***********************************************************************
  700 CONTINUE
      RETURN
C***********************************************************************
C     error exits
C***********************************************************************
 910  IDAV=0
      IF(TEST) WRITE(MSGFIL,911)
 911  FORMAT(' ',' end of file encountered during search for ">DAT",',
     1           ' position : end of file ')
      BACKSPACE NFISTO
      IDAV=0
      RETURN
 920  CONTINUE
      IF(TEST) WRITE(MSGFIL,921)
 921  FORMAT(' ',' end of file encountered during search for "<   "')
C---- restart and position to >dat or end
      IDAV=0
      GOTO 100
C***********************************************************************
C     format statements
C***********************************************************************
  810 FORMAT(' ',' string ">DAT" not found, positioned to the end ')
  800 FORMAT(A4)
C***********************************************************************
C     end of -POSIT-
C***********************************************************************
      END
