module atools
use tra_mod
use inttra_mod 
implicit none

contains

!
! ###              * Subroutine for reduced integration *                   ###
!
    subroutine integrate(nslo, q, qi, p_init, p_eq, t, ierr)
        ! Incoming variables
        integer, intent(in) :: nslo
        double precision, intent(in) :: q(NEQ, NEQ), qi(NEQ, NEQ), &
            p_init(NEQ), p_eq(NEQ)  
        ! Outgoing variables
        type(tra), intent(out) :: t
        integer, intent(out) :: ierr
        
        ! Local variables
        integer :: nfas, nci, ierr2, j
        type(tra) :: fatra, slotra
        double precision :: p1(NEQ)

        ! Global error indicator
        ierr = 0
        
        nci = nslo + NCON
        nfas = NEQ - nci

        t%nvert = 0
        
        ! Copy initial point into p1
        p1 = p_init

        ! Fast integration
        ierr2 = 0

        call inttra(2, nci, p1, fatra, q, qi, ierr2)
        if (ierr2 /= 0) then
            p1 = p_init
            ierr = 3
        end if

        ! Check data consistence if required
        if (I_checks > 0 .and. ierr == 0) then
            do j = 3, NEQ
                if (minval(fatra%d(j,:)) < -1 .or. &
                    maxval(fatra%d(j,:))>100) then
                    ierr = 4
                    p1 = p_init 
                end if
            end do

            !call chkdom(p1, ierr2)
            !if (ierr2 > 0) then
            !    ierr = 4
            !    p1 = p_init
            !end if

        end if

        ! Slow integration
        ierr2 = 0
        call inttra(1, nci, p1, slotra, q, qi, ierr2)
        write(*,*) 'ierr', ierr2
        if (ierr2 /= 0) then
            ierr = 5
        end if

        ! If both integrations succeeded return fast + slow trajectory,
        ! if only slow integration succeeded, return only slow trajectory
        if (ierr == 0) then
            call fatra%mrg(slotra)
            t = fatra
        else if (ierr < 5) then
            t = slotra
        end if
        call fatra%delete
        call slotra%delete
        if( ierr < 5) then 
            call t%prop
            call output(4, ns = nslo, red = t)
        end if
        
        return
    end subroutine integrate

!
! ###               * Subroutine to calculate GQL basis *                   ###
!
    subroutine gqlbas(p, tdet, rating_ref, minns, ierr)
        ! Incoming variables
        double precision, intent(in) :: p(NEQ)
        type(tra), intent(in) :: tdet
        ! Reference ratings of the best reduced integrations for each species
        double precision, dimension(NEQ-NCON), intent(inout) :: &
            rating_ref 
        ! Outgoing variables
        integer, intent(inout) :: minns, ierr(NEQ-NCON)
        double precision, dimension(NEQ, NEQ) :: q, qi

        ! Local variables
        integer :: i, j, k, nfas, ns, ir, ic, ierr2, minns_loc
        double precision :: refjac(NEQ, NEQ), temp1(NEQ), temp2(NEQ, NEQ), &
            xjac(NEQ, NEQ), qlt, rating_ref_loc(NEQ-NCON)

        ! Variables required by homevc
        integer :: mdim, rdim, iw(3*NEQ)
        double precision :: ar(NEQ), ai(NEQ), trow(NEQ, NEQ), tcol(NEQ, NEQ), &
            avr(NEQ, NEQ), srcone, srconv, rw(10*NEQ*NEQ)
        logical :: tw(1000)
        type(tra) :: t
        

        minns_loc = 100
        rating_ref_loc = 9.9d9
        ierr(1) = 1

        ierr2 = 0
        call trares(2, p, temp1, jac = refjac, ierr = ierr2)
        if (ierr2 > 0) then
            ierr(1) = 2
            return
        end if
        ! Print out reference point, jacobian and gql basis if options "all" is 
        ! on
        if (maxval(IO_table(1:4,1)) > 0) then
            call output(1, ic = ic, refp = p, &
                jac = refjac, gql = q, gqli = qi )
        end if

        qlt = 9.9d9
        dimensions: do i= A_ns(1), A_ns(2)
            write(*,*) 'NEQ = ', neq, 'i = ', i
            nfas = NEQ - i - ncon
            xjac = refjac
            mdim = i
            rdim = 0
            ierr2 = 0
            call homevc(1, NEQ, xjac, NEQ, mdim, rdim, 'S', 'S', ar, ai,&
                trow, NEQ, tcol, NEQ, avr, NEQ, srcone, srconv, rw, &
                10 * NEQ * NEQ, iw, 3 * NEQ, tw, 1000, rpar, ipar, IO_intouti,&
                ierr2)
            if (ierr2 > 0) then
                ierr(i) = -1
                cycle dimensions
            end if
            q = trow
            qi = tcol

            if (A_staban /= 0) then
                ierr2 = 0
                call staban(nfas, tdet%yeq, q, qi, ierr2)
                if (ierr2 > 0) then
                    ierr(i) = 3
                    cycle
                end if
            end if
            ierr2 = 0
            call integrate(i, q, qi, tdet%yi, tdet%yeq, t, ierr2)
            ierr(i) = -ierr2
            if (ierr2 > 4) then
                cycle dimensions
            else
                qlt = t%compare(tdet)
                if (qlt < rating_ref(i)) then
                    rating_ref(i) = qlt
                    ! Print out reference point, jacobian, gql basis and 
                    ! reduced trajectory if option best is enabled
                    if (i < minns) minns = i
                    if (i < minns_loc) minns_loc = i
                    call output(3, rating_ref = rating_ref, minns = minns, &
                        ns = i, jac = refjac, gql = q, gqli = qi, &
                        red = t)
                end if
                if (qlt < rating_ref_loc(i)) then
                    rating_ref_loc(i) = qlt
                end if
            end if
        end do dimensions

        if (minns_loc < 100) then
            call output(2, rating_ref, minns = minns_loc, ir = ir, refp = p, &
                jac = refjac, gql = q, gqli = qi)
        end if
        return
    end subroutine gqlbas
!
!   ###             * Stability check at point "p" *                        ###
!
    subroutine staban(nf, p, q, qi, ierr)
        ! Incoming variables
        double precision, dimension(NEQ, NEQ), intent(in) :: p, q, qi
        integer, intent(in) :: nf
        ! Outgoing variable
        integer, intent(out) :: ierr

        ! Local variables
        integer :: ierr2, nc, i
        double precision :: del(NEQ), xjac(NEQ, NEQ)

        ! Variables required by homevc
        integer :: mdim, rdim, iw(nf*(nf+10))
        double precision :: ar(nf), ai(nf), tr(nf,nf), tc(nf,nf), &
            avr(nf,nf), srcone, srconv, rw(nf*(nf+10)), xjacfast(nf, nf)
        logical :: tw(1000)

        ierr = 0
        ierr2 = 0
        !call masjac(NEQ, 0.d0, p, del, xjac, rpar, ipar, ierr2)
        call trares(2, p, del, jac = xjac, ierr = ierr2)
        if (ierr2 > 0) then
            ierr = - 3
            return
        end if

        nc = NEQ - nf
        mdim = 0
        rdim = 0
        xjac = matmul(xjac, qi)
        xjac = matmul(q, xjac)

        xjacfast = xjac(nc+1:NEQ, nc + 1:NEQ)
        ar = 0.d0
        ai = 0.d0

        ierr2 = 1
        i = -1
        call homevc(i, nf, xjacfast, nf, mdim, rdim, &
            'S', 'S', ar, ai, tr, nf, tc, nf, avr, nf, srcone, srconv, &
            rw, nf*(nf+10), iw , nf*(nf+10), tw, 1000, rpar, ipar, IO_intouti,&
            ierr2)
        if (ierr2 > 0) then
            ierr = -1
            return
        end if

        do i = 1, nf
            if (ar(i) > A_staban_tol) then
                ierr = -2
                return
            end if
        end do

        return
    end subroutine staban

!
!
!
    
    subroutine genpoi_init(pos, smfmax, t)
        !
        ! Generate random point
        !
        type(tra), intent(inout) :: t
        integer, intent(out) :: pos
        double precision, intent(out) :: smfmax(NEQ)
        integer, allocatable :: seed(:)
        double precision :: dummy, dummy2(NEQ)
        integer :: i, k, clock

        select case (A_pointgen)
            case(1)
                i = t%nvert
            case(21)
                call t%igndel(i, dummy, dummy2)
            case(22)
                call t%halftim(i, dummy, dummy2)
            case(4)
                i = t%nvert
            case(51)
                call t%igndel(i, dummy, dummy2)
            case(52)
                call t%halftim(i, dummy, dummy2)
        end select
        pos = i
        do i = 1, NEQ
            smfmax(i) = maxval(t%d(i,1:t%nvert))
        end do
            
        call random_seed(size=k)
        if (k < NEQ) k = NEQ
        allocate(seed(k))
        call system_clock(count=clock)
        seed = clock + 37 * (/ (i -1, i = 1, k) /) 
        call random_seed(put=seed)


        return
    end subroutine genpoi_init


    subroutine genpoi(pos, smfmax, t, rand)

        ! Subroutine to generate a random point
        integer, intent(in) :: pos
        double precision, intent(in) :: smfmax(NEQ)
        type(tra), intent(in) :: t
        double precision, intent(out) :: rand(NEQ)


        integer :: pointgen, no, k, i, r1, r2, ierr, clock, ncor
        integer, allocatable :: seed(:)
        double precision, allocatable :: rno(:)
        double precision :: s, smf(NEQ), del(NEQ), jac(NEQ, NEQ), value, vhit
        logical :: keep

        if (A_pointgen == 1 .or. A_pointgen == 21 .or. A_pointgen == 22) then
            no = NEQ 
        else if (A_pointgen == 3) then
            no = 1
        else if (A_pointgen == 4 .or. A_pointgen == 51 .or. A_pointgen == 52)&
            then
            no = 3
        end if
        allocate(rno(no))

        keep = .false.
        do while(.not.keep)
            keep = .false.
            call random_number(rno)
            if (A_pointgen == 1 .or. A_pointgen == 21 .or. A_pointgen == 22)&
                then
                smf = (/ (rno(i) * smfmax(i), i = 1, NEQ) /)
            else if (A_pointgen == 3) then
                smf = t%d(1:NEQ, nint(rno(1)*t%nvert))
            else if (A_pointgen == 4 .or. A_pointgen == 51 .or. &
                A_pointgen == 52) then

                r1 = max(1, nint( rno(2) * pos))
                r2 = max(1, nint( rno(3) * pos))
                smf = rno(1) * t%d(1:NEQ, r1) + &
                    (1 - rno(1)) * t%d(1:NEQ,r2)
            end if
            ! Do correction

            smf(A_ncor + 2) = 0.d0
            s = sum( (/ (smf(i + 2) * SSPV(i) , i = 1, NSPEC )/) )
            smf(A_ncor + 2) = (1.d+0 - s)/SSPV(A_ncor)
            smf(1:2) = t%d(1:2, 1)

            ierr = 0
            !call chkdom(0, NEQ, smf, smf, ierr, vhit, value)
            if (ierr > 0) cycle

            ierr = 0
            s = 0.d0
           
            call trares(1, smf, del, ierr = ierr)
            if (ierr > 0) cycle
            
            rand = smf
            ! Staban too?
            keep = .true.
        end do

        return

    end subroutine genpoi

    subroutine output_init()
        
        if (IO_table(1, 1) > 0) then
            if (IO_table(1, 5) > 0) then
                open(unit = 100, file = 'refp_all.traj', status = 'replace')
                close(100)
            end if
            if (IO_table(1,6) > 0) then
                open(unit = 100, file = 'refp_all.tec', status = 'replace')
                call techead(100, 3, 10000, (/ 'ic', 'ns', 'td' /))
                close(100)
            end if
        end if
        if (IO_table(1,2) > 0) then
            if (IO_table(1, 5) > 0) then
                open(unit=100, file = 'refp_fin.traj', status = 'replace')
                close(100)
            end if
            if (IO_table(1,6) > 0) then
                open(unit = 100, file = 'refp_fin.tec', status = 'replace')
                call techead(100, 3, 10000, (/ 'ic', 'ns', 'td' /))
                close(100)
            end if
        end if
        if (IO_table(1,3) > 0) then
            if (IO_table(1, 5) > 0) then
                open(unit=100, file = 'refp_bst.tec', status = 'replace')
                call techead(100, 2, A_ns(2) - A_ns(1), &
                    (/ 'ns', 'td' /) )
                close(100)
            end if
            if (IO_table(1,6) > 0) then
                open(unit = 100, file = 'refp_best.traj', status = 'replace')
                close(100)
            end if
        end if
        return
    end subroutine output_init

    subroutine output(pos, rating_ref, minns, ns, ic, ir, refp, jac, gql, &
            gqli, red)
        integer, intent(in) :: pos
        integer, optional :: minns, ic, ir, ns
        double precision, optional :: rating_ref(NEQ-NCON), refp(NEQ), &
            jac(NEQ, NEQ), gql(NEQ, NEQ), gqli(NEQ, NEQ)
        type(tra), optional :: red

        integer :: i, temp1(2), temp2(4)
        character(len=3) :: ps, os

        select case(pos)
        case(1)
            ps = 'all'
            os = '#ic'
            i = ic
        case(2) 
            ps = 'fin'
            os = '#ir' 
            i = ir
        case(3)
            ps = 'bst'
            os = '#ns'
            i = ns
        case(4) 
            ps = 'int'
            os = '#ns'
            i = ns
        end select

        if (IO_table(1, pos) > 0 .and. present(refp)) then
            temp2 = IO_table(1, 5:8)
            call wpoi(minns, rating_ref(minns), ps, refp, temp2)
        end if
        if (IO_table(2, pos) > 0 .and. present(jac)) then
            temp1 = IO_table(2,7:8)
            call wmat(nmat = 1, fno= i, fid = os, fnam = 'jac', mat1 = jac, &
                opt = temp1)
        end if
        if (IO_table(3, pos) > 0 .and. present(gql)) then
            temp1 = IO_table(3,7:8)
            call wmat(nmat = 2, fno = i, fid = os, fnam = 'gql', mat1 = gql, &
                mat2 = gqli, opt = temp1)
        end if
        if (IO_table(4, pos) > 0 .and. present(red)) then
            temp1 = IO_table(4,7:8)
            call wtra(i, os, 'red', red, temp1)
        end if
        return
    end subroutine output

    subroutine stat_prog(istat, ic, ir, minns, rating_ref, ierr)
        integer, intent(in) :: istat, ic, ir, minns
        double precision, intent(in) :: rating_ref(NEQ-NCON)
        integer, intent(out) :: ierr

        integer :: io
        character(len=28) :: sfmt

        ierr = 0
        write(sfmt,'(a6,i2,a20)') "('##',", A_ns(2) - A_ns(1) + 1, &
            "(1X, 1pe9.3), ' ##')"
        open(file = 'live', unit = istat, status = 'old', position = 'append',&
            iostat = io)
        if (io /= 0) goto 900

        write(istat, 100) ic, ir, minns
        write(istat, sfmt) rating_ref(A_ns(1):A_ns(2))

        close(istat)
        return

    100 format('### IC = ', i10, '## IR = ', i10, '## MINNS = ', i10, '###')
        ! Error exists
    900 continue
        write(IO_errf, *) 'Error opening live file, aborting analysis'
        ierr = 1
        return
    end subroutine stat_prog

    subroutine stat_fin(ic, ir, rating_ref, minns)
        integer, intent(in) :: ic, ir, minns
        double precision, intent(in) :: rating_ref(NEQ-NCON)

        integer :: io, i


        open(unit = 8989, file = trim(IO_statf), status = 'new', iostat = io)
        write(8989, *) '    ** Final statistics for global analysis **    '
        write(8989,*) '  '
        write(8989, *) '**  Tried ', ic, ' points, managed to integrate ', ir 
        write(8989, *) ' Best rating for each ns :'
        do i = A_ns(1), A_ns(2)
            write(8989, *) ' => ns = ', i, '; rating = ', rating_ref(i)
        end do
        return
    end subroutine stat_fin

    subroutine ret_stoma(stoma)
        integer, intent(out) :: stoma(:,:)
        integer :: i, temp1(3, NRINS)

        stoma = 0
        temp1 = reshape(ireac(NNRVEC:NNRVEC+3*NRINS), [3, NRINS])
        do i=1, NRINS
            stoma(temp1(1,i),temp1(2,i)) = temp1(3,i)
        end do
        return
    end subroutine ret_stoma
end module atools
