C>AUTOCENTER
      SUBROUTINE EXCINI(NEQ,NSPEC,TIME,NFILE6,V0,P0,CIL,CL,CPL,SUMH,
     1                  SUMRAT,VL,TL,RATL,F)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *            MODEL OF AN AUTOIGNITION CENTER AND            *
C     *                   ITS DYNAMIC BEHAVIOUR                   *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C     IGEOMET := geometry dependence (1:=planar; 2:=cylindrical;
C                                     3:=spherical)
C     R0,RADIUS := location of the kernel boundary
C     GAMMA := isentropic exponent
C     XMEMOL := mean molar mass of mixture
C     VSOUND := speed of sound
C     RPR = dR/dt  := temporal deviation of the kernel boundary
C     VPR = dV/dt  := Temporal deviation of the volume
C***********************************************************************
C     CHAPTER I: STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      DIMENSION F(*),RATL(MNSPE),CIL(MNSPE+MNTHB+1)
C***********************************************************************
C     COMMON BLOCK FOR AUTOIGNITION CENTER
C***********************************************************************
      COMMON/SPOPT1/NEXCET,NENGIN
      COMMON/BEXCAL/RADIUS,QDOT,QEX,WEX,TIMEOLD
C***********************************************************************     
      PARAMETER (ZERO=0.0D0,ONE=1.0D0,SMALL=1.0D-15,RGAS=8.3143D0)
C***********************************************************************
      IGEOMET = NEXCET
      PACT    = CL*RGAS*TL
      GAMMA   = CPL/(CPL-RGAS)
      ALPHA   = (GAMMA-ONE)/(2*GAMMA)
      IF(TIME .LT. SMALL) THEN
C---- USING INITIAL VOLUME MAGNITUDE AS IGNITION KERNEL RADIUS !!!
        R0      = V0
        RADIUS  = R0
        TIMEOLD = 1.0D-99
        QEX     = ZERO
        WEX     = ZERO 
        VSOUND  = ZERO
      ENDIF
      XMEMOL = ZERO       
      DO I=1,NSPEC
         XMEMOL = XMEMOL + CIL(I)/CL*XMOL(I)
      ENDDO
      VSOUND = SQRT(GAMMA*RGAS/XMEMOL*TL)      
C---- CALCULATE dR/dt
      RPR = 2*VSOUND / (GAMMA-ONE) * ((PACT/P0)**ALPHA - ONE)
C---- CALCULATION OF NEW KERNEL BOUNDARY
      IF(RPR .GT. ZERO) RADIUS = RADIUS+RPR*(TIME-TIMEOLD)
C---- CALCULATE dV/dt                
      VPR = IGEOMET*VL/RADIUS * RPR
C---- SPECIES EQUATION
      DO I=1,NSPEC
         F(I) = RATL(I) - CIL(I)*(VPR/VL)     
      ENDDO
C---- ENERGY EQUATION
      HELP1  = (RGAS*TL*SUMRAT-SUMH)/CL
      HELP2  = RGAS*TL*VPR/VL
      F(NEQ) = (HELP1-HELP2) / (CPL-RGAS)
C---- EXOTHERMIC CHEMICAL POWER OF THE CENTER
      QDOT = HELP1/(CPL-RGAS)
      IF(QDOT .LT. ZERO) QDOT = ZERO
C---- EXOTHERMIC CHEMICAL ENERGY OF THE CENTER
      QEX = QEX+QDOT*(TIME-TIMEOLD)
C---- WORK DONE BY THE EXPANSION OF THE KERNEL WEX=pdV
      WEX = WEX+HELP2/(CPL-RGAS)*(TIME-TIMEOLD)
      IF(WEX .LT. ZERO) WEX = ZERO
C*********************************************************************** 
  883 FORMAT(13('+'),5X,A42,5X,13('+'))
  884 FORMAT(13('+'),7X,A6,1X,I4,1X,A21,1X,I4,7X,13('+'))
C***********************************************************************      
      RETURN
C***********************************************************************
  900 WRITE(NFILE6,901)
  901 FORMAT('0',2X)
      WRITE(NFILE6,902)
      WRITE(NFILE6,902)
      WRITE(NFILE6,902)
  902 FORMAT('0',28('+'),' ABNORMAL END -EXCINP- ',28('+'))
      WRITE(NFILE6,901)
      WRITE(NFILE6,901)
      STOP
C***********************************************************************
      END

      SUBROUTINE EXCCAL(TIME,NTA)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *            MODEL OF AN AUTOIGNITION CENTER AND            *
C     *                   ITS DYNAMIC BEHAVIOUR                   *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      COMMON/BEXOUT/RCEN(MNIPAR),QDOTCEN(MNIPAR),QCEN(MNIPAR),
     1              WCEN(MNIPAR)
      COMMON/BEXCAL/RADIUS,QDOT,QEX,WEX,TIMEOLD      
C***********************************************************************
C---- BUILD ARRAY FOR OUTPUT IN 'EXCOUT'
      RCEN(NTA)    = RADIUS
      QDOTCEN(NTA) = QDOT       
      QCEN(NTA)    = QEX
      WCEN(NTA)    = WEX
      TIMEOLD      = TIME
C***********************************************************************
      RETURN
C***********************************************************************
      END

      SUBROUTINE EXCOUT(NFILE6,NTS,V1,V3,T)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *            MODEL OF AN AUTOIGNITION CENTER AND            *
C     *                   ITS DYNAMIC BEHAVIOUR                   *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INCLUDE 'homdim.f'
      DIMENSION T(NTS),V1(NTS),V3(NTS)
C***********************************************************************
C     COMMON BLOCK FOR AUTOIGNITION CENTER  (06.02.2003/O.Maiwald)
C***********************************************************************
      COMMON/BEXOUT/RCEN(MNIPAR),QDOTCEN(MNIPAR),QCEN(MNIPAR),
     1              WCEN(MNIPAR)
C***********************************************************************
      PARAMETER  (ZERO = 0.0D0)
      PARAMETER  (NFIL60 = 60, NFIL61 = 61)
C***********************************************************************
C     II.1: MODEL OF AN EXOTHERMIC CENTER AND ITS DYNAMIC BEHAVIOUR  - 
C           OUTPUT OF RESULTS TO FILE 'fort.33' 
C           (06.02.03 / O.Maiwald)
C***********************************************************************
      QDOTMAX = ZERO
      QMAX    = ZERO
      WMAX    = ZERO
      TMAX    = ZERO
      PMAX    = ZERO
      TQDOTMAX= ZERO
C---- NORMALIZE THE RESULTS OF QDOTCEN,QCEN,WCEN
      DO J=1,NTS
        IF(V3(J) .GT. PMAX) THEN
          PMAX = V3(J)
        ENDIF
        IF(QDOTCEN(J) .GT. QDOTMAX) THEN
          QDOTMAX  = QDOTCEN(J)
          TQDOTMAX = T(J)-T(1)
        ENDIF
        IF(QCEN(J) .GT. QMAX) QMAX=QCEN(J)
        IF(WCEN(J) .GT. WMAX) WMAX=WCEN(J)        
        IF(V1(J) .GT. TMAX)   TMAX=V1(J)
      ENDDO
      
C---- CALCULATE IGNITION DELAY TIME AND EXCITATION TIME
C---- INDUCTION DELAY TIME  TI=T(QDOTMAX*0.05)
C---- EXCITATION TIME      TE=T(QDOTMAX)-TI
        DO J=1,NTS
          IF(T(J) .LT. TQDOTMAX) THEN
            IF(QDOTCEN(J) .LE. QDOTMAX*0.05) TI=T(J)-T(1)
          ENDIF
        ENDDO
        TE=TQDOTMAX-TI 
C---- OUTPUT OF HEADER
        OPEN(UNIT=NFIL60,FILE='Extcenter.dat',POSITION='REWIND')
        WRITE(NFIL60,880) 
        WRITE(NFIL60,*)
C---- OUTPUT OF IGNITION DELAY TIME AND EXCITATION TIME
        WRITE(NFIL60,884) 'INDUCTOIN DELAY TIME TI [s]:',TI
        WRITE(NFIL60,884) 'EXCITATION TIME TE [s]:     ',TE
        WRITE(NFIL60,*)
        WRITE(NFIL60,884) 'MAX. CHEMICAL POWER [K/s]:  ',QDOTMAX
        WRITE(NFIL60,884) 'MAX PRESSURE RATIO [-]:     ',PMAX/V3(1)
        WRITE(NFIL60,*)

C---- OUTPUT OF RESULTS AT EACH TIME STEP
        WRITE(NFIL60,882)
        DO J=1,NTS
           WRITE(NFIL60,883) T(J),RCEN(J),V3(J),V1(J),
     1                  QDOTCEN(J)/QDOTMAX,QCEN(J)/QMAX,WCEN(J)/QMAX
Cola          WRITE(60,883) T(J),Rcen(J)*1000,V3(J)/5,V1(J)/1000,
Cola     1                  QDOTCEN(J)/QDOTMAX,QCEN(J)/QMAX,WCEN(J)/QMAX
        ENDDO
        CLOSE(NFIL60)
	
C---- ADDITIONAL OUTPUT
        OPEN(UNIT=NFIL61,FILE='Extinit.dat',POSITION='REWIND')
        WRITE(NFIL61,886)        
        WRITE(NFIL61,885) RCEN(1),V3(1),V1(1),TI,TE,QDOTMAX,PMAX/V3(1)
        CLOSE(NFIL61)
C***********************************************************************
  880 FORMAT('<',5X,'SIMULATION OF AN EXOTHERMIC CENTER AND ITS',
     1                ' DYNAMIC BEHAVIOUR')
  882 FORMAT(1X,'   t(s)   ',2X,' Rcen (m) ',2X,'Pcen (bar)',2X,
     1            ' Tcen (K) ',2X,' Qdot (ex)',2X,'  Q (ex)  ',2X,
     1            '   pdV    ')
  883 FORMAT(1X,1PE10.4,2X,1PE10.4,2X,1PE10.4,2X,1PE10.4,
     1         2X,1PE10.4,2X,1PE10.4,2X,1PE10.4)
  884 FORMAT(1(1X,A28,1PE11.3))
  885 FORMAT(1X,7(1PE11.3,2X))
  886 FORMAT(1X,' Rcen (m) ',2X,'Pcen (bar)',2X,' Tcen (K) ',2X,
     1          '  Q (ex)  ',2X,' Tign (s) ',2X,'ex TIME(s)',2X,
     1          '   ???    ')
C***********************************************************************
      RETURN
C***********************************************************************
  900 WRITE(NFILE6,901)
  901 FORMAT('0',2X)
      WRITE(NFILE6,902)
      WRITE(NFILE6,902)
      WRITE(NFILE6,902)
  902 FORMAT('0',28('+'),' ABNORMAL END -EXCINP- ',28('+'))
      WRITE(NFILE6,901)
      WRITE(NFILE6,901)
      STOP
C***********************************************************************
      END
