#!/bin/bash
# Extract automatic version number for HOMREA/INSFLA after 
# git commit or git checkout
#
# 2015-05-27 Marc-Sebastian Benzinger
# 2016-01-15 Marc-Sebastian Benzinger
# 
# Get version of master
master=`git merge-base master HEAD`
tag=`git describe --tags --long $master`
version=${tag#*-}
version=${tag%%-*}.${version%%-*}
#
# Get current branch
branch=`git branch --no-color --contains`
branch=${branch##*\*[[:space:]]}
branch=${branch%%[[:space:]]*}
branch=${branch/(/}
version=$version-$branch
#
# Get revision of current branch
if ! [ "$branch" == "master" ]; then
    revision=$((`git rev-list --count HEAD`-`git rev-list --count $master`))
    version=$version-$revision
fi
#
# Search for uncommitted changes
if [[ `git describe --dirty` == *"dirty" ]]; then
    version=$version-dirty
fi    
# Get date of commit
date=`git log -1 --format=format:%ci`
date=${date%% *}
#
# Save version number into file
echo "$version" > version
echo "$date" >> version