###############################################################################
#     Makefile for HOMREA
#     date: 31.05.2005
#     author: U. Maas, R. Stauch
#
#------------------------------------------------------------------------------
#     WARNING!
#	  Before using "make", call "./configure"
#     All configuration, such architecture additional flags should be set up
#        through ./configure
#     Please refer to ./configure --help or http://www.gnu.org/software/autoconf
#------------------------------------------------------------------------------
#
#     History
#       16.01.2009 S. Lipp (Variables changed and edited decently)
#       06.03.2013 S. Benzinger (now works for Windows and Linux)
#       03.04.2014 S. Benzinger (small changes))
#		12.05.2015 A. Koksharov, usage with autoconf (multi-platform deployment)
###############################################################################


#------------------------------------------------------------------------------
# Parameters from the configuration script
#------------------------------------------------------------------------------
CC = gcc
FC = gfortran
USE_MINI = 0
prefix = /usr/local
CFLAGS = -g -O2
FCFLAGS = -fcheck=all -fbacktrace -Og -g -DVERSION_NUM='"unknown"' -DVERSION_DATE='"unknown"' -DCOMLIB_NUM='"unknown"' -DCOMLIB_DATE='"unknown"'
LIBS =

#------------------------------------------------------------------------------
# directories
#------------------------------------------------------------------------------
HOMEDIR  = ..
# Sources
PRODIR   = $(HOMEDIR)/homrea
INPSRC   = $(PRODIR)/hominp
RUNSRC   = $(PRODIR)/homrun
# directories for library
LIBDIR   = $(HOMEDIR)/library
MIGSRC   = $(LIBDIR)/MiniGraphik
# Object linking rules
MAKEDIR  = $(PRODIR)/mkincludes

# Output
BINDIR = $(PRODIR)/bin-$(FC)
OBJDIR = $(PRODIR)/obj-$(FC)

#------------------------------------------------------------------------------
# assign VPATH to search for sources
#------------------------------------------------------------------------------
VPATH = $(INPSRC):$(RUNSRC):$(LIBDIR):$(MIGSRC)
# Force .mod file to the object dir for gfortran
ifeq ($(FC),gfortran)
  FCFLAGS += -J$(OBJDIR)
endif
ifeq ($(FC),ifort)
  FCFLAGS += -module $(OBJDIR)
endif

#------------------------------------------------------------------------------
# minigraphik objects
#------------------------------------------------------------------------------
ifeq ($(USE_MINI),1)
# preprocessor directive MINI.
  FCFLAGS += -DMINI
$(OBJDIR)/xdriv.o: xdriv.c minigraph.h
	$(CC) $(MIGSRC)/xdriv.c $(CFLAGS) $(LIBS) -c -o $(OBJDIR)/xdriv.o
$(OBJDIR)/mgdp.o: mg2dpl.f
	$(FC) -c $(MIGSRC)/mg2dpl.f $(FCFLAGS) -o $(OBJDIR)/mg2dpl.o
$(OBJDIR)/mg2dx.o: mg2dx.f
	$(FC) -c $(MIGSRC)/mg2dx.f $(FCFLAGS) -o $(OBJDIR)/mg2dx.o
$(OBJDIR)/mg3dpl.o: mg3dpl.f
	$(FC) -c $(MIGSRC)/mg3dpl.f $(FCFLAGS) -o $(OBJDIR)/mg3dpl.o

include $(MAKEDIR)/homrun_minigraphic_objects.mk
$(MIGOBJ) : config.h
endif
#------------------------------------------------------------------------------
# rule for object files
#------------------------------------------------------------------------------
$(OBJDIR)/%.o: %.f
	$(FC) $(FCFLAGS) -c $< -o $@
$(OBJDIR)/%.o: %.f90
	$(FC) $(FCFLAGS) -c $< -o $@
$(OBJDIR)/%.o: %.F
	$(FC) $(FCFLAGS) -c $< -o $@
$(OBJDIR)/%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@
#------------------------------------------------------------------------------
# hominp objects
#------------------------------------------------------------------------------
include $(MAKEDIR)/hominp_objects.mk
INPOBJ := $(addprefix $(OBJDIR)/,$(HOMIOBS))
#------------------------------------------------------------------------------
# homrun objects
#------------------------------------------------------------------------------
include $(MAKEDIR)/homrun_objects.mk
include $(MAKEDIR)/homrun_library_objects.mk
HOMROBS= $(HOMRLO) $(HOMRIO) $(MINOBJ)
RUNOBJ := $(addprefix $(OBJDIR)/,$(HOMROBS))
$(RUNOBJ) : $(RUNSRC)/homdim.f
.PHONY : inp run clean help cleanobj cleanbin cleanmod install
###############################################################################
#    Linking objects to binaries
###############################################################################
all : inp run 
inp : $(INPOBJ)
	$(FC) $(INPOBJ) $(LDFLAGS) -o $(BINDIR)/hominp$(EXEEXT)
run : $(RUNOBJ)
	$(FC) $(RUNOBJ) $(LDFLAGS) $(LIBS) -o $(BINDIR)/homrun$(EXEEXT)
###############################################################################
#    Cleaning objects
###############################################################################
clean     : cleanbin cleanobj cleanmod
cleanbin  :
	rm -f $(BINDIR)/*$(EXEEXT)
cleanobj  :
	rm -f $(OBJDIR)/*.o
cleanmod  :
	rm -f $(OBJDIR)/*.mod
	rm -f *.mod

###############################################################################
#    Installing binaries to system common path
###############################################################################
install:
	$(PRODIR)/install-sh -c -m 644 $(BINDIR)/hominp$(EXEEXT) $(prefix)/bin//hominp$(EXEEXT)
	$(PRODIR)/install-sh -c -m 644 $(BINDIR)/homrun$(EXEEXT) $(prefix)/bin//homrun$(EXEEXT)

###############################################################################
#    Help for the Makefile
###############################################################################
help:
	@echo
	@echo "use ./configure to set up the make file (i.e. compiler and options)"
	@echo " "
	@echo "Options: "
	@echo "make all  	- compile hominp and homrun"
	@echo "make clean 	- remove objects and binaries"
	@echo "make install - install hominp and homrun to the common path"
	@echo "make inp 	- compile hominp only"
	@echo "make run		- compile homrun only"
	@echo "make help	- show this help"

