C>    hominp_main
      PROGRAM HOMINP
C***********************************************************************
C
C    .-------------------------------------------------------------.
C    |                                                             |
C    |      Program for the Generation of a Link-File for the      |
C    |          Simulation of a Chemical Reaction System           |
C    |                                                             |
C    |                        28.02.1984                           |
C    |              Last change: 12.14.1989/U.Maas                 |
C    |                                                             |
C    '-------------------------------------------------------------'
C
C***********************************************************************
C
C
C***********************************************************************
C                                                                      C
C     INPUT  : OPTIONS           FROM UNIT NFILE2  IN SUBR. HOIOPT     C
C              SPECIES SYMBOLS   FROM UNIT NFILE2  IN SUBR. GENSYM     C
C              CONDITIONS        FROM UNIT NFILE2  IN SUBR. HOICON     C
C              COLLIS. EFFIC.    FROM UNIT NFILE2  IN SUBR. HOICET     C
C              ELEM.REACTIONS    FROM UNIT NFILE4  IN SUBR. HOIMEC     C
C              THERMOCHEM. DATA  FROM UNIT NFILE9  IN SUBR. HOISPE     C
C                                                                      C
C     OUTPUT : INPUT DATA               ON   UNIT NFILE6               C
C              DATA FOR INTEGRATION     ON   UNIT NFILE3               C
C                                                                      C
C**********************************************************************C
C                                                                      C
C  NOTES:                                                              C
C                                                                      C
C                                                                      C
C                                                                      C
C  Options:                                                            C
C                                                                      C
C     TIGN    I     : IGNITION DELAY TIME IS DERIVED FOR SPECIES I     C
C                                                                      C
C     EXTRA         : USE OF EXTRAPOLATION INTEGRATOR                  C
C                                                                      C
C     GSENS         : GLOBAL SENSITIVITY ANALYSIS WITH RESPECT TO ALL  C
C                     REACTIONS AND THE SPECIES SPECIFIED IN SPECIES   C
C                     LIST                                             C
C                                                                      C
C     LSENS         : GLOBAL SENSITIVITY ANALYSIS WITH RESPECT TO REA- C
C                     CTIONS AND SPECIES SPECIFIED IN MECHANISM AND    C
C                     SPECIES LIST                                     C
C                                                                      C
C     MOLE    1     : OUTPUT OF MOLE FRACTIONS                         C
C      "      2     : OUTPUT OF MASS FRACTIONS                         C
C                                                                      C
C     NOCHEC        : NO CHECK OF MECHANISM FOR IDENTICAL REACTIONS    C
C                     (TO SAVE TIME FOR VERY LARGE MECHANISMS)         C
C                                                                      C
C     NOMEC         : NO OUTPUT OF MECHANISM FOR -TSO -                C
C                                                                      C
C     OUTPUT  1     : OUTPUT OF INTERMEDIATE RESULTS FOR MARKED SPECIESC
C       "     2     : OUTPUT OF INTERMEDIATE RESULTS FOR ALL SPECIES   C
C       "     3     : ADDITIONAL INTEGRATION MONITOR                   C
C       "     4     : ADDITIONAL OUTPUT FOR TESTING                    C
C                                                                      C
C     PLOT    1     : PLOT OF THE RESULTS FOR MARKED SPECIES           C
C             2     : PLOT OF THE RESULTS FOR ALL SPECIES              C
C                                                                      C
C     STOMEC        :                                                  C
C                                                                      C
C     TABLE   1     : TABLES OF THE RESULTS FOR MARKED SPECIES         C
C       "     2     : TABLES OF THE RESULTS FOR ALL SPECIES            C
C                                                                      C
C     TPRO          : PROFILE OF TEMPERATURE (CONSTANT OR VARIABLE)    C
C                     GIVEN ---NO ENERGY-CONSERVATION IN THE SYSTEM    C
C                                                                      C
C     TSO           : OUTPUT OF SMALL DRAWINGS FOR TERMINAL,           C
C                     INTERACTIVE CALCULATION (BATCH OUTPUT OTHERWISE) C
C                                                                      C
C     VPRO          : PROFILE OF VOLUME (CONSTANT OR VARIABLE) GIVEN   C
C                     PROFILE OF PRESSURE ASSUMED OTHERWISE            C
C                                                                      C
C     WALL          :                                                  C
C                                                                      C
C     complx        : Calculation with complex reactions               C
c                                                                      c
c     THETA  1      : fixed coordinates (ILDM Calculation)             c
C            2      : generalized coordinates                          C
c                                                                      c
c     LaLoop--      : for last cells (ILDM)                            c
c                                                                      c
C Karin Koenig                                                          C
C     MANSENS       : SENSITIVITY OF AN ILDM                           C
Cend Karin Koenig                                                       C
C                                                                      C
C     ENGINE 1      : ENGINE MODEL                                     C
C                                                                      C
C     MULZON I      : MULTIZONE MODEL WITH I REACTORS                  C
C                                                                      C
C**********************************************************************C
C                                                                      C
C**********************************************************************C
C                                                                      C
C                                                                      C
C                    BEGIN OF MAIN PROGRAM                             C
C                                                                      C
C                                                                      C
C**********************************************************************C
C**********************************************************************C
C     CHAPTER I: STORAGE ORGANIZATION,  INITIAL VALUES                 C
C**********************************************************************C
C'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
C     2.1   Data Types
C.......................................................................
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C***********************************************************************
C     Work arrays
C***********************************************************************
      LOGICAL STOP,TEST,TSO,NCHECK,CHECK,NOMECH,ELEMEN,NEWMOL,LW
      LOGICAL TROE,REVSIM,REDPAR
      PARAMETER (LRW = 100000, LIW = 1159, LLW = 1159)
      DIMENSION RW(LRW),IW(LIW),LW(LLW)
      PARAMETER (MNSPE=4000,MNTHB=28,MNREA=9278,NRIMAX=6*MNREA,
     1           MNELE=20,MNF = 6, MNCV = 20,MNZONE = 70,
     1           MNPOI=300,MNCRE=20,MNLIM=100)
C'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
C     2.2   Arrays
C.......................................................................
C---- Storage for element informations nsmax,nemax
      CHARACTER*2 SELE(4,MNSPE),SYME(MNELE)
      DIMENSION NELE(4,MNSPE),NCOMPE(MNSPE,MNELE),XE(MNSPE,MNELE),
     1          XMOL(MNSPE),EMOL(MNELE)
C---- Storage for reactions (to be compatible to NRMAX in DATA statem.)
      DIMENSION PK(MNREA),TEXP(MNREA),EXA(MNREA),
     1          IREVER(MNREA),ISENS(MNREA)
      DIMENSION VT4(MNREA),VT5(MNREA),VT6(MNREA),IARRH(MNREA)
      DIMENSION NUMC(MNREA),ISTICK(MNREA)
C---- Storage for falloff data
      DIMENSION AINF(MNREA), BINF(MNREA), EINF(MNREA),
     1          T3(MNREA), T1(MNREA), T2(MNREA),
     1          AFA(MNREA), BFA(MNREA)
C---- Storage for species (to be compatible to NSMAX in DATA statem.)
      DIMENSION INERT(MNSPE),XU(MNSPE),
     1          NCHECK(MNSPE),
     1          HCPL(7,MNSPE),HCPH(7,MNSPE),XIPR(MNSPE),
     1          HINF(3,MNSPE),
     1          IFSP(MNSPE)
C---- Storage for NSMAX+4 species
      PARAMETER(LENSPE=8)
      CHARACTER(LEN=LENSPE)  NSYMB(MNSPE+MNTHB+1)
      CHARACTER(LEN=LENSPE)  VSYMB(MNSPE+2)
C---- Storage for NRMAX reactions, NSMAX+4 species
      DIMENSION KOEFL(MNREA,MNSPE+MNTHB),KOEFR(MNREA,MNSPE+MNTHB)
C---- Storage for temperature, pressure, and volume history )
      DIMENSION TU(MNPOI),P(MNPOI),PSI(MNPOI),VU(MNPOI),TIM(3,MNPOI)
C---- Storage for complex reactions (<MNCRE reactions)
      DIMENSION ABRU(MNCRE),TBRU(MNCRE),EBRU(MNCRE),ORD(MNCRE,6),
     1          NUMS(MNCRE,6),STOBR(MNCRE,6)
C---- Miscellaneous
      PARAMETER (LENCOL=8)
      CHARACTER(LEN=LENCOL), DIMENSION(MNTHB) ::  MCOLLI
      DIMENSION EFF(MNSPE*MNREA),VALUE(12)
      DIMENSION IENHANCE(MNREA),XENHANCE(MNSPE*MNREA)
      DIMENSION TF(MNF),HF(MNF),PF(MNF),RHOF(MNF),PHIF(MNF)
      DIMENSION  WF(MNSPE*MNF),XF(MNSPE*MNF),EWF(MNELE*MNF),
     1      EXF(MNELE*MNF)
      DIMENSION MATRV(MNSPE*MNCV),IWORK1(MNSPE,MNSPE),
     1     IWORK3(MNSPE),RWORK(MNSPE,MNSPE),QCON(MNSPE*MNCV),
     1     ECV(2*MNCV),
     1     XLIM(MNLIM,MNSPE),ELIM(2,MNLIM),
     1     ICS(MNSPE),IRM(MNSPE)
      DIMENSION ZJ(MNELE)
      DIMENSION MAT(12,MNREA),NRVEC(3,6*MNREA)
      DIMENSION NOPT(60)
      CHARACTER*8 NTOPT(60)
      PARAMETER (MNSPE4=4*MNSPE)
C'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
C     2.3   DATA Statements
C.......................................................................
      DATA NSMAX/MNSPE/,NRMAX/MNREA/,NEMAX/MNELE/,NCVMAX/MNCV/
      DATA NFILE3/03/,NFILE4/04/,NFILE2/02/,NFILE6/06/,NFILE9/09/,
     1     NFIL10/10/,NFIL11/11/,NFIL12/12/,NFILE1/01/
      DATA SELE/MNSPE4*'  '/,NELE/MNSPE4*0/
C***********************************************************************
C
C***********************************************************************
      DATA NUMOPT/60/
      DATA NTOPT(1) /'VProfile'/,NTOPT(2) /'TProfile'/,
     1     NTOPT(3) /'ResFile '/,NTOPT(4) /'Wall    '/,
     1     NTOPT(5) /'Engine  '/,NTOPT(6) /'PSR     '/,
     1     NTOPT(7) /'State   '/,NTOPT(8) /'REST    '/,
     1     NTOPT(9) /'ISens.  '/,NTOPT(10)/'GSens.  '/,
     1     NTOPT(11)/'LSens.  '/,NTOPT(12)/'STOMech '/,
     1     NTOPT(13)/'ILDM    '/,NTOPT(14)/'ManPar  '/,
     1     NTOPT(15)/'Tables  '/,NTOPT(16)/'Plots   '/,
     1     NTOPT(17)/'Mole    '/,NTOPT(18)/'TIgnit. '/,
     1     NTOPT(19)/'LogaPlot'/,NTOPT(20)/'Extrap. '/,
     1     NTOPT(21)/'Transp. '/,NTOPT(22)/'NOx     '/,
     1     NTOPT(23)/'Fuel    '/,NTOPT(24)/'INMAN   '/,
     1     NTOPT(25)/'Theta   '/,NTOPT(26)/'LaLoop  '/,
     1     NTOPT(27)/'Mansens '/,NTOPT(28)/'TGILM   '/,
     1     NTOPT(29)/'JacUsed '/,NTOPT(30)/'LocJac--'/,
     1     NTOPT(31)/'OrdILDM '/,NTOPT(32)/'Mulzon  '/,
     1     NTOPT(33)/'UpdVert-'/,NTOPT(34)/'RevSim--'/,
     1     NTOPT(35)/'Complx  '/,NTOPT(36)/'Center  '/,
     1     NTOPT(37)/'NoCheck '/,NTOPT(38)/'NoMech. '/,
     1     NTOPT(39)/'Output  '/,NTOPT(40)/'TSO     '/,
     1     NTOPT(41)/'LocAna  '/,NTOPT(42)/'GQL     '/
C***********************************************************************
C     Define collision body symbols
C***********************************************************************
      DATA NMMAX/MNTHB/
      DATA MCOLLI(1) /'M      '  /,MCOLLI(2) /'M''      '/,
     1     MCOLLI(3) /'M''''     '  /,MCOLLI(4) /'M''''''    '/,
     1     MCOLLI(5) /'M''''''''   '    /,MCOLLI(6) /'M''''''''''  '/
     1     MCOLLI(7) /'M'''''''''''' '  /,MCOLLI(8) /'M'''''''''''''''/
     1     MCOLLI(9) /'M*      '       /,MCOLLI(10)/'M**     '      /,
     1     MCOLLI(11)/'M***    '       /,MCOLLI(12)/'M****   '      /,
     1     MCOLLI(13)/'M*****  '       /,MCOLLI(14)/'M****** '      /,
     1     MCOLLI(15)/'M*******'/,
     1     MCOLLI(16)/'M(1)    '       /,MCOLLI(17)/'M(2)    '      /,
     1     MCOLLI(18)/'M(3)    '       /,MCOLLI(19)/'M(4)    '      /,
     1     MCOLLI(20)/'M(5)    '       /,MCOLLI(21)/'M(6)    '      /,
     1     MCOLLI(22)/'M(7)    '       /,MCOLLI(23)/'M(8)    '      /,
     1     MCOLLI(24)/'M(9)    '       /,MCOLLI(25)/'M(10)   '      /,
     1     MCOLLI(26)/'M(11)   '       /,MCOLLI(27)/'M(12)   '      /
C***********************************************************************
C     OUTPUT OF HEADING ON MSGFIL
C***********************************************************************
CBZ version number included
      WRITE(NFILE6,8100) VERSION_NUM,VERSION_DATE,
     1                   COMLIB_NUM,COMLIB_DATE
 8100 FORMAT(1X,/,1X,79('*'),/,
     1       1X,4('*'),4X,'PROGRAM HOMREA - HOMINP',/,
     1       1X,4('*'),4X,'BUILD VERSION: ',A,/,
     1       1X,4('*'),4X,'BUILD DATE: ',A,/,
     1       1X,4('*'),4X,'LIBRARY VERSION: ',A,/,
     1       1X,4('*'),4X,'LIBRARY DATE: ',A,/,
     1       1X,79('*'))
CBZ end
C**********************************************************************C
C     CHAPTER II: REMOVE COMMENT CARDS ON NFILE2 AND NFILE4
C**********************************************************************C
C---- Remove comment cards in input on NFILE2
      CALL REMCOM(NFILE6,NFILE2,NFIL10)
      NFICON=NFIL10
C---- Remove comment cards in input on NFILE4
      CALL REMCOM(NFILE6,NFILE4,NFIL11)
      NFIMEC=NFIL11
C---- Asssign file number for thermodynamics
C---- Remove comment cards in input on NFILE9
      CALL REMCOM(NFILE6,NFILE9,NFIL12)
      NFITHE=NFIL12
C---- Asssign file number for storage
      NFISTO=NFILE3
C---- Asssign file number for storage
      MSGFIL=NFILE6
C---- Asssign file number for species data
      NFISPE=NFILE1
C**********************************************************************C
C     CHAPTER III: INPUT OF OPTIONS                                    C
C**********************************************************************C
      NEWMOL = .TRUE.
      STOP=.TRUE.
   20 FORMAT(' ',2X)
      WRITE(MSGFIL,20)
      CALL GENOPT(NUMOPT,NFICON,MSGFIL,NFISTO,NTOPT,NOPT,STOP)
      IF(.NOT.STOP) GO TO 10000
C***********************************************************************
C     INITIALIZE TEST AND TSO OPTION
C***********************************************************************
      TEST=.FALSE.
      IF(NOPT(39).GE.4) TEST  =.TRUE.
      TSO =.FALSE.
      IF(NOPT(40).NE.0) TSO   =.TRUE.
      CHECK=.TRUE.
      IF(NOPT(37).NE.0) CHECK =.FALSE.
      NOMECH=.FALSE.
      IF(NOPT(38).NE.0) NOMECH =.TRUE.
      ELEMEN=.FALSE.
CASSA      IF(NOPT(7).NE.0.OR.NOPT(17).GE.2) ELEMEN =.TRUE
      REDPAR =.FALSE.
      IF(NOPT(13).NE.0.OR.NOPT(24).NE.0) REDPAR = .TRUE.
      IF(NOPT(7).NE.0) ELEMEN =.TRUE.
      IEQ = 1
      IF(NOPT(1).GT.0) IEQ = 2
      TROE=.FALSE.
C---- Logical for Simultaneous Calc. of Reverse Reaction Rate in HOMRUN
      REVSIM=.FALSE.
      IF(NOPT(34).NE.0) REVSIM=.TRUE.
C**********************************************************************C
C     CHAPTER IV: INPUT OF SPECIES SYMBOLS                             C
C**********************************************************************C
C      CALL GENSYM(NFICON,MSGFIL,NS,NSMAX,NSYMB,IFSP,IERR)
      CALL GENSYM('Species',NFICON,MSGFIL,NS,NSMAX,NSYMB,IFSP,IERR)
      IF(IERR.GT.0) GO TO 10000
C**********************************************************************C
C     Input of Thermodynamic Data                                      C
C**********************************************************************C
      CALL GENTHE(NS,NFITHE,MSGFIL,NSYMB,TEST,NCHECK,
     1     HCPL,HCPH,HINF,SELE,NELE,STOP)
      IF(.NOT.STOP) GOTO 10000
C**********************************************************************C
C     Calculation of Element Composition                               C
C**********************************************************************C
      CALL ELECOM(NS,SELE,NELE,NEMAX,NE,SYME,NCOMPE,XE,XMOL,EMOL,
     1                       MSGFIL,STOP)
      IF(.NOT.STOP) GOTO 10000
C**********************************************************************C
C     Calculation of Element Composition                               C
C**********************************************************************C
      CALL INIMOI(NS,NSYMB,XMOL,NE,SYME,EMOL,XE,NFISTO)
C---- NCOMPE CONTAINS IN ITS COLUMNS THE ELEMENT VECTORS
C***********************************************************************
C     Generation of species data
C***********************************************************************
      IF(NOPT(21).GT.0) THEN
      TUTRA=   0.0
      TBTRA=2500.0
      CALL GENSPD(NS,NFISPE,NFISTO,MSGFIL,NSYMB,XMOL,
     1     HCPL,HCPH,HINF,TUTRA,TBTRA,STOP,TEST,NEWMOL,
     1       LRW,RW,LIW,IW,LLW,LW)
      IF(.NOT.STOP) GO TO  10000
      ENDIF
C**********************************************************************C
C     GENERATION OF MOLAR SPECIFIC HEAT CAPACITIES / ENTHALPIES
C**********************************************************************C
      CALL PRISPE(MSGFIL,NFISTO,TEST,ELEMEN,
     1            NS,NSYMB,XMOL,HCPL,HCPH,HINF,
     1            NE,EMOL,XE,STOP)
c     1            NE,SYME,EMOL,XE,STOP)
C**********************************************************************C
C     CHAPTER VIII: INPUT OF REACTION MECHANISM                        C
C**********************************************************************C
	ICIN = 0
      CALL GENMEC(ICIN,NRMAX,
     1     NS,NS,NSYMB,NFIMEC,MSGFIL,NFISTO,PK,TEXP,EXA,
     1     VT4,VT5,VT6,IARRH,ISTICK,NUMC,AINF,BINF,EINF,T3,T1,
     1     T2,AFA,BFA,HCPL,HCPH,HINF,NM,NMMAX,MCOLLI,
     1     INERT,NINERT,IREVER,ISENS,
     1     MAT,KOEFL,KOEFR,NR,NRIMAX,NRVEC,IENHANCE,XENHANCE,EFF,
     1     NMTOT,
     1     STOP,TEST,NOMECH,TSO,
     1     REVSIM,TROE)
      WRITE(MSGFIL,20)
      IF(.NOT.STOP) GO TO 10000
C**********************************************************************C
C     CHAPTER IX: INPUT OF COLLISION EFFICIENCIES                      C
C**********************************************************************C
      REWIND NFICON
C Rainer Stauch 06.09.2004: 1st search in mechanism, 2nd in inputfile
      CALL GENCEF(NR,NS,NM,NMMAX,NMTOT,
     1    NSYMB,NFIMEC,NFICON,MSGFIL,NFISTO,
     1    KOEFL,EFF,
     1     STOP)
Cend Rainer Stauch
      WRITE(MSGFIL,20)
      IF(.NOT.STOP) GO TO 10000
C**********************************************************************C
C     CHAPTER X:  INPUT OF REACTION VECTORS                            C
C**********************************************************************C
      REWIND NFICON
      IF(ELEMEN) THEN
        NEQ= NS+2
        CALL MANPAR(NCVMAX,NS,NSYMB,NEQ,VSYMB,NE,SYME,NCOMPE,
     1        NFICON,NFISTO,MSGFIL,
     1        IRM,IWORK1,IWORK3,RWORK,NRV,MATRV,ELEMEN,QCON,ECV,
     1        MNLIM,XLIM,ELIM,STOP,TEST,TSO,IEQ)
        IF(.NOT.STOP) GO TO 10000
C***********************************************************************
C A. Neagos:
C read REDIM Options
C***********************************************************************
      IF (NOPT(24).GE.1) THEN
        REWIND NFICON
        call redimopt(NFICON,NFISTO,MSGFIL,STOP)
      end if
C***********************************************************************
C A. Neagos:
C Not necessary to call tabpar and redmec if INMAN(NOPT(24))=1,
C e.q. REDIM is generated
C***********************************************************************
      IF (NOPT(24).LT.1) THEN
C**********************************************************************C
C     CHAPTER X:  INPUT OF REACTION VECTORS                            C
C**********************************************************************C
        CALL TABPAR(NS,NSYMB,NE,SYME,NCOMPE,XE,XMOL,
     1        HCPL,HCPH,HINF,EMOL,MNF,TF,HF,PF,RHOF,PHIF,WF,XF,EWF,EXF,
     1        NFICON,NFISTO,MSGFIL,
     1        ELEMEN,STOP,TEST,TSO,IEQ)
        IF(.NOT.STOP) GO TO 10000
C**********************************************************************C
C     CHAPTER X:  INPUT OF CONVENTIONAL MECH. RED.                     C
C**********************************************************************C
        CALL REDMEC(NS,NSYMB,NRMAX,NR,KOEFL,KOEFR,IREVER,
     1       NFICON,NFISTO,MSGFIL,IWORK1,STOP,TEST,TSO,
     1       NCS,NPE,NQS,NZS,ICS)
      ENDIF
      ENDIF
C***********************************************************************
C     INPUT OF COMPLEX REACTIONS
C***********************************************************************
      CALL GENCRE(NS,NR,0,NSYMB,NFIMEC,MSGFIL,NFISTO,0,ABRU,TBRU,EBRU,
     1     ORD,NUMS,STOBR,MNCRE,NBRUT,STOP)
      IF((NOPT(35) .GT. 0 .AND. NBRUT .EQ. 0) ) THEN
        WRITE(MSGFIL,840)' '
        WRITE(MSGFIL,840)'COMPLEX REACTIONS ERROR (-HOMINP_MAIN-)      '
        WRITE(MSGFIL,840) ' complex reactions requested but not defined'
        WRITE(MSGFIL,840)' '
        GO TO 10000
      ENDIF
      IF( (NOPT(35) .EQ. 0 .AND. NBRUT .GT. 0)) THEN
        WRITE(MSGFIL,840)' '
        WRITE(MSGFIL,840)'COMPLEX REACTIONS WARNING (-HOMINP_MAIN-)   '
        WRITE(MSGFIL,840) ' complex reactions defined but not requested'
        WRITE(MSGFIL,840)' '
       ENDIF
C       write(*,*) 'STOP',stop
  840 FORMAT(6X,9('*'),5X,A45)
C      WRITE(MSGFIL,20)
C       write(*,*) 'OOOOOOOOOO'
      IF(.NOT.STOP) GO TO 10000
C       write(*,*) 'OOOOOOOOOO'
C***********************************************************************
C     Output of species flags
C***********************************************************************
cjoergb
c  850 FORMAT(' Spec. Flags',3X,20I3,/,1(15X,20I3))
  850 FORMAT(' SPEC. Flags',3x,20I3)
cjoergb---
      WRITE(NFISTO,850) (IFSP(I),I=1,NS)
C**********************************************************************C
C     CHAPTER VI: INPUT OF BOUNDARY AND REACTION CONDITIONS            C
C**********************************************************************C
C***********************************************************************
C A. Neagos:
C Not necessary to read input for homrea calculation if manifold is
C generated (OPTION ILDM=1)
C***********************************************************************
      IF (NOPT(24).LT.1) THEN
      REWIND NFICON
      CALL HOICON(NS,NE,NOPT,NSYMB,SYME,NFICON,MSGFIL,NFISTO,
     1  HCPL,HCPH,HINF,XMOL,NECO,ZJ,VALUE,XU,TU,P,PSI,VU,TIM,XIPR,
     1  NCOMPE,ELEMEN,MNPOI,MNZONE,STOP)
      END IF
      WRITE(MSGFIL,20)
  481 FORMAT(' ',79('*'))
      IF(TSO) WRITE(MSGFIL,481)
  482 FORMAT(' ',132('*'))
      IF(.NOT.TSO) WRITE(MSGFIL,482)
      IF(.NOT.STOP) GO TO 10000
      WRITE(MSGFIL,*)' '
      IF(     TSO) WRITE(MSGFIL, 829)
      IF(.NOT.TSO) WRITE(MSGFIL,1829)
      WRITE(MSGFIL,1830)' HOMINP: NORMAL END '
      IF(     TSO) WRITE(MSGFIL, 829)
      IF(.NOT.TSO) WRITE(MSGFIL,1829)
  829 FORMAT(1X,79('*'))
 1829 FORMAT(1X,132('*'))
 1830 FORMAT(1X,10('*'),19X,A20,20X,10('*'))
      STOP
C**********************************************************************C
C     CHAPTER X: ABNORMAL END                                          C
C**********************************************************************C
10000 CONTINUE
 9999 FORMAT(' ',5X,31('*'),'Anormal End',31('*'))
      WRITE(MSGFIL,9999)
      WRITE(MSGFIL,9999)
      WRITE(MSGFIL,9999)
      STOP
C**********************************************************************C
C     END OF MAIN PROGRAM                                              C
C**********************************************************************C
      END

      SUBROUTINE PRISPE(MSGFIL,NFISTO,TEST,ELEMEN,
     1     NS,NSYMB,XMOL,HCPL,HCPH,HINF,
     1     NE,EMOL,XE,STOP)
C**********************************************************************C
C                                                                      C
C    ***************************************************************   C
C    *                                                             *   C
C    *    PROGRAM FOR THE PRINTOUT   OF SPECIES DATA               *   C
C    *                                                             *   C
C    *                     AUTHOR: U.MAAS                          *   C
C    *                         09/22/1989                          *   C
C    *                                                             *   C
C    *                LAST CHANGE: 19.10.1984/MAAS                 *   C
C    *                                                             *   C
C    ***************************************************************   C
C                                                                      C
C**********************************************************************C
C                                                                      C
C     INPUT  :                                                         C
C                                                                      C
C     NS            = NUMBER OF SPECIES                                C
C     NE            = NUMBER OF ELEMENTS                               C
C     NFISTO        = NUMBER OF OUTPUT FILE FOR STORAGE                C
C     MSGFIL        = NUMBER OF OUTPUT FILE FOR LISTING                C
C     NSYMB(NS)     = SPECIES SYMBOLS                                  C
C     TEST          = .TRUE. FOR ADDITIONAL OUTPUT, =.FALSE. OTHERWISE C
C     ELEMEN        =                                                  C
C                                                                      C
C     STOP          = .TRUE. FOR NORMAL END, .FALSE. FOR ABNORMAL END  C
C                                                                      C
C                                                                      C
C**********************************************************************C
C                                                                      C
C                                                                      C
C                                                                      C
C**********************************************************************C
C     STORAGE ORGANIZATION AND DEFINITION OF INITIAL VALUES            C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL STOP,TEST,ELEMEN
      CHARACTER(LEN=*)  NSYMB(NS)
      DIMENSION XE(NS,*),XMOL(NS),EMOL(*)
      DIMENSION HCPL(7,NS),HCPH(7,NS),HINF(3,NS)
      STOP=.TRUE.
C**********************************************************************C
C     OUTPUT ON NFISTO                                                 C
C**********************************************************************C
      DO 811 I=1,NS
      WRITE(NFISTO,810) I,(HCPL(K,I),K=1,7),
     1        (HCPH(K,I),K=1,7),(HINF(K,I),K=1,3)
  811 CONTINUE
  810 FORMAT(' HL,HH(',I8,')',5(1PE11.4,1X),/,5(1PE11.4,1X),/,
     1      4(1PE11.4,1X),2X,3(1X,1PE9.2))
C**********************************************************************C
C     REGULAR EXIT                                                     C
C**********************************************************************C
      RETURN
C**********************************************************************C
C     END OF PRISPE                                                    C
C**********************************************************************C
      END
