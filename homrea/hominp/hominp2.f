      SUBROUTINE TABCON(NFILE2,NFILE6,IEQ,
     1      THM,THP,TPM,TPP,TCM,TCP,TXM,TXP,NH,NP,NC,NX)
C**********************************************************************C
C                                                                      C
C     *************************************************************    C
C     *                                                           *    C
C     *                                                           *    C
C     *    PROGRAM FOR THE IN/AND OUTPUT OF REACTION CONDITIONS   *    C
C     *                                                           *    C
C     *            LAST CHANGE: 15.11.1984/U.MAAS                 *    C
C     *                                                           *    C
C     *                                                           *    C
C     *************************************************************    C
C                                                                      C
C**********************************************************************C
C                                                                      C
C                                                                      C
C**********************************************************************C
C**********************************************************************C
C     CHAPTER I: STORAGE ORGANIZATION                                  C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL EQUSYM,STOP
      CHARACTER*4 ISYMB(4)
C*********************************************************************C
C     IV.2: INITIALIZATION                                            C
C*********************************************************************C
C---- NUMBERS
      THM = 0.0D0
      THP = 0.0D0
      TPM = 0.0D0
      TPP = 0.0D0
      TCM = 0.0D0
      TCP = 0.0D0
      TXM = 0.0D0
      TXP = 0.0D0
      NP=0
      NC=0
      NH=0
      NX=0
C**********************************************************************C
C     CHAPTER I:  FIND FIRST CONDITION HEADING                         C
C**********************************************************************C
   10 READ(NFILE2,11,END=9000) ISYMB(1)
   11 FORMAT(4A4)
      IF(.NOT.EQUSYM(4,ISYMB(1),'Tabu')) GO TO 10
C**********************************************************************C
C     IV.3: INPUT                                                      C
C**********************************************************************C
  300 FORMAT(2A4,1X,E10.2,14X,E10.2)
  301 CONTINUE
      READ(NFILE2,300,END=9999) ISYMB(1),ISYMB(2),XNM,YNM
      IF(EQUSYM(3,ISYMB(1),'END')) GO TO 900
      IF(ISYMB(1).EQ.'****') GO TO 301
C---- Density
      IF(ISYMB(1).EQ.'RHO ') THEN
        IF(IEQ.EQ.1) GOTO 9999
        NP  = 1
        TPM=XNM
        TPP=YNM
        GO TO 301
      ENDIF
C---- PRESSURE
      IF(ISYMB(1).EQ.'P   ') THEN
        IF(IEQ.EQ.2) GOTO 9999
        NP  = 1
        TPM=XNM * 1.D5
        TPP=YNM * 1.D5
        GO TO 301
      ENDIF
C---- Enthalpy
      IF(ISYMB(1).EQ.'H(T0') THEN
        NH = 1
        THM = XNM
        THP = YNM
        GO TO 301
      ENDIF
C---- mixture fraction
      IF(ISYMB(1).EQ.'CHI ') THEN
        NC = 1
        TCM = XNM
        TCP = YNM
        GO TO 301
      ENDIF
C---- second mixture fraction
      IF(ISYMB(1).EQ.'XI  ') THEN
        NX = 1
        TXM = XNM
        TXP = YNM
        GO TO 301
      ENDIF
C---- IF WE ARE HERE UNKNOWN SYMBOL IS DETECTED
      GOTO 9100
  900 CONTINUE
C**********************************************************************C
C
C     IV.: CHECKS                                                      C
C
C**********************************************************************C
C**********************************************************************C
C     Regular exit                                                     C
C**********************************************************************C
      RETURN
C**********************************************************************C
C     ERROR EXITS                                                      C
C**********************************************************************C
 9000 WRITE(NFILE6,9001)
 9001 FORMAT(1H ,' No tabulation coordinates specified for conserved',
     1      ' quantities')
      RETURN
 9100 WRITE(NFILE6,9101) ISYMB(1),ISYMB(2)
 9101 FORMAT(1H1,5X,'ERROR - Wrong symbol in reaction conditions : ',
     F       2A4,'...')
      GOTO 9999
 9999 CONTINUE
      WRITE(NFILE6,9998)
 9998 FORMAT(1H ,5X,'ERROR - Wrong operation of TABCON')
      STOP=.FALSE.
      STOP
C**********************************************************************C
C     END OF TABCON                                                    C
C**********************************************************************C
      END
      SUBROUTINE MANPAR(NCVMAX,NS,NSYMB,NEQ,VSYMB,NE,SYME,NCOMPE,
     1   INPF,NFILE3,NFILE6,
     2  ICS,IWORK,IZC,RWORK,NRV,MATRV,ELEMEN,
     1  CVMAT,ECV,MNLIM,XLIM,ELIM,STOP,TEST,TSO,IEQ)
C***********************************************************************
C                                                                      *
C     ************************************************************     *
C     *                                                          *     *
C     *      Input and Computation of a System of Linearly       *     *
C     *               Independent Reaction Vectors               *     *
C     *                                                          *     *
C     ************************************************************     *
C                                                                      *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL STOP,TEST,TSO,ELEMEN,EQUSYM
      PARAMETER (NZH=20)
      CHARACTER(LEN=*) NSYMB(NS)
      CHARACTER(LEN=*) VSYMB(NEQ)
      CHARACTER*2 SYME(NE)
      DIMENSION IWORK(*),RWORK(*),IZC(*),MATRV(NS,*)
      DIMENSION NCOMPE(NS,NE),CVMAT(NS,NCVMAX),ECV(2,NCVMAX),ICS(NS)
      CHARACTER*(NZH) CVSYMB(2),CLSYMB(2),CPSYMB(2),CUSYMB(2),
     1                CSSYMB(2),CGSYMB(2)
      CHARACTER*4 MM
      DIMENSION XLIM(NS,MNLIM),ELIM(2,MNLIM)
      PARAMETER (ZERO=0.0D0,ONE=1.0D0)
      DOUBLE PRECISION, DIMENSION(NS,MNLIM) :: UXLIM
      DOUBLE PRECISION, DIMENSION(2,MNLIM)  :: UELIM
      DOUBLE PRECISION, DIMENSION(NS+2,MNLIM) :: GXVE,GSVE
      DOUBLE PRECISION, DIMENSION(2,MNLIM)  :: GXVAL,GSVAL
C----
      DATA NGVH/2/,CGSYMB/'Gradient Vectors    ','Gradient Vectors    '/
      DATA NSVH/2/,CSSYMB/'Search Vectors      ','Search Vectors      '/
      DATA NCLH/2/,CLSYMB/'Limiting values     ','Limits              '/
      DATA NULH/2/,CUSYMB/'Update Limits       ','UpLimits            '/
      DATA NPLH/2/,CPSYMB/'Parametrization     ','Parametrization     '/
C***********************************************************************
C
C     Initialization                                                   C
C
C***********************************************************************
      TEST=.TRUE.
      STOP=.TRUE.
C***********************************************************************
C     set extremes of controlling vectors to zero
C***********************************************************************
C vorgabe von vsymb
C vsymb = nsymb + H(J/kg) + p(Pa)
      VSYMB(1) = 'H(J/KG) '
      VSYMB(2) = 'P(PA)   '
      VSYMB(3:NS+2) = NSYMB(1:NS)
C***********************************************************************
C     set extremes of controlling vectors to zero
C***********************************************************************
      DO 10 I = 1,NCVMAX
      ECV(1,I) = ZERO
      ECV(2,I) = ZERO
   10 CONTINUE
C***********************************************************************
C     Output of header                                                 C
C***********************************************************************
      WRITE(NFILE6,81)
   81 FORMAT(' ',79('-'),/,/,
     1 ' ',' Data for the Construction of a Low-Dimensional Manifold ')
C***********************************************************************
C
C     Store Element vectors as controlling vectors
C
C***********************************************************************
      NCON=0
      DO 110 J=1,NE
      DO 110 I=1,NS
      CVMAT(I,J+NCON) = FLOAT(NCOMPE(I,J))
  110 CONTINUE
      NCON=NCON+NE
C***********************************************************************
C
C     Input of parameterization first cell
C
C***********************************************************************
      CALL GENPVE(MNLIM,NPLH,CPSYMB,NZH,NS,NSYMB,INPF,NFILE6,NLI,
     1     CVMAT(1,NCON+1),ECV(1,NCON+1))
      NCON=NCON+NLI
C***********************************************************************
C
C     Input of limiting values
C
C***********************************************************************
      CALL GENPVE(MNLIM,NCLH,CLSYMB,NZH,NS,NSYMB,INPF,NFILE6,
     1    NLI,XLIM,ELIM)
C***********************************************************************
C
C     Input of limiting values
C
C***********************************************************************
      CALL GENPVE(MNLIM,NULH,CUSYMB,NZH,NS,NSYMB,INPF,NFILE6,
     1    NULI,UXLIM,UELIM)
C***********************************************************************
C
C     Input of Gradient vectors
C
C***********************************************************************
      CALL GENPVE(MNLIM,NGVH,CGSYMB,NZH,NS+2,VSYMB,INPF,NFILE6,
     1    NGV,GXVE,GXVAL)
C***********************************************************************
C
C     Input of search vectors 
C
C***********************************************************************
      CALL GENPVE(MNLIM,NSVH,CSSYMB,NZH,NS+2,VSYMB,INPF,NFILE6,
     1    NSV,GSVE,GSVAL)
C***********************************************************************
C
C     OUTPUT OF DATA ON NFILE3
C
C***********************************************************************
      WRITE(NFILE3,820)
      WRITE(NFILE3,824) ((FLOAT(NCOMPE(I,J)),I=1,NS),J=1,NE)
      WRITE(NFILE3,8828) NCON
      IF(NCON.GT.0) WRITE(NFILE3,824)((CVMAT(I,J),I=1,NS),J=1,NCON)
      IF(NCON.GT.0) WRITE(NFILE3,824)(ECV(1,J),ECV(2,J),J=1,NCON)
      WRITE(NFILE3,840) NLI
      IF(NLI.GT.0) WRITE(NFILE3,824)((XLIM(I,J),I=1,NS),J=1,NLI)
      IF(NLI.GT.0) WRITE(NFILE3,824)(ELIM(1,J),ELIM(2,J),J=1,NLI)
      WRITE(NFILE3,841) NULI,CUSYMB(1)
      IF(NULI.GT.0) WRITE(NFILE3,824)((UXLIM(I,J),I=1,NS),J=1,NULI)
      IF(NULI.GT.0) WRITE(NFILE3,824)(UELIM(1,J),UELIM(2,J),J=1,NULI)
      WRITE(NFILE3,841) NGV,CGSYMB(1)
      IF(NGV.GT.0) WRITE(NFILE3,824)((GXVE(I,J),I=1,NS+2),J=1,NGV)
      IF(NGV.GT.0) WRITE(NFILE3,824)(GXVAL(1,J),GXVAL(2,J),J=1,NGV)
      WRITE(NFILE3,841) NSV,CSSYMB(1)
      IF(NSV.GT.0) WRITE(NFILE3,824)((GSVE(I,J),I=1,NS+2),J=1,NSV)
      IF(NSV.GT.0) WRITE(NFILE3,824)(GSVAL(1,J),GSVAL(2,J),J=1,NSV)
 8828 FORMAT(I3,' controlling vectors')
  840 FORMAT(I3,' domain limits      ')
  841 FORMAT(I3,1X,A20)
  820 FORMAT('ELEMENT COMPOSITION OF THE SPECIES')
  821 FORMAT(I6,I6,I6,' ELEMENTS,REACT. VAR           ')
  822 FORMAT(8(A2,'-Atoms',2X))
  824 FORMAT(5(1PE16.9))
  825 FORMAT(8('Rvec.',I3,2X))
  830 FORMAT(20A5)
  831 FORMAT(I2)
C***********************************************************************
C     CHAPTER VI: Output on nfile3
C***********************************************************************
      STOP = .TRUE.
  700 CONTINUE
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
  900 CONTINUE
      WRITE(NFILE6,901)
  901 FORMAT('0',' Error in input occured in -ipar-              ')
      GOTO 999
  910 CONTINUE
      WRITE(NFILE6,911) SYME(J),I
  911 FORMAT(' ',' Element ',A2,' not conserved in reaction vector',I3)
      GOTO 999
  915 CONTINUE
  930 CONTINUE
      WRITE(NFILE6,931)
  931 FORMAT(' ',' Error: System of specified reaction',
     1           ' vectors is linearly dependent')
      GOTO 999
  940 CONTINUE
      WRITE(NFILE6,941)
  941 FORMAT('0',' end of file detected during input')
      GOTO 999
  950 CONTINUE
      WRITE(NFILE6,951)
  951 FORMAT(' ',' Error: System of elementary reactions does not',
     1           ' span the space of possible reaction vectors')
      GOTO 999
  999 CONTINUE
      STOP=.FALSE.
      WRITE(NFILE6,998)
  998 FORMAT(3(/,' +++++  ERROR IN MANPAR  +++++ '))
      RETURN
C***********************************************************************
C     END OF MANPAR
C***********************************************************************
      END
      SUBROUTINE TABPAR(NS,NSYMB,NE,SYME,NCOMPE,XE,XMOL,
     1     HCPL,HCPH,HINF,EMOL,MNF,TF,HF,PF,RHOF,PHIF,WF,XF,EWF,EXF,
     1     INPF,NFILE3,NFILE6,
     2     ELEMEN,STOP,TEST,TSO,IEQ)
C***********************************************************************
C                                                                      *
C     ************************************************************     *
C     *                                                          *     *
C     *      Input and Computation of a System of Linearly       *     *
C     *               Independent Reaction Vectors               *     *
C     *                                                          *     *
C     ************************************************************     *
C                                                                      *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL STOP,TEST,TSO,ELEMEN
      CHARACTER(LEN=*)  NSYMB(NS)
      CHARACTER*2 SYME(NE)
      DIMENSION XMOL(NS),EMOL(NE)
      DIMENSION XE(NS,NE)
      DIMENSION HCPH(7,NS),HCPL(7,NS),HINF(3,NS)
      DIMENSION NCOMPE(NS,NE)
      DIMENSION TF(MNF),HF(MNF),RHOF(MNF),PF(MNF),PHIF(MNF)
      DIMENSION WF(NS,MNF),XF(NS,MNF),EWF(NE,MNF),EXF(NE,MNF)
      data NFILE2/2/
C**********************************************************************C
C
C     Initialization                                                   C
C
C**********************************************************************C
      TEST=.TRUE.
      STOP=.TRUE.
C**********************************************************************C
C
C     Input of tabulation parameters for conserved variables
C
C**********************************************************************C
      REWIND INPF
      CALL TABCON(INPF,NFILE6,IEQ,THM,THP,TPM,TPP,TCM,TCP,TXM,TXP,
     1      IVH,IVP,IMIX,IMIY)
      MIXFRA = IMIX+IMIY
      IF(IVH+IVP+IMIX.GT.0) THEN
                    WRITE(NFILE6,831)
      IF(IVH .GT.0) WRITE(NFILE6,832) THM,THP
      IF(IVP .GT.0.AND.IEQ.EQ.1) WRITE(NFILE6,833) TPM,TPP
      IF(IVP .GT.0.AND.IEQ.EQ.2) WRITE(NFILE6,835) TPM,TPP
      IF(IMIX.GT.0) WRITE(NFILE6,834) TCM,TCP
      IF(IMIY.GT.0) WRITE(NFILE6,834) TXM,TXP
  831 FORMAT(' ','  Variation of conserved quatities in table setup:')
  832 FORMAT(' ','  specific enthalpy ',
     1     'from H0',1PE10.3,' J/kg to H0',1PE10.3,' J/kg')
  833 FORMAT(' ','  pressure          ',
     1     'from P0',1PE10.3,' Pa   to P0',1PE10.3,' Pa')
  834 FORMAT(' ','  mixture fraction  ',
     1     'from   ',1PE10.3,'      to   ',1PE10.3,'   ')
  835 FORMAT(' ','  density           ',
     1     'from r0',1PE10.3,'      to r0',1PE10.3,'   ')
      ENDIF
C**********************************************************************C
C
C     Input of fuel and oxidizer composition
C
C**********************************************************************C
C**********************************************************************C
C     Input of fuel composition
C**********************************************************************C
      REWIND INPF
      DO KKK = 1,MIXFRA+1
      CALL CHICON('FUEL',NS,NE,NSYMB,SYME,INPF,NFILE6,
     1  HCPL,HCPH,HINF,XMOL,EMOL,
     2  NCOMPE,TF(KKK),HF(KKK),PF(KKK),RHOF(KKK),PHIF(KKK),
     1  WF(1,KKK),XF(1,KKK),EWF(1,KKK),EXF(1,KKK),STOP)
      IF(.NOT.STOP) GO TO 10000
  661 FORMAT(1H ,18('*'),3X,'Properties of the fuel stream ',I4,
     1              3X,3X,18('*'))
      WRITE(NFILE6,661)  KKK
      CHIF = KKK - 1
      CALL PRICOM(NS,NE,NSYMB,SYME,NFILE6,
     1         CHIF,TF(KKK),HF(KKK),PF(KKK),RHOF(KKK),WF(1,KKK),
     1    XF(1,KKK),EWF(1,KKK),EXF(1,KKK))
      ENDDO
C**********************************************************************C
C     Output on nfile3
C**********************************************************************C
      STOP = .TRUE.
      IONE = 1
      IZER = 0
C-Proj
      WRITE(NFILE3,892) IONE
 892  FORMAT(20I3)
      WRITE(NFILE3,893) MIXFRA,IVH,IVP
      WRITE(NFILE3,824) TCM,TCP,TXM,TXP
      WRITE(NFILE3,824) THM,THP
      WRITE(NFILE3,824) TPM,TPP
 893  FORMAT(I3,' MIX ',I3,' H   ',I3,' P   ')
      DO 655 KKK=1,MIXFRA+1
         WRITE(NFILE3,824) HF(KKK),PF(KKK),RHOF(KKK),TF(KKK),
     1(WF(I,KKK),I=1,NS)
         WRITE(NFILE3,824) HF(KKK),PF(KKK),RHOF(KKK),TF(KKK),
     1(EWF(I,KKK),I=1,NE)
 655  CONTINUE
10000 CONTINUE
 824  FORMAT(5(1PE16.9))
C**********************************************************************C
C
C     Input of additional points ins state space
C
C**********************************************************************C
C**********************************************************************C
C     Input of Mixing point
C**********************************************************************C
      REWIND INPF
      KKK = MIXFRA + 2
      PHIF(KKK) = 0
      IPMIX = 1
      CALL CHICON('MIXP',NS,NE,NSYMB,SYME,INPF,NFILE6,
     1  HCPL,HCPH,HINF,XMOL,EMOL,
     2  NCOMPE,TF(KKK),HF(KKK),PF(KKK),RHOF(KKK),PHIF(KKK),
     1  WF(1,KKK),XF(1,KKK),EWF(1,KKK),EXF(1,KKK),STOP)
      IF(.NOT.STOP) IPMIX = 0
  662 FORMAT(18('*'),3X,'Properties of the Mixing point',3X,18('*'))
      WRITE(NFILE6,662)
      CHIF = KKK - 1
      IF(IPMIX.GT.0)
     1CALL PRICOM(NS,NE,NSYMB,SYME,NFILE6,
     1         CHIF,TF(KKK),HF(KKK),PF(KKK),RHOF(KKK),WF(1,KKK),
     1    XF(1,KKK),EWF(1,KKK),EXF(1,KKK))
      WRITE(NFILE3,823) IPMIX,PHIF(KKK)
 823  FORMAT(I3,' Mixing points      ',1PE10.3)
      IF(IPMIX.GT.0) THEN
      WRITE(NFILE3,824) HF(KKK),PF(KKK),RHOF(KKK),TF(KKK),
     1     (WF(I,KKK),I=1,NS)
      ENDIF
C**********************************************************************C
C     Input of Mixing point
C**********************************************************************C
      REWIND INPF
      KKK = MIXFRA + 3
      PHIF(KKK) = 0
      IPADD = 1
      CALL CHICON('CHAR',NS,NE,NSYMB,SYME,INPF,NFILE6,
     1  HCPL,HCPH,HINF,XMOL,EMOL,
     2  NCOMPE,TF(KKK),HF(KKK),PF(KKK),RHOF(KKK),PHIF(KKK),
     1  WF(1,KKK),XF(1,KKK),EWF(1,KKK),EXF(1,KKK),STOP)
      IF(.NOT.STOP) IPADD = 0
  663 FORMAT(18('*'),3X,'Properties of the additional point',3X,18('*'))
      WRITE(NFILE6,663)
      CHIF = KKK - 1
      IF(IPADD.GT.0)
     1CALL PRICOM(NS,NE,NSYMB,SYME,NFILE6,
     1         CHIF,TF(KKK),HF(KKK),PF(KKK),RHOF(KKK),WF(1,KKK),
     1    XF(1,KKK),EWF(1,KKK),EXF(1,KKK))
      WRITE(NFILE3,873) IPADD,PHIF(KKK)
 873  FORMAT(I3,' Additional points  ',1PE10.3)
      IF(IPADD.GT.0) THEN
      WRITE(NFILE3,824) HF(KKK),PF(KKK),RHOF(KKK),TF(KKK),
     1     (WF(I,KKK),I=1,NS)
      ENDIF
      STOP = .TRUE.
C**********************************************************************C
C     Input of fuel composition
C**********************************************************************C
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
  930 CONTINUE
      WRITE(NFILE6,931)
  931 FORMAT(' ',' Error: System of specified reaction',
     1           ' vectors is linearly dependent')
      GOTO 999
  950 CONTINUE
      WRITE(NFILE6,951)
  951 FORMAT(' ',' Error: System of elementary reactions does not',
     1           ' span the space of possible reaction vectors')
      GOTO 999
  999 CONTINUE
      STOP=.FALSE.
      WRITE(NFILE6,998)
  998 FORMAT(3(/,' +++++  ERROR IN TABPAR  +++++ '))
      RETURN
C***********************************************************************
C     END OF TABPAR
C***********************************************************************
      END
      SUBROUTINE GENPVE(MNLIM,NH,HEAD,NZH,
     1        NS,NSYMB,INPF,NFILE6,NC,XMAT,ECV)
C***********************************************************************
C
C    ***************************************************************
C    *                                                             *
C    *    PROGRAM FOR THE INPUT A OF CONTROLING VARIABLES          *
C    *                    AUTHOR: U. MAAS                          *
C    *                                                             *
C    ***************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C     NS             = NUMBER OF SPECIES
C     NSYMB(*)       = SPECIES SYMBOLS
C     INPF           = NUMBER OF INPUT FILE
C     NFILE6         = NUMBER OF OUTPUT FILE FOR LISTING
C
C     OUTPUT :
C
C     NC             = NUMBER OF CONTROLING VARIABLES
C     XMAT(NS,NC)    = MATRIX OF CONTROLING VECTORS
C
C     INPUT FROM FILE INPF :
C
C***********************************************************************
C
C
C-Character 8 characters allowed for species input
C
C***********************************************************************
C     STORAGE ORGANIZATION,  INITIAL VALUES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL EQUSYM
      PARAMETER (NIVA=3,LENINC=8)
      CHARACTER(LEN=*) NSYMB(NS)
      CHARACTER(LEN=LENINC) NSY(NIVA)
      CHARACTER*80 CH80
      CHARACTER*4 MM
      CHARACTER*8 HSY
      CHARACTER(NZH) HEAD(NH)
      DIMENSION FST(NIVA)
      DIMENSION XMAT(NS,*),ECV(2,*)
      PARAMETER (NCS=8,ZERO=0.0D0,ONE=1.0D0)
C***********************************************************************
C
C     Initialize number of given extremes
C
C***********************************************************************
      LENSYM=LEN(NSYMB(1))
      NFILL=LENSYM-LENINC
      REWIND INPF
      NC = 0
C***********************************************************************
C
C     Search for valid header
C
C***********************************************************************
  100 READ(INPF,800,END=700,ERR=900) HSY
      DO 110 I=1,NH
      IF(EQUSYM(6,HSY,HEAD(I))) GOTO 120
  110 CONTINUE
      GOTO 100
  120 CONTINUE
C***********************************************************************
C
C     Input of extreme values
C
C***********************************************************************
  200 CONTINUE
C***********************************************************************
C     Input of a line
C***********************************************************************
  210 CONTINUE
  820 FORMAT(A4,F7.1,1X,A,2(F7.3,1X,A),2(1X,E9.2))
      READ(INPF,820,ERR=900,END=940)
     1      MM,(FST(J),NSY(J),J=1,NIVA),ECV1,ECV2
C***********************************************************************
C     Check for end
C***********************************************************************
      IF(EQUSYM(3,MM,'END')) GOTO 600
      IF(.NOT.EQUSYM(4,MM,'    ')) GOTO 950
C***********************************************************************
C     Check for blank line
C***********************************************************************
      DO 220 J = 1,NIVA
      IF(.NOT.EQUSYM(8,NSY(J),'        ')) GOTO 230
  220 CONTINUE
      GOTO 210
  230 CONTINUE
C***********************************************************************
C     valid line found, increase NC
C***********************************************************************
      NC = NC + 1
      IF(NC.GT.MNLIM) GOTO 955
C***********************************************************************
C     set limits
C***********************************************************************
      ECV(1,NC) = ECV1
      ECV(2,NC) = ECV2
C***********************************************************************
C     set entries of matrix to zero
C***********************************************************************
      DO 240 I=1,NS
      XMAT(I,NC)=ZERO
  240 CONTINUE
C***********************************************************************
C     set all zero of FST entries to one
C***********************************************************************
c     DO 250 I=1,NIVA
c       IF(FST(I).EQ.ZERO) FST(I)=ONE
c 250 CONTINUE
C***********************************************************************
C     assign values to species numbers
C***********************************************************************
      DO 350 J=1,NIVA
        IF(EQUSYM(8,NSY(J),'        ')) THEN
        ELSE
          DO 310 I=1,NS
            IF(EQUSYM(LENSYM,NSY(J)//REPEAT(' ',NFILL),NSYMB(I))) THEN
              XMAT(I,NC) = XMAT(I,NC) + FST(J)
              GOTO 340
            ENDIF
  310     CONTINUE
C---- IF WE ARE HERE, SYMBOL IS NEITHER A BLNK, NOR A SPECIES SYMBOL
          GOTO 930
  340     CONTINUE
        ENDIF
  350 CONTINUE
C***********************************************************************
C     Output on NFILE6
C***********************************************************************
      IF(NC.EQ.1) WRITE(NFILE6,'(80A1)') (HEAD(1)(I:I),I=1,NZH)
      IFC = 1
      CH80( 1:80) = REPEAT(' ',80)
      CH80(IFC:IFC+4) = ' x ='
      IFC = IFC+4
      DO 440 I=1,NIVA
      IF(.NOT.EQUSYM(8,NSY(I),'        ')) THEN
      WRITE(CH80(IFC:IFC+18),'(SP,F7.3,'' * '',A)') FST(I),NSY(I)
      IFC = IFC+18
      ENDIF
  440 CONTINUE
      WRITE(CH80(59:80),'(A1,1PE8.2E1,A3,1PE8.2E1,A1)')
     1               '(',ECV1,'),(',ECV2,')'
      WRITE(NFILE6,'(A)') CH80
      GOTO 200
C***********************************************************************
C     REGULAR EXIT
C***********************************************************************
  600 CONTINUE
      RETURN
C***********************************************************************
C     REGULAR EXIT
C***********************************************************************
  700 CONTINUE
      WRITE(NFILE6,870)
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
  900 CONTINUE
      WRITE(NFILE6,901)
  901 FORMAT('0',' Error in input occured in -gencva-            ')
      GOTO 999
  930 CONTINUE
      WRITE(NFILE6,931) NSY(J)
  931 FORMAT('0',' Wrong symbol in list of controling variables: ',A)
      GOTO 999
  940 CONTINUE
      WRITE(NFILE6,941)
  941 FORMAT('0',' end of file detected during input')
      GOTO 999
  950 CONTINUE
      WRITE(NFILE6,951) MM
  951 FORMAT('0',' first characters (',A,') neither blank nor end')
      GOTO 999
  955 CONTINUE
      WRITE(NFILE6,956)
  956 FORMAT('0',' too many limits specified ')
      GOTO 999
  999 CONTINUE
      WRITE(NFILE6,998)
  998 FORMAT(3(/,' +++++ Error in GENPVE +++++'))
      STOP
C***********************************************************************
C     FORMAT STATEMENTS
C***********************************************************************
  800 FORMAT(A)
  810 FORMAT(I3)
  870 FORMAT(' No tabulation limits specified ')
C***********************************************************************
C     END OF -GENCVA-
C***********************************************************************
      END
      SUBROUTINE REDIMOPT(INPF,NFILE3,NFILE6,STOP)
C
C
C
      LOGICAL STOP,EQUSYM
      CHARACTER*13 FLAG,HEADER
      PARAMETER(NUMOPT=20,LENOPT=8)
      CHARACTER(LEN=LENOPT) DUMOPT,REDIMO(NUMOPT)
      DOUBLE PRECISION, DIMENSION(NUMOPT)        ::OPTVAL
      data redimo(1)/'zeta    '/,redimo(2)/'stmax   '/,
     1     redimo(3)/'deltat0 '/,redimo(4)/'freb    '/,
     1     redimo(5)/'highdif '/,redimo(6)/'lowdif  '/,
     1     redimo(7)/'changegr'/,redimo(8)/'correctm'/
     1     redimo(9)/'setzero '/,redimo(10)/'difftype'/,
     1     redimo(11)/'wallreac'/,redimo(12)/'infowr'/,
     1     redimo(13)/'$$$$$$$$'/,redimo(14)/'$$$$$$$$'/,
     1     redimo(15)/'$$$$$$$$'/,redimo(16)/'$$$$$$$$'/,
     1     redimo(17)/'$$$$$$$$'/,redimo(18)/'$$$$$$$$'/,
     1     redimo(19)/'$$$$$$$$'/,redimo(20)/'$$$$$$$$'/
      DATA HEADER/'REDIM options'/
C
C
      optval=0.0d0
C
C
      DO
        READ(INPF,802,END=940,ERR=900) FLAG
        IF(EQUSYM(13,FLAG,HEADER)) EXIT     
      ENDDO    
C
C
      DO       
      READ(INPF,800,END=940,ERR=900) DUMOPT,DUMMYVAL
      IF(EQUSYM(3,DUMOPT,'END')) EXIT      
      DO I=1,NUMOPT
        IF(EQUSYM(8,DUMOPT,REDIMO(I)))THEN
        OPTVAL(I)=DUMMYVAL
        EXIT
        END IF
      END DO
      ENDDO
C
C
C
C
      WRITE(NFILE3,801) NUMOPT,'REDIM options'
      WRITE(NFILE3,824)(optval(I),I=1,NUMOPT)
C
C
      STOP = .TRUE.
      return



  800 FORMAT(A8,1X,E10.2)
  801 FORMAT(I3,1X,A13)
  802 FORMAT(A13)
  824 FORMAT(5(1PE16.9))

  900 CONTINUE
      WRITE(NFILE6,901)
  901 FORMAT('0',' Error in input occured in -redimopt-')
      GOTO 999
  940 CONTINUE
      WRITE(NFILE6,941)
  941 FORMAT('0',' end of file detected during input in -redimopt-')
      GOTO 999
  999 CONTINUE
      WRITE(NFILE6,998)
  998 FORMAT(3(/,' +++++ Error in redimopt +++++'))
      STOP
      end subroutine redimopt
