C>    chilib
      SUBROUTINE CHICON(HEAD,NS,NE,NSYMB,SYME,NFILE2,NFILE6,
     1 HCPL,HCPH,HINF,XMOL,EMOL,
     2    NCOMPE,TI,HI,PI,RHOI,PHII,WI,XI,EWI,EXI,STOP)
C**********************************************************************C
C                                                                      C
C     *************************************************************    C
C     *                                                           *    C
C     *                                                           *    C
C     *    PROGRAM FOR THE IN/AND OUTPUT OF REACTION CONDITIONS   *    C
C     *                                                           *    C
C     *            LAST CHANGE: 15.11.1984/U.MAAS                 *    C
C     *                                                           *    C
C     *                                                           *    C
C     *************************************************************    C
C                                                                      C
C**********************************************************************C
C                                                                      C
C                                                                      C
C**********************************************************************C
C**********************************************************************C
C     CHAPTER I: STORAGE ORGANIZATION                                  C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL STOP,EQUSYM
      PARAMETER (ZERO=0.0D0)
      CHARACTER*2 SYME(NE)
      CHARACTER(LEN=*) NSYMB(NS),HEAD
      PARAMETER(LHMAX=50)
      CHARACTER(LEN=LHMAX) ISYMB                
      DIMENSION HCPL(7,NS),HCPH(7,NS),HINF(3,NS),XMOL(NS),EMOL(NE)
      DIMENSION XI(NS),WI(NS),EXI(NE),EWI(NE)
      DIMENSION NCOMPE(NS,NE)
      DATA RGAS/8.3143D0/
C**********************************************************************C
C     CHAPTER I:  FIND FIRST CONDITION HEADING                         C
C**********************************************************************C
      LHEAD=LEN(HEAD)
      NC=1
   10 READ(NFILE2,'(A)',END=9000) ISYMB
      IF(.NOT.EQUSYM(LHEAD,ISYMB,HEAD)) GOTO 10
      GOTO 1101
C*********************************************************************C
C     IV.2: INITIALIZATION                                            C
C*********************************************************************C
 1101 CONTINUE
C---- MOLE FRACTIONS
      XI(1:NS)=ZERO
      PHI = ZERO
C---- PHYSICAL CONDITIONS
C---- NUMBERS
      IM=0
      NP=0
      NT=0
      NH=0
      ND=0
      NX=0
C**********************************************************************C
C     IV.3: INPUT                                                      C
C**********************************************************************C
  300 FORMAT(A8,1X,E10.2,14X,E10.2)
  301 CONTINUE
      READ(NFILE2,300)  ISYMB,XNM,YNM
      IF(EQUSYM(3,ISYMB,'END')) GO TO 900
      IF(EQUSYM(4,ISYMB,'****')) GO TO 301
C----  DENSITY
      IF(ISYMB.EQ.'RHO     ') THEN
        ND  = 1
        RHOI=XNM
        GO TO 301
      ENDIF
C---- PRESSURE
      IF(ISYMB.EQ.'P       ') THEN
        NP  = 1
        PI=XNM * 1.D5
        GO TO 301
      ENDIF
C---- TEMPERATURE
      IF(ISYMB.EQ.'T       ') THEN
        NT=1
        TI = XNM
        GO TO 301
      ENDIF
      IF(ISYMB.EQ.'H(T0)   ') THEN
        NH = 1
        HI = XNM
        GO TO 301
      ENDIF
      IF(ISYMB.EQ.'PHI '    ) THEN
        NX = 1
        PHII = XNM
        GO TO 301
      ENDIF
C---- GAS COMPOSITION
      DO 320 J=1,NS
      IF((ISYMB.EQ.NSYMB(J))) THEN
        IF(XI(J).GT.ZERO) THEN
          ISE = J 
          GOTO 9222
        ENDIF
        XI(J)=XNM
        IM=IM+1
        GO TO 301
      ENDIF
  320 CONTINUE
  330 CONTINUE
C---- IF WE ARE HERE UNKNOWN SYMBOL IS DETECTED
      GOTO 9100
  900 CONTINUE
C**********************************************************************C
C
C     IV.: CHECKS                                                      C
C
C**********************************************************************C
C**********************************************************************C
C     IV.1 : CHECK FOR COMPLETENESS OF DATA                            C
C**********************************************************************C
  400 CONTINUE
C-----INITIAL P,V
      IF(NP.LE.0) GOTO 9220
      IF((NT.LE.0).AND.(NH.LE.0))GOTO 9224
      IF((NT.GT.0).AND.(NH.GT.0))GOTO 9226
      IF((ND.GT.0).AND.(NP.GT.0))GOTO 9232
      IF(IM.GT.NS) GOTO 9230
      IF(IM.LE.0)  GOTO 9240
C**********************************************************************C
C     IV.5: NORMALIZATION OF INITIAL MIXTURE                           C
C**********************************************************************C
      SUM=0.0
      DO 510 I=1,NS
      SUM=SUM+XI(I)
  510 CONTINUE
      DO 520 I=1,NS
      XI(I)=XI(I)/SUM
  520 CONTINUE
      XMOLM = 0.0D0
      DO 521 I=1,NS
      XMOLM = XMOLM + XI(I) * XMOL(I)
  521 CONTINUE
      DO 522 I=1,NS
      WI(I)=XI(I)/XMOLM
  522 CONTINUE
C**********************************************************************C
C     IV.5: NORMALIZATION OF ATOM MOLE FRACTIONS                       C
C**********************************************************************C
      SUM=0.D0
      DO 570 J=1,NE
      EXI(J)=0.D0
      DO 560 I=1,NS
      EXI(J)=EXI(J)+FLOAT(NCOMPE(I,J))*XI(I)
  560 CONTINUE
      SUM=SUM+EXI(J)
  570 CONTINUE
      DO 580 J=1,NE
      EXI(J)=EXI(J)/SUM
  580 CONTINUE
      XMOLE=0.0D0
      DO 551 J=1,NE
      XMOLE=XMOLE+EXI(J)*EMOL(J)
  551 CONTINUE
      DO 552 J=1,NE
      EWI(J)=EXI(J)/XMOLE
  552 CONTINUE
C**********************************************************************C
C     IV.6: CALCULATE INITIAL TEMPERATURE FOR SPECIFIC ENTHALPY GIVEN  C
C**********************************************************************C
      XMOLM=0.0D0
      DO 583 I=1,NS
      XMOLM = XI(I) * XMOL(I) + XMOLM
  583 CONTINUE
      IF(NH.GT.0) THEN
        CALL GETT(NS,HI,XI,HCPL,HCPH,HINF,XMOL,TI,NFILE6)
        NT=1
      ELSE
      HI  = 0.D0
      DO 6110 I=1,NS
      CALL CALHI(I,TI,HCPL,HCPH,HINF,HH,NFILE6)
      HI  = HI  + XI(I)*HH
 6110 CONTINUE
      HI    = HI / XMOLM
      ENDIF
      IF(NP.EQ.1) THEN
        RHOI = PI * XMOLM / (RGAS*TI)
      ENDIF
      IF(ND.EQ.1) THEN
        PI = RHOI * RGAS * TI / XMOLM
      ENDIF
  999 CONTINUE
 1000 RETURN
C**********************************************************************C
C     ERROR EXITS                                                      C
C**********************************************************************C
 9000 WRITE(NFILE6,9001)
 9001 FORMAT(1H ,5X,'WARNING - No composition specified')
      STOP = .FALSE.
      RETURN
 9100 WRITE(NFILE6,9101) ISYMB
 9101 FORMAT(1H1,5X,'ERROR - Wrong symbol in reaction conditions : ',
     F       A,'...')
      GOTO 9999
 9210 WRITE(NFILE6,9211)
 9211 FORMAT(1H1,5X,'ERROR - Data on reaction conditions incomplete')
      GOTO 9999
 9220 WRITE(NFILE6,9221)
 9221 FORMAT(1H1,5X,'ERROR - Initial P or V not specified')
      GOTO 9999
 9222 WRITE(NFILE6,9223) NSYMB(ISE)
 9223 FORMAT(1H1,5X,'ERROR - Species ',A,' multiply defined')
      GOTO 9999
 9213 WRITE(NFILE6,9214)
 9214 FORMAT('1 ','ERROR - Inconsistency in specified atom composition')
      GOTO 9999
 9224 WRITE(NFILE6,9225)
 9225 FORMAT(1H1,5X,'ERROR - Neither initial T nor initial enthalpy ',/,
     1       '1',5X,'        specified for adiabatic reactor        ')
      GOTO 9999
 9226 WRITE(NFILE6,9227)
 9227 FORMAT(1H1,5X,'ERROR - Initial T and initial enthalpy ',/,
     1       '1',5X,'        specified for adiabatic reactor        ')
      GOTO 9999
 9232 WRITE(NFILE6,9233)
 9233 FORMAT(1H1,5X,'ERROR - Initial density and pressure   ',/,
     1       '1',5X,'        specified at the same time             ')
      GOTO 9999
C9228 WRITE(NFILE6,9229)
C9229 FORMAT(1H1,5X,'ERROR - Initial ENTHALPY only valid together ',/,
C    1       '1',5X,'        with option ELEM                       ')
      GOTO 9999
 9230 WRITE(NFILE6,9231) NS
 9231 FORMAT(1H1,5X,'ERROR - More than ',I2,' initial values defined')
      GOTO 9999
 9240 WRITE(NFILE6,9241)
 9241 FORMAT(1H ,5X,'ERROR - No initial composition specified')
      GOTO 9999
C9250 WRITE(NFILE6,9251)
C9251 FORMAT(1H ,5X,'ERROR - Values of volume out of order')
C     GOTO 9999
C9260 WRITE(NFILE6,9261)
C9261 FORMAT(1H ,5X,'ERROR - Values of temperature out of order')
C     GOTO 9999
 9999 CONTINUE
      WRITE(NFILE6,9998)
 9998 FORMAT(1H ,5X,'ERROR - Wrong operation of CHICON')
      STOP=.FALSE.
      STOP
      RETURN
C**********************************************************************C
C     END OF CHICON                                                    C
C**********************************************************************C
      END
      SUBROUTINE PRICOM(NS,NE,NSYMB,SYME,NFILE6,
     1                     CHI,TI,HI,PI,RHOI,WI,XI,EWI,EXI)
C**********************************************************************C
C                                                                      C
C     *************************************************************    C
C     *                                                           *    C
C     *                                                           *    C
C     *    PROGRAM FOR THE OUTPUT OF CONDITIONS                   *    C
C     *                                                           *    C
C     *                                                           *    C
C     *************************************************************    C
C                                                                      C
C**********************************************************************C
C                                                                      C
C                                                                      C
C**********************************************************************C
C**********************************************************************C
C     CHAPTER I: STORAGE ORGANIZATION                                  C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      CHARACTER*2 SYME(NE)
      CHARACTER(LEN=*)  NSYMB(NS)
      CHARACTER(LEN=31) MSYMB(4)
      DIMENSION XI(NS),WI(NS),EXI(NE),EWI(NE),XIPR(4)
C**********************************************************************C
C
C     OUTPUT ON FILE NFILE6
C
C**********************************************************************C
      LENSY=LEN(NSYMB(1))
      WRITE(NFILE6,802) CHI
C**********************************************************************C
C     OUTPUT of values in mole fractions
C**********************************************************************C
C---- Extract non-zero mole fraction for print-out
      WRITE(NFILE6,810)
      I = 0
   10 CONTINUE     
      IPR=0
   20 CONTINUE     
      I = I + 1
      IF(I.GT.NS) GOTO 30
      IF(XI(I).GT.0.0D0) THEN
        IPR=IPR+1
        MSYMB(IPR)(1:LENSY) = NSYMB(I)
        XIPR(IPR)  = XI   (I)
      ENDIF    
      IF(IPR.LT.3) GOTO 20
   30 CONTINUE
      WRITE(NFILE6,812) (MSYMB(J)(1:LENSY),XIPR(J),J=1,IPR)
      IF(I.LT.NS) GOTO 10
      WRITE(NFILE6,815)
      WRITE(NFILE6,817)  (SYME(J),EXI(J),J=1,NE)
C**********************************************************************C
C     OUTPUT of values in mass fractions
C**********************************************************************C
C---- Extract non-zero mole fraction for print-out
      WRITE(NFILE6,811)
      I = 0
   60 CONTINUE     
      IPR=0
   70 CONTINUE     
      I = I + 1
      IF(I.GT.NS) GOTO 80
      IF(WI(I).GT.0.0D0) THEN
        IPR=IPR+1
        MSYMB(IPR)(1:LENSY)=NSYMB(I)
        XIPR(IPR)   =WI(I)
      ENDIF    
      IF(IPR.LT.3) GOTO 70
   80 CONTINUE
      WRITE(NFILE6,812)  (MSYMB(J)(1:LENSY),XIPR(J),J=1,IPR)
      IF(I.LT.NS) GOTO 60
      WRITE(NFILE6,816)
      WRITE(NFILE6,817)  (SYME(J),EWI(J),J=1,NE)
C**********************************************************************C
C     OUTPUT of physical properties
C**********************************************************************C
      WRITE(NFILE6,820) PI
      WRITE(NFILE6,821) TI
      WRITE(NFILE6,822) HI
      WRITE(NFILE6,823) RHOI
      RETURN
C**********************************************************************C
C     FORMAT STATEMENTS                                                C
C**********************************************************************C
  802 FORMAT(/,' ',7X,'Values for mixture fraction =',F5.3)
  810 FORMAT(/,' ',7X,'Gas Composition (mole fractions)')
  811 FORMAT(/,' ',7X,'Gas Composition (specific mole numbers)')
  812 FORMAT(1(' ',7X,3(A,':',1PE10.3,2X)))
  815 FORMAT(/,' ',7X,'Element Composition (mole fractions)')
  816 FORMAT(/,' ',7X,'Element Composition (specific mole numbers)')
  817 FORMAT(1(1H ,7X,4(A2,':',1PE14.7,1X)))
  820 FORMAT(/,' ',7X,'P : ',1PE14.7,' P(Pa)')
  821 FORMAT(  ' ',7X,'T : ',1PE14.7,' K ')
  822 FORMAT(  ' ',7X,'H : ',1PE14.7,' J/kg ')
  823 FORMAT(  ' ',7X,'r : ',1PE14.7,' kg/m**3')
C**********************************************************************C
C     END OF CHICON                                                    C
C**********************************************************************C
      END
      SUBROUTINE ZHICOM(NS,NE,XMOL,EMOL,HCPL,HCPH,HINF,NRV,NMAT,NCOMPE,
     2    HF,PF,RHOF,WF,XF,EWF,EXF,HO,PO,RHOO,WO,XO,EWO,EXO,
     2   CHI,H ,P ,RHO,W ,T ,X ,EW ,EX,
     2       HG,PG,RHOG,WG,TG,XG,EWG,EXG)
C**********************************************************************C
C                                                                      C
C     *************************************************************    C
C     *                                                           *    C
C     *                                                           *    C
C     *    PROGRAM FOR THE CALCULATION OF COMPOSITION             *    C
C     *                                                           *    C
C     *                                                           *    C
C     *                                                           *    C
C     *************************************************************    C
C                                                                      C
C**********************************************************************C
C                                                                      C
C                                                                      C
C**********************************************************************C
C**********************************************************************C
C     CHAPTER I: STORAGE ORGANIZATION                                  C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION HCPL(7,NS),HCPH(7,NS),HINF(3,NS),XMOL(NS),EMOL(NE)
C-DIMENSION
      DIMENSION NMAT(NS,*),NCOMPE(NS,NE)
      DIMENSION WF(NS),XF(NS),EWF(NE),EXF(NE)
      DIMENSION WO(NS),XO(NS),EWO(NE),EXO(NE)
      DIMENSION W(NS), X(NS), EW(NE), EX(NE)
      DIMENSION WG(NS),XG(NS),EWG(NE),EXG(NE)
      DATA ONE/1.0D0/,ZERO/0.0D0/
C**********************************************************************C
C
C     compute actual values of w,x,ew,ex
C
C**********************************************************************C
C**********************************************************************C
C     compute actual values of w
C**********************************************************************C
      DO 10 I=1,NS
      W(I) = WO(I) + CHI * (WF(I) - WO(I))
   10 CONTINUE
C**********************************************************************C
C     compute actual values of ew
C**********************************************************************C
      DO 40 I=1,NE
      EW(I) = EWO(I) + CHI * (EWF(I) - EWO(I))
   40 CONTINUE
C**********************************************************************C
C     compute actual value of h
C**********************************************************************C
      H    = HO + CHI * (HF - HO)
      P    = PO + CHI * (PF - PO)
      RHO  = RHOO + CHI * (RHOF - RHOO)
C**********************************************************************C
C     transform w -> x
C**********************************************************************C
      XMOLM = ZERO
      DO 20 I=1,NS
      XMOLM = XMOLM + W(I)
   20 CONTINUE
      XMOLM = ONE / XMOLM
      DO 30 I=1,NS
      X(I) = W(I) * XMOLM
   30 CONTINUE
C**********************************************************************C
C     transform ew -> ex
C**********************************************************************C
      XMOLE = 0.0D0
      DO 50 I=1,NE
      XMOLE = XMOLE + EW(I)
   50 CONTINUE
      XMOLE = 1.0D0 / XMOLE
      DO 60 I=1,NE
      EX(I) = EW(I) * XMOLE
   60 CONTINUE
C**********************************************************************C
C     compute T
C**********************************************************************C
      CALL GETT(NS,H,X,HCPL,HCPH,HINF,XMOL,T,NFILE6)
C**********************************************************************C
C
C    try to obtain an estimate for equilibrium
C
C**********************************************************************C
      HG = H
      PG = P
      RHOG = RHO
      IF(NRV.LE.0) THEN
C**********************************************************************C
C     no progress to equilibrium allowed
C**********************************************************************C
      DO 110 I=1,NS
      WG(I) = W(I)
      XG(I) = X(I)
  110 CONTINUE
      DO 120 I=1,NE
      EWG(I) = EW(I)
      EXG(I) = EX(I)
  120 CONTINUE
      TG = T
C**********************************************************************C
C     progress to equilibrium allowed according to specified reactions
C**********************************************************************C
      ELSE
      DO 205 I=1,NS
      WG(I) = W(I)
  205 CONTINUE
      DO 290 J=1,NRV
C**********************************************************************
C     compute maximum possible step to equilibrium
C**********************************************************************
      EMPFAC = 1.0
      DELMAX = 1.D30
      DO 210 I=1,NS
      NM  = NMAT(I,J)
      XNM = FLOAT(NM)
      IF(NM.LT.0) DELMAX = DMIN1(DELMAX,-WG(I)/XNM)
      IF(NM.GT.0) DELMAX = DMIN1(DELMAX,(1.0D0/XMOL(I)-WG(I))/XNM)
  210 CONTINUE
      DELMAX = DELMAX * EMPFAC
C     WRITE(6,*) ' DELMAX ' ,DELMAX
C**********************************************************************
C     update wg
C**********************************************************************
      DO 220 I=1,NS
      WG(I) = WG(I) + DELMAX * FLOAT(NMAT(I,J))
  220 CONTINUE
  290 CONTINUE
C**********************************************************************
C     end of loop over reactions
C**********************************************************************
      ENDIF
C**********************************************************************
C     transform w -> x
C**********************************************************************
      XMOLM = 0.0D0
      DO 230 I=1,NS
      XMOLM = XMOLM + WG(I)
  230 CONTINUE
      XMOLM = 1.0D0 / XMOLM
      DO 240 I=1,NS
      XG(I) = WG(I) * XMOLM
  240 CONTINUE
C**********************************************************************
C     compute T
C**********************************************************************
      CALL GETT(NS,HG,XG,HCPL,HCPH,HINF,XMOL,TG,NFILE6)
C**********************************************************************
C     compute ewg
C**********************************************************************
      DO 250 J=1,NE
      EWG(J) = 0.0D0
      DO 250 I=1,NS
      EWG(J) = EWG(J) + WG(I) * FLOAT(NCOMPE(I,J))
  250 CONTINUE
C**********************************************************************
C     compute exg
C**********************************************************************
      XMOLE = 0.0D0
      DO 260 I=1,NE
      XMOLE = XMOLE + EWG(I)
  260 CONTINUE
      XMOLE = 1.0D0 / XMOLE
      DO 270 I=1,NE
      EXG(I) = EWG(I) * XMOLE
  270 CONTINUE
      RETURN
C**********************************************************************C
C     END OF
C**********************************************************************C
      END
