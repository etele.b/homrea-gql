# Readme for analysis part of homrea

## Global Analysis
    

## Local Analysis (Output)

## Reaction relevance analysis ()

## Configuration file

1. General
    * G_detfile = The file from which the default trajectory is read
2. Analysis
    Block for global analysis
    * A_pointgen = Controls the random point generation for the GQL bases.
    Possible options:
        * -1 - No global analysis or (reduced) integration
        * 0 - No global analysis
        * 1 - Points are generated within the boundaries of the trajectory in state space
        * 21/22 - Points are generated within the boundaries of the trajectory in state space, between initial point and ignition delay/halftime point of defined species
        * 3 - Points from the trajectory
        * 4 - Points from within the convex shell of trajectory
        * 51/52 - Points from convex shell between initial and ignition/halftime points
    * A_nr - Number of reference points which will be generated
    * A_ns - Range of slow dimensions. If upper boundary is 0, it will be substituted by the highest possible number of slow dimnensions (NEQ - NCON)
    * A_staban - If >0, do stability cheks
    * A_staban_tol - If any eigenvalues are bigger than the tolerance, it will be skipped
    * A_tol - Tolerance in
    * A_tdel = Weight of the ignition time delay difference in the rating of a trajectory
    * A_thalf = Weight of the halftime point difference in rating
    * A_intdif = Weight of the difference in the integral of one speicies over the other (in rating)
    * A_rref = Not used...
    * A_nrad = Species for ignition time delay
    * A_ncor = Species for correction (of random points)
    * A_nhalf = Specied for half time calculation
    * A_nint = The integral of species 1 over species 2 is calcualted
3. Integration
    * I_tend = Integration time
    * I_ns = dimension of the slow subspace of reduced integration. If = -1, the number of slow dimensions can be entered
    * I_nvert_max = Maximum number of vertices for integration
    * I_cheks = Do cheks of point after fast integration
4. LA Output
    * Not
    * Implemented
    * Yet
5. Output
    * IO_format = Trajectory file format
    * IO_intouti= Unit for integration message output
    * IO_errf = Unit for error output
    * IO_statf = File for statistics
    * IO_table = Table for output control:
    
|                   | All   | Finished int. | Best  | Inttegration  | tec  | traj  | bin  | ascii | 
| ----------------- | ----- | ------------- | ----- | ------------- | ---- | ----- | ---- | ----- |
| Reference point   | 1     | 1             | 0     | 0             | 1    | 1     | 0    | 1     |
| Jacobian          | 1     | 1             | 1     | 0             | 0    | 0     | 1    | 1     |
| GQL basis         | 1     | 1             | 1     | 0             | 0    | 0     | 1    | 1     |
| Reduced trajectory| 0     | 1             | 1     | 1             | 1    | 1     | 0    | 1     |
