#/bin/bash
ulimit -s unlimited
ITTDIR='../../'

cp Inp-Phi-6_P-10_T-650 fort.2
cp PolyMech_Homrea_221117.txt fort.4
cp Polygeneration_V1_Thermo.txt fort.9

$ITTDIR/homrea/bin-gfortran/hominp
rm fort.4
rm fort.2
rm fort.9
rm fort.10
rm fort.11

$ITTDIR/homrea/bin-gfortran/homrun
