C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *        LIBRARY FOR THE EVALUATION OF PHYSICO-CHEMICAL     *    *
C     *      PROPERTIES (TO BE USED IN CONJUNCTION WITH INSINP)   *    *
C     *                                                           *    *
C     *                    10/20/1989 U.MAAS                      *    *
C     *                                                           *    *
C     *              LAST CHANGE: 14/03/2002/MAIWALD              *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C     LOOK FOR C-DIMENSION TO CHANGE (1) IN (*)                        *
C                                                                      *
C***********************************************************************
C***********************************************************************
C                                                                      *
C     CHANGE REPORT:                                                   *
C                                                                      *
C        LATEST CHANGE: NEW EVALUATION METHOD FOR THERMODYNAMIC        *
C                       PROPERTIES (SEE -CALHCP-)_                     *
C                                                                      *
C***********************************************************************
      SUBROUTINE INPSPE(NS,NE,NFISTO,MSGFIL,IPRI,IERR,SYMB,XMOL,
     1   SYMBE,EMOL,XE,
     1 LRSPEC,RSPEC,NEPS,NSIG,NHL,NHSW,
     1 NETA,NLCOEF,NLCOEM,NLCOED,NDCOEF)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *        INPUT OF THE SPECIES DATA  FROM UNIT NFISTO        *    *
C     *                      25.04.1983                           *    *
C     *              LAST CHANGE: 25.11.1985/MAAS                 *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C***********************************************************************
C     ALL DATA IN SI UNITS (IF NOT SPECIFIED)                          *
C***********************************************************************
C                                                                      *
C     THIS SUBROUTINE READS SPECIES DATA AND BUILDS                    *
C     UP ALL INFORMATION FOR THE EVALUATION OF TRANSPORT COEFFICIENTS  *
C     FURTHERMORE SYMBOLS ARE ASSIGNED TO THE SPECIES                  *
C     SYMB(1  .... NS)                                                 *
C                                                                      *
C    INPUT PARAMETERS:                                                 *
C      NS           = NUMBER OF SPECIES                                *
C      LRSPEC       = DIMENSION OF REAL ARRAY                          *
C      NFISTO       = NUMBER OF INPUT FILE                             *
C      MSGFIL       = NUMBER OF FILE FOR LISTING AND ERROR MESSAGES    *
C      IPRI         > 1 FOR DETAILED OUTPUT OF TEST                    *
C    * IERR         = 0 all species data are expected and read         *
C                   > 0 only mass data are read                        *
C                                                                      *
C    OTHER SUBROUTINES NEEDED: NONE                                    *
C                                                                      *
C     DESCRIPTION OF PARAMETERS:                                       *
C                                                                      *
C      IERR         = RETURN CODE (IERR > 0 FOR ABNORMAL END)          *
C      SYMB(NS)     = SPECIES SYMBOLS (CHARACTER*(*))                  *
C      XMOL(NS)     = MOLAR MASSES (KG/M**3)                           *
C      RSPEC       =  REAL WORK ARRAY CONTAINING L-J PARAMETER, FITS   *
C                     FOR ETA,D,DTH,LAMDA ETC.                         *
C                                                                      *
C      DESCRIPTION OF ENTRIES:                                         *
C                                                                      *
C  A) REAL ARRAY                                                       *
C                                                                      *
C      EPS(NS)                L-J- PARAMETERS, SEE BELOW               *
C                                ---> STARTING AT RSPEC(NEPS)          *
C      SIG(NS)                                                         *
C                                ---> STARTING AT RSPEC(NSIG)          *
C      HL(7,NS)                                                        *
C                                ---> STARTING AT RSPEC(NHL)           *
C      HH(7,NS)                                                        *
C                                ---> STARTING AT RSPEC(NHH)           *
C      HSW(3,NS)                                                       *
C                                ---> STARTING AT RSPEC(NHSW)          *
C      ETA(4,NS)                                                       *
C                                ---> STARTING AT RSPEC(NETA)          *
C      XLCOEF(4,NS)                                                    *
C                                ---> STARTING AT RSPEC(NLCOEF)        *
C      XLCOEM(4,NS)                                                    *
C                                ---> STARTING AT RSPEC(NLCOEM)        *
C      XLCOED(3,NS)                                                    *
C                                ---> STARTING AT RSPEC(NLCOED)        *
C      DCOEF(4,NS,NS)                                                  *
C                                ---> STARTING AT RSPEC(NDCOEF)        *
C                                                                      *
C                                                                      *
C***********************************************************************
C     STORAGE ORGANIZATION, INITIAL VALUES                             *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION   RSPEC(LRSPEC)
      DIMENSION   XMOL(NS),EMOL(NE),XE(NS,NE)
      CHARACTER(LEN=*) SYMB(NS)
      CHARACTER(LEN=*) SYMBE(NE)
      LOGICAL TRANSD
C***********************************************************************
C     DECIDE ABOUT TESTING                                             *
C***********************************************************************
      LOGICAL TEST
      TEST=.FALSE.
      IF(IPRI.GE.2) TEST=.TRUE.
      TRANSD= .TRUE.
      IF(IERR.GE.1) TRANSD = .FALSE.
                 IMTRA = 0
      IF(TRANSD) IMTRA = 1
      IERR = 0
C***********************************************************************
C     SET UP POINTERS FOR SPECIES DATA                                 *
C***********************************************************************
C---- multiplication with IMTRA ensures that no storage is needed for
C     TRANSD = .FALSE.
      NEPS   = 1
      NSIG   = NEPS   +   NS    * IMTRA
      NHL    = NSIG   +   NS    * IMTRA
      NHH    = NHL    + 7*NS    * IMTRA
      NHSW   = NHH    + 7*NS    * IMTRA
      NETA   = NHSW   + 3*NS    * IMTRA
      NLCOEF = NETA   + 4*NS    * IMTRA
      NLCOEM = NLCOEF + 4*NS    * IMTRA
      NLCOED = NLCOEM + 4*NS    * IMTRA
      NDCOEF = NLCOED + 3*NS    * IMTRA
      NNEXT  = NDCOEF + 4*NS*NS * IMTRA
      IF(NNEXT-1.GT.LRSPEC) GOTO 999
C***********************************************************************
C     INPUT OF SPECIES SYMBOLS                                         *
C***********************************************************************
      READ(NFISTO,805)  (SYMB(I),I=1,NS)
      IF(TEST) WRITE(MSGFIL,812)  (SYMB(I),I=1,NS)
C***********************************************************************
C     INPUT OF MOLAR MASSES                                            *
C***********************************************************************
C---- XMOL(KG/MOL), EPS/K(K), SIG(A)
      READ(NFISTO,809)  (XMOL(I),I=1,NS)
      IF(TEST) WRITE(MSGFIL,811)  (XMOL(I),I=1,NS)
C***********************************************************************
C     INPUT OF Element SYMBOLS                                         *
C***********************************************************************
      READ(NFISTO,805)  (SYMBE(I)(1:8),I=1,NE)
      IF(TEST) WRITE(MSGFIL,812)  (SYMBE(I),I=1,NE)
C***********************************************************************
C     INPUT OF ELEMENT MASSES
C***********************************************************************
C---- EMOL(KG/MOL)
      READ(NFISTO,809)  (EMOL(I),I=1,NE)
      IF(TEST) WRITE(MSGFIL,811)  (EMOL(I),I=1,NE)
C***********************************************************************
C     INPUT OF ELEMENT Composition
C***********************************************************************
C---- EMOL(KG/MOL)
      READ(NFISTO,809)  ((XE(I,J),J=1,NE),I=1,NS)
      IF(TEST) WRITE(MSGFIL,811) ((XE(I,J),J=1,NE),I=1,NS)
C***********************************************************************
C     INPUT OF COEFFICIENTS FOR HEAT CONDUCTION                        *
C***********************************************************************
      IF(TRANSD) THEN
C---- XLCOEF(4,1)    = COEFF. OF LOG(LAMBDA) (J/S*K*M)
      DO 210 I=1,NS
      READ(NFISTO,802)           (RSPEC(NLCOEF-1+K+(I-1)*4),K=1,4)
      IF(TEST) WRITE(MSGFIL,811) (RSPEC(NLCOEF-1+K+(I-1)*4),K=1,4)
  210 CONTINUE
C---- XLCOEM(4,1) = COEFF. OF LOG(LAMBDA,MON) (J/S*K*M)
      DO 220 I=1,NS
      READ(NFISTO,802)           (RSPEC(NLCOEM-1+K+(I-1)*4),K=1,4)
      IF(TEST) WRITE(MSGFIL,811) (RSPEC(NLCOEM-1+K+(I-1)*4),K=1,4)
  220 CONTINUE
C***********************************************************************
C     INPUT OF COEFFICIENTS FOR VISCOSITY                              *
C***********************************************************************
C---- ETA(4,1) = COEFF. OF LOG(ETA) (KG/M*S)
      DO 230 I=1,NS
      READ(NFISTO,802)           (RSPEC(NETA-1+K+(I-1)*4),K=1,4)
      IF(TEST) WRITE(MSGFIL,811) (RSPEC(NETA-1+K+(I-1)*4),K=1,4)
  230 CONTINUE
C***********************************************************************
C     INPUT OF BINARY DIFFUSION COEFFICIENTS                           *
C***********************************************************************
C---- DCOEF = COEFF. OF LOG(D) (IN M**2/S) AT P=1 PA
      DO I=1,NS
      DO J=1,I
      READ(NFISTO,802)  (RSPEC(NDCOEF-1+K+(I-1)*4+(J-1)*4*NS),K=1,4)
      DO 240 K=1,4
      RSPEC(NDCOEF-1+K+(J-1)*4+(I-1)*4*NS)=
     1                   RSPEC(NDCOEF-1+K+(I-1)*4+(J-1)*4*NS)
  240 CONTINUE
      IF(TEST) WRITE(MSGFIL,811)
     1                  (RSPEC(NDCOEF-1+K+(I-1)*4+(J-1)*4*NS),K=1,4)
      ENDDO
      ENDDO
C***********************************************************************
C     INPUT OF LJ PARAMETERS                                           *
C***********************************************************************
C---- EPS/K(K)
      READ(NFISTO,802)            (RSPEC(NEPS-1+I),I=1,NS)
      IF(TEST) WRITE(MSGFIL,811)  (RSPEC(NEPS-1+I),I=1,NS)
C---- SIGMA IN AENGSTROEM
      READ(NFISTO,802)            (RSPEC(NSIG-1+I),I=1,NS)
      IF(TEST) WRITE(MSGFIL,811)  (RSPEC(NSIG-1+I),I=1,NS)
C***********************************************************************
C     INPUT OF XLCOED USED IN TRANSPORT MIXING RULES                   *
C***********************************************************************
      READ(NFISTO,802)            (RSPEC(NLCOED    +3*(I-1)),I=1,NS)
      IF(TEST) WRITE(MSGFIL,811)  (RSPEC(NLCOED    +3*(I-1)),I=1,NS)
      READ(NFISTO,802)            (RSPEC(NLCOED  +1+3*(I-1)),I=1,NS)
      IF(TEST) WRITE(MSGFIL,811)  (RSPEC(NLCOED  +1+3*(I-1)),I=1,NS)
      READ(NFISTO,802)            (RSPEC(NLCOED  +2+3*(I-1)),I=1,NS)
      IF(TEST) WRITE(MSGFIL,811)  (RSPEC(NLCOED  +2+3*(I-1)),I=1,NS)
C***********************************************************************
C     INPUT OF THERMODYNAMIC VALUES                                    *
C***********************************************************************
C                                                                      *
C                                                                      *
C     HL(K,J)             = NASA COEFF. OF H  (7,NS), T<1000K          *
C     HH(K,J)             = NASA COEFF. OF H  (7,NS), T>1000K          *
C                                                                      *
C     HH(1,I),HL(1,I)     = STANDARD SPECIFIC HEAT CAPACITY            *
C     HH(6,I),HL(6,I)     = STANDARD SPECIFIC ENTHALPY                 *
C     HH(7,I),HL(7,I)     = STANDARD SPECIFIC ENTROPY                  *
C     HH(2-5,I),HL(2-5,I) = COEFFICIENTS OF POLYNOMICAL FIT FOR CP(T)  *
C                                                                      *
C     HSW(K,I)            = TEMPERATURE LIMITS FOR FIT                 *
C                                                                      *
C     HSW(1,I)            = LOWER TEMPERATURE LIMIT (K)                *
C     HSW(2,I)            = TEMPERATURE FOR SWITCH FROM LOW TO HIGH    *
C                            TEMPERATURE FIT (K)                       *
C     HSW(3,I)            = UPPER TEMPERATURE LIMIT (K)                *
C                                                                      *
C     ALL GIVEN IN SI UNITS (J,KG,K)                                   *
C                                                                      *
C***********************************************************************
      DO 290 I=1,NS
C---- H(J/KG)
      READ(NFISTO,803)
     1         (RSPEC(NHL-1+K+(I-1)*7),K=1,7),
     1         (RSPEC(NHH-1+K+(I-1)*7),K=1,7),
     1         (RSPEC(NHSW-1+K+(I-1)*3),K=1,3)
      IF(TEST) WRITE(MSGFIL,813) I,(RSPEC(NHL-1+K+(I-1)*7),K=1,7),
     1                           I,(RSPEC(NHH-1+K+(I-1)*7),K=1,7),
     1                           I,(RSPEC(NHSW-1+K+(I-1)*3),K=1,3)
  290 CONTINUE
      ENDIF
C***********************************************************************
C     REGULAR EXIT                                                     *
C***********************************************************************
      IERR=0
      RETURN
C***********************************************************************
C     ERROR EXIT                                                       *
C***********************************************************************
  999 IERR=1
        write(*,*)'TEST MEMORY POINTERS:'
        write(*,*)'LRSPEC MUST BE GREATER THAN NNEXT!'
        write(*,*)'ADJUST NUMBER OF SPECIES IN INSDIM.F.'
        write(*,*)'NNEXT: ',NNEXT-1
        write(*,*)'LRSPEC: ',LRSPEC
      WRITE(MSGFIL,998)
      WRITE(MSGFIL,998)
      WRITE(MSGFIL,998)
  998 FORMAT(' ','++++++      ERROR IN -INPSPE-      ++++++')
      RETURN
C***********************************************************************
C     FORMAT STATEMENTS                                                *
C***********************************************************************
  802 FORMAT(12X,5(E11.4,1X))
  809 FORMAT(12X,5(E20.13,1X))
  803 FORMAT(12X,5(1PE11.4,1X),/,
     1           5(1PE11.4,1X),/,
     1           4(1PE11.4,1X),1PE10.3,1X,1PE10.3,1X,1PE10.3)
  805 FORMAT(12X,5(A,2X))
C---- SPECIAL FORMATS FOR TEST OUTPUT
  811 FORMAT(' ',11X,5(1PE11.4,1X)/(12X,5(1PE11.4,1X)))
  812 FORMAT(1(3X,6(A,2X)))
  813 FORMAT(' HL  (',I4,')',1X,4(1X,1PE11.4),/,12X,3(1X,1PE11.4),/,
     1       ' HH  (',I4,')',1X,4(1X,1PE11.4),/,12X,3(1X,1PE11.4),/,
     1       ' HSW (',I4,')',1X,3(1X,1PE11.4))
C***********************************************************************
C     END OF -INPSPE-                                                  *
C***********************************************************************
      END
      SUBROUTINE INPMEC(NS,SYMB,NFISTO,NFIMEC,MSGFIL,IPRI,IERR,
     1    NREAC,NM,NRINS,NRTAB,NINERT,
     2    LIREAC,IREAC,NIREV,NIARRH,NMATLL,NMATLR,NNRVEC,
     2    LRREAC,RREAC,NPK,NTEXP,NEXA,NVT4,NVT5,NVT6,
     3    NXZSTO,NRATCO,LIW,IWORK,
     3    NAINF,NBINF,NEINF,NT3,NT1,NT2,NAFA,NBFA)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *  INPUT OF THE REACTION MECHANISM DATA FROM UNIT NFISTO    *    *
C     *                      10/03/1988                           *    *
C     *              LAST CHANGE: 10/03/1988/MAAS                 *    *
C     *                   CHANGE: 26/11/2001/MAIWALD              *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C     ALL DATA IN SI UNITS (IF NOT SPECIFIED)                          *
C***********************************************************************
C                                                                      *
C     THIS SUBROUTINE READS THE DATA OF A REACTION MECHANISM AND BUILDS*
C     UP ALL INFORMATION FOR THE EVALUATION OF REACTION RATES          *
C     FURTHERMORE THE THIRD BODY SYMBOLS ARE ADDED TO SYM IN           *
C     SYMB(NS+1 .... NS+NM) AND BLANKS ARE ASSIGNED TO SYMB(NS+NM+1)   *
C                                                                      *
C    INPUT PARAMETERS:                                                 *
C      NS           = NUMBER OF SPECIES                                *
C      LRREAC       = DIMENSION OF REAL ARRAY                          *
C      LIREAC       = DIMENSION OF INTEGER ARRAY                       *
C      NRTAB        = NUMBER OF ENTRYS IN TABLE OF RATE COEFFICIENTS   *
C      SYMB(*)      = SPECIES SYMBOLS (CHARACTER*(*)                   *
C      NFISTO       = NUMBER OF INPUT FILE                             *
C      MSGFIL       = NUMBER OF FILE FOR LISTING AND ERROR MESSAGES    *
C      IPRINT       = 1 FOR PRINTOUT OF THE MECHANISM                  *
C      IPRI         = 2 FOR DETAILED OUTPUT FOR TEST                   *
C      IERR         = RETURN CODE (IERR > 0 FOR ABNORMAL END)          *
C                                                                      *
C    OTHER SUBROUTINES NEEDED: SETMEC,PRIMEC                           *
C                                                                      *
C     DESCRIPTION OF PARAMETERS:                                       *
C      NREAC        = NO. OF REACTIONS                                 *
C      NM           = NO. OF DIFFERENT THIRD BODIES                    *
C      NINERT       = NO. OF INERT SPECIES                             *
C      NRINS        = DIMENSION OF SPARSE PATTERN IN NRVEC             *
C      IREAC        = INTEGER WORK ARRAY CONTAINING THE MATRICES OF    *
C                     STOICHIOMETRY ETC.                               *
C      RREAC        = REAL WORK ARRAY CONTAINING ARRHENIUS PARAMETERS,
C                     COLLISION EFFICIENCIES ETC.                      *
C                                                                      *
C      DESCRIPTION OF ENTRIES:                                         *
C                                                                      *
C  A) REAL   ARRAY                                                     *
C                                                                      *
C      XZSTO(NM,NS)           THIRD BODY COLLISION EFFICIENCIES        *
C                                ---> STARTING AT RREAC(NXZSTO)        *
C      PK(NREAC)              PREEXPONENTIAL COEFFICIENT               *
C                                ---> STARTING AT RREAC(NPK)           *
C      TEXP(NREAC)            TEMPERATURE EXPONENT                     *
C                                ---> STARTING AT RREAC(NTEXP)         *
C      EXA(NREAC)             ACTIVATION ENERGY                        *
C                                ---> STARTING AT RREAC(NEXA)          *
C      AINF(NREAC),BINF(NREAC),                                        *
C      EINF(NREAC),T3(NREAC),T1(NREAC),                                *
C      T2(NREAC)AFA(NREAC),BFA(NREAC)                                  *
C                             PARAMETERS FOR FALLOFF-REACTIONS         *
C                                ---> STARTING AT RREAC(NAINF)         *
C      RATCO(NREAC,NRTAB)     TABLE OF RATE COEFFICIENTS K(T=10*NRTAB) *
C                                ---> STARTING AT RREAC(NRATCO)        *
C                                                                      *
C  B) INTEGER ARRAY                                                    *
C                                                                      *
C      MATLL(3,NREAC)         LEFT HAND SIDE MATRIX OF REACTIONS       *
C                                ---> STARTING AT IREAC(NMATLL)        *
C      MATLR(3,NREAC)         RIGHT HAND SIDE MATRIX OF REACTIONS      *
C                                ---> STARTING AT IREAC(NMATLR)        *
C      NRVEC(3,NRINS)         SPARSE PATTERN FOR REACTIONS             *
C                                ---> STARTING AT IREAC(NNRVEC)        *
C                                                                      *
C     DESCRIPTION OF SPARSE PATTERN:                                   *
C                                                                      *
C     SPECIES NRVEC(1,J) REACTS IN REACTION NRVEC(3,J) WITH            *
C     STOICHOMETRY NRVEC(2,J)                                          *
C     MAT(1,J),MAT(2,J),MAT(3,J) GIVE THE INDICES OF THE SPECIES THAT  *
C     ARE EDUCTS (MATLL) OR PRODUCTS(MATLR). THIRD BODIES ARE TREATED  *
C     WITH INDEX I(TB) + NS. IF LESS THAN THREE SPECIES REACT, DUMMIES *
C     ARE INDEXED BY NS+NM+1                                           *
C***********************************************************************
C     STORAGE ORGANIZATION, INITIAL VALUES                             *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C---- DIMENSIONS AND TYPES OF FORMAL PARAMETERS
      CHARACTER(LEN=*) SYMB(*)
      DIMENSION RREAC(LRREAC)
      DIMENSION IREAC(LIREAC)
C---- IWORK IS NOT USED
      DIMENSION IWORK(LIW)
C***********************************************************************
C     INPUT OF NUMBERS                                                 *
C***********************************************************************
      READ(NFISTO,801)  NREAC
      IF(IPRI.GE.2) WRITE(MSGFIL,801)  NREAC
      READ(NFISTO,801)            NM
      IF(IPRI.GE.2) WRITE(MSGFIL,801)  NM
      NSNM=NS+NM
C***********************************************************************
C
C     INPUT OF REACTION EQUATIONS                                      *
C
C***********************************************************************
C***********************************************************************
C     CALCULATE POINTERS FOR REAL AND INTEGER WORK ARRAY               *
C***********************************************************************
      NPK    = 1
      NTEXP  = NPK   + NREAC
      NEXA   = NTEXP + NREAC
      NVT4   = NEXA  + NREAC
      NVT5   = NVT4  + NREAC
      NVT6   = NVT5  + NREAC
      NXZSTO = NVT6  + NREAC
      NAINF  = NXZSTO+ (NM*NS)
      NBINF  = NAINF + NREAC
      NEINF  = NBINF + NREAC
      NT3    = NEINF + NREAC
      NT1    = NT3   + NREAC
      NT2    = NT1   + NREAC
      NAFA   = NT2   + NREAC
      NBFA   = NAFA  + NREAC
      NMATLL=             1
      NMATLR=NMATLL+3*NREAC
      NIARRH=NMATLR+3*NREAC
      NNRVEC=NIARRH+  NREAC + 1
      IF(NNRVEC-1.GT.LIREAC) GOTO 920
C***********************************************************************
C     CALCULATE POINTERS FOR REAL AND INTEGER WORK ARRAY               *
C***********************************************************************
      DO 510 M=1,NREAC
      READ(NFISTO,852)
     1      (IREAC(NMATLL-1+3*(M-1)+I),I=1,3),
     1      (IREAC(NMATLR-1+3*(M-1)+I),I=1,3),
     1       RREAC(NPK  -1+M),RREAC(NTEXP-1+M),RREAC(NEXA -1+M),
     1       IREAC(NIARRH-1+M),
     1       RREAC(NVT4 -1+M),RREAC(NVT5 -1+M),RREAC(NVT6 -1+M)
      READ(NFISTO,8525)
     1       RREAC(NAINF-1+M),RREAC(NBINF-1+M),RREAC(NEINF-1+M),
     2       RREAC(NT3  -1+M),RREAC(NT1  -1+M),RREAC(NT2  -1+M),
     3       RREAC(NAFA -1+M),RREAC(NBFA -1+M)
  510 CONTINUE
  852 FORMAT(6(1X,I5),3X,3(1PE11.4,1X),/,1X,I5,33X,3(1PE11.3,1X))
 8525 FORMAT(25X,3(1PE11.4,1X),/,25X,3(1PE9.2,1X),1X,1PE9.2,1X,1PE9.2)
C**********************************************************************C
C     CHECK IF THERE ARE ANY NON-ARRHENIUS TYPE REACTIONS              C
C**********************************************************************C
      IREAC(NIARRH-1+NREAC+1) = 0
      DO 453 M = 1,NREAC
      IF(IREAC(NIARRH-1+M).EQ.1) THEN
        IREAC(NIARRH-1+NREAC+1) = 1
        GOTO 454
      ELSE IF(IREAC(NIARRH-1+M).EQ.2) THEN
        IREAC(NIARRH-1+NREAC+1) = 2
        GOTO 454
      ENDIF
  453 CONTINUE
  454 CONTINUE
C**********************************************************************C
C     INPUT OF REACTION POINTERS                                       C
C**********************************************************************C
      READ(NFISTO,853) NRINS
      NIREV=NNRVEC+3*NRINS
      IF(NIREV-1.GT.LIREAC) GOTO 920
  853 FORMAT(I5)
      READ(NFISTO,851) ((IREAC(NNRVEC-1+(J-1)*3+I),I=1,3),J=1,NRINS)
  851 FORMAT(4(2X,3I5))
C***********************************************************************
C     INPUT OF INFORMATION ABOUT REVERSE REACTION AND INERT SPECIES    *
C***********************************************************************
      NEEDIR=NIREV+NREAC-1
      IF(NEEDIR.GT.LIREAC) GOTO 920
      READ(NFISTO,806)                 (IREAC(NIREV-1+I),I=1,NREAC)
      IF(IPRI.GE.2) WRITE(MSGFIL,806)  (IREAC(NIREV-1+I),I=1,NREAC)
C---- NINERT IS READ, BUT NOT USED
      READ(NFISTO,801)                  NINERT
      IF(IPRI.GE.2) WRITE(MSGFIL,801)   NINERT
C***********************************************************************
C     INPUT OF COLLISION EFFICIENCIES                                  *
C***********************************************************************
      NRATCO = NXZSTO + NM * NS
      IF(NRATCO-1.GT.LRREAC) GOTO 910
      DO 390 I=1,NM
      READ(NFISTO,804) SYMB(NS+I),(RREAC(NXZSTO-1+I+(K-1)*NM),K=1,NS)
      IF(IPRI.GE.2) WRITE(MSGFIL,811)
     1         (RREAC(NXZSTO-1+I+(K-1)*NM),K=1,NS)
  390 CONTINUE
      SYMB(NS+NM+1)=REPEAT(' ',LEN(SYMB(NS+NM+1)))
C***********************************************************************
C     CALCULATE POINTERS FOR REAL WORK ARRAY                           *
C***********************************************************************
      NRNEED = NRATCO + NRTAB * NREAC
      IF(NRNEED.GT.LRREAC) GOTO 910
C***********************************************************************
C     COMPUTE FINAL ENTRIES OF REACTION ARRAYS                         *
C***********************************************************************
      CALL SETMEC(NS,NM,NSNM,NREAC,NRTAB,
     1 RREAC(NPK),RREAC(NEXA),RREAC(NTEXP),
     1 RREAC(NVT4),RREAC(NVT5),RREAC(NVT6),RREAC(NAINF),RREAC(NBINF),
     1 RREAC(NEINF),RREAC(NT3),RREAC(NT1),RREAC(NT2),RREAC(NAFA),
     1 RREAC(NBFA),IREAC(NIARRH),RREAC(NRATCO),
     1 SYMB,IREAC(NMATLL),IREAC(NMATLR),MSGFIL,IPRI,IERR)
C***********************************************************************
C     REGULAR EXIT                                                     *
C***********************************************************************
      IERR=0
      RETURN
C***********************************************************************
C     ERROR EXIT                                                       *
C***********************************************************************
  910 WRITE(MSGFIL,911) NRNEED,LRREAC
  911 FORMAT('  NEEDED STORAGE FOR RREAC(',I6,') EXCEEDS GIVEN SPACE(',
     1  I6,')',/,'     ----->  CHECK DIMENSION OF RREAC  ')
      IERR=1
      GOTO 999
  920 WRITE(MSGFIL,921) NEEDIR,LIREAC
  921 FORMAT('   NOT ENOUGH SPACE PROVIDED FOR IREAC',/,
     1      'NEEDIR=',I10,'   LIREAC=',I10,
     1      '     ----->  CHECK DIMENSION OF IREAC  ')
      IERR=2
      GOTO 999
  999 CONTINUE
      WRITE(MSGFIL,998)
      WRITE(MSGFIL,998)
      WRITE(MSGFIL,998)
      STOP
  998 FORMAT(' ','++++++      ERROR IN -INPMEC-      ++++++')
      RETURN
C***********************************************************************
C     FORMAT STATEMENTS                                                *
C***********************************************************************
  801 FORMAT(I5,9X,I5)
C 802 FORMAT(12X,5(E11.4,1X))
  804 FORMAT(1X,A8,3X,5(E11.4,1X))
  806 FORMAT(12X,30I2)
C---- SPECIAL FORMATS FOR TEST OUTPUT
  811 FORMAT(' ',11X,5(1PE11.4,1X)/(12X,5(1PE11.4,1X)))
C***********************************************************************
C     END OF -MECINP-                                                  *
C***********************************************************************
      END
      SUBROUTINE SETMEC(NS,NM,NSNM,NREAC,NRTAB,PK,EXA,TEXP,
     1     VT4,VT5,VT6,AINF,BINF,EINF,T3,T1,T2,AFA,
     1     BFA,IARRH,RATCO,SYMB,MATLL,MATLR,MSGFIL,IPRI,IERR)
C***********************************************************************
C                                                                      *
C     ************************************************************     *
C     *                                                          *     *
C     *   CALCULATION OF TABLE OF RATE COEFFICIENTS              *     *
C     *                                                          *     *
C     ************************************************************     *
C                                                                      *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL TSO
      CHARACTER(LEN=*)  SYMB(*)
      DIMENSION MATLL(3,NREAC),MATLR(3,NREAC)
      DIMENSION PK(NREAC),EXA(NREAC),TEXP(NREAC),RATCO(NREAC,*)
      DIMENSION VT4(NREAC),VT5(NREAC),VT6(NREAC)
      DIMENSION AINF(NREAC),BINF(NREAC),EINF(NREAC),T3(NREAC)
      DIMENSION T2(NREAC),T1(NREAC),AFA(NREAC),BFA(NREAC)
      DIMENSION IARRH(NREAC+1)
C-DIM DIMENSION IARRH(*)
      DIMENSION NSTOE(6)
C---- RGAS
      DATA RGAS/8.3143D0/
C***********************************************************************
C                                                                      *
C     INITIALIZATON                                                    *
C                                                                      *
C***********************************************************************
      NSNM =NS+NM
      NSNM1=NSNM+1
      TSO=.TRUE.
C***********************************************************************
C                                                                      *
C     CALCULATE TABLE OF RATE COEFFICIENTS                             *
C                                                                      *
C***********************************************************************
      DO 120 N=1,NREAC
      DO 110 K=20,NRTAB
      T=FLOAT(K)*10.0D0
      IF(IARRH(N).EQ.0) THEN
      HVAR=-EXA(N)/(RGAS*T)
      IF(HVAR.LT.-170.) RATCO(N,K)=0.0D0
      IF(HVAR.LT.-170.) GOTO 110
      RATCO(N,K)=PK(N)*T**TEXP(N)*DEXP(HVAR)
      ELSE IF(IARRH(N).EQ.1) THEN
      CALL VTREAC(IARRH(N),PK(N),TEXP(N),EXA(N),VT4(N),VT5(N),VT6(N),
     1       T,TTRAT)
      RATCO(N,K) = TTRAT
      ELSE
C      	CALL FALLOFF(PK(I),TEXP(I),EXA(I),AINF(I),BINF(I),EINF(I),
C     1                T3(I),T1(I),T2(I),AFA(I),BFA(I),MCON,T,
C     2                VORFINF,PCOR,IORD)
         TTRAT = 0.0D0
      RATCO(N,K)= TTRAT
      ENDIF
  110 CONTINUE
  120 CONTINUE
C***********************************************************************
C                                                                      *
C     CALCULATION OF POINTERS FOR REACTION RATES                       *
C                                                                      *
C***********************************************************************
C***********************************************************************
C     CALCULATION OF POINTERS FOR LEFT HAND SIDE                       *
C***********************************************************************
C---- SET INDICES OF MATRIX ELEMENTS TO NS+NM+1
      DO  J=1,NREAC
      DO  I=1,3
      IF(MATLL(I,J).EQ.0) MATLL(I,J)=NSNM1
      ENDDO
      ENDDO
C***********************************************************************
C     CALCULATION OF POINTERS FOR RIGHT HAND SIDE                      *
C***********************************************************************
C---- SET INDICES OF MATRIX ELEMENTS TO NS+NM+1
      DO J=1,NREAC
      DO I=1,3
      IF(MATLL(I,J).EQ.0) MATLL(I,J)=NSNM1
      IF(MATLR(I,J).EQ.0) MATLR(I,J)=NSNM1
      ENDDO
      ENDDO
C***********************************************************************
C                                                                      *
C     PRINTOUT MECHANISM FOR TESTING                                   *
C                                                                      *
C***********************************************************************
      IF(IPRI.GT.0) THEN
      WRITE(MSGFIL,803)
  803 FORMAT(/,' ',10X,' REACTION EQUATION  ',30X,'A',6X,'T-EXP',4X,'E',
     1         /,56X,' (M,MOL,S)',2X,'(-)',1X,'(KJ/MOL)',/,' ',79('-'))
      DO 555 I=1,NREAC
      DO 551 J=1,3
      NSTOE(J)  =MATLL(J,I)
      NSTOE(J+3)=MATLR(J,I)
 551  CONTINUE
      CALL PRIMEC(SYMB,NSTOE,6,I,PK(I),TEXP(I),EXA(I),
     1     VT4(I),VT5(I),VT6(I),IARRH(I),TSO)
 555  CONTINUE
      ENDIF
      IERR=0
      RETURN
C***********************************************************************
C     CHAPTER VI.: ERROR EXIT                                          *
C***********************************************************************
C 999 CONTINUE
C 998 FORMAT(' ','++++++      ERROR IN -SETMEC-      ++++++')
C     WRITE(MSGFIL,998)
C     WRITE(MSGFIL,998)
C     WRITE(MSGFIL,998)
C     RETURN
C***********************************************************************
C     END OF SETMEC                                                    *
C***********************************************************************
      END
      SUBROUTINE PRIMEC(NSYMB,NSTOE,NFILE6,NR,PK,TEXP,EXA,
     1      VT4,VT5,VT6,IARRH,TSO)
C***********************************************************************
C                                                                      *
C     **************************************************************   *
C     *                                                            *   *
C     *             PRINTING OUT OF REACTION EQATIONS              *   *
C     *                                                            *   *
C     **************************************************************   *
C                                                                      *
C***********************************************************************
C                                                                      *
C     INPUT  :                                                         *
C                                                                      *
C     NSYMB(VAR.)  = SPECIES SYMBOLS       (CHARACTER**(*)             *
C     NSTOE(*)     = SPECIES INDICES                                   *
C     NFILE6       = UNIT OF OUTPUT ON PRINTER                         *
C     TSO          = .TRUE. FOR 80 SIGNS OUTPUT                        *
C     NR,PK,TEXP,EXA:                                                  *
C            PARAMETERS TO BE PRINTED WITH THE REACTION EQUATIO        *
C                                                                      *
C                                                                      *
C***********************************************************************
C                                                                      *
C                                                                      *
C***********************************************************************
C     STORAGE ORGANIZATION, INITIAL VALUES                             *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL TSO
      PARAMETER(ISLEN=16)
      CHARACTER(LEN=*)     NSYMB(*)
      CHARACTER(LEN=ISLEN) SYMB(8)
      CHARACTER(LEN=2) NSIG(7)
      DIMENSION NSTOE(6)
C***********************************************************************
C     ASSIGN SYMBOLS                                                   *
C***********************************************************************
      IF(LEN(NSYMB(1)).GT.ISLEN) THEN
         IF(NR.EQ.1) WRITE(NFILE6,*) ' Symbols too long in Primec, ',
     1                   'mechanism not printed'
        RETURN
      ENDIF
C***********************************************************************
C     ASSIGN SYMBOLS                                                   *
C***********************************************************************
      DO 10 I=1,7
      SYMB(I)=REPEAT(' ',ISLEN)
      NSIG(I)='  '
   10 CONTINUE
C---- ASSIGN SPECIES SYMBOLS
      DO 20 I=1,6
      INDEX=NSTOE(I)
      IF(INDEX.EQ.0) SYMB(I)=REPEAT(' ',LEN(NSYMB(1)))
      IF(INDEX.NE.0) SYMB(I)=NSYMB(INDEX)
   20 CONTINUE
C---- SET SYMBOLS
      DO 30 I=1,5
      IF(NSTOE(I+1).NE.0)  NSIG(I)='+ '
   30 CONTINUE
      NSIG(3)='->'
C***********************************************************************
C     CHECK TASK                                                       *
C***********************************************************************
      IF(TSO)       GOTO 100
      IF(.NOT.TSO)  GOTO 200
C***********************************************************************
C     COMPRESS REACTION FOR PRINTING IN TSO                            *
C***********************************************************************
  100 CONTINUE
      DO 150 K=1,6
       DO 120 I=1,6
        IF(LEN_TRIM(SYMB(I)).GT.0) GOTO 120
        SYMB(I)  = SYMB(I+1)
        NSIG(I-1)  = NSIG(I)
        SYMB(I+1)=REPEAT(' ',LEN(NSYMB(1)))
  120  CONTINUE
  150 CONTINUE
      IF(IARRH.EQ.0) THEN
      WRITE(NFILE6,811) NR,(SYMB(II),NSIG(II),II=1,4),SYMB(5),
     2          PK,TEXP,EXA
      ELSE
      WRITE(NFILE6,821) NR,(SYMB(II),NSIG(II),II=1,4),SYMB(5),
     2          PK,TEXP,EXA,VT4,VT5,VT6
      ENDIF
  811 FORMAT(' ',I3,':',1X,4(A,A2,1X),A,1PE9.2,1X,0PF6.2,-3PF6.1)
  821 FORMAT(' ',I3,':',1X,4(A,A2,1X),A,3(/,58X,2(1PE10.3,2X)))
      GOTO 300
C***********************************************************************
C     PRINT REACTION FOR BATCH                                         *
C***********************************************************************
  200 CONTINUE
      IF(IARRH.EQ.0) THEN
      WRITE(NFILE6,812) NR,(SYMB(II),NSIG(II),
     1   II=1,5),SYMB(6),PK,TEXP,EXA
      ELSE
      WRITE(NFILE6,822) NR,(SYMB(II),NSIG(II),
     1   II=1,5),SYMB(6),PK,TEXP,EXA,VT4,VT5,VT6
      ENDIF
  812 FORMAT(' ',I3,':',3X,5(A,A2,2X),A,10X,
     1              1PE9.2,5X,0PF6.2,5X,-3PF7.2)
  822 FORMAT(' ',I3,':',3X,5(A,A2,2X),A,10X,3(/,86X,2(1PE10.3,2X)))
  300 CONTINUE
      RETURN
C***********************************************************************
C     END OF -PRIMEC-                                                  *
C***********************************************************************
      END
      SUBROUTINE CALXMO(NSPEC,WI,XMOL,XMOLM)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *                                                           *    *
C     *             CALCULATION OF MEAN MOLAR MASS                *    *
C     *                   AUTHOR: U. MAAS                         *    *
C     *                                                           *    *
C     *           LAST CHANGE:  6/10/1988 / U. MAAS               *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C                                                                      *
C     INPUT  :                                                         *
C                                                                      *
C     NSPEC         = NUMBER OF SPECIES                                *
C     WI(NSPEC)     = SPECIES MASS FRACTIONS                           *
C     XMOL(NSPEC)   = MOLAR MASS  (KG/MOLE)                            *
C                                                                      *
C     OUTPUT :                                                         *
C                                                                      *
C     XMOLM         = MEAN MOLAR MASS, KG/MOL                          *
C                                                                      *
C                                                                      *
C***********************************************************************
C                                                                      *
C                                                                      *
C                                                                      *
C***********************************************************************
C     STORAGE ORGANIZATION                                             *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION WI(NSPEC),XMOL(NSPEC)
C***********************************************************************
C     CALCULATION OF MEAN MOLAR MASS                                   *
C***********************************************************************
      XMOLM=0.0D0
      DO I=1,NSPEC
      XMOLM=XMOLM+WI(I)/XMOL(I)
      ENDDO
      XMOLM=1.0D0/XMOLM
      RETURN
C***********************************************************************
C     END OF -CALXMO-                                                  *
C***********************************************************************
      END
      SUBROUTINE CALXCI(NSPEC,WI,RHO,XMOL,XMOLM,C,CI,XI)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *      CALCULATION OF MOLE FRACTIONS AND CONCENTRATIONS     *    *
C     *                   AUTHOR: U. MAAS                         *    *
C     *                       01.02.1987                          *    *
C     *           LAST CHANGE: U. MAAS /  9.08.1988               *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C                                                                      *
C     INPUT  :                                                         *
C                                                                      *
C     NSPEC         = NUMBER OF SPECIES                                *
C     WI(NSPEC)     = SPECIES MASS FRACTIONS                           *
C     RHO           = DENSITY  (KG/M**3)                               *
C     XMOL(NSPEC)   = MOLAR MASSES (KG/MOL)                            *
C     XMOLM         = MEAN MOLAR MASS (KG/MOL)                         *
C                                                                      *
C     OUTPUT :                                                         *
C                                                                      *
C     C             = TOTAL CONCENTRATION, MOL/M**3                    *
C     CI(NSPEC)     = SPECIES CONCENTRATIONS, MOL/M**3                 *
C     XI(NSPEC)     = SPECIES MOLE FRACTIONS                           *
C                                                                      *
C***********************************************************************
C                                                                      *
C***********************************************************************
C     I. STORAGE ORGANIZATION                                          *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION XMOL(NSPEC),CI(NSPEC),XI(NSPEC),WI(NSPEC)
C***********************************************************************
C     II. CALCULATION OF TOTAL CONCENTRATION                           *
C***********************************************************************
      C=RHO/XMOLM
C***********************************************************************
C     III. CALCULATION OF MOLE FRACTIONS AND SPECIES CONCENTRATIONS    *
C***********************************************************************
      DO I=1,NSPEC
        XI(I)=WI(I)*XMOLM/XMOL(I)
        CI(I)=C*XI(I)
      ENDDO
      RETURN
C***********************************************************************
C     END OF -CALXCI-                                                  *
C***********************************************************************
      END
      SUBROUTINE CALDIF(NSPEC,W,X,T,P,DCOEF,DMX,DJK,IERR)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *                   AUTHOR: J. WARNATZ                      *    *
C     *                       08.04.1983                          *    *
C     *              LAST CHANGE: 29.01.1986 / U.MAAS             *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C                                                                      *
C      INPUT  :                                                        *
C                                                                      *
C       NSPEC                = NUMBER OF SPECIES                       *
C       W(NSPEC)             = MASS FRACTIONS IN MIXTURE               *
C       X(NSPEC)             = MOLE FRACTIONS IN MIXTURE               *
C       T                    = TEMPERATURE  (K)                        *
C       P                    = PRESSURE  (N/M**2)                      *
C       MSGFIL               = OUTPUT FILE FOR ERROR MESSAGES          *
C       DCOEF(4,NSPEC,NSPEC) = COEFFICIENTS OF POLYNOMIAL FIT FOR      *
C                            BINAR DIFFUSION COEFFICIENTS (M**2/S)     *
C                            AT P = 1 N/M**2                           *
C                                                                      *
C                                                                      *
C     OUTPUT :                                                         *
C                                                                      *
C     DMX(NSPEC)    = DIFFUSION COEFFICIENTS IN MIXTURE, M**2/S        *
C     IERR          = 1 IF NEGATIVE TEMPERATURE HAS BEEN DETECTED      *
C                                                                      *
C***********************************************************************
C                                                                      *
C                                                                      *
C                                                                      *
C***********************************************************************
C     I. STORAGE ORGANIZATION AND DEFINITION OF INITIAL VALUES         *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C***********************************************************************
C     II. DIMENSIONS                                                   *
C***********************************************************************
      DIMENSION DCOEF(4,NSPEC,NSPEC)
      DIMENSION DJK(NSPEC,NSPEC),W(NSPEC),X(NSPEC),DMX(NSPEC)
C***********************************************************************
C     II. INITIAL CHECKS                                               *
C***********************************************************************
C     IF(T.LE.0.0D0) GO TO 9200
C***********************************************************************
C     III. CALCULATION OF THE BINARY DIFFUSION COEFFICIENTS            *
C***********************************************************************
C---- NOTE: DCOEF AND TL ARE IN SINGLE PRECISION
C     TL=DLOG(T)
      TL=DLOG(DMAX1(T,1.D1))
      DO I=1,NSPEC
      DO K=1,I
      DJK(I,K)= DEXP(  (((DCOEF(4,I,K)*TL) + DCOEF(3,I,K) ) *TL
     1               +DCOEF(2,I,K) ) * TL +DCOEF(1,I,K) )  / P
      ENDDO
      ENDDO
      DO I=1,NSPEC
      DO K=1,I
      DJK(K,I)=DJK(I,K)
      ENDDO
      ENDDO
C***********************************************************************
C     III. SIMPLIFIED DIFFUSION COEFFICIENTS INTO THE MIXTURE          *
C***********************************************************************
      DO 110 K=1,NSPEC
      SUMME=0.0D0
C_rst
      IF(W(K).GE.0.9999D0) THEN
         DO J=1,NSPEC
            IF(J.NE.K) SUMME = SUMME + (1.D0/DJK(J,K))
         ENDDO
         DMX(K) = 1.D0 / SUMME
         GOTO 110
      ENDIF
      DO 100 J=1,NSPEC
      IF(J.EQ.K) GO TO 100
      SUMME=SUMME+X(J)/DJK(J,K)
  100 CONTINUE
      DMX(K)=(1.0D0-W(K))/SUMME
  110 CONTINUE
      IERR=0
      RETURN
C***********************************************************************
C     IV. ERROR EXITS                                                  *
C***********************************************************************
C---- NEGATIVE TEMPERATURE
 9200 CONTINUE
      IERR=1
      RETURN
C***********************************************************************
C     END OF -CALDIF-                                                  *
C***********************************************************************
      END
      SUBROUTINE CALDIF2(NSPEC,W,X,T,P,DCOEF,DMX,DJK,IERR)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *                   AUTHOR: Alexander Neagos
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C                                                                      *
C      INPUT  :                                                        *
C                                                                      *
C       NSPEC                = NUMBER OF SPECIES                       *
C       W(NSPEC)             = MASS FRACTIONS IN MIXTURE               *
C       X(NSPEC)             = MOLE FRACTIONS IN MIXTURE               *
C       T                    = TEMPERATURE  (K)                        *
C       P                    = PRESSURE  (N/M**2)                      *
C       MSGFIL               = OUTPUT FILE FOR ERROR MESSAGES          *
C       DCOEF(4,NSPEC,NSPEC) = COEFFICIENTS OF POLYNOMIAL FIT FOR      *
C                            BINAR DIFFUSION COEFFICIENTS (M**2/S)     *
C                            AT P = 1 N/M**2                           *
C                                                                      *
C                                                                      *
C     OUTPUT :                                                         *
C                                                                      *
C     DMX(NSPEC)    = DIFFUSION COEFFICIENTS IN MIXTURE, M**2/S        *
C     IERR          = 1 IF NEGATIVE TEMPERATURE HAS BEEN DETECTED      *
C                                                                      *
C***********************************************************************
C                                                                      *
C                                                                      *
C                                                                      *
C***********************************************************************
C     I. STORAGE ORGANIZATION AND DEFINITION OF INITIAL VALUES         *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C***********************************************************************
C     II. DIMENSIONS                                                   *
C***********************************************************************
      DIMENSION DCOEF(4,NSPEC,NSPEC)
      DIMENSION DJK(NSPEC,NSPEC),W(NSPEC),X(NSPEC),DMX(NSPEC)
C***********************************************************************
C     II. INITIAL CHECKS                                               *
C***********************************************************************
C     IF(T.LE.0.0D0) GO TO 9200
C***********************************************************************
C     III. CALCULATION OF THE BINARY DIFFUSION COEFFICIENTS            *
C***********************************************************************
C---- NOTE: DCOEF AND TL ARE IN SINGLE PRECISION
C     TL=DLOG(T)
      TL=DLOG(DMAX1(T,1.D1))
      DO I=1,NSPEC
      DO K=1,I
      DJK(I,K)= DEXP(  (((DCOEF(4,I,K)*TL) + DCOEF(3,I,K) ) *TL
     1               +DCOEF(2,I,K) ) * TL +DCOEF(1,I,K) )  / P
      ENDDO
      ENDDO
      DO I=1,NSPEC
      DO K=1,I
      DJK(K,I)=DJK(I,K)
      ENDDO
      ENDDO
C***********************************************************************
C     III. SIMPLIFIED DIFFUSION COEFFICIENTS INTO THE MIXTURE          *
C***********************************************************************
C      SUMW = SUM(W(1:NSPEC))
      DO 110 K=1,NSPEC
      SUMME=0.0D0
      SUMME2=0.0D0
C_rst
*      IF(W(K).GE.0.9999D0) THEN
*         DO J=1,NSPEC
*            IF(J.NE.K) SUMME = SUMME + (1.D0/DJK(J,K))
*         ENDDO
*         DMX(K) = 1.D0 / SUMME
*         GOTO 110
*      ENDIF
      DO 100 J=1,NSPEC
      IF(J.EQ.K) GO TO 100
      SUMME=SUMME+X(J)/DJK(J,K)
  100 CONTINUE
C
      DO 200 J=1,NSPEC
      IF(J.EQ.K) GO TO 200
      SUMME2=SUMME2+W(J)/DJK(J,K)
  200 CONTINUE
C
      DMX(K)=summe+(X(K)/(1.0d0-W(K)))*SUMME2
      DMX(K)=1.0d0/DMX(K)
C      DMX(K)=(1.0D0-W(K)/sumw)/SUMME
  110 CONTINUE
      IERR=0
      RETURN
C***********************************************************************
C     IV. ERROR EXITS                                                  *
C***********************************************************************
C---- NEGATIVE TEMPERATURE
 9200 CONTINUE
      IERR=1
      RETURN
C***********************************************************************
C     END OF -CALDIF-                                                  *
C***********************************************************************
      END
      SUBROUTINE CALDTH(NSPEC,T,C,XMOLM,DMIX,EPS,XMOL,W,X,
     1      XMMAX,DTHERM,IERR)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *    CALCULATION OF THE THERMAL DIFFUSION COEFFICIENTS      *    *
C     *                   AUTHOR: J.WARNATZ                       *    *
C     *                       21.04.1983                          *    *
C     *              LAST CHANGE: 10/03/1988/MAAS                 *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C                                                                      *
C     INPUT  :                                                         *
C                                                                      *
C     NSPEC        = NUMBER OF ACTUAL SPECIES                          *
C     T            = TEMPERATURE, K                                    *
C     C            = TOTAL CONCENTRATION, MOL/M**3                     *
C     XMOLM        = MEAN MOLAR MASS, KG/MOL                           *
C     DMIX(NSPEC)  = DIFFUSION COEFFICIENT INTO MIXTURE, M**2/S (NSPEC)*
C R*4 EPS(NSPEC)   = L-J PARAMETERS EPS/K IN KELVIN                    *
C     XMOL(NSPEC)  = MOLAR MASSES IN KG/MOLE                           *
C     W(NSPEC)     = MASS FRACTIONS, - (NSPEC)                         *
C     XMMAX        = MAXIMUM MOLAR MASS FOR SPECIES TO BE CONSIDERED   *
C                                                                      *
C     OUTPUT :                                                         *
C                                                                      *
C     DTHERM(NSPEC)= THERMAL DIFFUSION COEFFICIENTS,(KG/M**3)*(KG/M*S) *
C     IERR         = POSITIVE, IF ERROR HAS BEEN DETECTED              *
C                                                                      *
C                                                                      *
C***********************************************************************
C                                                                      *
C                                                                      *
C                                                                      *
C***********************************************************************
C     STORAGE ORGANIZATION AND DEFINITION OF INITIAL VALUES            *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION EPS(NSPEC)
      DIMENSION XMOL(NSPEC),W(NSPEC),X(NSPEC),DMIX(NSPEC),DTHERM(NSPEC)
C***********************************************************************
C     CALCULATION OF THERMAL DIFFUSION RATIOS                          *
C***********************************************************************
      DO 3000 I=1,NSPEC
      DTHERM(I)=0.0D0
      IF(XMOL(I).GT.XMMAX) GO TO 3000
      DO 2500 J=1,NSPEC
      TST=T/(SQRT(EPS(I)*EPS(J)))
      IF(TST.GT.6.5D0) GOTO 1990
      IF((TST.GT.4.0D0).AND.(TST.LE.6.5D0)) GOTO 1980
      IF((TST.GT.2.5D0).AND.(TST.LE.4.0D0)) GOTO 1970
      IF((TST.GT.1.7D0).AND.(TST.LE.2.5D0)) GOTO 1960
      IF((TST.GT.1.0D0).AND.(TST.LE.1.7D0)) GOTO 1950
C---- HERE IF TST < 1.
      RT= 0.0D0
      GOTO 2000
 1950 RT=0.015D0+(TST-1.0D0)*0.300D0
      GOTO 2000
 1960 RT=0.225D0+(TST-1.7D0)*0.188D0
      GOTO 2000
 1970 RT=0.375D0+(TST-2.5D0)*0.097D0
      GOTO 2000
 1980 RT=0.520D0+(TST-4.0D0)*0.0264D0
      GOTO 2000
 1990 RT=0.60D0
      GOTO 2000
 2000 CONTINUE
C---- DTHERM THE (DIMENSIONLESS) THERMODIFFUSION RATIO AT THIS PLACE
      DTHERM(I)=DTHERM(I)+RT*(XMOL(I)-XMOL(J))*X(I)*X(J)/(XMOL(I)+
     1       XMOL(J))
 2500 CONTINUE
C---- AVOID DIVISION BY ZERO
      DTHERM(I)=0.5D0*DTHERM(I)*DMIX(I)*C*XMOL(I)
      IF(DABS(1.0D0-X(I)).LT.1.D-10) GOTO 3000
      DTHERM(I)=DTHERM(I)* (1.0D0-W(I))/(1.0D0-X(I))
 3000 CONTINUE
      IERR=0
      RETURN
C***********************************************************************
C     END OF -CALDTH-                                                  *
C***********************************************************************
      END
      SUBROUTINE CALLAM(NSPEC,W,X,T,XMOL,SIG,EPS,XLCOEF,XLCOEM,
     1     XLCOED,SYMB,XLMIX,IERR)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *    PROGRAM FOR THE CALC. OF MIXTURE HEAT CONDUCTIVITIES   *    *
C     *               AUTHORS: A.HEINTZ/J.WARNATZ                 *    *
C     *                       06.12.1984                          *    *
C     *             LAST CHANGE: 04.01.1985/WARNATZ               *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C                                                                      *
C     INPUT  :                                                         *
C                                                                      *
C     NSPEC           = NUMBER OF SPECIES                              *
C     W(NSPEC)        = MASS FRACTIONS, - (NSPEC)                      *
C     X(NSPEC)        = MOLE FRACTIONS, - (NSPEC)                      *
C     T               = TEMPERATURE, K                                 *
C R*4 XLCOEF(4,NSPEC) = COEFFICIENTS OF POLYNOMIAL FIT FOR SPECIES HEAT*
C                       CONDUCTIVITIES, J/M*K*S                        *
C R*4 XLCOEM(4,NSPEC) = COEFFICIENTS OF POLYNOMIAL FIT FOR SPECIES HEAT*
C                       CONDUCTIVITIES (MONOATOMIC PART), J/M*K*S      *
C R*4 XLCOED(3,NSPEC) = COEFFICIENTS OF D(INT)                         *
C R*4 SIG(NSPEC)      = LJ POTENTIALPARAMETER SIGMA (A)                *
C R*4 EPS(NSPEC)      = LJ POTENTIALPARAMETER EPS/K (K)                *
C     SYMB(NSPEC)     = SPECIES SYMBOLS                                *
C     XMOL(NSPEC)     = MOLAR MASSES, KG/MOL                           *
C                                                                      *
C     OUTPUT :                                                         *
C                                                                      *
C     XLMIX         = MIXTURE HEAT CONDUCTIVITIES, J/M*K*S             *
C                                                                      *
C                                                                      *
C     XLCOEF,XLCOEM,XLCOED,XLA,TL ARE IN SINGLE PRECISION              *
C     IERR         = POSITIVE, IF ERROR HAS BEEN DETECTED              *
C                                                                      *
C***********************************************************************
C                                                                      *
C                                                                      *
C                                                                      *
C***********************************************************************
C     TYPES AND DIMENSIONS                                             *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION  (A-H,O-Z)
      LOGICAL EQUSYM
C---- XLA IS NOT USED AT THE MOMENT
      DIMENSION XLA(2,1)
      CHARACTER(LEN=*)  SYMB(NSPEC)
      DIMENSION SIG(NSPEC),EPS(NSPEC)
      DIMENSION XLCOEF(4,NSPEC),XLCOEM(4,NSPEC),XLCOED(3,NSPEC)
      DIMENSION W(NSPEC),X(NSPEC),XMOL(NSPEC)
C***********************************************************************
C     CALCULATION OF MIXTURE HEAT CONDUCTIVITY (SIMPLE RULE)           *
C***********************************************************************
C---- DUMMY CARD TO AVOID WARNING FROM WATFIV COMPILER
C----
      IF(1.GT.2) GO TO 1010
C----
      TL=DLOG(DMAX1(T,1.D2))
      SUM1=0.0D0
      SUM2=0.0D0
      DO 100 I=1,NSPEC
      XLAX= DEXP( ((XLCOEF(4,I)*TL+XLCOEF(3,I))*TL+XLCOEF(2,I))*TL
     1        +XLCOEF(1,I) )
      SUM1=SUM1+X(I)*XLAX
      SUM2=SUM2+X(I)/XLAX
  100 CONTINUE
      XLMIX=(SUM1+1.0D0/SUM2)*0.5D0
      IERR=0
      RETURN
C***********************************************************************
C     CALCULATION OF PURE SPECIES CONDUCTIVITY AND MONOATOMIC PART     *
C     DIMENSION XLA IF THIS WILL EVER BE USED                          *
C***********************************************************************
 1010 CONTINUE
      TL=DLOG(T)
      DO 55 I=1,NSPEC
C---- SINGLE SPECIES HEAT CONDUCTIVITY
      XLA(2,I)=XLCOEM(4,I)
      XLA(1,I)=XLCOEF(4,I)
      DO K=1,3
      XLA(2,I)=XLA(2,I)*TL+XLCOEM(4-K,I)
      XLA(1,I)=XLA(1,I)*TL+XLCOEF(4-K,I)
      ENDDO
      XLA(2,I)=EXP(XLA(2,I))
      XLA(1,I)=EXP(XLA(1,I))
   55 CONTINUE
C***********************************************************************
C     CALCULATION OF MIXTURE HEAT CONDUCTIVITY                         *
C***********************************************************************
      SUM1=0.0D0
      SUM4=0.0D0
      DO 3 I=1,NSPEC
      TS=T/EPS(I)
      AG=DLOG(TS)
      OMEGD=DEXP(0.348D0-0.459D0*AG+0.095D0*AG*AG-1.D-1*AG*AG*AG)
      DIF=2.6636D-7*(T*T*T/DSQRT(1.D3*XMOL(I)))/(OMEGD*SIG(I)*SIG(I))
      SUM2=0.0D0
      SUM3=1.0D0
      DO 4 J=1,NSPEC
      X12=XLA(2,I)/XLA(2,J)
      A12=XMOL(I)/XMOL(J)
      AIJ=(1.0D0+DSQRT(X12)*DSQRT(DSQRT(A12)))
      AIJ=AIJ*AIJ/DSQRT(8.0D0*(1.0D0+A12))
      ABI=1.07D0
      ABJ=1.07D0
      IF(EQUSYM(8,SYMB(I),'CO2     ')) ABI=0.93D0
      IF(EQUSYM(8,SYMB(I),'H2O     ')) ABI=0.62D0
      IF(EQUSYM(8,SYMB(I),'NH3     ')) ABI=0.69D0
      IF(EQUSYM(8,SYMB(J),'CO2     ')) ABJ=0.93D0
      IF(EQUSYM(8,SYMB(J),'H2O     ')) ABJ=0.62D0
      IF(EQUSYM(8,SYMB(J),'NH3     ')) ABJ=0.69D0
      ABC=(ABI+ABJ)/2.0D0
      IF(I.EQ.J) ABC=1.0D0
      SUM2=SUM2+X(J)*AIJ*ABC
      E12=SQRT(EPS(I)*EPS(J))
      S12=(SIG(I)+SIG(J))/2.0D0
      TS12=T/E12
      AG=DLOG(TS12)
      OMEGA=DEXP(0.348D0-0.459D0*AG+0.095D0*AG*AG-1.D-1*AG*AG*AG)
      DF12=2.6636D-7*DSQRT(T*T*T*(XMOL(I)+XMOL(J))/(2.D3*XMOL(I)
     1     *XMOL(J))) / (OMEGA*S12*S12)
      F=1.0D0
      IF(I.EQ.J) F=0.0D0
      DI=XLCOED(1,I)+XLCOED(2,I)*T+XLCOED(3,I)*T*T
      DJ=XLCOED(1,J)+XLCOED(2,J)*T+XLCOED(3,J)*T*T
      SUM3=SUM3+F*X(J)*DIF*DSQRT(DI/DJ)/(X(I)*DF12)
    4 CONTINUE
      SUM1=X(I)*XLA(2,I)/SUM2+SUM1
      XLIN=(XLA(1,I)-XLA(2,I))
      SUM4=SUM4+XLIN/SUM3
    3 CONTINUE
      XLMIX=(SUM1+SUM4)
      IERR=0
      RETURN
C***********************************************************************
C     ERROR EXITS                                                      *
C***********************************************************************
C***********************************************************************
C     END OF -CALLAM-                                                  *
C***********************************************************************
      END
      SUBROUTINE CALVIS(NSPEC,W,X,T,ETA,VISC,IERR)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *    PROGRAM FOR THE CALC. OF MIXTURE VISCOSITIES           *    *
C     *                                                           *    *
C     *             LAST CHANGE:     4.1986/MAAS                  *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C                                                                      *
C     INPUT  :                                                         *
C                                                                      *
C     NSPEC         = NUMBER OF SPECIES                                *
C     W(NSPEC)      = MASS FRACTIONS, - (NSPEC)                        *
C     X(NSPEC)      = MOLE FRACTIONS, - (NSPEC)                        *
C     T             = TEMPERATURE, K                                   *
C R*4 ETA(4,NSPEC)  = COEFFICIENTS OF POLYNOMIAL FIT FOR SPECIES VIS-  *
C                     COSITY, KG/M*S                                   *
C                                                                      *
C     OUTPUT :                                                         *
C                                                                      *
C     VISC          = MIXTURE HEAT CONDUCTIVITIES, KG/M*S              *
C     IERR          = POSITIVE, IF ERROR HAS BEEN DETECTED             *
C                                                                      *
C                                                                      *
C***********************************************************************
C                                                                      *
C                                                                      *
C                                                                      *
C***********************************************************************
C     COMMON BLOCK                                                     *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION  (A-H,O-Z)
C***********************************************************************
C     STORAGE ORGANIZATION AND DEFINITION OF INITIAL VALUES            *
C***********************************************************************
      DIMENSION ETA(4,NSPEC)
      DIMENSION W(NSPEC),X(NSPEC)
      DATA ZERO/0.0D0/,ONE/1.0D0/,HALF/0.5D0/
C***********************************************************************
C     CALCULATION OF MIXTURE VISCOSITY                                 *
C***********************************************************************
      TL=DLOG(DMAX1(10.0D0,T))
      SUM1=ZERO
      SUM2=ZERO
      DO 100 I=1,NSPEC
      XHELP = DEXP(((ETA(4,I)*TL+ETA(3,I))*TL+ETA(2,I))*TL+ETA(1,I))
      SUM1=SUM1+X(I)*XHELP
      SUM2=SUM2+X(I)/XHELP
  100 CONTINUE
      VISC =HALF*(SUM1+ONE/SUM2)
      IERR=0
      RETURN
C***********************************************************************
C     END OF -CALVIS-                                                  *
C***********************************************************************
      END
      SUBROUTINE CALHCP(NSPEC,W,T,HLHH,HSW,CPI,CP,H,IERR)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *  PROGR. FOR THE CALC. OF ENTHALPIES AND HEAT CAPACITIES   *    *
C     *                   AUTHOR: U.MAAS                          *    *
C     *                       03.20.1988                          *    *
C     *              LAST CHANGE: 15.03.1996/MAAS                 *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C
C         Note: This subroutine can handle molar and specific
C               properties.
C               In addition, if molar units are chosen, and
C               concentrations are used as input, then the heat capacity
C               per unit volume is returned
C
C     Input:
C
C     NSPEC           = number of species
C     W(NSPEC)        = mass fractions/mole fractions/concentrations
C     T               = temperature (K)
C     HLHH(7,NSPEC,2) = (specific/molar) NASA coefficients
C     HSW(3,I)        = temperature limits for fit
C
C
C     output :
C
C     CPI(NSPEC)      = (specific/molar) heat capacities of species
C     CP              = (specific/molar,volumetric) heat capacity
C     H(NSPEC)        = (specific/molar) enthalpies of species
C     IERR            = error flag, positive if error ocurred
C
C
C     NASA coefficients
C     HLHH(1,I,J)     = standard (specific/molar) heat capacity
C     HLHH(6,I,J)     = standard (specific/molar) enthalpy
C     HLHH(7,I,J)     = standard (specific/molar) entropy
C     HLHH(2-5,I,J)   = coefficients of polynomial fit for CPI
C
C                   J = 1 refers to low temperature fit
C                   J = 2 refers to high temperature fit
C
C     HSW(1,I)        = lower temperature limit
C     HSW(2,I)        = temperature for switch from low to high temp.
C     HSW(3,I)        = upper temperature limit
C
C***********************************************************************
C                                                                      *
C     NEW VERSION ACCOUNTS FOR DIFFERENT TEMPERATURE LIMITS OF FITS    *
C     TO GET THE OLD VERSION, SIMPLY COMMENT THE STATEMENT DESCRIBED   *
C     BELOW                                                            *
C                                                                      *
C***********************************************************************
C     STORAGE ORGANIZATION AND DEFINITION OF INITIAL VALUES            *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION HLHH(7,NSPEC,2),HSW(3,NSPEC)
      DIMENSION CPI(NSPEC),W(NSPEC),H(NSPEC)
      DATA FIVE/5.0D0/,FOUR/4.0D0/,THREE/3.0D0/,TWO/2.0D0/,ZERO/0.0D0/
      DATA ONE/1.0D0/,HALF/0.5D0/,XMAR/1.0D-1/
      IERR=0
C---- FOR OLD VERSION COMMENT THE FOLLOWING LINE
      GOTO 2000
C***********************************************************************
C                                                                      *
C     BLOCK FOR STANDARD CALULATION (HSW(2)=1000., HSW(3)=5000.)       *
C                                                                      *
C***********************************************************************
C     S(I)=((((HLHH(5,I,1)/FOUR )*T
C    1        +HLHH(4,I,1)/THREE)*T
C    1        +HLHH(3,I,1)/TWO)  *T
C    1        +HLHH(2,I,1)      )*T
C    1        +HLHH(1,I,1)*DLOG(T)
C    1        +HLHH(7,I,1)
C***********************************************************************
C     CALCULATION OF CPI, CP, AND H                                    *
C***********************************************************************
      TLIMI=1000.0D0
      CP=ZERO
      IF(T.LT.TLIMI) GOTO 100
      IF(T.LT.5.0D3) GOTO 200
      GOTO 300
C***********************************************************************
C     LOW TEMPERATURES  ( T < 1000 K )                                 *
C***********************************************************************
  100 CONTINUE
      DO 110 I=1,NSPEC
C---- ENTHALPY
      H(I)= ((((HLHH(5,I,1)*T/FIVE+HLHH(4,I,1)/FOUR)*T
     1      +HLHH(3,I,1)/THREE)*T
     1      +HLHH(2,I,1)/TWO)*T + HLHH(1,I,1))*T + HLHH(6,I,1)
C---- HEAT CAPACITY
      CPI(I)= (((HLHH(5,I,1)*T+HLHH(4,I,1))*T+HLHH(3,I,1))*T
     1           +HLHH(2,I,1))*T +HLHH(1,I,1)
  110 CONTINUE
      GOTO 4000
C***********************************************************************
C     HIGH TEMPERATURES ( 1000 K < T < 5000 K )                        *
C***********************************************************************
  200 CONTINUE
      DO 210 I=1,NSPEC
C---- ENTHALPY
      H(I)= ((((HLHH(5,I,2)*T/FIVE+HLHH(4,I,2)/FOUR)*T
     1      +HLHH(3,I,2)/THREE)*T
     1      +HLHH(2,I,2)/TWO)*T + HLHH(1,I,2))*T + HLHH(6,I,2)
C---- HEAT CAPACITY
      CPI(I)= (((HLHH(5,I,2)*T+HLHH(4,I,2))*T+HLHH(3,I,2))*T
     1            +HLHH(2,I,2))*T +HLHH(1,I,2)
  210 CONTINUE
      GOTO 4000
C***********************************************************************
C     VERY HIGH TEMPERATURES ( T > 5000 K )   CP= CP(5000K)            *
C***********************************************************************
  300 CONTINUE
      T5=5.0D3
      DO 310  I=1,NSPEC
C---- ENTHALPY
      H(I)= ((((HLHH(5,I,2)*T5/FIVE+HLHH(4,I,2)/FOUR)*T5
     1      +HLHH(3,I,2)/THREE)*T5
     1      +HLHH(2,I,2)/TWO)*T5 + HLHH(1,I,2))*T5 + HLHH(6,I,2)
C---- HEAT CAPACITY
      CPI(I)= (((HLHH(5,I,2)*T5+HLHH(4,I,2))*T5
     1          +HLHH(3,I,2))*T5+HLHH(2,I,2))*T5+HLHH(1,I,2)
      H(I)=H(I)+(T-T5)*CPI(I)
  310 CONTINUE
      GOTO 4000
C***********************************************************************
C                                                                      *
C     BLOCK FOR DETAILED CALCULATION (VARIABLE HSW)                    *
C                                                                      *
C***********************************************************************
 1000 CONTINUE
      CP= ZERO
C***********************************************************************
C                                                                      *
C***********************************************************************
      DO 1100 I=1,NSPEC
      IF(T.LT.HSW(1,I)) THEN
             J = 1
                 IERR = IERR + 1
      ELSE
      IF(T.LT.HSW(2,I)) THEN
             J = 1
      ELSE
      IF(T.LT.HSW(3,I)) THEN
             J = 2
      ELSE
             J = 2
                 IERR = IERR + 1
      ENDIF
      ENDIF
      ENDIF
C---- ENTHALPY
      H(I)= ((((HLHH(5,I,J)*T/FIVE+HLHH(4,I,J)/FOUR)*T
     1      +HLHH(3,I,J)/THREE)*T
     1      +HLHH(2,I,J)/TWO)*T + HLHH(1,I,J))*T + HLHH(6,I,J)
C---- HEAT CAPACITY
      CPI(I)= (((HLHH(5,I,J)*T+HLHH(4,I,J))*T+HLHH(3,I,J))*T+
     1             HLHH(2,I,J))*T +HLHH(1,I,J)
 1100 CONTINUE
      GOTO 4000
C**********************************************************************
C                                                                      *
C     BLOCK FOR DETAILED CALULATION (VARIABLE HSW)                     *
C                                                                      *
C***********************************************************************
 2000 CONTINUE
      CP= ZERO
C***********************************************************************
C                                                                      *
C***********************************************************************
      DO 2100 I=1,NSPEC
      IF(T.LT.HSW(1,I)) THEN
             J = 1
             IERR = IERR + 1
      ELSE
      IF(T.LT.HSW(2,I)) THEN
             J = 1
      ELSE
      IF(T.LT.HSW(3,I)) THEN
             J = 2
      ELSE
             J = 2
             IERR = IERR + 1
      ENDIF
      ENDIF
      ENDIF
      IF(-1.LT.0) THEN
        DELN  = (T - HSW(2,I)) / (XMAR * HSW(2,I))
        WEIGL = DMIN1(ONE,DMAX1(ZERO,HALF-DELN))
        WEIGH = ONE - WEIGL
      ELSE
        IF(T.LE.HSW(2,I)) THEN
          WEIGL = ONE
          WEIGH = ZERO
        ELSE
          WEIGL = ZERO
          WEIGH = ONE
        ENDIF
      ENDIF
C---- ENTHALPY
      H(I)= WEIGL *(((((HLHH(5,I,1)*T/FIVE+HLHH(4,I,1)/FOUR)*T
     1      +HLHH(3,I,1)/THREE)*T
     1      +HLHH(2,I,1)/TWO)*T + HLHH(1,I,1))*T + HLHH(6,I,1))
     1    + WEIGH *(((((HLHH(5,I,2)*T/FIVE+HLHH(4,I,2)/FOUR)*T
     1      +HLHH(3,I,2)/THREE)*T
     1      +HLHH(2,I,2)/TWO)*T + HLHH(1,I,2))*T + HLHH(6,I,2))
C---- HEAT CAPACITY
      CPI(I)= WEIGL * ((((HLHH(5,I,1)*T+HLHH(4,I,1))*T+HLHH(3,I,1))*T+
     1             HLHH(2,I,1))*T +HLHH(1,I,1))
     1      + WEIGH * ((((HLHH(5,I,2)*T+HLHH(4,I,2))*T+HLHH(3,I,2))*T+
     1             HLHH(2,I,2))*T +HLHH(1,I,2))
 2100 CONTINUE
      GOTO 4000
C***********************************************************************
C     CALCULATE MIXTURE HEAT CAPACITY                                  *
C***********************************************************************
 4000 CONTINUE
      DO I=1,NSPEC
        CP=CP+CPI(I)*W(I)
      ENDDO
      RETURN
C***********************************************************************
C     END OF -CALHCP-                                                  *
C***********************************************************************
      END
      SUBROUTINE CALMEC(NSPEC,NREAC,NM,NRINS,NRTAB,
     1     MATLL,NRVEC,PK,TEXP,EXA,VT4,VT5,VT6,IARRH,
     1     XZSTO,RATCO,CSUM,C,T,RATE,NSENS,ISENS,RPAR,
     1     LRW1,G,IPRI,MSGFIL,IERR,
     1     AINF,BINF,EINF,T3,T1,T2,AFA,BFA,HLHH,HINF)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *    PROGRAM FOR THE CALC. OF RATES OF CHEMICAL FORMATION   *    *
C     *    (WITH MECHANISM STORED IN SPARSE MATRIX STRUCTURE)     *    *
C     *                   AUTHOR: U.MAAS                          *    *
C     *                       12/08/1985                          *    *
C     *                   CHANGE: 12/08/1984 / MAAS               *    *
C     *                   CHANGE: 21/01/2003 / MAIWALD            *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C                                                                      *
C     INPUT  :                                                         *
C                                                                      *
C     NSPEC         = NUMBER OF SPECIES                                *
C     C(*)          = SPECIES CONCENTRATIONS, MOL/M**3 (NSPEC+NM)      *
C     T             = TEMPERATURE, K                                   *
C     WALL          = .TRUE. FOR ADDITIONAL WALL REACTIONS             *
C                                                                      *
C     OUTPUT :                                                         *
C                                                                      *
C     RATE(NSPEC)   = MOLAR SCALE RATES OF FORMATION, mol/M**3*S       *
C                                                                      *
C***********************************************************************
C                                                                      *
C***********************************************************************
C     COMMON BLOCKS                                                    *
C***********************************************************************
C***********************************************************************
C     STORAGE ORGANIZATION, INITIAL VALUES                             *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION MCON
      PARAMETER (ZERO=0.0D0)
      DIMENSION RATCO(NREAC,NRTAB),
     1     PK(NREAC),TEXP(NREAC),EXA(NREAC),XZSTO(NM,NSPEC),
     1     VT4(NREAC),VT5(NREAC),VT6(NREAC),
     1     AINF(NREAC),BINF(NREAC),EINF(NREAC),
     1     T3(NREAC),T1(NREAC),T2(NREAC),AFA(NREAC),BFA(NREAC)
      DIMENSION HLHH(7,NSPEC*2),HINF(3,NSPEC)
      DIMENSION C(*),RATE(NSPEC),IARRH(NREAC+1)
      DIMENSION MATLL(3,NREAC),NRVEC(3,NRINS),MAT(6)
      DIMENSION G(LRW1)
      DIMENSION RPAR(*),ISENS(*)
C---- BLOCKS FOR CALCULATION OF SPECIES PROPERTIES
      DATA RGAS/8.3143D0/
Cmms
C     write(*,*) '****####****mms#mms#mms#mms***###***'
C     write(*,*) 'Calmec wird aufgerufen ...'
C     write(*,*) 'IARRH ist gleich ',IARRH
C     write(*,*) '****####****mms#mms#mms#mms***###***'
Cmms-end
      IF(LRW1.LT.NREAC) GOTO 9100
C****************      IF(NM.LT.2) GOTO 4
C***********************************************************************
C     1. CALCULATION OF THE EFFICIENCIES OF THE COLLISION PARTNERS     *
C***********************************************************************
      MCON=ZERO
      NSNM=NSPEC+NM
      C(NSPEC+1)=CSUM
      IF(NM.LT.2) GOTO 4
      DO 3 I=2,NM
        CHEL=0.0D0
        DO 2 K=1,NSPEC
          CHEL=CHEL+C(K)*XZSTO(I,K)
    2   CONTINUE
        C(I+NSPEC)=CHEL
    3 CONTINUE
    4 CONTINUE
C***********************************************************************
C     2. CALCULATION OF DUMMY CONCENTRATION                            *
C***********************************************************************
      C(NSNM+1)=1.0D0
C>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>C
C     NEW VERSION (January 21, 2003)  by O. Maiwald                    C
C>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>C
C***********************************************************************
C     3. CALCULATE RATES OF FORWARD REACTION                           *
C***********************************************************************
      I=1
      DO WHILE (I .LE. NREAC)
C---- N1,N2,N3 = SPECIES OF FORWARD REACTION
        N1=MATLL(1,I)
        N2=MATLL(2,I)
        N3=MATLL(3,I)
C---- GAS PHASE REACTION  (IARRH(I)= 0 .OR. 3)
        IF(IARRH(I).EQ. 0 .OR. IARRH(I).EQ. 3) THEN
          HVAR=DMAX1((-EXA(I)/(RGAS*T)),-1.7D2)
          VORF = PK(I) * (T**TEXP(I)) * DEXP(HVAR)
        ELSE
C---- SURFACE REACTIONS  (IARRH(I)= 1)
          IF(IARRH(I).EQ.1) THEN
            CALL VTREAC(IARRH(I),PK(I),TEXP(I),EXA(I),VT4(I),VT5(I),
     1                VT6(I),T,VORF)
          ELSE
C---- THIRD BODY REACTIONS  (IARRH(I)= 2)
            IF(N1.GT.NSPEC) THEN
              MCON = C(N1)
            ELSE IF(N2.GT.NSPEC) THEN
              MCON = C(N2)
            ELSE IF(N3.GT.NSPEC) THEN
              MCON = C(N3)
            ELSE
              GOTO 9990
            ENDIF
            IORD=0
            DO K=1,3
               IF(MATLL(K,I) .NE. NSPEC+NM+1) IORD=IORD+1
            ENDDO
            IJAC = 0
            CALL FALLOFF(IJAC,PK(I),TEXP(I),EXA(I),AINF(I),
     1                   BINF(I),EINF(I),
     1                   T3(I),T1(I),T2(I),AFA(I),BFA(I),MCON,T,
     2                   VORFINF,PCOR,IORD,DVORDT,DVORDM,PCORDT,PCORDM)
            VORF=VORFINF*PCOR
          END IF
        ENDIF
C---- CALCULATE G(I) (OF FORWARD REACTION)
        G(I) = VORF  * C(N1)*C(N2)*C(N3)
C***********************************************************************
C     4. CALC. RATES OF REVERSE REACTION G(I+1) SIMULTANEOUS           *
C        (OPTION"REVSIM" IS CHOOSEN  (22/03/2002  by O. MAIWALD))      *
C***********************************************************************
        IF(IARRH(I) .EQ. 2 .OR. IARRH(I) .EQ. 3) THEN
C---- CHECK IF IARRH(I+1) OF REVERSE REACTION IS NEGATIVE (<0) (SHOULD
C---- BE FOR SIMULTANEOUS CALC. OF REVERSE REACTION RATE (see "HOMINP")
          IF(IARRH(I+1) .LT. ZERO) THEN
C---- CREATE MAT(N)
             DO N=1,6
                IF(N .LE. 3) MAT(N)=MATLL(N,I)
                IF(N .GE. 4) MAT(N)=MATLL(N-3,I+1)
             ENDDO
C---- N4,N5,N6 = SPECIES OF REVERSE REACTION
            N4=MATLL(1,I+1)
            N5=MATLL(2,I+1)
            N6=MATLL(3,I+1)
C---- REVERSE REACTION VELOCITY OF PRESSURE DEPENDENT REACTIONS
            IF(IARRH(I) .EQ. 2) THEN
              CALL REVREA(NSPEC,NREAC,I,MSGFIL,HLHH,HINF,
     1                    MAT,T,ECC,DECCDT)
              VORFREV=VORFINF/ECC
              VORFREV=VORFREV*PCOR
            ELSE
C---- REVERSE REACTION VELOCITY OF GAS PHASE REACTIONS
              CALL REVREA(NSPEC,NREAC,I,MSGFIL,HLHH,HINF,
     1                    MAT,T,ECC,DECCDT)
              VORFREV=VORF/ECC
            END IF
C---- CALCULATION OF REACTION VELOCITY G(I+1) FOR REVERSE REACTION
            I=I+1
C---- TRANSFORMATION OF REVERSE REACTION RATE TO CORRECT UNIT
            G(I) = VORFREV * C(N4)*C(N5)*C(N6)
          END IF
        END IF
        I=I+1
      END DO
C***********************************************************************
C     5. Multiply with RPAR for sensitivity analysis                   *
C***********************************************************************
 2500 CONTINUE
      IF(NSENS.NE.0) THEN
      DO 510 JP=1,NSENS
      NRCO=ISENS(JP)
      G(NRCO)=G(NRCO)*RPAR(JP)
  510 CONTINUE
      ENDIF
C***********************************************************************
C     6. CALCULATION OF THE REACTION RATES                             *
C***********************************************************************
      RATE(1:NSPEC)=0.0D0
C***********************************************************************
C     7. NORMAL COMPILER                                               *
C***********************************************************************
 1000 CONTINUE
      DO 1010 I=1,NRINS
      NR=NRVEC(2,I)
      NS=NRVEC(1,I)
      ND=NRVEC(3,I)
      RATE(NS)=RATE(NS)+ G(NR)*FLOAT(ND)
 1010 CONTINUE
C***********************************************************************
C     8. Output for mechanisms analysis                                *
C***********************************************************************
C     IF(IPRI.GT.0) THEN
C2101 FORMAT(7(E10.3,1X))
C2002 FORMAT('>MOLAR RATES (SI) RATE(I),I=1,NS')
C2003 FORMAT('>MOLAR RATES (SI) RATE(I),I=1,NREAC')
C---- INITIALIZE
C     WRITE(NFIMEC,2002)
C     WRITE(NFIMEC,2101) (RATE(J),J=1,NSPEC)
C     WRITE(NFIMEC,2003)
C     WRITE(NFIMEC,2101) (G(J),J=1,NREAC)
C     ENDIF
C***********************************************************************
C     9. CALCULATE MASS RATES                                          *
C***********************************************************************
 3000 CONTINUE
C     DO 20 J=1,NSPEC
C     RATE(J)=RATE(J)*XMOL(J)
C  20 CONTINUE
      IERR=0
C***********************************************************************
C     10. REGULAR EXITS                                                *
C***********************************************************************
      RETURN
C***********************************************************************
C     11. ERROR EXITS                                                  *
C***********************************************************************
 9100 IERR=1
      GOTO 9999
 9990 CONTINUE
      WRITE(MSGFIL,*) ' '
      WRITE(MSGFIL,9998) '          ERROR IN -CALMEC-            '
      WRITE(MSGFIL,9998) 'NO ENHANCED THIRD BODY EFFICIENCY FOUND'
      STOP
 9999 CONTINUE
      WRITE(MSGFIL,9998) '  ERROR IN -CALMEC-   '
      WRITE(MSGFIL,9998) '  ERROR IN -CALMEC-   '
 9998 FORMAT(3X,6('+'),6X,A39,6X,6('+'))
      STOP
C***********************************************************************
C     END OF -CALMEC-                                                  *
C***********************************************************************
      END
CNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
C     START OF NEW ROUTINES, THAT ARE WORKING, BUT HAVE NOT BEEN
C     CHECKED INTENSIVELY
CNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
      SUBROUTINE VTREAC(ITYP,VT1,VT2,VT3,VT4,VT5,VT6,T,XK)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *    PROGRAM FOR THE CALC. OF RATES OF CHEMICAL FORMATION   *
C     *               IN V-T-RELAXATION REACTIONS                 *
C     *                       10.03.1990 / RIEDEL                 *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C     VT1 - VT5     =  LANDAU TELLER PARAMETER
C     VT6           :  K(V-1,V) = K(V,V-1) * EXP ( -VT6/(RT) )
C                      K> = K(V --> V-1)
C     T             = TEMPERATURE, K
C
C     OUTPUT :
C
C     XK            = RATE COEFFICIENT OF REACTION
C
C***********************************************************************
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C
      PARAMETER ( XM13 = 1.D0/3.D0 )
      PARAMETER ( RGAS = 8.3143D0  )
C
      VT6H= VT6 * 1000.D0
      FT = VT5 * DSQRT(T)
      GT = VT1 * EXP (VT3 / T**XM13  + VT2 / SQRT(T) + VT4 / T)
      XK = FT * GT / (FT + GT )
C
      IF(ITYP.LT.0) XK = XK * DEXP(- VT6H / (RGAS*T)  )
      RETURN
C***********************************************************************
C     END OF -VTREAC-
C*********************************************************************
      END
C*********************************************************************
      SUBROUTINE HNYREAC(ITYP,VT1,VT2,VT3,VT4,VT5,VT6,T,XK)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *    PROGRAM FOR THE CALC. OF RATES OF CHEMICAL FORMATION   *
C     *             IN PHOTOCHEMICAL (H-NY) REACTIONS             *
C     *                       26.09.2007 / SCHERR                 *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C !!! INPUT  :
C !!!
C !!! VT1 - VT5     =  LANDAU TELLER PARAMETER
C !!! VT6           :  K(V-1,V) = K(V,V-1) * EXP ( -VT6/(RT) )
C !!!                  K> = K(V --> V-1)
C !!! T             = TEMPERATURE, K
C !!!
C !!! OUTPUT :
C !!!
C !!! XK            = RATE COEFFICIENT OF REACTION
C
C***********************************************************************
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C
      PARAMETER ( XM13 = 1.D0/3.D0 )
      PARAMETER ( RGAS = 8.3143D0  )
C
      XK = VT4 * VT1 * (T**(VT2)) * DEXP(- VT3 / (RGAS*T)  )
      RETURN
C***********************************************************************
C     END OF -HNYREAC-
C***********************************************************************
      END
C*********************************************************************
      SUBROUTINE CALT(NSPEC,H,WI,HLHH,HINF,HI,CPI,CP,T,MSGFIL,IERR)
C***********************************************************************
C
C    ***************************************************************
C    *                                                             *
C    *      GET TEMPERATURE FROM GIVEN ENTHALPY (NEWTON ITERATION) *
C    *                                                             *
C    *                                                             *
C    ***************************************************************
C
C***********************************************************************
C
C         Note: This subroutine can handle molar and specific
C               properties.
C               In addition, if molar units are chosen, and
C               concentrations are used as input, then the heat capacity
C               per unit volume is returned
C
C
C     Input:
C
C     NSPEC           = number of species
C     H               = (specific/molar) enthalpy
C     WI(NSPEC)       = (mass/mole) fractions
C
C
C     work arrays:
C
C     CPI(NSPEC)
C     HI(NSPEC)
C
C
C     NASA HLHH and HSW passed to subroutine CALHCP
C
C     OUTPUT :
C
C     T               = temperature
C     IERR            = error flag, positive if error ocurred
C
C***********************************************************************
C
C
C
C***********************************************************************
C     STORAGE ORGANIZATION AND DEFINITION OF INITIAL VALUES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION WI(NSPEC),CPI(NSPEC),HI(NSPEC),HLHH(*),HINF(*)
      DATA TGUESS/1700.D0/,ITMAX/60/,RTOL/1.D-7/
C***********************************************************************
C     INITIALIZATION
C***********************************************************************
      IERR=0
      TOLD =  TGUESS
      ITER = 0
C***********************************************************************
C     ITERATION LOOP
C***********************************************************************
   10 CONTINUE
      IF(ITER.GT.ITMAX) GOTO 910
C***********************************************************************
C     COMPUTE NOMAL ENTHALPY AND HEAT CAPACITY
C***********************************************************************
      CALL CALHCP(NSPEC,WI,TOLD,HLHH,HINF,CPI,CP,HI,IERR)
      HACT = 0.0D0
      DO 20 I=1,NSPEC
      HACT = HI(I) * WI(I) + HACT
   20 CONTINUE
      IF(IERR.GT.0) GOTO 920
C***********************************************************************
C     PERFORM NEWTON STEP
C***********************************************************************
      CORR  = (H - HACT) / CP
      TNEW = TOLD + CORR
C     WRITE(6,*) HACT,H,TOLD,TNEW, 'HACT,H,TOLD,TNEW'
      IF(DABS(CORR).LT.RTOL*TNEW) GOTO 60
      ITER=ITER+1
      TOLD = TNEW
      GOTO 10
C***********************************************************************
C     SOLUTION EXIT
C***********************************************************************
   60 CONTINUE
      T = TNEW
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
  910 CONTINUE
      WRITE(MSGFIL,911) TOLD,H,(WI(I),I=1,NSPEC)
  911 FORMAT(' ',' Newton iteration did not converge in CALT',/,
     1           ' TOLD = ',1PE11.4,', H = ',1PE11.4,/,
     1           ' wi = ',/,5(1PE11.4,2x))
      IERR=1
      GOTO 999
  920 CONTINUE
      WRITE(MSGFIL,921) TOLD,H,(WI(I),I=1,NSPEC)
  921 FORMAT(' ',' Calculation of enthalpies failed in CALT',/,
     1           ' TOLD = ',1PE11.4,', H = ',1PE17.10,/,
     1           ' wi = ',/,5(1PE11.4,2x))
      IERR=2
      GOTO 999
  999 CONTINUE
      T=TNEW
      RETURN
C***********************************************************************
C     END OF -CALT -
C***********************************************************************
      END
      SUBROUTINE ORASSP(IEQ,NEQ,SMF,NSPEC,T,C,CI,RHO,H,P,TD,IERR)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *    Compute physical variables from state space variables  *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C***********************************************************************
C
C
C   INFO > 0   calculate concentrations ond temp. from specific values
C
C      INFO = 1
C             Input:  SMF = (h,p,phi)
C             Output: T,CI,C,RHO,H,P
C
C      INFO = 2
C             Input:  SMF = (h,rho,phi)
C             Output: T,CI,C,P,RHO,H
C
C      INFO = 3
C             Input:  SMF = (h,p,w)
C             Output: T,CI,C,RHO,P,H
C
C      INFO = 4
C             Input:  SMF(h,rho,w)
C             Output: T,CI,C,P,RHO,H
C
C      INFO = 5
C             Input:  SMF = (T,p,phi)
C             Output: T,CI,C,RHO,H,P
C
C      INFO = 6
C             Input:  SMF = (T,rho,phi)
C             Output: T,CI,C,P,RHO,H
C
C      INFO = 7
C             Input:  SMF = (T,p,w)
C             Output: T,CI,C,RHO,P,H
C
C      INFO = 8
C             Input:  SMF(T,rho,w)
C             Output: T,CI,C,P,RHO,H
C
C
C
C   INFO < 0   calculate specific values from concentrations ond temp.
C
C      INFO = -1
C             Input:  T,CI
C             Output: C,RHO,H,P,SMF  SMF = (h,p,phi)
C
C      INFO = -2
C             Input:  T,CI
C             Output: C,RHO,H,P,SMF  SMF = (h,rho,phi)
C
C      INFO = -3
C             Input:  T,CI
C             Output: C,RHO,H,P,SMF  SMF = (h,rho,w)
C
C      INFO = -4
C             Input:  T,CI
C             Output: C,RHO,H,P,SMF  SMF = (h,rho,w)
C
C      phi denote specific mole numbers, w denote mass fractions
C
C     TD has to contain the thermodynamic properies:
C          molar masses: TD(1:NSPEC)
C          NASA coeff.   TD(NSPEC+1,NSPEC*15) molar units
C          NASA limits   TD(NSPEC*15+1:NSPEC*18
C          work array for CPI,HI: TD(NSPEC*18+1:NSPEC*20)
C
C***********************************************************************
C***********************************************************************
C***********************************************************************
C
C     CHAPTER I: STORAGE ORGANIZATION                                  *
C
C***********************************************************************
C***********************************************************************
C     I.I.: TYPES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      LOGICAL FOPT
C***********************************************************************
C     I.II.: DIMENSIONS
C***********************************************************************
      DIMENSION CI(NSPEC),SMF(*),TD(*)
C**********************************************************************C
C     INITIALIZE
C**********************************************************************C
      IERR = 0
      IXMOL = 1
      IHLHH = NSPEC+1
      IHINF = NSPEC*15+1
      IHI   = NSPEC*18+1
      ICPI  = NSPEC*19+1
C**********************************************************************C
C     call XCIHPW
C**********************************************************************C
      FOPT = .FALSE.
      IF(IEQ.EQ.1) THEN
        H = SMF(1)
        P = SMF(2)
        FOPT = . TRUE.
      ELSE IF(IEQ.EQ.2)  THEN
        H   = SMF(1)
        RHO = SMF(2)
        FOPT = . TRUE.
      ELSE IF(IEQ.EQ.3)  THEN
        H   = SMF(1)
        P   = SMF(2)
        FOPT = . TRUE.
      ELSE IF(IEQ.EQ.4)  THEN
        H   = SMF(1)
        RHO = SMF(2)
        FOPT = . TRUE.
      ELSE IF(IEQ.EQ.5)  THEN
        T = SMF(1)
        P = SMF(2)
        FOPT = . TRUE.
      ELSE IF(IEQ.EQ.6)  THEN
        T   = SMF(1)
        RHO = SMF(2)
        FOPT = . TRUE.
      ELSE IF(IEQ.EQ.7)  THEN
        T   = SMF(1)
        P   = SMF(2)
        FOPT = . TRUE.
      ELSE IF(IEQ.EQ.8)  THEN
        T   = SMF(1)
        RHO = SMF(2)
        FOPT = . TRUE.
      ENDIF
      CALL XCIHPW(IEQ,NSPEC,T,C,CI,RHO,H,P,SMF(3),
     1       TD(IXMOL),TD(IHLHH),TD(IHINF),TD(IHI),CP,TD(ICPI),IERR)
      IF(IEQ.EQ.-1) THEN
        SMF(1) = H
        SMF(2) = P
        FOPT = . TRUE.
      ELSE IF(IEQ.EQ.-2) THEN
        SMF(1) = H
        SMF(2) = RHO
        FOPT = . TRUE.
      ELSE IF(IEQ.EQ.-3) THEN
        SMF(1) = H
        SMF(2) = P
        FOPT = . TRUE.
      ELSE IF(IEQ.EQ.-4) THEN
        SMF(1) = H
        SMF(2) = RHO
        FOPT = . TRUE.
      ENDIF
        IF(.NOT.FOPT) GOTO  990
      RETURN
C***********************************************************************
C     Error exits
C***********************************************************************
  990 CONTINUE
      IERR = 9
      GOTO 999
  999 CONTINUE
      RETURN
C***********************************************************************
C     END OF -TRASSP-
C***********************************************************************
      END
      SUBROUTINE XCIHPW(INFO,NSPEC,T,C,CI,RHO,H,P,WMI,
     1                  XMOL,HLHH,HINF,HI,CP,CPI,IERR)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *    TRANSFORMATION OF THE THERMOCHEMICAL STATE SPACE       *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C***********************************************************************
C
C
C   INFO > 0   calculate concentrations ond temp. from specific values
C
C      INFO = 1
C             Input:  H,P,W        where W = specific mole numbers
C             Output: T,CI,C,RHO
C
C      INFO = 2
C             Input:  H,RHO,W      where W = specific mole numbers
C             Output: T,CI,C,P
C
C      INFO = 3
C             Input:  H,P,W        where W = mass fractions
C             Output: T,CI,C,RHO
C
C      INFO = 4
C             Input:  H,RHO,W        where W = mass fractions
C             Output: T,CI,C,P
C
C      INFO = 5
C             Input:  T,P,W        where W = specific mole numbers
C             Output: H,CI,C,RHO
C
C      INFO = 6
C             Input:  T,RHO,W      where W = specific mole numbers
C             Output: H,CI,C,P
C
C      INFO = 7
C             Input:  T,P,W        where W = mass fractions
C             Output: H,CI,C,RHO
C
C      INFO = 8
C             Input:  T,RHO,W        where W = mass fractions
C             Output: H,CI,C,P
C
C
C
C   INFO < 0   calculate specific values from concentrations ond temp.
C
C      INFO = -1
C             Input:  T,CI
C             Output: C,RHO,H,P,W  where W = specific mole numbers
C
C      INFO = -2
C             Input:  T,CI
C             Output: C,RHO,H,P,W  where W = specific mole numbers
C
C      INFO = -3
C             Input:  T,CI
C             Output: C,RHO,H,P,W  where W = mass fractions
C
C      INFO = -4
C             Input:  T,CI
C             Output: C,RHO,H,P,W  where W = mass fractions
C
C***********************************************************************
C***********************************************************************
C
C     CHAPTER I: STORAGE ORGANIZATION                                  *
C
C***********************************************************************
C***********************************************************************
C     I.I.: TYPES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      LOGICAL MASSFR,SPEMOL,TGIVEN,HGIVEN,PGIVEN,DGIVEN
C***********************************************************************
C     I.II.: DIMENSIONS
C***********************************************************************
      DIMENSION CI(NSPEC),WMI(NSPEC)
      DIMENSION CPI(NSPEC),HI(NSPEC),XMOL(NSPEC)
      DIMENSION HLHH(7,NSPEC,2),HINF(3,NSPEC)
C**********************************************************************C
C     I.V.: DATA STATEMENTS AND COMMON BLOCKS
C**********************************************************************C
      PARAMETER (ZERO=0.0D0,ONE=1.0D0)
      DATA NFILE6/06/
      DATA RGAS/8.3143D0/
C***********************************************************************
C
C     CHAPTER II: INITIALIZATION                                       *
C
C***********************************************************************
      IERR = 0
      SPEMOL = (INFO.EQ.1).OR.(INFO.EQ.2).OR.(INFO.EQ.5).OR.(INFO.EQ.6)
      MASSFR = (INFO.EQ.3).OR.(INFO.EQ.4).OR.(INFO.EQ.7).OR.(INFO.EQ.8)
      HGIVEN = (INFO.EQ.1).OR.(INFO.EQ.2).OR.(INFO.EQ.3).OR.(INFO.EQ.4)
      TGIVEN = (INFO.EQ.5).OR.(INFO.EQ.6).OR.(INFO.EQ.7).OR.(INFO.EQ.8)
      PGIVEN = (INFO.EQ.1).OR.(INFO.EQ.3).OR.(INFO.EQ.5).OR.(INFO.EQ.7)
      DGIVEN = (INFO.EQ.2).OR.(INFO.EQ.4).OR.(INFO.EQ.6).OR.(INFO.EQ.8)
      IF(INFO.LT. 0) GOTO 100
      IF(INFO.GT. 0) GOTO 200
      RETURN
C***********************************************************************
C                                                                      *
C     CHAPTER II: TRANSFORM FROM T,CI INTO H,P,W/M COORDINATES         *
C                                                                      *
C***********************************************************************
  100 CONTINUE
C***********************************************************************
C     CALCULATE TOTAL CONCENTRATION
C***********************************************************************
      C  = ZERO
      DO 120 I=1,NSPEC
      C  =   C + CI(I)
  120 CONTINUE
C***********************************************************************
C     Calculate Pressure                                               *
C***********************************************************************
      P = C * RGAS * T
C***********************************************************************
C     CALCULATE MOLAR ENTHALPY * C                                  *
C***********************************************************************
      CALL CALHCP(NSPEC,CI,T,HLHH,HINF,CPI,CPDEN,HI,IEHCP)
      HDEN = DDOT(NSPEC,CI,1,HI,1)
      IF(IEHCP.GT.0) GOTO 900
C---- HDEN, CPDEN ARE THE ENTHALPY AND HEAT CAPACITY VER VOLUME
C***********************************************************************
C     CALCULATE MEAN MOLAR MASS                                        *
C***********************************************************************
C---- RHO  = XMOLM * C
      RHO  =ZERO
      DO 140 I=1,NSPEC
      RHO  = RHO +CI(I)*XMOL(I)
  140 CONTINUE
C***********************************************************************
C     CALCULATE SPECIFIC MOLE NUMBERS                                  *
C***********************************************************************
      IF(INFO.EQ.-1.OR.INFO.EQ.-2.OR.INFO.EQ.-5.OR.INFO.EQ.-6) THEN
      DO 150 I=1,NSPEC
      WMI(I)  = CI(I)/RHO
  150 CONTINUE
      ELSEIF(INFO.EQ.-3.OR.INFO.EQ.-4.OR.INFO.EQ.-7.OR.INFO.EQ.-8) THEN
      DO 160 I=1,NSPEC
      WMI(I)  = XMOL(I)*CI(I)/RHO
  160 CONTINUE
      ELSE
        GOTO 990
      ENDIF
      H     = HDEN / RHO
      CP    = CPDEN / RHO
C***********************************************************************
C
C***********************************************************************
      GOTO 700
C***********************************************************************
C                                                                      *
C     CHAPTER II: TRANSFORM FROM H,P,W/M Into t,ci - COORDINATES       *
C                                                                      *
C***********************************************************************
  200 CONTINUE
C***********************************************************************
C     CALCULATE MEAN MOLAR MASS AND MOLE FRACTIONS                     *
C***********************************************************************
C---- COMPUTE MEAN MOLAR MASS
      XMOLM=ZERO
      IF(SPEMOL) THEN
      DO 210 I=1,NSPEC
      XMOLM=XMOLM+WMI(I)
  210 CONTINUE
      ELSE IF(MASSFR) THEN
      DO 215 I=1,NSPEC
      XMOLM=XMOLM+WMI(I)/XMOL(I)
  215 CONTINUE
      ELSE
        GOTO 990
       ENDIF
      XMOLM=ONE/XMOLM
C---- STORE MOLE FRACTIONS IN CI
      IF(SPEMOL) THEN
      DO 220 I=1,NSPEC
      CI(I)= WMI(I)*XMOLM
  220 CONTINUE
      ELSE IF(MASSFR) THEN
      DO 225 I=1,NSPEC
      CI(I)= WMI(I)*XMOLM/XMOL(I)
  225 CONTINUE
      ELSE
        GOTO 990
      ENDIF
C***********************************************************************
C     Calculate Temperature                                            *
C***********************************************************************
      IF(HGIVEN) THEN
C---- NOTE: CI ARE MOLE FRACTIONS RIGHT NOW
      HMOL=H * XMOLM
      IFA = 0
      IEHCP= 0
  209 IFA=IFA+1
      CALL CALT(NSPEC,HMOL,CI,HLHH,HINF,HI,CPI,CPMOL,T,NFILE6,IEHCP)
      CP  = CPMOL / XMOLM
      IF(IEHCP.GT.0.AND.IFA.EQ.1) THEN
        IEHCP = 1
C***********************************************************************
C A. Neagos                                                            *
C Error flag für xcihpw auf 1 setzen, damit REDIM Iteration abbricht,  *
C falls Enthalpie nicht berechnet werden kann.
C***********************************************************************
        IERR = 1
        GOTO 209
      ENDIF
      IF(IEHCP.GT.0) T = 0.1D0
      ELSE IF(TGIVEN) THEN
        CALL CALHCP(NSPEC,CI,T,HLHH,HINF,CPI,CPMOL,HI,IEHCP)
        HMOL = DDOT(NSPEC,CI,1,HI,1)
        H   = HMOL / XMOLM
        CP  = CPMOL / XMOLM
      ELSE
        GOTO 990
      ENDIF
C***********************************************************************
C     Calculate Pressure and Total Concentration                       *
C***********************************************************************
      IF(PGIVEN) THEN
        C = P  / (RGAS*T)
        RHO= C*XMOLM
      ELSE IF(DGIVEN) THEN
        C = RHO / XMOLM
        P = C * RGAS * T
      ELSE
        GOTO 990
      ENDIF
C***********************************************************************
C     Calculate Concentrations                                         *
C***********************************************************************
      DO 230 I=1,NSPEC
      CI(I)=CI(I)*C
  230 CONTINUE
C***********************************************************************
C     EXIT                                                             *
C***********************************************************************
      IF(IEHCP.GT.0) GOTO 900
      GOTO 700
C***********************************************************************
C     REgular exit
C***********************************************************************
  700 CONTINUE
      RETURN
C***********************************************************************
C     CHAPTER IV: ABNORMAL END
C***********************************************************************
  900 CONTINUE
      IERR = 1
      GOTO 999
  990 CONTINUE
      IERR = 9
      GOTO 999
  999 CONTINUE
      RETURN
C***********************************************************************
C     END OF -XCIHPW-
C***********************************************************************
      END
      SUBROUTINE DIFMA(INFO,NSPEC,
     1     P,T,D,W,X,XMOLM,CP,HI,XLA,VIS,
     1     DSS,DSE,DSP,DES,DEE,DEP,LRW,RW,IERR,
     1     XMOL,LRSPEC,RSPEC,NEPS,NSIG,NHL,NHSW,
     1     NETA,NLCOEF,NLCOEM,NLCOED,NDCOEF,IDIA)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *   CALCULATION OF TRANSPORT COEFFICIENTS AT ONE POSITION   *
C     *                                                           *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C     Input parameters:
C
C       NSPEC       number of species
C
C       P                      pressure
C       T                      temperature
C       D                      density
C       CP                     specific heat at constant pressure
C       W(NSPEC)               mass fractions
C       X(NSPEC)               mole fractions
C       XMOLM                  mean molar mass
C       HI(NSPEC)              specific enthalpies
C       LRW                    length of real work array (>3NSPEC)
C
C     Work arrays
C       RW(LRW)
C
C     Output Paramaters:
C
C       entries of diffusion matrices
C
C       DSS(NSPEC,NSPEC)   DSE(NSPEC)   DSP(NSPEC)
C       DES(NSPEC)         DEE          DEP
C
C       such that
C
C         j    = DSS * grad(x_i) + DSE * grad(T) + DSP * grad(p)
C
C         j_q  = DES * grad(x_i) + DEE * grad(T) + DEP * grad(p)
C
C
C
C***********************************************************************
C***********************************************************************
C
C     CHAPTER A: STORAGE ORGANIZATION, STATEMENT FUNCTIONS
C
C***********************************************************************
C
C***********************************************************************
C     I.: TYPES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      PARAMETER (ZERO = 0.0D0, ONE = 1.0D0)
      LOGICAL TEST,VISCO,SORET,PREDIF,HEATCO,DUFOUR,EQDIF,LEWIS1
C***********************************************************************
C     II.: DIMENSIONS
C***********************************************************************
      CHARACTER(LEN=16), DIMENSION(NSPEC) :: SYMB
      DIMENSION W(NSPEC),X(NSPEC),HI(NSPEC)
      DIMENSION DSS(NSPEC,NSPEC),DES(NSPEC),DSE(NSPEC),DSP(NSPEC)
      DIMENSION RSPEC(LRSPEC),XMOL(NSPEC),RW(LRW)
      DIMENSION HELP(NSPEC)
C***********************************************************************
C     IV.: DATA STATEMENTS
C***********************************************************************
      DATA MSGFIL/06/
C***********************************************************************
C     Check and assign work array
C***********************************************************************
      SYMB(1:NSPEC) = ' '
      IDDF   = 1
      IETADI = IDDF   + NSPEC
      ICHI   = IETADI + NSPEC
      INEW   = ICHI   + NSPEC
      IF(INEW-1.GT.LRW) GOTO 900
C***********************************************************************
C
C     Initialization
C
C***********************************************************************
      TEST = .FALSE.
      IERR=0
      XTDF = 1.0D10
      PREDIF = .FALSE.
      DUFOUR = .FALSE.
      IF(INFO.EQ.0) THEN
        SORET    = .TRUE.
          XTDF   = 15.D-3
        EQDIF    = .FALSE.
        LEWIS1   = .FALSE.
      ELSE IF(INFO.EQ.1) THEN
        SORET    = .TRUE.
        EQDIF    = .FALSE.
        LEWIS1   = .FALSE.
      ELSE IF(INFO.EQ.2) THEN
        SORET    = .FALSE.
        EQDIF    = .FALSE.
        LEWIS1   = .FALSE.
      ELSE IF(INFO.EQ.3) THEN
        SORET    = .FALSE.
        EQDIF    = .TRUE.
        LEWIS1   = .FALSE.
      ELSE IF(INFO.EQ.4) THEN
        SORET    = .FALSE.
        EQDIF    = .TRUE.
        LEWIS1   = .TRUE.
      ELSE
        GOTO 910
      ENDIF
      HEATCO = .TRUE.
      VISCO  = .TRUE.
C***********************************************************************
C
C
C     Calculation of transport coefficients
C
C
C***********************************************************************
C***********************************************************************
C     Coefficient XLA for heat conduction
C***********************************************************************
      IF(HEATCO) CALL CALLAM(NSPEC,W,X,T,XMOL,RSPEC(NSIG),RSPEC(NEPS),
     1         RSPEC(NLCOEF),RSPEC(NLCOEM),RSPEC(NLCOED),SYMB,XLA,IERR)
C***********************************************************************
C     Coefficient VIS for viscosity
C***********************************************************************
      VIS=ZERO
      IF(VISCO) CALL CALVIS(NSPEC,W,X,T,RSPEC(NETA),VIS,IERR)
C***********************************************************************
C     Coefficients for ordinary diffusion
C***********************************************************************
C---- DSS used as a work array
      IF(.NOT.LEWIS1)
     1      CALL CALDIF(NSPEC,W,X,T,P,RSPEC(NDCOEF),RW(IDDF),DSS,IERR)
      IF(EQDIF) THEN
        IF(LEWIS1) THEN
          D1 = XLA / (D*CP)
        ELSE
          D1 = DDOT(NSPEC,X,1,RW(IDDF),1)
        ENDIF
        DO 205 I=1,NSPEC
          RW(IDDF-1+I)= D1
  205   CONTINUE
      ENDIF
C************************************************************************
C A. Neagos:
C Für den Fall Le=1:
C Bisher wurde hier die diagonale Diffusionsmatrix DSS mit den identi-
C schen Einträgen (lambda/(rho*cp)) mit dw/dx multipliziert. Die Massen-
C stromdichte ergibt sich somit zu j=DSS*dw/dx*grad(x). Die Formulierung
C dieses Terms mit den Gradienten spezifischer Molzahlen führt auf
C j=DSS*dw/dx*dx/dPsi*grad(Psi). dx/dPsi wird in der subroutine TRASSP
C berechnet. Da jedoch dw/dx*dx/dPsi = dw/dPsi ist, kann j in folgender
C Weise geschrieben werden: j=DSS*dw/dPsi*grad(Psi). Somit kann DSS an
C dieser Stelle als Diadonalmatrix D=lambda/(rho*cp) belassen werden.
C************************************************************************
      IF(IDIA.EQ.1.AND.EQDIF.AND.LEWIS1)THEN
         CALL DIAMAT(NSPEC,RW(IDDF),DSS,NSPEC)
      ELSE
      DO 210 I=1,NSPEC
          RW(IDDF-1+I)=(RW(IDDF-1+I) * XMOL(I)  / XMOLM )
  210 CONTINUE
C---- calculate flux diffusion matrix
      CALL DIAMAT(NSPEC,RW(IDDF),DSS,NSPEC)
C---- premultiply with P = I - w*e^T
      SCA  = -ONE/SUM(W)
      CALL DGER(NSPEC,NSPEC,SCA,W,1,RW(IDDF),1,DSS,NSPEC)
C-    up to now DSS is already - dss / rho
C---- postmultiply with P = I - w*e^T
      HELP = SCA*MATMUL(DSS,W)
      DO J=1,NSPEC
      DSS(1:NSPEC,J) = DSS(1:NSPEC,J) + HELP
      ENDDO
      ENDIF
C***********************************************************************
C     Coefficients CHI for thermal diffusion
C***********************************************************************
      IF(SORET) THEN
      CALL CALTHD(NSPEC,T,XMOLM,RSPEC(NEPS),XMOL,W,X,XTDF,RW(ICHI),IERR)
      ENDIF
C***********************************************************************
C
C
C     Calculation of transport matrices in original formulation
C
C
C***********************************************************************
C
C     j_q  = DES * grad(x_i) + DEE * grad(T) + DEP * grad(p)
C
C     j    = DSS * grad(x_i) + DSE * grad(T) + DSP * grad(p)
C
C***********************************************************************
C
C     Calculation of the species diffusion matrix
C
C***********************************************************************
C***********************************************************************
C     ordinary diffusion
C***********************************************************************
      DO 310 I=1,NSPEC
C--- scale only with d because dss had been multiplied with w_i
      SCF  = -  D
      CALL DSCAL(NSPEC,SCF,DSS(I,1),NSPEC)
  310 CONTINUE
C***********************************************************************
C     thermal diffusion
C***********************************************************************
C---- DSE = (1/T) * DSS * CHI
      IF(SORET) THEN
        TINV = ONE / T
        CALL DGEMV('N',NSPEC,NSPEC,TINV,DSS,NSPEC,RW(ICHI),1,ZERO,DSE,1)
      ELSE
         DSE = ZERO
      ENDIF
C***********************************************************************
C     pressure diffusion
C***********************************************************************
      IF(PREDIF) THEN
        WRITE(*,*) ' pressure diffusion not yet implemented '
        STOP
      ELSE
         DSP = ZERO
      ENDIF
C***********************************************************************
C
C     Calculation of the matrix partition for energy
C
C***********************************************************************
C***********************************************************************
C     calculate ETADI = eta * rho_i^{-1}
C***********************************************************************
C---- term P * chi (dufour effect) not yet included
      CALL DCOPY(NSPEC,HI,1,RW(IETADI),1)
      IF(DUFOUR) THEN
        WRITE(*,*) ' dufour effect not yet implemented '
        STOP
      ENDIF
C***********************************************************************
C     diffusion
C***********************************************************************
      CALL DGEMV ('T',NSPEC,NSPEC,ONE,DSS,NSPEC,RW(IETADI),1,ZERO,DES,1)
C***********************************************************************
C     heat conduction
C***********************************************************************
      DEE = - XLA +  DDOT(NSPEC,RW(IETADI),1,DSE,1)
C***********************************************************************
C     pressure diffusion
C***********************************************************************
      IF(PREDIF) THEN
        DEP = DDOT(NSPEC,RW(IETADI),1,DSP,1)
      ELSE
        DEP = ZERO
      ENDIF
C***********************************************************************
C
C***********************************************************************
      IF(TEST) THEN
      WRITE(MSGFIL,8110) T,P,D,XMOLM
      WRITE(MSGFIL,8120) (W(I),I=1,NSPEC)
      WRITE(MSGFIL,8140) (X(I),I=1,NSPEC)
      WRITE(MSGFIL,8150) (RW(IDDF-1+I),I=1,NSPEC)
      WRITE(MSGFIL,8190) XLA,VIS
      ENDIF
C***********************************************************************
C     REGULAR EXIT
C***********************************************************************
      RETURN
C***********************************************************************
C     Error exits
C***********************************************************************
  900 CONTINUE
      WRITE(MSGFIL,901) LRW,3*NSPEC
  901 FORMAT(' supplied work array (LRW=',I4,') smaller than needed',
     1       ' array (',I4,')')
      GOTO 999
  910 CONTINUE
      WRITE(MSGFIL,*) 'INFO=',INFO,' not yet available in DIFMAT'
      GOTO 999
  999 CONTINUE
      WRITE(MSGFIL,*) 'Error in DIFMA'
      STOP
C***********************************************************************
C     FORMAT STATEMENTS
C***********************************************************************
 8110 FORMAT(1H0,'T,P,RHO,XMOLM',/,1(3X,4(1PE9.2,2X)))
 8120 FORMAT(1H0,'WL                 ',/,1(3X,6(1PE9.2,2X)))
 8140 FORMAT(1H0,'X              ',/,1(3X,6(1PE9.2,2X)))
 8150 FORMAT(1H0,'DDF            ',/,1(3X,6(1PE9.2,2X)))
 8190 FORMAT(1H0,'XLA,VISC       ',/,1(3X,6(1PE9.2,2X)))
C***********************************************************************
C     END OF -DIFMAT-
C***********************************************************************
      END
      SUBROUTINE TRADIF(NPDE,NSPEC,S,
     1     DSS,DSE,DSP,DES,DEE,DEP,DXDS,DTDS,DMATS,DMATE,IERR)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *   Transform diffusion matrix for dependent variables      *
C     *                                                           *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C     Input parameters:
C
C       NPDE        number of partial differential equations
C       NSPEC       number of species
C
C       DXDS(NSPEC,NPDE)       d(x) / d(s)
C       DTDS(NPDE)             d(T) / d(s)
C
C       entries of diffusion matrices
C
C       DSS(NSPEC,NSPEC)   DSE(NSPEC)   DSP(NSPEC)
C       DES(NSPEC)         DEE          DEP
C
C       such that
C
C         j    = DSS * grad(x_i) + DSE * grad(T) + DSP * grad(p)
C
C         j_q  = DES * grad(x_i) + DEE * grad(T) + DEP * grad(p)
C
C
C
C     Output parameters:
C
C
C       entries of diffusion matrices
C
C       DMATS(NSPEC,NPDE)  DMATE(NPDE)
C
C       such that
C
C            j    = DMATS * grads
C
C            j_q  = DMATE * grad(s)
C
C
C***********************************************************************
C***********************************************************************
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      PARAMETER (ZERO = 0.0D0, ONE = 1.0D0)
      DIMENSION S(NPDE)
      DIMENSION DXDS(NSPEC,NPDE),DTDS(NPDE)
      DIMENSION DSS(NSPEC*NSPEC),DES(NSPEC),DSE(NSPEC),DSP(NSPEC)
      DIMENSION DMATS(NSPEC,NPDE),DMATE(NPDE)
C***********************************************************************
C
C     Initialization
C
C***********************************************************************
      IERR=0
C***********************************************************************
C
C     transformation into conserved variables
C
C***********************************************************************
C***********************************************************************
C     species
C***********************************************************************
      CALL DGEMM('N','N',NSPEC,NPDE,NSPEC,
     1     ONE,DSS,NSPEC,DXDS,NSPEC,ZERO,DMATS,NSPEC)
      CALL DGEMM('N','N',NSPEC,NPDE ,    1,
     1     ONE,DSE,NSPEC,DTDS,    1,ONE ,DMATS,NSPEC)
C***********************************************************************
C     energy
C***********************************************************************
      CALL DGEMV ('T',NSPEC,NPDE,ONE,DXDS,NSPEC,DES,1,ZERO,DMATE,1)
      CALL DAXPY(NPDE,DEE,DTDS,1,DMATE,1)
C***********************************************************************
C     REGULAR EXIT
C***********************************************************************
      RETURN
C***********************************************************************
C     END OF -TRADIF-
C***********************************************************************
      END
      SUBROUTINE CALTHD(NSPEC,T,XMOLM,EPS,XMOL,W,X,XMMAX,CHI,IERR)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *    CALCULATION OF THE THERMAL DIFFUSION COEFFICIENTS      *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C                                                                      *
C     INPUT  :                                                         *
C                                                                      *
C     NSPEC        = NUMBER OF ACTUAL SPECIES                          *
C     T            = TEMPERATURE, K                                    *
C     C            = TOTAL CONCENTRATION, MOL/M**3                     *
C     XMOLM        = MEAN MOLAR MASS, KG/MOL                           *
C     EPS(NSPEC)   = L-J PARAMETERS EPS/K IN KELVIN                    *
C     XMOL(NSPEC)  = MOLAR MASSES IN KG/MOLE                           *
C     W(NSPEC)     = MASS FRACTIONS, - (NSPEC)                         *
C     XMMAX        = MAXIMUM MOLAR MASS FOR SPECIES TO BE CONSIDERED   *
C                                                                      *
C     OUTPUT :                                                         *
C                                                                      *
C     CHI(NSPEC) =  k_i^T * (1-w_i)/(1-x_i)
C
C
C                                                                      *
C***********************************************************************
C                                                                      *
C                                                                      *
C                                                                      *
C***********************************************************************
C     STORAGE ORGANIZATION AND DEFINITION OF INITIAL VALUES            *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      PARAMETER (ZERO=0.0D0,ONE=1.0D0)
      DIMENSION EPS(NSPEC)
      DIMENSION XMOL(NSPEC),W(NSPEC),X(NSPEC),CHI(NSPEC)
C***********************************************************************
C
C     CALCULATION OF THERMAL DIFFUSION RATIOS                          *
C
C***********************************************************************
      DO 3000 I=1,NSPEC
C***********************************************************************
C     calculate dimensionless thermal diffusion ratio
C***********************************************************************
      IF(XMOL(I).LE.XMMAX) THEN
        THDRAT = ZERO
        DO 2500 J=1,NSPEC
          TST=T/(SQRT(EPS(I)*EPS(J)))
          IF(TST.LE.1.0D0) THEN
            RT= 0.0D0
          ELSE
            IF(TST.LE.1.7D0) THEN
              RT=0.015D0+(TST-1.0D0)*0.300D0
            ELSE
              IF(TST.LE.2.5D0) THEN
                RT=0.225D0+(TST-1.7D0)*0.188D0
              ELSE
                IF(TST.LE.4.0D0) THEN
                  RT=0.375D0+(TST-2.5D0)*0.097D0
                ELSE
                  IF(TST.LE.6.5D0) THEN
                     RT=0.520D0+(TST-4.0D0)*0.0264D0
                  ELSE
                     RT=0.60D0
                  ENDIF
                ENDIF
              ENDIF
            ENDIF
          ENDIF
      THDRAT=THDRAT+RT*(XMOL(I)-XMOL(J))*X(I)*X(J)/(XMOL(I)+ XMOL(J))
 2500   CONTINUE
      THDRAT = THDRAT * 0.5D0
C---- THDRAT = k_i^T  = dimensionless thermodiffusion ratio
C***********************************************************************
C     calculate dimensionless thermal diffusion ratio
C***********************************************************************
C---- AVOID DIVISION BY ZERO
      IF(DABS(1.0D0-X(I)).LT.1.D-10) THEN
        CHI(I) = ZERO
      ELSE
        CHI(I)= THDRAT  * (1.0D0-W(I))/(1.0D0-X(I))
      ENDIF
C***********************************************************************
C     species not considered for thermal diffusion
C***********************************************************************
      ELSE
         CHI(I) = ZERO
      ENDIF
 3000 CONTINUE
      IERR=0
      RETURN
C***********************************************************************
C     END OF -CALTHD-                                                  *
C***********************************************************************
      END
      SUBROUTINE CALSME(NSREAC,NSPEC,XMOL,NSSPEC,T,SITE,COV,CIL,SKDOT,
     1    XCONC,RKF,RATL,FCOV,PK,TEXP,EXA,VT5,XEXA,
     1    NRINSS,IREACP,MATLLS,ISTICK,IARRH,INUMC,
     1    NSENS,ISENS,RPAR,IPAR,INFOM)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *       CALCULATION OF SURFACE REACTION RATE                *
C     *               OUTPUT OF ADSORBED/DESORBED SPECIES         *
C     *               BY SURFACE REACTIONS                        *
C     *                      27.08.1993                           *
C     *              LAST CHANGE: 18.05.97/XF.YAN                 *
C     *                                                           *
C     *************************************************************
C***********************************************************************
C     DATA IN  SI -SYSTEM (IF NOT SPECIFIED)
C     0. DESCCRIPTION OF PARAMETERS                                    *
C***********************************************************************
C     NSSPEC      NUMBER OF SURFACE  SPECIES
C     NSPEC       NUMBER OF GAS PHASE SPECIES
C     NS          NSSPEC+NSPEC
C
C     NSREAC     = NUMBER OF SURFACE  REACTIONS                        *
C     NPOINT     = NUMBER OF REACTION POINTERS                         *
C     NPDE       = NUMBER OF VARIABLRS (NT+NS) IN  GAS PHASE           *
C     SITE       =  SITE DENSITY (MOL/M**2)                            *
C     XMOL(*)    = MOLAR MASS (KG/MOL)
C
C     T           GAS TEMPERATURE AT LAST GRIDPOINT
C
C     COV(NSSPEC) COVERAGE
C     FCOV(NSSPEC) RESIDUEN FOR COVERAGE
C
C     CIL(NSPEC)  GAS PHASE SPECIES CONCENTRATIONS AT LAST GRID POINT
C                     (MOL/M**3)
C     RATL(NSPEC) GAS PHASE SPECIES PRODUCTION RATE BY SURFACE REACTIONS
C                     (KG/M**2/S)
C                 (beachte unterschied zu INRCRE in einheiten)
C     XCONC(*)    MOLAR CONCENTRATION (MOL/M**3,MOL/M**2)
C     SKDOT(*)    CHEMICAL PRODUCTION RATE OF SURFACE REACTIONS
C                   (MOL/M**2/S)
C     IREAC(NIARRH+2*NREAC+NGREAC-1+I) = FLAG IF STICKING COEFFICIENT  *
C                                               IS GIVEN (0=NO,1=YES)  *
C                    						       *
C     VALUE(I)   =  I=1,NSSPEC  SURFACE SPECIES VALUES                 *
C                   I=NSSPEC+1,NS      GAS PHASE SPECIES VALUES        *
C                                                                      *
C     DATA FOR SURFACE REACTION:                                       *
C     MAT(I,NSREAC) = SPECIES NUMBER I IN NSREAC REACTION              *
C     RREAC(NPK+NGREAC-1+I)  = PREEXPONENT. FACTOR                     *
C     RREAC(NTEXP+NGREAC-1+I)= TEMPERATUR EXPONENT                     *
C     RREAC(NEXA+NGREAC-1+I) = ACTIVATION ENERGY                       *
C     RREAC(NSRVEC-1+(J-1)*3+I) = REACTION POINTERS                    *
C                     (I,1)  =  SPECIES NUMBER                         *
C                     (I,2)  =  REACTION NUMBER                        *
C                     (I,3)  =  STOICHIOMETRIC COEFFICIENT             *
C     IREAC(NSTICK-1+I)      =  DENOTE THE PROCESS OF ADSORPTION       *
C     RKF(NSREAC)   = RATE CONSTANT
C                                                                      *
C     INPUT:  T,COV,CIL,IREAC,RPAR,IPAR
C     OUTPUT: SKDOT,RATL,FCOV
C     Change report:
C     The variable names in this suroutine are modified to fit the
C     main program                                  ---XFY
C
C     zu optimieren:
C     RKF(I) wie RATE in globalem Array RREAC speichern ?
C     Getestet: XCONC,RKF,IREACP,IREAC,IR,IMAT. Die Rechnung mit RKF und
C     SKDOT stimmt mit OD, wenn die XCONC bei beiden gleich sind.
C     SKDOT evtl. noch f"ur h"ohere Konzentrationen zu testen.     --XFY
C***********************************************************************
C     STORAGE ORGANIZATION, INITIAL VALUES
C***********************************************************************
C     I. STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL TEST
CEMERG
      PARAMETER(PI=3.141593D0)
C***********************************************************************
C     BLOCK FOR REACTION MECHANISM DATA
C***********************************************************************
C---- NSSPEC
      DIMENSION COV(NSSPEC),FCOV(NSSPEC),CIL(NSPEC)
C---- NSPEC
      DIMENSION RATL(NSPEC),XMOL(NSPEC)
C---- NSPEC+NSSPEC
      DIMENSION SKDOT(*),XCONC(*)
C---- NSREAC
      DIMENSION ISTICK(NSREAC),INUMC(NSREAC),
     1    PK(NSREAC),TEXP(NSREAC),EXA(NSREAC),VT5(NSREAC),XEXA(NSREAC),
     1    IARRH(NSREAC),MATLLS(6,NSREAC),ISENS(NSENS),RKF(NSREAC)
C---- NRINSS
      DIMENSION IREACP(3,NRINSS)
C---- others
      DIMENSION RPAR(*),IPAR(*)
C***********************************************************************
C     BLOCK FOR OPTIONS, OTHER PARAMETERS
C***********************************************************************
      DATA RGAS/8.3143D0/
      DATA NFIMEC/10/
C***********************************************************************
C     II. INITIAL VALUES
C***********************************************************************
      TEST=.FALSE.
C***********************************************************************
C     III. CONVERSION FROM ROUTINE INRIBO/INROBO TO ROUTINE INRCRV
C***********************************************************************
C-----MOLAR SURFACE CONCENTRATION OF SURFACE SPECIES
      DO 110 I=1,NSSPEC
      XCONC(I+NSPEC)=COV(I)*SITE
  110 CONTINUE
C-----MOLAR GAS PHASE CONCENTRATION
      DO 160 I=1,NSPEC
      XCONC(I)=CIL(I)
  160 CONTINUE
C***********************************************************************
C     VI: CALCULATION OF RATE CONSTANTS
C***********************************************************************
      DO 200 I=1,NSREAC
        RT = RGAS * T
      RKF(I)=PK(I)*T**TEXP(I)*EXP(-EXA(I)/RT)
      IF(IARRH(I).NE.0) THEN
        IF(INUMC(I).NE.0) THEN
        COVTMP=XCONC(INUMC(I))/SITE
        ELSE
        COVTMP = 0
        ENDIF
        RKF(I)=RKF(I)*EXP(XEXA(I)*COVTMP/RT)
      IF((ABS(VT5(I)).GT.1.0D-6).AND.(COVTMP.GT.1.0D-15).AND.
     1      (COVTMP.LT.1.0D0)) RKF(I)=RKF(I)*(COVTMP**VT5(I))
      ENDIF
       NTAU = 0
      DO 320 J=6,1,-1
        IMAT=MATLLS(J,I)
        IF(IMAT.NE.0) RKF(I) = RKF(I) * XCONC(IMAT)
        IF(IMAT.GT.NSPEC.AND.IMAT.LE.NSPEC+NSSPEC) NTAU = NTAU + 1
  320 CONTINUE
        IF(ISTICK(I).NE.0) THEN
        RKF(I) = RKF(I)*SQRT(RGAS/2.0D0/PI/XMOL(ISTICK(I)))/(SITE**NTAU)
      ENDIF
  200 CONTINUE
C***********************************************************************
C     VI.II: CALCULATION OF RATE CONSTANTS FOR SENSITIVITY ANAL.
C***********************************************************************
      IF(NSENS.NE.0) THEN
      DO 510 JP=1,NSENS
      NRCO=ISENS(JP)
      RKF(NRCO)=RKF(NRCO)*RPAR(JP)
  510 CONTINUE
      ENDIF
C***********************************************************************
C     V: CALCULATION OF SURFACE REACTION RATE  (Gl. (2.67) von Diss.OD)
C************************************************************************
      SKDOT(1:NSSPEC+NSPEC) = 0.0D0
      	DO 310 K=1,NRINSS
          IS = IREACP(1,K)
          IR = IREACP(2,K)
          ID = IREACP(3,K)
        SKDOT(IS)=SKDOT(IS)+FLOAT(ID)*RKF(IR)
  310 	CONTINUE
C***********************************************************************
C     CONVERSION FOR ROUTINE INRIBO/INROBO
C***********************************************************************
C-----adsorption/desorption (mass) rates of gas phase species
      DO 201 I=1,NSPEC
      RATL(I)= SKDOT(I)*XMOL(I)
  201 CONTINUE
C-----coverage rates (s. Diss OD Gl. (2.25) mit sigma_i=1)
      DO 210 I=1,NSSPEC
        FCOV(I)=SKDOT(I+NSPEC)/SITE
  210 CONTINUE
C***********************************************************************
C     OUTPUT FOR SURFACE MECHANISM ANALYSIS
C***********************************************************************
CXFY  Part for surface reaction
      IF(INFOM.LT.0) THEN
C---- OUTPUT OF SURFACE REACTION CONSTANT
      WRITE(NFIMEC,8104) (RKF(I),I=1,NSREAC)
C---- OUTPUT OF SURFACE REACTION RATES
      WRITE(NFIMEC,8105) (SKDOT(I),I=1,NSPEC+NSSPEC)
      ENDIF
 8104 FORMAT('>SURFACE REACTION CONSTANT ',/,7(E10.3,1X))
 8105 FORMAT('>SURFACE REACTION RATE (MOL/M**2/S) ',/,7(E10.3,1X))
C***********************************************************************
      RETURN
C***********************************************************************
C     END OF -CALSME-                                                  *
C***********************************************************************
      END
C
C
C_rst SPECIFY OPTION FOR HLRS-COMPILER
!OPTION! -O nodiv
      SUBROUTINE FALLOFF(IJAC,AZER,BZER,EZER,AINF,BINF,EINF,T3,T1,T2,
     1    AFA,BFA,XMCON,XT,XKINF,XPCOR,IORD,KINFT,KINFM,PCORT,PCORM)
C**********************************************************************C
C                                                                      C
C     *************************************************************    C
C     *                                                           *    C
C     *              CALCULATION OF FALLOFF RATES                 *    C
C     *                      08/03/2003                           *    C
C     *                    OLIVER MAIWALD                         *    C
C     *************************************************************    C
C                                                                      C
C**********************************************************************C
C
C**********************************************************************C
C                                                                      C
C   THIS SUBROUTINE CALCULATES FALLOFF RATES DUE TO THE TROE METHOD    C
C                                                                      C
C    INPUT PARAMETERS:                                                 C
C      AZER,BZER,EZER = ARRHENIUS PARAMETERS FOR LOW PRESSURE          C
C      AINF,BINF,EINF = ARRHENIUS PARAMETERS FOR HIGH PRESSURE         C
C      T3,T1,T2,AFA   = CONSTANTS FOR THE BROADENING FACTOR Fc         C
C      BFA            = ALTERNATIV VALUE FOR THE BROADENING FACTOR Fc  C
C      T              = TEMPERATURE (KELVIN)                           C
C      MCON           = EFFECTIVE CONCENTRATION OF THIRD BODIES        C
C      IORD           = FORWARD REACTION ORDER                         C
C                                                                      C
C    OUTPUT PARAMETERS:                                                C
C      KINF           = RATE CONSTANT FOR HIGH PRESSURE                C
C      PCOR           = PRESSURE CORRECTION TERM                       C
C                                                                      C
C    SUBROUTINE PARAMETERS                                             C
C      TTRAT  = TROE RATE CONSTANT AT TEMPERATURE T AND PRESSURE (CTOT)C
C                                                                      C
C    OTHER SUBROUTINES NEEDED: NONE                                    C
C                                                                      C
C**********************************************************************C

C**********************************************************************C
C     CHAPTER I: STORAGE ORGANIZATION                                  C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION KZER, KINF, FC, N, DEN1, F,  KLIN
      DOUBLE PRECISION KINFT, KINFM, PCORT, PCORM
      DOUBLE PRECISION HVAR, RGAS, XMCON, MCON, XT, T,  PRED, C
      DOUBLE PRECISION AINF, BINF, EINF, AZER, BZER, EZER
      DATA RGAS/8.3143D00/,ZERO/0.0D0/,ETAD/1.D-6/
      PARAMETER (SMALL=1.D-15)
C**********************************************************************C
C
C     This causes a loop to calculate the jacobian entries
C
C**********************************************************************C
      ILOO = -1
  100 CONTINUE
      ILOO = ILOO + 1
      T    = XT
      MCON = XMCON
      IF(ILOO.EQ.1) THEN
         DELTA = XT * ETAD
         T = XT  + DELTA
      ENDIF
      IF(ILOO.EQ.2) THEN
         DELTA = XMCON  * ETAD
         MCON  = XMCON  + DELTA
      ENDIF
C**********************************************************************C
C     CHAPTER II: CALCULATION OF FALLOFF RATES                         C
C                 R.Gilbert, K.Luther, J.Troe (Bunsenges. 1987,169-177)C
C**********************************************************************C
C---- LOW PRESSURE LIMIT
      HVAR=DMAX1((-EZER/(RGAS*T)),-1.7D2)
      KZER = AZER * T**BZER * EXP(HVAR)
C---- HIGH PRESSURE LIMIT
      HVAR=DMAX1((-EINF/(RGAS*T)),-1.7D2)
      KINF = AINF * T**BINF * EXP(HVAR)
C---- LINDEMANN RATE CONSTANT
      KLIN = KZER * KINF * MCON / ( KZER * MCON + KINF )
C---- BROADENING FACTOR FC
      IF(T3.GT.SMALL) THEN
        FC = ( 1.0 - AFA ) * EXP(-T/T3)
      ELSE
        FC = ZERO
      ENDIF
      IF(T1.GT.SMALL) FC = FC + AFA * EXP(-T/T1)
      IF(T2.GT.SMALL) FC = FC + EXP(-T2/T)
      FC=MAX(FC,SMALL)
      IF(BFA .GT. ZERO) FC = BFA

C---- DENOMINATOR OF LOG(F)
      N    = 0.75 - 1.27 * LOG10(FC)
      C    = -0.4 - 0.67 * LOG10(FC)

C---- CHANGE UNITS  --> see CHEMKIN Package (cklib.f)
      PRED = KZER * MCON / KINF

C     PRED DEPENDS ON THE REACTION ORDER
      IF(IORD .EQ. 3) PRED=PRED*1.D6
      IF(PRED .GT. ZERO) THEN
        DEN1 = ( (LOG10(PRED)+C) / (N-0.14*(LOG10(PRED)+C)) )**2
        DEN1 = DEN1 + 1
      ELSE
        PCOR=1.0D0
        GOTO 400
      END IF

C---- BROADENING FACTOR F
      F = 10.D0**(LOG10(FC)/DEN1)
C---- TROE RATE CONSTANT
      PCOR  = (PRED/(1.0D0+PRED)) * F
C---- divide by MCON to have the correct formulation (it is multiplied
C     by MCON in calling routine
      PCOR = PCOR / MCON
C     PCOR DEPENDS ON THE REACTION ORDER
      IF(IORD .EQ. 3) PCOR=PCOR*1D-6
C**********************************************************************C
C     OUTPUT FOR TESTING                                               C
C**********************************************************************C
C      WRITE(6,*) '+++++++++++++++++++++++++++++++++++++++++++++++'
C	 WRITE(6,*) '      >>>>>   FALLOFF CALCULATION             '
C      WRITE(6,1234) 'KZER   = ',KZER
C      WRITE(6,1234) 'KINF   = ',KINF
C      WRITE(6,1234) 'PRED   = ',PRED
C      WRITE(6,1234) 'FC     = ',FC
C      WRITE(6,1234) 'CON   =  ',MCON
C      WRITE(6,1234) 'F      = ',F
C      WRITE(6,1234) 'PCOR   = ',PCOR*MCON
C1234  FORMAT(7X,A9,2X,1PE12.5)
C**********************************************************************C
C     CHAPTER III: REGULAR EXIT                                        C
C**********************************************************************C
  400 CONTINUE
CET      
      !write(*,*) 'delta', delta
      IF(ILOO.EQ.0) THEN
        XKINF = KINF
        XPCOR = PCOR
      ENDIF
      IF(ILOO.EQ.1) THEN
        KINFT = (KINF - XKINF)/DELTA
        PCORT = (PCOR - XPCOR)/DELTA
      ENDIF
      IF(ILOO.EQ.2) THEN
        KINFM = (KINF - XKINF)/DELTA
        PCORM = (PCOR - XPCOR)/DELTA
      ENDIF
C     write(*,*) 'JJJJJJ',IJAC,ILOO
      IF(IJAC.GT.0.AND.ILOO.LT.2) GOTO 100
      RETURN
      END
C**********************************************************************C
C     END OF -FALLOFF-                                                 C
C**********************************************************************C
C
C
      SUBROUTINE REVREA(NS,NREAC,I,NFILE6,HLHH,HINF,
     1                  MAT,T,ECC,DECCDT)
C**********************************************************************C
C                                                                      C
C    ***************************************************************   C
C    *                                                             *   C
C    *             CALCULATION OF REVERSE REACTION RATES           *   C
C    *                                                             *   C
C    *              LAST CHANGE: 25/03/2002  O. MAIWALD            *   C
C    *                                                             *   C
C    ***************************************************************   C
C                                                                      C
C**********************************************************************C
C                                                                      C
C     INPUT  :                                                         C
C                                                                      C
C     NS            = NUMBER OF ALL SPECIES                            C
C     NREAC         = NUMBER OF ALL REACTIONS                          C
C     I             = NUMBER OF REACTION IN REACTION LIST              C
C     NFILE6        = NUMBER OF OUTPUT FILE FOR LISTING                C
C     T             = TEMPERATURE, K                                   C
C     HLHH(7,I)     = LOW  TEMPERATURE NASA COEFFICIENTS,  J,K (7,NS)  C
C     HLHH(7,I+NS)  = HIGH TEMPERATURE NASA COEFFICIENTS,  J,K (7,NS)  C
C     HINF(3,I)     = TEMPERATURE RANGES FOR THERMOCHEMICAL DATA       C
C     MAT(I)        = VECTOR OF SPECIES IN CONCWERNING REACTION        C
C     RCFORW        = RATE OF REACTION FOR FORWARD REACTION            C
C                                                                      C
C     VARIABLES :                                                      C
C                                                                      C
C     HR            = CHANGE OF ENTHALPY IN REACTION                   C
C     SR            = CHANGE OF ENTROPY IN REACTION                    C
C     GR            = CHANGE OF GIBBS ENERGY IN REACTION               C
C     MR            = CHANGE OF ENTHALPY IN REACTION                   C
C     ECP           = EQUILIBRIUM CONSTANT IN PRESSURE UNITS           C
C     ECC           = EQUILIBRIUM CONSTANT IN CONCENTRATION UNITS      C
C                                                                      C
C     OUTPUT :                                                         C
C                                                                      C
C     RCREV         = RATE OF REACTION FOR REVERSE REACTION            C
C                                                                      C
C**********************************************************************C
C                                                                      C
C**********************************************************************C
C     STORAGE ORGANIZATION                                             C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION HLHH(7,NS*2),HINF(3,NS),MAT(6)
      DATA ZERO/0.0D0/
C**********************************************************************C
C     DEFINITION OF INITIAL VALUES                                     C
C**********************************************************************C
C---- GAS CONSTSANT RGAS(SI-UNITS), RBARCM(UNITS OF BAR,CM,MOLE)
      DATA RGAS/8.3143D0/,RBARCM/83.143D0/,RBARM/8.3143D-5/
C**********************************************************************C
C     INITIALIZATION                                                   C
C**********************************************************************C
      MR=0
      HR=ZERO
      SR=ZERO
      ECP=ZERO
      ECC=ZERO
C**********************************************************************C
C     CALCULATE CHANGE OF MOLECULARITY, ENTHALPY, ENTROPY              C
C**********************************************************************C
C---- SUM UP LEFT SIDE
      DO 310 II=1,3
        LL=MAT(II)
        IF(LL.EQ.0 .OR. LL.GT.NS) GOTO 310
        CALL CALHSI(NS,LL,T,HLHH,HINF,HI,SI,NFILE6)
        HR=HR-HI
        SR=SR-SI
        MR=MR-1
 310  CONTINUE
C---- SUM UP RIGHT SIDE
      DO 320 II=4,6
        LL=MAT(II)
        IF(LL.EQ.0.OR.LL.GT.NS) GOTO 320
        CALL CALHSI(NS,LL,T,HLHH,HINF,HI,SI,NFILE6)
        HR=HR+HI
        SR=SR+SI
        MR=MR+1
  320 CONTINUE
C**********************************************************************C
C     CALCULATE EQUILIBRIUM CONSTANT KP, KC                            C
C**********************************************************************C
C---- GIBBS-FREE ENERGY
      GR=HR-T*SR
C---- ECP REFERS TO EQUILIBRIUM CONSTANT FOR PARTIAL PRESSURES
C---- UNIT IS BAR**(-MR)
      ECP=DEXP(-GR/(RGAS*T))
C---- ECC REFERS TO EQUILIBRIUM CONSTANT FOR CONCENTRATIONS (MOLE,CM)
C---- DIVISION BY FACTOR :   R(BAR,CM)**MR
      ECC=ECP/(RBARM*T)**MR
      IF(ECC.EQ.0) THEN
         WRITE(NFILE6,831)
         ECC=1.0E-35
         WRITE(NFILE6,*) 'hr,t,sr,ECP,MR,RBARM',hr,t,sr,ECP,MR,RBARM
      ENDIF
C**********************************************************************C
C     Calculate DECCDT
C**********************************************************************C
      DECCDT = ECC * (HR - FLOAT(MR) * RGAS *T) /(RGAS * T**2)
C**********************************************************************C
C     REGULAR EXIT                                                     C
C**********************************************************************C
      RETURN
C**********************************************************************C
C     FORMAT STATEMENTS                                                C
C**********************************************************************C
  831 FORMAT('0',5X,'++++++  ERROR IN -REVREA-  ++++++','   INCORRECT',
     1       ' REVERSE REACT. ARRH. PARAM. ','DUE TO BAD THERMODYN.',
     2       'OR INPUT DATA  ++++++')
C**********************************************************************C
C     END OF -REVREA-                                                  C
C**********************************************************************C
      END
C
      SUBROUTINE CALHSI(NS,I,T,HLHH,HINF,HI,SI,NFILE6)
C**********************************************************************C
C                                                                      C
C    ***************************************************************   C
C    *                                                             *   C
C    *         PROGRAM FOR CALCULATION OF MOLAR ENTHALPIES         *   C
C    *                    AND MOLAR ENTROPIES                      *   C
C    *                                                             *   C
C    *              LAST CHANGE: 25/03/2002 MAIWALD                *   C
C    *                                                             *   C
C    ***************************************************************   C
C                                                                      C
C**********************************************************************C
C                                                                      C
C     INPUT  :                                                         C
C                                                                      C
C       NS            = NUMBER OF ALL SPECIES                          C
C       I             = NUMBER OF SPECIES IN SPECIES LIST              C
C       T             = TEMPERATURE, K                                 C
C       HINF(3,I)     = TEMPERATURE RANGES FOR THERMOCHEMICAL DATA     C
C       HLHH(7,I)     = LOW  TEMPERATURE NASA COEFFICIENTS             C
C       HLHH(7,I+NS)  = HIGH TEMPERATURE NASA COEFFICIENTS             C
C                                                                      C
C           HLHH(1-5,I)   = STANDARD HEAT CAPACITY, LOW TEMPERATURE    C
C           HLHH(6,I)     = STANDARD ENTHALPY     , LOW TEMPERATURE    C
C           HLHH(7,I)     = STANDARD ENTROPIE     , LOW TEMPERATURE    C
C           HLHH(1-5,I+NS)= STANDARD HEAT CAPACITY, HIGH TEMPERATURE   C
C           HLHH(6,I+NS)  = STANDARD ENTHALPY     , HIGH TEMPERATURE   C
C           HLHH(7,I+NS)  = STANDARD ENTROPIE     , HIGH TEMPERATURE   C
C                                                                      C
C     OUTPUT :                                                         C
C                                                                      C
C       HI            = MOLAR ENTHALPY, J/MOL                          C
C       SI            = MOLAR ENTROPY, J/MOL*K                         C
C                                                                      C
C**********************************************************************C
C
C**********************************************************************C
C     STORAGE ORGANIZATION AND DEFINITION OF INITIAL VALUES            C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION HLHH(7,NS*2),HINF(3,NS)
      DATA FIVE/5.0D0/,FOUR/4.0D0/,THREE/3.0D0/,TWO/2.0D0/,ZERO/0.0D0/
      DATA ONE/1.0D0/,HALF/0.5D0/,XMAR/1.0D-1/
C**********************************************************************C
C     CALCULATION OF H                                                 C
C**********************************************************************C
      IERR = 0
      IF(T.LT.HINF(1,I)) THEN
        J = 1
        IERR = IERR + 1
      ELSE
        IF(T.LT.HINF(2,I)) THEN
          J = 1
        ELSE
          IF(T.LT.HINF(3,I)) THEN
            J = 2
          ELSE
            J = 2
            IERR = IERR + 1
          ENDIF
        ENDIF
      ENDIF
      IF( -1.LT.0) THEN
        DELN  = (T - HINF(2,I)) / (XMAR * HINF(2,I))
        WEIGL = DMIN1(ONE,DMAX1(ZERO,HALF-DELN))
        WEIGH = ONE - WEIGL
      ELSE
        IF(T.LE.HINF(2,I)) THEN
          WEIGL = ONE
          WEIGH = ZERO
        ELSE
          WEIGL = ZERO
          WEIGH = ONE
        ENDIF
      ENDIF
C---- CALCULATION OF ENTHALPY
      HI= WEIGL *(((((HLHH(5,I)*T/FIVE+HLHH(4,I)/FOUR)*T
     1          + HLHH(3,I)/THREE)*T
     2          + HLHH(2,I)/TWO)*T + HLHH(1,I))*T + HLHH(6,I))
     3  + WEIGH *(((((HLHH(5,I+NS)*T/FIVE+HLHH(4,I+NS)/FOUR)*T
     4          + HLHH(3,I+NS)/THREE)*T
     5          + HLHH(2,I+NS)/TWO)*T + HLHH(1,I+NS))*T + HLHH(6,I+NS))

C---- CALCULATION OF ENTROPY
      SI= WEIGL *((((HLHH(5,I)*T/FOUR+HLHH(4,I)/THREE)*T
     1          + HLHH(3,I)/TWO)*T + HLHH(2,I))*T
     2          + HLHH(1,I)*DLOG(T) + HLHH(7,I))
     3  + WEIGH *((((HLHH(5,I+NS)*T/FOUR+HLHH(4,I+NS)/THREE)*T
     4          + HLHH(3,I+NS)/TWO)*T + HLHH(2,I+NS))*T
     5          + HLHH(1,I+NS)*DLOG(T) + HLHH(7,I+NS))
C**********************************************************************C
C     REGULAR EXIT                                                     C
C**********************************************************************C
      RETURN
C**********************************************************************C
C     END OF -CALHSI -                                                 C
C**********************************************************************C
      END
      SUBROUTINE MECHJA(INFO,NSPEC,NREAC,NM,NRINS,NRTAB,
     1     MATLL,NRVEC,PK,TEXP,EXA,VT4,VT5,VT6,IARRH,
     1     XZSTO,RATCO,CSUM,C,T,RATE,NSENS,ISENS,RPAR,
     1     LRW1,G,IPRI,MSGFIL,IERR,
     1     AINF,BINF,EINF,T3,T1,T2,AFA,BFA,HLHH,HINF,
     1     NSD,NRD,DGDC,DGDT,DODC,DODT)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *    PROGRAM FOR THE CALC. OF RATES OF CHEMICAL FORMATION   *    *
C     *    (WITH MECHANISM STORED IN SPARSE MATRIX STRUCTURE)     *    *
C     *    In addition the Jacobian can be calculated             *    *
C     *                   CHANGE: 15/09/2003 / Maas               *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C
C  Input:
C     INFO               = control parameter
C                            INFO = 0: compute only reaction rates
C                            INFO = 1: compute rates and Jacobian
C     NSPEC              = number of species
C     NREAC              = number of reactions
C     NM                 = number of third bodies
C     C(1)               = SPECIES CONCENTRATIONS, MOL/M**3 (NSPEC+NM)
C     T                  = TEMPERATURE, K
C     NSD                : set to NSPEC for INFO > 0
C                          use dummy value of 1 otherwise
C     NRD                : set to NREAC for INFO > 0
C                          use dummy value of 1 otherwise
C
C  Output:
C
C     RATE(NSPEC)        = molar scale rates of formation  mol/m**3*s
C     RAT(NSPEC)         = omega  (molar rates of formation)
C     G(NREAC)           = r      (molar reaction rates)
C
C   Only if INFO > 0:
C     DGDC(NREAC,NSPEC)  = d(r) / dc
C     DGDT(NREAC)        = d(r) / dT
C     DODC(NSPEC,NSPEC)  = d(omega) / dc
C     DODT(NSPEC)        = d(omega) / dT
C
C                                                                      *
C***********************************************************************
C                                                                      *
C***********************************************************************
C     COMMON BLOCKS                                                    *
C***********************************************************************
C***********************************************************************
C     STORAGE ORGANIZATION, INITIAL VALUES                             *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL CALREV
      LOGICAL, DIMENSION(NREAC) :: GETREV
      DOUBLE PRECISION MCON
      DIMENSION RATCO(NREAC,NRTAB),
     1     PK(NREAC),TEXP(NREAC),EXA(NREAC),XZSTO(NM,NSPEC),
     1     VT4(NREAC),VT5(NREAC),VT6(NREAC),
     1     AINF(NREAC),BINF(NREAC),EINF(NREAC),
     1     T3(NREAC),T1(NREAC),T2(NREAC),AFA(NREAC),BFA(NREAC)
      DIMENSION DGDC(NRD,NSD),DGDT(NRD),
     1          DODC(NSD,NSD),DODT(NSD)
      DIMENSION HLHH(7,NSPEC*2),HINF(3,NSPEC)
      DIMENSION C(*),RATE(NSPEC),IARRH(NREAC+1)
      DIMENSION MATLL(3,NREAC),NRVEC(3,NRINS),MAT(6,NREAC)
      DIMENSION G(LRW1)
      DIMENSION RPAR(*),ISENS(*)
C
      DOUBLE PRECISION, DIMENSION(NREAC) :: VORF, DVORDT
C---- BLOCKS FOR CALCULATION OF SPECIES PROPERTIES
      PARAMETER (ONE=1.0D0,ZERO=0.0D0)
      DATA RGAS/8.3143D0/
C***********************************************************************
Cmms
C     write(*,*) '****####****mms#mms#mms#mms***###***'
C     write(*,*) 'Mechja wird aufgerufen ...'
C     write(*,*) 'IARRH ist gleich ',IARRH
C     write(*,*) '****####****mms#mms#mms#mms***###***'
Cmms-end
CET   
      IF(LRW1.LT.NREAC) GOTO 9100
      DREVDM = 0
C***********************************************************************
C     Setup useful variables
C***********************************************************************
      MCON  = ZERO
      NSNM  = NSPEC+NM
      NSDUM = NSPEC+NM+1
C***********************************************************************
C     Calculate third body efficiencies
C***********************************************************************
C---- sum of concentrations for first third body
      C(NSPEC+1)=CSUM
C---- weighted concentrations for other third bodies
      IF(NM.GT.1) THEN
        DO  I=2,NM
          C(I+NSPEC)=DOT_PRODUCT(C(1:NSPEC),XZSTO(I,1:NSPEC))
        ENDDO
      ENDIF
C---- dummy value
      C(NSDUM)=ONE
C***********************************************************************
C     Set sensitivity information to zero
C***********************************************************************
      IF(INFO.GT.0) THEN
C---- stop if Jacobian cannot be calculated
            IF(IARRH(NREAC+1).EQ.1) GOTO 9980
C           IF(IARRH(NREAC+1).EQ.2) GOTO 9980
            DGDT(1:NREAC)         = ZERO
            DGDC(1:NREAC,1:NSPEC) = ZERO
      ENDIF
C***********************************************************************
C     Set flag for calculation of reverse reaction
C***********************************************************************
      DO I = 1,NREAC
        GETREV(I) =     (IARRH(I).EQ.2.OR.IARRH(I).EQ.3)
     1             .AND. IARRH(I+1).LT.ZERO
      ENDDO
C***********************************************************************
C     Set indices of involved species
C***********************************************************************
      DO I = 1,NREAC
         MAT(1:3,I)=MATLL(1:3,I)
         IF(GETREV(I)) THEN
            MAT(4:6,I)=MATLL(1:3,I+1)
         ENDIF
      ENDDO
C***********************************************************************
C     Rate coefficient for standard gas phase reaction (IARRH= 0,3)
C***********************************************************************
      DO I = 1,NREAC
C
         IF(IARRH(I).EQ. 0 .OR. IARRH(I).EQ. 3) THEN
            LT=T*0.1D0
            IF(LT.LE.20) LT=20
            IF(LT.LT.(NRTAB-1)) THEN
               DT=T*0.1D0-FLOAT(LT)
               VORF(I) = RATCO(I,LT)+(RATCO(I,LT+1)-RATCO(I,LT))*DT
            ELSE
               HVAR=DMAX1((-EXA(I)/(RGAS*T)),-1.7D2)
               VORF(I) = PK(I) * (T**TEXP(I)) * DEXP(HVAR)
            END IF
         END IF
      END DO
C***********************************************************************
C     Rate coefficient for VT relaxation (IARRH(I)= 1)
C***********************************************************************
      DO I = 1,NREAC
        IF ( IARRH(I) .EQ. 1 ) THEN
          CALL VTREAC(IARRH(I),PK(I),TEXP(I),EXA(I),VT4(I),VT5(I),
     1                VT6(I),T,VORF(I))
        END IF
      END DO
C
C***********************************************************************
C     Rate coefficient for VT relaxation (IARRH(I)= 1)
C***********************************************************************
      DO I = 1,NREAC
        IF ( IARRH(I) .EQ. 4 ) THEN
          CALL HNYREAC(IARRH(I),PK(I),TEXP(I),EXA(I),VT4(I),VT5(I),
     1                VT6(I),T,VORF(I))
        END IF
      END DO
C
C
C***********************************************************************
C     Calculate reaction rates
C***********************************************************************
      CALREV = .FALSE.
      I=1
      DO WHILE (I .LE. NREAC)
C***********************************************************************
C     get reaction order of forward reaction
C***********************************************************************
      IORDF = 0
      DO K = 1,3
        IF(MAT(K,I).NE.NSDUM) IORDF = IORDF + 1
      ENDDO
C***********************************************************************
C     get reaction order of reverse reaction
C***********************************************************************
C      IF(GETREV(I)) THEN
C      IORDR = 0
C      DO K = 4,6
C        IF(MAT(K,I).NE.NSDUM) IORDR = IORDR + 1
C      ENDDO
C      ENDIF
C***********************************************************************
C     get calculated values for reverse reaction if already known
C***********************************************************************
      IF(CALREV) THEN
C---- in this case the rate coefficient had been stored in VORFREV
        VORF(I)   = VORFREV
        IF(INFO.GT.0) THEN
          DVORDT(I) = DREVDT
          DVORDM    = DREVDM

        ENDIF
        CALREV = .FALSE.
C***********************************************************************
C
C     Calculate forward rate coefficient
C
C***********************************************************************
      ELSE
C***********************************************************************
C     Rate coefficient for standard gas phase reaction (IARRH= 0,3)
C***********************************************************************
        IF(IARRH(I).EQ. 0 .OR. IARRH(I).EQ. 3) THEN
C
        IF(INFO.GT.0) DVORDT(I) = VORF(I) *
     1                            (TEXP(I)/T + EXA(I)/(RGAS*T**2))
          IF(GETREV(I)) THEN
            CALL REVREA(NSPEC,NREAC,I,MSGFIL,HLHH,HINF,MAT(1,I),T,
     1            ECC,DECCDT)
            VORFREV=VORF(I)/ECC
            IF(INFO.GT.0) DREVDT = (DVORDT(I)- VORFREV*DECCDT)/ECC
            CALREV = .TRUE.
          ENDIF
        ELSE IF(IARRH(I).EQ. 4 ) THEN
        IF(INFO.GT.0) DVORDT(I) = VORF(I) *
     1                            (TEXP(I)/T + EXA(I)/(RGAS*T**2))
C***********************************************************************
C     Get rate coefficient for unimolecular reactions (IARRH(I)= 2)
C***********************************************************************
        ELSE
C----  assign value for third body concentration
          DO JJ=1,3
            IF(MAT(JJ,I).GT.NSPEC.AND.MAT(JJ,I).LE.NSNM) THEN
              MCON = C(MAT(JJ,I))
              GOTO 721
            ENDIF
          ENDDO
          GOTO 9990
  721     CONTINUE
          IJAC = INFO
CET          
          !write(*,*) 'mcon', mcon
          if (mcon > 0) then
          CALL FALLOFF(IJAC,PK(I),TEXP(I),EXA(I),
     1                   AINF(I),BINF(I),EINF(I),
     1                   T3(I),T1(I),T2(I),AFA(I),BFA(I),MCON,T,
     2               VORFINF,PCOR,IORDF,DVINDT,DVINDM,DPCODT,DPCODM)
          VORF(I)=VORFINF*PCOR
          IF(INFO.GT.0) THEN
            DVORDT(I) = DVINDT * PCOR + VORFINF *  DPCODT
            DVORDM    = DVINDM * PCOR + VORFINF *  DPCODM

          ENDIF
          IF(GETREV(I)) THEN
            CALL REVREA(NSPEC,NREAC,I,MSGFIL,HLHH,HINF,MAT(1,I),T,
     1            ECC,DECCDT)
            VORFREV=VORF(I)/ECC
            IF(INFO.GT.0) THEN
              DREVDT = (DVORDT(I)- VORFREV*DECCDT)/ECC
              DREVDM = (DVORDM                   )/ECC

CET
              !write(*,*) 'dv4', dvordm
            ENDIF
            CALREV = .TRUE.
          ENDIF
          end if
       ENDIF
C***********************************************************************
C     End of block for rate coefficient
C***********************************************************************
       ENDIF
C***********************************************************************
C     Calculate entries of Jacobian
C
C     calculate dG/dC
C***********************************************************************
C***********************************************************************
C     Look at C(MAT(1))
C***********************************************************************
      IF(INFO.GT.0) THEN
      DO JJ=1,3
      IF(JJ.EQ.1) THEN
       CC  = C(MAT(2,I)) * C(MAT(3,I))
      ELSE IF(JJ.EQ.2) THEN
       CC  = C(MAT(1,I)) * C(MAT(3,I))
      ELSE
       CC  = C(MAT(1,I)) * C(MAT(2,I))
      ENDIF
      IF(MAT(JJ,I).LE.NSPEC) THEN
        DGDC(I,MAT(JJ,I)) = DGDC(I,MAT(JJ,I)) + VORF(I) * CC
      ELSE IF(MAT(JJ,I).LE.NSNM) THEN
        DGDC(I,1:NSPEC) = DGDC(I,1:NSPEC)
     1                  + VORF(I) * XZSTO(MAT(JJ,I)-NSPEC,1:NSPEC)*CC
C---- for Troe formulation add extra term
        IF(IARRH(I).EQ.2) THEN
        DGDC(I,1:NSPEC) = DGDC(I,1:NSPEC)
     1                      + XZSTO(MAT(JJ,I)-NSPEC,1:NSPEC)* DVORDM
     1                      * C(MAT(1,I))*C(MAT(2,I))*C(MAT(3,I))
        ENDIF
CET
      !if (i == 847) then
      !    read(*,*)
      !end if
      ENDIF
      ENDDO
      ENDIF
C***********************************************************************
C
C     Rate of reverse reactio if option "revsim" is chosen
C
C***********************************************************************
        I=I+1
      END DO
C***********************************************************************
C     END OF Calculate reaction rates
C***********************************************************************
C
C***********************************************************************
C     Calculate G(I) for forward reaction
C***********************************************************************
      DO I = 1,NREAC
         G(I) = VORF(I)  * C(MAT(1,I))*C(MAT(2,I))*C(MAT(3,I))
      ENDDO
C***********************************************************************
C     Calculate entries of Jacobian
C
C     Calculate dGdT for forward reaction
C***********************************************************************
      IF(INFO.GT.0) THEN
         DO I = 1,NREAC
C            DGDT(I) = G(I) * (TEXP(I)/T + EXA(I)/(RGAS*T**2))
            DGDT(I) = DVORDT(I)  * C(MAT(1,I))*C(MAT(2,I))*C(MAT(3,I))
         ENDDO
      ENDIF
C
C***********************************************************************
C     5. Multiply with RPAR for sensitivity analysis                   *
C***********************************************************************
 2500 CONTINUE
      IF(NSENS.NE.0) THEN
        DO JP=1,NSENS
          NRCO=ISENS(JP)
          G(NRCO)=G(NRCO)*RPAR(JP)
        ENDDO
      ENDIF
C***********************************************************************
C     6. CALCULATION OF THE REACTION RATES                             *
C***********************************************************************
      RATE(1:NSPEC)=ZERO
      IF(INFO.GT.0) THEN
        DODT(1:NSPEC)=ZERO
        DODC(1:NSPEC,1:NSPEC)=ZERO
      ENDIF
C***********************************************************************
C     Calculate reaction rates                                         *
C***********************************************************************
      DO I=1,NRINS
        NR=NRVEC(2,I)
        NS=NRVEC(1,I)
        ND=NRVEC(3,I)
        RATE(NS)=RATE(NS)+ G(NR)*FLOAT(ND)
        IF(INFO.GT.0) THEN
         DODT(NS)=DODT(NS)+ DGDT(NR)*FLOAT(ND)
         DODC(NS,1:NSPEC)=DODC(NS,1:NSPEC)+ DGDC(NR,1:NSPEC)*FLOAT(ND)
CET
!         write(*,*) 'i', i, 'ns', ns , 'nr', nr
!         do j=1,nspec
!            if (dgdc(nr,j) /= dgdc(nr,j)) then
!                read(*,*)
!            end if
!         end do
        ENDIF
      ENDDO
C***********************************************************************
C     8. Output for mechanisms analysis                                *
C***********************************************************************
C     IF(IPRI.GT.0) THEN
C2101 FORMAT(7(E10.3,1X))
C2002 FORMAT('>MOLAR RATES (SI) RATE(I),I=1,NS')
C2003 FORMAT('>MOLAR RATES (SI) RATE(I),I=1,NREAC')
C---- INITIALIZE
C     WRITE(NFIMEC,2002)
C     WRITE(NFIMEC,2101) (RATE(J),J=1,NSPEC)
C     WRITE(NFIMEC,2003)
C     WRITE(NFIMEC,2101) (G(J),J=1,NREAC)
C     ENDIF
C***********************************************************************
C     9. CALCULATE MASS RATES                                          *
C***********************************************************************
 3000 CONTINUE
      IERR=0
C***********************************************************************
C     10. REGULAR EXITS                                                *
C***********************************************************************
      RETURN
C***********************************************************************
C     11. ERROR EXITS                                                  *
C***********************************************************************
 9100 IERR=1
      GOTO 9999
 9980 CONTINUE
      WRITE(MSGFIL,9998) '          ERROR IN -MECHJA-            '
      WRITE(MSGFIL,9998) ' Jacobian cannot be calculated         '
      STOP
 9990 CONTINUE
      WRITE(MSGFIL,*) ' '
      WRITE(MSGFIL,9998) '          ERROR IN -MECHJA-            '
      WRITE(MSGFIL,9998) 'NO ENHANCED THIRD BODY EFFICIENCY FOUND'
      STOP
 9999 CONTINUE
      WRITE(MSGFIL,9998) '  ERROR IN -MECHJA-   '
      WRITE(MSGFIL,9998) '  ERROR IN -MECHJA-   '
 9998 FORMAT(3X,6('+'),6X,A39,6X,6('+'))
      STOP
C***********************************************************************
C     END OF -MECHJA-                                                  *
C***********************************************************************
      END
      SUBROUTINE TRASSP(IEQ,NEQ,SMF,NSPEC,T,C,CI,RHO,H,P,TD,IERR,
     1                  INFDER,DXDS,LDXDS,DTDS,DPDS)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *    Compute physical variables from state space variables  *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C***********************************************************************
C
C
C   INFO > 0   calculate concentrations ond temp. from specific values
C
C      INFO = 1
C             Input:  SMF = (h,p,phi)
C             Output: T,CI,C,RHO,H,P
C
C      INFO = 2
C             Input:  SMF = (h,rho,phi)
C             Output: T,CI,C,P,RHO,H
C
C      INFO = 3
C             Input:  SMF = (h,p,w)
C             Output: T,CI,C,RHO,P,H
C
C      INFO = 4
C             Input:  SMF(h,rho,w)
C             Output: T,CI,C,P,RHO,H
C
C      INFO = 5
C             Input:  SMF = (T,p,phi)
C             Output: T,CI,C,RHO,H,P
C
C      INFO = 6
C             Input:  SMF = (T,rho,phi)
C             Output: T,CI,C,P,RHO,H
C
C      INFO = 7
C             Input:  SMF = (T,p,w)
C             Output: T,CI,C,RHO,P,H
C
C      INFO = 8
C             Input:  SMF(T,rho,w)
C             Output: T,CI,C,P,RHO,H
C
C
C
C   INFO < 0   calculate specific values from concentrations ond temp.
C
C      INFO = -1
C             Input:  T,CI
C             Output: C,RHO,H,P,SMF  SMF = (h,p,phi)
C
C      INFO = -2
C             Input:  T,CI
C             Output: C,RHO,H,P,SMF  SMF = (h,rho,phi)
C
C      INFO = -3
C             Input:  T,CI
C             Output: C,RHO,H,P,SMF  SMF = (h,p,w)
C
C      INFO = -4
C             Input:  T,CI
C             Output: C,RHO,H,P,SMF  SMF = (h,rho,w)
C
C      phi denote specific mole numbers, w denote mass fractions
C
C     TD has to contain the thermodynamic properies:
C          molar masses: TD(1:NSPEC)
C          NASA coeff.   TD(NSPEC+1,NSPEC*15) molar units
C          NASA limits   TD(NSPEC*15+1:NSPEC*18
C          work array for CPI,HI: TD(NSPEC*18+1:NSPEC*20)
C
C***********************************************************************
C***********************************************************************
C***********************************************************************
C
C     CHAPTER I: STORAGE ORGANIZATION                                  *
C
C***********************************************************************
C***********************************************************************
C     I.I.: TYPES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      LOGICAL FOPT
      PARAMETER (ZERO= 0.0D0, ONE = 1.0D0)
C***********************************************************************
C     I.II.: DIMENSIONS
C***********************************************************************
      DIMENSION CI(NSPEC),SMF(NEQ),TD(*)
      DIMENSION DXDS(LDXDS,NEQ),DTDS(NEQ),DPDS(NEQ)
C**********************************************************************C
C     INITIALIZE
C**********************************************************************C
      IERR = 0
      IXMOL = 1
      IHLHH = NSPEC+1
      IHINF = NSPEC*15+1
      IHI   = NSPEC*18+1
      ICPI  = NSPEC*19+1
C**********************************************************************C
C     call xcihpw
C**********************************************************************C
      FOPT = .FALSE.
      IF(IEQ.EQ.1) THEN
        H = SMF(1)
        P = SMF(2)
        FOPT = . TRUE.
      ELSE IF(IEQ.EQ.2)  THEN
        H   = SMF(1)
        RHO = SMF(2)
        FOPT = . TRUE.
      ELSE IF(IEQ.EQ.3)  THEN
        H   = SMF(1)
        P   = SMF(2)
        FOPT = . TRUE.
      ELSE IF(IEQ.EQ.4)  THEN
        H   = SMF(1)
        RHO = SMF(2)
        FOPT = . TRUE.
      ELSE IF(IEQ.EQ.5)  THEN
        T = SMF(1)
        P = SMF(2)
        FOPT = . TRUE.
      ELSE IF(IEQ.EQ.6)  THEN
        T   = SMF(1)
        RHO = SMF(2)
        FOPT = . TRUE.
      ELSE IF(IEQ.EQ.7)  THEN
        T   = SMF(1)
        P   = SMF(2)
        FOPT = . TRUE.
      ELSE IF(IEQ.EQ.8)  THEN
        T   = SMF(1)
        RHO = SMF(2)
        FOPT = . TRUE.
      ENDIF
C**********************************************************************C
C     call xcihpw
C**********************************************************************C
      CALL XCIHPW(IEQ,NSPEC,T,C,CI,RHO,H,P,SMF(3),
     1       TD(IXMOL),TD(IHLHH),TD(IHINF),TD(IHI),CP,TD(ICPI),IERR)
      XMOLM = RHO/C
C**********************************************************************C
C     calculate Jacobian
C**********************************************************************C
      IF(INFDER.NE.0) THEN
C**********************************************************************C
C
C**********************************************************************C
      DXDS(:,1:NEQ) = ZERO
      IF(IEQ.EQ.3.OR.IEQ.EQ.4.OR.IEQ.EQ.7.OR.IEQ.EQ.8) THEN
      DO 217 I = 1,NSPEC
      DO 216 J = 1,NSPEC
      DXDS(I,2+J) =  -XMOLM*(CI(I)/C) / TD(IXMOL-1+J)
  216 CONTINUE
      DXDS(I,2+I) =  DXDS(I,2+I) + XMOLM / TD(IXMOL-1+J)
  217 CONTINUE
      ELSE
      DO 227 I = 1,NSPEC
      DO 226 J = 1,NSPEC
      DXDS(I,2+J) =  -XMOLM*(CI(I)/C)
  226 CONTINUE
      DXDS(I,2+I) =  DXDS(I,2+I) + XMOLM
  227 CONTINUE
      ENDIF
C**********************************************************************C
C     h,p,phi
C**********************************************************************C
      IF(IEQ.EQ.1) THEN
        DTDS(1) = ONE/CP
        DTDS(2) = ZERO
        DPDS(1) = ZERO
        DPDS(2) = ONE
        DO I=1,NSPEC
          DTDS(2+I) = -TD(IHI-1+I)/CP
          DPDS(2+I) = ZERO
       ENDDO
C**********************************************************************C
C     h,rho,phi
C**********************************************************************C
      ELSE IF(IEQ.EQ.2)  THEN
        DTDS(1) = ONE/CP
        DTDS(2) = ZERO
        DPDS(1) = P/(CP*T)
        DPDS(2) = P/RHO
        DO I=1,NSPEC
          DTDS(2+I) = -TD(IXMOL-1+I)*TD(IHI-1+I)/CP
          DPDS(2+I) =  TD(IXMOL-1+I)*P*(
     1               TD(IHI-1+I)/(T*CP)  + RHO/(TD(IXMOL-1+I)*C))
       ENDDO
C**********************************************************************C
C     h,p,w
C**********************************************************************C
      ELSE IF(IEQ.EQ.3)  THEN
        DTDS(1) = ONE/CP
        DTDS(2) = ZERO
        DPDS(1) = ZERO
        DPDS(2) = ONE
        DO I=1,NSPEC
          DTDS(2+I) = -TD(IHI-1+I)/CP
          DPDS(2+I) = ZERO
       ENDDO
C**********************************************************************C
C     h,rho,w
C**********************************************************************C
      ELSE IF(IEQ.EQ.4)  THEN
        DTDS(1) = ONE/CP
        DTDS(2) = ZERO
        DPDS(1) = P/(CP*T)
        DPDS(2) = P/RHO
        DO I=1,NSPEC
          DTDS(2+I) = -TD(IHI-1+I)/CP
          DPDS(2+I) =  P*(
     1               TD(IHI-1+I)/(T*CP)  + RHO/(TD(IXMOL-1+I)*C))
       ENDDO
C**********************************************************************C
C     T,p,phi
C**********************************************************************C
      ELSE IF(IEQ.EQ.5)  THEN
        DTDS(1:NEQ) = ZERO
        DTDS(1) = ONE
        DPDS(1:NEQ) = ZERO
        DPDS(2) = ONE
C**********************************************************************C
C     T,rho,phi
C**********************************************************************C
      ELSE IF(IEQ.EQ.6)  THEN
        DTDS(1:NEQ) = ZERO
        DTDS(1) = ONE
        DPDS(1) = P/T
        DPDS(2) = P/RHO
        DO I=1,NSPEC
          DPDS(2+I) =  P*RHO/(TD(IXMOL-1+I)*C)
        ENDDO
C**********************************************************************C
C     T,p,w
C**********************************************************************C
      ELSE IF(IEQ.EQ.7)  THEN
        DTDS(1:NEQ) = ZERO
        DTDS(1) = ONE
        DPDS(1:NEQ) = ZERO
        DPDS(2) = ONE
C**********************************************************************C
C     T,rho,w
C**********************************************************************C
      ELSE IF(IEQ.EQ.8)  THEN
        DTDS(1:NEQ) = ZERO
        DTDS(1) = ONE
        DPDS(1) = P/T
        DPDS(2) = P/RHO
        DO I=1,NSPEC
          DPDS(2+I) =  P*TD(IXMOL-1+I)*RHO/(TD(IXMOL-1+I)*C)
        ENDDO
      ENDIF
      ENDIF
C**********************************************************************C
C     call xcihpw
C**********************************************************************C
      IF(IEQ.EQ.-1) THEN
        SMF(1) = H
        SMF(2) = P
        FOPT = . TRUE.
      ELSE IF(IEQ.EQ.-2) THEN
        SMF(1) = H
        SMF(2) = RHO
        FOPT = . TRUE.
      ELSE IF(IEQ.EQ.-3) THEN
        SMF(1) = H
        SMF(2) = P
        FOPT = . TRUE.
      ELSE IF(IEQ.EQ.-4) THEN
        SMF(1) = H
        SMF(2) = RHO
        FOPT = . TRUE.
      ELSE IF(IEQ.EQ.-5) THEN
        SMF(1) = T
        SMF(2) = P
        FOPT = . TRUE.
      ELSE IF(IEQ.EQ.-6) THEN
        SMF(1) = T
        SMF(2) = RHO
        FOPT = . TRUE.
      ENDIF
        IF(.NOT.FOPT) GOTO  990
      RETURN
C***********************************************************************
C     Error exits
C***********************************************************************
  990 CONTINUE
      IERR = 9
      GOTO 999
  999 CONTINUE
      RETURN
C***********************************************************************
C     END OF -XRASSP-
C***********************************************************************
      END
C
C***********************************************************************
C     FOLLOWING: SUBROUTINES FOR CALCULATION OF LIQUID PROPERTIES
C***********************************************************************
      SUBROUTINE DELTHO(NSPECP,XMOLP,T,W,PVPI,PAMB,RPRTLI,RHOI,RHO)
C***********************************************************************
C
C     *************************************************************
C     *               CALCULATION OF THE LIQUID DENSITY           *
C     *                  A.Schubert  07/2003                      *
C     *************************************************************
C
C     INPUT:
C     NSPECP     =  NUMBER OF SPECIES IN PARTICLE
C     XMOLP      =  MOLAR MASS OF SPECIES IN PARTICLE
C     T          =  TEMPERATURE
C     W          =  MASS FRACTIONS OF SPECIES IN PARTICLE
C     PVPI       =  VAPOR PRESSURES OF SPECIES IN PARTICLE
C     PAMB       =  AMBIENT PRESSURE
C
C     OUTPUT:
C     RHOI       =  DENSITIES OF SPECIES IN PARTICLE
C     RHO        =  DENSITY OF PARTICLE
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C
      DIMENSION XMOLP(NSPECP),W(NSPECP)
      DIMENSION PVPI(NSPECP)
      DIMENSION TRI(NSPECP),RHOI(NSPECP),VI(NSPECP)
      DOUBLE PRECISION, DIMENSION(24*NSPECP) :: RPRTLI
C
      PARAMETER (ZERO=0.0D0,ONE=1.0D0,TWO=2.0D0,THREE=3.0D0,FOUR=4.0D0,
     1     FIVE=5.0D0, SIX=6.0D0,TEN=10.D0)
      RHOI(1:NSPECP) = ZERO
      RHO = ZERO
C
      DO I=1,NSPECP
        RHOI(I)=DSPEPA(I,NSPECP,XMOLP,T,PVPI(I),PAMB,RPRTLI)
C***********************************************************************
C---- CALCULATION OF RHO WITH MASS FRACTIONS
C***********************************************************************
        RHO = RHO + W(I)/RHOI(I)
      ENDDO
      RHO = ONE / RHO
C       WRITE(*,*)'RHO', RHO ,'kg/m*3'
      RETURN
      END
C***********************************************************************
      FUNCTION DSPEPA(I,NSPECP,XMOLP,T,PVP,PAMB,RPRTLI)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *               Calculation of the density                  *
C     *         (Correlation: Hankinson/Brobst/Thomson)           *
C     *             (Pressure Correction: Thomson)                *
C     *                  A.Schubert  07/2003                      *
C     *                    R.Emig    03/2005                      *
C     *************************************************************
C
C     INPUT:
C     I          =  SPECIES I IN PARTICLE
C     NSPECP     =  NUMBER OF SPECIES IN PARTICLE
C     XMOLP      =  MOLAR MASS OF SPECIES IN PARTICLE
C     T          =  TEMPERATURE
C     W          =  MASS FRACTIONS OF SPECIES IN PARTICLE
C     PVP        =  VAPOR PRESSURES OF SPECIES IN PARTICLE
C     PAMB       =  AMBIENT PRESSURE
C
C     OUTPUT:
C     DSPEPA     =  DENSITY OF LIQUID SPECIES I
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C
      DIMENSION XMOLP(NSPECP)
      DOUBLE PRECISION, DIMENSION(24*NSPECP) :: RPRTLI
      LOGICAL :: VAPCOR
C
      PARAMETER (ZERO=0.0D0,ONE=1.0D0,TWO=2.0D0,THREE=3.0D0,FOUR=4.0D0,
     1     FIVE=5.0D0,SIX=6.0D0,TEN=10.D0,HALF=0.5D0)
C***********************************************************************
      NLIPRO = INT (RPRTLI(((I-1)*24)+1) + HALF)
C***********************************************************************
      IF (NLIPRO.LT.10) THEN
C---- SWITCHING ON/OFF CORRECTION DUE TO VAPOR PRESSURE
         VAPCOR = .TRUE.
C---- REDUCED TEMPERATURE
         TR = T / RPRTLI((24*(I-1))+6)
C***********************************************************************
C---- HBT-METHOD
         VOLR0 = ONE - 1.52816D0 * (ONE - TR)**(ONE / THREE) +
     1        1.43907D0 * (ONE - TR)**(TWO / THREE) - 0.81446D0 * (ONE
     1        - TR) + 0.190454D0 * (ONE - TR)**(FOUR / THREE)
         VOLRD = ((-0.296123D0) + 0.386914D0 * TR - 0.0427258D0 *
     1        TR**TWO - 0.0480645D0 * TR**THREE) / (TR  - 1.00001D0)
         VOLS = RPRTLI(((I-1)*24)+2) * VOLR0 *
     1        (ONE - RPRTLI(((I-1)*24)+3) * VOLRD)
C---- CORRECTION OF LIQUID DENSITY DUE TO VAPOR PRESSURE
         IF (VAPCOR) THEN
            VOLCVA = 0.0861488D0 + (0.0344483D0 * RPRTLI(((I-1)*24)+3))
            VOLEVA = EXP (4.79594D0 + 0.250047D0 * RPRTLI(((I-1)*24)+3)
     1           + 1.14188D0 * RPRTLI(((I-1)*24)+3)**TWO)
            VOLBTA = RPRTLI(((I-1)*24)+5) *TEN**FIVE *
     1           ((-ONE) - 9.070217D0 *(ONE-TR) ** (ONE/THREE)
     1           + 62.45326D0 *(ONE-TR)**(TWO/THREE) -
     1           135.1102D0 *(ONE-TR) + VOLEVA*(ONE-TR)**(FOUR/THREE))
            VOLM = VOLS * (ONE - VOLCVA * LOG((VOLBTA+PAMB)/
     1           (VOLBTA+PVP)))
         ELSE
C---- WITHOUT CORRECTION
            VOLM = VOLS
         ENDIF
            DSPEPA = (XMOLP(I) * TEN**(THREE) / VOLM)
      ELSE
C---- SIMPLE LINEAR FIT
         DSPEPA = RPRTLI(((I-1)*24)+2) + RPRTLI(((I-1)*24)+3) * T
      ENDIF
      RETURN
      END FUNCTION
C***********************************************************************
      SUBROUTINE WPVP(NSPECP,W,XMOLP,TCOR,RPRTLI,PVPWI,PVPW)
C***********************************************************************
C
C     *************************************************************
C     *
C     *             CALCULATION OF THE VAPOR PRESSURE
C     *                 (Correlation: Wagner, Antoine)
C     *                  A.Schubert  09/2003
C     *                  R.Stauch    12/2005
C     *************************************************************
C
C     INPUT:
C     NSPECP     =  NUMBER OF SPECIES IN PARTICLE
C     W          =  MASS FRACTIONS OF SPECIES IN PARTICLE
C     XMOLP      =  MOLAR MASS OF SPECIES IN PARTICLE
C     TCOR       =  TEMPERATURE (FROM INRCOR)
C
C     OUTPUT:
C     PVPWI      =  VAPOR PRESSURES OF SPECIES IN PARTICLE
C     PVPW       =  VAPOR PRESSURE OF PARTICLE
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C
C     DIMENSION PVPWI(MNSPEP)
      DIMENSION W(NSPECP),XMOLP(NSPECP)
      DIMENSION XIP(NSPECP)
      DIMENSION PVPWE(NSPECP), PVPWI(NSPECP)
      DOUBLE PRECISION, DIMENSION(24*NSPECP) :: RPRTLI
C
      PARAMETER (ZERO=0.0D0,ONE=1.0D0,TWO=2.0D0,THREE=3.0D0,FIVE=5.0D0,
     1           SIX=6.0D0,TEN=10.0D0,HALF=0.5D0)
C***********************************************************************
C---- CALCULATION OF MEAN MOLAR MASS
      CALL CALXMO(NSPECP,W,XMOLP,XM)
      DO I = 1,NSPECP
C----   GET MOLAR FRACTIONS
        XIP(I) = W(I) * XM / XMOLP(I)
      ENDDO

C---- VAPOR PRESSURE (Wagner)
      PVPW = ZERO
      DO I=1,NSPECP
         NLIPRO = INT (RPRTLI(((I-1)*24)+1) + HALF)
            INERT=INT (RPRTLI(((I-1)*24)+3) + HALF)
C---- WAGNER CORRELATION OF VAPOR PRESSURE
         IF (NLIPRO.LT.5) THEN
            IF(TCOR.GE.RPRTLI(((I-1)*24)+6)) THEN
               TR = 0.95D0
            ELSE
               TR = TCOR/RPRTLI(((I-1)*24)+6)
            ENDIF
            TX = ONE - TR
            PVPWE(I) = (RPRTLI(((I-1)*24)+8)*TX + RPRTLI(((I-1)*24)+9)
     1           *TX**(THREE/TWO) + RPRTLI(((I-1)*24)+10)*TX**THREE
     1           + RPRTLI(((I-1)*24)+11)*TX**SIX) / TR
            PVPWI(I) = RPRTLI(((I-1)*24)+5) * DEXP(PVPWE(I))
C---- VAPOR PRESSURE IN bar -> Pa
            PVPWI(I) = PVPWI(I) * TEN**FIVE
C      PVPWI(I) = RPRTLI(((I-1)*24)+5) * DEXP(PVPWE)
C---- derivative d (vapor pressure) / d (temperature)
C       DPDT = DEXP(PVPWE) *
C     1 ( PVPWE / TR - (RPRTLI(8) + THREE/TWO*RPRTLI(9)*TX**(ONE/TWO) +
C     1         THREE*RPRTLI(10)*TX**TWO + SIX*RPRTLI(11)*TX**FIVE) / TR)
C
         ELSE
C---- ANTOINE CORRELATION OF VAPOR PRESSURE (Pa)
            PVPWI(I) = EXP(RPRTLI(((I-1)*24)+8) -
     1           (RPRTLI(((I-1)*24)+9)/(TCOR + RPRTLI(((I-1)*24)+10))))
         ENDIF
C---- USE RAOULT'S LAW
         PVPW = PVPW + XIP(I)*PVPWI(I)
      ENDDO
C
      RETURN
      END
C***********************************************************************
      SUBROUTINE CPIPA(NSPECP,XMOLP,TCOR,RPRTLI,CPI)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *             CALCULATION OF THE HEAT CAPACITY              *
C     *              (Correlation: Rowlinson-Bondi)               *
C     *                  A.Schubert  09/2003                      *
C     *                   R.Emig     02/2005                      *
C     *************************************************************
C
C     INPUT:
C     NSPECP     =  NUMBER OF SPECIES IN PARTICLE
C     XMOLP      =  MOLAR MASS OF SPECIES IN PARTICLE
C     TCOR       =  TEMPERATURE (FROM INRCOR)
C
C     OUTPUT:
C     CPI        =  HEAT CAPACITY OF SPECIES IN PARTICLE
C***********************************************************************
C     CALCULATED VALUES NOT THAT GOOD, COULD BE IMPROVED
C
C     IN FACT: Rowlinson-Bondi IS NOT SUITABLE FOR CALCULATING
C     HEAT CAPACITIES OF ALCOHOLS FOR T < 0.75*T_R
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C
      DIMENSION XMOLP(NSPECP)
      DIMENSION CPI(NSPECP)
      DIMENSION CP0(NSPECP),TRI(NSPECP)
      DOUBLE PRECISION, DIMENSION(24*NSPECP) :: RPRTLI
C
      PARAMETER (ZERO=0.0D0,ONE=1.0D0,TWO=2.0D0,THREE=3.0D0,HALF=0.5D0,
     1     R = 8.31433D0)
C***********************************************************************
      CPI(1:NSPECP) = ZERO
      DO I=1, NSPECP
C***********************************************************************
       NLIPRO = INT (RPRTLI(((I-1)*24)+1) + HALF)
C---- REDUCED TEMPERATURE
       TRI(I) = TCOR / RPRTLI(((I-1)*24)+6)
C***********************************************************************
       CP0(I) = RPRTLI(((I-1)*24)+12) + RPRTLI(((I-1)*24)+13) * TCOR
     1  + RPRTLI(((I-1)*24)+14) * TCOR**TWO
     1  + RPRTLI(((I-1)*24)+15) * TCOR**THREE
       IF (NLIPRO.LT.10) THEN
          CPI(I) = R * (1.45D0 + 0.45D0 * (ONE - TRI(I))**(-ONE)
     1         + 0.25D0*RPRTLI(((I-1)*24)+4) * (17.11D0 + 25.2D0
     1         * (ONE-TRI(I))**(ONE/THREE) * TRI(I)**(-ONE)
     1         + 1.742D0 * (ONE - TRI(I))**(-ONE))) + CP0(I)
          CPI(I) = CPI(I) / XMOLP(I)
       ELSE
          CPI(I) = CP0(I)
       ENDIF
C      WRITE(*,*)'CPI', CPI(I) ,'J/kgK
      ENDDO
      RETURN
      END
C***********************************************************************
      SUBROUTINE THECONPA(NSPECP,XMOLP,W,RHOI,TCOR,PCORDE,RPRTLI,XLA)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *          Calculation of the thermal conductivity          *
C     *                  A.Schubert  09/2003                      *
C     *                   R.Emig     02/2005                      *
C     *************************************************************
C
C     INPUT:
C     NSPECP     =  NUMBER OF SPECIES IN PARTICLE
C     XMOLP      =  MOLAR MASS OF SPECIES IN PARTICLE
C     W          =  MASS FRACTIONS OF SPECIES IN PARTICLE
C     RHOI       =  DENSITIES OF SPECIES IN PARTICLE
C     TCOR       =  TEMPERATURE
C     PCORDE     =  AMBIENT PRESSURE
C
C     OUTPUT:
C     XLA        =  THERMAL CONDUCTIVITY OF THE PARTICLE
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C
      DIMENSION XMOLP(NSPECP),W(NSPECP),RHOI(NSPECP)
      DIMENSION XLAI(NSPECP),TRI(NSPECP)
      DIMENSION XIP(NSPECP), VI(NSPECP)
      DOUBLE PRECISION, DIMENSION(24*NSPECP) :: RPRTLI
C
      PARAMETER (ZERO=0.0D0,ONE=1.0D0,TWO=2.0D0,THREE=3.0D0,FIVE=5.0D0,
     1     SIX=6.0D0, TEN=10.0D0,HALF=0.5D0)
C***********************************************************************
C---- EQUATION FOR EACH SPECIES (Latini)
C***********************************************************************
      DO I=1, NSPECP
C***********************************************************************
C---- REDUCED TEMPERATURE
       TRI(I) = TCOR / RPRTLI(((I-1)*24)+6)
C***********************************************************************
       NLIPRO = INT (RPRTLI(((I-1)*24)+1) + HALF)
C
       IF (NLIPRO.NE.4) THEN
          XLAA0 = (RPRTLI(((I-1)*24)+16) * RPRTLI(((I-1)*24)+7)
     1         **RPRTLI(((I-1)*24)+17))
     1         / ((XMOLP(I) * TEN ** THREE) ** RPRTLI(((I-1)*24)+18)*
     1         RPRTLI(((I-1)*24)+6)**RPRTLI(((I-1)*24)+19))
C---- PRESSURE DEPENDENCE OF THERMAL CONDUCTIVITY
C for saturated hydrocarbons
          IF ((NLIPRO.EQ.1) .OR. (NLIPRO.EQ.5)) THEN
             XLAA1 = 0.0673D0 / (XMOLP(I)*TEN**THREE)**0.84D0
C for aromatics
          ELSEIF (NLIPRO.EQ.3) THEN
             XLAA1 = 102.5D0 / (XMOLP(I)*TEN**THREE)**2.4D0
C without pressure correlation
          ELSE
             XLAA1 = 0.D0
          ENDIF
C----
          XLAA = XLAA0 +
     1         XLAA1 * (PCORDE/(RPRTLI(((I-1)*24)+5)*TEN**FIVE))
          XLAI(I) = (XLAA * (ONE - TRI(I))**0.38D0) / TRI(I)**(ONE/SIX)
C----  OTHER (SIMPLE) CORRELATION FOR THERMAL CONDUCT. (WATER)
C----  Miller et al., Reid p. 544
       ELSEIF (NLIPRO.EQ.4) THEN
          XLAI(I) = RPRTLI(((I-1)*24)+16) + RPRTLI(((I-1)*24)+17)*TCOR
     1         + RPRTLI(((I-1)*24)+18) * TCOR**2
       ENDIF
      ENDDO
C***********************************************************************
C---- CALCULATION FOR THE MIXTURE (Li)
C***********************************************************************
      XLA = ZERO
      SUM = ZERO
C---- CALCULATION OF MEAN MOLAR MASS
      CALL CALXMO(NSPECP,W,XMOLP,XM)
      DO I = 1,NSPECP
C----   GET MOLAR FRACTIONS
        XIP(I) = W(I) * XM / XMOLP(I)
C----   GET MOLAR VOLUMES
        VI(I) = XMOLP(I) / RHOI(I)
C----   CALCULATION OF SUM FOR EQUATION (Li)
        SUM = SUM + XIP(I) * VI(I)
      ENDDO
C***********************************************************************
C     EQUATION
C***********************************************************************
      DO I=1,NSPECP
         PHII = XIP(I) * VI(I) / SUM
         DO J=1,NSPECP
            PHIJ = XIP(J) * VI(J) / SUM
            XLAIJ = 2*(( XLAI(I)**(-ONE) + XLAI(J)**(-ONE) ) ** (-ONE))
            XLA = XLA + PHII*PHIJ*XLAIJ
         ENDDO
      ENDDO
C      WRITE(6,*)'XLA ',XLA ,'W/mK'
      RETURN
      END
C***********************************************************************
      SUBROUTINE HIVAPA(NSPECP,XMOLP,TCOR,RPRTLI,HI)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *        Calculation of the enthalpy of evaporation         *
C     *          (Correlation:  Watson,Viswanath+Riedel)          *
C     *                  A.Schubert  09/2003                      *
C     *                   R.Emig     02/2005                      *
C     *************************************************************
C
C     INPUT:
C     NSPECP     =  NUMBER OF SPECIES IN PARTICLE
C     XMOLP      =  MOLAR MASS OF SPECIES IN PARTICLE
C     TCOR       =  TEMPERATURE (FROM INRCOR)
C
C     OUTPUT:
C     HI         =  ENTHALPIES OF EVAPORATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C
      DIMENSION XMOLP(NSPECP),TRI(NSPECP)
      DIMENSION HI(NSPECP)
      DOUBLE PRECISION, DIMENSION(24*NSPECP) :: RPRTLI
C
      PARAMETER (ZERO=0.0D0,ONE=1.0D0,TEN=10.0D0,HALF=0.5D0,
     1     R = 8.31433D0)
C***********************************************************************
      HI(1:NSPECP) = ZERO
C
      DO I=1, NSPECP
C***********************************************************************
C---- REDUCED TEMPERATURE
         TRI(I) = TCOR / RPRTLI(((I-1)*24)+6)
         IF(TRI(I) .GT. ONE) TRI(I) = 0.99D0
C
         NLIPRO = INT (RPRTLI(((I-1)*24)+1) + HALF)
C***********************************************************************
         IF (NLIPRO.LT.10) THEN
C---- RIEDEL EQUATION
            HIVAP = 1.093D0 * R * RPRTLI(((I-1)*24)+6)
     1           *((RPRTLI(((I-1)*24)+7) /
     1           RPRTLI(((I-1)*24)+6))
     1           *((DLOG(RPRTLI(((I-1)*24)+5))-1.013D0)) /
     1           (0.93D0 - (RPRTLI(((I-1)*24)+7)
     1           / RPRTLI(((I-1)*24)+6))))
C--- EXPONENT EQUATION OF VISWANATH, KULOOR
            HIN = (0.00264D0 * (HIVAP / (R * RPRTLI(((I-1)*24)+7)))
     1           + 0.8794D0) ** TEN
C---- WATSON EQUATION
            HI(I) = HIVAP * ((ONE-TRI(I)) / (ONE-(RPRTLI(((I-1)*24)+7)/
     1           RPRTLI(((I-1)*24)+6))))**HIN
         ELSE
C---- EQUATION OF YAWS
            HI(I) = RPRTLI(((I-1)*24)+23)
     1           * (ONE-TRI(I))**(RPRTLI(((I-1)*24)+24))
         ENDIF
C---- ENTHALPY OF EVAPORATION IS SET NEGATIVE
       HI(I) = - HI(I)/XMOLP(I)
c	write(*,*)'HI',HI,'J/kg'
      ENDDO
      RETURN
      END
C***********************************************************************
      SUBROUTINE CALVISPA(NSPECP,XMOLP,RHOI,TCOR,RPRTLI,VISPAI)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *             Calculation of liquid viscosity               *
C     *         (Correlation:  Letsou+Stiel - Brule+Starling)     *
C     *       (Correlation:  Van Velzen,Yaws+Duhne - for water)   *
C     *                   R.Emig     02/2005                      *
C     *************************************************************
C
C     INPUT:
C     NSPECP     =  NUMBER OF SPECIES IN PARTICLE
C     XMOLP      =  MOLAR MASS OF SPECIES IN PARTICLE
C     RHOI       =  DENSITIES OF SPECIES IN PARTICLE
C     TCOR       =  TEMPERATURE (FROM INRCOR)
C
C     OUTPUT:
C     VISPAI     =  VISCOSITY OF SPECIES IN cP = 10E-3 N*s/m**2
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,K,O-Z)
C
      DIMENSION XMOLP(NSPECP), RHOI(NSPECP)
      DIMENSION VISPAI(NSPECP)
      DIMENSION TRI(NSPECP),EI(10), GI(2)
      DOUBLE PRECISION, DIMENSION(24*NSPECP) :: RPRTLI
C
      PARAMETER (HALF=0.5D0,ZERO=0.0D0,ONE=1.0D0,TWO=2.0D0,THREE=3.0D0,
     1           FOUR=4.0D0,SIX=6.0D0,TEN=10.0D0)
C***********************************************************************
      DO I=1, NSPECP
C---- REDUCED TEMPERATURE
         TRI(I) = TCOR / RPRTLI(((I-1)*24)+6)
         NLIPRO = INT (RPRTLI((24*(I-1))+1) + HALF)
         IF (NLIPRO.EQ.4) THEN
C***********************************************************************
C_RAEM  CALCULATION FOR WATER (Van Velzen+Yaws+Duhne)
C***********************************************************************
            VISPAI(I) = EXP(-2.471D1+4.209D3/TCOR+4.527D-2*TCOR-
     1           3.376D-5*TCOR**TWO)
         ELSEIF (TRI(I).GE.0.75D0) THEN
C***********************************************************************
C---- CALCULATION FOR REDUCED TEMP. >=0.75 (Letsou+Stiel)
C***********************************************************************
            ETA0 = (TEN**(-THREE))*(2.648D0 - 3.725D0*TRI(I) + 1.309D0*
     1             TRI(I)**TWO)
            ETA1 = (TEN**(-THREE))*(7.425D0 - 13.39D0*TRI(I) + 5.933D0*
     1             TRI(I)**TWO)
            KSI  = 0.176D0*((RPRTLI(((I-1)*24)+6)) / (((XMOLP(I)*TEN
     1             **THREE)**THREE)*(RPRTLI(((I-1)*24)+5)**FOUR)))**
     1             (ONE/SIX)
            VISPAI(I) = (ETA0 + RPRTLI(((I-1)*24)+4)*ETA1) / KSI
         ELSE
C***********************************************************************
C---- CALCULATION FOR REDUCED TEMP. <0.75 (Brule+Starling)
C***********************************************************************
            TMUL = 1.2593D0*TRI(I)
            OMEGAV = (1.16145D0*TMUL**(-0.14874D0))+0.52487D0*(EXP(
     1           -0.7732D0*TMUL))+2.16178D0*(EXP(-2.43787D0*TMUL))
C---- REDUCED DIPOLE MOMENT
C---- UYR = 131.3 * MU(DEBYE) / (V_C(CM^3/MOL) * T_C(K))
            UYR = 131.3D0*RPRTLI(((I-1)*24)+21)/
     1           ((RPRTLI(((I-1)*24)+20)*
     1           RPRTLI(((I-1)*24)+6))**HALF)
            FC = ONE - 0.2756D0*RPRTLI(((I-1)*24)+4)+
     1           0.059035D0*UYR**FOUR+RPRTLI(((I-1)*24)+22)
            EI(1) = 17.45D0 + 34.063D0*RPRTLI(((I-1)*24)+4)
            EI(2) = -9.611D-4 + 7.235D-3*RPRTLI(((I-1)*24)+4)
            EI(3) = 51.043D0 + 169.46D0*RPRTLI(((I-1)*24)+4)
            EI(4) = -0.6059D0 + 71.174D0*RPRTLI(((I-1)*24)+4)
            EI(5) = 21.382D0 - 2.11D0*RPRTLI(((I-1)*24)+4)
            EI(6) = 4.668D0 - 39.941D0*RPRTLI(((I-1)*24)+4)
            EI(7) = 3.762D0 + 56.623D0*RPRTLI(((I-1)*24)+4)
            EI(8) = 1.004D0 + 3.14D0*RPRTLI(((I-1)*24)+4)
            EI(9) = -7.774D-2 - 3.584D0*RPRTLI(((I-1)*24)+4)
            EI(10)= 0.3175D0 + 1.16D0*RPRTLI(((I-1)*24)+4)
            Y = (RHOI(I)*TEN**(-SIX)/XMOLP(I))*RPRTLI(((I-1)*24)+20)
     1          /SIX
            GI(1) = (ONE - HALF*Y) / ((ONE - Y)**THREE)
            GI(2) = (EI(1)*((ONE-EXP(-EI(4)*Y))/Y) + EI(2)*GI(1)*
     1           EXP(EI(5)*Y) + EI(3)*GI(1)) / (EI(1)*EI(4)+EI(2)+EI(3))
            VISPASS = EI(7)*(Y**TWO)*GI(2)*EXP(EI(8)+EI(9)*TMUL**(-ONE)+
     1           EI(10)*TMUL**(-TWO))
            VISPAS = (TMUL**HALF)/OMEGAV*(FC*(GI(2)**(-ONE)+EI(6)*Y))+
     1           VISPASS
            VISPAI(I) = (VISPAS*36.344D0*
     1           ((XMOLP(I)*10**THREE*RPRTLI(((I-1)*24)+6))**HALF)/
     1           (RPRTLI(((I-1)*24)+20)**(TWO/THREE)))
     1           *TEN**(-FOUR)
         ENDIF
       ENDDO
C_RAEMtest
c       WRITE(6,*)'viscosity [cP=10E-3 Ns/m**2]',VISPAI
       RETURN
       END
C***********************************************************************
       SUBROUTINE XDIPA(NSPECP,XMOLP,W,RHOI,TCOR,PVPI,PCOR,RPRTLI,XDI)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *           Calculation of diffusion coefficients           *
C     *            (Correlation:  Tyn-Calus + Vignes)             *
C     *                    R.Emig     03/2005                     *
C     *************************************************************
C
C     INPUT:
C     NSPECP     =  NUMBER OF SPECIES IN PARTICLE
C     XMOLP      =  MOLAR MASS OF SPECIES IN PARTICLE
C     W          =  MASS FRACTIONS OF SPECIES IN PARTICLE
C     RHOI       =  DENSITIES OF SPECIES IN PARTICLE
C     TCOR       =  TEMPERATURE (FROM INRCOR)
C     PVPI       =  VAPOR PRESSURES OF SPECIES IN PARTICLE
C     PCOR       =  AMBIENT PRESSURE
C
C     OUTPUT:
C     XDI        =  DIFFUSION COEFFICIENTS
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,K,O-Z)
C
      DIMENSION XMOLP(NSPECP),W(NSPECP),RHOI(NSPECP)
      DIMENSION VISPAI(NSPECP),PVPI(NSPECP)
      DIMENSION VI(NSPECP),XIP(NSPECP),XD0(NSPECP,NSPECP), XDIM(NSPECP,
     1          NSPECP)
      DIMENSION TBRI(NSPECP),ALPHI(NSPECP),SIGMAI(NSPECP)
      DIMENSION XDI(NSPECP)
      DOUBLE PRECISION, DIMENSION(24*NSPECP) :: RPRTLI
C
      PARAMETER (HALF=0.5D0,ZERO=0.0D0,ONE=1.0D0,TWO=2.0D0,THREE=3.0D0,
     1           FOUR=4.0D0,SIX=6.0D0,EIGHT=8.0D0,OINE=9.0D0,
     1           TEN=10.0D0,ELEVEN=11.0D0)
C***********************************************************************
C---- get viscosities
      VISPAI(1:NSPECP) = ZERO
      CALL CALVISPA(NSPECP,XMOLP,RHOI,TCOR,RPRTLI,VISPAI)
C---- calculate molar volumes [cm*3/mol] at boiling temperature
      DO I=1,NSPECP
         TB = RPRTLI(((I-1)*24)+7)
         VI(I) = XMOLP(I) /
     1           DSPEPA(I,NSPECP,XMOLP,TB,PVPI(I),PCOR,RPRTLI)
     1           *TEN**SIX
      ENDDO
C***********************************************************************
C     CALCULATION OF COEFFICIENTS XD0(I,J) OF I IN PURE J
C***********************************************************************
      DO I=1,NSPECP
         NLIPRO = INT (RPRTLI((24*(I-1))+1) + HALF)
         DO J=1,NSPECP
C----       self-diffusion coefficient not needed here
            IF (I.EQ.J) THEN
               XD0(I,J) = ZERO
            ELSE
C***********************************************************************
C     TYN - CALUS CORRELATION
C***********************************************************************
C     RESTRICTIONS: REID et al., p.600
C***********************************************************************
C----          test if the actual solute is water
               IF (NLIPRO.EQ.4) THEN
                  VIDUMI=37.4D0
               ELSE
                  VIDUMI = VI(I)
               ENDIF
C***********************************************************************
               DO L=1,NSPECP
                TBRI(L)=RPRTLI((24*(L-1))+7)/RPRTLI((24*(L-1))+6)
                ALPHI(L)=0.9076D0*(ONE +
     1               (TBRI(L)*LOG(RPRTLI((24*(L-1))+5)
     1               /1.013D0))/(ONE-TBRI(L)))
                SIGMAI(L)=RPRTLI((24*(L-1))+5)**(TWO/THREE)
     1               *RPRTLI((24*
     1               (L-1))+6)**(ONE/THREE)*(0.132*ALPHI(L)-0.278D0)*
     1               (ONE-TBRI(L))**(ELEVEN/OINE)
               ENDDO
C***********************************************************************
C---- CALCULATE DIFFUSION COEFFICIENT IN cm2/s
C***********************************************************************
               XD0(I,J) = 8.93D0*TEN**(-EIGHT)*(VI(J)**0.267D0/VIDUMI**
     1              0.433D0)*TCOR/VISPAI(J)*(SIGMAI(J)/SIGMAI(I))**
     1              0.15D0
C***********************************************************************
C---- CALCULATE DIFFUSION COEFFICIENT IN m2/s
C***********************************************************************
               XD0(I,J) = XD0(I,J) * (TEN**(-FOUR))
c             ENDIF
            ENDIF
         ENDDO
      ENDDO
C***********************************************************************
C     CALCULATION OF COEFFICIENTS XDI
C***********************************************************************
C-----calculation of molar fractions
      CALL CALXMO(NSPECP,W,XMOLP,XMOLM)
      DO I = 1,NSPECP
        XIP(I) = W(I) * XMOLM / XMOLP(I)
      ENDDO
C_RAEM
C---- calculation of ALPHA not yet implemented; ideal solution
      ALPHA = ONE
C***********************************************************************
C_RAEM     VIGNES CORRELATION FOR BINARY MIXTURES
C***********************************************************************
      XDIM(1:NSPECP,1:NSPECP) = ZERO
      DO I=1,NSPECP
         DO J=1,NSPECP
C_rst
            IF (J.NE.I) THEN
               IF((XIP(I).GE.ONE) .OR. (XIP(J).LT.ZERO)) THEN
                  XDIM(I,J) = XD0(J,I)
               ELSEIF((XIP(J).GE.ONE) .OR. (XIP(I).LT.ZERO)) THEN
                  XDIM(I,J) = XD0(I,J)
               ELSE
                  XDIM(I,J) = ((XD0(I,J)**XIP(J))*
     1                         (XD0(J,I)**XIP(I)))*ALPHA
               ENDIF
            ENDIF
         ENDDO
      ENDDO
C***********************************************************************
C_RAEM     SIMPLIFIED DIFF. COEFF. FOR MULTI COMPONENT MIXTURES
C_RAEM     (LIKE IN GAS PHASE)
C***********************************************************************
      DO I=1,NSPECP
         SUM = ZERO
         DO J=1,NSPECP
           IF (J.NE.I) SUM = SUM + XIP(J)/XDIM(J,I)
         ENDDO
         XDI(I) = (ONE - XIP(I))/SUM
      ENDDO
c      WRITE(6,*)'XDI ',XDI ,'m**2/s'
      RETURN
      END
C***********************************************************************
      SUBROUTINE GENSME(NSSPEC,SYMB,NFISTO,NFIMEC,MSGFIL,IPRI,IERR,
     1    NSREAC,NRINSS,NINERT,NIARRHS,NMATLLS,NMATLRS,
     2    NPKS,NTEXPS,NEXAS,NVT4S,NVT5S,NVT6S,NSTICK,NNUMC,NSREV,NSRVEC,
     3    LIW,LIREAS,IREAS,LRREAS,RREAS)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *  INPUT OF THE REACTION MECHANISM DATA FROM UNIT  NFISTO   *    *
C     *                      10/03/1988                           *    *
C     *              LAST CHANGE: 10/03/1988/MAAS                 *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C     STORAGE ORGANIZATION, INITIAL VALUES                             *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C---- DIMENSIONS AND TYPES OF FORMAL PARAMETERS
      CHARACTER(LEN=*) SYMB(*)
      DIMENSION RREAS(LRREAS)
      DIMENSION IREAS(LIREAS)
C***********************************************************************
C     INPUT OF NUMBERS                                                 *
C***********************************************************************
      READ(NFISTO,801)  NSREAC
      READ(NFISTO,801)  NDUMMY
      IF(IPRI.GE.2) WRITE(MSGFIL,801)  NSREAC
C***********************************************************************
C
C     INPUT OF REACTION EQUATIONS                                      *
C
C***********************************************************************
C***********************************************************************
C
C***********************************************************************
      NMATLLS= 1
      NMATLRS= NMATLLS + 6*NSREAC
      NIARRHS= NMATLRS + 6*NSREAC
      NSTICK = NIARRHS + (NSREAC+1)
      NNUMC  = NSTICK  + NSREAC
C
      NPKS   =  1
      NTEXPS = NPKS  + NSREAC
      NEXAS  = NTEXPS+ NSREAC
      NVT4S  = NEXAS + NSREAC
      NVT5S  = NVT4S + NSREAC
      NVT6S  = NVT5S + NSREAC
      NRNEED = NVT6S + NSREAC - 1
      IF(NRNEED.GT.LRREAS) GOTO 910
C***********************************************************************
C
C***********************************************************************
      DO M=1,NSREAC
         READ(NFISTO,854)
     1        (IREAS(NMATLLS-1+6*(M-1)+I),I=1,6),
     1        (IREAS(NMATLRS-1+6*(M-1)+I),I=1,6),
     1        RREAS(NPKS-1+M),RREAS(NTEXPS-1+M),
     1        RREAS(NEXAS-1+M),
     1        IREAS(NIARRHS-1+M),IREAS(NSTICK-1+M),IREAS(NNUMC-1+M),
     1        RREAS(NVT4S-1+M),RREAS(NVT5S-1+M),RREAS(NVT6S-1+M)
      ENDDO
  854 FORMAT(12(1X,I5),/,36X,3X,3(1PE11.4,1X),/,
     1       3(1X,I5),18X,3X,3(1PE11.4,1X))
C**********************************************************************C
C     INPUT OF REACTION POINTERS                                      C
C**********************************************************************C
      READ(NFISTO,853) NRINSS
      NSRVEC = NNUMC + NSREAC + 1
      IF(NSRVEC-1.GT.LIREAS) GOTO 920
  853 FORMAT(I5)
      READ(NFISTO,851)
     1    ((IREAS(NSRVEC-1+(J-1)*3+I),I=1,3),J=1,NRINSS)
  851 FORMAT(4(2X,3I5))
C***********************************************************************
C     INPUT OF INFORMATION ABOUT REVERSE REACTION AND INERT SPECIES    *
C***********************************************************************
      NSREV=NSRVEC+3*NRINSS
      IF(NSREV-1.GT.LIREAS) GOTO 920
      NEEDIR=NSREV+NSREAC-1
      IF(NEEDIR.GT.LIREAS) GOTO 920
      READ(NFISTO,806)                 (IREAS(NSREV-1+I),I=1,NSREAC)
      IF(IPRI.GE.2) WRITE(MSGFIL,806)  (IREAS(NSREV-1+I),I=1,NSREAC)
      READ(NFISTO,801)                  NINERT
      IF(IPRI.GE.2) WRITE(MSGFIL,801)   NINERT
C***********************************************************************
C     REGULAR EXIT                                                     *
C***********************************************************************
      IERR=0
      RETURN
C***********************************************************************
C     ERROR EXIT                                                       *
C***********************************************************************
  910 WRITE(MSGFIL,911) NRNEED,LRREAS
  911 FORMAT('  NEEDED STORAGE FOR RREAC(',I5,') EXCEEDS GIVEN SPACE(',
     1  I5,')',/,'     ----->  CHECK DIMENSION OF RREAC  ')
      IERR=1
      GOTO 999
  920 WRITE(MSGFIL,921)
  921 FORMAT('   NOT ENOUGH SPACE PROVIDED FOR IREAC',/,
     1      '     ----->  CHECK DIMENSION OF IREAC  ')
      IERR=2
      GOTO 999
  999 CONTINUE
      WRITE(MSGFIL,998)
      WRITE(MSGFIL,998)
      WRITE(MSGFIL,998)
  998 FORMAT(' ','++++++      ERROR IN -GENSME-      ++++++')
      RETURN
C***********************************************************************
C     FORMAT STATEMENTS                                                *
C***********************************************************************
  801 FORMAT(I5,9X,I5)
C 802 FORMAT(12X,5(E11.4,1X))
  804 FORMAT(1X,A8,3X,5(E11.4,1X))
  806 FORMAT(12X,30I2)
C---- SPECIAL FORMATS FOR TEST OUTPUT
  811 FORMAT(' ',11X,5(1PE11.4,1X)/(12X,5(1PE11.4,1X)))
C***********************************************************************
C     END OF -MECINP-                                                  *
C***********************************************************************
      END
