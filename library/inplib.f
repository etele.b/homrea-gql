      SUBROUTINE REMCOM(NFILE6,NFILIN,NFIOUT)
C**********************************************************************C
C                                                                      C
C    ***************************************************************   C
C    *                                                             *   C
C    *       PROGRAM TO REMOVE COMMENT CARDS FROM DATASETS         *   C
C    *                                                             *   C
C    *                                                             *   C
C    ***************************************************************   C
C                                                                      C
C**********************************************************************C
C---- Storage Organization, Initialization
      IMPLICIT NONE
      INTEGER LINLEN,NFILE6,NFILIN,NFIOUT
      PARAMETER (LINLEN=255)
      CHARACTER (LEN=LINLEN) STRING
C---- Read and Re-Write
    2 CONTINUE
    1 FORMAT(A)
      READ(NFILIN,1,END=3,ERR=4) STRING           
      IF(STRING(1:4).EQ.'****') GO TO 2
      IF(STRING(1:4).EQ.'----') GO TO 2
      IF(STRING(1:1).EQ.'!')    GO TO 2
      IF(LEN_TRIM(STRING).EQ.0) GO TO 2
      WRITE(NFIOUT,1) TRIM(STRING) 
      GO TO 2
    3 CONTINUE
      REWIND NFIOUT
      RETURN
C***********************************************************************
C     Error Exit
C***********************************************************************
    4 CONTINUE
    5 FORMAT(1X,5X,'ERROR - DEFECT INPUT ON UNIT ',I3)
      WRITE(NFILE6,5) NFILIN
      RETURN
C***********************************************************************
C     End of -REMCOM-
C***********************************************************************
      END
      SUBROUTINE MXIMAN(NSYMB,NSTOE,
     1           NFILE6,NR,PK,TEXP,EXA,VT4,VT5,VT6,IARRH,
     1           AINF,BINF,EINF,T3,T1,T2,AFA,BFA) 
C**********************************************************************C
C                                                                      C
C     **************************************************************   C
C     *                                                            *   C
C     *           PRINTING OUT ATION OF REACTION EQATIONS          *   C
C     *                                                            *   C
C     *              LAST CHANGE :  10/12/2017 U. Maas             *   C
C     *                                                            *   C
C     **************************************************************   C
C                                                                      C
C**********************************************************************C
C                                                                      C
C     INPUT  :                                                         C
C                                                                      C
C     NSYMB(VAR.)  = SPECIES SYMBOLS       (CHARACTER*8)               C
C     NSTOE(1)     = SPECIES INDICES                                   C
C     NFILE6       = UNIT OF OUTPUT ON PRINTER                         C
C     SURF           = SURFACE REACTION FLAG                           C
C                                                                      C
C     OUTPUT :                                                         C
C                                                                      C
C                                                                      C
C**********************************************************************C
C
C
C**********************************************************************C
C     STORAGE ORGANIZATION, INITIAL VALUES                             C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL LONG
      PARAMETER(LSMAX=30)
      CHARACTER(LEN=*)  NSYMB(*)
      PARAMETER(LENSTR=255)
      PARAMETER(LEOSTR=106)
      CHARACTER(LEN=LENSTR) STRING
      CHARACTER(LEN=LEOSTR) OSTRING
      CHARACTER(LEN=22) STRARRH    
      DIMENSION NSTOE(*)
C**********************************************************************
C      
C**********************************************************************
      LENSY=LEN(NSYMB(1))
C**********************************************************************
C**********************************************************************C
C     ASSIGN SYMBOLS to output string                                  C
C**********************************************************************C
       STRING (1:LENSTR)=REPEAT(' ',LENSTR)
       IFI=1
C---- ASSIGN SPECIES SYMBOLS
         INDE=NSTOE(1)
         IF(INDE.NE.0) THEN
           STRING(IFI:IFI-1+LENSY)=NSYMB(INDE)
           IFI=IFI+LENSY
         ENDIF
      DO I=2,6
         INDE=NSTOE(I)
         IF(INDE.NE.0) THEN
           STRING(IFI:IFI-1+LENSY+3)=' + '//NSYMB(INDE)
           IFI = IFI+LENSY+3
         ENDIF
      ENDDO
CBUG
C         STRING(IFI:IFI-1+4)=' > '//NSYMB(INDE)
          STRING(IFI:IFI-1+4)=' > '
          IFI = IFI+3
          INDE=NSTOE(7)
         IF(INDE.NE.0) THEN
           STRING(IFI:IFI-1+LENSY)=NSYMB(INDE)
           IFI=IFI+LENSY
         ENDIF
      DO I=8,12
         INDE=NSTOE(I)
         IF(INDE.NE.0) THEN
           STRING(IFI:IFI-1+LENSY+3)=' + '//NSYMB(INDE)
           IFI = IFI+LENSY+3
         ENDIF
      ENDDO
          IFI = IFI - 1
C**********************************************************************C
C     CHECK TASK                                                       C
C**********************************************************************C
      STRARRH=' '
      IF(IARRH.EQ.3.OR.IARRH.EQ.0.OR.IARRH.EQ.2)  THEN
           WRITE(STRARRH,'(1PE9.2,0PF6.2,-3PF7.1)') PK,TEXP,EXA
      ELSE
      STRARRH=' calc. during inetgr.'
      ENDIF 
      ILAS=IFI
      LONG = .FALSE.
      IF(ILAS.GT.LEOSTR) THEN
         LONG=.TRUE.
         IFI= INDEX(STRING(1:LENSTR),'>')
         OSTRING(1:LEOSTR) = ' '
         OSTRING(1:IFI-1) = STRING(1:IFI-1) 
         WRITE(NFILE6,833) NR,OSTRING,STRARRH
         ILA =LEN(TRIM(STRING))
         OSTRING(1:LEOSTR) = ' '
         OSTRING(1:1+ILAS-IFI) = STRING(IFI:ILAS) 
           WRITE(NFILE6,834) OSTRING
      ELSE
         OSTRING(1:LEOSTR) = ' '
         OSTRING(1:ILAS) = STRING(1:ILAS) 
         WRITE(NFILE6,833) NR,OSTRING,STRARRH       
      ENDIF
      IF(IARRH.EQ.1) WRITE(NFILE6,941) VT4,VT5,VT6
      IF(IARRH.EQ.2) WRITE(NFILE6,831) AINF,BINF,EINF,T3,T1,T2,AFA,BFA
 833  FORMAT(' ',I4,3X,A,A)  
 834  FORMAT(' ',5X,A)
C821  FORMAT(' ',I4,3X,5(A,A2),A,' calc. during intergr.')
  831 FORMAT(2X,'FALLOFF  ',45X,1PE9.2,0PF6.2,-3PF6.1,/,
     1         32X,3(1PE9.2,1X),1X,1PE9.2,2X,0PF4.2)
C 841 FORMAT(2X,'VT-RELAXATION  ',28X,3(1PE10.3,2X))
 941  FORMAT(2X,'COV.-DEP. ACTIVATION ENERGY  ',14X,3(1PE10.3,2X))
      RETURN
C**********************************************************************C
C     END OF -MXIMAN-                                                  C
C**********************************************************************C
      END
      SUBROUTINE INIMRP(NS,NFILE6,PK,TEXP,EXA,
     1           VT4,VT5,VT6,IARRH,HCPL,HCPH,HINF,MAT,
     1           PKR,TEXPR,EXAR,VT4R,VT5R,VT6R,IARRHR)
C**********************************************************************C
C                                                                      C
C    ***************************************************************   C
C    *                                                             *   C
C    *     PROGRAM FOR THE CALCULATION OF ARRHENIUS PARAMETERS     *   C
C    *                    OF  REVERSE REACTIONS                    *   C
C    *                          21.02.1983                         *   C
C    *              LAST CHANGE: 07/07/2005  R. STAUCH             *   C
C    *                                                             *   C
C    ***************************************************************   C
C                                                                      C
C**********************************************************************C
C                                                                      C
C     INPUT  :                                                         C
C                                                                      C
C     NS             = NUMBER OF SPECIES                               C
C     NFILE6         = NUMBER OF OUTPUT FILE FOR LISTING               C
C                                                                      C
C     OUTPUT :                                                         C
C                                                                      C
C     PK,TEXP,EXA(1) = PREEXPONENTIAL, TEMPERATURE COEFFICIENT, AND AC-C
C                      TIVATION ENERGY (NRMAX)                         C
C     NM             = NUMBER OF DIFFERENT THIRD BODIES INCLUDING TOTALC
C                      CONCENTRATION M. NM=3 AT THE MOMENT, SEE DATA   C
C                      STATEMENT. NM<6 IS NECESSARY.                   C
C     INERT(1)       = INERT SPECIES NUMBERS (NS)                      C
C     NINERT         = NUMBER OF INERT SPECIES                         C
C     IREVER(1)      = VECTOR FOR THE IDENTIFICATION OF REVERSE REAC-  C
C                      TIONS (NRMAX)                                   C
C     NR             = NUMBER OF REACTIONS                             C
C     STOP           = .TRUE. FOR NORMAL END, .FALSE. FOR ABNORMAL END C
C     SURF           = SURFACE REACTION FLAG                           C
C                                                                      C
C     INPUT FROM FILE INPF :                                           C
C                                                                      C
C                                                                      C
C     OUTPUT ON FILE NFILE3 :                                          C
C                                                                      C
C     STOICHIOMETRIC COEFFICIENTS AND ARRHENIUS PARAMETERS (M,MOL,S,J),C
C     INERT SPECIES, INFORMATION ON REVERSE REACTIONS                  C
C                                                                      C
C**********************************************************************C
C                                                                      C
C                                                                      C
C                                                                      C
C**********************************************************************C
C     STORAGE ORGANIZATION                                             C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION HCPL(7,*),HCPH(7,*),HINF(3,*),MAT(12)
C**********************************************************************C
C     DEFINITION OF INITIAL VALUES                                     C
C**********************************************************************C
C---- LOWER UND UPPER TEMPERATURE FOR CALC. OF THE REVERSE REACTION
C     DATA TU/600.0D0/,TO/2500.0D0/
C     DATA TU/1000.0D0/,TO/10000.0D0/
C---- GAS CONSTSANT RGAS(SI-UNITS), RBARCM(UNITS OF BAR,CM,MOLE)
      DATA RGAS/8.3143D0/,RBARCM/83.143D0/
C**********************************************************************C
C     INITIALIZATION
C**********************************************************************
C***********************************************************************
C     CALCULATE ARRHENIUS PARAMETERS FOR REVERSE REACTION
C***********************************************************************
Cmaiwald      IF(IARRH.NE.0) THEN
      IF(IARRH.EQ.1) THEN
        EXAR = EXA
        TEXPR = TEXP
        PKR = PK
        VT4R = VT4
        VT5R = VT5
        VT6R = VT6
        IARRHR = - IARRH
      ELSE
        VT4R = 0.0D0
        VT5R = 0.0D0
        VT6R = 0.0D0
C***********************************************************************
C     CALCULATE TO and TU
C***********************************************************************
      TU =     0.0D0
      TO =     1.0D30
      DO 305 II=1,6
      LL=MAT(II)
      IF(LL.EQ.0.OR.LL.GT.NS) GOTO 305
      TLOW = HINF(1,LL)
      TMID = HINF(2,LL)
      THIG = HINF(3,LL)
      TU = DMAX1(TU,TLOW+ 400.0D0)
      TO = DMIN1(TO,TMID+1500.0D0)
 305  CONTINUE
      IF(TO.GT.6000.0D0) TU = DMAX1(TU,1000.0D0)
C***********************************************************************
C     CALCULATE CHANGE OF MOLECULARITY, ENTHALPY, ENTROPY
C***********************************************************************
      IARRHR = IARRH
      MR=0
      HRO=0.0
      HRU=0.0
      SRO=0.0
      SRU=0.0
C---- SUM UP LEFT SIDE
      DO 310 II=1,6  
        LL=MAT(II)
        IF(LL.EQ.0.OR.LL.GT.NS) GOTO 310
        CALL CALHI(LL,TO,HCPL,HCPH,HINF,HO,NFILE6)
        CALL CALSI(LL,TO,HCPL,HCPH,HINF,SO,NFILE6)
        CALL CALHI(LL,TU,HCPL,HCPH,HINF,HU,NFILE6)
        CALL CALSI(LL,TU,HCPL,HCPH,HINF,SU,NFILE6)
        HRO=HRO-HO
        SRO=SRO-SO
        HRU=HRU-HU
        SRU=SRU-SU
        MR=MR-1
 310  CONTINUE
C---- SUM UP RIGHT SIDE
      DO 320 II=7,12
        LL=MAT(II)
        IF(LL.EQ.0.OR.LL.GT.NS) GOTO 320
        CALL CALHI(LL,TO,HCPL,HCPH,HINF,HO,NFILE6)
        CALL CALSI(LL,TO,HCPL,HCPH,HINF,SO,NFILE6)
        CALL CALHI(LL,TU,HCPL,HCPH,HINF,HU,NFILE6)
        CALL CALSI(LL,TU,HCPL,HCPH,HINF,SU,NFILE6)
        HRO=HRO+HO
        SRO=SRO+SO
        HRU=HRU+HU
        SRU=SRU+SU
        MR=MR+1
  320 CONTINUE
C***********************************************************************
C     CALCULATE EQUILIBRIUM CONSTANT KP, KC
C***********************************************************************
      GRU=HRU-TU*SRU
      GRO=HRO-TO*SRO
      HEHE1=-GRO/(RGAS*TO)
      HEHE2=-GRU/(RGAS*TU)
C---- ECP REFERS TO EQUILIBRIUM CONSTANT FOR PARTIAL PRESSURES
C---- UNIT IS BAR**(-MR)
      ECPO=DEXP(-GRO/(RGAS*TO))
      ECPU=DEXP(-GRU/(RGAS*TU))
C---- ECC REFERS TO EQUILIBRIUM CONSTANT FOR CONCENTRATIONS (MOLE,CM)
C         DIVISION BY FACTOR :   R(BAR,CM)**MR
      ECCO=ECPO/(RBARCM*TO)**MR
      ECCU=ECPU/(RBARCM*TU)**MR
      IF(ECCO.EQ.0) WRITE(NFILE6,831)
      IF(ECCO.EQ.0) ECCO=1.0E-35
      IF(ECCU.EQ.0) WRITE(NFILE6,831)
      IF(ECCU.EQ.0) ECCU=1.0E-35
C***********************************************************************
C     CALCULATE REVERSE RATE COEFFICIENTS
C***********************************************************************
C---- CALCULATE FOREWARD RATES AT TU AND TO
      RCFO=PK*DEXP(-EXA/(RGAS*TO))
      RCFU=PK*DEXP(-EXA/(RGAS*TU))
      RCRO=RCFO/ECCO
      RCRU=RCFU/ECCU
      EACT=RGAS*DLOG(RCRU/RCRO)/(1.0/TO-1.0/TU)
      PKR =RCRO*DEXP(EACT/(RGAS*TO))
C***********************************************************************
C     STORE REVERSE REACTION
C***********************************************************************
      EXAR         = EACT
      PKR          = PKR
      TEXPR        = TEXP
      ENDIF
      RETURN
C**********************************************************************C
C     FORMAT STATEMENTS                                                C
C**********************************************************************C
  831 FORMAT('0',5X,'INCORRECT REVERSE REACT. ARRH. PARAM. ',
     1       'DUE TO BAD THERMODYN. OR INPUT DATA')
C 835 FORMAT('0',5X,'ERROR IN -INIMRP- : REVERSE REACTIONS FOR FALLOFF',
C    1       ' ARE NOT ADMITTED')
C**********************************************************************C
C     END OF -INIMRP-                                                  C
C**********************************************************************C
      END
      SUBROUTINE GENCEF(NR,NS,NM,NMMAX,NMTOT,
     1    NSYMB,INPF1,INPF2,NFILE6,NFILE3,
     1    KOEFL,EFF,STOP)
C**********************************************************************C
C                                                                      C
C    ***************************************************************   C
C    *                                                             *   C
C    *  PROGRAM FOR THE INPUT AND OUTPUT OF COLLISION EFFICIENCIES *   C
C    *                    AUTHOR: J.WARNATZ                        *   C
C    *                       04.03.1983                            *   C
C    *              CHANGE: 06.10.1986/MAAS                        *   C
C    *         LAST CHANGE  25.04.2002/MAIWALD                     *   C
C    *                                                             *   C
C    ***************************************************************   C
C                                                                      C
C**********************************************************************C
C                                                                      C
C     INPUT  :                                                         C
C                                                                      C
C     NR             = NUMBER OF REACTIONS                             C
C     NS             = NUMBER OF SPECIES                               C
C     NM             = NUMBER OF THIRD BODIES (INCLUDING M) - VARIABLE C
C                      (see SUBROUTINE GENMEC)                         C
C     NSYMB(1)       = SPECIES SYMBOLS (NS+NM, NM=NUMBER OF THIRD BO-  C
C                      DIES, INCLUDING M)                              C
C     INPF           = NUMBER OF INPUT FILE                            C
C     NFILE6         = NUMBER OF OUTPUT FILE FOR LISTING               C
C     NFILE3         = NUMBER OF OUTPUT ON STORAGE                     C
C     KOEFL(NR,1)    = STOICHIOMETRIC NUMBERS (NRMAX,NS)               C
C     TROE           = .TRUE. FOR PRESS-DEPENDENT THIRD BODY REACTIONS C
C                                                                      C
C     OUTPUT :                                                         C
C                                                                      C
C     EFF(NS,*)       = COLLISION EFFICIENCIES (5,NS), FIVE THIRD BODIESC
C                      AT MAXIMUM                                      C
C     STOP           = .TRUE. FOR NORMAL END, .FALSE. FOR ABNORMAL END C
C                                                                      C
C     INPUT FROM FILE INPF :                                           C
C                                                                      C
C     IN THE FORMAT A4,1X,6(2A4,1X),/,5X,6(F5.3,4X)                    C
C     THE THIRD BODIES' SPECIES SYMBOL                                 C
C     AND THEN SPECIES SYMBOLS AND THE CORRESPONDING COLLISION EF-     C
C     FICIENCIES (12 SPECIES AT MAXIMUM). THE INPUT IS STOPPED BY THE  C
C     CHARACTER STRING -END - ON THE NEXT CARD. FIVE THIRD BODIES CAN  C
C     BE DEFINED AT MAXIMUM.                                           C
C                                                                      C
C     OUTPUT ON FILE NFILE3 :                                          C
C                                                                      C
C     COLLISION EFFICIENCIES                                           C
C                                                                      C
C**********************************************************************C
C                                                                      C
C                                                                      C
C                                                                      C
C**********************************************************************C
C     STORAGE ORGANIZATION,  INITIAL VALUES                            C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL STOP,EQUSYM
      PARAMETER(NENPL=15,ONE=1.0D0,ZERO=0.0D0,LSMAX=31)
      CHARACTER*80 HEAD
      PARAMETER (LENDEL=1,LENSPE=50,LINLEN=255)
      CHARACTER(LEN=LINLEN) STRING
      CHARACTER(LEN=*) NSYMB(*)
      CHARACTER(LEN=LSMAX)  M12
      CHARACTER(LEN=LSMAX) ,      DIMENSION(NMMAX+1,NENPL) :: INM
      DOUBLE PRECISION, DIMENSION(NMMAX+1,NENPL) :: XNM
      CHARACTER(LEN=LENSPE) SYMSPE(LINLEN)
      CHARACTER(LEN=LENDEL) SYMDEL(LINLEN)
      INTEGER,          DIMENSION(NM+1) :: NSUM  
      INTEGER,          DIMENSION(NMMAX+1) :: MDEF
      DIMENSION EFF(NS,*),KOEFL(NR,*)
      PARAMETER(NFL=2)
      CHARACTER*1 SFL(NFL)
      DATA SFL/'=','+'/
C**********************************************************************C
C     INITIALIZE
C**********************************************************************C
      LENSY=LEN(NSYMB(1))
      IF(LSMAX.LT.LENSY) THEN
        WRITE(NFILE6,*) 'Species length larger than', 
     1     LSMAX,' in GENCEF'
        STOP
      ENDIF
      STOP=.TRUE.
C**********************************************************************
C      
C**********************************************************************
C---- SET EFFICIENCY FOR FIRST THIRD BODY TO 1, AND ALL OTHERS TO ZERO
      EFF(1:NS,1)    = ONE 
      EFF(1:NS,2:NM) = ZERO
C     IF(TROE .EQV. .TRUE.) EFF(1:NS,2:NM) = ONE
      MDEF(1:NMMAX)=0
C**********************************************************************C
C     INPUT OF COLLISION EFFICIENCIES HEADING                          C
C**********************************************************************C
C---- FIRST SEARCH IN FIRST DATASET
      INPF=INPF1
      REWIND INPF
  103 FORMAT(A80)
   40 READ(INPF,103,ERR=50,END=50) HEAD
      IF(.NOT.EQUSYM(4,HEAD,'COLL'))  GOTO 40
      GOTO 90
C---- HEADING WAS NOT FOUND, NOW SEARCH IN SECOND
   50 CONTINUE
      INPF=INPF2
      REWIND INPF
   60 READ(INPF,103,ERR=990,END=990) HEAD
      IF(.NOT.EQUSYM(4,HEAD,'COLL')) GOTO 60
C**********************************************************************C
C     CHECK IF "TROE-FORMALISM" KEYWORD IS DEFINED IN NEXT LINE        C
C**********************************************************************C
   90 CONTINUE
C
      READ(INPF,103,ERR=50,END=50) HEAD
      IF(EQUSYM(4,HEAD,'TROE')) THEN
C---- FOR TROE OR LINDEMANN FORMALISM, ALL EFFICIENCIES HAVE TO BE 1
        EFF(1:NS,1)    = ONE 
        EFF(1:NS,2:NM) = ONE
      ELSE
        BACKSPACE INPF
      END IF
C**********************************************************************C
C     INPUT OF COLLISION EFFICIENCIES FROM FILE INPF                   C
C**********************************************************************C
      IM=1
  100 IM=IM+1
      READ(INPF,'(A)') STRING
      IF(EQUSYM(3,STRING,'END')) THEN
        IM=IM-1
        GOTO 200
      ENDIF
C**********************************************************************C
C     process input
C**********************************************************************C
      CALL COMPDE(STRING,ILAS,NFL,SFL)
      CALL SYDEL(STRING(1:ILAS),
     1    NSYSPE,LENSPE,SYMSPE,NSYDEL,LENDEL,SYMDEL,NFL,SFL,NFILE6)
      IF(SYMDEL(1).NE.'=') THEN
         WRITE(*,*) 'wrong first delimiter in gencef'
         STOP
      ENDIF
      INM(IM,1:NENPL)(1:LENSY) = REPEAT(' ',LENSY)
      INM(IM,1:NSYSPE-1)(1:LENSY) = SYMSPE(2:NSYSPE)(1:LENSY)
C**********************************************************************C
C     INPUT OF COLLISION EFFICIENCIES FROM FILE INPF                   C
C**********************************************************************C
      READ(INPF,*,ERR=920) (XNM(IM,I),I=1,NSYDEL)
C 812 FORMAT(9X,15(F8.3,1X))
C**********************************************************************C
C     Check if symbol is allowed
C**********************************************************************C
      DO 110 J=2,NMMAX
      IF(EQUSYM(LENSY,SYMSPE(1),NSYMB(NS+J))) THEN
        MDEF(J)=1
        JFOU     = J
        GOTO 120
      ENDIF
  110 CONTINUE
C---- IF WE ARE HERE WRONG SYMBOL FOR THIRD BODY WAS DETECTED
      GOTO 910
C**********************************************************************C
C     INPUT OF COLLISION EFFICIENCIES FROM FILE INPF                   C
C**********************************************************************C
  120 CONTINUE
      J=JFOU          
      DO 230 I=1,NENPL
C---- OVERJUMP SEARCH IF SYMBOL IS A BLANK
      IF(EQUSYM(LENSY,INM(IM,I),REPEAT(' ',LENSY))) GO TO 230
      IFOUND = 0
      DO 210 K=1,NS
      IF(EQUSYM(LENSY,INM(IM,I),NSYMB(K))) THEN
        IF(JFOU.GT.1.AND.JFOU.LE.NM) THEN
          EFF(K,J)=XNM(IM,I)
        ENDIF
        IFOUND = IFOUND + 1
      ELSE IF(    EQUSYM(4,INM(IM,I),NSYMB(K))
     1      .AND. EQUSYM(4,INM(IM,I),'N2(V')) THEN
        IF(JFOU.GT.1.AND.JFOU.LE.NM) EFF(K,J)=XNM(IM,I)
        IFOUND = IFOUND + 1
      ELSEIF(EQUSYM(4,INM(IM,I),NSYMB(K))
     1      .AND. EQUSYM(4,INM(IM,I),'O2(V')) THEN
        IF(JFOU.GT.1.AND.JFOU.LE.NM) EFF(K,J)=XNM(IM,I)
        IFOUND = IFOUND + 1
      ENDIF
  210 CONTINUE
C---- IF WE ARE HERE SYMBOL WAS NOT FOUND IN SPECIES LIST
      IF(IFOUND.LE.0) WRITE(NFILE6,936) INM(IM,I)(1:LENSY)
  936 FORMAT(' ',5X,'Warning: Third body ',A,' not found in species',
     1              ' list')
C---- IF NUMBER OF SYMB. DEF. IS NM, THEN CHECK NEXT LINE FOR END
  230 CONTINUE
      LENACT= 0
      DO I=1,NS
      LENACT=MAX(LENACT,LEN(TRIM(NSYMB(I))))
      ENDDO
C**********************************************************************C
C     Output of collision efficiencies 
C**********************************************************************C
      WRITE(NFILE6,8442) NSYMB(NS+J)(1:8),
     1         (XNM(IM,I),INM(IM,I)(1:LENACT),I=1,NSYSPE-1)
 8442 FORMAT(' ',7X,A,' = ',4(:'+',F9.3,1X,A,:)
     1      /19X,4(:'+',F9.3,1X,A,:),
     1      /19X,4(A1,F9.3,1X,A,:))
      GOTO 100
C---- IF WE ARE HERE, MAXIMUM NUMBER OF THIRD BODIES WAS REACHED
C---- CHECK IF THE NEXT LINE CONTAINS THE STRING 'END'
C**********************************************************************C
C     LINK SPECIES NUMBER AND COLLISION EFFICIENCY                     C
C**********************************************************************C
  200 CONTINUE
      IF(IM.EQ.1) GO TO 300
C**********************************************************************C
C     SEARCH FOR THIRD BODIES' SYMBOLS IN THE MECHANISM                C
C**********************************************************************C
  300 CONTINUE
C---- NSUM.NE.0 IF REACTION I CONTAINS A THIRD BODY SYMBOL
      DO J=1,NM
        NSUM(J)=SUM(KOEFL(1:NR,NS+J))
      ENDDO
      IF(NSUM(1).NE.0)  WRITE(NFILE6,831) NSYMB(NS+1)
      DO 330 J=2,NM
      IF((NSUM(J).EQ.0).AND.(MDEF(J).GT.0))
     1            WRITE(NFILE6,832) NSYMB(NS+J)
      IF((NSUM(J).NE.0).AND.(MDEF(J).LE.0)) GOTO 940
  330 CONTINUE
      DO J=NM+1,NMMAX
      IF(MDEF(J).GT.0)    WRITE(NFILE6,832) NSYMB(NS+J)
      ENDDO    
C**********************************************************************C
C
C     OUTPUT OF THE COLLISION EFFICIENCIES ON FILE NFILE6              C
C
C**********************************************************************C
      NSM=NS+1
      IMM1=IM-1
      IF(IM.EQ.1) WRITE(NFILE6,840) NSYMB(NS+1)
      IF(IM.EQ.1) GOTO 500
      IF(IM.GT.1) WRITE(NFILE6,841) IMM1,NSYMB(NS+1)
C**********************************************************************C
C     OUTPUT OF THE COLLISION EFFICIENCIES ON FILE NFILE3              C
C**********************************************************************C
C-Character
C-     only first 4 characters in output^
  500 CONTINUE
      DO 510 J=1,NM
      WRITE(NFILE3,851) NSYMB(NS+J),(EFF(I,J),I=1,NS)
  510 CONTINUE
      IF(NMTOT.GT.NM) THEN
      DO J=NM+1,NMTOT
      WRITE(NFILE3,852) J-NM,(EFF(I,J),I=1,NS)
      ENDDO      
      ENDIF
C**********************************************************************C
C     REGULAR EXIT
C**********************************************************************C
      RETURN
C**********************************************************************C
C     ERROR EXITS                                                      C
C**********************************************************************C
  910 WRITE(NFILE6,911) M12(1:LENSY)
  911 FORMAT('1',5X,'ERROR: wrong symbol of third body : ',A)
      GOTO 999
  920 WRITE(NFILE6,921) NSYDEL
  921 FORMAT('1',5X,'ERROR: less than ',I2,' efficencies defined')
       GOTO 999
C 930 WRITE(NFILE6,931) INM(J,I)
C 931 FORMAT('1',5X,'ERROR: species ',A,' not identified ')
C     GOTO 999
  940 WRITE(NFILE6,941) NSYMB(NS+J)
  941 FORMAT('1',5X,'ERROR: ',A,
     1      ' referenced in mechanism, but not defined')
      GOTO 999
  990 WRITE(NFILE6,991)
  991 FORMAT('1','ERROR: heading for collision efficiencies missing ')
      GOTO 999
  999 STOP=.FALSE.
      RETURN
C**********************************************************************C
C     FORMAT STATEMENTS                                                C
C**********************************************************************C
C 801 FORMAT(A)
C 811 FORMAT(16(A8,1X))
  831 FORMAT(' ',3X,A,6X,'has equal effectivity for all species')
  832 FORMAT('0',3X,'WARNING: ',A,' defined, but not used in ',
     1        'the mechanism')
  840 FORMAT(' ',7X,' no additional third bodies defined besides ',A)
  841 FORMAT(' ',7X,'NM = ',I3,' additional third bodies defined ',
     1       '(besides ',A,')')
CMMag F6.2 -> F9.3   
C 842 FORMAT(' ',7X,A8,' = ',4(A1,F9.3,1X,A8,:)/19X,4(A1,F9.3,1X,A8,:),
C    1      /19X,4(A1,F9.3,1X,A8,:))
C-Character
C-     only first 4 characters in output^
  851 FORMAT(' EFF.',A4,3X,5(1PE11.4,1X))
  852 FORMAT(' E.',I6,3X,5(1PE11.4,1X))
C**********************************************************************C
C     END OF -GENCEF-                                                  C
C**********************************************************************C
      END
      SUBROUTINE GENCRE(NS,NR,NPSPEC,NSYMB,INPF,NFILE6,NFILE3,NSOLID,
     1     ABRU,TBRU,EBRU,ORD,NUMS,STOBR,MNCRE,NBRUT,STOP)
C***********************************************************************
C
C    ***************************************************************
C    *                                                             *
C    *    PROGRAM FOR THE INPUT AND OUTPUT OF COMPLEX REACTIONS    *
C    *                                                             *
C    ***************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C     NS             = NUMBER OF SPECIES
C     NR             = NUMBER OF REACTIONS
C     NSYMB(1)       = SPECIES SYMBOLS (NS+NM, NM=NUMBER OF THIRD BO-
C                      DIES, INCLUDING M)
C     INPF           = NUMBER OF INPUT FILE
C     NFILE6         = NUMBER OF OUTPUT FILE FOR LISTING
C     NFILE3         = NUMBER OF OUTPUT ON STORAGE
C     NSOLID         = OPTION OF SOLID PARTICLE
C
C     OUTPUT :
C
C     NUMS,STOBR,    = SPECIES NUMBERS, STOICHIOMETRIC NUMBERS, AND
C      ORDBR(MNCRE,8)     REACTION ORDERS IN THE COMPLEX REACTIONS
C     ABRU,TBRU,     = PREEXPONENTIAL (M,MOL,S), TEMPERATURE COEFFI-
C      EBRU(MNCRE)        CIENT, AND ACTIVATION ENERGIES (J/MOL) FOR THE
C                      COMPLEX REACTIONS
C     NBRUT          = NUMBER OF COMPLEX REACTIONS
C     STOP           = .TRUE. FOR NORMAL END, .FALSE. FOR ABNORMAL END
C
C     INPUT FROM FILE INPF :
C
C     IN THE FIRST LINE IN THE FORMAT I3,2X,16A4 THE NUMBER OF COMPLEX
C     REACTIONS AND TEXT, BEGINNING WITH -COMP-.
C     IN THE FORMAT 4(A1,F4.1,1X,A8,1X,F3.0,1X),E10.3 IN THE 2ND LINE
C     THE LEFT HAND SIDE STOICHIOMETRIC NUMBERS, SPECIES SYMBOLS, AND
C     REACTION ORDERS (4 SPECIES) AND THE PREEXPONENTIAL (CM,MOL,S).
C     IN THE FORMAT(4(A1,F4.1,1X,A8,1X,F3.0,1X),2F5.1) IN THE 3RD LINE
C     THE RIGHT HAND SIDE STOICHIOMETRIC NUMBERS, SPECIES SYMBOLS, AND
C     REACTION ORDERS (4 SPECIES) AND TEMPERATURE EXPONENT AND ACTIVA-
C     TION ENERGY (KJ/MOL). THE A1 INPUT IS NOT RELEVANT (+,= SIGNS,
C     BRACKETS, OR BLANKS FOR OUTPUT LISTING).
C     FOR EACH ADDITIONAL COMPLEX REACTIONS TWO MORE LINES ARE READ.
C
C     OUTPUT ON FILE NFILE3  :
C
C     COMPLEX REACTIONS AND THEIR ARRHENIUS PARAMETERS
C
C***********************************************************************
C
C
C
C***********************************************************************
C     STORAGE ORGANIZATION,  INITIAL VALUES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL STOP,EQUSYM
      PARAMETER(LINMAX=30)
C-DIMENSION
      CHARACTER*80 HEAD
      CHARACTER(LEN=LINMAX) KNSY(20,6)
      CHARACTER(LEN=*)  NSYMB(*)
      CHARACTER*1 LIZ(8)
      DIMENSION ABRU(MNCRE),TBRU(MNCRE),EBRU(MNCRE),
     1          STOBR(MNCRE,6),ORD(MNCRE,6),NUMS(MNCRE,6)
      DATA MNCREL/20/
      STOP=.TRUE.
C---- avoid warning
      DUMMY = NPSPEC
C***********************************************************************
C
C     careful: subroutine reads only 8 characters long symbols
C     
C***********************************************************************
      LENSY=LEN(NSYMB(1))
      NFILL=LENSY-LINMAX
      IF(LINMAX.LT.LENSY) THEN 
        WRITE(NFILE6,*) 'Species length larger than', LINMAX 
        STOP 
      ENDIF
C***********************************************************************
C     INPUT OF THE HEADING OF THE SIMPL. COMPLEX REACT. FROM FILE INPF
C***********************************************************************
      REWIND INPF
   10 READ(INPF,800,END=50,ERR=50) HEAD
  800 FORMAT(A80)
      IF(.NOT.EQUSYM(4,HEAD,'COMP')) GOTO 10
  801 FORMAT(I4)
      READ(INPF,801) NBRUT
      GOTO 54
   55 FORMAT('1',5X,' No complex reactions defined ')
   50 WRITE(NFILE6,55)
      NBRUT=0
   54 CONTINUE
C***********************************************************************
C     OUTPUT OF HEADING ON FILE NFILE3
C***********************************************************************
  131 FORMAT(I5,' COMPLEX REACTIONS')
      WRITE(NFILE3,131) NBRUT
      IF(NBRUT.EQ.0) GO TO 1000
      IF(NBRUT.GE.MNCRE) GOTO 910
      IF(NBRUT.GE.MNCREL) GOTO 910
C***********************************************************************
C     INPUT OF THE SIMPLIFIED COMPLEX REACTIONS FROM FILE INPF
C***********************************************************************
C-UM
      DO 2010 I=1,NBRUT
      NRBR=NR+I
 2001 FORMAT(3(A1,F4.1,1X,A8,1X,F3.0,1X),E10.3)
      READ(INPF,2001) (LIZ(K),STOBR(I,K),KNSY(I,K),
     1                  ORD(I,K),K=1,3),ABRU(I)
 2002 FORMAT(3(A1,F4.1,1X,A8,1X,F3.0,1X),2F5.1)
      READ(INPF,2002) (LIZ(K),STOBR(I,K),KNSY(I,K),
     1             ORD(I,K),K=4,6),TBRU(I),EBRU(I)
      LIZ(7)=' '
 2010 CONTINUE
C***********************************************************************
C     OUTPUT OF THE SIMPLIFIED COMPLEX REACTIONS ON FILE NFILE6
C***********************************************************************
      DO 3010 I=1,NBRUT
      NRBR=NR+I
      KMAX=0
      DO K=1,3
         IF(STOBR(I,K).NE.0) KMAX=KMAX+1
      ENDDO
  478 FORMAT('1',5X,'ERROR: Wrong entry in complex reaction ',I2)
      IF (NSOLID .LE. 0) THEN
         IF(KMAX.EQ.0) WRITE(NFILE6,478) I
         IF(KMAX.EQ.0) STOP=.FALSE.
         IF(KMAX.EQ.0) RETURN
      ENDIF
 2015 FORMAT(' ','(',I3,')',A1,F4.2,1X,A8,'(',F4.1,')',
     1                  5(1X,A1,F4.2,1X,A8,'(',F4.1,')'))
      WRITE(NFILE6,2015)  NRBR,
     1             (LIZ(K),STOBR(I,K),KNSY(I,K),
     2             ORD(I,K),K=1,KMAX)
      KMAX=3
      DO K=4,6
         IF(STOBR(I,K).NE.0) KMAX=KMAX+1
      ENDDO
C 484 FORMAT('1',5X,'ERROR IN COMPLEX REACTION ',I2)
C     IF(KMAX.LE.4) WRITE(NFILE6,484) I
C     IF(KMAX.LE.4) STOP=.FALSE.
C     IF(KMAX.LE.4) RETURN
  484 FORMAT('1',5X,'WARNING: No products in complex reaction no.',I2)
      IF(KMAX.LE.2) WRITE(NFILE6,484) I
 2016 FORMAT(' ',A4,4(1X,A1,F4.2,1X,A8,'(',F4.1,')'))
      WRITE(NFILE6,2016)  '  ->',
     1     (LIZ(K),STOBR(I,K),KNSY(I,K),ORD(I,K),K=4,KMAX)
 2017 FORMAT(' ',7X,'A =',1PE10.3,'           ',5X,'TC =',0PF8.3,
     1       5X,'E =',0PF10.2,' KJ/MOL')
      WRITE(NFILE6,2017) ABRU(I),TBRU(I),EBRU(I)
 3010 EBRU(I)=EBRU(I)*1000.0
C***********************************************************************
C     CHECK OF SPECIES SYMBOLS, LINK WITH SPECIES NUMBERS
C***********************************************************************
      DO 2050 I=1,NBRUT
      DO 2050 K=1,6
      NUMS(I,K)=0
                                         ILL = 1
      IF(EQUSYM(LENSY,REPEAT(' ',LENSY),KNSY(I,K))) ILL = 0
      DO L=1,NS
C_rst MAYBE: NS+NPSPEC
         IF(EQUSYM(LENSY,NSYMB(L),KNSY(I,K))) THEN
            ILL = 0
            NUMS(I,K)=L
      ENDIF
      ENDDO
      IF(ILL.EQ.0)  GO TO 890
 2035 FORMAT('1',5X,'ERROR: Wrong entry in complex reaction no.',I2)
      WRITE(NFILE6,2035) I
      STOP=.FALSE.
      GO TO 1000
  890 CONTINUE
 2050 CONTINUE
C***********************************************************************
C     OUTPUT OF COMPLEX REACTIONS ON FILE NFILE3
C***********************************************************************
      DO 849 I=1,NBRUT
  846 FORMAT(' STOBR',6X,8(F6.3,1X))
      WRITE(NFILE3,846) (STOBR(I,K),K=1,6)
  847 FORMAT(' NUMS ',6X,8(2X,I2,3X))
      WRITE(NFILE3,847) (NUMS (I,K),K=1,6)
  848 FORMAT(' ORD  ',6X,8(F6.3,1X))
      WRITE(NFILE3,848) (ORD  (I,K),K=1,6)
C---- DETERMINE REACTION ORDER AND TRANSFORM TO SI UNITS
      REORD=0.0
      DO 723 K=1,3
  723 REORD=REORD+ORD(I,K)
      ABRU(I)=ABRU(I)*(1.0E-6)**(REORD-1.0)
  851 FORMAT(' ABR,TBR,EBR',5(1PE11.4,1X))
  849 WRITE(NFILE3,851) ABRU(I),TBRU(I),EBRU(I)
 1000 RETURN
C***********************************************************************
C     Error exits
C***********************************************************************
  910 CONTINUE
      WRITE(NFILE6,*) ' Too many complex reactions, NBRUT (',NBRUT,
     1      ') > MNCRE (',MNCRE,')'
      STOP
C***********************************************************************
C     END OF -GENCRE-
C***********************************************************************
      END
      SUBROUTINE CALSI(I,T,HCPL,HCPH,HINF,S,NFILE6)
C***********************************************************************
C
C    ***************************************************************
C    *                                                             *
C    *         PROGRAM FOR CALCULATION OF MOLAR ENTROPIES          *
C    *                                                             *
C    *                 LAST CHANGE 09/22/1989 MAAS                 *
C    *                                                             *
C    ***************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C     I             = NUMBER OF SPECIES IN SPECIES LIST
C     T             = TEMPERATURE, K
C     HCPL(7,1)     = LOW  TEMPERATURE NASA COEFFICIENTS,    J,K (7,NS)
C     HCPH(7,1)     = HIGH TEMPERATURE NASA COEFFICIENTS,    J,K (7,NS)
C
C     OUTPUT :
C
C     S             = MOLAR ENTROPY, J/MOL*K
C
C***********************************************************************
C
C
C
C***********************************************************************
C     STORAGE ORGANIZATION AND DEFINITION OF INITIAL VALUES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION HCPL(7,*),HCPH(7,*),HINF(3,I)
C***********************************************************************
C     CALCULATION OF S
C***********************************************************************
      IF(T.GE.HINF(2,I)) GO TO 3
      IF(T.LT.HINF(1,I))   WRITE(NFILE6,921) T,HINF(3,I),I
C**** LOW TEMPERATURE CASE
      S=HCPL(5,I)/4.0
      DO 1 K=1,3
      S=S*T+HCPL(5-K,I)/FLOAT(4-K)
    1 CONTINUE
      S=S*T+DLOG(T)*HCPL(1,I)+HCPL(7,I)
      GO TO 5
    3 CONTINUE
      IF(T.GT.HINF(3,I))   WRITE(NFILE6,931) T,HINF(3,I),I
C**** HIGH TEMPERATURE CASE
      S=HCPH(5,I)/4.0
      DO 4 K=1,3
      S=S*T+HCPH(5-K,I)/FLOAT(4-K)
    4 CONTINUE
      S=S*T+DLOG(T)*HCPH(1,I)+HCPH(7,I)
    5 CONTINUE
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
  921 FORMAT('0','WARNING: Temperature (',F6.0,') exceeds lower limit',
     1       ' of reliability (',F6.0,')',/,
     1       ' for fit of thermodynamic properties of species no.',I3)
  931 FORMAT('0','WARNING: Temperature (',F6.0,') exceeds upper limit',
     1       ' of reliability (',F6.0,')',/,
     1       ' for fit of thermodynamic properties of species no.',I3)
C***********************************************************************
C     END OF -CALSI -
C***********************************************************************
      END
      SUBROUTINE CALHI(I,T,HCPL,HCPH,HINF,H,NFILE6)
C***********************************************************************
C
C    ***************************************************************
C    *                                                             *
C    *         PROGRAM FOR CALCULATION OF MOLAR ENTHALPIES         *
C    *                                                             *
C    *              LAST CHANGE: 09/22/1989 MAAS                   *
C    *                                                             *
C    ***************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C     I             = NUMBER OF SPECIES IN SPECIES LIST
C     T             = TEMPERATURE, K
C     HCPL(7,*)     = LOW  TEMPERATURE NASA COEFFICIENTS, J,K (7,NS)
C     HCPH(7,*)     = HIGH TEMPERATURE NASA COEFFICIENTS, J,K (7,NS)
C
C     OUTPUT :
C
C     H             = MOLAR ENTHALPY, J/MOL
C
C***********************************************************************
C
C
C
C***********************************************************************
C     STORAGE ORGANIZATION AND DEFINITION OF INITIAL VALUES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION HCPL(7,*),HCPH(7,*),HINF(3,*)
C***********************************************************************
C     CALCULATION OF H
C***********************************************************************
      IF(T.GE.HINF(2,I)) GO TO 3
C---- LOW TEMPERATURE CASE
      IF(T.LT.HINF(1,I))      WRITE(NFILE6,921) T,HINF(1,I),I
      H=HCPL(5,I)/5.0
      DO 1 K=1,4
      H=H*T+HCPL(5-K,I)/FLOAT(5-K)
    1 CONTINUE
      H=H*T+HCPL(6,I)
      GO TO 5
C---- HIGH TEMPERATURE CASE
    3 CONTINUE
      IF(T.GT.HINF(3,I))     WRITE(NFILE6,931) T,HINF(3,I),I
      H=HCPH(5,I)/5.0
      DO 4 K=1,4
      H=H*T+HCPH(5-K,I)/FLOAT(5-K)
    4 CONTINUE
      H=H*T+HCPH(6,I)
    5 CONTINUE
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
  921 FORMAT('0','WARNING: Temperature (',F6.0,') exceeds lower limit',
     1       ' of reliability (',F6.0,')',/,
     1       ' for fit of thermodynamic properties of species no.',I3)
  931 FORMAT('0','WARNING: Temperature (',F6.0,') exceeds upper limit',
     1       ' of reliability (',F6.0,')',/,
     1       ' for fit of thermodynamic properties of species no.',I3)
C***********************************************************************
C     END OF -CALHI -
C***********************************************************************
      END
      SUBROUTINE CALCPI(I,T,HCPL,HCPH,HINF,CP,NFILE6)
C***********************************************************************
C
C    ***************************************************************
C    *                                                             *
C    *      PROGRAM FOR CALCULATION OF MOLAR HEAT CAPACITIES       *
C    *                                                             *
C    *              LAST CHANGE: 09.22.1989/MAAS                   *
C    *                                                             *
C    ***************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C     I             = NUMBER OF SPECIES IN SPECIES LIST
C     T             = TEMPERATURE, K
C     HCPL(7,*)     = LOW  TEMPERATURE NASA COEFFICIENTS, J,K (7,NS)
C     HCPH(7,*)     = HIGH TEMPERATURE NASA COEFFICIENTS, J,K (7,NS)
C
C     OUTPUT :
C
C     CP            = MOLAR HEAT CAPACITY, J/MOL*K
C
C***********************************************************************
C
C
C
C***********************************************************************
C     STORAGE ORGANIZATION AND DEFINITION OF INITIAL VALUES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION HCPL(7,*),HCPH(7,*),HINF(3,*)
C***********************************************************************
C     CALCULATION OF CP
C***********************************************************************
      IF(T.GE.HINF(2,I)) GO TO 3
C---- LOW TEMPERATURE CASE
      IF(T.LT.HINF(1,I))      WRITE(NFILE6,921) T,HINF(1,I),I
      CP=HCPL(5,I)
      DO 1 K=1,4
      CP=CP*T+HCPL(5-K,I)
    1 CONTINUE
      GO TO 5
C---- HIGH TEMPERATURE CASE
    3 CONTINUE
      IF(T.GT.HINF(3,I))     WRITE(NFILE6,931) T,HINF(3,I),I
      CP=HCPH(5,I)
      DO 4 K=1,4
      CP=CP*T+HCPH(5-K,I)
    4 CONTINUE
    5 CONTINUE
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
  921 FORMAT('0','WARNING: Temperature (',F6.0,') exceeds lower limit',
     1       ' of reliability (',F6.0,')',/,
     1       ' for fit of thermodynamic properties of species no.',I3)
  931 FORMAT('0','WARNING: Temperature (',F6.0,') exceeds upper limit',
     1       ' of reliability (',F6.0,')',/,
     1       ' for fit of thermodynamic properties of species no.',I3)
C***********************************************************************
C     END OF -CALCPI-
C***********************************************************************
      END
      SUBROUTINE ISTDEC(MATH,MAT)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *    SORT ENTRIES OF AN INTEGER VECTOR IN DESCENDING ORDER  *    *
C     *                                                           *    *
C     *              LAST CHANGE :  07/07/2005 R. STAUCH          *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C***********************************************************************
C     I.I.: TYPES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION MATH(12),MAT(12)
C***********************************************************************
C     STORE MATH IN MAT
C***********************************************************************
      DO I=1,12
         MAT(I)=MATH(I)
      ENDDO
C***********************************************************************
C_rst NEW VERSION OF SORTING FOR ARBITRARY NUMBER OF REACTANTS
C_rst FOR OLD VERSION COMMENT THE FOLLOWING LINE
      GOTO 1000
C***********************************************************************
C     LOOP FOR 1-3, 4-6
C***********************************************************************
      DO 10 K=1,2
      IADD = (K-1)*3
C***********************************************************************
C     CHAPTER GET MAXIMUM OF 1,2,3 AND SORT                            *
C***********************************************************************
      IM  = 1
      IVM = MAT(IADD+1)
      IF(MAT(IADD+2).GT.IVM) THEN
        IM  = 2
        IVM = MAT(IADD+2)
      ENDIF
      IF(MAT(IADD+3).GT.IVM) THEN
        IM  = 3
        IVM = MAT(IADD+3)
      ENDIF
      MAT(IADD+IM) = MAT(IADD+1)
      MAT(IADD+1)  = IVM
C***********************************************************************
C     CHAPTER GET MAXIMUM OF 2,3 AND SORT                              *
C***********************************************************************
      IM  = 2
      IVM = MAT(IADD+2)
      IF(MAT(IADD+3).GT.IVM) THEN
        IM  = 3
        IVM = MAT(IADD+3)
      ENDIF
      MAT(IADD+IM) = MAT(IADD+2)
      MAT(IADD+2)  = IVM
   10 CONTINUE
      GOTO 2000
C***********************************************************************
C_rst SORT AN ARBITRARY NUMBER OF EDUCTS AND PRODUCTS 
C***********************************************************************
C_rst assume matrix of 6 reactands and 6 products 
 1000 CONTINUE
      DO K=1,2
         IF(K.EQ.1) IADD  = 0
         IF(K.EQ.2) IADD  = 6
         DO L=1,5 
            DO M=L+1,6
               IVM = MAT(IADD+L)
               IF (MAT(IADD+M).GT.IVM) THEN
                  MAT(IADD+L) = MAT(IADD+M)
                  MAT(IADD+M) = IVM
               ENDIF
            ENDDO
         ENDDO
      ENDDO
C***********************************************************************
 2000 CONTINUE
      RETURN
C***********************************************************************
      END
      SUBROUTINE GENTHE(NS,INPFT,NFILE6,
     1         NSYMB,TEST,NCHECK,HCPL,HCPH,HINF,SELE,NELE,STOP)
C**********************************************************************C
C                                                                      C
C    ***************************************************************   C
C    *                                                             *   C
C    *    PROGRAM FOR THE INPUT AND PRINTOUT OF THERMODYNAMIC      *   C
C    *                PROPERTIES FROM A DATA BASE                  *   C
C    *                                                             *   C
C    *                LAST CHANGE: 16/11/2001 / O. MAIWALD         *   C
C    *                                                             *   C
C    ***************************************************************   C
C                                                                      C
C**********************************************************************C
C                                                                      C
C     INPUT  :                                                         C
C                                                                      C
C     NS            = NUMBER OF SPECIES                                C
C     INPFT         = NUMBER OF INPUT FILE (THERMOCHEM. DATA)          C
C     NFILE6        = NUMBER OF OUTPUT FILE FOR LISTING                C
C     NSYMB(*)      = SPECIES SYMBOLS (NS+NM)                          C
C     TEST          = .TRUE. FOR ADDITIONAL OUTPUT, =.FALSE. OTHERWISE C
C                                                                      C 
C     OUTPUT :                                                         C 
C                                                                      C 
C     HCPL(7,*)     = POLYNOMIAL COEFF. OF H,  (7,NS), T<1000K         C 
C     HCPH(7,*)     = POLYNOMIAL COEFF. OF H,  (7,NS), T>1000K         C 
C                      IN UNITS OF J    AND KELVIN                     C 
C     STOP          = .TRUE. FOR NORMAL END, .FALSE. FOR ABNORMAL END  C
C                                                                      C
C     INPUT FROM FILES  INPFT  :                                       C
C     SELE(4,NS)          = SYMBOL OF ATOM                             C
C     NELE(4,NS)          = NUMBER OF ATOMS                            C
C
C     THERMOCHEMICAL DATA (HIGH AND LOW TEMPERATURE CASE)              C
C     HCPL(7,1)           = NASA COEFF. OF H  (7,NS), T<1000K
C     HCPH(7,1)           = NASA COEFF. OF H  (7,NS), T>1000K
C     HCPH(1,I),HCPL(1,I) = STANDARD MOLAR HEAT CAPACITY
C     HCPH(6,I),HCPL(6,I) = STANDARD MOLAR ENTHALPY
C     HCPH(7,I),HCPL(7,I) = STANDARD MOLAR ENTROPY
C     HCPH(2,I)-HCPH(5,I) = POLYNOM COEFFICIENTS FOR CP
C     HCPL(2,I)-HCPL(5,I) = POLYNOM COEFFICIENTS FOR CP
C     ALL GIVEN IN UNITS OF RGAS AND KELVIN
C
C                                                                      C
C**********************************************************************C
C                                                                      C
C                                                                      C
C                                                                      C
C**********************************************************************C
C     STORAGE ORGANIZATION AND DEFINITION OF INITIAL VALUES            C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL STOP,TEST,NCHECK(NS),EQUSYM
      CHARACTER(LEN=*)  NSYMB(NS)
      PARAMETER(LNREAD=16)
      CHARACTER(LEN=LNREAD) MMMM
      CHARACTER*2 SELE(4,NS),TXTHLP(4)
      CHARACTER*1 NZL(6),NZH(6)
      DIMENSION HCPL(7,NS),HCPH(7,NS),HINF(3,NS),NELE(4,NS)
      DIMENSION HHHELP(7),HLHELP(7),NUMHLP(4),XNUMHLP(4)
      DATA RGAS/8.3143D0/
      STOP=.TRUE.
C**********************************************************************C
C     INITIAL PREPARATIONS                                             C
C**********************************************************************C
      LNSY = LEN(NSYMB(1))
      IF(LNSY.GT.LNREAD) GOTO 910
      REWIND INPFT
      DO 10 I=1,NS
   10 NCHECK(I)=.FALSE.
      LAUF=0
C**********************************************************************C
C     READ ONE DATA SET AND CHECK IF IT IS NEEDED                      C
C**********************************************************************C
C-CHARACTER conversion
C----- careful: only first 16 characters are read, change format
   60 CONTINUE
   65 FORMAT(A,8X,4(A2,F3.0),1X,2E10.0,E8.0/,5E15.8,/,5E15.8,/,4E15.8)
C  65 FORMAT(A,8X,4(A2,I3),1X,2E10.0,E8.0/,5E15.8,/,5E15.8,/,4E15.8)
C     READ(INPFT,65,END=100,ERR=120)  MMMM,(TXTHLP(K),NUMHLP(K),K=1,4),
      READ(INPFT,65,END=100,ERR=120)  MMMM,(TXTHLP(K),XNUMHLP(K),K=1,4),
     1    TMINH,TMAXH,TJUMP,(HHHELP(K),K=1,7),(HLHELP(K),K=1,7)
C     WRITE(*,*) (TXTHLP(K),NUMHLP(K),K=1,4)
      NUMHLP=NINT(XNUMHLP)
C**********************************************************************C
C     Check species                                                    C
C**********************************************************************C
      DO 70 I=1,NS
C**********************************************************************C
C     Take only characters until first blank  
C**********************************************************************C
C     IBLANK=INDEX(MMMM,' ')
C     IF(IBLANK.GT.0) MMMM(IBLANK:LNREAD) = REPEAT(' ',LNREAD-IBLANK+1)
C**********************************************************************C
C     Remove element information              
C**********************************************************************C
C     IREM =INDEX(MMMM,'REF')
C     IF(IREM.GT.0)   MMMM(IBLANK:LNREAD) = REPEAT(' ',LNREAD-IBLANK+1)
      IND=I
      IF(EQUSYM(LNSY,MMMM,NSYMB(I))) GO TO 80
   70 CONTINUE
      GOTO 60
C**********************************************************************C
C     DASET WAS FOUND TO CORRESPOND TO SPECIES NO. IND                 C
C**********************************************************************C
   80 CONTINUE
      DO 90 K=1,7
      HCPH(K,IND)=HHHELP(K)*RGAS
      HCPL(K,IND)=HLHELP(K)*RGAS
   90 CONTINUE
      DO 93 K=1,4
      SELE(K,IND)=TXTHLP(K)
      NELE(K,IND)=NUMHLP(K)
   93 CONTINUE
C      WRITE(6,*) (SELE(IZIZ,IND),IZIZ=1,4)
C      WRITE(6,*) (NELE(IZIZ,IND),IZIZ=1,4)
C---- ALLOW LOWER LIMIT TO DIFFER BY 20 K
      HINF(1,IND)=TMINH-20.D0
C     HINF(1,IND)=TMINH
      IF(TJUMP.EQ.0.0) HINF(2,IND)=1000.
      IF(TJUMP.NE.0.0) HINF(2,IND)=TJUMP
      HINF(3,IND)=TMAXH
      NCHECK(IND)=.TRUE.
      LAUF=LAUF+1
C     IF(LAUF.EQ.NS) GOTO 100
C**********************************************************************C
C     GO BACK AND READ NEXT DATA                                       C
C**********************************************************************C
      GOTO 60
C**********************************************************************C
C     END OF DATASET WAS DETECTED, NOW DO FINAL CHECKS
C**********************************************************************C
  120 CONTINUE
      write(NFILE6,912) MMMM
  912 FORMAT(10X,'I/O ERROR. CHECK FORMAT OF THE "',A16,'" BLOCK IN 
     1THERMOCHEMICAL DATABASE!')
  100 CONTINUE
      DO 110 I=1,NS
      IF(.NOT.NCHECK(I)) THEN
      WRITE(NFILE6,911) NSYMB(I)
  911 FORMAT(6X,'ERROR - Species ',A16,' not found in ',
     1              'thermochemical database')
      STOP=.FALSE.
      ENDIF
  110 CONTINUE
C**********************************************************************C
C     OUTPUT ON NFILE6 FOR TESTING                                     C
C**********************************************************************C
      IF(TEST) THEN
      DO 220 I=1,NS
      NZL(1)=' '
      NZH(1)=' '
      DO 210  L=2,6
      NZL(L)='+'
      NZH(L)='+'
      IF(HCPL(L,I).LT.0.0) NZL(L)='-'
      IF(HCPH(L,I).LT.0.0) NZH(L)='-'
  210 CONTINUE
      WRITE(NFILE6,211) NSYMB(I),
     1           (NZL(K),HCPL(K,I),K=1,6),(NZH(K),HCPH(K,I),K=1,6)
  211 FORMAT(' ','Thermodynamic fits for species ',A8,/,
     1       ' ',5X,'H   =',A1,1PE9.2,'*T',4X,A1,1PE9.2,'*T**2',
     1      1X,A1,1PE9.2,'*T**3',/,11X,A1,1PE9.2,
     2      '*T**4',1X,A1,1PE9.2,'*T**5',
     3      1X,A1,1PE9.2,'          K   FOR T<1000 K',
     3      /,' ',5X,'H   =',A1,1PE9.2,'*T',4X,A1,1PE9.2,'*T**2',
     4      1X,A1,1PE9.2,'*T**3',/,11X,A1,1PE9.2,'*T**4',
     5      1X,A1,1PE9.2,'*T**5',1X,A1,1PE9.2,8X,'  K   FOR T>1000 K')
  220 CONTINUE
      ENDIF
C**********************************************************************C
C    REGULAR EXIT
C**********************************************************************C
      RETURN
C**********************************************************************C
C    ERROR EXIT
C**********************************************************************C
 910  CONTINUE
      WRITE(NFILE6,931) LNSY,LNREAD
  931 FORMAT(' Length of Symbols (',I2,
     1      ') larger than read characters (',I2,')')
      STOP
C900  CONTINUE
C     WRITE(NFILE6,901)'ERROR IN GENTHE  -  MAYBE WRONG FORMAT IN LINE '
C     WRITE(NFILE6,902) MMMM,(TXTHLP(K),NUMHLP(K),K=1,4),
C    1    TMINH,TMAXH,TJUMP,(HHHELP(K),K=1,7),(HLHELP(K),K=1,7)
C     STOP
C 901 FORMAT(7('+'),4X,A47,4X,7('+'))
C 902 FORMAT(7('+'),3X,A8,16X,4(A2,I3),2X,2E10.1,E8.1/,
C    1       7('+'),2X,5E15.8,/,7('+'),2X,5E15.8,/,7('+'),2X,4E15.8)
C**********************************************************************C
C    END OF _GENTHE- 
C**********************************************************************C
      END
      SUBROUTINE ELECOM(NS,SINP,NINP,NEMAX,NE,SYME,NCOMPE,XE,XMOL,EMOL,
     2                         NFILE6,STOP)
C***********************************************************************
C
C    ***************************************************************
C    *                                                             *
C    *    PROGRAM FOR THE CALCULATION OF ELEMENT COMPOSITIONS      *
C    *                     AUTHOR: U.MAAS                          *
C    *                       09/22/1989                            *
C    *              LAST CHANGES: 09/22/1989 U. MAAS               *
C    *                            15/04/2001 O. Maiwald            *
C    *                                                             *
C    ***************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C     NS            = NUMBER OF SPECIES
C     SINP(4,NS)    = SYMBOLS OF ATOMS IN SPECIES
C     NINP(4,NS)    = NUMBER OF ATOMS OF ELEMENT SINP IN SPECIES
C     NEMAX         = MAXIMUM DIFFERENT ATOMS ALLOWED
C
C  OUTPUT:
C     NE            = NUMBER OF DIFFERENT ATOMS
C     SYME(NE)      = SYMBOLS OF ATOMS
C     NCOMPE(NS,NE) = NUMBER OF ATOMS OF SYME IN SPECIES I
C     XE(NS,NE)     = ELEMENT MASS FRACTIONS
C
C
C***********************************************************************
C
C
C
C***********************************************************************
C     STORAGE ORGANIZATION AND DEFINITION OF INITIAL VALUES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL STOP,EQUSYM
      PARAMETER (NEDMAX=23)
C---- INTERNALLY NEEDED ELEMENT LIST
      LOGICAL     LATOM(NEDMAX)
      CHARACTER*2 SATOM(NEDMAX)
      DIMENSION   XATOM(NEDMAX)
      DIMENSION   EMOL(NEMAX)
C---- INPUT TO ROUTINE
      CHARACTER*2 SINP(4,NS)
      DIMENSION   NINP(4,NS)
C---- OUTPUT OF ROUTINE
C-DIMENSION
      CHARACTER*2 SYME(*)
C-DIMENSION
      DIMENSION   NCOMPE(NS,*)
C-DIMENSION
      DIMENSION   XE(NS,*),XMOL(NS)
      PARAMETER (ZERO=0.0D0)
      DATA SATOM( 1)/'H '/,XATOM( 1)/  1.0079D-3/
      DATA SATOM( 2)/'C '/,XATOM( 2)/ 12.0110D-3/
      DATA SATOM( 3)/'O '/,XATOM( 3)/ 15.9994D-3/
      DATA SATOM( 4)/'N '/,XATOM( 4)/ 14.0067D-3/
      DATA SATOM( 5)/'HE'/,XATOM( 5)/  4.0026D-3/
      DATA SATOM( 6)/'NE'/,XATOM( 6)/ 20.1179D-3/
      DATA SATOM( 7)/'AR'/,XATOM( 7)/ 39.9480D-3/
      DATA SATOM( 8)/'KR'/,XATOM( 8)/ 83.8000D-3/
      DATA SATOM( 9)/'XE'/,XATOM( 9)/ 131.290D-3/
      DATA SATOM(10)/'F '/,XATOM(10)/ 18.9984D-3/
      DATA SATOM(11)/'NA'/,XATOM(11)/ 22.9898D-3/
      DATA SATOM(12)/'MG'/,XATOM(12)/ 24.3050D-3/
      DATA SATOM(13)/'AL'/,XATOM(13)/ 26.9815D-3/
      DATA SATOM(14)/'SI'/,XATOM(14)/ 28.0855D-3/
      DATA SATOM(15)/'P '/,XATOM(15)/ 30.9738D-3/
      DATA SATOM(16)/'S '/,XATOM(16)/ 32.0600D-3/
      DATA SATOM(17)/'CL'/,XATOM(17)/ 35.4530D-3/
      DATA SATOM(18)/'K '/,XATOM(18)/ 39.0983D-3/
      DATA SATOM(19)/'CA'/,XATOM(19)/ 40.0800D-3/
      DATA SATOM(20)/'BR'/,XATOM(20)/ 79.9040D-3/
      DATA SATOM(21)/'I '/,XATOM(21)/ 126.905D-3/
      DATA SATOM(22)/'Pt'/,XATOM(22)/ 195.090D-3/      
      DATA SATOM(23)/'YY'/,XATOM(23)/ 1000.00D-3/      
      STOP=.TRUE.
      NAMAX=NEDMAX
C***********************************************************************
C     CHECK HOW MANY DIFFERENT ELEMENTS ARE IN THE SYSTEM
C***********************************************************************
      NE=0
      DO 120 J=1,NAMAX
      LATOM(J)=.FALSE.
      DO 110 I=1,NS
      DO 110 K=1,4
      IF(EQUSYM(2,SINP(K,I),SATOM(J))) THEN
      LATOM(J)=.TRUE.
      NE=NE+1
      GOTO 115
       ENDIF
  110 CONTINUE
  115 CONTINUE
  120 CONTINUE
      IF(NE.GT.NEMAX) GOTO 910
C***********************************************************************
C     BUILD UP ELEMENTS VECTOR
C***********************************************************************
      LAUF=0
      DO 210 I=1,NAMAX
      IF(LATOM(I)) THEN
      LAUF=LAUF+1
      SYME(LAUF) = SATOM(I)
      EMOL(LAUF) = XATOM(I)
      ENDIF
  210 CONTINUE
C***********************************************************************
C
C     LOOP OVER SPECIES STARTS
C
C***********************************************************************
      DO 390 I=1,NS
      DO 305 J=1,NE
      NCOMPE(I,J)=0
      XE(I,J)    =0.D0
  305 CONTINUE
C***********************************************************************
C     BUILD UP COMPOSITION MATRIX
C***********************************************************************
      DO 320 K=1,4
      IF(EQUSYM(2,SINP(K,I),'  ')) GOTO 315
      IF(EQUSYM(2,SINP(K,I),'0 ')) GOTO 315
      IF(EQUSYM(2,SINP(K,I),'00')) GOTO 315
      DO 310 J=1,NE
      IF(EQUSYM(2,SINP(K,I),SYME(J))) THEN
        NCOMPE(I,J)=NINP(K,I)
        GOTO 315
      ENDIF
  310 CONTINUE
        NFAIL=I
        GOTO 930
  315 CONTINUE
  320 CONTINUE
C***********************************************************************
C     CHECK FOR SPECIES WITH NO INFORMATION GIVEN
C***********************************************************************
      DO 340 J=1,NE
      IF(NCOMPE(I,J).NE.0) GOTO 350
  340 CONTINUE
      ILL=1
      WRITE(NFILE6,941) I
  941 FORMAT(' ','Warning: No element composition defined for species',
     1           ' no.',I4,'.',/,' This will cause the element vectors',
     1           ' and the molar mass to become 0')
      DO 345 J=1,NE
      XE(I,J) =  ZERO
  345 CONTINUE
      XMOL(I)= ZERO
      GOTO 380
  350 CONTINUE
C***********************************************************************
C     CALCULATE ELEMENT MASS FRACTIONS
C***********************************************************************
      XMASS=0.D0
      DO 360 J=1,NE
      XMASS=XMASS + FLOAT(NCOMPE(I,J))*EMOL(J)
  360 CONTINUE
      XMOL(I)=XMASS
      DO 370 J=1,NE
      XE(I,J) =  FLOAT(NCOMPE(I,J))*EMOL(J) / XMASS
  370 CONTINUE
  380 CONTINUE
C     WRITE(6,*) (XE(I,J),J=1,NE)
  390 CONTINUE
C***********************************************************************
C     REGULAR EXIT
C***********************************************************************
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
  910 WRITE(NFILE6,911) NE,NEMAX
  911 FORMAT('0','ERROR: number of elements (',I2,') > NEMAX (',I2,')')
      GOTO 999
  930 WRITE(NFILE6,931) SINP(K,I),NFAIL
  931 FORMAT('0','ERROR: element ',A2,
     1    ' not found in elements list, processing species ',I4)
      GOTO 999
  999 CONTINUE
      STOP=.FALSE.
      RETURN
C***********************************************************************
C     END OF ELECOM
C***********************************************************************
      END
      SUBROUTINE INIMOI(NS,NSYMB,XMOL,NE,SYME,EMOL,XE,NFILE3)
C***********************************************************************
C
C    ***************************************************************
C    *                                                             *
C    *     CALCULATION AND OUTPUT OF ELEMENT INFORMATION           *
C    *                      AUTHOR U. Maas                         *
C    *                                                             *
C    ***************************************************************
C
C***********************************************************************
C     INPUT  :
C
C     XMOL(NS)       = MOLAR MASSES, KG/MOL, (NS)
C
C
C     CONTROL VALUES
C
C     OUTPUT:
C
C     OUTPUT ON FILE NFILE3 : SEE COMMENT BELOW
C
C***********************************************************************
C
C
C***********************************************************************
C     STORAGE ORGANIZATION, INITIAL VALUES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      CHARACTER(LEN=*) NSYMB
      CHARACTER*2 SYME(NE)
      DIMENSION XE(NS,NE),EMOL(NE),XMOL(NS),NSYMB(NS)
C***********************************************************************
C
C     output of number of species
C
C***********************************************************************
  733 FORMAT(I5,' SPECIES ')
      WRITE(NFILE3,733) NS
C----
      WRITE(NFILE3,806) NE
  806 FORMAT(I5,'  Elements')
C***********************************************************************
C     OUTPUT OF SPECIES SYMBOLS
C***********************************************************************
      IF(NS.GT.0) THEN
  804 FORMAT(' NSYMB',6X,5(A,2X))
      WRITE(NFILE3,804)  (NSYMB(I),I=1,NS)
C***********************************************************************
C     OUTPUT OF MOLAR MASSES (KG/MOL)
C***********************************************************************
  805 FORMAT(' XMOL',7X,5(1PE20.13,1X))
      WRITE(NFILE3,805)  (XMOL(I),I=1,NS)
C***********************************************************************
C     OUTPUT OF Elements and Element composition
C***********************************************************************
      WRITE(NFILE3,807)  (SYME(I),I=1,NE)
  807 FORMAT(' SYME ',6X,5(A2,6X,2X))
      WRITE(NFILE3,808)  (EMOL(I),I=1,NE)
  808 FORMAT(' EMOL',7X,5(1PE20.13,1X))
      WRITE(NFILE3,809)  ((XE(I,J),J=1,NE),I=1,NS)
  809 FORMAT(' XE  ',7X,5(1PE20.13,1X))
      ENDIF
      RETURN
C***********************************************************************
C     END OF -INIMOI-
C***********************************************************************
      END
      SUBROUTINE GENSPE(NS,INPFS,NFILE6,NSYMB,NLINM,SIG,EPS,
     1          XMOL,DI1,DI2,DI3,TLIMIT,AL,
     2         BL,ZRINF,NCHECK,STOP,TEST,NEWMOL)
C***********************************************************************
C
C    ***************************************************************
C    *                                                             *
C    *    PROGRAM FOR THE INPUT AND PRINTOUT OF SPECIES TRANSPORT  *
C    *        AND THERMODYNAMIC PROPERTIES FROM A DATA BASE        *
C    *                     AUTHOR: J.WARNATZ                       *
C    *                         21.02.1983                          *
C    *              LAST CHANGE: 15.07.1984/WARNATZ                *
C    *                                                             *
C    ***************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C     NS            = NUMBER OF SPECIES
C     INPFT         = NUMBER OF INPUT FILE (THERMOCHEM. DATA)
C     INPFS         = NUMBER OF INPUT FILE (SPECIES DATA)
C     NFILE6        = NUMBER OF OUTPUT FILE FOR LISTING
C     NSYMB(NS)      = SPECIES SYMBOLS (NS+NM)
C     TEST          = .TRUE. FOR ADDITIONAL OUTPUT, =.FALSE. OTHERWISE
C
C     OUTPUT :
C
C     NLINM(NS)      = 0 FOR ATOMS, =1 FOR LINEAR MOLECULES, =2 FOR NON-
C                     LINEAR MOLECULES
C     SIG(NS)        = LJ-PARAMETERS SIGMA, A,     (NS)
C     EPS(NS)        = LJ-PARAMETERS EPSILON/K, K, (NS)
C     XMOL(NS)       = MOLAR MASSES, G/MOL, (NS)
C     DI1,2,3(NS)    = FITTING CONSTANTS FOR D(INT), - (NS)
C     TLIMIT        = LIMITING T FOR DI
C     AL,BL(NS)      = COEFFICIENTS OF ZVIB
C     ZRINF(NS)      = INFINITE T ROTATIONAL RELAXATION COLLISION NUMBER
C
C     ALL GIVEN IN UNITS OF J,K MOLE (AFTER TRANSFORMATION)
C
C     CONTROL VALUES
C
C     STOP          = .TRUE. FOR NORMAL END, .FALSE. FOR ABNORMAL END
C
C     INPUT FROM FILES  INPFS,INPFT  :
C
C     SPECIES DATA FOR THE CALCULATION OF THERMODYNAMIC AND TRANSPORT
C     PROPERTIES FROM DATA BASES
C
C***********************************************************************
C
C
C
C***********************************************************************
C     STORAGE ORGANIZATION AND DEFINITION OF INITIAL VALUES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL STOP,TEST,NEWMOL,NCHECK(NS),EQUSYM
C-Character
      PARAMETER(LNREAD=16)
      CHARACTER(LEN=LNREAD) MMMM
      CHARACTER(LEN=*)  NSYMB
      DIMENSION NSYMB(NS),NLINM(NS),SIG(NS),EPS(NS),XMOL(NS),
     1   DI1(NS),DI2(NS),DI3(NS),AL(NS),BL(NS),ZRINF(NS),TLIMIT(NS)
C**********************************************************************
C
C**********************************************************************
      LENSY=LEN(NSYMB(1))
      IF(LNREAD.LT.LENSY) GOTO 910 
C**********************************************************************C
C     INITIAL PREPARATIONS                                             C
C**********************************************************************C
      STOP=.TRUE.
      REWIND INPFS
      DO 10 I=1,NS
   10 NCHECK(I)=.FALSE.
      LAUF=0
C**********************************************************************C
C     READ ONE DATA SET AND CHECK IF IT IS NEEDED                      C
C**********************************************************************C
C-Character only first 8 characters read, erst assumed to be blank
   60 CONTINUE
      IF(.NOT.NEWMOL) THEN
      READ(INPFS,81,END=100) MMMM(1:LENSY),NLINMI,XMOLI,EPSI,SIGI,
     1    DI1I,DI2I,DI3I,ZRINFI,ALI,BLI,TLIMI
   81 FORMAT(A8,I2,5E11.5,/,10X,5E11.5)
      ELSE
C-Format free for NEWMOL
      ZRINFI=0
      ALI=0
      BLI=0 
      TLIMI=0
   71 CONTINUE
c     READ(INPFS,82,END=100) MMMM(1:LENSY),NLINMI,XMOLI,EPSI,SIGI,
      READ(INPFS,*,END=100,ERR=71) MMMM(1:LENSY),NLINMI,XMOLI,EPSI,SIGI,
     1    DI1I,DI2I,DI3I
C  82 FORMAT(A8,I2,6F10.3)
      ENDIF
      DO 70 I=1,NS
      IND=I
      IF(EQUSYM(LENSY,MMMM,NSYMB(I))) GO TO 80
   70 CONTINUE
      GOTO 60
C**********************************************************************C
C     DASET WAS FOUND TO CORRESPOND TO SPECIES NO. IND                 C
C**********************************************************************C
   80 CONTINUE
      NLINM(IND)  = NLINMI
      XMOL(IND)   = XMOLI
      EPS(IND)    = EPSI
      SIG(IND)    = SIGI
      DI1(IND)    = DI1I
      DI2(IND)    = DI2I
      DI3(IND)    = DI3I
      ZRINF(IND)  = ZRINFI
      AL(IND)     = ALI
      BL(IND)     = BLI
      TLIMIT(IND) = TLIMI
      NCHECK(IND)=.TRUE.
      LAUF=LAUF+1
C     IF(LAUF.EQ.NS) GOTO 100
      GOTO 60
C**********************************************************************C
C     END OF DATASET WAS DETECTED, NOW DO FINAL CHECKS
C**********************************************************************C
  100 CONTINUE
      DO 110 I=1,NS
      IF(.NOT.NCHECK(I)) THEN
      WRITE(NFILE6,85) NSYMB(I)
   85 FORMAT(6X,'ERROR - Species ',A,' not found in ',
     1              'species database')
      STOP=.FALSE.
      ENDIF
  110 CONTINUE
C***********************************************************************
C     PRINTOUT OF SPECIES DATA ON UNIT NFILE6
C***********************************************************************
   86 FORMAT(8X,'SPECIES PROPERTIES FROM DATA BASE')
      IF(TEST) WRITE(NFILE6,86)
      DO 130 I=1,NS
   87 FORMAT(2X,A,': M  =',1PE9.2,' G/MOL,',2X,'SIG=',1PE9.2,
     1       ' A   ,',2X,'EPS=',1PE9.2,' K')
      IF(TEST) WRITE(NFILE6,87) NSYMB(I),XMOL(I),SIG(I),EPS(I)
                        MMMM='Atom    '
      IF(NLINM(I).EQ.1) MMMM='linear  '
      IF(NLINM(I).EQ.2) MMMM='non-lin.'
      IF(TEST) WRITE(NFILE6,88) DI1(I),DI1(I),DI3(I),MMMM(1:8)
   88 FORMAT(8X,'DI1=',1PE9.2,6X,',  ','DI2=',1PE9.2,5X,',  ',
     1       'DI3=',1PE9.2,2X,',  ',A8)
  130 CONTINUE
C***********************************************************************
C     Exit
C***********************************************************************
      RETURN
C***********************************************************************
C     Exit
C***********************************************************************
 910  CONTINUE
      WRITE(NFILE6,931) LNSY,LNREAD
  931 FORMAT(' Length of Symbols in molar data (',I2,
     1      ') larger than read characters (',I2,')')
      STOP
C***********************************************************************
C     FORMAT STATEMENTS
C***********************************************************************
C***********************************************************************
C     END OF -GENSPE-
C***********************************************************************
      END
      SUBROUTINE GENOPT(NUMOPT,INPF,NFILE6,NFILE3,NTOPT,NOPT,STOP)
C**********************************************************************C
C                                                                      C
C     *************************************************************    C
C     *                                                           *    C
C     *         CONTROL OF THE PROGRAM FLUX BY OPTIONS            *    C
C     *                    AUTHOR J.WARNATZ                       *    C
C     *                       09.03.1983                          *    C
C     *               LAST CHANGE: 03.09.2001 / O.Maiwald         *    C
C     *************************************************************    C
C                                                                      C
C                                                                      C
C     INPUT  :                                                         C
C                                                                      C
C     INPF          = NUMBER OF INPUT FILE                             C
C     NFILE6        = NUMBER OF OUTPUT FILE (LISTING)                  C
C     NFILE3        = NUMBER OF OUTPUT FILE (STORAGE)                  C
C                                                                      C
C                                                                      C
C     OUTPUT :                                                         C
C                                                                      C
C     NOPT(*)      = NUMBERS >1 FOR TRUE OPTION, =0 ELSEWHERE         C
C     STOP          = .TRUE. FOR NORMAL, =.FALSE. FOR ABNORMAL END     C
C                                                                      C
C                                                                      C
C     INPUT FROM FILE NFILE2 :                                         C
C                                                                      C
C     IN THE FORMAT(7(A4,3X,I3))  OPTIONS (SEE DATA STATEMENT), CLOSED C
C     BY STRING -END -. MEANING OF THE OPTIONS (4 CHARACTERS READ):    C
C                                                                      C
C     OUTPUT ON FILE NFILE3 : Options                                  C
C                                                                      C
C                                                                      C
C**********************************************************************C
C                                                                      C
C                                                                      C
C**********************************************************************C
C     STORAGE ORGANIZATION, INITIAL VALUES                             C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL EQUSYM,STOP
      CHARACTER*8 NTOPT(NUMOPT),N(9)
      DIMENSION NUMB(7),NOPT(NUMOPT)
C***********************************************************************
C     Initialization
C***********************************************************************
      DO 10 II=1,NUMOPT
   10 NOPT(II)=0
      STOP=.TRUE.
C***********************************************************************
C     INPUT OF HEADING FOR OPTIONS FROM FILE INPF
C***********************************************************************
  801 FORMAT(9A8)
C---- Read heading for options
   20 READ(INPF,801,ERR=900,END=900) (N(K),K=1,9)
      IF(.NOT.EQUSYM(4,N(1),'COMM')) GO TO 30
C---- Write comment
  802 FORMAT(' ',74('*'),/,' ','*',4X,8A8,4X,'*',/,' ',74('*'),/,'0')
      WRITE(NFILE6,802) (N(K),K=3,9)
      GO TO  20
   30 CONTINUE
      IF(EQUSYM(4,N(1),'OPTI')) GOTO 100
      GOTO 900
C***********************************************************************
C     INPUT OF OPTIONS FROM FILE INPF
C***********************************************************************
  100 CONTINUE
      DO 110 I=1,7
      N(I)='        '
  110 CONTINUE
      READ(INPF,803,END=910) (N(I),NUMB(I),I=1,7)
  803 FORMAT(7(A6,I3,1X))
      IF(EQUSYM(3,N(1),'END')) GO TO 500
C***********************************************************************
C     LINK OF SYMBOLS AND NUMBERS
C***********************************************************************
      DO 120 I=1,7
      IF(EQUSYM(4,N(I),'    '))    GOTO 120
      IF(EQUSYM(3,N(I),'END'))     GOTO 130
      DO 115 K=1,NUMOPT
      IF(EQUSYM(4,N(I),NTOPT(K))) THEN
        NOPT(K)=NUMB(I)
        IF(NUMB(I).EQ.0) NOPT(K)=1
        GOTO 120
      ENDIF
  115 CONTINUE
C---- ERROR EXIT
  808 FORMAT(6X,'ERROR - Option ',A6,I3,' is incorrect')
      WRITE(NFILE6,808)  N(I),NUMB(I)
      STOP=.FALSE.
  120 CONTINUE
      GOTO 100
  130 CONTINUE
C***********************************************************************
  500 CONTINUE
C***********************************************************************
C     OUTPUT OF OPTIONS ON FILE NFILE6
C***********************************************************************
  861 FORMAT(3X,'Options : ')
      WRITE(NFILE6,861)
      IOPT=0
      DO 160 I = 1,NUMOPT
      IF(NOPT(I).NE.0) THEN
        IOPT = IOPT + 1
        N   (IOPT) = NTOPT(I)
        NUMB(IOPT) = NOPT(I)
      ENDIF
      IF(IOPT.EQ.5.OR.I.EQ.NUMOPT) THEN
        WRITE(NFILE6,863) (N(J),NUMB(J),J=1,IOPT)
        IOPT = 0
      ENDIF
  160 CONTINUE
  863 FORMAT(1(13X,5(A8,I3,2X)))
C***********************************************************************
C     OUTPUT OF OPTIONS ON FILE NFILE3
C***********************************************************************
  830 FORMAT(1(' OPTIONS :',2X,20I3))
      WRITE(NFILE3,830) (NOPT(I),I=1,NUMOPT)
      RETURN
C***********************************************************************
C     Error Exits
C***********************************************************************
C---- Empty data set or wrong heading of options
  900 CONTINUE
  901 FORMAT(6X,'ERROR - Wrong heading of options : ',A8,'...')
      WRITE(NFILE6,901) N(1)
      GOTO 999
  910 CONTINUE
  911 FORMAT(6X,'ERROR - Missing "END" string ')
      WRITE(NFILE6,911) N(1)
      GOTO 999
  999 CONTINUE
      WRITE(NFILE6,998)
  998 FORMAT(3(' ','+++ Error in GENOPT +++',/))
      STOP=.FALSE.
      RETURN
C***********************************************************************
C     END OF -GENOPT-
C***********************************************************************
      END
      SUBROUTINE GENSPD(NS,NFILE1,NFILE3,NFILE6,NSYMB,XMOL,
     1     HCPL,HCPH,HINF,TU,TB,STOP,TEST,NEWMOL,
     1       LRW,RW,LIW,IW,LLW,LW)
C***********************************************************************
C
C    ***************************************************************
C    *                                                             *
C    *                                                             *
C    ***************************************************************
C
C***********************************************************************
C
C
C
C***********************************************************************
C
C
C***********************************************************************
C
C***********************************************************************
C     STORAGE ORGANIZATION,  INITIAL VALUES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL STOP,TEST,NEWMOL,LW
      CHARACTER(LEN=*) NSYMB
      DIMENSION NSYMB(NS),XMOL(NS)
      DIMENSION HCPL(7,NS),HCPH(7,NS),HINF(3,NS)
      DIMENSION RW(LRW),IW(LIW),LW(LLW)
C***********************************************************************
C     Initialization
C***********************************************************************
      STOP=.TRUE.
C***********************************************************************
C     Build up work arrays
C***********************************************************************
C---- Real storage array
      IXMOLG= 1
      ISIG  = IXMOLG + NS
      IEPS  = ISIG   + NS
      IDI1  = IEPS   + NS
      IDI2  = IDI1   + NS
      IDI3  = IDI2   + NS
      ITLIMI= IDI3   + NS
      IAL   = ITLIMI + NS
      IBL   = IAL    + NS
      IZRINF= IBL    + NS
      IXLAM = IZRINF + NS
      IXLMO = IXLAM  + NS * 4
      IETA  = IXLMO  + NS * 4
      IDJK  = IETA   + NS * 4
      INEXT = IDJK   + NS * NS * 4
      NEED  = INEXT -1
      IF(NEED.GT.LRW) GOTO 910
C---- Integer storage array
      INLINM= 1
      INEXT = INLINM + NS
      NEED  = INEXT -1
      IF(NEED.GT.LIW) GOTO 920
C---- Logical storage array
      INCHEC= 1
      INEXT = INCHEC + NS
      NEED  = INEXT -1
      IF(NEED.GT.LLW) GOTO 930
C***********************************************************************
C     INPUT OF SPECIES DATA
C***********************************************************************
      CALL GENSPE(NS,NFILE1,NFILE6,NSYMB,IW(INLINM),RW(ISIG),RW(IEPS),
     1          RW(IXMOLG),RW(IDI1),RW(IDI2),RW(IDI3),
     1          RW(ITLIMI),RW(IAL),RW(IBL),RW(IZRINF),
     1          LW(INCHEC),STOP,TEST,NEWMOL)
      WRITE(NFILE6,80)
      IF(.NOT.STOP) GO TO  999
C***********************************************************************
C     CALCULATION OF TRANSPORT PROPERTIES
C***********************************************************************
      CALL GENTRA(NS,TU,TB,HCPL,HCPH,HINF,XMOL,NSYMB,
     1      STOP,TEST,NEWMOL,NFILE6,NFILE3,
     1      RW(IEPS),RW(ISIG),RW(IDI1),RW(IDI2),
     1      RW(IDI3),RW(ITLIMI),RW(IAL),RW(IBL),
     1      RW(IZRINF),IW(INLINM),
     2      RW(IDJK),RW(IXLAM),RW(IXLMO),RW(IETA))
      WRITE(NFILE6,80)
      IF(.NOT.STOP) GO TO  999
      RETURN
C***********************************************************************
C     ABNORMAL END
C***********************************************************************
  910 CONTINUE
  911 FORMAT(' Needed real work array (',I8,') exceeds',
     1       ' supplied array RW(',I8,')')
      WRITE(NFILE6,911) NEED,LRW
      GOTO 999
  920 CONTINUE
  921 FORMAT(' Needed integer work array (',I8,') exceeds',
     1       ' supplied array IW(',I8,')')
      WRITE(NFILE6,921) NEED,LIW
      GOTO 999
  930 CONTINUE
  931 FORMAT(' Needed logical work array (',I8,') exceeds',
     1       ' supplied array LW(',I8,')')
      WRITE(NFILE6,931) NEED,LLW
      GOTO 999
  999 CONTINUE
 9998 FORMAT(3(/,6X,15('*'),'ABNORMAL END IN GENSPD',15('*')))
      WRITE(NFILE6,9998)
      STOP
C***********************************************************************
C     FORMAT STATEMENTS
C***********************************************************************
   80 FORMAT(2X)
C  81 FORMAT('1',/,1X,79('*'),/,1X,4('*'),5X,
C    1  '   PROGRAM INSINP  (VERSION OF 30.05.1987)                   ',
C    2  5X,4('*'),/,1X,79('*'))
C  82 FORMAT('>INPUT DATA FROM PROGRAM INSINP')
C  83 FORMAT('<   ')
C***********************************************************************
C     END OF MAIN PROGRAM -SLPF1I-
C***********************************************************************
      END
      SUBROUTINE GENTRA(NS,TU,TB,HCPL,HCPH,HINF,XMOL,NSYMB,
     1   STOP,TEST,NEWMOL,NFILE6,NFILE3,
     1   EPS,SIG,DI1,DI2,
     1   DI3,TLIMIT,AL,BL,ZRINF,NLINM,
     2   DJK,XLAM,XLMO,ETA)
C***********************************************************************
C
C    ***************************************************************
C    *                                                             *
C    *     CALCULATION OF THE HEAT CONDUCTIVITIES, VISCOSITIES,    *
C    *               AND BINARY DIFFUSION COEFFICIENTS             *
C    *                      AUTHOR J.WARNATZ                       *
C    *                                                             *
C    ***************************************************************
C
C***********************************************************************
C     INPUT  :
C
C     NLINM(NS)     = 0 FOR ATOMS, =1 FOR LINEAR MOLECULES, =2 FOR NON-
C                     LINEAR MOLECULES
C     SIG(NS)        = LJ-PARAMETERS SIGMA, A,     (NS)
C     EPS(NS)        = LJ-PARAMETERS EPSILON/K, K, (NS)
C     XMOL(NS)       = MOLAR MASSES, KG/MOL, (NS)
C     DI1,2,3(NS)    = FITTING CONSTANTS FOR D(INT), - (NS)
C     TLIMIT        = LIMITING T FOR DI
C     AL,BL(NS)      = COEFFICIENTS OF ZVIB
C     ZRINF(NS)      = INFINITE T ROTATIONAL RELAXATION COLLISION NUMBER
C
C     THERMODYNAMIC DATA
C
C     HCPL(7,NS)           = NASA COEFF. OF H  (7,NS), T<1000K
C     HCPH(7,NS)           = NASA COEFF. OF H  (7,NS), T>1000K
C     HCPH(1,I),HCPL(1,I) = STANDARD MOLAR HEAT CAPACITY
C     HCPH(6,I),HCPL(6,I) = STANDARD MOLAR ENTHALPY
C     HCPH(7,I),HCPL(7,I) = STANDARD MOLAR ENTROPY
C     ALL GIVEN IN SI UNITS (J, K, MOLE)
C
C     CONTROL VALUES
C     STOP          = .TRUE. FOR NORMAL END, .FALSE. FOR ABNORMAL END
C
C     OUTPUT:
C
C     XLAM(4,NS)    = COEFF. OF LOG(LAMBDA) (J/S*K*M)
C     XLMO(4,NS)    = COEFF. OF LOG(LAMBDA,MON) (J/S*K*M)
C     ETA(4,NS)     = COEFF. OF LOG(ETA) (KG/M*S)
C     STOP         = .TRUE. FOR NORMAL END, =.FALSE. OTHERWISE
C     INPUT, OUTPUT: SEE SUBROUTINES GENLAM, GENDIF
C
C     OUTPUT ON FILE NFILE3 : SEE COMMENT BELOW
C
C***********************************************************************
C
C
C***********************************************************************
C     STORAGE ORGANIZATION, INITIAL VALUES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL TEST,STOP,NEWMOL
      CHARACTER(LEN=*)  NSYMB
      DIMENSION HCPL(7,NS),HCPH(7,NS),HINF(3,NS),
     1    XMOL(NS),EPS(NS),SIG(NS),DI1(NS),DI2(NS),
     1          DI3(NS),AL(NS),BL(NS),NSYMB(NS),
     1     NLINM(NS),DJK(4,NS,NS),
     2          XLAM(4,NS),XLMO(4,NS),ETA(4,NS),ZRINF(NS),TLIMIT(NS)
C***********************************************************************
C     Determine inteval for polynomial fit
C***********************************************************************
      NTO=IDINT((TB+101.0)/100.0)
      NTU=IDINT((TU-  1.0)/100.0)
      IF((NTO-NTU).GT.50) GOTO 910
      IF((NTO-NTU).LT.4)  GOTO 920
C***********************************************************************
C
C     CALCULATION OF TRANSPORT PROPERTIES
C
C***********************************************************************
C---- change molar masses into units of g/mol
      DO 465 I = 1,NS
      XMOL(I) = 1000.0D0 * XMOL(I)
  465 CONTINUE
C***********************************************************************
C     CALCULATION OF VISCOSITIES AND HEAT CONDUCTIVITIES
C***********************************************************************
      CALL GENLAM(NS,NTU,NTO,HCPL,HCPH,HINF,
     1   XMOL,SIG,EPS,NLINM,DI1,DI2,DI3,
     1   TLIMIT,AL,BL,ZRINF,NSYMB,NFILE6,
     2    XLAM,XLMO,ETA,STOP,TEST,NEWMOL)
C***********************************************************************
C     CALCULATION OF BINARY DIFFUSION COEFFICIENTS
C***********************************************************************
      CALL GENDIF(NS,EPS,SIG,DI1,DI2,XMOL,NSYMB,NTU,NTO,NFILE6,
     1            DJK,STOP,TEST,NEWMOL)
C---- CHANGE BACK INTO SI UNITS
      DO 1804 I=1,NS
 1804 XMOL(I)=XMOL(I)/1000.0
C***********************************************************************
C
C     OUTPUT OF THE SECONDARY DATA (SPECIES AND THERMODYNAMIC DATA):
C
C***********************************************************************
C***********************************************************************
C     OUTPUT OF COEFFICIENTS FOR HEAT CONDUCTION
C***********************************************************************
C---- XLAM(4,NS)    = COEFF. OF LOG(LAMBDA) (J/S*K*M)
      DO 813 I=1,NS
  812 FORMAT(' XLAM(',I4,')',1X,5(1PE11.4,1X))
  813 WRITE(NFILE3,812) I,(XLAM(K,I),K=1,4)
C---- XLMO(4,NS) = COEFF. OF LOG(LAMBDA,MON) (J/S*K*M)
      DO 1813 I=1,NS
 1812 FORMAT(' XLMO(',I4,')',1X,5(1PE11.4,1X))
 1813 WRITE(NFILE3,1812) I,(XLMO(K,I),K=1,4)
C***********************************************************************
C     OUTPUT OF VISCOSITY COEFFICIENTS
C***********************************************************************
C---- ETA(4,NS)     = COEFF. OF LOG(ETA) (KG/M*S)
      DO 2813 I=1,NS
 2812 FORMAT(' ETA (',I4,')',1X,5(1PE11.4,1X))
 2813 WRITE(NFILE3,2812) I,(ETA(K,I),K=1,4)
C***********************************************************************
C     OUTPUT OF BINARY DIFFUSION COEFFICIENTS
C***********************************************************************
C---- DJK = COEFF. OF LOG(D) (IN M**2/S) AT P=1 PA
      DO 815 I=1,NS
      DO 815 J=1,I
  814 FORMAT(' D(',2I4,')',5(1PE11.4,1X))
  815 WRITE(NFILE3,814) I,J,(DJK(K,I,J),K=1,4)
C***********************************************************************
C     OUTPUT OF LJ- PARAMETERS
C***********************************************************************
C---- EPS/K
  843 FORMAT(' EPS/K',6X,5(1PE11.4,1X))
      WRITE(NFILE3,843)  (EPS(I),I=1,NS)
C---- SIGMA IN AENGSTROEM
  845 FORMAT(' SIGMA  ',4X,5(1PE11.4,1X))
      WRITE(NFILE3,845)  (SIG(I),I=1,NS)
C***********************************************************************
C     OUTPUT OF DI USED IN TRANSPORT MIXING RULES
C***********************************************************************
  846 FORMAT(' DI1    ',4X,5(1PE11.4,1X))
      WRITE(NFILE3,846)  (DI1(I),I=1,NS)
  847 FORMAT(' DI2    ',4X,5(1PE11.4,1X))
      WRITE(NFILE3,847)  (DI2(I),I=1,NS)
  848 FORMAT(' DI3    ',4X,5(1PE11.4,1X))
      WRITE(NFILE3,848)  (DI3(I),I=1,NS)
C***********************************************************************
C     OUTPUT OF THERMODYNAMIC DATA ON NFILE3 (SI UNITS)
C***********************************************************************
C
C     HCPL(7,NS)           = NASA COEFF. OF H  (7,NS), T<1000K
C     HCPH(7,NS)           = NASA COEFF. OF H  (7,NS), T>1000K
C     HCPH(1,I),HCPL(1,I) = STANDARD SPECIFIC HEAT CAPACITY
C     HCPH(6,I),HCPL(6,I) = STANDARD SPECIFIC ENTHALPY
C     HCPH(7,I),HCPL(7,I) = STANDARD SPECIFIC ENTROPY
C
C     ALL GIVEN IN UNITS OF J,KG AND KELVIN
C
C***********************************************************************
      DO 811 I=1,NS
  810 FORMAT(' HL,HH(',I4,')',5(1PE11.4,1X),/,
     1           5(1PE11.4,1X),/,
     1           4(1PE11.4,1X),1PE10.3,1X,1PE10.3,1X,1PE10.3)
  811 WRITE(NFILE3,810) I,
     1           (HCPL(K,I)/XMOL(I),K=1,7),
     1           (HCPH(K,I)/XMOL(I),K=1,7),
     1           (HINF(K,I),K=1,3)
      RETURN
C***********************************************************************
C     Error exits
C***********************************************************************
  910 CONTINUE
  911 FORMAT(5X,'TEMPERATURE INTERVAL TOO LARGE, TB-TU > 5000 K')
      WRITE(NFILE6,911)
      GOTO 999
  920 CONTINUE
  921 FORMAT(5X,'TEMPERATURE INTERVAL TOO SMALL, TB-TU < 300 K')
      WRITE(NFILE6,921)
      GOTO 999
  999 CONTINUE
      STOP=.FALSE.
      RETURN
C***********************************************************************
C     END OF -GENTRA-
C***********************************************************************
      END
      SUBROUTINE GENLAM(NS,NTU1,NTO,HCPL,HCPH,HINF,
     1          XMOL,SIG,EPS,NLINM,DI1,
     1          DI2,DI3,TLIMIT,AL,BL,ZRINF,NSYMB,NFILE6,XLAM,XLMO,
     2          ETA,STOP,TEST,NEWMOL)
C***********************************************************************
C
C    ***************************************************************
C    *                                                             *
C    *  PROGRAM FOR THE CALC. OF HEAT CONDUCTIV. AND VISCOSITIES   *
C    *                    AUTHOR: J.WARNATZ                        *
C    *                        24.02.1983                           *
C    *              LAST CHANGE: 15.05.1984/WARNATZ                *
C    *                                                             *
C    ***************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C     NS           = NUMBER OF SPECIES
C     NTU,NTU1     = IDINT((TU-1K)/100.0)
C     NTO          = IDINT((TB+101K)/100.0)
C     HCPL(7,NS)    = COEFF. OF MOLAR ENTHALPY, J,K(7,NS), T <1000K
C     HCPH(7,NS)    = COEFF. OF MOLAR ENTHALPY, J,K(7,NS), T >1000K
C     XMOL(NS)      = MOLAR MASS, G/MOL (NS)
C     SIG(NS)       = LJ-SIGMA, A (NS)
C     EPS(NS)       = LJ-EPS/K, K (NS)
C     NLINM(NS)     = 0 ATOMS, =1 FOR LINEAR MOLECULES, =2 FOR NON-
C                    LINEAR MOLECULES
C     DI1,2,3(NS)   = COEFFICIENTS OF D(INT), - (NS)
C     TLIMIT       = LIMITING T FOR DI
C     AL,BL(NS)     = COEFFICIENTS OF ZVIB
C     ZRINF(NS)     = INFINITE T ROTATIONAL RELAXATION COLLISION NUMBER
C     NSYMB(NS)     = SPECIES SYMBOLS
C     NFILE6       = LOGICAL NUMBER OF OUTPUT FILE (LISTING)
C
C     OUTPUT :
C
C     XLAM(4,NS)    = COEFF. OF LOG(LAMBDA) (J/S*K*M)
C     XLMO(4,NS)    = COEFF. OF LOG(LAMBDA,MON) (J/S*K*M)
C     ETA(4,NS)     = COEFF. OF LOG(ETA) (KG/M*S)
C     STOP         = .TRUE. FOR NORMAL END, =.FALSE. OTHERWISE
C
C     OUTPUT ON FILE NFILE3 :
C
C     XLAM(4,NS)    = COEFF. OF LOG(LAMBDA) (J/S*K*M)
C     XLMO(4,NS)    = COEFF. OF LOG(LAMBDA,MON) (J/S*K*M)
C     ETA(4,NS)     = COEFF. OF LOG(ETA) (KG/M*S)
C
C
C
C***********************************************************************
C
C
C
C***********************************************************************
C     STORAGE ORGANIZATION, INITIAL VALUES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      PARAMETER (NTEMP = 50)
      LOGICAL STOP,TEST,NEWMOL
      CHARACTER(LEN=*)  NSYMB
      DOUBLE PRECISION MXIOME
      DIMENSION XMOL(NS),SIG(NS),EPS(NS)
      DIMENSION HCPL(7,NS),HCPH(7,NS),HINF(3,NS),
     1    TLIMIT(NS),XLAM(4,NS),XLMO(4,NS),NLINM(NS),
     1    DI1(NS),DI2(NS),DI3(NS),
     2    AL(NS),BL(NS),ZRINF(NS),ETA(4,NS),NSYMB(NS)
C---- PROVIDE STORAGE FOR NTEMP TEMPERATURES
      DIMENSION C(4),HELP(NTEMP),TEMP(NTEMP)
      DIMENSION A(NTEMP,4),X(NTEMP),Y(NTEMP),
     1          XLM(NTEMP),XLA(NTEMP),XETA(NTEMP)
      DATA NDI/4/,RGAS/8.3143D0/,ALER/0.02D0/
C***********************************************************************
C     INITIALIZATION
C***********************************************************************
      STOP=.TRUE.
      NTU=NTU1
      IF(NTU.LT.2) NTU=2
      MMAX=NTEMP
      M=NTO-NTU+1
C***********************************************************************
C
C     CALC. OF LAMBDA AND ETA: LOOP FOR SPECIES
C
C***********************************************************************
      DO 700 K=1,NS
      IF(TEST) WRITE(NFILE6,805) NSYMB(K)
C***********************************************************************
C
C     CALC. OF LAMBDA AND ETA: LOOP FOR T
C
C***********************************************************************
      DO 100 I=NTU,NTO
      T=FLOAT(I)*100.0D0
      TEMP(I)=T
      TRED=T/EPS(K)
      IF(NEWMOL) THEN
C***********************************************************************
C
C     CALC. OF LAMBDA AND ETA (new version)
C
C***********************************************************************
      TR=TRED
      HELP1=2.662D-3*DSQRT(1.0D0/XMOL(K))/(SIG(K)*SIG(K))
C---- CALCULATE ROTATIONAL RELAXATION NUMBER
      CALL MXIPAR(T,EPS(K),PARK)
      ZROT=DI3(K)*PARK
      IF(ZROT.LT.1.0E-20) ZROT=1.0E20
C---- CALCULATE HEAT CAPACITIES (J/MOL*K)
      CALL CALCPI(K,T,HCPL,HCPH,HINF,CP,NFILE6)
      CV=CP-RGAS
                        F=0.0
      IF(NLINM(K).EQ.0) F=0.0
      IF(NLINM(K).EQ.1) F=1.0
      IF(NLINM(K).EQ.2) F=1.5
      CVTRA=1.5*RGAS
      CVROT=F*RGAS
      CVVIB=CV-CVROT-CVTRA
C***********************************************************************
C     CALCULATION OF ETA, RO, AND D
C***********************************************************************
C---- ETA (G/CM*S)
      DST=3621.6*DI1(K)*DI1(K)/(EPS(K)*SIG(K)*SIG(K)*SIG(K))
      HELP2=2.669D-5*DSQRT(XMOL(K)*T)/(SIG(K)*SIG(K))
      XETA(I)=HELP2/MXIOME(2,TR,DST,NFILE6,STOP)
      IF(.NOT.STOP) RETURN
C---- D (CM**2/S AT P=1BAR)
      HELP3=MXIOME(1,TR,DST,NFILE6,STOP)
      DKK=DSQRT(T*T*T)*HELP1/HELP3
      IF(.NOT.STOP) RETURN
C---- RESONANCE CORRECTION FOR DKK
      DELTA=1.0D0
      IF(NSYMB(K)(1:8).NE.'H2O     ') GO TO 373
      DELTA=1.0D0+2.37D6/(SIG(K)**2*HELP3*T**2*(CVROT+CVVIB)/RGAS)
      DKK=DKK/DELTA
  373 CONTINUE
C---- RO (G/CM**3 AT P=1BAR)
      RO=0.01203*XMOL(K)/T
C***********************************************************************
C     CALC. OF MODIFIED EUCKEN CORRECTIONS
C***********************************************************************
      RODET=RO*DKK/XETA(I)
      AA=2.5-RODET
      BB=ZROT+0.63662*(1.6667*CVROT/RGAS+RODET)
      FTRA=2.5*(1.0-0.63662*CVROT*AA/(CVTRA*BB))
      FROT=RODET*(1.0+0.63662*AA/BB)
      FVIB=RODET
C---- LAMBDA (J/CM*K*S)
      XLA(I)=XETA(I)*(FTRA*CVTRA+FROT*CVROT+FVIB*CVVIB)/XMOL(K)
      XLM(I)=XETA(I)*FTRA*CVTRA/XMOL(K)
      ELSE
C***********************************************************************
C
C     CALCULATION OF LAMBDA AND ETA (old version)
C
C***********************************************************************
      ZROT=ZRINF(K)/(1.0+2.784D0/DSQRT(TRED)+5.609D0/TRED)
      ZVIB=10.0D0**(AL(K)+BL(K)*T**(-0.33333D0))
      CALL CALCPI(K,T,HCPL,HCPH,HINF,CP,NFILE6)
      CV=CP-RGAS
      IF(NLINM(K).EQ.0) F=0.0D0
      IF(NLINM(K).EQ.1) F=1.0D0
      IF(NLINM(K).EQ.2) F=1.5D0
      IF(ZROT.LT.1.0D-20) ZROT=1.0D20
      IF(ZVIB.LT.1.0D-20) ZVIB=1.0D20
      CVROT=F*RGAS
      CVINT=CP-5.0D0*RGAS/2.0D0
      CVVIB=CVINT-CVROT
      ZINT=ZROT+ZVIB
      CVRZ=(CVROT/ZROT+CVVIB/ZVIB)/RGAS
      TRLOG=DLOG(TRED)
      TRL=DLOG(TRED)
      TRL2=TRL*TRL
      TRL4=TRL2*TRL2
      OMEG=DEXP(0.46649D0-0.57015D0*TRL+0.19164D0*TRL2-0.03708D0*
     1         TRL2*TRL+0.00241D0*TRL4)
      XETA(I)=2.669D0*1.0D-5*DSQRT(XMOL(K)*T)/(OMEG*SIG(K)*SIG(K))
C***********************************************************************
C     CALC. OF MODIFIED EUCKEN CORRECTIONS
C***********************************************************************
      XI=1.0D0+0.0042D0*(1.0D0-DEXP(0.33D0*(1.0D0-TRED)))
      AST=DEXP(0.1281D0-0.1108D0*TRL+0.0962D0*TRL2-0.0271D0*
     1         TRL2*TRL+0.0024D0*TRL4)
      DD=DI1(K)+DI2(K)*T+DI3(K)*T*T
      IF(T.GT.TLIMIT(K))
     F    DD=DI1(K)+DI2(K)*TLIMIT(K)+DI3(K)*TLIMIT(K)*TLIMIT(K)
      DEL=0.63661D0*CVRZ*(2.5D0-1.2D0*AST*DD)/
     1   (1.0D0+1.061033D0*CVRZ+12.0D0*AST*DD/
     2    (15.708D0*ZINT))
      XLINT=RGAS*XETA(I)*1.2D0*AST*DD*(CVINT/RGAS+DEL)/XMOL(K)
      XLTRA=2.5D0*RGAS*XETA(I)*(1.5D0-DEL)*XI/XMOL(K)
      XLA(I)=XLINT+XLTRA
      TLG=DLOG10(T)
      IF(NSYMB(K)(1:8).EQ.'H2O     ')
     1           XLA(I)=10**(6.6493D0-13.123D0*TLG+5.0499D0*TLG*TLG
     2                           -0.583D0*TLG*TLG*TLG)
      XLM(I)=XLTRA*1.5D0/(1.5D0-DEL)
      ENDIF
C***********************************************************************
C     CHANGE TO SI-UNITS
C***********************************************************************
      XLA(I)  = XLA(I) *100.0D0
      XLM(I)  = XLM(I) *100.0D0
      XETA(I) = XETA(I)*  0.1D0
  100 CONTINUE
C***********************************************************************
C     POLYNOMIAL FIT FOR LAMBDA
C***********************************************************************
      IF(TEST) WRITE(NFILE6,821)
      CALL LOGFIT(M,NDI,TEMP(NTU),XLA(NTU),HELP,C,
     1          ERRM,IERM,NFILE6,A,X,Y,STOP,TEST)
      IF(.NOT.STOP) GOTO 910
      DO 30 I=1,NDI
   30 XLAM(I,K)=C(I)
      IF(TEST) WRITE(NFILE6,822) (XLAM(I,K),I=1,NDI)
      IF(ERRM.GT.ALER) WRITE(NFILE6,825) ERRM,NSYMB(K),TEMP(NTU+IERM-1)
C***********************************************************************
C     POLYNOMIAL FIT FOR MONOATOMIC PART OF LAMBDA
C***********************************************************************
      IF(TEST) WRITE(NFILE6,831)
      CALL LOGFIT(M,NDI,TEMP(NTU),XLM(NTU),HELP,C,
     1          ERRM,IERM,NFILE6,A,X,Y,STOP,TEST)
      IF(.NOT.STOP)  GO TO 910
      DO 1030 I=1,NDI
 1030 XLMO(I,K)=C(I)
      IF(TEST) WRITE(NFILE6,832) (XLMO(I,K),I=1,NDI)
      IF(ERRM.GT.ALER) WRITE(NFILE6,835) ERRM,NSYMB(K),TEMP(NTU-1+IERM)
C***********************************************************************
C     POLYNOMIAL FIT FOR ETA
C***********************************************************************
      IF(TEST) WRITE(NFILE6,841)
      CALL LOGFIT(M,NDI,TEMP(NTU),XETA(NTU),HELP,C,
     1          ERRM,IERM,NFILE6,A,X,Y,STOP,TEST)
      IF(.NOT.STOP)  GOTO 910
      DO 2030 I=1,NDI
 2030 ETA(I,K)=C(I)
      IF(TEST)         WRITE(NFILE6,842) (ETA(I,K),I=1,NDI)
      IF(ERRM.GT.ALER) WRITE(NFILE6,845) ERRM,NSYMB(K),TEMP(NTU-1+IERM)
C***********************************************************************
C     END OF LOOP OVER SPECIES
C***********************************************************************
  700 CONTINUE
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
  910 CONTINUE
      WRITE(NFILE6,911)
  911 FORMAT(1X,' ERROR WHILE FITTING DATA FOR -GENLAM- ')
      GOTO 999
  999 STOP=.FALSE.
      RETURN
C***********************************************************************
C     FORMAT STATEMENTS
C***********************************************************************
  805 FORMAT(/,4X,'RESULTS OF -INLAM- FOR SPECIES ',A,' AT 1 BAR',/)
  821 FORMAT(1X,4X,' VALUES OF HEAT CONDUCTIVITY (J/(M*K*S))',/)
  822 FORMAT(1X,4X,'COEFF. OF LAMBDA :',4(3X,1PE9.2),/)
  825 FORMAT(3X,'WARNING: FIT OF LAMBDA COEFFICIENT CAUSES',
     1          ' AN ERROR OF',/,3X,2PF5.1,'%  FOR SPECIES ',
     1          A,' AT ',0PF5.0,' K')
  831 FORMAT(1X,5X,'VALUES OF MONOATOMIC PART OF LAMBDA (J/(M*K*S))',/)
  832 FORMAT(1X,4X,'COEFF. OF LAMMON :',4(3X,1PE9.2),/)
  835 FORMAT(3X,'WARNING: FIT OF XLM       COEFFICIENT CAUSES',
     1          ' AN ERROR OF',/,3X,2PF5.1,'%  FOR SPECIES ',
     1          A,' AT ',0PF5.0,' K')
  841 FORMAT(1X,4X,' VALUES OF VISCOSITY (KG/(M*S))',/)
  842 FORMAT(1X,4X,'COEFF. OF ETA    :',4(3X,1PE9.2))
  845 FORMAT(3X,'WARNING: FIT OF VISCOSITY COEFFICIENT CAUSES',
     1          ' AN ERROR OF',/,3X,2PF5.1,'%  FOR SPECIES ',
     1          A,' AT ',0PF5.0,' K')
C***********************************************************************
C     END OF -GENLAM-
C***********************************************************************
      END
      SUBROUTINE INIAGL(A,Y,M,N,EPS,X,STOP,NFILE6)
C***********************************************************************
C
C    ***************************************************************
C    *                                                             *
C    * FITTING OF M POINTS BY A POLYNOM OF ORDER N (M<MMAX POINTS) *
C    *                     AUTHOR: J.WARNATZ                       *
C    *                        24.02.1983                           *
C    *              LAST CHANGE: 15.05.1984/WARNATZ                *
C    *                                                             *
C    *                                                             *
C    ***************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C     A(M,N)       = COEFFICIENT MATRIX ( =X(I)**(J-1); I=1,N ; J=1,N) )
C     Y(M)         = VECTOR OF Y-VALUES AT M POINTS
C     M            = NUMBER OF POINTS (M>N)
C     MMAX         = DIMENSION OF FIRST INDEX OF A (MMAX>M)
C     N            = ORDER OF POLYNOMIAL
C     EPS          = RELATIVE ERROR TOLERANCE
C
C     OUTPUT :
C
C     Z(N)         = VECTOR OF POLINOMIAL COEFFICIENTS
C     STOP         = .TRUE. FOR NORMAL END, .FALSE. FOR ABNORMAL END
C
C***********************************************************************
C
C
C
C***********************************************************************
C     STORAGE ORGANIZATION, INITIAL VALUES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION A(M,N),Y(M),X(N),C(1275)
      LOGICAL STOP
      INTEGER P,Q
      IF(N.GT.M)  GO TO 998
      STOP=.TRUE.
C***********************************************************************
C     DETERM. OF POLYNOM COEFFICIENTS (HRZ OF TH DARMSTADT)
C***********************************************************************
      L=0
      DO 100 I=1,N
      S=0.
      DO 10 K=1,M
   10 S=A(K,I)*Y(K)+S
      X(I)=S
      DO 20 K=1,I
      S=0.
      DO 15 J=1,M
   15 S=S+A(J,I)*A(J,K)
      KL=K+L
   20 C(KL)=S
  100 L=L+I
      P=0
      DO 160 I=1,N
      Q=P+1
      L=0
      DO 160 J=1,I
      S=C(P+1)
      IF(P.LT.Q)  GO TO 111
      DO 110 K=Q,P
      L=L+1
  110 S=S-C(K)*C(L)
  111 L=L+1
      P=P+1
      IF(I.EQ.J)  GO TO 150
      C(P)=S*C(L)
      GO TO 160
  150 IF(S.LE.EPS)  GO TO 999
      C(P)=1./DSQRT(S)
  160 CONTINUE
      P=1
      DO 210 I=1,N
      Q=I-1
      S=X(I)
      IF(Q.LT.1)  GO TO 205
      DO 200 K=1,Q
      S=S-C(P)*X(K)
  200 P=P+1
  205 X(I)=S*C(P)
  210 P=P+1
      DO 300 I=1,N
      II=N+1-I
      P=P-1
      L=P
      Q=II+1
      S=X(II)
      IF(Q.GT.N)  GO TO 300
      DO 220 K=Q,N
      KK=N+Q-K
      S=S-C(L)*X(KK)
  220 L=L-KK+1
  300 X(II)=S*C(L)
      RETURN
C***********************************************************************
C     ERROR EXIT
C***********************************************************************
  998 WRITE(NFILE6,900)
  900 FORMAT(/,5X,'NUMBER OF UNKNOWN VALUES IN -INIAGL- TOO LARGE')
      STOP=.FALSE.
      RETURN
  788 FORMAT(/,5X,'STATIONARITY IN -INIAGL-')
  999 WRITE(NFILE6,788)
      STOP=.FALSE.
      RETURN
C***********************************************************************
C     END OF -INIAGL-
C***********************************************************************
      END
      SUBROUTINE GENDIF(NS,EPS,SIG,DI1,DI2,XMOL,NSYMB,NTU1,NTO,
     1                  NFILE6,DJK,STOP,TEST,NEWMOL)
C***********************************************************************
C
C    ***************************************************************
C    *                                                             *
C    *     PROGRAM FOR THE CALCULATION OF DIFFUSION COEFFICIENTS   *
C    *                     AUTHOR: J.WARNATZ                       *
C    *                        24.02.1983                           *
C    *              LAST CHANGE: Maas                              *
C    *                                                             *
C    ***************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C     NS           = NUMBER OF SPECIES
C     EPS(*)       = LJ-EPS/K (K)
C     SIG(*)       = LJ-SIGMA (A)
C     XMOL(*)      = MOLAR MASS (G/MOL)
C     NSYMB(*)     = SPECIES SYMBOLS
C     NTU,NTU1     = INT((TU-1K)/100.0)
C     NTO          = INT((TB+101K)/100.0)
C     NFILE6       = LOGICAL NUMBER OF OUTPUT FILE (LISTING)
C     TSO          = .TRUE. FOR OUTPUT 80 CHARACTERS
C
C     OUTPUT :
C
C     DJK          =  COEFF. OF LOG(D) (IN M**2/S) AT P=1 PA
C     STOP         = .TRUE. FOR NORMAL END, =.FALSE. OTHERWISE
C
C***********************************************************************
C
C
C
C***********************************************************************
C     STORAGE ORGANIZATION, INITIAL VALUES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      PARAMETER (NTEMP = 50,LENSPE=21)
      LOGICAL STOP,TEST,NEWMOL,EQUSYM
      DOUBLE PRECISION MXIOME
      CHARACTER(LEN=LENSPE)  JSYMB,KSYMB
      CHARACTER(LEN=*)  NSYMB
      EXTERNAL DIFFIT
      DIMENSION DJK(4,NS,NS),EPS(NS),SIG(NS),XMOL(NS),NSYMB(NS),
     1   DI1(NS),DI2(NS),
     1   DT(NTEMP),X(NTEMP),HELP(NTEMP),A(NTEMP,4),TEMP(NTEMP),Y(NTEMP)
      DIMENSION C(4)
C---- INPUT FOR IDENTIFICATION OF SPECIES SYMBOLS
      STOP=.TRUE.
      DATA NDI/4/
      NTU=NTU1
      IF(NTU.LT.2) NTU=2
      M=NTO-NTU+1
      MMAX=NTEMP
C**********************************************************************C
C     Initialization                                                   C    
C**********************************************************************C
      LNSY = LEN(NSYMB(1))
      IF(LENSPE.LT.LNSY) THEN 
        WRITE(NFILE6,*) 'Species length larger than', LENSPE
        STOP 
      ENDIF
C***********************************************************************
C     DETAILED OUTPUT FOR TEST
C***********************************************************************
      IF(TEST)       WRITE(NFILE6,3)
    3 FORMAT(/,7X,'RESULTS OF GENDIF AT P = 1.00 BAR')
C***********************************************************************
C     CALC. OF D: LOOP FOR SPECIES PAIRS
C***********************************************************************
      DO 100 J=1,NS
      JSYMB = NSYMB(J)//REPEAT(' ',LENSPE-LNSY)
      DO 100 K=1,J
      KSYMB = NSYMB(K)//REPEAT(' ',LENSPE-LNSY)
  666 FORMAT(/,2X)
      IF(TEST) WRITE(NFILE6,666)
    6 FORMAT(1X,5X,'SPECIES PAIR ',A,'-',A)
      IF(TEST) WRITE(NFILE6,6) JSYMB,KSYMB
C***********************************************************************
C     CALC. OF MEAN VALUES OF EPS AND SIGMA
C***********************************************************************
      HELP1=(XMOL(J)+XMOL(K))/(2.0D0*XMOL(J)*XMOL(K))
      IF(NEWMOL) THEN
      IF((DI1(J).LT.1.0D-20).AND.(DI1(K).GT.1.0D-20)) THEN
C---- MOLECULE K IS POLAR
      DST=0.0D0
      XI=1.0+0.25*(DI2(J)/SIG(J)**3)*(7243.23*DI1(K)**2/(EPS(K)*
     F   SIG(K)**3))*DSQRT(EPS(K)/EPS(J))
      SIGM=(SIG(J)+SIG(K))*0.5*XI**(-0.166666667)
      EPSM=DSQRT(EPS(J)*EPS(K))*XI**2
      ELSE IF((DI1(J).GT.1.0D-20).AND.(DI1(K).LT.1.0D-20)) THEN
C  80 CONTINUE
C---- MOLECULE J IS POLAR
      DST=0.0D0
      XI=1.0+0.25*(DI2(K)/SIG(K)**3)*(7243.23*DI1(J)**2/(EPS(J)*
     F   SIG(J)**3))*DSQRT(EPS(J)/EPS(K))
      SIGM=(SIG(J)+SIG(K))*0.5*XI**(-0.166666667)
      EPSM=DSQRT(EPS(J)*EPS(K))*XI**2
      ELSE
C---- BOTH MOLECULES POLAR OR NON-POLAR
      EPSM=DSQRT(EPS(J)*EPS(K))
      SIGM=(SIG(J)+SIG(K))/2.0
      DST =3621.5*DI1(J)*DI1(K)/(EPSM*SIGM**3)
      ENDIF
      ELSE
C---- CALCULATE T-INDEPENDENT TERM
      EPSM=DSQRT(EPS(J)*EPS(K))
      SIGM=(SIG(J)+SIG(K))/2.0
C---- OVERJUMP IF OTHER MOLECULES THAN FOLLOWING
      IF(EQUSYM(8,JSYMB,'H2      ')) THEN
        IF(EQUSYM(8,KSYMB,'H2      ')) EPSM= 99.5
        IF(EQUSYM(8,KSYMB,'H2      ')) SIGM=2.690
        IF(EQUSYM(8,KSYMB,'O2      ')) EPSM=112.1
        IF(EQUSYM(8,KSYMB,'O2      ')) SIGM=2.793
        IF(EQUSYM(8,KSYMB,'N2      ')) EPSM=101.8
        IF(EQUSYM(8,KSYMB,'N2      ')) SIGM=3.098
        IF(EQUSYM(8,KSYMB,'H2O     ')) EPSM=241.0
        IF(EQUSYM(8,KSYMB,'H2O     ')) SIGM=2.592
        IF(EQUSYM(8,KSYMB,'CO      ')) EPSM=101.8
        IF(EQUSYM(8,KSYMB,'CO      ')) SIGM=3.129
        IF(EQUSYM(8,KSYMB,'CO2     ')) EPSM=156.2
        IF(EQUSYM(8,KSYMB,'CO2     ')) SIGM=3.036
        IF(EQUSYM(8,KSYMB,'NH3     ')) EPSM=206.0
        IF(EQUSYM(8,KSYMB,'NH3     ')) SIGM=2.785
        IF(EQUSYM(8,KSYMB,'AR      ')) EPSM=118.7
        IF(EQUSYM(8,KSYMB,'AR      ')) SIGM=2.854
        IF(EQUSYM(8,KSYMB,'HE      ')) EPSM= 32.2
        IF(EQUSYM(8,KSYMB,'HE      ')) SIGM=2.752
      ENDIF
      IF(EQUSYM(8,JSYMB,'O2      ')) THEN
        IF(EQUSYM(8,KSYMB,'H2      ')) EPSM=112.1
        IF(EQUSYM(8,KSYMB,'H2      ')) SIGM=2.793
        IF(EQUSYM(8,KSYMB,'O2      ')) EPSM=126.3
        IF(EQUSYM(8,KSYMB,'O2      ')) SIGM=3.382
        IF(EQUSYM(8,KSYMB,'N2      ')) EPSM=110.2
        IF(EQUSYM(8,KSYMB,'N2      ')) SIGM=3.516
        IF(EQUSYM(8,KSYMB,'H2O     ')) EPSM=271.5
        IF(EQUSYM(8,KSYMB,'H2O     ')) SIGM=3.133
        IF(EQUSYM(8,KSYMB,'CO      ')) EPSM=114.7
        IF(EQUSYM(8,KSYMB,'CO      ')) SIGM=3.506
        IF(EQUSYM(8,KSYMB,'CO2     ')) EPSM=155.7
        IF(EQUSYM(8,KSYMB,'CO2     ')) SIGM=3.599
        IF(EQUSYM(8,KSYMB,'NH3     ')) EPSM=232.1
        IF(EQUSYM(8,KSYMB,'NH3     ')) SIGM=3.202
        IF(EQUSYM(8,KSYMB,'AR      ')) EPSM=134.1
        IF(EQUSYM(8,KSYMB,'AR      ')) SIGM=3.363
        IF(EQUSYM(8,KSYMB,'HE      ')) EPSM= 82.4
        IF(EQUSYM(8,KSYMB,'HE      ')) SIGM=2.852
      ENDIF
      IF(EQUSYM(8,JSYMB,'N2      ')) THEN
        IF(EQUSYM(8,KSYMB,'H2      ')) EPSM=101.8
        IF(EQUSYM(8,KSYMB,'H2      ')) SIGM=3.098
        IF(EQUSYM(8,KSYMB,'O2      ')) EPSM=110.2
        IF(EQUSYM(8,KSYMB,'O2      ')) SIGM=3.516
        IF(EQUSYM(8,KSYMB,'N2      ')) EPSM=104.2
        IF(EQUSYM(8,KSYMB,'N2      ')) SIGM=3.632
        IF(EQUSYM(8,KSYMB,'CO      ')) EPSM=104.2
        IF(EQUSYM(8,KSYMB,'CO      ')) SIGM=3.632
        IF(EQUSYM(8,KSYMB,'CO2     ')) EPSM=151.2
        IF(EQUSYM(8,KSYMB,'CO2     ')) SIGM=3.705
        IF(EQUSYM(8,KSYMB,'NH3     ')) EPSM=210.9
        IF(EQUSYM(8,KSYMB,'NH3     ')) SIGM=3.327
        IF(EQUSYM(8,KSYMB,'AR      ')) EPSM=120.1
        IF(EQUSYM(8,KSYMB,'AR      ')) SIGM=3.480
        IF(EQUSYM(8,KSYMB,'HE      ')) EPSM= 59.3
        IF(EQUSYM(8,KSYMB,'HE      ')) SIGM=3.029
      ENDIF
      IF(EQUSYM(8,JSYMB,'H2O     ')) THEN
        IF(EQUSYM(8,KSYMB,'H2      ')) EPSM=241.0
        IF(EQUSYM(8,KSYMB,'H2      ')) SIGM=2.592
        IF(EQUSYM(8,KSYMB,'O2      ')) EPSM=271.5
        IF(EQUSYM(8,KSYMB,'O2      ')) SIGM=3.133
      ENDIF
      IF(EQUSYM(8,JSYMB,'CO      ')) THEN
        IF(EQUSYM(8,KSYMB,'H2      ')) EPSM=101.8
        IF(EQUSYM(8,KSYMB,'H2      ')) SIGM=3.129
        IF(EQUSYM(8,KSYMB,'O2      ')) EPSM=114.7
        IF(EQUSYM(8,KSYMB,'O2      ')) SIGM=3.506
        IF(EQUSYM(8,KSYMB,'N2      ')) EPSM=104.2
        IF(EQUSYM(8,KSYMB,'N2      ')) SIGM=3.632
        IF(EQUSYM(8,KSYMB,'CO      ')) EPSM=104.2
        IF(EQUSYM(8,KSYMB,'CO      ')) SIGM=3.631
        IF(EQUSYM(8,KSYMB,'CO2     ')) EPSM=159.2
        IF(EQUSYM(8,KSYMB,'CO2     ')) SIGM=3.705
        IF(EQUSYM(8,KSYMB,'NH3     ')) EPSM=210.9
        IF(EQUSYM(8,KSYMB,'NH3     ')) SIGM=3.326
        IF(EQUSYM(8,KSYMB,'AR      ')) EPSM=115.0
        IF(EQUSYM(8,KSYMB,'AR      ')) SIGM=3.500
        IF(EQUSYM(8,KSYMB,'HE      ')) EPSM= 41.1
        IF(EQUSYM(8,KSYMB,'HE      ')) SIGM=3.144
      ENDIF
      IF(EQUSYM(8,JSYMB,'CO2     ')) THEN
        IF(EQUSYM(8,KSYMB,'H2      ')) EPSM=156.2
        IF(EQUSYM(8,KSYMB,'H2      ')) SIGM=3.036
        IF(EQUSYM(8,KSYMB,'O2      ')) EPSM=155.7
        IF(EQUSYM(8,KSYMB,'O2      ')) SIGM=3.599
        IF(EQUSYM(8,KSYMB,'N2      ')) EPSM=151.2
        IF(EQUSYM(8,KSYMB,'N2      ')) SIGM=3.705
        IF(EQUSYM(8,KSYMB,'CO      ')) EPSM=159.2
        IF(EQUSYM(8,KSYMB,'CO      ')) SIGM=3.705
        IF(EQUSYM(8,KSYMB,'CO2     ')) EPSM=245.3
        IF(EQUSYM(8,KSYMB,'CO2     ')) SIGM=3.769
        IF(EQUSYM(8,KSYMB,'NH3     ')) EPSM=323.5
        IF(EQUSYM(8,KSYMB,'NH3     ')) SIGM=3.396
        IF(EQUSYM(8,KSYMB,'AR      ')) EPSM=156.6
        IF(EQUSYM(8,KSYMB,'AR      ')) SIGM=3.604
        IF(EQUSYM(8,KSYMB,'HE      ')) EPSM= 46.4
        IF(EQUSYM(8,KSYMB,'HE      ')) SIGM=3.286
      ENDIF
      IF(EQUSYM(8,JSYMB,'NH3     ')) THEN
        IF(EQUSYM(8,KSYMB,'H2      ')) EPSM=206.0
        IF(EQUSYM(8,KSYMB,'H2      ')) SIGM=2.785
        IF(EQUSYM(8,KSYMB,'O2      ')) EPSM=232.1
        IF(EQUSYM(8,KSYMB,'O2      ')) SIGM=3.202
        IF(EQUSYM(8,KSYMB,'N2      ')) EPSM=210.9
        IF(EQUSYM(8,KSYMB,'N2      ')) SIGM=3.327
        IF(EQUSYM(8,KSYMB,'CO      ')) EPSM=210.9
        IF(EQUSYM(8,KSYMB,'CO      ')) SIGM=3.326
        IF(EQUSYM(8,KSYMB,'CO2     ')) EPSM=323.5
        IF(EQUSYM(8,KSYMB,'CO2     ')) SIGM=3.396
        IF(EQUSYM(8,KSYMB,'NH3     ')) EPSM=426.6
        IF(EQUSYM(8,KSYMB,'NH3     ')) SIGM=3.022
        IF(EQUSYM(8,KSYMB,'AR      ')) EPSM=245.8
        IF(EQUSYM(8,KSYMB,'AR      ')) SIGM=3.186
        IF(EQUSYM(8,KSYMB,'HE      ')) EPSM= 66.6
        IF(EQUSYM(8,KSYMB,'HE      ')) SIGM=2.812
      ENDIF
      IF(EQUSYM(8,JSYMB,'AR      ')) THEN
        IF(EQUSYM(8,KSYMB,'H2      ')) EPSM=118.7
        IF(EQUSYM(8,KSYMB,'H2      ')) SIGM=2.854
        IF(EQUSYM(8,KSYMB,'O2      ')) EPSM=134.1
        IF(EQUSYM(8,KSYMB,'O2      ')) SIGM=3.363
        IF(EQUSYM(8,KSYMB,'N2      ')) EPSM=120.1
        IF(EQUSYM(8,KSYMB,'N2      ')) SIGM=3.480
        IF(EQUSYM(8,KSYMB,'CO      ')) EPSM=115.0
        IF(EQUSYM(8,KSYMB,'CO      ')) SIGM=3.500
        IF(EQUSYM(8,KSYMB,'CO2     ')) EPSM=156.6
        IF(EQUSYM(8,KSYMB,'CO2     ')) SIGM=3.604
        IF(EQUSYM(8,KSYMB,'NH3     ')) EPSM=245.8
        IF(EQUSYM(8,KSYMB,'NH3     ')) SIGM=3.186
        IF(EQUSYM(8,KSYMB,'AR      ')) EPSM=141.6
        IF(EQUSYM(8,KSYMB,'AR      ')) SIGM=3.350
        IF(EQUSYM(8,KSYMB,'HE      ')) EPSM= 50.9
        IF(EQUSYM(8,KSYMB,'HE      ')) SIGM=2.956
      ENDIF
      IF(EQUSYM(8,JSYMB,'HE      ')) THEN
        IF(EQUSYM(8,KSYMB,'H2      ')) EPSM= 32.2
        IF(EQUSYM(8,KSYMB,'H2      ')) SIGM=2.752
        IF(EQUSYM(8,KSYMB,'O2      ')) EPSM= 82.4
        IF(EQUSYM(8,KSYMB,'O2      ')) SIGM=2.852
        IF(EQUSYM(8,KSYMB,'N2      ')) EPSM= 59.3
        IF(EQUSYM(8,KSYMB,'N2      ')) SIGM=3.029
        IF(EQUSYM(8,KSYMB,'CO      ')) EPSM= 41.1
        IF(EQUSYM(8,KSYMB,'CO      ')) SIGM=3.144
        IF(EQUSYM(8,KSYMB,'CO2     ')) EPSM= 46.4
        IF(EQUSYM(8,KSYMB,'CO2     ')) SIGM=3.286
        IF(EQUSYM(8,KSYMB,'NH3     ')) EPSM= 66.6
        IF(EQUSYM(8,KSYMB,'NH3     ')) SIGM=2.812
        IF(EQUSYM(8,KSYMB,'AR      ')) EPSM= 50.9
        IF(EQUSYM(8,KSYMB,'AR      ')) SIGM=2.956
        IF(EQUSYM(8,KSYMB,'HE      ')) EPSM= 10.4
        IF(EQUSYM(8,KSYMB,'HE      ')) SIGM=2.602
      ENDIF
      ENDIF
C***********************************************************************
C     CALC. OF D: LOOP FOR T
C***********************************************************************
      DO 50 I=NTU,NTO
      T=FLOAT(I)*100.0
      TEMP(I-NTU+1) =T
      TR=T/EPSM
      HELP2=2.663E-2*DSQRT(HELP1*T*T*T)
      AG=DLOG(T/EPSM)
      OMEGD=DEXP(0.348-0.459*AG+0.095*AG*AG-0.01*AG*AG*AG)
      IF(NEWMOL) THEN
      DT(I-NTU+1)=HELP2/(MXIOME(1,TR,DST,NFILE6,STOP)*SIGM**2)
      ELSE
      DT(I-NTU+1)=HELP2/(OMEGD*SIGM**2)
      ENDIF
   50 CONTINUE
C***********************************************************************
C     POLYNOMIAL FIT FOR D
C***********************************************************************
      CALL LOGFIT(M,NDI,TEMP(1),DT(1),HELP,C,
     1          ERRMAX,IERMAX,NFILE6,A,X,Y,STOP,TEST)
C***********************************************************************
C     STORE DJK, ERROR EXIT
C***********************************************************************
      DO 30 I=1,NDI
   30 DJK(I,J,K)=C(I)
C     IF(TEST) WRITE(NFILE6,445)
C445  FORMAT(2X,'  T(K)    ',2X,' D(EXACT) ',2X,' D(FITTED)',
C    1       2X,' ERROR    ')
      IF(ERRMAX.GT.0.02) WRITE(NFILE6,429) ERRMAX,
     1      JSYMB(1:LNSY),KSYMB(1:LNSY),TEMP(IERMAX)
  429 FORMAT(3X,'WARNING: FIT OF DIFFUSION COEFFICIENT CAUSES',
     1          ' AN ERROR OF',/,3X,2PF5.1,'%  FOR SPECIES PAIR  ',
     1          A8,'-',A8,' AT ',0PF5.0,' K')
  428 FORMAT(1X,2X,'COEFF. OF BIN. D :',4(3X,1PE9.2))
      IF(TEST) WRITE(NFILE6,428) (DJK(I,J,K),I=1,NDI)
  100 CONTINUE
C     TEST=.FALSE.
      RETURN
C***********************************************************************
C     END OF -GENDIF-
C***********************************************************************
      END
      SUBROUTINE DIFFIT (NP,P,X,Y,N,IFLAG)
C***********************************************************************
C                                                                      *
C     ************************************************************     *
C     *                                                          *     *
C     *             FITTING FUNCTION USED IN NLSQN               *     *
C     *                                                          *     *
C     ************************************************************     *
C                                                                      *
C***********************************************************************
C                                                                      *
C     FUNCTION TO BE ALTERED BY THE SKILLFUL USER                      *
C                                                                      *
C     INPUT PARAMETERS:                                                *
C                                                                      *
C     N      NUMBER OF EXPERIMENTAL VALUES                             *
C     NP     NUMBER OF FIT PARAMETERS                                  *
C     X(N)   INPUT FOR FITTING FUNCTION                                *
C     P(NP)  FITTING PARAMETERS                                        *
C                                                                      *
C     OUTPUT PARAMETERS:                                               *
C                                                                      *
C     Y(N)   OUTPUT FOR FITTING FUNCTION                               *
C     IFLAG   0 FOR REGULAR PERFORMANCE                                *
C     IFLAG  -1 IF AN ERROR WAS DETECTED                               *
C                                                                      *
C     SET NUMBP TO NUMBER OF PARAMETERS USED IN FUNCTION               *
C     CHECK IS DONE IF NUMBER OF PARAMETRES TO BE ESTIMATED EQUALS     *
C     TO NUMBER OF PARAMETERS IN MODEL FUNCTION                        *
C                                                                      *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION Y(N),X(N),P(NP)
      DATA NUMBP/4/
      IFLAG=0
      IF(NP.NE.NUMBP) IFLAG=-1
C
C
      DO 10 I=1,N
      Y(I)=P(1)
      DO 5 K=2,NP
   5  Y(I)=P(K) * X(I)**(K-1) + Y(I)
10    CONTINUE
C
      RETURN
      END
      SUBROUTINE LOGFIT(M,NDI,TEMP,VALUE,HELP,C,
     1          ERRMAX,IERMAX,NFILE6,
     2          A,X,Y,STOP,TEST)
C***********************************************************************
C
C    ***************************************************************
C    *                                                             *
C    *  PROGRAM FOR THE CALC. OF LOGARITHMIC FITS                  *
C    *              LAST CHANGE: 15.05.1984/WARNATZ                *
C    *                                                             *
C    ***************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C     NDI          = NUMBER OF COEFFICIENTS
C     M            = NUMBER OF DATA TO BE FITTED
C     TEMP         = X-VALUES
C     VALUE        = Y-VALUES
C     NFILE6       = LOGICAL NUMBER OF OUTPUT FILE (LISTING)
C
C     OUTPUT :
C
C     COEFF(NDI)    = COEFFICIENTS
C     ERRMAX        = MAXIMUM ERROR OF FIT
C     IERMAX        = INDEX OF MAXIMUM ERROR
C     STOP         = .TRUE. FOR NORMAL END, =.FALSE. OTHERWISE
C
C
C
C
C***********************************************************************
C
C
C
C***********************************************************************
C     STORAGE ORGANIZATION, INITIAL VALUES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL STOP,TEST
      DIMENSION  TEMP(M),VALUE(M),HELP(M),C(NDI),X(M),Y(M),A(M,NDI)
      DATA EPSIL/1.0D-5/
C***********************************************************************
C     POLYNOMIAL FIT FOR LAMBDA
C***********************************************************************
      DO 10 I=1,M
      X(I)=DLOG(TEMP(I))
      Y(I)=DLOG(VALUE(I))
   10 CONTINUE
      DO 30  I=1,M
      DO 20  J=1,NDI
   20 A(I,J)=X(I)**(J-1)
   30 CONTINUE
      CALL INIAGL(A,Y,M,NDI,EPSIL,C,STOP,NFILE6)
   12 FORMAT(1X,5X,'STATIONARITY IN -GENLAM-(AGL)')
      IF(.NOT.STOP)  WRITE(NFILE6,12)
C**********************************************************************
C     TEST FOR ACCURACY OF FIT
C**********************************************************************
      DO 220 I=1,M
      HELP(I)=0.0D0
      DO 210 N=1,NDI
  210 HELP(I) = HELP(I)+C(N)*A(I,N)
  220 HELP(I) = DEXP(HELP(I))
      ERRMAX=0.0
      IERMAX=1
      IF(TEST) WRITE(NFILE6,445)
 445  FORMAT(2X,'  T(K)    ',2X,' V(EXACT) ',2X,' V(FITTED)',
     1       2X,' ERROR    ')
      DO 610 I=1,M
      ERR=DABS((-HELP(I)+VALUE(I))/VALUE(I))
      IF(TEST) WRITE(NFILE6,444) TEMP(I),
     1       VALUE(I),HELP(I),ERR
444   FORMAT(6(2X,1PE10.3))
      IF(ERR.LT.ERRMAX) GOTO 610
      ERRMAX=ERR
      IERMAX=I
  610 CONTINUE
      RETURN
C***********************************************************************
C     END OF -GENLAM-
C***********************************************************************
      END
      SUBROUTINE PHI
      STOP
      END
      DOUBLE PRECISION FUNCTION MXIOME(N,TR,DR,NFILE6,STOP)
C***********************************************************************
C
C     **************************************************************
C     *                                                            *
C     *     COLLISION INTEGRALS BY INTERPOLATION FROM A TABLE      *
C     *                   AUTHOR: J.WARNATZ                        *
C     *                      08.03.1983                            *
C     *             LAST CHANGE: 16.07.1986/WARNATZ                *
C     *                                                            *
C     **************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C     N              = INDEX OF COLLISION INTEGRAL
C     TR             = REDUCED TEMPERATURE, -
C     DR             = REDUCED DIPOLE MOMENT, -
C     NFILE6         = FILE FOR OUTPUT OF ERROR MESSAGES
C     STOP           = .TRUE. FOR NORMAL END, =.FALSE. OTHERWISE
C
C     OUTPUT :
C
C     MXIOME         = REDUCED COLLISION INTEGRAL
C
C***********************************************************************
C
C
C
C***********************************************************************
C     STORAGE ORGANIZATION,  INITIAL VALUES
C***********************************************************************
C---- VARIABLE TYPES
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL STOP
      REAL TSTERN,DELTA,O,P
C---- ARRAYS
      DIMENSION VERT(3),ARG(3),VAL(3),TSTERN(37),DELTA(8),O(37,8),
     F          P(37,8)
C---- INITIALIZATION
      DATA TSTERN/.1,.2,.3,.4,.5,.6,.7,.8,.9,1.,1.2,1.4,1.6,1.8,2.,2.5,
     1            3.,3.5,4.,5.,6.,7.,8.,9.,10.,12.,14.,16.,18.,20.,25.,
     2            30.,35.,40.,50.,75.,100./
      DATA DELTA/0.,.25,.5,.75,1.,1.5,2.,2.5/
C***********************************************************************
C     TABLE OF OMEGA(1,1)
C***********************************************************************
      DATA O(1,1),O(1,2),O(1,3),O(1,4),O(1,5),O(1,6),O(1,7),O(1,8)
     F     /4.008,4.002,4.655,5.52,6.454,8.214,9.824,11.31/
      DATA O(2,1),O(2,2),O(2,3),O(2,4),O(2,5),O(2,6),O(2,7),O(2,8)
     F     /3.130,3.164,3.355,3.721,4.198,5.23,6.225,7.160/
      DATA O(3,1),O(3,2),O(3,3),O(3,4),O(3,5),O(3,6),O(3,7),O(3,8)
     F     /2.649,2.657,2.77,3.002,3.319,4.054,4.785,5.483/
      DATA O(4,1),O(4,2),O(4,3),O(4,4),O(4,5),O(4,6),O(4,7),O(4,8)
     F     /2.314,2.32,2.402,2.572,2.812,3.386,3.972,4.539/
      DATA O(5,1),O(5,2),O(5,3),O(5,4),O(5,5),O(5,6),O(5,7),O(5,8)
     F     /2.066,2.073,2.14,2.278,2.472,2.946,3.437,3.918/
      DATA O(6,1),O(6,2),O(6,3),O(6,4),O(6,5),O(6,6),O(6,7),O(6,8)
     F     /1.877,1.885,1.944,2.06,2.225,2.628,3.054,3.747/
      DATA O(7,1),O(7,2),O(7,3),O(7,4),O(7,5),O(7,6),O(7,7),O(7,8)
     F     /1.729,1.738,1.79,1.893,2.036,2.388,2.763,3.137/
      DATA O(8,1),O(8,2),O(8,3),O(8,4),O(8,5),O(8,6),O(8,7),O(8,8)
     F     /1.6122,1.622,1.67,1.76,1.886,2.198,2.535,2.872/
      DATA O(9,1),O(9,2),O(9,3),O(9,4),O(9,5),O(9,6),O(9,7),O(9,8)
     F     /1.517,1.527,1.572,1.653,1.765,2.044,2.35,2.657/
      DATA O(10,1),O(10,2),O(10,3),O(10,4),O(10,5),O(10,6),O(10,7),O(10,
     F     8)/1.44,1.45,1.49,1.564,1.665,1.917,2.196,2.4780/
      DATA O(11,1),O(11,2),O(11,3),O(11,4),O(11,5),O(11,6),O(11,7),O(11,
     F     8)/1.3204,1.33,1.364,1.425,1.51,1.72,1.956,2.199/
      DATA O(12,1),O(12,2),O(12,3),O(12,4),O(12,5),O(12,6),O(12,7),O(12,
     F     8)/1.234,1.24,1.272,1.324,1.394,1.573,1.777,1.99/
      DATA O(13,1),O(13,2),O(13,3),O(13,4),O(13,5),O(13,6),O(13,7),O(13,
     F     8)/1.168,1.176,1.202,1.246,1.306,1.46,1.64,1.827/
      DATA O(14,1),O(14,2),O(14,3),O(14,4),O(14,5),O(14,6),O(14,7),O(14,
     F     8)/1.1166,1.124,1.146,1.185,1.237,1.372,1.53,1.7/
      DATA O(15,1),O(15,2),O(15,3),O(15,4),O(15,5),O(15,6),O(15,7),O(15,
     F     8)/1.075,1.082,1.102,1.135,1.181,1.3,1.441,1.592/
      DATA O(16,1),O(16,2),O(16,3),O(16,4),O(16,5),O(16,6),O(16,7),O(16,
     F     8)/1.0006,1.005,1.02,1.046,1.08,1.17,1.278,1.397/
      DATA O(17,1),O(17,2),O(17,3),O(17,4),O(17,5),O(17,6),O(17,7),O(17,
     F     8)/.95,.9538,.9656,.9852,1.012,1.082,1.168,1.265/
      DATA O(18,1),O(18,2),O(18,3),O(18,4),O(18,5),O(18,6),O(18,7),O(18,
     F     8)/.9131,.9162,.9256,.9413,.9626,1.019,1.09,1.17/
      DATA O(19,1),O(19,2),O(19,3),O(19,4),O(19,5),O(19,6),O(19,7),O(19,
     F     8)/.8845,.8871,.8948,.9076,.9252,.972,1.03,1.098/
      DATA O(20,1),O(20,2),O(20,3),O(20,4),O(20,5),O(20,6),O(20,7),O(20,
     F     8)/.8428,.8446,.850,.859,.8716,.9053,.9483,.9984/
      DATA O(21,1),O(21,2),O(21,3),O(21,4),O(21,5),O(21,6),O(21,7),O(21,
     F     8)/.813,.8142,.8183,.825,.8344,.8598,.8927,.9316/
      DATA O(22,1),O(22,2),O(22,3),O(22,4),O(22,5),O(22,6),O(22,7),O(22,
     F     8)/.7898,.791,.794,.7993,.8066,.8265,.8526,.8836/
      DATA O(23,1),O(23,2),O(23,3),O(23,4),O(23,5),O(23,6),O(23,7),O(23,
     F     8)/.7711,.772,.7745,.7788,.7846,.8007,.822,.8474/
      DATA O(24,1),O(24,2),O(24,3),O(24,4),O(24,5),O(24,6),O(24,7),O(24,
     F     8)/.7555,.7562,.7584,.7619,.7667,.78,.7976,.8189/
      DATA O(25,1),O(25,2),O(25,3),O(25,4),O(25,5),O(25,6),O(25,7),O(25,
     F     8)/.7422,.743,.7446,.7475,.7515,.7627,.7776,.796/
      DATA O(26,1),O(26,2),O(26,3),O(26,4),O(26,5),O(26,6),O(26,7),O(26,
     F     8)/.72022,.7206,.722,.7241,.7271,.7354,.7464,.76/
      DATA O(27,1),O(27,2),O(27,3),O(27,4),O(27,5),O(27,6),O(27,7),O(27,
     F     8)/.7025,.703,.704,.7055,.7078,.7142,.7228,.7334/
      DATA O(28,1),O(28,2),O(28,3),O(28,4),O(28,5),O(28,6),O(28,7),O(28,
     F     8)/.68776,.688,.6888,.6901,.6919,.697,.704,.7125/
      DATA O(29,1),O(29,2),O(29,3),O(29,4),O(29,5),O(29,6),O(29,7),O(29,
     F     8)/.6751,.6753,.676,.677,.6785,.6827,.6884,.6955/
      DATA O(30,1),O(30,2),O(30,3),O(30,4),O(30,5),O(30,6),O(30,7),O(30,
     F     8)/.664,.6642,.6648,.6657,.6669,.6704,.6752,.681/
      DATA O(31,1),O(31,2),O(31,3),O(31,4),O(31,5),O(31,6),O(31,7),O(31,
     F     8)/.6414,.6415,.6418,.6425,.6433,.6457,.649,.653/
      DATA O(32,1),O(32,2),O(32,3),O(32,4),O(32,5),O(32,6),O(32,7),O(32,
     F     8)/.6235,.6236,.6239,.6243,.6249,.6267,.629,.632/
      DATA O(33,1),O(33,2),O(33,3),O(33,4),O(33,5),O(33,6),O(33,7),O(33,
     F     8)/.60882,.6089,.6091,.6094,.61,.6112,.613,.6154/
      DATA O(34,1),O(34,2),O(34,3),O(34,4),O(34,5),O(34,6),O(34,7),O(34,
     F     8)/.5964,.5964,.5966,.597,.5972,.5983,.600,.6017/
      DATA O(35,1),O(35,2),O(35,3),O(35,4),O(35,5),O(35,6),O(35,7),O(35,
     F     8)/.5763,.5763,.5764,.5766,.5768,.5775,.5785,.58/
      DATA O(36,1),O(36,2),O(36,3),O(36,4),O(36,5),O(36,6),O(36,7),O(36,
     F     8)/.5415,.5415,.5416,.5416,.5418,.542,.5424,.543/
      DATA O(37,1),O(37,2),O(37,3),O(37,4),O(37,5),O(37,6),O(37,7),O(37,
     F     8)/.518,.518,.5182,.5184,.5184,.5185,.5186,.5187/
C***********************************************************************
C     TABLE OF OMEGA(2,2)
C***********************************************************************
      DATA P(1,1),P(1,2),P(1,3),P(1,4),P(1,5),P(1,6),P(1,7),P(1,8)
     F     /4.1,4.266,4.833,5.742,6.729,8.624,10.34,11.890/
      DATA P(2,1),P(2,2),P(2,3),P(2,4),P(2,5),P(2,6),P(2,7),P(2,8)
     F     /3.263,3.305,3.516,3.914,4.433,5.57,6.637,7.618/
      DATA P(3,1),P(3,2),P(3,3),P(3,4),P(3,5),P(3,6),P(3,7),P(3,8)
     F     /2.84,2.836,2.936,3.168,3.511,4.329,5.126,5.874/
      DATA P(4,1),P(4,2),P(4,3),P(4,4),P(4,5),P(4,6),P(4,7),P(4,8)
     F     /2.531,2.522,2.586,2.749,3.004,3.64,4.282,4.895/
      DATA P(5,1),P(5,2),P(5,3),P(5,4),P(5,5),P(5,6),P(5,7),P(5,8)
     F     /2.284,2.277,2.329,2.46,2.665,3.187,3.727,4.249/
      DATA P(6,1),P(6,2),P(6,3),P(6,4),P(6,5),P(6,6),P(6,7),P(6,8)
     F     /2.084,2.081,2.13,2.243,2.417,2.862,3.329,3.786/
      DATA P(7,1),P(7,2),P(7,3),P(7,4),P(7,5),P(7,6),P(7,7),P(7,8)
     F     /1.922,1.924,1.97,2.072,2.225,2.641,3.028,3.435/
      DATA P(8,1),P(8,2),P(8,3),P(8,4),P(8,5),P(8,6),P(8,7),P(8,8)
     F     /1.7902,1.795,1.84,1.934,2.07,2.417,2.788,3.156/
      DATA P(9,1),P(9,2),P(9,3),P(9,4),P(9,5),P(9,6),P(9,7),P(9,8)
     F     /1.682,1.689,1.733,1.82,1.944,2.258,2.596,2.933/
      DATA P(10,1),P(10,2),P(10,3),P(10,4),P(10,5),P(10,6),P(10,7),P(10,
     F     8)/1.593,1.60,1.644,1.725,1.84,2.124,2.435,2.746/
      DATA P(11,1),P(11,2),P(11,3),P(11,4),P(11,5),P(11,6),P(11,7),P(11,
     F     8)/1.455,1.465,1.504,1.574,1.67,1.913,2.181,2.45/
      DATA P(12,1),P(12,2),P(12,3),P(12,4),P(12,5),P(12,6),P(12,7),P(12,
     F     8)/1.355,1.365,1.4,1.461,1.544,1.754,1.989,2.228/
      DATA P(13,1),P(13,2),P(13,3),P(13,4),P(13,5),P(13,6),P(13,7),P(13,
     F     8)/1.28,1.289,1.321,1.374,1.447,1.63,1.838,2.053/
      DATA P(14,1),P(14,2),P(14,3),P(14,4),P(14,5),P(14,6),P(14,7),P(14,
     F     8)/1.222,1.231,1.26,1.306,1.37,1.532,1.718,1.912/
      DATA P(15,1),P(15,2),P(15,3),P(15,4),P(15,5),P(15,6),P(15,7),P(15,
     F     8)/1.176,1.184,1.209,1.25,1.307,1.45,1.618,1.795/
      DATA P(16,1),P(16,2),P(16,3),P(16,4),P(16,5),P(16,6),P(16,7),P(16,
     F     8)/1.0933,1.1,1.119,1.15,1.193,1.304,1.435,1.578/
      DATA P(17,1),P(17,2),P(17,3),P(17,4),P(17,5),P(17,6),P(17,7),P(17,
     F     8)/1.039,1.044,1.06,1.083,1.117,1.204,1.31,1.428/
      DATA P(18,1),P(18,2),P(18,3),P(18,4),P(18,5),P(18,6),P(18,7),P(18,
     F     8)/.9996,1.004,1.016,1.035,1.062,1.133,1.22,1.32/
      DATA P(19,1),P(19,2),P(19,3),P(19,4),P(19,5),P(19,6),P(19,7),P(19,
     F     8)/.9699,.9732,.983,.9991,1.021,1.08,1.153,1.236/
      DATA P(20,1),P(20,2),P(20,3),P(20,4),P(20,5),P(20,6),P(20,7),P(20,
     F     8)/.9268,.9291,.936,.9473,.9628,1.005,1.058,1.12/
      DATA P(21,1),P(21,2),P(21,3),P(21,4),P(21,5),P(21,6),P(21,7),P(21,
     F     8)/.8962,.8979,.903,.9114,.923,.9545,.9955,1.044/
      DATA P(22,1),P(22,2),P(22,3),P(22,4),P(22,5),P(22,6),P(22,7),P(22,
     F     8)/.8727,.8741,.878,.8845,.8935,.918,.9505,.9893/
      DATA P(23,1),P(23,2),P(23,3),P(23,4),P(23,5),P(23,6),P(23,7),P(23,
     F     8)/.8538,.8549,.858,.8632,.8703,.890,.9164,.9482/
      DATA P(24,1),P(24,2),P(24,3),P(24,4),P(24,5),P(24,6),P(24,7),P(24,
     F     8)/.8379,.8388,.8414,.8456,.8515,.868,.8895,.916/
      DATA P(25,1),P(25,2),P(25,3),P(25,4),P(25,5),P(25,6),P(25,7),P(25,
     F     8)/.8243,.8251,.8273,.8308,.8356,.8493,.8676,.89/
      DATA P(26,1),P(26,2),P(26,3),P(26,4),P(26,5),P(26,6),P(26,7),P(26,
     F     8)/.8018,.8024,.8039,.8065,.810,.820,.8337,.8504/
      DATA P(27,1),P(27,2),P(27,3),P(27,4),P(27,5),P(27,6),P(27,7),P(27,
     F     8)/.7836,.784,.7852,.7872,.7899,.7976,.808,.8212/
      DATA P(28,1),P(28,2),P(28,3),P(28,4),P(28,5),P(28,6),P(28,7),P(28,
     F     8)/.7683,.7687,.7696,.771,.7733,.7794,.788,.7983/
      DATA P(29,1),P(29,2),P(29,3),P(29,4),P(29,5),P(29,6),P(29,7),P(29,
     F     8)/.7552,.7554,.7562,.7575,.7592,.764,.771,.7797/
      DATA P(30,1),P(30,2),P(30,3),P(30,4),P(30,5),P(30,6),P(30,7),P(30,
     F     8)/.7436,.7438,.7445,.7455,.747,.7512,.757,.7642/
      DATA P(31,1),P(31,2),P(31,3),P(31,4),P(31,5),P(31,6),P(31,7),P(31,
     F     8)/.71982,.72,.7204,.7211,.7221,.725,.7289,.7339/
      DATA P(32,1),P(32,2),P(32,3),P(32,4),P(32,5),P(32,6),P(32,7),P(32,
     F     8)/.701,.7011,.7014,.702,.7026,.7047,.7076,.7112/
      DATA P(33,1),P(33,2),P(33,3),P(33,4),P(33,5),P(33,6),P(33,7),P(33,
     F     8)/.68545,.6855,.686,.686,.6867,.6883,.6905,.693/
      DATA P(34,1),P(34,2),P(34,3),P(34,4),P(34,5),P(34,6),P(34,7),P(34,
     F     8)/.6723,.6724,.6726,.673,.6733,.6745,.676,.6784/
      DATA P(35,1),P(35,2),P(35,3),P(35,4),P(35,5),P(35,6),P(35,7),P(35,
     F     8)/.651,.651,.6512,.6513,.6516,.6524,.6534,.6546/
      DATA P(36,1),P(36,2),P(36,3),P(36,4),P(36,5),P(36,6),P(36,7),P(36,
     F     8)/.614,.614,.6143,.6145,.6147,.6148,.6148,.6147/
      DATA P(37,1),P(37,2),P(37,3),P(37,4),P(37,5),P(37,6),P(37,7),P(37,
     F     8)/.5887,.5889,.5894,.59,.5903,.5901,.5895,.5885/
      STOP=.TRUE.
C***********************************************************************
C     ERROR EXIT FOR PROHIBITED VALUES OF TR AND DR
C***********************************************************************
    1 FORMAT(/,5X,'ERROR: Collision integral undefined in -MXIOME-')
      ILL=0
C---- EXIT FOR DR<0
      IF((DR.LT.-.00001D0).OR.(DR.GT.2.5D0))    ILL=1
C---- EXIT FOR UNPHYSICAL VALUES OF TR
      IF((TR.LT..09999D0).OR.(TR.GT.500.D0))    ILL=1
C---- EXIT FOR TR/DR COMBINATIONS OUTSIDE OF TABLE
      IF((DABS(DR).GT.1.0D-5).AND.(TR.GT.100.0D0)) ILL=1
      IF(ILL.NE.0)  WRITE(NFILE6,1)
      IF(ILL.NE.0)  STOP=.FALSE.
      IF(ILL.NE.0)  RETURN
C***********************************************************************
C     CALC. OF COLLISION INTEGRALS FOR A KRIEGER-12-6-3-POTENTIAL
C***********************************************************************
      IF(TR.GT.100.0D0)  GO TO 90
      KK=2
      IF(DABS(DR).LT.1.0D-5)  GO TO 20
C---- DETERMINE COLUMNS KK-1, KK, AND KK+1 IN TABLE NEAR DR
      KK=-1
      DO 10 K=2,7
      IF((DELTA(K-1).LT.DR).AND.(DELTA(K+1).GE.DR))  KK=K
   10 CONTINUE
C---- DETERMINE ROWS II-1, II, II+1 IN TABLE NEAR TR
   20 CONTINUE
      II=-1
      DO 30 I=2,36
      IF((TSTERN(I-1).LT.TR).AND.(TSTERN(I+1).GE.TR))  II=I
   30 CONTINUE
C---- CHECK TABLE
   11 FORMAT(/,5X,'ERROR: Table of collision integrals in -MXIOME- ',
     F       'incorrect')
      IF((II.LT.0D0).OR.(KK.LT.0D0)) WRITE(NFILE6,11)
      IF((II.LT.0D0).OR.(KK.LT.0D0)) STOP=.FALSE.
      IF((II.LT.0D0).OR.(KK.LT.0D0)) RETURN
C---- DOUBLE INTERPOLATION FOR DR>0 ON KK-1,KK,KK+1 AND II-1,II,II+1
      IF(DABS(DR).LT.1.0D-5)  GO TO 70
      DO 50 I=1,3
      DO 40 K=1,3
      ARG(K)=DELTA(KK-2+K)
      IF(N.EQ.1) VAL(K)=O(II-2+I,KK-2+K)
      IF(N.EQ.2) VAL(K)=P(II-2+I,KK-2+K)
   40 CONTINUE
      CALL MXIINT(DR,ARG,VAL,VERT(I))
   50 CONTINUE
      DO 60 I=1,3
   60 ARG(I)=TSTERN(II-2+I)
      CALL MXIINT(TR,ARG,VERT,MXIOME)
      RETURN
C---- SINGLE INTERPOLATION IN COLUMN 1 FOR DR=0
   70 CONTINUE
      DO 80 I=1,3
      ARG(I)=TSTERN(II-2+I)
      IF(N.EQ.1) VAL(I)=O(II-2+I,1)
      IF(N.EQ.2) VAL(I)=P(II-2+I,1)
   80 CONTINUE
      CALL MXIINT(TR,ARG,VAL,MXIOME)
      RETURN
C---- DETERMINATION FOR TR>100
   90 IF(N.EQ.1) MXIOME=.623D0-.136D-2*TR
     F                 +.346D-5*TR*TR-.343D-8*TR*TR*TR
      IF(N.EQ.2) MXIOME=.703D0-.146D-2*TR
     F                 +.357D-5*TR*TR-.343D-8*TR*TR*TR
      RETURN
C***********************************************************************
C     END OF -MXIOME-
C***********************************************************************
      END
      SUBROUTINE MXIPAR(T,EPS,P)
C***********************************************************************
C
C     **************************************************************
C     *                                                            *
C     *     PROGR. FOR THE CALC. OF THE TEMPERATURE DEP. OF ZROT   *
C     *                   AUTHOR: J.WARNATZ                        *
C     *                      08.03.1983                            *
C     *             LAST CHANGE: 16.07.1986/WARNATZ                *
C     *                                                            *
C     **************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C     T              = TEMPERATURE (K)
C     EPS            = DEPTH OF WELL OF LJ POTENTIAL (K)
C
C     OUTPUT :
C
C     P              = ZROT/ZROT(298K) AT T
C
C***********************************************************************
C
C
C
C***********************************************************************
C     STORAGE ORGANIZATION,  INITIAL VALUES
C***********************************************************************
C---- VARIABLE TYPES
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C---- CALCULATE ZROT/ZROT(298K)
      N=0
      P=0.0
      D=EPS/T
    1 DMIN=DSQRT(D)
      DMAX=D*DMIN
      PSTO=P
      P=1.0+2.7842*DMIN+4.4674*D+5.5683*DMAX
      N=N+1
      IF(N.GE.2)  GO TO 2
      D=EPS/298.0
      GO TO 1
    2 P=P/PSTO
      RETURN
C***********************************************************************
C     END OF -MXIPAR-
C***********************************************************************
      END
      SUBROUTINE MXIRPR(RPREC)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C***********************************************************************
C     DETERMINE RELATIVE MACHINE PRECISION
C***********************************************************************
      RPREC=1.0
    1 RPREC=RPREC*0.5
      COMP=1.0+RPREC
      IF(COMP.NE.1.0) GO TO 1
      RPREC=RPREC*2.0
      RETURN
C***********************************************************************
C     END OF -MXIRPR-
C***********************************************************************
      END
      SUBROUTINE MXIINT(ARG,X,Y,VAL)
C***********************************************************************
C
C     **************************************************************
C     *                                                            *
C     *           PROGRAM FOR QUADRATIC INTERPOLATION              *
C     *                   AUTHOR: J.WARNATZ                        *
C     *                      08.03.1983                            *
C     *             LAST CHANGE: 16.07.1986/WARNATZ                *
C     *                                                            *
C     **************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C     X(3)           = X COORDINATES OF POINTS GIVEN
C     Y(3)           = Y COORDINATES OF POINTS GIVEN
C     ARG            = ARGUMENT TO CLCULATE FUNCTION VALUE FOR
C
C     OUTPUT :
C
C     VAL            = FUNCTION VALUE FOR X=ARG
C
C***********************************************************************
C
C
C
C***********************************************************************
C     STORAGE ORGANIZATION,  INITIAL VALUES
C***********************************************************************
C---- VARIABLE TYPES
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C---- ARRAYS
      DIMENSION X(3),Y(3)
C---- EVALUATION OF VAL
      VAL1=Y(1)+(ARG-X(1))*(Y(2)-Y(1))/(X(2)-X(1))
      VAL2=Y(2)+(ARG-X(2))*(Y(3)-Y(2))/(X(3)-X(2))
      FAC1=(ARG-X(1))/(X(2)-X(1))/2.0
      FAC2=(X(3)-ARG)/(X(3)-X(2))/2.0
      IF(ARG.LE.X(2))  VAL=(VAL1+VAL2*FAC1)/(1.0+FAC1)
      IF(ARG.GE.X(2))  VAL=(VAL1*FAC2+VAL2)/(1.0+FAC2)
      RETURN
C***********************************************************************
C     END OF -MXIINT-
C***********************************************************************
      END
      SUBROUTINE GETT(NSPEC,H,XI,HCPL,HCPH,HINF,XMOL,T,NFILE6)
C***********************************************************************
C
C    ***************************************************************
C    *                                                             *
C    *         PROGRAM FOR CALCULATION OF MOLAR ENTHALPIES         *
C    *                                                             *
C    *              LAST CHANGE: 09/22/1989 MAAS                   *
C    *                                                             *
C    ***************************************************************
C
C***********************************************************************
C
C     INPUT  :
C
C     I             = NUMBER OF SPECIES IN SPECIES LIST
C     T             = TEMPERATURE, K
C     HCPL(7,1)     = LOW  TEMPERATURE NASA COEFFICIENTS, J,K (7,NS)
C     HCPH(7,1)     = HIGH TEMPERATURE NASA COEFFICIENTS, J,K (7,NS)
C
C     OUTPUT :
C
C     T             = TEMPERATURE
C
C***********************************************************************
C
C
C
C***********************************************************************
C     STORAGE ORGANIZATION AND DEFINITION OF INITIAL VALUES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION HCPL(7,NSPEC),HCPH(7,NSPEC),HINF(3,NSPEC),XMOL(NSPEC)
      DIMENSION XI(NSPEC)
C***********************************************************************
C     CALCULATION OF XMOLM
C***********************************************************************
      XMOLM=0.D0
      DO 10 I=1,NSPEC
      XMOLM = XMOLM + XI(I) * XMOL(I)
   10 CONTINUE
C***********************************************************************
C     CALCULATION OF HI
C***********************************************************************
C---- INITIAL GUESS FOR T
      TOLD = 1200.D0
      ITER = 0
  100 CONTINUE
      IF(ITER.GT.60) GOTO 910
      HACT  = 0.D0
      CPACT = 0.D0
      DO 110 I=1,NSPEC
      CALL CALHI(I,TOLD,HCPL,HCPH,HINF,HI,NFILE6)
      CALL CALCPI(I,TOLD,HCPL,HCPH,HINF,CPI,NFILE6)
      HACT  = HACT  + XI(I)*HI
      CPACT = CPACT + XI(I)*CPI
  110 CONTINUE
      HACT  = HACT / XMOLM
      CPACT = CPACT/ XMOLM
C---- NEWTON STEP
      CORR  = (H - HACT) / CPACT
      TNEW = TOLD + CORR
      IF(DABS(CORR).LT.1.D-7*TNEW) GOTO 200
C     WRITE(6,*) TNEW,TOLD
      ITER=ITER+1
      TOLD = TNEW
      GOTO 100
  200 CONTINUE
      T = TNEW
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
  910 CONTINUE
      WRITE(NFILE6,911)
  911 FORMAT(' ','ERROR  : NO CONVERGENCE OF NEWTON ITERATION')
      STOP
C***********************************************************************
C     END OF -CALHI -
C***********************************************************************
      END
      SUBROUTINE GENSYM(HEAD,NFIINP,NFIOUT,NS,
     1     NSMAX,NSYMB,IFSP,IERR)
C***********************************************************************
C
C    ***************************************************************
C    *                                                             *
C    *    PROGRAM FOR THE INPUT AND PRINTOUT OF SPECIES SYMBOLS    *
C    *                                                             *
C    *              LAST CHANGE: 14.03.2017/Maas                   *
C    *                                                             *
C    ***************************************************************
C
C
C***********************************************************************
C
C     INPUT  :
C     HEAD          = Header string 
C     NFIINP        = NUMBER OF INPUT FILE
C     NFIOUT        = NUMBER OF OUTPUT FILE FOR LISTING
C
C     OUTPUT :
C
C     NS            = NUMBER OF SPECIES
C     NSYMB(NS)     = SPECIES SYMBOLS (DIMENSIONED WITH NS AT LEAST IN
C                     THE CALLING PROGRAM) CHARACTER*8
C     IFSP(NS)      = flag for species
C                     1 for symbol '*' behind species symbol
C                     2 for symbol '+' behind species symbol
C                     3 for symbol '>' behind species symbol
C                     0 otherwise
C     IERR          = error flag (ierr = 0 for normal end)
C
C     INPUT FROM FILE NFIINP : Species
C
C    RULES:
C         everything after ! is not considered
C         separators can be blanks,commas,*,+,>
C         more than one separator can follow another
C         the first separator different from a blank or a comma
C         after a species determines the flag 
C***********************************************************************
C
C
C
C***********************************************************************
C     STORAGE ORGANIZATION AND DEFINITION OF INITIAL VALUES            C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL FOUND,EQUSYM
      PARAMETER (LENDEL=3,LENSPE=50,LINLEN=255)
      CHARACTER(LEN=LENSPE) SYMSPE(LINLEN)
      CHARACTER(LEN=LENDEL) SYMDEL(LINLEN)
      CHARACTER(LEN=*) NSYMB(NSMAX)
      CHARACTER(LEN=*) HEAD
      CHARACTER(LEN=LINLEN) STRING
C---- there are at most LINLEN Species and delimiters
      CHARACTER*1 SFL(10)
      DIMENSION IFSP(NSMAX)
      DATA NFL/3/
      DATA SFL/'@','<','>',' ',',',' ',' ',' ',' ',' '/
C**********************************************************************C
C     Initialization                                                   C
C**********************************************************************C
      LNSY = LEN(NSYMB(1))
      IF(LENSPE.LT.LNSY) THEN
        WRITE(NFIOUT,*) 'Species length larger than', LENSPE
        STOP
      ENDIF
      LHEAD=LEN(HEAD)
      IERR  = 0
      NS    = 0
      FOUND = .FALSE.
      REWIND NFIINP
C**********************************************************************C
C     Search until header is found                                     C
C**********************************************************************C
  10  READ(NFIINP,'(A)',END=900) STRING
      IF(.NOT.EQUSYM(LHEAD,STRING,HEAD)) GOTO 10 
C**********************************************************************C
C     Input of the line and check for end                              C
C**********************************************************************C
  20  CONTINUE 
      READ(NFIINP,'(A)',END=900) STRING
      IF(EQUSYM(3,STRING,'END')) GOTO 50
C**********************************************************************C
C
C     process line after line     
C
C**********************************************************************C
C**********************************************************************C
C     read line
C**********************************************************************C
      CALL GETNAM(STRING,
     1   NSYSPE,LENSPE,SYMSPE,NSYDEL,LENDEL,SYMDEL,NFL,SFL,NFIOUT)
      IF(NSYSPE.EQ.0) THEN
         WRITE(NFIOUT,*) ' Warning: blank line encountered'
         GOTO 20
      ENDIF
C**********************************************************************C
C     process line after line     
C**********************************************************************C
      IFI = NS + 1
      NS  = NS + NSYSPE
      IF(NS.GT.NSMAX) GOTO 920
      NSYMB(IFI:NS)(1:LNSY) = SYMSPE(1:NSYSPE)(1:LNSY)
      IFSP(NS)  = 0
      DO J = 1, NSYDEL
      DO K = 1, NFL
        IF(SYMDEL(J)(1:1).EQ.SFL(K)) IFSP(NS-NSYSPE+J) = K
      ENDDO
      ENDDO
      GOTO 20
C**********************************************************************C
C     INPUT OF THE SPECIES SYMBOLS                                     C
C**********************************************************************C
   50 CONTINUE
C**********************************************************************C
C     PRINTOUT OF SPECIES SYMBOLS                                      C
C**********************************************************************C
      WRITE(NFIOUT,82) NS
      WRITE(NFIOUT,83) (NSYMB(I),IFSP(I),I=1,NS)
      RETURN
C**********************************************************************C
C     ERROR IN INPUT                                                   C
C**********************************************************************C
C---- no heading received
  900 CONTINUE
  901 FORMAT(5X,'Warning In species input: No heading, ',
     F       'no end card, or missing data')
      WRITE(NFIOUT,901)
      IERR = 1
      GOTO 999
C---- too many species
  920 CONTINUE
      WRITE(NFIOUT,921) NS,NSMAX
  921 FORMAT(' ',2X,'ERROR in GENSYM - Too many species:',
     1              ' NS(',I4,') > NSMAX(',I4,')')
      IERR = 1
      GOTO 999
  999 CONTINUE
      RETURN
C**********************************************************************C
C     FORMAT STATEMENT                                                 C
C**********************************************************************C
   82 FORMAT(8X,'List of Species and Flags, NS =',I5)
   83 FORMAT(8X,'Species : ',5(A,1X,I1,2X),
     1        /,1(18X,5(A,1X,I1,2X)))
C**********************************************************************C
C     END OF HOISYM                                                    C
C**********************************************************************C
      END
      SUBROUTINE GETNAM(STRING,
     1    NSYSPE,LENSPE,SYMSPE,NSYDEL,LENDEL,SYMDEL,NFL,SFL,NFIOUT)
C**********************************************************************C
C
C     Get names and delimiters from a string
C
C
C     Input:
C       STRING (character)    : string of arbitrary length
C       LENSPE (INTEGER)      : numbers of characters in one name
C       LENDEL (INTEGER)      : numbers of characters in one delimiter
C       NFL (integer)         : number of strings allowed as delimiters
C       SFL (character*1(NFL)): strings allowed as delimiters
C     Output:
C       NSYSPE                            : number of names found
C       SYMSPE (character*LENSPE(NSYSPE))): names
C       NSYDEL                            : number of delimiters found
C       SYMDEL (character*LENSPE(NSYDEL))): delimiters
C
C   Note: The string is assumed to start with a name, COMPDE should
C         be called before
C**********************************************************************C
      IMPLICIT NONE      
      INTEGER LENSPE,LENDEL,NSYSPE,NSYDEL,NFL,NFIOUT
      INTEGER ILAS ,I,NSYNEW
      LOGICAL EQUSYM
      CHARACTER(LEN=*) STRING     
      CHARACTER(LEN=LENSPE) SYMSPE(*)
      CHARACTER(LEN=LENDEL) SYMDEL(*)
      CHARACTER*1 SFL(NFL)
C**********************************************************************C
C     Compress line
C**********************************************************************C
      CALL COMPDE(STRING,ILAS,NFL,SFL)
C**********************************************************************C
C     get symbols and delimiters
C**********************************************************************C
C*******************************************************************************
      CALL SYDEL(STRING(1:ILAS),
     1    NSYSPE,LENSPE,SYMSPE,NSYDEL,LENDEL,SYMDEL,NFL,SFL,NFIOUT)
C**********************************************************************C
C     Check if END has been detected
C**********************************************************************C
      NSYNEW=0
      DO I=1,NSYSPE 
      IF(EQUSYM(3,'END',SYMSPE)) GOTO 20
      NSYNEW=NSYNEW+1
      ENDDO
   20 CONTINUE
      NSYSPE=NSYNEW
      RETURN
      END
      SUBROUTINE COMPDE(STRING,ILAS,NFL,SFL)
C*******************************************************************************
C
C     Remove unneccesary blanks,commas, and comments
C     
C
C     Input:
C       STRING (character)    : string of arbitrary length
C       NFL (integer)         : number of strings allowed as delimiters
C       SFL (character*1(NFL)): strings allowed as delimiters
C     Output:
C       ILAS                              : index of last entry   
C
C   
C  Example: |   abcb ** % oooo 1234 ! 5555
C           will return as
C           |abcb**%oooo 1234|
C*******************************************************************************
      IMPLICIT NONE
      CHARACTER(LEN=*) STRING
      CHARACTER*1  SFL(NFL)
      INTEGER LSTRI,ILNBE,LEXCM,IFNB
      INTEGER NFL,IFI,ILAS,IFEX
      INTEGER L    
      LOGICAL DELI,DELETE
C**********************************************************************C
C     Change commas to blanks
C**********************************************************************C
      DO L=1,LEN(STRING)
      IF(STRING(L:L).EQ.',') STRING(L:L)=' '
      ENDDO
C**********************************************************************C
C     Remove first blanks
C**********************************************************************C
      ILAS= LEN(STRING) 
      IFI =IFNB(STRING)
      ILAS = ILAS- IFI + 1 
      STRING(1:ILAS) = STRING(IFI:LEN(STRING))
      IFI  = 1
C**********************************************************************C
C     Throw away everything starting from first !
C**********************************************************************C
      IFEX=  LEXCM(STRING(1:ILAS))
      IF(IFEX.NE.0.AND.IFEX.LE.ILAS) STRING(IFEX:ILAS) =' '
      ILAS = IFEX - 1
C*******************************************************************************
C     check for lenght of non-blan entries
C*******************************************************************************
      ILNBE=LSTRI(STRING)
      ILAS = ILNBE
C**********************************************************************C
C     Remove blanks at the beginning 
C**********************************************************************C
      STRING= ADJUSTL(STRING)
C**********************************************************************C
C     Check if a delimiter appears at the beginning
C**********************************************************************C
      DELI=.FALSE.
      DO  L=1,NFL
      IF(STRING(IFI:IFI).EQ.SFL(L)) DELI= .TRUE.
C     IF(STRING(IFI:IFI).EQ.' ') DELI= .TRUE.
      ENDDO
      IF(DELI) THEN
        write(*,*) L,SFL(L),STRING
        GOTO 910 
      ENDIF
C---- if we are here first entry is no blank and no delimiter
C**********************************************************************C
C     Remove additional blanks
C**********************************************************************C
      DO WHILE (IFI.LT.ILAS) 
      IFI = IFI+1
      IF(STRING(IFI:IFI).EQ.' ')   THEN
C---- Check whether there is a neighbouring delimiter
      DELETE=.FALSE.
      IF(STRING(IFI-1:IFI-1).EQ.' ')    DELETE= .TRUE.
      DO  L=1,NFL
      IF(STRING(IFI-1:IFI-1).EQ.SFL(L)) DELETE= .TRUE.
      ENDDO
      IF(IFI.LT.ILAS) THEN
        DO  L=1,NFL
        IF(STRING(IFI+1:IFI+1).EQ.SFL(L)) DELETE= .TRUE.
        ENDDO
        IF(STRING(IFI+1:IFI+1).EQ.' ')    DELETE= .TRUE.
      ENDIF 
      IF(DELETE) THEN
        STRING(IFI:ILAS-1) = STRING(IFI+1:ILAS)
        ILAS = ILAS-1
        IFI= IFI -1
      ENDIF
      ENDIF
      ENDDO
C***********************************************************************
C
C***********************************************************************
      STRING(ILAS+1:LEN(STRING)) = REPEAT(' ',LEN(STRING)-ILAS)
      RETURN
  910 CONTINUE
      WRITE(*,*) ' Delimiter found at the beginning of line in COMPDE'
      STOP
      END
      SUBROUTINE SYDEL(STRING,
     1    NSYSPE,LENSPE,SYMSPE,NSYDEL,LENDEL,SYMDEL,NFL,SFL,NFIOUT)
C**********************************************************************C
C
C     Pick species and delimiters
C
C
C     Input:
C       STRING (character)    : string of arbitrary length
C       LENSPE (INTEGER)      : numbers of characters in one name
C       LENDEL (INTEGER)      : numbers of characters in one delimiter
C       NFL (integer)         : number of strings allowed as delimiters
C       SFL (character*1(NFL)): strings allowed as delimiters
C     Output:
C       NSYSPE                            : number of names found
C       SYMSPE (character*LENSPE(NSYSPE))): names
C       NSYDEL                            : number of delimiters found
C       SYMDEL (character*LENSPE(NSYDEL))): delimiters
C
C   Note: The string is assumed to start with a name, COMPDE should
C         be called before
C**********************************************************************C
      IMPLICIT NONE      
      INTEGER LENSPE,LENDEL,NSYSPE,NSYDEL,NFL,IFI,ILAS,IIFDE,IILDE,
     1      LENSTRI,NFIOUT
      INTEGER IFDE,ILDE
      CHARACTER(LEN=*) STRING     
      CHARACTER(LEN=LENSPE) SYMSPE(*)
      CHARACTER(LEN=LENDEL) SYMDEL(*)
      CHARACTER*1 SFL(NFL)
C**********************************************************************C
C     Pick species and delimiters
C**********************************************************************C
      NSYSPE = 0
      NSYDEL = 0
      ILAS= 0 
      LENSTRI=LEN(STRING)
C**********************************************************************C
C     Start with Species
C**********************************************************************C
  10  CONTINUE
      IFI= ILAS + 1
      IF(IFI.GT.LENSTRI) GOTO 20 
      IIFDE= IFDE(STRING(IFI:LENSTRI),NFL,SFL)
      ILAS=IIFDE - 1 + IFI - 1
      IF(IIFDE.EQ.0) THEN
         ILAS = LENSTRI
      ENDIF
      NSYSPE = NSYSPE+1
      IF(ILAS-IFI+1.GT.LENSPE) THEN
        WRITE(NFIOUT,*) ' Name too long in SYDEL:',STRING(IFI:ILAS) 
        STOP
      ENDIF
      SYMSPE(NSYSPE) = STRING(IFI:ILAS)
      IF(ILAS.EQ.LENSTRI) GOTO 20
C**********************************************************************C
C     continue with delimiter
C**********************************************************************C
      IFI = ILAS+ 1
      IF(IFI.GT.LENSTRI) GOTO 20 
      IILDE= ILDE(STRING(IFI:LENSTRI),NFL,SFL)
      ILAS= IILDE + IFI -1 
      IF(IILDE.EQ.0) GOTO 20
      NSYDEL = NSYDEL + 1
      IF(ILAS-IFI+1.GT.LENDEL) THEN
        WRITE(NFIOUT,*) ' Delim too long in SYDEL:',STRING(IFI:ILAS) 
        STOP
      ENDIF
      SYMDEL(NSYDEL) = STRING(IFI:ILAS)
      GOTO 10 
C**********************************************************************C
C     END 
C**********************************************************************C
   20 CONTINUE
      RETURN
      END
      SUBROUTINE INIMIN(NFIOUT,NS,NM,NSYMB,INPF,NFILE6,EXASCA,
     1       PK,TEXP,EXA,VT4,VT5,VT6,IARRH,ISTICK,NUMC,MAT,
     1       AINF,BINF,EINF,T3,T1,T2,AFA,BFA,
     1       STOP,IREV,IPREV,PKREV,TEXPREV,EXAREV,
     1       IENHANCE,XENHANCE,ISEN,IDUP,
     1      FINISH)
C**********************************************************************C
C                                                                      C
C    ***************************************************************   C
C    *                                                             *   C
C    *  PROGRAM FOR THE INPUT OF AN ELEMENTARY REACTION            *   C
C    *                       21.02.1983                            *   C
C    *              LAST CHANGE: 07.07.2005 / R. STAUCH            *   C
C    *                                                             *   C
C    ***************************************************************   C
C                                                                      C
C**********************************************************************C
C                                                                      C
C     INPUT  :                                                         C
C                                                                      C
C     NS             = NUMBER OF ALL SPECIES (NSPEC+NSSPEC)            C
C     NM             = NUMBER OF THIRD BODIES                          C
C     NSYMB(1)       = SPECIES SYMBOLS (NS+NM)                         C
C     INPF           = NUMBER OF INPUT FILE                            C
C     NFILE6         = NUMBER OF OUTPUT FILE FOR LISTING               C
C     SURF           = SURFACE REACTION FLAG                           C
C                                                                      C
C     OUTPUT :                                                         C
C                                                                      C
C     PK,TEXP,EXA(1) = PREEXPONENTIAL, TEMPERATURE COEFFICIENT, AND AC-C
C                      TIVATION ENERGY                                 C
C     M              = NUMBER OF DIFFERENT THIRD BODIES INCLUDING TOTALC
C                      CONCENTRATION M. NM=3 AT THE MOMENT, SEE DATA   C
C                      STATEMENT. NM<6 IS NECESSARY.                   C
C     AINF,BINF,EINF = HIGH PRESSURE LIMIT FOR FALLOFF                 C
C   T3,T1,T2,AFA,BFA = BROADENING FACTORS FOR FALLOFF                  C
C     FINISH         = .TRUE. IF END OF MECHANISM HAS BEEN DETECTED    C
C     STOP           = .TRUE. FOR NORMAL END, .FALSE. FOR ABNORMAL END C
C     MAT(12)        = MATRIX OF COEFFICIENTS                          C
C     IREV           = 0 IF REVERSE REACTION HAS NOT TO BE CONSIDERED  C
C                    = 1 IF REVERSE REACTION HAS TO BE CONSIDERED      C
C     ISTICK         = 0 STICKING COEFFICIENTS NOT GIVEN               C
C                    = 1 STICKING COEFFICIENTS ARE GIVEN               C
C                                                                      C
C     INPUT FROM FILE INPF :                                           C
C                                                                      C
C                                                                      C
C     This subroutine is changed to input and output both of gas phase C
C     and surface reaction mechanism.             ---XFY               C
C**********************************************************************C
C                                                                      C
C                                                                      C
C                                                                      C
C**********************************************************************C
C     STORAGE ORGANIZATION                                             C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL STOP,FINISH,EQUSYM,AUXKEY,IDUP
      LOGICAL PDEP,LLOW,LHIGH
      CHARACTER*1 NZS(13),CHAR1
      PARAMETER (LENKEY=16)           
      PARAMETER (LENVAL=40)           
      PARAMETER (NKMAX=20)           
      CHARACTER(LEN=LENKEY)  SKEY(NKMAX)
      CHARACTER(LEN=LENVAL)  SVAL(NKMAX)
      CHARACTER(LEN=*)  NSYMB(*)
      PARAMETER(LENINC=33)
      CHARACTER(LEN=LENINC) NSC33
      CHARACTER(LEN=LENINC) NSYR(12),NSC
      DIMENSION MAT(12)
      DIMENSION XENHANCE(NS),IIENHANCE(NS)
      DIMENSION INFSPE(12)
      DIMENSION XLOW(3),XTROE(4),
     1                               XREV(3)
      DIMENSION IRETAUX(1)
C**********************************************************************C
C     INITIALIZATION
C**********************************************************************C
      ISYLEN= LEN(NSYMB(1))
      LSEP  = LEN(NZS(1))
      IF(LENKEY.LT.ISYLEN) THEN
        WRITE(*,*) ' Character length of KEY < Length of Species '
        STOP
      ENDIF
      STOP=.TRUE.
      FINISH=.FALSE.
      IREV=0
      ISTICK=0
      NUMC = 0
      NSNM =NS+NM
C**********************************************************************C
C
C     INPUT OF REACTIONS                                               C
C
C**********************************************************************C
C 100 CONTINUE
C***********************************************************************
C     READ REACTION NR
C***********************************************************************
CCXFY NSYR(i):Spezies i in der Reaktion, NZR(i) Vorzeichen i+1. Spezies
      VT4 = 0.0D0
      VT5 = 0.0D0
      VT6 = 0.0D0
      AINF= 0.0D0
      BINF= 0.0D0
      EINF= 0.0D0
      T3=   0.0D0
      T1=   0.0D0
      T2=   0.0D0
      AFA=  0.0D0
      BFA=  1.0D0
C**********************************************************************C
C
C     INPUT OF REACTIONS                                               C
C
C**********************************************************************C
C***********************************************************************
C     Initialize enhanced third bodies 
C***********************************************************************
      IENHANCE = 0
      XENHANCE(1:NS) = 1.0D0
C***********************************************************************
C     Initialize enhanced third bodies 
C***********************************************************************
      CALL MECEXP(INPF,NFIOUT,LENINC,NSYO,
     1      NSYR,PK,TEXP,EXA,IEND,
     1      ISEN,INFSPE,ISTICK,IREV)
	      IF(IEND.EQ. 1) GOTO 400
C***********************************************************************
C     Change to SI units
C***********************************************************************
      EXA=EXA*EXASCA
C***********************************************************************
C
C     CHECK OF SPECIES CONTAINED IN THE MECHANISM
C
C***********************************************************************
C---- set all surplus entries to blanks
      NSYR(NSYO+1:12) = REPEAT(' ',ISYLEN)
C***********************************************************************
C     Loop over entries
C***********************************************************************
      CALL STOADJ(NFIOUT,LENINC,6,NSYR,INFSPE)
      CALL STOADJ(NFIOUT,LENINC,6,NSYR(7),INFSPE(7))
C***********************************************************************
C     Loop over entries
C***********************************************************************
      DO  KK=1,12
      NSC(1:ISYLEN)=NSYR(KK)(1:ISYLEN)
C---- check for stoichiometric coefficient - only 1,2,3 allowed
      NUMS = 0
C---- IS SYMBOL KK A BLANK ?
      IF(NSC(1:8).EQ.'        ')               GOTO 130
C---- IS SYMBOL KK IN SPECIES LIST OR THIRD BODY? (nsnm=ns+nm)
      DO 110 II=1,NSNM
      IF(EQUSYM(ISYLEN,NSC(1:ISYLEN),NSYMB(II))) THEN
            NUMS = II
            GOTO 130
      ENDIF
  110 CONTINUE
C---- IF WE ARE HERE SYMBOL IS INVALID
      GOTO 920
  130 CONTINUE
      MAT(KK) =NUMS
      ENDDO     
C***********************************************************************
C     Check for third body reaction
C***********************************************************************
      IT1 = SUM(INFSPE(1:6))
      IT2 = SUM(INFSPE(7:12))
      PDEP =.FALSE.
      IF(IT1+IT2.GT.0) THEN
C       WRITE(*,*) ' Pressure dependent reaction specified '
        PDEP=.TRUE.
C---- check
      IF(IT1.NE.IT2) THEN
          WRITE(*,*) ' Pressure dependent reaction not balanced '
          STOP
      ENDIF
      IF(IT1.GT.1.OR.IT2.GT.1) THEN
          WRITE(*,*) ' Too many thrid bodies in press. dep. react.'
          STOP
      ENDIF
C***********************************************************************
C     get pointers to third body or species in pressure dependent r.
C***********************************************************************
      NUMTHBL = 0
      NUMTHBR = 0
      DO I=1,6
        IF(INFSPE(I  ).NE.0) THEN
          NUMTHBL=MAT(I)
          IF(NUMTHBL.LE.NS) MAT(I)=NS+1
        ENDIF
        IF(INFSPE(I+6).NE.0) THEN
          NUMTHBR=MAT(I+6)
          IF(NUMTHBR.LE.NS) MAT(I+6)=NS+1
        ENDIF
      ENDDO 
      IF(NUMTHBL.NE.NUMTHBR) THEN
        WRITE(*,*) NSYMB(NUMTHBL),' and ',NSYMB(NUMTHBL),' do not match'
        STOP
      ENDIF
      IF((NUMTHBL.LE.0).OR.(NUMTHBR.LE.0)) THEN
        WRITE(*,*) ' fatal error in pressure dependent reaction'
        STOP
      ENDIF
C---- set third body concentration
      IF(NUMTHBL.LE.NS) THEN
        IENHANCE = 1
        XENHANCE=0.D0
        XENHANCE(NUMTHBL) = 1.0
      ENDIF
C***********************************************************************
C     end of check for pressure dependent reaction
C***********************************************************************
      ENDIF
C**********************************************************************C
C
C     Input of auxiliary data                                          C
C
C**********************************************************************C
      LLOW= .FALSE.
      LHIGH = .FALSE.
      IPREV = 0
C**********************************************************************C
C---- Check for other formulation  (in the surface mechanism)
C**********************************************************************C
      READ(INPF,'(A1)') CHAR1
      BACKSPACE INPF
      IF(CHAR1.EQ.'$') THEN
        IARRH = 1
        READ(INPF,811) CHAR1,NSC33,VT4,VT5,VT6
C---- change to SI units
        VT6=VT6*EXASCA      
        IF(CHAR1.NE.'$') GOTO 9820
         IF(NSC33(1:8).EQ.'        ') GOTO 9820
         DO 8110 II=1,NS
            IF(NSC33(1:ISYLEN).EQ.NSYMB(II)) THEN
               NUMC = II
               GOTO 8111
            ENDIF
 8110    CONTINUE
         GOTO 9820
 8111    CONTINUE
C**********************************************************************C
C---- INPUT OF THIRD BODY DATA
C**********************************************************************C
      ELSE IF(CHAR1.EQ.'#') THEN
        IARRH = 2
        READ(INPF,812) CHAR1,AINF,BINF,EINF
        IF(CHAR1.NE.'#') GOTO 9825
        READ(INPF,813) CHAR1,T3,T1,T2,AFA,BFA
        IF(CHAR1.NE.'#') GOTO 9825
C----CHANGE TO SI-UNITS
        EINF= EINF* EXASCA 
C**********************************************************************C
C---- INPUT OF THIRD BODY DATA
C**********************************************************************C
      ELSE IF(CHAR1.EQ.'@') THEN
        IARRH = 4
        READ(INPF,812) CHAR1,VT4,VT5,VT6
        Write(*,*) '*###+++#### IARRH changed to four #####'
        write(*,*) 'CHAR1: ',CHAR1,' -- VT4: ',VT4,' -- VT5: ',VT5
        write(*,*) 'und natuerlich VT6: ',VT6
        write(*,*) 'IREV ist in INIMIN gesetzt auf: ',IREV
        write(*,*) 'IARRH ist in INIMIN gesetzt auf: ',IARRH
        IF(CHAR1.NE.'@') GOTO 9830
      ELSE
        IARRH = 0
      ENDIF
C---- FORMAT STATEMENTS
C 810 FORMAT(5(A8,A1),E10.3,F7.1,F10.1)
 811  FORMAT(A1,A33,11X,E10.3,F7.1,F10.1)
  812 FORMAT(A1,5X,E10.3,2X,F6.2,2X,F10.2)
  813 FORMAT(A1,5X,4(E10.3,2X),F6.2)
Cmaiwald  812 FORMAT(A1,5X,E10.3,2X,F6.2,2X,F8.2)
Cmaiwald  813 FORMAT(A1,5X,3(E10.3,2X),F6.2,2X,F6.2)
C***********************************************************************
C     Check for reverse reaction
C***********************************************************************
      IF(IREV.EQ.1.AND.IARRH.EQ.4) THEN
         WRITE(*,*) ' REVERSE REACTION NOT ALLOWED FOR IARRH=4'
         STOP
      ENDIF
C**********************************************************************C
C
C     Check for auxiliary data
C
C***********************************************************************
      CALL AUXCHE(INPF,NFILE6,LENKEY,LENVAL,SKEY,SVAL,NKMAX,NFOU)
      IF(NFOU.LE.0) GOTO 200 
C***********************************************************************
C     Check for enhanced species
C***********************************************************************
      CALL AUXGE1(NFIOUT,LENKEY,LENVAL,SKEY,SVAL,NFOU,NS,1,NSYMB,
     1                IIENHANCE,XENHANCE)
      IF(ANY(IIENHANCE.NE.0)) THEN
         IF(IENHANCE.NE.0) THEN
           WRITE(*,*) ' Species defined in third body reaction',
     1                ' and enhanced bodies defined at the same time'
         STOP
         ENDIF
         IENHANCE=1
      ENDIF
C***********************************************************************
C     Check for duplicate reactions
C***********************************************************************
      IF(AUXKEY(LENKEY,SKEY,NFOU,'DUP'))  THEN
C        WRITE(*,*) 'REACTION allowed to be duplicate'
         IDUP = .TRUE.
      ELSE
         IDUP = .FALSE.
      ENDIF
C***********************************************************************
C     Check for Low Pressure data 
C***********************************************************************
      XLOW=0.D0
      CALL AUXGE1(NFIOUT,LENKEY,LENVAL,SKEY,SVAL,NFOU,1,3,'LOW',
     1                IRETAUX(1),XLOW)       
      ILOW = IRETAUX(1)
      IF(ILOW.NE.0) THEN
        LLOW  = .TRUE.
        IARRH = 2
      ALOW = XLOW(1) 
      BLOW = XLOW(2)
      ELOW = XLOW(3)*EXASCA
      AHIG = PK
      BHIG = TEXP   
      EHIG = EXA   
      AINF=AHIG
      BINF=BHIG
      EINF=EHIG
      PK  = ALOW
      TEXP=BLOW
      EXA =ELOW
C---- these values correspond to the values of A,B,Ea in HOMREA
      WRITE(*,*)  'LOW',ILOW,ALOW,BLOW,ELOW
      ENDIF             
C***********************************************************************
C     Check for Reverse reaction parameters
C***********************************************************************
      XREV=0.D0
      CALL AUXGE1(NFIOUT,LENKEY,LENVAL,SKEY,SVAL,NFOU,1,3,'REV',
     1                IRETAUX(1),XREV)       
      IPREV = IRETAUX(1)
      IF(IPREV.NE.0) THEN
        PKREV=XREV(1)
        TEXPREV=XREV(2)
        EXAREV=EXASCA*XREV(3)
      ELSE
        PKREV=0.0D0
        TEXPREV=0.0D0   
        EXAREV=0.0D0             
      ENDIF             
C***********************************************************************
C     Check for Troe data 
C***********************************************************************
      XTROE = 0.0D0
      CALL AUXGE1(NFIOUT,LENKEY,LENVAL,SKEY,SVAL,NFOU,1,4,'TROE',
     1                IRETAUX(1),XTROE)       
      ITROE = IRETAUX(1)
      IF(ITROE.NE.0) THEN
      HT1  = XTROE(3) 
      HT2  = XTROE(4) 
      HT3  = XTROE(2) 
      HAFA  = XTROE(1) 
      HBFA  = 0 
      T1=HT1
      T2=HT2
      T3=HT3
      AFA=HAFA
      BFA=HBFA
C---- these values correspond to the values of A,B,Ea in HOMREA
c     WRITE(*,*)  'TROE',HT1,HT2,HT3,HAFA,HBFA 
c     WRITE(*,*)  'TROE',T1,T2,T3,AFA,BFA 
      ENDIF             
C***********************************************************************
C     Check for Forbidden keywords
C***********************************************************************
      CALL FORKEY(NFIOUT,LENKEY,SKEY,NFOU,'FORD')
      CALL FORKEY(NFIOUT,LENKEY,SKEY,NFOU,'RORD')  
      CALL FORKEY(NFIOUT,LENKEY,SKEY,NFOU,'SRI')  
      CALL FORKEY(NFIOUT,LENKEY,SKEY,NFOU,'LT')
      CALL FORKEY(NFIOUT,LENKEY,SKEY,NFOU,'JAN')
      CALL FORKEY(NFIOUT,LENKEY,SKEY,NFOU,'HIGH')  
      CALL FORKEY(NFIOUT,LENKEY,SKEY,NFOU,'FIT1')  
      CALL FORKEY(NFIOUT,LENKEY,SKEY,NFOU,'TDEP')  
      CALL FORKEY(NFIOUT,LENKEY,SKEY,NFOU,'HV')
      CALL FORKEY(NFIOUT,LENKEY,SKEY,NFOU,'EXCL')
      CALL FORKEY(NFIOUT,LENKEY,SKEY,NFOU,'MOME')
      CALL FORKEY(NFIOUT,LENKEY,SKEY,NFOU,'XSML')
      CALL FORKEY(NFIOUT,LENKEY,SKEY,NFOU,'MOLE')  
      CALL FORKEY(NFIOUT,LENKEY,SKEY,NFOU,'CAL')  
      CALL FORKEY(NFIOUT,LENKEY,SKEY,NFOU,'KCAL')  
      CALL FORKEY(NFIOUT,LENKEY,SKEY,NFOU,'JOUL')  
      CALL FORKEY(NFIOUT,LENKEY,SKEY,NFOU,'KJOUL')  
      CALL FORKEY(NFIOUT,LENKEY,SKEY,NFOU,'KELV')  
      CALL FORKEY(NFIOUT,LENKEY,SKEY,NFOU,'EVOL')  
C**********************************************************************C
C
C     Check consistency of auxiliary data                              C
C
C**********************************************************************C
      IF(PDEP.AND.(.NOT.(LLOW.OR.LHIGH))) THEN
         WRITE(*,*) 'pressure dependent reaction without LOW or HIGH'
         STOP
      ENDIF
C***********************************************************************
C     End of block for auxillary data 
C***********************************************************************
 200  CONTINUE
C***********************************************************************
C     CONVERT STICKING COEFFICIENT TO RATE CONSTANT                   
C     Die Parameter in der Geschwindigkeitsgleichung sind nach Gl.(2.73)
C     in der Dissert. von OD gerechnet.                   ---XFY        
C***********************************************************************
      IF(ISTICK.NE.0) THEN     
      EXA=0.0D0  
      TEXP=0.5
      NSC(1:ISYLEN)=NSYR(1)(1:ISYLEN)
      DO 197 II=1,NS
      IF(EQUSYM(ISYLEN,NSC,NSYMB(II))) ISTICK = II
  197 CONTINUE
C-----if more or bulk species are considered, it has to be impl.
C-----end units for surface are in cm,s,g,mol - system!
      PK=PK/(1-PK/2.0)
      ENDIF
C***********************************************************************
C     
C***********************************************************************
      GOTO 700
C**********************************************************************C
C     ERROR EXITS                                                      C
C**********************************************************************C
  400 CONTINUE
      FINISH=.TRUE.
      GOTO 700
  700 CONTINUE
      RETURN
C**********************************************************************C
C     ERROR EXITS                                                      C
C**********************************************************************C
C9115 WRITE(*,*) ' ERROR IN INTERNAL READ ' 
C     STOP      
C900  WRITE(NFILE6,901)
C901  FORMAT('1',5X,' NO MECHANISM AVAILABLE (STRING "MECH" NOT FOUND)')
C     GOTO 999
C910  WRITE(NFILE6,911) 
C911  FORMAT(' ',5X,'ERROR - INCORRECT INPUT OF REACTION ')
C     GO TO 999
 920  WRITE(NFILE6,921) NSC(1:ISYLEN)
 921  FORMAT(' ',5X,'UNKNOWN SPECIES -',A,'-IN THE FOLLOWING REACTION')
      GOTO 999
 9820 WRITE(NFILE6,9821)
 9821 FORMAT(' ',5X,'Error in input of non-arrhenius rate coefficients')
      GOTO 999
 9825 WRITE(NFILE6,9826)
 9826 FORMAT(' ',5X,'Error in input of falloff rate coefficients')
      GOTO 999
 9830 WRITE(NFILE6,9821)
C9831 FORMAT(' ',5X,'Error in input of h-ny rate coefficients')
C     GOTO 999
 999  STOP=.FALSE.
      WRITE(NFILE6,995) 
     1       (NSYR(KK)(1:ISYLEN),NZS(KK),KK=1,5),PK,TEXP,EXA
  995 FORMAT(1X,79('='),/,1X,5(A,A1),E10.3,F7.1,-3PF10.1,/,79('='))
      RETURN
C**********************************************************************C
C     END OF -INIMIN-                                                  C
C**********************************************************************C
      END
      SUBROUTINE GENMEC(INFO,NRMAX,NS,NSS,NSYMB,INPF,NFILE6,NFILE3,PK,
     1      TEXP,EXA,VT4,VT5,VT6,IARRH,ISTICK,NUMC,AINF,BINF,EINF,T3,
     2      T1,T2,AFA,BFA,HCPL,HCPH,HINF,NM,NMMAX,MCOLLI,
     1      INERT,NINERT,IREVER,ISENS,
     3      MAT,KOEFL,KOEFR,NR,NRIMAX,NRVEC,
     1      IENHANCE,XENHANCE,EFF,NMTOT,STOP,TEST,NOMECH,TSO,
     4      REVSIM,TROE)
C**********************************************************************C
C                                                                      C
C    ***************************************************************   C
C    *                                                             *   C
C    *  PROGRAM FOR THE INPUT AND PRINTOUT OF A REACTION MECHANISM *   C
C    *  CONSISTING OF ELEMENTARY REACTIONS ( ORDER=MOLECULARITY )  *   C
C    *                    AUTHOR: J.WARNATZ                        *   C
C    *                       21.02.1983                            *   C
C    *              LAST CHANGE:  07.07.2005 / R.STAUCH            *   C
C    *                                                             *   C
C    ***************************************************************   C
C                                                                      C
C**********************************************************************C
C                                                                      C
C     INPUT  :                                                         C
C                                                                      C
C     NRMAX          = MAXIMUM NUMBER OF REACTIONS (FIRST INDEX OF     C
C                      KOEFR AND KOEFL)                                C
C     NS             = NUMBER OF SPECIES                               C
C     NSYMB(1)       = SPECIES SYMBOLS (NS+NM)                         C
C     INPF           = NUMBER OF INPUT FILE                            C
C     NFILE6         = NUMBER OF OUTPUT FILE FOR LISTING               C
C     NFILE3         = NUMBER OF OUTPUT FILE (INTERNAL STORAGE)        C
C                                                                      C
C     OUTPUT :                                                         C
C                                                                      C
C     PK,TEXP,EXA(1) = PREEXPONENTIAL, TEMPERATURE COEFFICIENT, AND AC-C
C                      TIVATION ENERGY (NRMAX)                         C
C     AINF,BINF,EINF = HIGH PRESSURE LIMIT FOR FALLOFF                 C
C   T3,T1,T2,AFA,BFA = BROADENING FACTORS FOR FALLOFF                  C
C     NM             = NUMBER OF DIFFERENT THIRD BODIES INCLUDING TOTALC
C                      CONCENTRATION M. NM=6 AT THE MOMENT, SEE        C
C                      SEE PARAMETER STATEMENT 'NMMAX'                 C
C     INERT(1)       = INERT SPECIES NUMBERS (NS)                      C
C     NINERT         = NUMBER OF INERT SPECIES                         C
C     IREVER(1)      = VECTOR FOR THE IDENTIFICATION OF REVERSE REAC-  C
C                      TIONS (NRMAX)                                   C
C     KOEFR(NR,1)    = MATRIX OF RIGHT AND LEFT HAND SIDE STOICHIOME-  C
C     KOEFL(NR,1)      TRIC COEFFICIENTS (NRMAX,NSNM)                  C
C     NR             = NUMBER OF REACTIONS                             C
C     STOP           = .TRUE. FOR NORMAL END, .FALSE. FOR ABNORMAL END C
C     REVSIM         = .TRUE. FOR SIMULTANEOUS CALC. OF THE REVERSE    C
C                      REACTION RATE LATER IN HOMRUN                   C
C     TROE           = .TRUE. FOR PRESS-DEPENDENT THIRD BODY REACTIONS C
C                                                                      C
C     INPUT FROM FILE INPF :                                           C
C                                                                      C
C                                                                      C
C     OUTPUT ON FILE NFILE3 :                                          C
C                                                                      C
C     STOICHIOMETRIC COEFFICIENTS AND ARRHENIUS PARAMETERS (M,MOL,S,J),C
C     INERT SPECIES, INFORMATION ON REVERSE REACTIONS                  C
C                                                                      C
C**********************************************************************C
C                                                                      C
C                                                                      C
C                                                                      C
C**********************************************************************C
C     STORAGE ORGANIZATION                                             C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      PARAMETER (ZERO=0.0D0)
      DIMENSION PK(NRMAX),VT4(NRMAX),VT5(NRMAX),VT6(NRMAX),IARRH(NRMAX)
      LOGICAL STOP,TEST,TSO,TBODY(NMMAX),FINISH,NOMECH,EQUSYM,SURF,
     1        REVSIM,IREVSIM,TROE
      CHARACTER*80 ITEXT
      CHARACTER*4 HEAD 
      CHARACTER(LEN=*), DIMENSION(NMMAX) ::  MCOLLI
      CHARACTER(LEN=*)  NSYMB
      PARAMETER (NEUNIT=6,NAUNIT=2)
      CHARACTER (LEN=16), DIMENSION(NEUNIT) :: SEUNIT
      CHARACTER (LEN=16), DIMENSION(NAUNIT) :: SAUNIT
      LOGICAL, DIMENSION(NRMAX)  :: IDUP     
      DIMENSION HCPL(7,*),HCPH(7,*),HINF(3,*)
      DIMENSION EXA(NRMAX),TEXP(NRMAX),NSYMB(*),KOEFR(*),
     1          KOEFL(*),INERT(*),IREVER(*),
     2          ISENS(*),ISTICK(*),NUMC(*),
     3          AINF(NRMAX),BINF(NRMAX),EINF(NRMAX),
     4          T3(NRMAX),T1(NRMAX),T2(NRMAX),AFA(NRMAX),BFA(NRMAX),
     5          MATH(12),MAT(12,NRMAX),INFR(2,6),
     6          NRVEC(3,NRIMAX)
      DIMENSION IENHANCE(*),XENHANCE(NS,*),EFF(NS,*)
C**********************************************************************C
C
C     DEFINITION OF INITIAL VALUES                       
C
C**********************************************************************C
C**********************************************************************C
C     Units
C**********************************************************************C
      DATA SAUNIT(1)/'MOLES           '/,SAUNIT(2)/'MOLECULES       '/
      DATA SEUNIT(1)/'KJOULES/MOLE    '/,SEUNIT(2)/'KCAL/MOLE       '/,
     1     SEUNIT(3)/'JOULES/MOLE     '/,SEUNIT(4)/'CAL/MOLE        '/,
     1     SEUNIT(5)/'KELVINS         '/,SEUNIT(6)/'EVOLTS          '/ 
C**********************************************************************C
C    
C**********************************************************************C
      IF(TEST) WRITE(NFILE6,*) ' GENMEC entered '
C**********************************************************************C
C     INITIALIZATION
C**********************************************************************C
      LSYMB=LEN(NSYMB(1))
      LENCOL=LEN(MCOLLI(1)) 
      LFILL= LSYMB-LENCOL
      AINF = ZERO
      BINF = ZERO
      EINF = ZERO 
      T1   = ZERO 
      T2   = ZERO 
      T3   = ZERO 
      AFA  = ZERO
      BFA  = ZERO 
      SURF = .FALSE.
      HEAD = 'MECH'
      IF(INFO.EQ.1) THEN
        SURF = .TRUE.
        HEAD = 'SURF'
      ENDIF
      STOP=.TRUE.
      NM=NMMAX
      NSNM =NS+NM
      DO I=1,NM
         NSYMB(NS+I)=MCOLLI(I)//REPEAT(' ',LFILL)
         TBODY(I)=.FALSE.
      ENDDO
      NSYMB(NS+NM+1) =REPEAT(' ',LSYMB)
C_rst CASE DIFFERENTIATION: GAS PHASE / SURFACE REACTIONS
C**********************************************************************C
C     INPUT,OUTPUT OF HEADING                                          C
C**********************************************************************C
   10 READ(INPF,801,END=900,ERR=900) ITEXT
      IF(.NOT.EQUSYM(4,ITEXT,HEAD))  THEN
        WRITE(NFILE6,802) ITEXT
        GOTO 10
      ENDIF
C**********************************************************************C
C
C     Transfor units    
C
C**********************************************************************C
C**********************************************************************C
C     Check for keywords for unit
C**********************************************************************C
C---- A
      IFO = 0
      IAUNIT = 1
      DO I=1,NAUNIT
      IF(INDEX(ITEXT,TRIM(SAUNIT(I))).NE.0) THEN
          IAUNIT = I
          IFO = IFO+1
      ENDIF
      ENDDO
      IF(IFO.GT.1) THEN
        WRITE(*,*) ' More than one keyword for units of A specified '
        STOP
      ELSE 
        WRITE(*,*) ' Units of A are:',TRIM(SAUNIT(IAUNIT))
      ENDIF
C---- E
      IFO = 0
      IEUNIT = 1
      DO I=1,NEUNIT
      IF(INDEX(ITEXT,TRIM(SEUNIT(I))).NE.0) THEN
          IEUNIT = I
          IFO = IFO+1
      ENDIF
      ENDDO
      IF(IFO.GT.1) THEN
        WRITE(*,*) ' More than one keyword for units of E_a specified '
        STOP
      ELSE 
        WRITE(*,*) ' Units of E are:',TRIM(SEUNIT(IEUNIT))
      ENDIF
C**********************************************************************C
C     Get conversion number                                            C
C**********************************************************************C
      IF(IAUNIT.LT.0.OR.IAUNIT.GT.1) THEN
          WRITE(*,*) ' Up to now only units cm,s,mol allowed for A'
          STOP
      ENDIF
      PKSCA = 1.0
C----      kJoules to Joules
      IF(IEUNIT.EQ.1) THEN
           EXASCA=1000.0
C----      kcals to Joules
      ELSE IF(IEUNIT.EQ.2) THEN
           EXASCA=4184.
C----      Joules to Joules
      ELSE IF(IEUNIT.EQ.3) THEN
           EXASCA=1.D0 
C----      cal    to Joules
      ELSE IF(IEUNIT.EQ.4) THEN
           EXASCA=4.184
C----      T      to Joules
      ELSE IF(IEUNIT.EQ.5) THEN
           EXASCA=8.3144598
C----      T      to Joules
      ELSE IF(IEUNIT.EQ.6) THEN
           EXASCA=1.602176487D-19
      ELSE
          WRITE(*,*) ' These Units for Ea  not allowed '
          STOP
      ENDIF
C**********************************************************************C
C
C     Write header for reactions 
C
C**********************************************************************C
      LFIL=6*LSYMB-42
      IF(.NOT.TSO.AND.(.NOT.NOMECH)) 
     1      WRITE(NFILE6,2303) ITEXT,REPEAT(' ',LFIL),REPEAT(' ',LFIL)
      IF(NOMECH)        WRITE(NFILE6,1203) ITEXT
 1203 FORMAT(' ',7X,A40,/,
     1       ' ',7X,'(Output suppressed)')
 2303 FORMAT(' ',3X,A60,A,'A',5X,'T-Exp.',4X,'E',/,
     1           A,60X,'(cm,mol,s)',2X,'(-)',2X,'(kJ/mol)')
C**********************************************************************C
C
C     INPUT OF REACTIONS                                               C
C
C**********************************************************************C
      NR=0
  100 CONTINUE
      NR=NR+1
      CALL INIMIN(NFILE6,NS,NM,NSYMB,INPF,NFILE6,EXASCA,
     1  PK(NR),TEXP(NR),EXA(NR),VT4(NR),VT5(NR),VT6(NR),IARRH(NR),
     2  ISTICK(NR),NUMC(NR),MATH,
     2  AINF(NR),BINF(NR),EINF(NR),T3(NR),T1(NR),T2(NR),AFA(NR),BFA(NR),
     3  STOP,IREV,IPREV,PKREV,TEXPREV,EXAREV,
     1  IENHANCE(NR),XENHANCE(1,NR),
     1   ISENS(NR),IDUP(NR),
     1   FINISH)
       TROE=.TRUE.
      IF(.NOT.STOP) GOTO 999
      IF(FINISH) THEN     
          NR=NR-1
          GOTO 400
      ENDIF 
C***********************************************************************
C     STORE MATH IN MAT IN DECENDING ORDER
C***********************************************************************
      CALL ISTDEC(MATH,MAT(1,NR))
C***********************************************************************
C     IF NO REVERSE REACTION JUMP TO 100 AND READ NEXT REACTION
C***********************************************************************
      IF(IREV.EQ.0) GOTO 111
C***********************************************************************
C
C      Handle reverse reactions 
C
C***********************************************************************
      IREVSIM=(REVSIM.OR.IARRH(NR).EQ.2)
C***********************************************************************
C     Reverse reaction calculated during integration     
C***********************************************************************
      IF(IPREV.NE.0)   THEN
        PK(NR+1)   = PKREV
        TEXP(NR+1) = TEXPREV
        EXA(NR+1)  = EXAREV  
        VT4(NR+1)  = ZERO
        VT5(NR+1)  = ZERO
        VT6(NR+1)  = ZERO
      ELSE IF(IREVSIM) THEN
        IF(IARRH(NR) .EQ. 0) IARRH(NR)=3
        IARRH(NR+1)=-IARRH(NR)
        PK(NR+1)   = ZERO
        TEXP(NR+1) = ZERO
        EXA(NR+1)  = ZERO
        VT4(NR+1)  = ZERO
        VT5(NR+1)  = ZERO
        VT6(NR+1)  = ZERO
      ELSE   
C***********************************************************************
C     arrhenius parameters precalculated
C***********************************************************************
        CALL INIMRP(NS,NFILE6,PK(NR),TEXP(NR),EXA(NR),
     1      VT4(NR),VT5(NR),VT6(NR),IARRH(NR),
     1      HCPL,HCPH,HINF,MAT(1,NR),
     1      PK(NR+1),TEXP(NR+1),EXA(NR+1),
     1      VT4(NR+1),VT5(NR+1),VT6(NR+1),IARRH(NR+1))
      END IF
C***********************************************************************
C     STORE REVERSE REACTION
C***********************************************************************
C***********************************************************************
C
C      Add reaction and calculate parameters
C
C***********************************************************************
      NR=NR+1
C***********************************************************************
C     STORE INFORMATION ABOUT SENSITIVITY IN IREVER
C***********************************************************************
      ISTICK(NR)=0
      ISENS(NR) = ISENS(NR-1)
      IENHANCE(NR) = IENHANCE(NR-1)
      IDUP(NR) = IDUP(NR-1)
      XENHANCE(1:NS,NR) = XENHANCE(1:NS,NR-1)
      NUMC(NR)=NUMC(NR-1)
C***********************************************************************
C     STORE COEFFICIENTS
C***********************************************************************
      MAT(1:6,NR)    = MAT(6+1:12,NR-1)
      MAT(6+1:12,NR) = MAT(1:6,NR-1)
      DO I=1,12
      IF(MAT(I,NR).GT.2000) STOP 'ppppppppppppppp'
      ENDDO
C**********************************************************************C
C     END OF INPUT OF REACTIONS                                        C
C**********************************************************************C
 111  CONTINUE
      GO TO 100
C**********************************************************************C
C     END OF INPUT OF REACTIONS                                        C
C**********************************************************************C
  400 CONTINUE
      WRITE(NFILE6,98)
C**********************************************************************C
C     IDENTIFICATION OF REVERSE REACTIONS AND INFORMATION ABOUT        C
C     SENSITIVITY ANALYSIS                                             C
C**********************************************************************C
      IFOR=0
      IREVER(1)=0
      DO 420 I=2,NR
      IREVER(I)=0
      ITST=0
      DO 410 LL=1,6
C     IF(MAT(LL  ,I).NE.MAT(LL+6,I-1))  GOTO 420
      IF(MAT(LL  ,I).NE.MAT(LL+6,I-1))  GOTO 420
      IF(MAT(LL+6,I).NE.MAT(LL  ,I-1))  GOTO 420
  410 CONTINUE
      IREVER(I)=     2
      IREVER(I-1)=   1
      IMINS=I-1
      IFOR=1
C     WRITE(NFILE6,841) I,IMINS
  420 CONTINUE
      DO I=1,NR
         IREVER(I)= IREVER(I)+1
         IF(ISENS(I).EQ.1) IREVER(I)=-IREVER(I)
      ENDDO
C***********************************************************************
C     Identify duplicate reactions 
C***********************************************************************
      NCE = 12
C        WRITE(*,*) ALL(MAT(1:NCE,1:NR-1).EQ.MAT(1:NCE,NR:NR),1)
      NDUP=0
      DO KK=2,NR
      DO JJ=1,KK-1 
        IF((.NOT.IDUP(KK).AND..NOT.IDUP(JJ)).AND.
     1        ALL(MAT(1:NCE,KK).EQ.MAT(1:NCE,JJ)))    THEN
              WRITE(NFILE6,9711) KK,JJ
              NDUP = NDUP+1
        ENDIF
      ENDDO
      ENDDO
C     IF(NDUP.GT.0) GOTO 9710
C**********************************************************************C
C
C     OUTPUT ON NFILE6                                                 C
C
C**********************************************************************C
C***********************************************************************
C     PRINT REACTION EQUATION
C***********************************************************************
      IF(.NOT.(NOMECH.AND.TSO)) THEN
      DO I=1,NR
         CALL MXIMAN(NSYMB,MAT(1,I),NFILE6,
     1    I,PK(I),TEXP(I),EXA(I),VT4(I),VT5(I),VT6(I),IARRH(I),
     1    AINF(I),BINF(I),EINF(I),T3(I),T1(I),T2(I),AFA(I),
     1    BFA(I))
      IF(IENHANCE(I).NE.0) 
     1   CALL PRIENH(NFILE6,'             Enhanced third body:',
     1       LSYMB,NS,NSYMB,1.0D0,XENHANCE(1,I),
     1    '(F5.2)')
      ENDDO 
      IF(IFOR.NE.0) WRITE(NFILE6,842)
  842 FORMAT(' ','   Pairs of forward and reverse reactions:  ')
      LAUF=0
      DO 427 I=1,NR
      IF((IABS(IREVER(I))-1).EQ.1) THEN
        LAUF=LAUF+1
        INFR(1,LAUF)=I
        INFR(2,LAUF)=I+1
      ENDIF
      IF(LAUF.EQ.5) THEN
        WRITE(NFILE6,841)
     1    ('(',INFR(1,JJ),',',INFR(2,JJ),')',JJ=1,5)
        LAUF=0
      ENDIF
  841 FORMAT('      ',5(A1,I4,A1,I4,A1,3X))
  427 CONTINUE
C---- CLEANUP LOOP
      IF(LAUF.NE.0) WRITE(NFILE6,841)
     1    ('(',INFR(1,JJ),',',INFR(2,JJ),')',JJ=1,LAUF)
      ENDIF
C**********************************************************************C
C     CHECK FOR OCCURENCE OF THIRD BODIES                              C
C**********************************************************************C
      DO J=1,NM
         DO K=1,12
            DO I=1,NR
               IF(MAT(K,I).EQ.NS+J) TBODY(J) = .TRUE.
            ENDDO
         ENDDO
      ENDDO
C**********************************************************************C
C     COMPRESSION OF STOECHIOMETRIC MATRIX                             C
C**********************************************************************C
C---- LOOK FOR LAST THIRD BODY
C---- BEGIN AT NM AND LOOK FOR LAST THIRD BODY
      DO 505 KK=2,NM
      LL=NM-KK+2
      NMNEW=LL
      IF(TBODY(LL)) GOTO 506
  505 CONTINUE
      NMNEW = 1
  506 CONTINUE
C---- SET NEW VALUES
      NM=NMNEW
      NSNM=NM+NS
C**********************************************************************
C
C     Test and print enhanced third bodies
C
C**********************************************************************
      NMTOT = NM
      DO I=1,NR
      IF(IENHANCE(I).GT.0) THEN
        NMTOT=NMTOT+1
        EFF(1:NS,NMTOT)=XENHANCE(1:NS,I)
C**********************************************************************
C     CHANGE stoichiometric matrix
C**********************************************************************
      DO KK=1,12
      IF(MAT(KK,I).EQ.NS+1) MAT(KK,I)=NS+NMTOT
      ENDDO
C************
      ENDIF
      ENDDO
      NSNM=NS+NMTOT
C***********************************************************************
C
C     CALCULATE STOICHIOMETRIC MATRIX
C
C***********************************************************************
      NRNSNM=NR*NSNM
      DO LL=1,NRNSNM
C---- SET KOEFFICIENTS TO ZERO
         KOEFR(LL)=0
         KOEFL(LL)=0
      ENDDO
C---- IF SYMBOL(KK) IS SPECIES LL THEN ADD COEFFICIENT OF KK
      DO I=1,NR
      DO KK=1,6
      NSTOEL=MAT(KK  ,I)
      NSTOER=MAT(KK+6,I)
      IF(NSTOEL.GT.0) KOEFL(I+(NSTOEL-1)*NR)=KOEFL(I+(NSTOEL-1)*NR)-1
      IF(NSTOER.GT.0) KOEFR(I+(NSTOER-1)*NR)=KOEFR(I+(NSTOER-1)*NR)+1
      ENDDO
      ENDDO
C***********************************************************************
C     IDENTIFICATION OF INERT SPECIES                                  C
C**********************************************************************C
      DO I=1,NS
         INERT(I)=1
         DO M=1,NR
            LAUF=(M-1)*NR
            IF(KOEFR(M+(I-1)*NR).NE.KOEFL(M+(I-1)*NR)) INERT(I)=0
         ENDDO
      ENDDO
C**********************************************************************C
C     LISTING OF INERT SPECIES ON FILE NFILE6                          C
C**********************************************************************C
      NINERT=0
      DO 440 I=1,NS
      IF(INERT(I).EQ.0) GO TO 440
      NINERT=NINERT+1
      WRITE(NFILE6,844) NSYMB(I)
  440 CONTINUE
      IF(NINERT.EQ.0) WRITE(NFILE6,843)
C**********************************************************************C
C     CHECK FOR ORDER OF SPECIES                                       C
C**********************************************************************C
      IF(NINERT.EQ.0) GOTO 460
      NSMI=NS-NINERT
      DO I=1,NSMI
         IF(INERT(I).NE.0) WRITE(NFILE6,846) NSYMB(I)
      ENDDO
  460 CONTINUE
C**********************************************************************C
C     CHECK FOR BALANCE OF THIRD BODIES                                C
C**********************************************************************C
      NSP1=NS+1
      DO I=1,NR
         DO LL=NSP1,NSNM
            IF(KOEFR(I+(LL-1)*NR).NE.-KOEFL(I+(LL-1)*NR)) GOTO 950
         ENDDO
      ENDDO
C**********************************************************************C
C
C     Transform into SI units
C
C**********************************************************************C
C**********************************************************************C
C     OUTPUT OF ARRHENIUS PARAMETERS ON FILE NFILE3                    C
C**********************************************************************C
C---- TRANSFORM TO SI UNITS
      IF(.NOT.SURF) THEN
         DO M=1,NR
            LAUF=(M-1)*NR
            IORD=0
            DO I=1,NSNM
               IORD=IORD-KOEFL(M+(I-1)*NR)
            ENDDO
            PK(M)=PK(M)    *(1.0E-6*PKSCA)**(IORD-1)
         ENDDO
      ELSE
C**********************************************************************C
C     SURFACE REACTION   
C**********************************************************************C
         DO M=1,NR                                                    
            NIA2M=0
            NIA3M=0
            DO KK=1,6
               IF((MAT(KK,M).GT.NS-NSS).AND.(MAT(KK,M).LE.NS)) THEN
                  NIA2M=NIA2M+1
               ELSEIF((MAT(KK,M).LE.NS-NSS).AND.(MAT(KK,M).NE.0)) THEN
                  NIA3M=NIA3M+1
               ENDIF
            ENDDO
            IF(ISTICK(M).EQ.0)
     1           PK(M)=PK(M)*1.0D-4**(NIA2M-1)*1.0D-6**(NIA3M)
         ENDDO
      ENDIF
C**********************************************************************C
C                                                                      C
C     OUTPUT OF STOICHIOMETRIC COEFFICIENTS ON FILE NFILE3             C
C                                                                      C
C**********************************************************************C
C**********************************************************************C
C     OUTPUT                                                           C
C**********************************************************************C
      WRITE(NFILE3,850) NR
      WRITE(NFILE3,851) NMTOT
C**********************************************************************C
C
C     Store for regular reaction
C
C**********************************************************************C
      IF(.NOT.SURF) THEN
C**********************************************************************C
C     CHECK whether entry of matrix > 3 prod or reactands 
C**********************************************************************C
      DO I=1,NR
      IF(ANY(MAT(4:6,I).NE.0).OR.ANY(MAT(10:12,I).NE.0))  THEN
        WRITE(*,*) ' Too many reactands or products in reaction',I
        STOP
      ENDIF
      ENDDO
C**********************************************************************C
C     Print reaction parameters
C**********************************************************************C
      DO M=1,NR
         WRITE(NFILE3,852) (MAT(I,M),I=1,3),(MAT(I,M),I=7,9),
     1      PK(M),TEXP(M),EXA(M),
     1        IARRH(M),VT4(M),VT5(M),VT6(M)
         WRITE(NFILE3,8525) AINF(M),BINF(M),EINF(M),T3(M),T1(M),
     1        T2(M),AFA(M),BFA(M)
      ENDDO
  852 FORMAT(6(1X,I5),3X,3(1PE11.4,1X),/,1X,I5,33X,3(1PE11.3,1X))
 8525 FORMAT(25X,3(1PE11.4,1X),/,25X,3(1PE9.2,1X),1X,1PE9.2,1X,1PE9.2)
C**********************************************************************C
C
C     Store for surface reaction
C
C**********************************************************************C
      ELSE
      DO M=1,NR
         WRITE(NFILE3,856) (MAT(I,M),I=1,12),PK(M),TEXP(M),EXA(M),
     1        IARRH(M),ISTICK(M),NUMC(M),VT4(M),VT5(M),VT6(M)
      ENDDO
  856 FORMAT(12(1X,I5),/,36X,3X,3(1PE11.4,1X),/,
     1       3(1X,I5),18X,3X,3(1PE11.4,1X))
      ENDIF
C***********************************************************************
C     CALCULATE SPARSE PATTERN FOR REACTION RATES                      *
C***********************************************************************
C---- SPECIES NRVEC(1,J) REACTS IN REACTION NRVEC(3,J) WITH
C     STOICHOMETRIE NRVEC(2,J)
      LAUF=0
      DO 660 I=1,NS
      DO 660 J=1,NR
      NHI=KOEFR(J+(I-1)*NR)+KOEFL(J+(I-1)*NR)
      IF(NHI.EQ.0) GOTO 660
      LAUF=LAUF+1
      IF(LAUF.GT.NRIMAX) GOTO 980
      NRVEC(1,LAUF)=I
      NRVEC(3,LAUF)=NHI
      NRVEC(2,LAUF)=J
  660 CONTINUE
      NRINS=LAUF
C**********************************************************************C
C     STORAGE OF NRVEC ON FILE NFILE3                                 C
C**********************************************************************C
      WRITE(NFILE3,853) NRINS
  853 FORMAT(I5,' REACTION POINTERS   ')
      WRITE(NFILE3,857) ((NRVEC(I,J),I=1,3),J=1,NRINS)
  857 FORMAT(4(2X,3I5))
C**********************************************************************C
C     STORAGE OF IREVER ON FILE NFILE3                                 C
C**********************************************************************C
      WRITE(NFILE3,854) (IREVER(I),I=1,NR)
C**********************************************************************C
C     OUTPUT OF INERT SPECIES ON FILE NFILE3                           C
C**********************************************************************C
      WRITE(NFILE3,855) NINERT
      RETURN
C**********************************************************************C
C     ERROR EXITS                                                      C
C**********************************************************************C
C9710 CONTINUE
C     GOTO 999
 9711 FORMAT(1X,' Reaxtion',I5,' is identical to reaction',I5,
     1          ' but not allowed to be a duplicate reaction')
      GOTO 999
 900  WRITE(NFILE6,901)
 901  FORMAT('1',5X,' no mechanism available (string "MECH" not found)')
      GOTO 999
C910  WRITE(NFILE6,911) NR
C911  FORMAT('0',5X,'INCORRECT INPUT OF REACTION ',I3)
C     GO TO 999
C930  WRITE(NFILE6,931) NR
C931  FORMAT('0',5X,'EMPTY LEFT OR RIGHT HAND SIDE IN REACTION ',I3)
C     GOTO 999
C_rst 940  WRITE(NFILE6,941) '              ERROR IN GENMEC  !!!           '
C_rst      WRITE(NFILE6,941) 'MORE THAN 10 INERT SPECIES FOUND --> SEE LIST'
C941  FORMAT(8('+'),4X,A45,4X,8('+'))
C942  FORMAT(3(9X,I2,2X,A8))
      GOTO 999
 950  WRITE(NFILE6,951) I
 951  FORMAT('1',5X,'THIRD BODIES NOT BALANCED IN REACTION',I4)
      GOTO 999
  980 WRITE(NFILE6,981) NRINS,NRIMAX
  981 FORMAT(' ',' NRINS( = ',I4,') >  NRIMAX     ',/,
     1      '     ----->  CHECK DIMENSION OF NRVEC  ')
      GOTO 999
C990  WRITE(NFILE6,991)'         HOMINP:  ERROR IN GENMEC  !!!        '
C     WRITE(NFILE6,991)' CALC. OF REVERSE REACTION RATES OF PRESSURE  ' 
C     WRITE(NFILE6,991)' DEPENDENT THIRD BODY REACTIONS USING TROE    ' 
C     WRITE(NFILE6,991)' FORMALISM ONLY POSSIBLE WHEN USING OPTION    '
C     WRITE(NFILE6,991)' "REVSIM"  !!!                                '
C991  FORMAT(8('+'),4X,A45,4X,8('+'))
C     GOTO 999
 999  STOP=.FALSE.
      RETURN
C**********************************************************************C
C     FORMAT STATEMENTS                                                C
C**********************************************************************C
  801 FORMAT(A80)
  802 FORMAT(' ',A72)
C 803 FORMAT(' ',60X,'A',6X,'T-EXP',4X,'E',/,
C    1           56X,'(CM,MOL,S)',2X,'(-)',1X,'(KJ/MOL)')
C 821 FORMAT(' ',7X,'WARNING:    REACTION ',I5,' IS EQUAL TO REACTION ',
C    1       I5)
C 831 FORMAT(' ',5X,'INCORRECT REVERSE REACT. ARRH. PARAM. ',
C    1       'DUE TO BAD THERMODYN. OR INPUT DATA')
C 841 FORMAT(' ',7X,'REACTION ',I3,' IS REVERSE REACTION OF ',I3)
  843 FORMAT(' ',7X,'no inert species')
  844 FORMAT(' ',7X,'Inert species : ',5(A,1X))
  846 FORMAT(' ',1X,'WARNING: Inert Species ',A,' not at end of list')
   98 FORMAT(' ',2X)
C---- FORMAT STATEMENTS FOR OUTPUT ON NFILE3
  850 FORMAT(I5,' ELEMENTARY REACTIONS')
  851 FORMAT(I5,' THIRD BODIES ')
  854 FORMAT(' IREVER',5X,30I2)
  855 FORMAT(I5,' INERT SPECIES :   ',5(A,1X))
C**********************************************************************C
C     END OF -GENMEC-                                                  C
C**********************************************************************C
      END
      SUBROUTINE MECEXP(NFIINP,NFIOUT,LENSPE,
     1      NSFOU,REASYM,AFAC,TEXP,EXA,IEND,
     1      ISEN,INFSPE,ISTICK,IREV)
C***********************************************************************
C
C    ***************************************************************
C    *                                                             *
C    *    PROGRAM FOR THE INPUT AND PRINTOUT OF a reaction line    *
C    *                                                             *
C    *              LAST CHANGE: 14.03.2017/Maas                   *
C    *                                                             *
C    ***************************************************************
C
C
C***********************************************************************
C
C     INPUT  :
C     HEAD          = Header string 
C     NFIINP        = NUMBER OF INPUT FILE
C     NFIOUT        = NUMBER OF OUTPUT FILE FOR LISTING
C     NFIOUT        = NUMBER OF OUTPUT FILE FOR LISTING
C     NSNM          = number of species and third bodies
C     LNSYMB        = character length  of NSYMB
C
C     OUTPUT :
C
C     REASYM 
C     SEP           
C     A     
C     TEXP
C     EXA
C     IERR          = error flag (ierr = 0 for normal end)
C
C     INPUT FROM FILE NFIINP : Mechanism
C
C    RULES:
C         everything after ! is not considered
C         separators can be blanks,commas,*,+,>
C         more than one separator can follow another
C         the first separator different from a blank or a comma
C         after a species determines the flag 
C***********************************************************************
C
C
C
C***********************************************************************
C     STORAGE ORGANIZATION AND DEFINITION OF INITIAL VALUES            C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      PARAMETER (LINLEN=255)
      LOGICAL EQUSYM,SENSI
      CHARACTER(LEN=LINLEN) STRING
C---- there are at most LINLEN Species and delimiters
      CHARACTER(LEN=LENSPE) REASYM(12)
      DIMENSION INFSPE(12)
C**********************************************************************C
C     Initialization                                                   C
C**********************************************************************C
      IERR  = 0
      IEND  = 0
C**********************************************************************C
C     Input of the line and check for end                              C
C**********************************************************************C
C  10 CONTINUE
      STRING=REPEAT(' ',LINLEN)
      READ(NFIINP,'(A)',END=900) STRING
        IEND = 0
C**********************************************************************C
C     Check for keyword STICK
C**********************************************************************C
      ISTICK = 0
      IF(EQUSYM(5,STRING,'STICK')) THEN    
        ISTICK  = 1
        STRING=REPEAT(' ',LINLEN)
        READ(NFIINP,'(A)',END=900) STRING
      ENDIF
C**********************************************************************C
C     Check for keyword maxi
C**********************************************************************C
      IMAXI  = 0
      IF(EQUSYM(4,STRING,'MAXI')) THEN    
        IMAXI  = 1
        STRING=REPEAT(' ',LINLEN)
        READ(NFIINP,'(A)',END=900) STRING
      ENDIF
C**********************************************************************C
C     Check for keyword END 
C**********************************************************************C
      IF(EQUSYM(3,STRING,'END')) THEN    
        IEND = 1   
        RETURN 
      ENDIF
C**********************************************************************C
C     read line
C**********************************************************************C
  11  CONTINUE
      CALL RETAVA(STRING,
     1   AFAC,TEXP,EXA,NFIOUT,IERR)
      CALL RETNAM(STRING,
     1   NSFOU,LENSPE,REASYM,SENSI,
     1   INFSPE,NFIOUT,IREV,IERR)
      ISEN=0
      IF(SENSI) ISEN=1
      IF(IERR.EQ.1) GOTO 11
      IF(1.EQ.2) THEN
      WRITE(NFIOUT,'(A)') 'TTTTTTTTTTTTTTTTTTTTTTT'
      WRITE(NFIOUT,'(A)') (REASYM(I),I=1,NSFOU)
      WRITE(NFIOUT,'(A)') (SEP(I),I=1,NDFOU)
      WRITE(NFIOUT,'(1PE10.3)')  AFAC,TEXP,EXA
      ENDIF 
C**********************************************************************C
C     PRINTOUT OF SPECIES SYMBOLS                                      C
C**********************************************************************C
      RETURN
C**********************************************************************C
C     ERROR IN INPUT                                                   C
C**********************************************************************C
C---- no heading received
  900 CONTINUE
  901 FORMAT(5X,'Warning In species input: No heading, ',
     F       'no end card, or missing data')
      WRITE(NFIOUT,901)
      IERR = 1
      GOTO 999
C---- too many species
C 920 CONTINUE
C     WRITE(NFIOUT,921) NS,NSMAX
C 921 FORMAT(' ',2X,'ERROR in MECEXP - Too many names:',
C    1              ' NS(',I4,') > NSMAX(',I4,')')
C     IERR = 1
C     GOTO 999
  999 CONTINUE
      RETURN
C**********************************************************************C
C     FORMAT STATEMENT                                                 C
C**********************************************************************C
C  80 FORMAT(7(A8,A1))
C  82 FORMAT(8X,'List of Species and Flags, NS =',I5)
C  83 FORMAT(8X,'Species : ',5(A,1X,I1,2X),
C    1        /,1(18X,5(A,1X,I1,2X)))
C**********************************************************************C
C     END OF HOISYM                                                    C
C**********************************************************************C
      END
      SUBROUTINE RETAVA(STRING,
     1    AFAC,TEXP,EXA,NFIOUT,IERR)
C**********************************************************************C
C
C     Get the Arrhenius parameters from a string (assumed to be at
C     the end
C
C
C     Input:
C       STRING (character)    : string of arbitrary length
C     Output:
C      A, Texp E_a
C   Note: The string is assumed to start with a name, COMPDE should
C         be called before
C**********************************************************************C
      IMPLICIT NONE      
      INTEGER NFIOUT,IERR
      INTEGER ILAS,IFI,IFEX,ILS
      CHARACTER(LEN=*) STRING     
      DOUBLE PRECISION AFAC,TEXP,EXA
C**********************************************************************C
C     Remove comment
C**********************************************************************C
      IERR = 0
      IFEX=SCAN(STRING,'!')
      IF(IFEX.EQ.0) THEN
        ILS=LEN(STRING)
        ILAS=LEN(STRING)
      ELSE
        ILAS = IFEX - 1          
        ILS=LEN(STRING)
        STRING(IFEX:ILS)=' '
      ENDIF  
      IF(ILAS.LE.1) GOTO 910
C**********************************************************************C
C     Trim trailing blanks
C**********************************************************************C
      ILAS=LEN(TRIM(STRING))
C**********************************************************************C
C     get last number    
C**********************************************************************C
      IFI=1+SCAN(STRING(1:ILAS),' ',.TRUE.)
      READ(STRING(IFI:ILAS),*,ERR=9111) EXA
      STRING(IFI:ILAS) = REPEAT(' ',ILAS-IFI+1)
      ILAS=IFI-1
C**********************************************************************C
C     Trim trailing blanks
C**********************************************************************C
      ILAS=LEN(TRIM(STRING))
C**********************************************************************C
C     get second but last number
C**********************************************************************C
      IFI=1+SCAN(STRING(1:ILAS),' ',.TRUE.)
      READ(STRING(IFI:ILAS),*,ERR=9112) TEXP
      STRING(IFI:ILAS) = REPEAT(' ',ILAS-IFI+1)
      ILAS=IFI-1
C**********************************************************************C
C     Trim trailing blanks
C**********************************************************************C
      ILAS=LEN(TRIM(STRING))
C**********************************************************************C
C     get third  but last number
C**********************************************************************C
      IFI=1+SCAN(STRING(1:ILAS),' ',.TRUE.)
      READ(STRING(IFI:ILAS),*,ERR=9113) AFAC
      STRING(IFI:ILAS) = REPEAT(' ',ILAS-IFI+1)
      ILAS=IFI-1
C**********************************************************************C
C     Compress line
C**********************************************************************C
      STRING(ILAS+1:ILS) = REPEAT(' ',ILAS-IFI+1)
      RETURN
C**********************************************************************C
C     get symbols and delimiters
C**********************************************************************C
 910  WRITE(NFIOUT,*) ' RETAVA empty line'             
      IERR =1 
      RETURN
9111  WRITE(NFIOUT,*) '   EXA   ',STRING(IFI:ILAS)
      WRITE(NFIOUT,*) '   ERROR IN RETAVA       '  
      STOP
9112  WRITE(NFIOUT,*) '   TEXP  ',STRING(IFI:ILAS)
      WRITE(NFIOUT,*) '   ERROR IN RETAVA       '  
      STOP
9113  WRITE(NFIOUT,*) '   AFAC  ',STRING(IFI:ILAS)
      WRITE(NFIOUT,*) '   ERROR IN RETAVA       '  
      STOP
      END
      SUBROUTINE RETNAM(STRING,
     1    NSYSPE,LENSPE,SYMSPE,SENSI,
     1    INFSPE,NFIOUT,IREV,IERR)
C**********************************************************************C
C
C     Get names and delimiters from a string
C
C
C     Input:
C       STRING (character)    : string of arbitrary length
C       LENSPE (INTEGER)      : numbers of characters in one name
C       LENDEL (INTEGER)      : numbers of characters in one delimiter
C       NFL (integer)         : number of strings allowed as delimiters
C       SFL (character*1(NFL)): strings allowed as delimiters
C     Output:
C       NSYSPE                            : number of names found
C       SYMSPE (character*LENSPE(NSYSPE))): names
C       NSYDEL                            : number of delimiters found
C       SYMDEL (character*LENSPE(NSYDEL))): delimiters
C
C   Note: The string is assumed to start with a name, COMPDE should
C         be called before
C**********************************************************************C
      IMPLICIT NONE      
      LOGICAL SENSI
      INTEGER LENSPE,NSYSPE,NFL,NFIOUT,IERR,LENSTR
      INTEGER ILAS,IFEX,ILS,IFEQ,INFSPE,LSTR,NSYMAX,ISEQ,IREV
      CHARACTER(LEN=*) STRING     
      PARAMETER(LENSTR=255)
      CHARACTER(LEN=LENSTR) STRINGR,STRINGP
      CHARACTER(LEN=LENSPE) SYMSPE(*)
      PARAMETER(NFL=1)
      CHARACTER*1 SFL(NFL)
      DIMENSION INFSPE(12)
      DATA SFL(1)/'%'/
      NSYMAX = 6
C**********************************************************************C
C     Remove comment 
C**********************************************************************C
      IERR = 0
      IFEX=SCAN(STRING,'!')
      IF(IFEX.EQ.0) THEN
        ILAS=LEN(STRING)
      ELSE
        ILAS = IFEX - 1          
        ILS=LEN(STRING)
        STRING(IFEX:ILS)=' '
      ENDIF  
      IF(ILAS.LE.1) GOTO 910
C**********************************************************************C
C     Trim trailing blanks
C**********************************************************************C
      ILAS=LEN(TRIM(STRING))
C**********************************************************************C
C     Compress line
C**********************************************************************C
      CALL COMPDE(STRING,ILAS,NFL,SFL)
C**********************************************************************C
C     Check left and right hand side separately
C**********************************************************************C
      IREV = 0
      ISEQ=0
C---- check for =
      IFEQ=INDEX(STRING,'<=>')
      IF(IFEQ.GT.0) THEN
         IREV=1
         STRING(IFEQ:IFEQ) = '='
         STRING(IFEQ+1:ILAS) =STRING(IFEQ+3:ILAS)
      ENDIF
      IFEQ=INDEX(STRING,'=>')
      IF(IFEQ.GT.0) THEN
         STRING(IFEQ:IFEQ) = '>'
         STRING(IFEQ+1:ILAS) = STRING(IFEQ+2:ILAS)
      ENDIF
      IFEQ=INDEX(STRING,'=')
      IF(IFEQ.GT.0) THEN
         IREV=1
         ISEQ = IFEQ 
      ENDIF
      IFEQ=INDEX(STRING,'>')
      IF(IFEQ.GT.0) THEN
         ISEQ = IFEQ 
      ENDIF
         IFEQ = ISEQ
      ILAS = LEN(TRIM(STRING))
      SENSI = .FALSE.
      IF(STRING(ILAS:ILAS).EQ.'%') THEN
        SENSI =.TRUE.
        STRING(ILAS:ILAS)=' '
      ILAS = ILAS-1
      ENDIF
      IF(ISEQ.LE.0) THEN 
        WRITE(NFIOUT,*) ' No arrow in reaction '
        STOP
      ENDIf
C**********************************************************************C
C     get symbols and delimiters
C**********************************************************************C
      SYMSPE(1:12) = ' '
      STRINGR(1:LENSTR) = ' '
      INFSPE = 0 
      STRINGR(1:IFEQ-1) = STRING(1:IFEQ-1)
      LSTR =IFEQ-1       
C     WRITE(*,*) 'STRINGR',STRINGR
      CALL  REAINF(STRINGR,NSYMAX,
     1    NSYSPE,LENSPE,SYMSPE,INFSPE,NFIOUT)
      STRINGP(1:LENSTR) = ' '
      STRINGP(1:ILAS-IFEQ) = STRING(IFEQ+1:ILAS)
      LSTR = ILAS-IFEQ
C     WRITE(*,*) 'STRINGP',STRINGP
      CALL  REAINF(STRINGP,NSYMAX,
     1    NSYSPE,LENSPE,SYMSPE(7),INFSPE(7),NFIOUT)
      NSYSPE= 12
C**********************************************************************C
C     get symbols and delimiters
C**********************************************************************C
C***********************************************************************
      RETURN
C**********************************************************************C
C     get symbols and delimiters
C**********************************************************************C
 910  WRITE(*,*) ' RETNAM empty line'             
      IERR =1 
      RETURN
      END
      SUBROUTINE REAINF(STRING,NSYMAX,
     1    NSYSPE,LENSPE,SYMSPE,INFSPE,NFIOUT)
C**********************************************************************C
C
C     Pick species and delimiters
C
C
C     Input:
C       STRING (character)    : string of arbitrary length
C       LENSPE (INTEGER)      : numbers of characters in one name
C     Output:
C       NSYSPE                            : number of names found
C       SYMSPE (character*LENSPE(NSYSPE))): names
C       INFSPE 
C
C**********************************************************************C
      IMPLICIT NONE      
      INTEGER LENSPE,NSYSPE,IFI,ILAS,
     1      NFIOUT,LNUM,IPLU,NSYMAX
      PARAMETER (LNUM=20)
      INTEGER IBRA,IFINEW
      CHARACTER(LEN=*) STRING     
      CHARACTER(LEN=LENSPE) SYMSPE(NSYMAX)
      INTEGER, DIMENSION (NSYMAX) ::  INFSPE
      LOGICAL TBF
C**********************************************************************C
C     Initialize
C**********************************************************************C
      INFSPE(:) = 0
C**********************************************************************C
C     GET LENGTH OF STRING
C**********************************************************************C
      ILAS = LEN(TRIM(STRING))
C**********************************************************************C
C     Initialize
C**********************************************************************C
C----- if neither '(+' nor '+' is at the beginning, then add '+'
       IF(STRING(1:1).NE.'+'.AND.STRING(1:2).NE.'(+') THEN
         STRING(2:ILAS+1) = STRING(1:ILAS)
         STRING(1:1) = '+'                   
         ILAS = LEN(TRIM(STRING))
       ENDIF
C**********************************************************************C
C    
C**********************************************************************C
       IFI=1
      NSYSPE = 0
  10  CONTINUE
C**********************************************************************C
C     Check for sign 
C**********************************************************************C
      TBF = .FALSE.
C**********************************************************************C
C     Check for simple case ( '(+' at beginning)
C**********************************************************************C
      IF(STRING(IFI:IFI+1).EQ.'(+') THEN
        TBF = .TRUE.
        IBRA = INDEX(STRING(IFI:ILAS),')+')
          IFINEW=IBRA+1
        IF(IBRA.EQ.0) THEN
          IF(STRING(ILAS:ILAS).EQ.')') THEN
            IBRA=ILAS
            IFINEW=IBRA+1
          ELSE
            WRITE(NFIOUT,*) ' Brakets in reaction not balances '
            STOP
        ENDIF
      ENDIF
        NSYSPE= NSYSPE+1
        IF(NSYSPE.GT.NSYMAX) GOTO 965
        INFSPE(NSYSPE)=1
        SYMSPE(NSYSPE) = STRING(IFI+2:IBRA-1)
        IFI = IFINEW
C**********************************************************************C
C     Check for more complicated case '+' at beginning 
C**********************************************************************C
      ELSE IF(STRING(IFI:IFI).EQ.'+') THEN    
      IPLU = INDEX(STRING(IFI+1:ILAS),'+') 
      IF(IPLU.EQ.0) THEN
        IPLU = ILAS+1
      ELSE
        IPLU = IFI+IPLU
      ENDIF
C---- Check if this was already part of '(+'
      IF(IPLU.LT.ILAS.AND.STRING(IPLU-1:IPLU).EQ.'(+') THEN 
        IPLU= IPLU-1
      ELSE 
C---- Check if next character is '+' or '(+'
  20  CONTINUE
      IF(IPLU.EQ.ILAS) THEN
        IPLU=ILAS+1
      ELSE
C---- check  if further '+' is detected
      IF(IPLU.LT.ILAS.AND.STRING(IPLU+1:IPLU+1).EQ.'+') THEN 
        IPLU = IPLU+1
        GOTO 20
      ENDIF
      ENDIF
      IF(IPLU.LT.ILAS-1.AND.STRING(IPLU+1:IPLU+2).EQ.'(+') THEN 
        IPLU = IPLU+1
      ENDIF
C     WRITE(*,*) 'XXX',IFI,ILAS,IPLU,'IFI,ILAS,IPLU'
      ENDIF
      NSYSPE= NSYSPE+1
      IF(NSYSPE.GT.NSYMAX) GOTO 965
      SYMSPE(NSYSPE) = STRING(IFI+1:IPLU-1)
      IFI= IPLU
      ELSE
        write(*,*) 'FATAL ERRO IN MECH READ'
      ENDIF
      IF(IFI.LT.ILAS) GOTO 10
c     DO II=1,NSYSPE
c     WRITE(*,*) NSYSPE,SYMSPE(II),'      ',INFSPE(II)
c     ENDDO
      IF(NSYSPE.EQ.0) GOTO 960
      RETURN
C**********************************************************************C
C     END 
C**********************************************************************C
      RETURN
C**********************************************************************C
C     Error exits
C**********************************************************************C
 960  WRITE(*,961)
 961  FORMAT('1',5X,'No reactands or products in the reaction')
      GOTO 999
 965  WRITE(*,966)
 966  FORMAT('1',5X,'MORE THAN 6 reactands or products in reaction')
      GOTO 999
 999  STOP
      END
      INTEGER FUNCTION IFNB(LINE)
C*******************************************************************************
C
C     Get index of first non-blank character in a string
C
C*******************************************************************************
      IMPLICIT NONE
      CHARACTER*(*) LINE
      INTEGER ILEN
C*******************************************************************************
C     
C*******************************************************************************
      ILEN = 1
      DO WHILE ((LINE(ILEN:ILEN).EQ.' ').AND.(ILEN.LE.LEN(LINE)))
      ILEN = ILEN+1
      ENDDO
      IFNB =ILEN
      RETURN
C*******************************************************************************
C     
C*******************************************************************************
      END
      INTEGER FUNCTION IFDE(LINE,NFL,SFL)
C*******************************************************************************
C
C     Get index of first delimiter
C
C*******************************************************************************
      IMPLICIT NONE
      CHARACTER*(*) LINE
      CHARACTER*1  SFL(NFL)
      INTEGER I,L   
      INTEGER NFL
      LOGICAL DELI
C*******************************************************************************
C     
C*******************************************************************************
      IFDE = 0
      DO  I = 1,LEN(LINE)
      DELI=.FALSE.
      DO  L=1,NFL
      IF(LINE(I:I).EQ.' ')    DELI= .TRUE.
      IF(LINE(I:I).EQ.SFL(L)) DELI= .TRUE.
      ENDDO
      IF(DELI) THEN
        IFDE = I
        GOTO 10
      ENDIF
      ENDDO
  10  CONTINUE
      RETURN
C*******************************************************************************
C     
C*******************************************************************************
      END
      INTEGER FUNCTION ILDE(LINE,NFL,SFL)
C*******************************************************************************
C
C     Get index of last  delimiter
C
C*******************************************************************************
      IMPLICIT NONE
      CHARACTER*(*) LINE
      CHARACTER*1  SFL(NFL)
      INTEGER NFL
      INTEGER I,L
      LOGICAL DELI
C*******************************************************************************
C     
C*******************************************************************************
      ILDE = 1 
      DO  I = 1,LEN(LINE)
      DELI=.FALSE.
      DO  L=1,NFL
      IF(LINE(I:I).EQ.SFL(L)) DELI= .TRUE.
      IF(LINE(I:I).EQ.' ') DELI= .TRUE.
      ENDDO
      IF(.NOT.DELI) THEN
        ILDE = I-1
        GOTO 10
      ENDIF
      ENDDO
  10  CONTINUE 
      RETURN
C*******************************************************************************
C     
C*******************************************************************************
      END
      INTEGER FUNCTION LEXCM(LINE)
C*******************************************************************************
C
C     Get position of first !
C
C*******************************************************************************
      IMPLICIT NONE
      CHARACTER*(*) LINE
C*******************************************************************************
C     
C*******************************************************************************
      LEXCM=SCAN(LINE,'!') 
      RETURN
C*******************************************************************************
C     
C*******************************************************************************
      END
      INTEGER FUNCTION LSTRI(LINE)
C*******************************************************************************
C     Get index of last non-blank character of a string
C*******************************************************************************
      IMPLICIT NONE
      CHARACTER*(*) LINE
      INTEGER ILEN
      ILEN = LEN(LINE)
      DO WHILE ((LINE(ILEN:ILEN).EQ.' ').AND.(ILEN.GE.1))
      ILEN = ILEN-1
      ENDDO
      LSTRI = ILEN
      RETURN
      END
      SUBROUTINE AUXCHE(NFIINP,NFIOUT,LENKEY,LENVAL,SKEY,SVAL,
     1     NKMAX,NFOU)
C***********************************************************************
C
C
C***********************************************************************
C     STORAGE ORGANIZATION AND DEFINITION OF INITIAL VALUES            C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C     INTEGER IFNB,IFDE
      LOGICAL EQUSYM
      PARAMETER (LINLEN=255)
      CHARACTER(LEN=LINLEN) STRING
      CHARACTER(LEN=LENKEY) SKEY(*)     
      CHARACTER(LEN=LENVAL) SVAL(*)     
C**********************************************************************C
C     Initialization                                                   C
C**********************************************************************C
      NFOU  = 0
C**********************************************************************C
C     Input of the line and check for end                              C
C**********************************************************************C
      NFOU=0
  10  CONTINUE
      STRING=REPEAT(' ',LINLEN)
      READ(NFIINP,'(A)',END=900) STRING
      IF(EQUSYM(4,STRING,'END ')) THEN
        BACKSPACE NFIINP
        RETURN  
      ENDIF
C**********************************************************************C
C     Recognize keywords without parameters                            C
C**********************************************************************C
      IP1=INDEX(STRING,'XSML')
      IF(IP1.GT.0) THEN
        IP2=IP1-1+INDEX(STRING(IP1:LINLEN),' ')
        STRING=STRING(1:IP1+3)//'//'//STRING(IP2:LINLEN)
      ENDIF
      IP1=INDEX(STRING,'MOME')
      IF(IP1.GT.0) THEN
        IP2=IP1-1+INDEX(STRING(IP1:LINLEN),' ')
        STRING=STRING(1:IP1+3)//'//'//STRING(IP2:LINLEN)
      ENDIF
      IP1=INDEX(STRING,'DUP')
      IF(IP1.GT.0) THEN
        IP2=IP1-1+INDEX(STRING(IP1:LINLEN),' ')
        STRING=STRING(1:IP1+2)//'//'//STRING(IP2:LINLEN)
      ENDIF
C**********************************************************************C
C     Remove comments
C**********************************************************************C
      ILAS = LEN(STRING)
      IFC =  INDEX(STRING,'!')
      IF(IFC.EQ.1) GOTO 10
      IF(IFC.GT.0) STRING(IFC:ILAS) = REPEAT(' ',ILAS-IFC+1)
C**********************************************************************C
C     Check for /     
C**********************************************************************C
      NUDEL = SCAN(STRING,'/')
      IF(NUDEL.EQ.0) THEN
        BACKSPACE NFIINP
        RETURN
      ENDIF
C**********************************************************************C
C     get keywords and values
C**********************************************************************C
      IFI=1
  20  CONTINUE
      IDEL = INDEX(STRING(IFI:LINLEN),'/')
      IF(IDEL.EQ.0) GOTO 30
      NFOU = NFOU +1 
      IF(NFOU.GT.NKMAX) THEN
        WRITE(*,*) ' Number of keys (',NFOU,') > NKMAX (',NKMAX,')'
        STOP
      ENDIF
      SKEY(NFOU) = ADJUSTL(STRING(IFI:IFI-1+IDEL-1))
      IFI=IFI-1+IDEL+1
      IDEL = INDEX(STRING(IFI:LINLEN),'/')
      IF(IDEL.EQ.0) THEN
         WRITE(*,*) ' ODD NUMBER OF /  IN AUX DATA for reactions'
         STOP
      ENDIF
      SVAL(NFOU) = ADJUSTL(STRING(IFI:IFI-1+IDEL-1))
      IFI=IFI-1+IDEL+1
      GOTO 20 
  30  CONTINUE
      GOTO 10
C**********************************************************************C
C     PRINTOUT OF SPECIES SYMBOLS                                      C
C**********************************************************************C
      RETURN
 900  WRITE(NFIOUT,*) 'ERROR IN INPUT OF AUXILIARY LINE FOR MECH'
      STOP
C**********************************************************************C
C     END OF HOISYM                                                    C
C**********************************************************************C
      END
      SUBROUTINE AUXGET(NFIOUT,LENKEY,LENVAL,SKEY,SVAL,NFOU,
     1        NS,NSYMB,IENHANCE,XENHANCE)
C***********************************************************************
C
C
C***********************************************************************
C     STORAGE ORGANIZATION AND DEFINITION OF INITIAL VALUES            C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL EQUSYM
      CHARACTER(LEN=LENKEY) SKEY(*)     
      CHARACTER(LEN=LENVAL) SVAL(*)     
      CHARACTER(LEN=LENVAL+1) SYIN        
      CHARACTER(LEN=*) NSYMB(NS)
      DIMENSION XENHANCE(NS)
C**********************************************************************C
C     Initialization                                                   C
C**********************************************************************C
      LENCOM=MIN(LEN(NSYMB(1)),LEN(SKEY(1)))
C**********************************************************************C
C     Initialization                                                   C
C**********************************************************************C
      DO I=1,NFOU
          SYIN=SVAL(I)//'/'
C**********************************************************************C
C     Check for enhanced species                                       C
C**********************************************************************C
      DO J=1,NS
        IF(EQUSYM(LENCOM,ADJUSTL(SKEY(I)),NSYMB(J))) THEN 
          IENHANCE = 1
          READ(SYIN,*,ERR=900) ENHA 
          XENHANCE(J) = ENHA 
          GOTO 101
        ENDIF
      ENDDO
101   CONTINUE
C**********************************************************************C
C     END OF LOOP
C**********************************************************************C
      ENDDO
C**********************************************************************C
C     PRINTOUT OF SPECIES SYMBOLS                                      C
C**********************************************************************C
      RETURN
 900  WRITE(NFIOUT,*) 'ERROR IN ASSIGNMENT OF KEYWORD',SKEY(J)    
      STOP
C**********************************************************************C
C     END OF HOISYM                                                    C
C**********************************************************************C
      END
      SUBROUTINE AUXGE1(NFIOUT,LENKEY,LENVAL,SKEY,SVAL,NFOU,NS,NV,NSYSE,
     1                IVAL,XVAL)
C***********************************************************************
C
C
C***********************************************************************
C     STORAGE ORGANIZATION AND DEFINITION OF INITIAL VALUES            C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL EQUSYM
      CHARACTER(LEN=LENKEY) SKEY(*)     
      CHARACTER(LEN=LENVAL) SVAL(*)     
      CHARACTER(LEN=LENVAL+1) SYIN        
      CHARACTER(LEN=*) NSYSE(NS)
      DIMENSION XVAL(NS,NV),IVAL(NS)
C**********************************************************************C
C     Initialization                                                   C
C**********************************************************************C
      LENCOM=MIN(LEN(NSYSE,(1)),LEN(SKEY(1)))
C**********************************************************************C
C     Initialization                                                   C
C**********************************************************************C
      IVAL=0
      DO I=1,NFOU
          SYIN=SVAL(I)//'/'
C**********************************************************************C
C     Check for symbol
C**********************************************************************C
      DO J=1,NS
        IF(EQUSYM(LENCOM,ADJUSTL(SKEY(I)),NSYSE(J))) THEN 
          IVAL(J) = 1
          READ(SYIN,*,ERR=900) (XVAL(J,K),K=1,NV) 
          GOTO 101
        ENDIF
      ENDDO
101   CONTINUE
C**********************************************************************C
C     END OF LOOP
C**********************************************************************C
      ENDDO
C**********************************************************************C
C     PRINTOUT OF SPECIES SYMBOLS                                      C
C**********************************************************************C
      RETURN
 900  WRITE(NFIOUT,*) 'ERROR IN ASSIGNMENT OF KEYWORD',SKEY(J)    
      STOP
C**********************************************************************C
C     END OF HOISYM                                                    C
C**********************************************************************C
      END
      LOGICAL FUNCTION  AUXKEY(LENKEY,SKEY,NFOU,NSYSE)
C***********************************************************************
C
C
C***********************************************************************
C     STORAGE ORGANIZATION AND DEFINITION OF INITIAL VALUES            C
C**********************************************************************C
      LOGICAL EQUSYM
      CHARACTER(LEN=LENKEY) SKEY(*)     
      CHARACTER(LEN=*) NSYSE
C**********************************************************************C
C     Initialization                                                   C
C**********************************************************************C
      LENCOM=MIN(LEN(NSYSE,(1)),LEN(SKEY(1)))
C**********************************************************************C
C     Initialization                                                   C
C**********************************************************************C
      AUXKEY=.FALSE.
      DO I=1,NFOU
C**********************************************************************C
C     Check for enhanced species                                       C
C**********************************************************************C
        IF(EQUSYM(LENCOM,ADJUSTL(SKEY(I)),NSYSE)) THEN 
          AUXKEY=.TRUE.
        ENDIF
      ENDDO
      RETURN
C**********************************************************************C
C    
C**********************************************************************C
      END
      SUBROUTINE FORKEY(NFIOUT,LENKEY,SKEY,NFOU,NSYSE)
C***********************************************************************
C
C      Check for forbidden keywords
C
C***********************************************************************
C     STORAGE ORGANIZATION AND DEFINITION OF INITIAL VALUES            C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL EQUSYM
      CHARACTER(LEN=LENKEY) SKEY(*)     
      CHARACTER(LEN=*) NSYSE
C**********************************************************************C
C     Initialization                                                   C
C**********************************************************************C
      LENCOM=MIN(LEN(NSYSE,(1)),LEN(SKEY(1)))
C**********************************************************************C
C     Initialization                                                   C
C**********************************************************************C
      DO I=1,NFOU
        IF(EQUSYM(LENCOM,ADJUSTL(SKEY(I)),NSYSE)) THEN 
         WRITE(NFIOUT,*) 
     1       'Keyword ',NSYSE,' not allowed in auxiliary data'
         STOP
        ENDIF
      ENDDO
      RETURN
C**********************************************************************C
C    
C**********************************************************************C
      END
      SUBROUTINE STOCHE(LENKEY,SKEY,IVAL)      
C***********************************************************************
C
C      Check for numbers            
C
C***********************************************************************
C     STORAGE ORGANIZATION AND DEFINITION OF INITIAL VALUES            C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      CHARACTER(LEN=LENKEY) SKEY
C**********************************************************************C
C     check for numbers 1,2,3 as stoichiometric coefficients
C**********************************************************************C
      IVAL=1
      IF(SKEY(1:1).EQ.'1') THEN
        IVAL=1
        SKEY(1:LENKEY) = ADJUSTL(SKEY(2:LENKEY)//' ')
      ENDIF
      IF(SKEY(1:1).EQ.'2') THEN
        IVAL=2
        SKEY(1:LENKEY) = ADJUSTL(SKEY(2:LENKEY)//' ')
      ENDIF
      IF(SKEY(1:1).EQ.'3') THEN
        IVAL=3
        SKEY(1:LENKEY) = ADJUSTL(SKEY(2:LENKEY)//' ')
      ENDIF
      RETURN
C**********************************************************************C
C    
C**********************************************************************C
      END
      SUBROUTINE STOADJ(NFIOUT,LENINC,NUR,NSYR,INFSPE)      
C***********************************************************************
C
C
C***********************************************************************
C     STORAGE ORGANIZATION AND DEFINITION OF INITIAL VALUES            C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      CHARACTER(LEN=LENINC) NSYR(NUR)
      INTEGER, DIMENSION(NUR) :: INFSPE
C***********************************************************************
C     Loop over entries
C***********************************************************************
      ISTA = 0
      DO  WHILE (ISTA.LT.NUR-1)
      ISTA=ISTA+1
      CALL STOCHE(LENINC,NSYR(ISTA),IVAL)
C***********************************************************************
C     handle IVAL2    s
C***********************************************************************
      IF(IVAL.EQ.2) THEN
C---- check if shift is o.k
      IF(NSYR(NUR).EQ.REPEAT(' ',LENINC)) THEN
      NSYR(ISTA+2:NUR) = NSYR(ISTA+1:NUR-1)
      INFSPE(ISTA+2:NUR) = INFSPE(ISTA+1:NUR-1)
      NSYR(ISTA+1) = NSYR(ISTA)
      INFSPE(ISTA+1) = INFSPE(ISTA)
      ELSE 
         WRITE(NFIOUT,*) ' not enough space for stoech. >'
         STOP
      ENDIF
      ISTA = ISTA + 1
      ENDIF
C***********************************************************************
C     handle IVAL3 
C***********************************************************************
      IF(IVAL.EQ.3) THEN
C---- check if shift is o.k
      IF(NSYR(NUR  ).EQ.REPEAT(' ',LENINC).AND.
     1   NSYR(NUR-1).EQ.REPEAT(' ',LENINC)) THEN
      NSYR(ISTA+3:NUR) = NSYR(ISTA+1:NUR-2)
      INFSPE(ISTA+3:NUR) = INFSPE(ISTA+1:NUR-2)
      NSYR(ISTA+1) = NSYR(ISTA)
      INFSPE(ISTA+1) = INFSPE(ISTA)
      NSYR(ISTA+2) = NSYR(ISTA)
      INFSPE(ISTA+2) = INFSPE(ISTA)
      ELSE 
         WRITE(*,*) ' not enough space for stoech. >'
         STOP
      ENDIF
      ISTA = ISTA + 2
      ENDIF
      ENDDO
      RETURN
C**********************************************************************C
C    
C**********************************************************************C
      END
      SUBROUTINE PRIENH(NFIOUT,HEAD,LENSYM,NENT,NSYMB,ZER,VAL,FORM)      
C***********************************************************************
C
C
C***********************************************************************
C     STORAGE ORGANIZATION AND DEFINITION OF INITIAL VALUES            C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL PRI
      PARAMETER (LSTR=260,ISTOP=80)
      DIMENSION PRI(NENT),VAL(NENT)
      CHARACTER(LEN=20    ) SYMVAL(NENT)
      CHARACTER(LEN=*     ) HEAD
      CHARACTER(LEN=LENSYM) NSYMB(NENT)
      CHARACTER(LEN=LSTR) STRING      
      CHARACTER(LEN=*) FORM  
C***********************************************************************
C     Loop over entries
C***********************************************************************
      SYMVAL=' '
      PRI=.FALSE.
      DO  I=1,NENT 
      IF(VAL(I).NE.ZER) THEN
        PRI(I) = .TRUE.
        WRITE(SYMVAL(I),FORM) VAL(I)
      ENDIF
      ENDDO
C***********************************************************************
C     get longest string
C***********************************************************************
      ILONG=0
      DO I=1,NENT
      ILONG=MAX(LEN(TRIM(SYMVAL(I))),ILONG)
      ENDDO 
C***********************************************************************
C     Build up string   
C***********************************************************************
      STRING= ' '
      IFI = 1
      DO  I=1,NENT 
      IF(PRI(I)) THEN
      LENSY= LEN(TRIM(NSYMB(I)) )
      STRING(IFI:IFI+LENSY-1) = NSYMB(I)
      IADC =3
      STRING(IFI+LENSY:IFI+LENSY+ILONG-1+IADC) 
     1        ='='// SYMVAL(I)(1:ILONG)//', '
      IFI = IFI + LENSY+ ILONG +IADC
      ENDIF
      IF(((I.EQ.NENT).AND.(IFI.NE.1)) .OR. (IFI.GT.ISTOP)) THEN
         WRITE(NFIOUT,'(A,A)') TRIM(HEAD),TRIM(STRING) 
         STRING = ' '
         IFI = 1
      ENDIF
      ENDDO
      RETURN
C**********************************************************************C
C    
C**********************************************************************C
      END
