      SUBROUTINE PLOTOU(NUCALL,NOPL,NSC,KONS,IOUMED,LPLO,TSO,SOVER,
     1    NZHD,HEADER,
     1    NTS,X,LDX,IOX,XMIN,XMAX,XSYMB,XSCA,NF,Y,LDY,IOY,
     1    IEXT,YMIN,YMAX,YSYMB,YSCA)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *         PROGRAM FOR THE OUTPUT OF PLOTS                   *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C***********************************************************************
C
C  Arguments
C  =========
C
C  NUCALL  (input/output) INTEGER
C
C          If on input NUCALL = 0 the windows will be initialized
C          On output NUCALL = 0 if the windows have been closed
C                    NUCALL > 0 if the windows are still open
C
C  NCONS   (input) INTEGER
C          The number of Y-coordinates to be plotted
C
C  KONS    (input) INTEGER array, dimension (NCONS)
C          The indices of the Y-coordinates to be plotted
C          Only for graphics:
C             If there is a KONS, which is < 0, a two-Y plot will be
C             generated with all KONS > 0 on the left axis and
C             all KONS < 0 on the right axis
C
C  IOUMED  (input) INTEGER
C          If IOUMED > 0, IOUMED determines the filenumber for vt100
C          output
C          If IOUMED = 0, graphics will be directed to the screen
C          If IOUMED = -1 graphcs will be written on a Postscript file
C          output and an eventual interactive mouse control will be
C          disabled.
C
C  LPLO    (input) LOGICAL
C          logarithmic plots (only for vt100 output
C
C  TSO     (input) LOGICAL
C          For TSO = .TRUE., allow interactive handling
C             For graphics: allow mouse control (supressed if SOVER
C             is set to 'O'
C             For vt100 output TSO =.TRUE. sets the screen width to 80
C             characters (133 otherwise)
C                       use batch version otherwise
C
C  SOVER   (input) CHARACTER*1
C          SOVER  = 'O' add new curves to the old ones
C                       replot otherwise
C
C  NZHD    (input) INTEGER
C          Number of characters in Header
C
C  HEADER  (input) CHARACTER*(*)
C          Header for the legend
C
C  NTS     (input) INTEGER
C          The number of points to be plotted
C
C  X       (input) DOUBLE PRECISION array, dimension (LDX,*)
C          The X-coordinates of the points
C
C  LDX     (input) INTEGER
C          leading dimension of array XA
C
C  IOX     (input) INTEGER
C          if IOX = 1 then the loop over the points is assumed to
C          be over the first index of the array
C          if IOX = 2 then the loop over the points is assumed to
C          be over the second index of the array
C
C  XMIN    (input/output) DOUBLE PRECISION
C          Minimum value of the X-coordinates
C          If IEXT = 0 on input, a scaling will be done and the value
C          of XMIN will be returned
C
C  XMAX    (input/output) DOUBLE PRECISION
C          Maximum value of the X-coordinates
C          If IEXT = 0 on input, a scaling will be done and the value
C          of XMAX will be returned
C
C  XSYMB   (input) CHARACTER*(*)
C          The symbol of the X-coordinate
C
C  YA      (input) DOUBLE PRECISION array, dimension (LDY,*)
C          The Y-coordinates of the points
C
C  LDY     (input) INTEGER
C          leading dimension of array YA
C
C  IOY     (input) INTEGER
C          if IOY = 1 then the loop over the points is assumed to
C          be over the first index of the array
C          if IOY = 2 then the loop over the points is assumed to
C          be over the second index of the array
C
C  YMIN    (input/output) DOUBLE PRECISION array, dimension (*)
C          Minimum value of the Y-coordinates
C          If IEXT = 0 on input, and I is the index of a
C          species considered for plotting (see KONS), a scaling will
C          be done and the value of YMIN(I) will be corrected
C
C  YMAX    (input/output) DOUBLE PRECISION array, dimension (*)
C          Maximum value of the Y-coordinates
C          If IEXT = 0  on input, and I is the index of a
C          species considered for plotting (see KONS), a scaling will
C          be done and the value of YMAX(I) will be corrected
C
C  YSYMB   (input) CHARACTER*8 array, dimension (LDY)
C          The symbols of the Y-coordinates
C
C***********************************************************************
C
C
C
C***********************************************************************
C     STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      LOGICAL LPLO,TSO,STARS,EQUSYM
      CHARACTER(LEN=*)  XSYMB,YSYMB(NF)
      CHARACTER(*) HEADER
      CHARACTER*1  SIADD,SINTER,SPOST,SOVER
      DIMENSION X(LDX,*),Y(LDY,*),KONS(*),YMIN(NF),YMAX(NF)
      DIMENSION JPI(2)
      PARAMETER (ZERO=0.0D0,ONE=1.0D0)
      PARAMETER (LIWP=2000,LRWP=2000)
      REAL RWP(LRWP)
      DIMENSION IWP(LRWP)
C***********************************************************************
C     IF(IOUMED.GT.0) THEN
C***********************************************************************
C---- if filenumber > 0 then produce output
      IF(IOUMED.GT.0) THEN
        STARS = .TRUE.
        NFILE6 = IOUMED
      ELSE
        STARS = .FALSE.
        SPOST ='N'
      ENDIF
C---- allow interactive change of the figures
              SINTER = 'N'
      IF(TSO) SINTER = 'I'
C---- correct options for postscript output
      IF(IOUMED.EQ.-1) THEN
        SPOST  = 'P'
        SINTER = 'N'
      ENDIF
C---- allow successive adding of curves
                            SIADD = 'N'
      IF(EQUSYM(1,SOVER,'O')) SIADD = 'A'
C---- do not allow interactive plotting for overlay plot
      IF(EQUSYM(1,SOVER,'O')) SINTER = 'N'
C=======================================================================
C     MiniGraphik
C-----------------------------------------------------------------------
C     This part is by default DISABLED.
C     To enable: "./configure --with-mg" and then "make all"
C     A.Koksharov
C-----------------------------------------------------------------------
#ifdef MINI
C***********************************************************************
C     DECIDE ABOUT TASK
C***********************************************************************
      IF(NSC.EQ.0) GOTO 100
C***********************************************************************
C     CALL OF PLTSIM
C***********************************************************************
      IF(STARS) THEN
              NDRA =  2
      IF(TSO) NDRA = -2
      DO 10 J=1,NSC,2
C---- REGULAR INDICES
        JPI(1) = ABS(KONS(J))
C---- LAST DATASET
      IF(J.GE.NSC) THEN
        NDRA=1
        JPI(2) = JPI(1)
      ELSE
        JPI(2) = ABS(KONS(J+1))
      ENDIF
C---- LOGARITHMIC PLOT
      IF(LPLO) THEN
        JPI(1)=-JPI(1)
        JPI(2)=-JPI(2)
      ENDIF
      IF(MOD(J,4).EQ.1) WRITE(NFILE6,82)
      NSEND=0
      CALL PLTSIM(NDRA,JPI(1),JPI(2),NFILE6,NSEND,
     1   NTS,X,LDX,IOX,XMIN,XMAX,XSYMB,XSCA,NF,Y,LDY,IOY,
     1   IEXT,YMIN,YMAX,YSYMB,YSCA)
   10 CONTINUE
      WRITE(NFILE6,81)

      ENDIF
C***********************************************************************
C     REGULAR EXIT
C***********************************************************************
      IF(.NOT.STARS) THEN
      IF(IEXT.EQ.0) THEN
      XMIN = ZERO
      XMAX = ZERO
      DO 180 I = 1,NF
      YMIN(I) = ZERO
      YMAX(I) = ZERO
  180 CONTINUE

      ENDIF
CC Rainer Stauch 16.04.04: needed for MiniGraphik
!      CALL MG2DPL(NUCALL,NOPL,SIADD,SINTER,'S',SPOST,NZHD,HEADER,
!     1     NTS,NF,X,LDX,IOX,XSYMB,Y,LDY,IOY,YSYMB,
!     1     NSC,KONS,XMIN,XMAX,YMIN,YMAX,LIWP,IWP,LRWP,RWP)
      ENDIF
CC***********************************************************************
CC     REGULAR EXIT
CC***********************************************************************
  100 CONTINUE
#endif
C-----------------------------------------------------------------------
C     MiniGraphik
C=======================================================================
      NUCALL = NUCALL +1
      RETURN
C***********************************************************************
C     FORMAT STATEMENTS
C***********************************************************************
   82 FORMAT('1',1X)
   81 FORMAT(' ',1X)
C***********************************************************************
C     END OF -HOROUP-
C***********************************************************************
      END
      SUBROUTINE PLTSIM(NPIC,N1,N2,NFILE6,NSEND,
     1   NP,X,LDX,IOX,XMIN,XMAX,XSYMB,XSCA,NF,Y,LDY,IOY,
     1   IEXT,YMIN,YMAX,YSYMB,YSCA)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *            PLOTTING ON TERMINAL OR PRINTER                *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C
C***********************************************************************
C     STORAGE ORGANISATION AND INITIAL VALUES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION X(LDX,*),Y(LDY,*),YMIN(NF),YMAX(NF)
      CHARACTER(LEN=*) XSYMB,YSYMB(NF)
      DIMENSION XU(6),FMI(2),FMA(2),ORDINT(2),YU(2,10)
      CHARACTER*1 WR(2,26,51)
      LOGICAL ONE,LPLO(2)
      DATA NLIN/21/,NCOL/51/,XLARGE/1.D30/
C***********************************************************************
C     initialize
C***********************************************************************
      NDRA=IABS(NPIC)
                                 ONE     = .FALSE.
      IF(NPIC.LT.0.OR.NDRA.EQ.1) ONE     = .TRUE.
                                 LPLO(1) = .FALSE.
                     IF(N1.LT.0) LPLO(1) = .TRUE.
                                 LPLO(2) = .FALSE.
                     IF(N2.LT.0) LPLO(2) = .TRUE.
                                 N1      = IABS(N1)
                                 N2      = IABS(N2)
C***********************************************************************
C
C***********************************************************************
      IF(IOX.NE.1.AND.IOX.NE.2) THEN
         WRITE(6,*) '  wrong IOX in HORPL2'
         STOP
      ENDIF
      IF(IOY.NE.1.AND.IOY.NE.2) THEN
         WRITE(6,*) '  wrong IOY in HORPL2'
         STOP
      ENDIF
C***********************************************************************
C     DETERMINE MAXIMUM/MINIMUM OF X- VALUES
C***********************************************************************
      IF(IEXT.EQ.0) THEN
        XMA=-XLARGE
        XMI= XLARGE
        DO 10 I=1,NP
      IF(IOX.EQ.1) THEN
          XMA=DMAX1(X(I,1),XMA)
          XMI=DMIN1(X(I,1),XMI)
      ELSE
          XMA=DMAX1(X(1,I),XMA)
          XMI=DMIN1(X(1,I),XMI)
      ENDIF
   10   CONTINUE
      ELSE
        XMA = XMAX
        XMI = XMIN
      ENDIF
C***********************************************************************
C     DETERMINE MAXIMUM/MINIMUM OF Y- VALUES
C***********************************************************************
      DO 30 K=1,NDRA
      IF(K.EQ.1) NDES=N1
      IF(K.EQ.2) NDES=N2
      IF(IEXT.EQ.0) THEN
        FMA(K)= -XLARGE
        FMI(K)=  XLARGE
        DO 20 I=1,NP
      IF(IOY.EQ.1) THEN
          FMA(K)=DMAX1(Y(I,NDES),FMA(K))
          FMI(K)=DMIN1(Y(I,NDES),FMI(K))
      ELSE
          FMA(K)=DMAX1(Y(NDES,I),FMA(K))
          FMI(K)=DMIN1(Y(NDES,I),FMI(K))
      ENDIF
   20   CONTINUE
      ELSE
        FMA(K)=YMAX(NDES)
        FMI(K)=YMIN(NDES)
      ENDIF
      IF(LPLO(K)) FMI(K)=DLOG10(DMAX1(1.D-16,FMI(K)))
      IF(LPLO(K)) FMA(K)=DLOG10(FMA(K))
C---- SPECIAL ARRANGEMENT FOR HOMRUN
      IF(DABS(FMI(K)).LT.1.D-15) FMI(K)=0.0D0
      FMA(K)=DMAX1(FMA(K),FMI(K)+1.D-13)
C     IF(NSEND.EQ.0) FMI(K)=0.0D0
      IF(FMI(K).GT.0.D0) FMI(K)=0.D0
C
      ORDINT(K)=(FMA(K)-FMI(K))/FLOAT(NLIN-1)
   30 CONTINUE
      ABSINT=(XMA-XMI)/FLOAT(NCOL-1)
      DO 40 I=1,NCOL,10
      K=(I-1)/10+1
   40 XU(K)=XMI+FLOAT(I-1)*ABSINT
C***********************************************************************
C
C     DETERMINE LEGEND AND SYMBOL MATRIX
C
C***********************************************************************
      DO 150 NN=1,NDRA
      IF(NN.EQ.1) NDES=N1
      IF(NN.EQ.2) NDES=N2
C***********************************************************************
C     DETERMINE LEGEND
C***********************************************************************
      DO 105 I=1,NLIN
      L=(I-1)/5 + 1
      IF(MOD(I-1,5).EQ.0) THEN
        XH = FLOAT(I-1)/FLOAT(NLIN-1)
        YH = FMA(NN)-FMI(NN)
        YU(NN,L)=FMA(NN)-XH*YH
        IF((YU(NN,L)-FMI(NN)).LT.(1.0D-3*YH))  YU(NN,L)=FMI(NN)
      ENDIF
  105 CONTINUE
C***********************************************************************
C     SET SYMBOLS EQUAL TO BLANKS
C***********************************************************************
      DO 110 I=1,NLIN
      DO 110 K=1,NCOL
  110 WR(NN,I,K)=' '
C***********************************************************************
C     SET STARS
C***********************************************************************
      DO 120 NS=1,NP
      IF(IOX.EQ.1) THEN
      XXTT = X(NS,1)
      ELSE
      XXTT = X(1,NS)
      ENDIF
      NPX=1+ IDINT((XXTT-XMI)/ABSINT+0.5D0)
      IF(IOY.EQ.1) THEN
      YYTT = Y(NS,NDES)
      ELSE
      YYTT = Y(NDES,NS)
      ENDIF
      IF(LPLO(NN)) THEN
        NPY=1+ IDINT((FMA(NN)-DLOG10(DMAX1(YYTT,1.D-50)))
     1         /ORDINT(NN)+0.5D0)
      ELSE
        NPY=1+ IDINT((FMA(NN)-YYTT)/ORDINT(NN)+0.5D0)
      ENDIF
      IF(NPY.LE.NLIN.AND.NPY.GE.1.AND.NPX.GE.1.AND.NPX.LE.NCOL) THEN
        WR(NN,NPY,NPX)='*'
      ELSE
        CONTINUE
      ENDIF
  120 CONTINUE
  150 CONTINUE
C***********************************************************************
C
C     OUTPUT FOR ONE PICTURE AFTER THE OTHER
C
C***********************************************************************
      IF(ONE) THEN
      DO 300 NN=1,NDRA
        IF(NN.EQ.1) NDES=N1
        IF(NN.EQ.2) NDES=N2
        WRITE(NFILE6,811) YSYMB(NDES)
        WRITE(NFILE6,800)
        WRITE(NFILE6,812)
      DO 310 I=1,NLIN
      L=(I-1)/5 + 1
      IF(MOD(I-1,5).GT.0) THEN
      WRITE(NFILE6,814)           (WR(NN,I,K),K=1,NCOL)
      ELSE
      WRITE(NFILE6,813)  YSCA*YU(NN,L),(WR(NN,I,K),K=1,NCOL)
      ENDIF
  310 CONTINUE
      WRITE(NFILE6,812)
      WRITE(NFILE6,815) (XSCA*XU(I),I=1,6),XSYMB
        WRITE(NFILE6,800)
  300 CONTINUE
C***********************************************************************
C
C     OUTPUT FOR TWO PICTURES BESIDES EACH OTHER
C
C***********************************************************************
      ELSE
        WRITE(NFILE6,821) YSYMB(N1),YSYMB(N2)
        WRITE(NFILE6,800)
        WRITE(NFILE6,822)
      DO 400 I=1,NLIN
      L=(I-1)/5 + 1
      IF(MOD(I-1,5).GT.0) THEN
        WRITE(NFILE6,824)  ((WR(II,I,K),K=1,NCOL),II=1,2)
      ELSE
        WRITE(NFILE6,823)  (YSCA*YU(II,L),(WR(II,I,K),K=1,NCOL),II=1,2)
      ENDIF
  400 CONTINUE
      WRITE(NFILE6,822)
      WRITE(NFILE6,825)(XSCA*XU(I),I=1,6),XSYMB,(XSCA*XU(I),I=1,6)
      ENDIF
      RETURN
C***********************************************************************
C     FORMAT STATEMENTS
C***********************************************************************
  800 FORMAT(' ',2X)
  811 FORMAT('0',21X,'            ',A8)
  821 FORMAT('0',21X,'            ',A8,48X,'            ',A8)
  812 FORMAT(' ',9X,' I+',5(10H---------+))
  822 FORMAT(' ',9X,' I+',5(10H---------+),15X,' I+',5(10H---------+))
  813 FORMAT(' ',1PE9.2,' I',51A1)
  823 FORMAT(' ',1PE9.2,' I',51A1,6X,1PE9.2,' I',51A1)
  814 FORMAT(' ',9X,' I',51A1)
  824 FORMAT(' ',9X,' I',51A1,15X,' I',51A1)
  815 FORMAT(' ',6X,6(1X,1PE9.2),2X,A8)
  825 FORMAT(' ',4X,6(2X,1PE8.1),1X,A8,1X,5(1PE8.1,2X),1PE8.1)
C***********************************************************************
C     END OF -PLOTS2-
C***********************************************************************
      END
      SUBROUTINE NUMANA(X,XM,IE,SIG,CHAR,LCH)
C     IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      REAL X,XM
      CHARACTER*1 CHAR(*)
      CHARACTER*1 SIG,SEX
      PARAMETER (NSB=3,ZERO=0.0,ONE=1.0,TEN=10.0,TENI=0.1)
      CHARACTER*1 ZIF(0:9)
      DIMENSION  NUMZIF(NSB),NUMEXP(2)
      DATA ZIF/'0','1','2','3','4','5','6','7','8','9'/
C***********************************************************************
C     Determine Sign
C***********************************************************************
      IF(X.GE.ZERO) THEN
        SIG=' '
      ELSE
        SIG='-'
      ENDIF
C***********************************************************************
C     Determine absolute value
C***********************************************************************
      XABS = ABS(X)
      ROUND = 5.1
C     XABS  = XABS *(ONE + ROUND*TEN**(-NSB-1))
C***********************************************************************
C     Determine Exponent
C***********************************************************************
      IF(XABS.NE.0) THEN
        XE = LOG10(XABS)
        IE = XE
      ELSE
        XE = 0.0
        IE = 0
      ENDIF
C***********************************************************************
C     Determine Mantisse
C***********************************************************************
      XM = XABS / 10.0**IE
C***********************************************************************
C     Clean up special cases
C***********************************************************************
      IF(XE.NE.0.0) THEN
      ROUND = 0.51
      XM = XM + ROUND*TEN**(-NSB)
      IF(XM.GE.TEN) THEN
        XM = XM / TEN
        IE = IE + 1
      ENDIF
      IF(XM.LT.ONE) THEN
        XM = XM * TEN
        IE = IE - 1
      ENDIF
      ENDIF
C***********************************************************************
C     determine numbers for mantisse
C***********************************************************************
      XTEMP = XM
C     ROUND = 5.1
C     XTEMP = XM + ROUND*TEN**(-NSB)
      DO 110 I=1,NSB
C     IC = INT(XTEMP*(ONE+TEN**(1-NSB)))
      IC = INT(XTEMP)
      NUMZIF(I) = IC
c     write(6,*) xtemp,ic
      XTEMP = XTEMP - IC
c     write(6,*) 'xtemp-ic',xtemp
      XTEMP = XTEMP * TEN
  110 CONTINUE
C     write(6,88) (numzif(i),i=1,NSB)
   88 format(20i1)
C***********************************************************************
C     determine numbers for Exponent
C***********************************************************************
      IF(IE.GE.0) THEN
        SEX = '+'
      ELSE
        SEX = '-'
      ENDIF
      IEABS = ABS(IE)
      NUMEXP(1) = IEABS / 10
      NUMEXP(2) = IEABS - 10 * (IEABS/10)
C***********************************************************************
C     Compose numbers
C***********************************************************************
      CHAR(1) = SIG
      CHAR(2) = ZIF(NUMZIF(1))
      CHAR(3) = '.'
      DO 55 I=2,NSB
      CHAR(2+I) = ZIF(NUMZIF(I))
   55 CONTINUE
      CHAR(2+NSB+1) = 'E'
      CHAR(2+NSB+2) = SEX
      CHAR(2+NSB+3) = ZIF(NUMEXP(1))
      CHAR(2+NSB+4) = ZIF(NUMEXP(2))
      LCH = 2+NSB+4
C***********************************************************************
C     Sucessful Exit
C***********************************************************************
      RETURN
      END
