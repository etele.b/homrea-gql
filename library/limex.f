      SUBROUTINE XLIMEX(N,NZC,NZV,LSC,FCN,JAC,T,Y,YP,TEND,RTOL,ATOL,
     1  RTOLS,ATOLS,HMAX,H,IJOB,NRW,RW,NIW,IW,ICALL,RPAR,IPAR,LOUTin)
C***********************************************************************
C                                                                      *
C     ************************************************************     *
C     *                                                          *     *
C     *      EXTRAPOLATION INTEGRATOR FOR LINEARLY IMPLICIT      *     *
C     *               DIFFERENTIAL EQUATION SYSTEMS              *     *
C     *                                                          *     *
C     ************************************************************     *
C                                                                      *
C                                                                      *
C                                                                      *
C                                                                      *
C                                                                      *
C                                                                      *
C                                                                      *
C     Note: this is a version that has been changed considerably       *
C           by me (U. Maas)                                            *
C           In case of errors or malfunction you should blame          *
C           me first.                                                  *
C                                                                      *
C                                                                      *
C                                                                      *
C                                                                      *
C                                                                      *
C                                                                      *
C                                                                      *
C     APPLICABLE TO SYSTEMS OF THE FORM:                               *
C                                                                      *
C      (*)    B(T,Y) * Y' (T) = F (T,Y)                                *
C                                                                      *
C                             B(T,Y) = BV(T,Y) + BC                    *
C                                                                      *
C            BV:(N,N)-MATRIX DEPENDING ON Y AND T                      *
C            BC:(N,N)-MATRIX, CONSTANT                                 *
C                             Y0 GIVEN, T0 < T <TEND                   *
C                                                                      *
C  EQUATION (*) REPRESENTS AN ODE, IF B IS NONSINGULAR, AND A          *
C  DIFFERENTIAL-ALGEBRAIC EQUATION (DAE), IF B IS SINGULAR.            *
C  LIMEX USES AN ITERATIVE REALIZATION OF THE SEMI-IMPLICIT EULER-     *
C  METHOD (EULSIM) FOR THE DISCRETIZATION OF (*).                      *
C***********************************************************************
C                                                                      *
C REFERENCES:                                                          *
C -----------                                                          *
C                                                                      *
C /1/ P. DEUFLHARD:                                                    *
C     RECENT PROGRESS IN EXTRAPOLATION METHODS FOR ORDINARY            *
C     DIFFERENTIAL EQUATIONS                                           *
C     UNIVERSITY OF HEIDELBERG, SFB 123, TECH. REP. 224 (1983)         *
C                                                                      *
C /2/ P. DEUFLHARD, E. HAIRER, J. ZUGCK:                               *
C     ONE-STEP AND EXTRAPOLATION METHODS FOR DIFFERENTIAL-ALGEBRAIC    *
C     SYSTEMS                                                          *
C     UNIVERSITY OF HEIDELBERG, SFB 123, TECH. REP. *** (1985)         *
C                                                                      *
C***********************************************************************
C                                                                      *
C  EXTERNAL SUBROUTINES (TO BE SUPPLIED BY THE USER):                  *
C  --------------------------------------------------                  *
C                                                                      *
C    FCN (N,NZV,T,Y,RHS,BV,IRV,ICV,RPAR,IPAR)                          *
C              RIGHT-HAND-SIDE F(T,Y) OF THE SYSTEM, INCLUDING         *
C              VARIABLE PART BV OF LEFT-HAND-SIDE MATRIX B(T,Y)        *
C              (SPECIFY BV(1),IRV(1),ICV(1) IF NO VARIABLE ENTRIES     *
C              OCCUR IN THE LEFT-HAND-SIDE MATRIX B(T,Y)               *
C                                                                      *
C    LSC (NZC,BC,IRC,ICC)                                              *
C              CONSTANT PART BC OF LEFT-HAND-SIDE MATRIX B(T,Y)        *
C              (NON-ZERO ENTRIES ONLY)                                 *
C              (PASS A DUMMY-NAME, IF THERE ARE NO CONSTANT ENTRIES    *
C              IN THE LEFT-HAND-SIDE MATRIX B(T,Y) )                   *
C                                                                      *
C    JAC (N,M,T,Y,A)                                                   *
C              ANALYTIC JACOBIAN OF THE RIGHT-HAND-SIDE FUNCTION       *
C              G(T,Y)= F(Y,T) - BV(Y,T) * YP(T)                        *
C              (PASS A DUMMY-NAME, IF NUMERICAL DIFFERENCE             *
C              APPROXIMATION OF THE JACOBIAN IS WANTED)                *
C              A(M,N) ARE ENTRIES OF THE JACOBIAN A AT T,Y,            *
C              M IS THE NUMBER OF ROWS OF A ( M = N IN THE DENSE       *
C              MATRIX CASE, M < N IN THE BAND MATRIX CASE, SEE         *
C              NOTE 2 BELOW )                                          *
C                                                                      *
C                                                                      *
C      T              ACTUAL VALUE OF INDEPENDENT VARIABLE             *
C      Y(N)           VALUES OF DEPENDENT VARIABLES AT T               *
C      N              DIMENSION OF THE SYSTEM                          *
C      BV(NZV)        ENTRIES OF VARIABLE PART BV OF LEFT-HAND-SIDE    *
C                     MATRIX B(T,Y) (SPARSE MODE STORAGE SCHEME, SEE   *
C                     NOTE 1 BELOW)                                    *
C      IRV(NDV)       INTEGER ARRAY CONTAINING ROW-INDICES OF BV       *
C      ICV(NDV)       INTEGER ARRAY CONTAINING COLUMN-INDICES OF BV    *
C      NZV            NUMBER OF ENTRIES OF BV                          *
C      BC(NZC)        ENTRIES OF CONSTANT PART BC OF LEFT-HAND-SIDE    *
C                     MATRIX B(T,Y) (SPARSE MODE STORAGE SCHEME, SEE   *
C                     NOTE 1 BELOW)                                    *
C      IRC(NZC)       INTEGER ARRAY CONTAINING ROW-INDICES OF BC       *
C      ICC(NZC)       INTEGER ARRAY CONTAINING COLUMN-INDICES OF BC    *
C      NZC            NUMBER OF ENTRIES OF BC                          *
C***********************************************************************
C                                                                      *
C  INPUT PARAMETERS (* MARKS TRANSIENT PARAMETERS):                    *
C  ------------------------------------------------                    *
C                                                                      *
C  * T               STARTING POINT OF INTEGRATION                     *
C  * Y(N)            INITIAL VALUES Y(1),...,Y(N)                      *
C    TEND            PRESCRIBED FINAL POINT OF INTEGRATION             *
C    RTOL            PRESCRIBED RELATIVE PRECISION (.GT.0)             *
C    ATOL            PRESCRIBED ABSOLUTE PRECISION (.GT.0)             *
C    HMAX            MAXIM.  PERMITTED STEPSIZE                        *
C  * H               INITIAL STEPSIZE GUESS                            *
C                    (FOR H=0 AN INITIAL STEPSIZE GUESS IS             *
C                    INTERNALLY GENERATED)                             *
C    IJOB            INTEGER VECTOR, CONTROLLING THE EXECUTION OF      *
C                    THE JOB                                           *
C   * IJOB(1)        =0: B IS KNOWN TO BE OR MIGHT BE SINGULAR         *
C                    =1: B IS KNOWN TO BE NONSINGULAR                  *
C     IJOB(2)        =0: NUMERICAL DIFFERENCE-APPROXIMATION OF THE     *
C                        JACOBIAN OF THE RIGHT-HAND-SIDE F(T,Y)        *
C                        INTERNALLY GENERATED                          *
C                    =1: ANALYTIC JACOBIAN SUPPLIED BY THE USER        *
C     IJOB(3)        INFORMATION ABOUT ITERATION MATRIX B-H*A          *
C                         ( A = JACOBIAN, H = STEPSIZE )               *
C                                                                      *
C                       IJOB(3)=0  DENSE MATRIX                        *
C                       IJOB(3)=1  BANDED MATRIX                       *
C                       IJOB(3)=2  BLOCK TRIDIAGONAL MATRIX            *
C                       IJOB(3)=3  BLOCK PENTADIAGONAL MATRIX          *
C                       IJOB(3)=4  BLOCK NONADIAGONAL MATRIX           *
C                       IJOB(3)=5  BLOCK TRIDIAGONAL + CORNERS         *
C                                                                      *
C       IF IJOB(3)=1 IJOB(4) = LOWER BAND WIDTH (EXCLUDING DIAGONAL)   *
C                    IJOB(5) = UPPER BAND WIDTH (EXCLUDING DIAGONAL)   *
C       IF IJOB(3)=2 IJOB(4) = DIMENSION OF SUBBLOCKS                  *
C                    IJOB(5) = NUMBER OF SUBBLOCKS                     *
C       IF IJOB(3)=3 IJOB(4) = DIMENSION OF SUBSUBBLOCK                *
C                    IJOB(5) = NUMBER OF SUBSUBBLOCKS                  *
C                    IJOB(6) = NUMBER OF SUBBLOCKS                     *
C       IF IJOB(3)=4 IJOB(4) = DIMENSION OF SUBSUBBLOCK                *
C                    IJOB(5) = NUMBER OF SUBSUBBLOCKS                  *
C                    IJOB(6) = NUMBER OF SUBBLOCKS                     *
C                                                                      *
C   * IJOB(7)           PERFORMANCE STATISTICS
C                       =0: NO OUTPUT
C                       =1: STANDARD OUTPUT
C                       =2: ADDITIONALLY INTEGRATION MONITOR
C                       =3: ADDITIONALLY ENHANCED INFORMATION
C                       =4: ADDITIONALLY ITERATION MONITOR ( OF ITERA-
C                           TIVE REALIZATION OF DISCRETIZATION )
C                       =5: ADDITIONALLY INITIAL AND SOLUTION VALUES
C                       =6: ADDITIONALLY SOLUTION VALUES AT INTERMEDIATE
C                           POINTS CHOSEN BY LIMEX
C--- - NOT YET REALIZED
C     IJOB(19)          INFORMATION ABOUT ERROR TOLERANCES
C                       =0: RTOL AND ATOL ARE SCALARS
C                       =1: RTOL IS A SCALAR, ATOL IS AN ARRAY
C                       =2: RTOL IS AN ARRAY, ATOL IS A SCALAR
C                       =3: RTOL AND ATOL ARE ARRAYS
C
C    NRW                DIMENSION OF REAL WORK-SPACE,
C                       TO BE CHOSEN .GE. :
C
C                       (MBH+MB+JM+9)*N+2*NDV+NZC+2*JM*JM+JM
C
C                         MBH = N                  , IF B-H*A IS DENSE,
C                             = 2*IJOB(4)+IJOB(5)+1, IF B-H*A IS BANDED,
C                         MB  = N                  , IF B-H*A IS DENSE
C                             =   IJOB(4)+IJOB(5)+1, IF B-H*A IS BANDED,
C                         JM  = 5                  , IF IJOB(1).EQ.0
C                             = 7                  , IF IJOB(1).EQ.1
C
C    RW                 REAL WORK-SPACE
C    NIW                DIMENSION OF INTEGER WORK-SPACE,
C                       TO BE CHOSEN .GE. :
C
C                       N+2*(NDV+NZC+JM)+KM
C
C                         KM  = JM-1
C
C    IW                 INTEGER WORK-SPACE
C
C ==================================================================
C ===  NOTE 1 : EXAMPLE FOR SPARSE MODE STORAGE SCHEME IN LIMEX  ===
C ==================================================================
C
C                   CONSIDER THE FOLLOWING CASE:
C
C                               |0     0    0  |
C                    B(T,Y)=    |1     1    0  |
C                               |T+1  -T   Y(2)|
C
C    WITH NZV=3 AND NZC=2 STORAGE OF BV AND BC IS DONE BY THE FOLLOWING
C    LINES:
C
C                   BV (1) = T+1.D0
C                   IRV(1) = 3
C                   ICV(1) = 1
C                   BV (2) = Y(2)
C                   IRV(2) = 3
C                   ICV(2) = 3
C                   BV (3) = -T
C                   IRV(3) = 3
C                   ICV(3) = 2
C            C
C                   BC (1) = 1.D0
C                   IRC(1) = 2
C                   ICC(1) = 1
C                   BC (2) = 1.D0
C                   IRC(2) = 2
C                   ICC(2) = 2
C
C        (THE ORDER OF THE ENTRIES IN BV AND BC IS ARBITRARY)
C
C ===========================================================
C ===  NOTE 2 : STORAGE OF USER SUPPLIED BANDED JACOBIAN  ===
C ===========================================================
C
C  IN THE BAND MATRIX CASE, THE FOLLOWING LINES MAY BUILD UP THE
C  ANALYTIC JACOBIAN A;
C  HERE AFL DENOTES THE QUADRATIC MATRIX A IN DENSE FORM, AND ABD THE
C  RECTANGULAR MATRIX A IN BANDED FORM :
C
C                   ML = IJOB(4)
C                   MU = IJOB(5)
C                   MH = MU+1
C                   DO 20 J = 1,N
C                   I1 = MAX0(1,J-MU)
C                   I2 = MIN0(N,J+ML)
C                   DO 10 I = I1,I2
C                   K = I-J+MH
C                   ABD(K,J) = AFL(I,J)
C           10      CONTINUE
C           20      CONTINUE
C
C           THE TOTAL NUMBER OF ROWS NEEDED IN  ABD  IS  ML+MU+1 .
C           THE  MU BY MU  UPPER LEFT TRIANGLE AND THE
C           ML BY ML  LOWER RIGHT TRIANGLE ARE NOT REFERENCED.
C
C ===========================================================
C
C
C  OUTPUT PARAMETERS:
C  ------------------
C
C    T                  ACTUAL FINAL POINT OF INTEGRATION
C    Y(N)               FINAL VALUES AT T
C    H                  STEPSIZE PROPOSAL FOR NEXT INTEGRATION STEP
C                       (H.EQ.0. ,IF LIMEX FAILS TO PROCEED)
C    IJOB(7)    .GE. 0: SUCCESSFUL INTEGRATION
C                       (IJOB(7) NOT ALTERED INTERNALLY)
C               .LT. 0: ERROR CODE AFTER A FAIL RUN OF LIMEX
C                  =-1: MORE THAN JRMAX STEPSIZE REDUCTIONS
C                       OCCURRED PER BASIC INTEGRATION STEP
C                  =-2: MORE THAN NSTMAX BASIC INTEGRATION STEPS PER
C                       INTERVAL HAVE BEEN PERFORMED
C                  =-3: STEPSIZE PROPOSAL FOR NEXT BASIC INTEGRATION
C                       STEP WAS TOO SMALL
C                  =-4: MATRIX PENCIL B-H*A IS SINGULAR:
C                       NO OR INFINITELY MANY SOLUTIONS EXIST
C                  =-5: ITERATIVE REALIZATION OF DISCRETIZATION FAILED
C                       TO SUCCEED
C                  =-6: NILPOTENCY OF THE SYSTEM IS GREATER THAN
C                       ONE
C                  =-7: INITIAL VALUES ARE INCONSISTENT OR NILPOTENCY
C                       OF THE SYSTEM IS GREATER THAN ONE
C                  =-8: REAL OR INTEGER WORK-SPACE IS EXHAUSTED
C                  =-9: THE GIVEN PROBLEM IS AN ALGEBRAIC EQUATION
C                       (LIMEX IS NOT SUITABLE IN THIS CASE)
C
C  OUTPUT AFTER A SUCCESSFULLY COMPLETED TASK ONLY:
C  ------------------------------------------------
C
C    IJOB( 8)      =    NUMBER OF PERFORMED INTEGRATION STEPS
C    IJOB( 9)      =    NUMBER OF FCN-EVALUATIONS
C    IJOB(10)      =    NUMBER OF GAUSSIAN DECOMPOSITIONS
C    IJOB(11)      =    NUMBER OF FORWARD-BACKWARD-SUBSTITUTIONS
C
C - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C
C  THE NUMERICAL SOLUTION OF THE ARISING LINEAR EQUATIONS IS DONE BY
C  MEANS OF THE SUBROUTINE LINSOL
C
C_______________________________________________________________________
C         THIS IS A DRIVER ROUTINE FOR THE CORE-INTEGRATOR LIMEX1
C_______________________________________________________________________
C
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      SAVE
C-DIMENSION
      DIMENSION Y(*),YP(*),RW(NRW),ATOL(*),RTOL(*)
      DIMENSION IW(NIW),IJOB(20)
C-DIMENSION
      DIMENSION RPAR(*),IPAR(*)
C
      EXTERNAL LSC,FCN,JAC
      optional loutin
C
      COMMON/UNIT/LOUT
      COMMON/MACHIN/EPMACH,SMALL
      COMMON/PARAM/NSTMAX,JRMAX,ISMAX
      COMMON/LIMLOC/
     1      JM,KM,NDV,NDC,NUMP,NDP,NGES,NDI,NDJ,III,
     2      I1 ,I2 ,I3 ,I4 ,I5 ,I6 ,I7 ,I8 ,I9 ,I10,
     3      I11,I12,I13,I14,I15,I16,I17,I18,I19,I20,
     3      I21,I22,I23,I24,I25,I26,I27,I28,I29,I30,
     2      N1 ,N2 ,N3 ,N4 ,N5 ,N6 ,N7 ,N8 ,N9 ,N10,
     3      N11,N12,N13,N14,N15,N16,N17,N18,N19,N20,
     3      N21,N22,N23,N24,N25,N26,N27,N28,N29,N30,
     3      IMAT(10)
C***********************************************************************
C                                                                      *
C     CHAPTER II.:INITIALIZATION                                       *
C                                                                      *
C***********************************************************************
C'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
C     SET UP SOME MACHINE DEPENDENT VALUES                             !
C.......................................................................
C
C  OUTPUT UNIT FOR INTEGRATION MONITOR
C
      LOUT=LOUTin
C
C  10 * RELATIVE MACHINE PRECISION   (ADAPTED TO IBM 3090 D)
C
      EPMACH=1.D-15
C
C SQUARE-ROOT OF SMALLEST POSITIVE MACHINE NUMBER (ADAPTED TO IBM 3090)
C
      SMALL=1.D-35
C'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
C   STANDARD VALUES, TO BE ALTERED, IF NECESSARY, BY THE SKILLFULL USER)
C.......................................................................
C
C  MAXIMUM PERMITTED NUMBER OF BASIC INTEGRATION STEPS PER INTERVAL
C
      NSTMAX=4000
C
C  MAXIMUM PERMITTED NUMBER OF STEPSIZE REDUCTIONS PER BASIC STEP
C  (DUE TO EXTRAPOLATION-TABLEAU)
C
      JRMAX=20
C
C  MAXIMUM PERMITTED NUMBER OF STEPSIZE REDUCTIONS PER BASIC STEP
C  (DUE TO ZERO PIVOT IN GAUSSIAN DECOMPOSITION)
C
      ISMAX=5
C'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
C   FOR CONTINUATION CALLS OVERJUMP NEXT BLOCK
C.......................................................................
      IF(ICALL.GE.2) GOTO 1234
C---- TIME ROUTINE
C     CALL TIMER(0,CPUTIM)
C***********************************************************************
C                                                                      *
C     CHAPTER III.:BUILD UP POINTERS AND WORKING ARRAYS                *
C                                                                      *
C***********************************************************************
C
C  PRESCRIBED MAXIMUM ROW NUMBER OF EXTRAPOLATION-TABLEAU
C
      JM=7
      IF(IJOB(1).EQ.1) GOTO 10
      JM=5
C
C  PRESCRIBED MAXIMUM COLUMN NUMBER OF EXTRAPOLATION-TABLEAU
C
   10 KM=JM-1
C***********************************************************************
C     CHECK TYPE OF LEFT-HAND-SIDE
C***********************************************************************
      NDV=MAX(NZV,1)
      NDC=MAX(NZC,1)
C---- ERROR EXIT FOR ALGEBRAIC EQUATION SYSTEM
      IF(NZV+NZC.EQ.0) GOTO 920
C***********************************************************************
C     COMPUTE NUMBERS FOR SENSITIVITY ANALYSIS
C***********************************************************************
      NUMP=IJOB(20)
      IF(NUMP.GT.0.AND.IJOB(3).GT.1) GOTO 950
      NDP=MAX(NUMP,1)
      NGES=N*(NUMP+1)
  30  CONTINUE
C---- NDJ DENOTES NUMBER OF ENTRIES IN JACOBIAN
C---- NDI DENOTES NUMBER OF ENTRIES IN ITERATION MATRIX (INCLUDING  WA)
C'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
C     ARRAYS FOR DENSE JACOBIAN
C.......................................................................
      IF(IJOB(3).EQ.0) THEN
C---- COMPUTE NUMBERS
      NDI=N*N
      NDJ=N*N
C---- STORE NUMBERS IN IMAT
      IMAT(1)=0
      IMAT(2)=N
      IMAT(3)=N
      IMAT(4)=N
      IMAT(5)=N
      IMAT(7)=NDJ
      IMAT(8)=NDI
      ENDIF
C'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
C     ARRAYS FOR BANDED JACOBIAN
C.......................................................................
      IF(IJOB(3).EQ.1) THEN
C---- COMPUTE NUMBERS
      ML=IJOB(4)
      MU=IJOB(5)
      NDJ=N*(ML+MU+1)
      MBJ=ML+MU+1
      MBH=2*ML+MU+1
      NDI=N*MBH
C---- STORE NUMBERS IN IMAT
      IMAT(1)=1
      IMAT(2)=N
      IMAT(3)=ML
      IMAT(4)=MU
      IMAT(5)=MBH
      IMAT(6)=MBJ
      IMAT(7)=NDJ
      IMAT(8)=NDI
      ENDIF
C'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
C     ARRAYS FOR BLOCK TRIDIAGONAL JACOBIAN
C.......................................................................
      IF(IJOB(3).EQ.2) THEN
C---- COMPUTE NUMBERS
      ND0=IJOB(4)
      ND1=IJOB(5)
      NDJ=  3*ND1*ND0*ND0
      NDI=  3*ND1*ND0*ND0
C---- STORE NUMBERS IN IMAT
      IMAT(1)=2
      IMAT(2)=N
      IMAT(3)=ND0
      IMAT(4)=ND1
      IMAT(5)=N
      IMAT(7)=NDJ
      IMAT(8)=NDI
      ENDIF
C'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
C     ARRAYS FOR BLOCK PENTA DIAGONAL JACOBIAN
C.......................................................................
      IF(IJOB(3).EQ.3) THEN
C---- COMPUTE NUMBERS
      ND0=IJOB(4)
      ND1=IJOB(5)
      ND2=IJOB(6)
      ND2M=ND2-1
      IF(ND2.EQ.1) ND2M=1
      NDJ = ND0*ND0* (3*ND1*ND2 + 2*ND1*(ND2M))
      NDI = ND0*ND0* (3*ND1*ND2 + 2*ND1*(ND2M))
C---- STORE NUMBERS IN IMAT
      IMAT(1)=3
      IMAT(2)=N
      IMAT(3)=ND0
      IMAT(4)=ND1
      IMAT(5)=ND2
      IMAT(7)=NDJ
      IMAT(8)=NDI
      ENDIF
C'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
C     ARRAYS FOR BLOCK NONA DIAGONAL JACOBIAN
C.......................................................................
      IF(IJOB(3).EQ.4) THEN
C---- COMPUTE NUMBERS
        ND0  = IJOB(4)
        ND1  = IJOB(5)
        ND2  = IJOB(6)
        ND2M = ND2-1
        IF(ND2.EQ.1) ND2M=1
        NDJ = ND0*ND0* (3*ND1*ND2 + 3*2*ND1*ND2M)
        NDI = ND0*ND0* (3*ND1*ND2 + 3*2*ND1*ND2M)
C---- STORE NUMBERS IN IMAT
        IMAT(1) = 4
        IMAT(2) = N
        IMAT(3) = ND0
        IMAT(4) = ND1
        IMAT(5) = ND2
        IMAT(7) = NDJ
        IMAT(8) = NDI
      ENDIF
C'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
C     ARRAYS FOR BLOCK TRIDIAGONAL JACOBIAN + CORNERS
C.......................................................................
      IF(IJOB(3).EQ.5) THEN
C---- COMPUTE NUMBERS
      ND0=IJOB(4)
      ND1=IJOB(5)
      NDJ=  (3*ND1+6)*ND0*ND0
      NDI=  (3*ND1+6)*ND0*ND0
C---- STORE NUMBERS IN IMAT
      IMAT(1)=5
      IMAT(2)=N
      IMAT(3)=ND0
      IMAT(4)=ND1
      IMAT(5)=N
      IMAT(7)=NDJ
      IMAT(8)=NDI
      ENDIF
C---- COMPUTE HELP VARIABLES
      NJM  =  N*JM
      JMJM = JM*JM
C'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
C     COMPUTE POSITIONS IN INTEGER WORK ARRAYS
C.......................................................................
      I1  =      1
      I2  = I1 + N
      I3  = I2 + JM
      I4  = I3 + JM
      I5  = I4 + KM
      I6  = I5 + NDC
      I7  = I6 + NDC
      I8  = I7 + NDV
      I9  = I8 + NDV
      I10 = I9 + NDP
C---- INDEX OF LAST INTEGER ELEMENT
      I11 = I10+ N
      MINIW=I11-1
      LEFTIW=NIW-MINIW
C'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
C     COMPUTE POSITIONS IN REAL WORK ARRAYS
C.......................................................................
      N1  =      1
      N2  = N1 + NDI
      N3  = N2 + NDJ
      N4  = N3 + NDC
      N5  = N4 + NDV
      N6  = N5 + NDV
      N7  = N6 + NJM +NUMP*NJM
      N8  = N7 + NJM
      N9  = N8 + JMJM
      N10 = N9 + JMJM
      N11 = N10+ JM
      N12 = N11+ N + NUMP*N
      N13 = N12+ N
      N14 = N13+ N + NUMP*N
      N15 = N14+ N + NUMP*N
      N16 = N15+ N
      N17 = N16+ N
      N18 = N17+ N
      N19 = N18+ N
      N20 = N19+ N
C---- INDEX OF LAST REAL*8 ELEMENT
      N21 = N20+ N
      MINRW=N21-1
      LEFTRW=NRW-MINRW
C***********************************************************************
C     CHECK FOR SUFFICIENT WORK AREA
C***********************************************************************
      IF(IJOB(7).GT.0) WRITE(LOUT,810) MINRW,MINIW
      IF(MINRW.GE.NRW .OR. MINIW.GE.NIW) GOTO 910
C***********************************************************************
C     CALL OF CORE INTEGRATOR LIMEX1
C***********************************************************************
1234  CALL LIMEX1(N,NDJ,NDI,NZC,NDC,NZV,NDV,JM,KM,LSC,FCN,JAC,T,Y,YP,
     1 TEND,NUMP,NDP,NGES,RTOL,ATOL,RTOLS,ATOLS,HMAX,H,IJOB,ICALL,IMAT,
     1         IW(I1),IW(I2),IW(I3),IW(I4),IW(I5),IW(I6),
     2  IW(I7),IW(I8),IW(I9),IW(I10),RW(N1),RW(N2),RW(N3),RW(N4),RW(N5),
     3         RW(N6),RW(N7),RW(N8),RW(N9),RW(N10),RW(N11),RW(N12),
     4         RW(N13),RW(N14),RW(N15),RW(N16),RW(N17),RW(N18),RW(N19),
     5         RW(N20),RPAR,IPAR,LEFTIW,IW(I10),LEFTRW,RW(N21))
C***********************************************************************
C     OUTPUT OF STATISTICS AND PREPARATION OF RETURN (REGULAR EXIT)
C***********************************************************************
C
C     CALL TIMER(1,TIME)
      TIME = 0
      IF(IJOB(7).GT.0) WRITE(LOUT,820) TIME,IJOB(8),IJOB(9),IJOB(12),
     1                               IJOB(10),IJOB(11)
      IF(IJOB(7).LT.0) GOTO 999
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
C---- WORK SPACE EXHAUSTED
  910 CONTINUE
      IJOB(7)=-8
      IF(MINIW.GT.NIW) WRITE(LOUT,911) NIW,MINIW
  911 FORMAT(1X,' +++++ INTEGER WORK-SPACE EXHAUSTED +++++',/,
     1       1X,' GIVEN SPACE:',I8,'     NEEDED SPACE:',I8)
      IF(MINRW.GT.NRW) WRITE(LOUT,912) NRW,MINRW
  912 FORMAT(1X,' +++++ REAL WORK-SPACE EXHAUSTED +++++',/,
     1       1X,' GIVEN SPACE:',I8,'     NEEDED SPACE:',I8)
      GOTO 999
C---- GIVEN PROBLEM IS AN ALGEBRAIC EQUATION SYSTEM
  920 CONTINUE
      IJOB(7)=-9
      WRITE(LOUT,921)
  921 FORMAT('  PROBLEM IS AN ALGEBRAIC EQUATION, LIMEX NOT SUITABLE',/)
      GOTO 999
C---- GIVEN PROBLEM IS AN ALGEBRAIC EQUATION SYSTEM
  950 CONTINUE
      IJOB(7)=-11
      WRITE(LOUT,951)
  951 FORMAT('  SUBROUTINE RHSVG NOT YET DESIGNED TO HANDEL OTHER ',/,
     1       '  THAN DENSE MATRIX                                 ',/)
      GOTO 999
C---- INTEGRATION FAILED
  999 CONTINUE
      WRITE(LOUT,998) IJOB(7)
  998 FORMAT(/1X,' ### ERROR CODE FROM LIMEX :',I3,' ###',/)
      RETURN
C***********************************************************************
C     FORMAT STATEMENTS
C***********************************************************************
  810 FORMAT(/1X,' **********************************',/,
     1        1X,' *  MINIMAL REQUIRED WORK-SPACE:  *',/,
     2        1X,' *    REAL ARRAY    RW(',I8,')  *',/,
     3        1X,' *    INTEGER ARRAY IW(',I8,')  *',/,
     4        1X,' **********************************',/ )
  820 FORMAT(/1X,' **********  STATISTICS  **********',/,
     1        1X,' ****    TIME      :',F6.2,5X,'****',/,
     2        1X,' ****    STEPS     :',I6,'     ****',/,
     3        1X,' ****    F.-EV     :',I6,'     ****',/,
     4        1X,' ****    F.-EV(J)  :',I6,'     ****',/,
     5        1X,' ****    DECOMP.   :',I6,'     ****',/,
     5        1X,' ****    SUBST.    :',I6,'     ****',/,
     6        1X,' **********************************',/)
C***********************************************************************
C     END OF DRIVER ROUTINE ULIMEX
C***********************************************************************
      END
      SUBROUTINE LIMEX1(N,NDJ,NDI,NZC,NDC,NZV,NDV,JM,KM,LSC,FCN,JAC,
     1                  T,Y,YP,TEND,
     1      NUMP,NDP,NGES,RTOL,ATOL,RTOLS,ATOLS,HMAX,H,IJOB,ICALL,IMAT,
     2                  IPIVOT,NJ,INCR,NRED,IRC,
     2                  ICC,IRV,ICV,KONVQ,IFCNP,B,A,BC,BV0,BVK,DT,DTP,
     3                  D,AL,AJ,YM,YPM,DEL,
     3                  DZ,SM,ETA,W1,W2,W3,W4,RPAR,IPAR,
     1                  LIW,IWORK,LRW,RWORK)
C***********************************************************************
C
C      CORE INTEGRATOR FOR DIFFERENTIAL/ALGEBRAIC EQUATION SYSTEMS
C
C***********************************************************************
C***********************************************************************
C
C     STORAGE ORGANIZATION
C
C***********************************************************************
C***********************************************************************
C     TYPES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      SAVE
      LOGICAL QDM,QDMA,QMY,LDIR,INEW,JGOOD,DUMP
      EXTERNAL LSC,FCN,JAC
C***********************************************************************
C     DIMENSIONS
C***********************************************************************
      DIMENSION Y(NGES),YP(N)
      DIMENSION RWORK(LRW),IWORK(LIW)
      DIMENSION BC(NDC),IRC(NDC),ICC(NDC),
     1           BVK(NDV),BV0(NDV),IRV(NDV),ICV(NDV),
     1          YM(NGES),DEL(NGES),DZ(NGES),
     1          W1(N),W2(N),W3(N),W4(N),YPM(N),SM(N),ETA(N),
     1          A(NDJ),B(NDI),
     1          DT(NGES,JM),DTP(N,JM),D(JM,JM),AL(JM,JM),
     2          AJ(JM)
      DIMENSION IPIVOT(N),NJ(JM),INCR(JM),NRED(KM),IJOB(20),IMAT(10)
      DIMENSION KONVQ(NDP),IFCNP(N)
      DIMENSION RPAR(*),IPAR(*),ATOL(*),RTOL(*)
C***********************************************************************
C     COMMON BLOCKS
C***********************************************************************
      COMMON/ITERA/NSOL
      COMMON/UNIT/LOUT
      COMMON/MACHIN/EPMACH,SMALL
      COMMON/PARAM/NSTMAX,JRMAX,ISMAX
C***********************************************************************
C     DATA STATEMENTS
C***********************************************************************
      DATA EPMIN/1.D-10/,ETADIF/1.D-6/,EX1/0.6D0/,EX2/1.5D0/,FMIN/1.D-2/
     1     ,ONE/1.D0/,ONE1/1.01D0/,QUART/0.25D0/,RMIN/0.9D0/,
     2     RO/0.25D0/,SAFE/0.5D0/,SAFEIN/2.D-2/,TEN/1.D1/,THREE/3.D0/,
     3     THRESH/1.D-1/,ZERO/0.D0/,TWO/2.D0/,SAFEDM/.8D0/
C***********************************************************************
C                                                                      *
C     INITIAL PREPARATIONS                                             *
C                                                                      *
C***********************************************************************
      DUMP=.FALSE.
      INEW=.TRUE.
      ITOPT =4
CAAAA   THE FOLLOWING VALUES ARE NOT ALTERED IF CONTINUATION IS PERFORME
      EPH    = RO*RTOL(1)
      FN=FLOAT(N)
      TOLN   = RTOL(1)**2*FLOAT(N)
      EPKAP  = 0.03D0
      EPDIFF=DSQRT(EPMACH)
CBBBB
C---- NEW FOR VARIABLE TEND
      H1=TEND-T
C----
      IF(ICALL.NE.2) JGOOD=.FALSE.
C                    JGOOD=.FALSE.
C***********************************************************************
C***********************************************************************
C     INITIALIZE FOR NEW PROBLEM
C***********************************************************************
C***********************************************************************
      HMAX=DABS(HMAX)
      HMAXU=HMAX
      IF(ICALL.GE.2) GOTO 1000
      DO 1823 I=1,NDI
 1823 B(I) = 0.D0
      JOB=0
      OMJO=ZERO
C***********************************************************************
C     INITIALIZE INFORMATIONS VECTOR FOR PRINTING
C***********************************************************************
C     IJOB(8) = NSTEP
      IJOB(8)= 0
C     IJOB(9) = NFCN
      IJOB(9) = 0
C     IJOB(10) = NDEC
      IJOB(10)= 0
C     IJOB(11) = NSOL
      IJOB(11)= 0
C     IJOB(12) = NFCNJ
      IJOB(12)= 0
C***********************************************************************
C     SET VECTOR FOR PERTUBATIONS ETA IN JACOBIAN EVALUATION
C***********************************************************************
          DO 10 I=1,N
             ETA(I)=ETADIF
  10      CONTINUE
C***********************************************************************
C     ASSIGN INITIAL VALUES FOR YP IF THIS IS THE FIRST CALL AT ALL
C***********************************************************************
      IF(ICALL.EQ.0) THEN
          DO 20 I=1,N
             YP(I)=ZERO
   20     CONTINUE
      ENDIF
C***********************************************************************
C     CALL LSC      (ONLY NEEDED FOR NEW START (ICALL.LT.2)
C***********************************************************************
      IF(NZC.NE.0) CALL LSC (NZC,BC,IRC,ICC)
C***********************************************************************
C     SET NJ,DT,DTP (ONLY FOR NEW START (ICALL.LT.2)
C***********************************************************************
      DO 210 J=1,JM
      NJ(J)=J
      DO 205 I=1,NGES
      DT(I,J) =0.D0
  205 CONTINUE
      DO 210 I=1,N
      DTP(I,J)=0.D0
  210 CONTINUE
C***********************************************************************
C           SET PARAMETERS FOR EXTRAPOLATION AND ORDER-CONTROL
C***********************************************************************
      AJ(1)=FLOAT(NJ(1))
      DO 240 J=2,JM
      J1=J-1
      INCR(J1)=0
      NRED(J1)=0
      FJ=FLOAT(NJ(J))
      V=AJ(J1)+FJ-ONE
      AJ(J)=V
      DO 220 K=1,J1
      W=FJ/FLOAT(NJ(K))
 220  D(J,K)=W
      IF(J.EQ.2) GOTO 240
      W=V-FLOAT(NJ(1))
      DO 230 K1=2,J1
      K=K1-1
      U=(AJ(K1)-V)/(W*FLOAT(K1))
CTEST U=(AJ(K1)-V)/(W*FLOAT(K1+K))
CTEST HIER DRUEBER
      U=EPH**U
  230 AL(J1,K)=U
  240 CONTINUE
C***********************************************************************
C  EVALUATION OF COST COEFFICIENTS
C***********************************************************************
C---- COST FOR ONE CALL OF RESLIM
      COSTF  = ONE
C---- COST FOR ONE DECOMPOSITION
      COSTLR = ZERO
C---- COST FOR ONE BACKWARD/FOREWARD SUBSTITUTION
      COSTS  = ZERO
C---- COST FOR COMPUTATION OF JACOBIAN
      IF(IMAT(1).EQ.0) COSTJ = COSTF * FN
      IF(IMAT(1).EQ.1) COSTJ = COSTF * FLOAT(IMAT(3)+IMAT(4)+1)
      IF(IMAT(1).EQ.2) COSTJ = COSTF * FLOAT(IMAT(3)) * THREE
      IF(IMAT(1).EQ.3) COSTJ = COSTF * FLOAT(IMAT(3)) * 10.0D0
      IF(IMAT(1).EQ.4) COSTJ = COSTF * FLOAT(IMAT(3)) * 10.0D0
      IF(IMAT(1).EQ.5) COSTJ = COSTF * FLOAT(IMAT(3)) * 7.77D0
C----
      AJ(1)=COSTJ+COSTLR+(COSTF+COSTS)*FLOAT(NJ(1))
      DO 310 J=2,JM
      J1=J-1
      AJ(J)=AJ(J1)+ COSTF * FLOAT(NJ(J)-1)
     1            + COSTS * FLOAT(NJ(J))
     2            + COSTLR
  310 CONTINUE
C***********************************************************************
C           SET PARAMETERS FOR EXTRAPOLATION AND ORDER-CONTROL
C***********************************************************************
  320 KOH=1
      JOH=2
  330 CONTINUE
      IF(JOH.GE.JM) GOTO 340
      IF(AJ(JOH+1)*ONE1.GT.AJ(JOH)*AL(JOH,KOH)) GOTO 340
      KOH=JOH
      JOH=JOH+1
      GOTO 330
  340 K=0
      KMH=KOH
      JMH=JOH
      INCR(JMH)=-1
C---- SAVE ORDER PARAMETERS IN IJOB ARRAY
      IJOB(13) = KMH
      IJOB(14) = JMH
      IJOB(15) = KOH
      IJOB(16) = JOH
C***********************************************************************
C           INITIAL PRINTOUT FOR START OR RE-START
C***********************************************************************
      IF(IJOB(7).GT.0) WRITE(LOUT,8010) RTOL(1),ATOL(1),KMH
      IF(IJOB(7).GE.5) WRITE(LOUT,8020) (Y(I),I=1,N)
      IF(IJOB(7).GE.3) WRITE(LOUT,8030)
      IF(IJOB(7).EQ.2) WRITE(LOUT,8040)
C***********************************************************************
C                                                                      *
C     BASIC INTEGRATION STEP
C                                                                      *
C***********************************************************************
1000  CONTINUE
      NSTEP = IJOB(8)
      NFCN  = IJOB(9)
      NDEC  = IJOB(10)
      NSOL  = IJOB(11)
      NFCNJ = IJOB(12)
C---- STORE BACK ORDER PARAMETERS
      KMH = IJOB(13)
      JMH = IJOB(14)
      KOH = IJOB(15)
      JOH = IJOB(16)
C---- OLD VALUE WAS 1.D-10
CUlri
      SRTOL=   1.D-8
      SATOL=   1.D-10
      IF(DABS(H1).LE.DABS(T)*EPMACH*TEN) GOTO 7000
      IF(DABS(H1).LE.EPMACH*DABS(H))     GOTO 7000
      IF(IJOB(7).EQ.2) WRITE(LOUT,8110)  NSTEP,T
      IF(IJOB(7).GE.3) WRITE(LOUT,8120)  NSTEP,NFCN,T,H,K,KOH
      IF(IJOB(7).GE.6.AND.NSTEP.GT.0)    WRITE(LOUT,8130) (Y(I),I=1,N)
      IF(DABS(H1).GE.ONE1*DABS(H))       GOTO 1010
      HR=H
      H=H1
1010  JRED=0
      ISING=0
      IT=0
      DMH=SAFEDM
      DO 1020 K=1,KMH
1020  INCR(K)=INCR(K)+1
      HMAX=DABS(H1)
      IF(HMAXU.LT.HMAX) HMAX=HMAXU
      IF(NUMP.LE.0) GOTO 1040
      DO 1030 K=1,NUMP
1030  KONVQ(K)=0
1040  CONTINUE
      KONVQS=0
C***********************************************************************
C     CALCULATE SCALING FACTORS
C***********************************************************************
      IF(.NOT.JGOOD) THEN
C---- IF JACOBIAN HAS TO BE EVALUATED, THEN JUST ONLY COMPUTE SCALING F.
      CALL SCASET(N,IJOB(19),ATOL,Y,SM)
      ELSE
C---- IF OLD JACOBIAN IS USED, THEN COMPUTE SM AND THE RATIO OF NEW TO
C     OLD SCALING FACTORS. BE SURE NOT TO CHANGE W4 UNTIL CALL OF JACOB
      DO 1050 I=1,N
      W4(I) =  SM(I)
 1050 CONTINUE
      CALL SCASET(N,IJOB(19),ATOL,Y,SM)
      DO 1060 I=1,N
      W4(I) = SM(I)/W4(I)
 1060 CONTINUE
      ENDIF
      GOTO 1090
C---- SCALING FOR OLD PROBLEM
C1070 CONTINUE
C     DO 1080 I=1,N
C     SM(I) = DMAX1(SM(I),Y(I))
C1080 CONTINUE
C     GOTO 1090
C---- EXIT FOR ALL SCALINGS
 1090 CONTINUE
C***********************************************************************
C     CALCULATE RESIDUAL (UNPERTURBED)
C***********************************************************************
      IFCN=0
C XXXXXXXXXXX
C---- IFCNP(NB1,NB2) = 0
      DO 1105 I=1,N
      IFCNP(I) = 0
 1105 CONTINUE
      CALL RESLIM (N,NZV,NDV,T,Y,DZ,YM,BV0,IRV,ICV,YP,FCN,RPAR,IPAR,
     1             IFCN,IFCNP)
      IF(IFCN.EQ.9) THEN
        IJOB(7) = -11
        WRITE(LOUT,*) ' Initial values lead to error in user',
     1              ' supplied subroutine FCN'
        IFCN = 0
        RETURN
      ENDIF
       NFCN=NFCN+1
C     IF(IJOB(7).GE.0)THEN
C         OVN = OVNORM(N,DZ  )
C         WVN = WVNORM(N,DZ,SM)
C         WRITE(LOUT,8041) OVN,WVN
C8041 FORMAT(/,1X,' NORM OF RESIDUALS   OVN=',1PE10.3,
C    1            '    WVN=',1PE10.3)
C     ENDIF
C***********************************************************************
C     CALCULATE RESIDUAL (UNPERTURBED) OF SENSITIVITY EQUATION
C***********************************************************************
      IF(NUMP.GE.1.AND.KONVQS.NE.NUMP)
     1   CALL RHSVG(N,NZV,NDV,NGES,NUMP,NDP,T,Y,DZ,W1,W2,BV0,IRV,ICV,
     1            SM,ETA,KONVQ,FCN,IMAT,RPAR,IPAR,
     1            IFCN,IFCNP)
C***********************************************************************
C                                                                      *
C     CALCULATION OF THE JACOBIAN (SCALED)                             *
C                                                                      *
C***********************************************************************
      IANA=IJOB(2)
      IF(JGOOD) THEN
      CALL SCAJAC(IXIDUM,IMAT,N,W4,NDJ,A)
      ELSE
         CALL JACOB(IANA,IMAT,FCN,JAC,N,T,Y,YP,YM,
     1        SM,ETADIF,ETA,EPDIFF,EPMIN,FMIN,
     1        DEL,W1,W2,W3,NZV,NDV,BVK,IRV,ICV,
     2        NFCNJ,NDJ,A,RPAR,IPAR,
     1            IFCN,IFCNP)
      IF(IFCN.EQ.9) THEN
        IJOB(7) = -11
        WRITE(LOUT,*) ' Values lead to error in user',
     1              ' supplied subroutine FCN during computation',
     1              ' of the Jacobian'
         IFCN=0
        RETURN
      ENDIF
         IFCN=0
      ENDIF
C***********************************************************************
C  SCALE RESIDUAL
C***********************************************************************
      DO 1410 K=1,N
1410  DZ(K)=DZ(K)/SM(K)
C***********************************************************************
C  SCALE RESIDUAL
C***********************************************************************
      IF(NUMP.LE.0) GOTO 1450
      DO 1440 KK=1,NUMP
      IF(KONVQ(KK).GE.1) GOTO 1440
      L=KK*N
      DO 1430 I=1,N
      DZ(L+I)=DZ(L+I)/SM(I)
 1430 CONTINUE
 1440 CONTINUE
 1450 CONTINUE
C***********************************************************************
C  INITIAL STEPSIZE GUESS, IF H=ZERO
C***********************************************************************
      IF(ICALL.GT.0.OR.H.NE.ZERO) GOTO 1500
      CALL MATNOR(3,IMAT,N,NDJ,A,ANORM1,NDUMMY)
      IF(ANORM1.EQ.ZERO)ANORM1=ONE
      H=SAFEIN/ANORM1
      IF(H.GT.H1)H=H1
C***********************************************************************
C     SET TIME STEP
C***********************************************************************
1500  TN=T+H
                     LDIR=.FALSE.
      IF(ITOPT.LE.0) LDIR=.TRUE.
      FCM=DMAX1(DABS(H)/HMAX,FMIN)
      FCMY=FCM
      DM=DMH*TWO
      QDM=.FALSE.
      DELT0=0.D0
C***********************************************************************
C     SET INFORMATIONS VECTOR TO ZERO
C***********************************************************************
      IF(NUMP.LE.0) GOTO 1520
      DO 1510 KK=1,NUMP
 1510 KONVQ(KK)=0
 1520 CONTINUE
C***********************************************************************
C
C                     DISCRETIZATION
C
C***********************************************************************
C NEW IMAT(9)= 5
      IMAT(9)= 0
C     IF(IMAT(1).EQ.5) IMAT(9)=4
      DO 4999 J=1,JMH
      IF(IJOB(7).GE.4) WRITE(LOUT,8220) J
      IEST=0
      IF(J.EQ.2) IEST=1
      M=NJ(J)
      G=H/FLOAT(M)
C***********************************************************************
C     COMPUTE  B0-G*A
C***********************************************************************
      CALL ITMAT(IMAT,N,G,NDJ,A,SM,
     1        NZV,NDV,BV0,IRV,ICV,NZC,NDC,BC,IRC,ICC,NDI,B)
C***********************************************************************
C  SEMI-IMPLICIT EULER STARTING STEP
C***********************************************************************
      DO 2110 I=1,N
      YM(I)=Y(I)
2110  DEL(I)=G*DZ(I)
C---- COMPUTE G*DZ FOR SENSITIVITY EQUATIONS
      IF(NUMP.LE.0) GOTO 2150
      DO 2140 KK=1,NUMP
      IF(KONVQ(KK).GE.1) GOTO 2140
      L=KK*N
      DO 2130 I=1,N
      YM(L+I)=Y(L+I)
2130  DEL(L+I)=G*DZ(L+I)
2140  CONTINUE
2150  CONTINUE
C---- CALL SOLVER
C   OLD VALUE WAS 15
      MAXIT=100
C
      IF(DUMP) THEN
      CALL ERRLIN(1,90,IMAT,N,SATOL,SRTOL,MAXIT,
     1         W1,W2,IPIVOT,NDJ,B,DEL,IFAIL)
      WRITE(90) (SM(III),III=1,N)
      STOP
      ENDIF
      CALL LINSOL(0,IMAT,N,SATOL,SRTOL,MAXIT,
     1      W1,W2,LRW,RWORK,LIW,IWORK,IPIVOT,NDI,B,DEL,IFAIL)
      IF((IMAT(1).GE.3).AND.IJOB(7).GE.3)  WRITE(LOUT,8181) IFAIL
      IF(DUMP.AND.IFAIL.GT.0) THEN
      CALL ERRLIN(1,90,IMAT,N,SATOL,SRTOL,MAXIT,
     1         W1,W2,IPIVOT,NDJ,A,DEL,IFAIL)
      WRITE(77) (SM(III),III=1,N)
      STOP
      ENDIF
      IF(IFAIL.GT.0)  WRITE(LOUT,*) ' IFAIL ',IFAIL
      IF(IFAIL.GT.0)  GOTO 5100
 8181 FORMAT(1H ,I4,' ITERATIONS NEEDED IN GS- METHOD')
      NDEC=NDEC+1
      NSOL=NSOL+1
C---- IF NECESSARY SOLVE FOR SENSITIVITY
      IF(NUMP.LE.0) GOTO 2190
      DO 2180 KK=1,NUMP
      L=KK*N+1
      CALL LINSOL(1,IMAT,N,SATOL,SRTOL,MAXIT,W1,W2,
     1  LRW,RWORK,LIW,IWORK,IPIVOT,NDI,B,DEL(L),IFAIL)
      IF(IFAIL.GT.0)  GOTO 5100
2180  CONTINUE
2190  CONTINUE
C***********************************************************************
C       CHECK FOR SYSTEMS WITH NILPOTENCY GREATER THAN ONE
C       AND/OR INCONSISTENT INITIAL VALUES
C***********************************************************************
      IF(IJOB(1).EQ.1.OR.J.GT.2.OR.NSTEP.GT.2) GOTO 2250
      IF(J.GT.1) GOTO 2220
      DELM=ZERO
      DO 2210 I=1,N
      W3(I)=DABS(DEL(I))
2210  IF(W3(I).GT.DELM)DELM=W3(I)
      GOTO 2250
 2220 CONTINUE
      DO 2230 I=1,N
      IF(DEL(I).EQ.ZERO) GOTO 2230
      IF(DABS(DEL(I)).LT.SMALL) GOTO 2230
      W3(I)=W3(I)/DABS(DEL(I))
      IF(DABS(W3(I)).GT.EX2) GOTO 2230
C     IF(IJOB(7).GT.2.AND.NSTEP.EQ.0.AND.JRED.EQ.0) WRITE(LOUT,8210)I
      IF(W3(I)*DABS(DEL(I)).GT.DELM*THRESH.AND.JRED.GT.2) GOTO 2240
2230  CONTINUE
      GOTO 2250
2240  IF(W3(I).LT.EX1) GOTO 9500
      GOTO 9600
C
2250  CONTINUE
C***********************************************************************
C     STORE STARTING STEP
C***********************************************************************
      DO 2310  I=1,N
 2310 YM(I)=YM(I)+DEL(I)*SM(I)
      IF(NUMP.LE.0) GOTO 2350
      DO 2340 KK=1,NUMP
      IF(KONVQ(KK).GE.1) GOTO 2340
      L=KK*N
      DO 2330 I=1,N
      YM(L+I)=YM(L+I)+DEL(L+I)*SM(I)
 2330 CONTINUE
 2340 CONTINUE
 2350 CONTINUE
      IF(.NOT.INEW) GOTO 7171
C              *************************************************
      CMYMAX = FLOAT(NJ(1))*0.9D0
      CMYRED = FLOAT(NJ(1))*0.5D0
C
C---3.1.2      COMPUTATIONAL ESTIMATION OF ONE-SIDED LIPSCHITZ CONSTANT
               DMY = ZERO
               DO 3121 I=1,N
 3121             DMY = DMY + DEL(I)*DEL(I)
C              ENDDO
               DELT0A = DELT0
               DELT0 = DSQRT(DMY)
               QDMA = QDM
               QDM = (DMY .GE. TOLN)
               QMY = .FALSE.
               IF (J .EQ. 2) THEN
                 FNJ=FLOAT(NJ(J))
                 FNJ1=FLOAT(NJ(J-1))
                 IF (QDMA) THEN
                    CKAP = DELT0 / DELT0A
                    CKQ = FNJ1 / FNJ
C              WRITE(LOUT,*) ' CONTRACTION FACTOR =',CKAP-CKQ
                    C = ONE - CKAP
                    IF (C .GT. EPKAP) THEN
                        CMYH = FNJ*(CKQ - CKAP) / C
                        CMY = CMYH / H
                        QMY = .TRUE.
                        IF(QMY.AND.CMY.GE.SMALL) THEN
C                         IF(IJOB(7).GE.4) WRITE(LOUT,*) '  MYH=',CMYH
                          FCMY=CMYH/CMYRED
                        ENDIF
                     ENDIF
                     IF(QMY) THEN
                     IF(CMYH.GE.CMYMAX) THEN
                        RED = CMYRED / CMYH
C                       IF(IJOB(7).GE.4) WRITE(LOUT,*) ' MY-REDUCTION'
                        GOTO  5050
C                       EXIT 3.1 TO STEPSIZE REDUCTION
                     ENDIF
                     ENDIF
                  ENDIF
               ENDIF
C              *************************************************
 7171 CONTINUE
      IF(M.EQ.1) GOTO 4000
      M=M-1
C***********************************************************************
C     START OF K-LOOP
C***********************************************************************
      DO 3999 K=1,M
      TH=T+FLOAT(K)*G
3400  CONTINUE
      CALL FCN(N,NZV,TH,YM,DEL,BVK,IRV,ICV,RPAR,IPAR,IFCN,IFCNP)
      IF(IFCN.EQ.9) THEN
        IJOB(7) = -12
        WRITE(LOUT,*) ' Values lead to error in user',
     1              ' supplied subroutine FCN during K-loop'
        IFCN=0
        GOTO 5100
      ENDIF
      NFCN=NFCN+1
      IF(NUMP.GE.1.AND.KONVQS.NE.NUMP) THEN
        CALL RHSVG (N,NZV,NDV,NGES,NUMP,NDP,TH,YM,DEL,W1,W2,BVK,IRV,ICV,
     1              SM,ETA,KONVQ,FCN,IMAT,RPAR,IPAR,
     1            IFCN,IFCNP)
      ENDIF
      IF(K.GT.1.OR.LDIR) GOTO 3430
      DIFFB=ZERO
      IF(NZV.EQ.0) GOTO 3430
C---- CALCULATE DIFFERENCE OF SCALED NORMS OF B0 AND BK
      DO 3420 I=1,NZV
      IR=IRV(I)
      IC=ICV(I)
      DIFF=(BVK(I)-BV0(I))*SM(IC)/SM(IR)
      DIFFB=DIFFB+DIFF*DIFF
3420  CONTINUE
      DIFFB=FLOAT(M)*DSQRT(DIFFB/FLOAT(NZV))
C---- SCALE FUNCTION AND MULTIPLY WITH STEPSIZE
3430  CONTINUE
      DO 3440 I=1,N
3440  DEL(I)=G*DEL(I)/SM(I)
      IF(NUMP.LE.0) GOTO 3470
      DO 3460 KK=1,NUMP
      IF(KONVQ(KK).GE.1) GOTO 3460
      L=KK*N
      DO 3450 I=1,N
 3450 DEL(L+I)=G*DEL(L+I)/SM(I)
 3460 CONTINUE
 3470 CONTINUE
C***********************************************************************
C***********************************************************************
C     ITERATIVE SOLUTION
C***********************************************************************
      IF(LDIR) GOTO 3600
      CALL LINSOL(1,IMAT,N,SATOL,SRTOL,MAXIT,W1,W2,
     1      LRW,RWORK,LIW,IWORK,IPIVOT,NDI,B,DEL,IFAIL)
      IF((IMAT(1).GE.3).AND.IJOB(7).GE.3)  WRITE(LOUT,8181) IFAIL
      IF(IFAIL.GT.0) THEN
         ITFAIL=9
         WRITE(LOUT,8235)
         GOTO 3520
      ENDIF
      NSOL=NSOL+1
      IF(NUMP.LE.0) GOTO 3490
      DO 3480 KK=1,NUMP
      IF(KONVQ(KK).GE.1) GOTO 3480
      L=KK*N+1
      CALL LINSOL(1,IMAT,N,SATOL,SRTOL,MAXIT,W1,W2,
     1    LRW,RWORK,LIW,IWORK,IPIVOT,NDI,B,DEL(L),IFAIL)
      IF(IFAIL.GT.0) THEN
         ITFAIL=9
         WRITE(LOUT,8235)
         GOTO 3520
      ENDIF
3480  CONTINUE
3490  CONTINUE
      IF(NZV.EQ.0.OR.DIFFB.LE.RTOL(1)*1.D-2) GOTO 3900
      ITFAIL=0
      DO 3510 I=1,N
3510  W1(I)=DEL(I)
      CALL ITER2 (N,NZV,NDV,NDI,KOH,B,BV0,BVK,DEL,SM,W1,
     1             W2,W4,LRW,RWORK,LIW,IWORK,
     2             IJOB,IPIVOT,IRV,ICV,RTOL,ATOL,SRTOL,SATOL,
     2             ITFAIL,THETA,DELTA,IEST,FNH,IMAT,
     3             ITER,ITOPT,RPAR,IPAR)
      IEST=0
      IF(IJOB(7).GE.4) WRITE(LOUT,8230) K,ITER,ITFAIL,THETA,DELTA
 3520 CONTINUE
C---- IF TOO MUCH ITERATIONS WERE NEEDED, SWITCH LDIR TO TRUE
      IF(ITFAIL.GE.1) LDIR=.TRUE.
C---- IF ITERATION FAILED TOTALLY RESTART AND USE DIRECT SOLUTION
      IF(ITFAIL.GE.2) GOTO 3400
      GOTO 3900
C***********************************************************************
C     DIRECT SOLUTION
C***********************************************************************
3600  CONTINUE
C***********************************************************************
C     COMPUTE  BK-G*A (SCALED)
C***********************************************************************
      CALL ITMAT(IMAT,N,G,NDJ,A,SM,
     1           NZV,NDV,BVK,IRV,ICV,NZC,NDC,BC,IRC,ICC,NDI,B)
C***********************************************************************
C     SOLVE EQUATION SYSTEM  (BK-GA)*DEL= H*FK
C***********************************************************************
      IFAIL=0
      CALL LINSOL(0,IMAT,N,SATOL,SRTOL,MAXIT,
     1       W1,W2,LRW,RWORK,LIW,IWORK,IPIVOT,NDI,B,DEL,IFAIL)
      IF((IMAT(1).GE.3).AND.IJOB(7).GE.3)  WRITE(LOUT,8181) IFAIL
C---- CHECK FOR FAIL DURING DIRECT SOLUTION
      IF(IJOB(7).GE.4) WRITE(LOUT,8240) K,IFAIL
      IF(IFAIL.GT.0) GOTO 5100
      NDEC=NDEC+1
      NSOL=NSOL+1
C***********************************************************************
C     SOLUTION ACCEPTED, DESCALE AND ADD TO CURRENT Y (YM)
C***********************************************************************
3900  CONTINUE
      IEST=0
      DO 3910 I=1,N
3910  YM(I)=YM(I)+DEL(I)*SM(I)
      IF(NUMP.LE.0) GOTO 3950
      DO 3940 KK=1,NUMP
      IF(KONVQ(KK).GE.1) GOTO 3940
      L=KK*N
      DO 3930 I=1,N
      YM(L+I)=YM(L+I)+DEL(L+I)*SM(I)
 3930 CONTINUE
 3940 CONTINUE
 3950 CONTINUE
C***********************************************************************
C     END OF K- LOOP
C***********************************************************************
3999  CONTINUE
C***********************************************************************
C
C                           EXTRAPOLATION
C
C***********************************************************************
4000  ERR=ZERO
C***********************************************************************
C NEWNEW  CALCULATE SECOND NEWTON CORRECTION
C***********************************************************************
      IF(J.GT.1) GOTO 4009
      CALL RESLIM(N,NZV,NDV,T,YM,W1,W4,BV0,IRV,ICV,YP,FCN,RPAR,IPAR,
     1        IFCN,IFCNP)
      IF(IFCN.EQ.9) THEN
        IJOB(7) = -12
        WRITE(LOUT,*) ' Values lead to error in user',
     1              ' supplied subroutine FCN during',
     1              ' computation of the second newton correction'
        IFCN=0
        GOTO 5100
      ENDIF
      DO 4001 I=1,N
      W4(I)=G*W1(I)
 4001 CONTINUE
      IF(NZV.EQ.0) GOTO 4007
      DO 4003 I=1,NZV
      ICH=ICV(I)
      IRH=IRV(I)
      W4(IRH)=W4(IRH)-BV0(I)*SM(ICH)*DEL(ICH)
 4003 CONTINUE
 4007 CONTINUE
      DO 4004 I=1,NZC
      ICH=ICC(I)
      IRH=IRC(I)
      W4(IRH)=W4(IRH)-BC(I)*SM(ICH)*DEL(ICH)
 4004 CONTINUE
      DO 4006 I=1,N
      W4(I)=W4(I)/SM(I)
 4006 CONTINUE
      CALL LINSOL(1,IMAT,N,SATOL,SRTOL,MAXIT,
     1        W1,W2,LRW,RWORK,LIW,IWORK,IPIVOT,NDI,B,W4,IFAIL)
      TET1=0.0D0
      DO 4002 I=1,N
      TET1=TET1+W4(I)*W4(I)
 4002 CONTINUE
      TET1=DSQRT(TET1)
      TET0=DMAX1(DELT0,SMALL)
      IF(IJOB(7).GT.1) THEN
      WRITE(LOUT,*) 'DEL0 =',TET0
      WRITE(LOUT,*) 'DEL1 =',TET1
      WRITE(LOUT,*) 'THETA=',TET1/TET0
      ENDIF
      JGOOD=.FALSE.
      IF(TET1/TET0.LE.5.D-3) JGOOD=.TRUE.
      IF(JGOOD.AND.IJOB(7).GT.1)
     1  WRITE(LOUT,*) ' +++++  JACOBIAN WILL BE KEPT (IF POSSIBLE) ++++'
 4009 CONTINUE
C***********************************************************************
C     CALCULATE VALUE OF YP (DESCALED)
C***********************************************************************
      DO 4010 I=1,N
      YP(I)=DEL(I)*SM(I)/G
 4010 CONTINUE
C***********************************************************************
C     EXTRAPOLATION OF YP
C***********************************************************************
      DO 4120 I=1,N
      C=YP(I)
      V=DTP(I,1)
      DTP(I,1)=C
      IF(J.EQ.1) GOTO 4120
      TA=C
      DO 4110 K=2,J
      JK=J-K+1
      B1=D(J,JK)
      W=C-V
      U=W/(B1-ONE)
      C=B1*U
      V=DTP(I,K)
      DTP(I,K)=U
 4110 TA=U+TA
      YPM(I)=TA
 4120 CONTINUE
C***********************************************************************
C     EXTRAPOLATION OF Y
C***********************************************************************
      ERRMAX=0.D0
      LMAX=0
      DO 4220 I=1,N
      C=YM(I)
      V=DT(I,1)
      DT(I,1)=C
      IF(J.EQ.1) GOTO 4220
      TA=C
      DO 4210 K=2,J
      JK=J-K+1
      B1=D(J,JK)
      W=C-V
      U=W/(B1-ONE)
      C=B1*U
      V=DT(I,K)
      DT(I,K)=U
4210  TA=U+TA
      YM(I)=TA
      TA=DABS(TA)
      IF(TA.LT.SM(I)) TA=SM(I)
      U=U/TA
      IF(DABS(U).GT.ERRMAX) LMAX=I
      IF(DABS(U).GT.ERRMAX) ERRMAX=DABS(U)
      ERR=ERR+U*U
 4220 CONTINUE
C---- SET KONVQS TO 0
      KONVQS=0
C***********************************************************************
C     EXTRAPOLATION OF SENSITIVITIES
C***********************************************************************
      IF(NUMP.LE.0) GOTO 4290
      ERRP=0.D0
      NPHH=N
      KONVQS=0
      DO 4280 KK=1,NUMP
      L=KK*N
      ERRQKK=0.D0
      IF(KONVQ(KK).GE.1) GOTO 4275
      NPHH=NPHH+N
      DO 4270 I=1,N
      C=YM(L+I)
      V=DT(L+I,1)
      DT(L+I,1)=C
      IF(J.EQ.1) GOTO 4270
      TA=C
      DO 4260 K=2,J
      JK=J-K+1
      B1=D(J,JK)
      W=C-V
      U=W/(B1-ONE)
      C=B1*U
      V=DT(L+I,K)
      DT(L+I,K)=U
4260  TA=U+TA
      YM(L+I)=TA
      TA=DABS(TA)
      IF(TA.LT.ATOLS) TA=ATOLS
      U=U/TA
      ERRQKK=ERRQKK+U*U
 4270 CONTINUE
      IF(J.EQ.1) GOTO 4280
      ERRP=ERRP+ERRQKK
      ERRQKK=DSQRT(ERRQKK/FN)
C     IF(IJOB(7).GE.4) WRITE(LOUT,*) ' IQ,ERRQ:=',KK,ERRQKK
      IF(ERRQKK.LE.RTOLS) KONVQ(KK)=1
 4275 CONTINUE
      KONVQS=KONVQS+KONVQ(KK)
 4280 CONTINUE
 4290 CONTINUE
      IF(J.EQ.1) GOTO 4999
C***********************************************************************
C     ERROR (SCALED ROOT MEAN SQUARE)
C***********************************************************************
      IF(NUMP.GE.1) ERRP=DSQRT(ERRP/FLOAT(N*NUMP))
C     WRITE(LOUT,*) ' ERRP:=',ERRP
C
      ERR=DSQRT(ERR/FN)
      KONV=0
CCCCC ERR=DSQRT((ERR+ERRP)/FLOAT(NPHH))
      IF(ERR.LE.RTOL(1)) KONV=1
CCCC  IF(ERR.LE.RTOL(1).AND.KONVQS.EQ.NUMP) KONV=1
      ERR=ERR/EPH
C***********************************************************************
C     ORDER CONTROL
C***********************************************************************
      K=J-1
C---- FC = EPSILON(K)
                  FC=DMAX1(ERR**(ONE/FLOAT(J)),FCM)
CTEST             FC=DMAX1(ERR**(ONE/FLOAT(2*K+1)),FCM)
CTEST HIER DRUEBER
      IF(INEW)    FC=DMAX1(FC,FCMY)
C***********************************************************************
C  OPTIMAL ORDER DETERMINATION
C***********************************************************************
C---- OMJ = W(K+1,K)
      OMJ=FC*AJ(J)
C---- LOOK IF  W(K+1,K) IS LESS THAN OLD OPTIMUM.
C     IF YES, ASSIGN NEW VALUES TO KO, JO, AND FCO AND UPDATE OMJO
      IF(J.GT.2.AND.OMJ*ONE1.GT.OMJO.OR.K.GT.JOH) GOTO 4310
      KO   = K
      JO   = J
      FCO  = FC
      OMJO = OMJ
4310  CONTINUE
      IF(J.LT.KOH.AND.NSTEP.GT.0) GOTO 4999
      IF(KONV.EQ.0)               GOTO 4500
      IF(KO.LT.K.OR.INCR(J).LT.0) GOTO 6000
C***********************************************************************
C     POSSIBLE INCREASE OF ORDER
C***********************************************************************
      IF(NRED(KO).GT.0) NRED(KO)=NRED(KO)-1
                  FC=FCO/AL(J,K)
                  FC=DMAX1(FC,FCM)
      IF(INEW)    FC=DMAX1(FC,FCMY)
      IF(AJ(J+1)*FC*ONE1.LE.OMJO) THEN
          FCO = FC
          KO  = JO
          JO  = JO+1
      ENDIF
      GOTO 6000
C***********************************************************************
C     CONVERGENCE MONITOR
C***********************************************************************
4500  RED=ONE/FCO
      JK=KMH
      IF(JOH.LT.KMH) JK=JOH
      IF(K.GE.JK) GOTO 5000
      IF(KO.LT.KOH) RED=AL(KOH,KO)*RED
      IF(AL(JK,KO).LT.FCO) GOTO 5000
4999  CONTINUE
C***********************************************************************
C
C     STEPSIZE REDUCTION
C
C***********************************************************************
C***********************************************************************
C  STEPSIZE REDUCTION DUE TO EXTRAPOLATION TABLEAU
C***********************************************************************
5000  RED=RED*SAFE
      IF(IJOB(7).GE.4.AND.LMAX.GT.0)
     1       WRITE(LOUT,8250) LMAX,ERRMAX,Y(LMAX),YM(LMAX)
      IF(IJOB(7).GE.4.AND.LMAX.LE.0)
     1       WRITE(LOUT,8250) LMAX,ERRMAX
C-NEW
      JGOOD=.FALSE.
5050  IF(RED.GE.RMIN) RED=RMIN
      H=H*RED
      GOTO 5300
C***********************************************************************
C  STEPSIZE REDUCTION (EMPIRICAL DEVICE)
C***********************************************************************
5100  IF(IFAIL.GT.0.AND.IJOB(1).EQ.0) ISING=ISING+1
      IF(ISING.GT.ISMAX)                           GOTO 9400
      IF(NSTEP.EQ.0.AND.JRED.GT.2)                 GOTO 9600
C-NEW
      JGOOD=.FALSE.
      HMAX=G*FLOAT(NJ(1))*QUART
C---- IF DECOMPOSITION OF MATRIX FAILED, THEN APPLY SAFETY FACTOR
      IF(IFAIL.GT.0.AND.IMAT(1).LT.3) HMAX=HMAX*SAFE
      RED=HMAX/DABS(H)
      H=HMAX
      IF(JRED.GT.0.OR.IT.GT.0)                     GOTO 5400
      GOTO 5300
C***********************************************************************
C  STEPSIZE REDUCTION DUE TO ERROR IN SENSITIVITIES
C***********************************************************************
C5200 CONTINUE
C     HMAX=G*QUART
C     HMAX=HMAX*SAFE
C     RED=HMAX/DABS(H)
C     H=HMAX
C     IF(JRED.GT.0.OR.IT.GT.0)                     GOTO 5400
C     GOTO 5300
C***********************************************************************
C  STEPSIZE REDUCTION
C***********************************************************************
5300  CONTINUE
      IF(NSTEP.EQ.0) GOTO 5400
      NRED(KOH)=NRED(KOH)+1
      INCR(KOH)=-2
5400  CONTINUE
      JRED=JRED+1
      IF(IJOB(7).GT.1) WRITE(LOUT,8260) JRED,RED
      IF(JRED.GT.JRMAX) GOTO 9100
      GOTO 1500
C***********************************************************************
C
C              PREPARATIONS FOR NEXT BASIC INTEGRATION STEP
C
C***********************************************************************
6000  CONTINUE
CNEW
CNEW  IF(KONVQS.NE.NUMP) GOTO 5200
C***********************************************************************
C     STORE TIME,SOLUTION AND STEPSIZE
C***********************************************************************
      TOLD=T
      T=TN
      H1=TEND-T
      DO 6010 I=1,NGES
      Y(I)  = YM(I)
 6010 CONTINUE
      DO 6020 I=1,N
      YP(I) = YPM(I)
 6020 CONTINUE
      NSTEP=NSTEP+1
      IF(NSTEP.GT.NSTMAX) GO TO 9200
C***********************************************************************
C     PREDICT STEPSIZE FOR NEXT INTEGRATION STEP
C***********************************************************************
      H   = H / FCO
C-UNTEST
      IF(TET1/TET0.GT.FCO*5.D-3) THEN
        JGOOD=.FALSE.
        IF(IJOB(7).GT.1) WRITE(LOUT,*) ' +++  JACOBIAN WILL NOT BE KEPT'
      ENDIF
      KOH = KO
      JOH = KOH + 1
C---- SAVE ORDER PARAMETERS IN IJOB ARRAY
      IJOB(13) = KMH
      IJOB(14) = JMH
      IJOB(15) = KOH
      IJOB(16) = JOH
C---- SAVE INFORMATION NUMBERS
      IJOB(8)  = NSTEP
      IJOB(9)  = NFCN
      IJOB(10) = NDEC
      IJOB(11) = NSOL
      IJOB(12) = NFCNJ
C     IF(DABS(H).GT.DABS(T)*EPMACH*TEN) GOTO 1000
      IF(DABS(H).GT.DABS(T)*EPMACH*TEN) RETURN
      GO TO 9300
C***********************************************************************
C     SOLUTION EXIT
C***********************************************************************
7000  H=HR
      HMAX=HMAXU
      IJOB(8)=NSTEP
      IJOB(9)=NFCN
      IJOB(10)=NDEC
      IJOB(11)=NSOL
      IJOB(12)=NFCNJ
      IJOB(17)=0
      IF(JGOOD) IJOB(17)=1
      IF(IJOB(7).GE.3) WRITE(LOUT,8310) NSTEP,NFCN,T,H,K,KO
      IF(IJOB(7).EQ.2) WRITE(LOUT,8320) NSTEP,T
      IF(IJOB(7).GE.5) WRITE(LOUT,8330) T,(Y(I),I=1,N)
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
9100  CONTINUE
      WRITE(LOUT,9101)JRMAX
9101  FORMAT(1X,' MORE THAN JRMAX=',I3,' STEPSIZE REDUCTIONS DUE TO EXTR
     *APOLATION TABLEAU')
      IJOB(7)=-1
      GOTO 9999
9200  CONTINUE
      WRITE(LOUT,9201) NSTMAX
9201  FORMAT(1X,' MORE THAN NSTMAX=',I4,' INTEGRATION STEPS'//)
      IJOB(7)=-2
      GOTO 9999
9300  CONTINUE
      WRITE(LOUT,9301)
9301  FORMAT(/1X,' STEPSIZE REDUCTION FAILED TO SUCCEED '//)
      IJOB(7)=-3
      GOTO 9999
9400  CONTINUE
      WRITE(LOUT,9401)
9401  FORMAT(/1X,' THE MATRIX-PENCIL B-H*A IS SINGULAR ',/)
      IJOB(7)=-4
      GOTO 9999
9500  CONTINUE
      WRITE(LOUT,9501)
9501  FORMAT(1X,' PROBLEM NOT SOLVABLE WITH LIMEX, PROBABLE REASON:',/1X
     *,' NILPOTENCY OF THE SYSTEM GREATER THAN ONE',/)
      IJOB(7)=-6
      GOTO 9999
9600  CONTINUE
      WRITE(LOUT,9601)
9601  FORMAT(1X,' PROBLEM NOT SOLVABLE WITH LIMEX, PROBABLE REASON:',/1X
     *,' INITIAL DATA INCONSISTENT OR NILPOTENCY OF THE SYSTEM GREATER T
     *HAN ONE',/)
      IJOB(7)=-7
9999  H=ZERO
      HMAX=HMAXU
      RETURN
C***********************************************************************
C     FORMAT STATEMENTS
C***********************************************************************
C---- NEW CALLS TO LIMEX
 8010 FORMAT(2X,70('-'),/,3X,
     1 'REL.PREC. ',1PE10.3,3X,'ABS.PREC.',1PE10.3,3X,'MAX. COL. ',I3,/,
     2       2X,70('-'),/)
 8020 FORMAT(1X,' *** INITIAL VALUES *** ',/(1X,4D18.9))
 8030 FORMAT(/,3X,'STEP',5X,'F-CALLS',10X,'T',14X,'H',10X,
     1                                    'USED/AIMED COLUMN',/)
 8040 FORMAT(/,5X,'STEP',8X,'T')
C---- OUTPUT BEFORE BASIC INTEGRATION STEP
 8110 FORMAT(/1X,I7,2X,D15.5)
 8120 FORMAT(/1X,I5,2X,I9,3X,D15.5,2X,D15.5,I9,I9)
 8130 FORMAT(/,1X,' --- CURRENT VALUES --- ',/(1X,4D18.9),/)
C---- INTEGRATION MONITOR
C8210 FORMAT(1X,' * WARNING * : COMPONENT',I5,
C    1   ' HAS NO ASYMP. EXPANSION IN THE INIT. PHASE ')
 8220 FORMAT(/,' J =',I2,' STARTS:')
 8230 FORMAT(5X,'FOR K=',I1,': ITER=',I2,' ITFAIL=',I1,/,
     1      20X,'THETA=',D13.3,'  DELTA0=',D13.3)
 8235 FORMAT(5X,'  ERROR IN STARTING STEP OF ITERATION')
 8240 FORMAT(5X,'FOR K=',I1,': DIRECT SOLUTION; IFAIL =',I2)
 8250 FORMAT('  MAXIMUM ERROR DETECTED FOR COMPONENT NO.',I4,' (',
     1       1PE9.2,')',/,
     1       '  OLD VALUE WAS ',1PE9.2,
     1       '  CURRENT VALUE IS ',1PE9.2)
 8260 FORMAT(/1X,I3,'.REDUCTION, STEPSIZE REDUCTION FACTOR ',D10.3)
C---- OUTPUT OF SOLUTION
 8310 FORMAT(/1X,I5,2X,I9,3X,D15.5,2X,D15.5,I9,I9)
 8320 FORMAT(/1X,I7,2X,D15.5)
 8330 FORMAT(/1X,' *** SOLUTION AT T =',D18.9,' ***',/(1X,4D18.9))
C***********************************************************************
C     END OF CORE INTEGRATOR LIMEX1
C***********************************************************************
      END
      SUBROUTINE ITER2 (N,NZV,NDV,NDI,KO,B,BV0,BVK,DEL,SM,W1,W2,W4,
     1                 LRW,RWORK,LIW,IWORK,
     1                 IJOB,IPIVOT,IRV,ICV,RTOL,ATOL,SRTOL,SATOL,ITFAIL,
     2                 THETA,DELTA,IEST,FNH,IMAT,
     3                 ITER,ITOPT,RPAR,IPAR)
C***********************************************************************
C
C     ************************************************************
C     *                                                          *
C     *     ITERATIVE REALIZATION OF THE DISCRETIZATION FOR      *
C     *       VARIABLE LEFT-HAND-SIDE MATRIX B(T,Y)              *
C     *                                                          *
C     ************************************************************
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C
      DIMENSION B(NDI),BV0(NDV),BVK(NDV),DEL(N),SM(N),W1(N),W4(N),
     1          W2(N),IJOB(20),IPIVOT(N),IRV(NDV),ICV(NDV),IMAT(10),
     1          RWORK(LRW),IWORK(LIW)
C-DIMENSION
      DIMENSION RPAR(*),IPAR(*),ATOL(*),RTOL(*)
C
      COMMON/ITERA/NSOL
      COMMON/UNIT/LOUT
C
      DATA ZERO/0.D0/
      DATA THTMX1/.25D0/ , THTMX2/.50D0/ , DELMX1/.25D0/ , DELMX2/.5D0/
C***********************************************************************
C     PREPARATIONS
C***********************************************************************
      JOB=0
      ITMAX=2*ITOPT
      ITER=1
      THETA=ZERO
      DELTA=ZERO
      IF(IEST.EQ.1) DELMAX=DELMX1
      IF(IEST.EQ.1) THTMAX=THTMX1
      IF(IEST.EQ.0) DELMAX=DELMX2
      IF(IEST.EQ.0) THTMAX=THTMX2
      IF(IJOB(7).GT.4.AND.IEST.EQ.1) WRITE(LOUT,150)
C***********************************************************************
C     DESCALE INITIAL VALUE OF ITERATION
C***********************************************************************
      DO 10 I=1,N
10    W2(I)=W1(I)*SM(I)
C***********************************************************************
C  COMPUTATION OF DELTA-B*X
C***********************************************************************
20    CONTINUE
      DO 30 I=1,N
      W1(I)=ZERO
30    CONTINUE
      DO 40  I=1,NZV
      IC=ICV(I)
      IR=IRV(I)
      W1(IR)=W1(IR)+(BV0(I)-BVK(I))*W2(IC)
40    CONTINUE
C***********************************************************************
C     SCALE W1
C***********************************************************************
      DO 50 I=1,N
50    W1(I)=W1(I)/SM(I)
C***********************************************************************
C     SOLUTION OF THE LINEAR EQUATION
C***********************************************************************
      ILL=0
C    OLD VALUE WAS 15
      MAXIT=100
      CALL LINSOL(1,IMAT,N,SATOL,SRTOL,MAXIT,W4,W2,
     1      LRW,RWORK,LIW,IWORK,IPIVOT,NDI,B,W1,ILL)
      IF((IMAT(1).GE.3).AND.IJOB(7).GE.3)  WRITE(LOUT,8181) ILL
 8181 FORMAT(1H ,I4,' ITERATIONS NEEDED IN GS- METHOD')
      IF(ILL.GT.0) GOTO 260
      NSOL=NSOL+1
C***********************************************************************
C     ERROR ESTIMATION
C***********************************************************************
      ERR=ZERO
      DO 80 I=1,N
80    ERR=ERR+W1(I)*W1(I)
      ERR=DSQRT(ERR/FLOAT(N))
C***********************************************************************
C     CALCULATION OF STEPSIZE CONTROL PARAMETERS
C***********************************************************************
      IF(ITER.GT.2) GOTO 100
      IF(ITER.EQ.1 .AND. ERR.GT.DELMAX) GOTO 220
      IF(ITER.EQ.1) GOTO 90
      THETA=ERR/DELTA
      IF(THETA.GT.THTMAX)               GOTO 230
      IF(IEST.EQ.0) GOTO 100
      ZZ=THETA**FLOAT(ITOPT+2)*DELTA
      IF(ZZ.GT.RTOL(1)*1.D-1)              GOTO 240
      GOTO 100
90    DELTA=ERR
C
100   IF(IJOB(7).GT.4.AND.IEST.EQ.1) WRITE(LOUT,160) ITER,ERR
      IF(ERR.LE.RTOL(1)*1.D-1) GOTO 130
      ITER=ITER+1
      DO 110 I=1,N
      DEL(I)=DEL(I)+W1(I)
110   W2(I)=W1(I)*SM(I)
C---- RESTART IF ITMAX NOT REACHED
      IF(ITER.LE.ITMAX) GOTO 20
      GOTO 250
C***********************************************************************
C     SOLUTION EXIT
C***********************************************************************
130   DO 140 I=1,N
140   DEL(I)=DEL(I)+W1(I)
      ITFAIL=0
      IF(IEST.EQ.1 .AND. ITER.GT.ITOPT) ITFAIL=1
      RETURN
C***********************************************************************
C     FAIL EXITS
C***********************************************************************
220   ITFAIL=2
      RETURN
C
230   ITFAIL=3
      RETURN
C
240   ITFAIL=4
      RETURN
C
250   ITFAIL=5
      RETURN
C---- PENTADIAGONAL OR NONADIAGONAL ITERATION FAILED
260   ITFAIL=6
      RETURN
C
150   FORMAT(10X,' ####       ITER           ERROR      ####')
160   FORMAT(10X,' ####      ',I4,7X,D12.4,'    ####')
C
C***********************************************************************
C     END OF ITERAT
C***********************************************************************
      END
      SUBROUTINE RESLIM (NEQ,NZV,NDV,T,U,URHS,URES
     1      ,BV,IR,IC,UP,FCN,RPAR,IPAR,
     1            IFCN,IFCNP)
C*********************************************************************
C
C     COMPUTATION OF RESIDUAL
C
C*********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C
C
      DIMENSION U(NEQ),URHS(NEQ),URES(NEQ),UP(NEQ)
      DIMENSION BV(NDV),IR(NDV),IC(NDV)
      DIMENSION RPAR(*),IPAR(*),IFCNP(*)
C
      EXTERNAL FCN
C
      CALL FCN (NEQ,NZV,T,U,URHS,BV,IR,IC,RPAR,IPAR,IFCN,IFCNP)
C
      DO 100 I=1,NEQ
      URES(I)=URHS(I)
100   CONTINUE
C
      IF(NZV.EQ.0) RETURN
C
      DO 200 I=1,NZV
      ICH=IC(I)
      IRH=IR(I)
      URES(IRH)=URES(IRH)-BV(I)*UP(ICH)
200   CONTINUE
C
      RETURN
C*********************************************************************
C     END OF RESLIM
C*********************************************************************
      END
      SUBROUTINE DAJAC(N,T,Y,SM,A,JAC,RPAR,IPAR,IFCN,IFCNP)
C***********************************************************************
C
C      DENSE ANALYTIC JACOBIAN
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      EXTERNAL JAC
      DIMENSION Y(N),SM(N),A(N,N)
      DIMENSION RPAR(*),IPAR(*),IFCNP(*)
C
C  SUBROUTINE JAC SHOULD PROVIDE JACOBIAN MATRIX A=DF/DY
C  LIMEX1 USES INTERNALLY THE MATRIX A=-DF/DY (SCALED)
C
      CALL JAC (N,N,T,Y,A,RPAR,IPAR,IFCN,IFCNP)
      DO 10 I=1,N
      DO 10 K=1,N
      A(I,K)=-SM(K)*A(I,K)/SM(I)
  10  CONTINUE
      RETURN
C***********************************************************************
C     END OF DAJAC
C***********************************************************************
      END
      SUBROUTINE BAJAC(N,ML,MU,MB,T,Y,SM,A,JAC,RPAR,IPAR,IFCN,IFCNP)
C***********************************************************************
C
C      BANDED  ANALYTIC JACOBIAN
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      EXTERNAL JAC
      DIMENSION Y(N),SM(N),A(MB,N)
      DIMENSION RPAR(*),IPAR(*),IFCNP(*)
C
C  SUBROUTINE JAC SHOULD PROVIDE JACOBIAN MATRIX A=DF/DY
C  LIMEX1 USES INTERNALLY THE MATRIX A=-DF/DY (SCALED)
C
      CALL JAC (N,MB,T,Y,A,RPAR,IPAR,IFCN,IFCNP)
      DO 20 I=1,MB
      DO 10 K=1,N
      KK=I+K-1-MU
      IF(KK.LT.1) GOTO 10
      IF(KK.GT.N) GOTO 20
      A(I,K)=-SM(K)*A(I,K)/SM(KK)
  10  CONTINUE
  20  CONTINUE
      RETURN
C***********************************************************************
C     END OF BAJAC
C***********************************************************************
      END
      SUBROUTINE DNJAC(N,T,Y,YP,DEL0,DEL,SM,ETA,EPDIFF,EPMIN,FMIN,A,
     1     NZV,NDV,BVK,IRV,ICV,
     1     NFCNJ,
     1     WORK,FCN,RPAR,IPAR,
     1            IFCN,IFCNP)
C***********************************************************************
C
C     NUMERICAL DIFFERENCE APPROXIMATION OF DENSE JACOBIAN
C     (FEED-BACK CONTROL OF DISCRETIZATION AND ROUNDING ERRORS)
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      EXTERNAL FCN
      DIMENSION Y(N),YP(N),DEL0(N),DEL(N),SM(N),ETA(N),A(N,N),WORK(N)
      DIMENSION RPAR(*),IPAR(*),IFCNP(*)
      DIMENSION BVK(NDV),IRV(NDV),ICV(NDV)
      DATA ZERO/0.0D0/
C
C   DENSE JACOBIAN
C   --------------
C
      DO 100 K=1,N
      IS=0
  10  W=Y(K)
      U=SM(K)*ETA(K)
      IF(DEL0(K).GT.ZERO) U=-U
      Y(K)=W+U
      CALL RESLIM (N,NZV,NDV,T,Y,WORK,DEL,BVK,IRV,ICV,YP,FCN,RPAR,IPAR,
     1            IFCN,IFCNP)
      NFCNJ=NFCNJ+1
      Y(K)=W
      U=SM(K)/U
      SUMD=ZERO
      DO 50 I=1,N
      WZ=DEL0(I)
      WY=DEL(I)
      W=DABS(WY)
      WY=WZ-WY
      WZ=DABS(WZ)
      IF(W.LT.WZ) W=WZ
      IF(W.EQ.ZERO) GOTO 50
      W=WY/W
      SUMD=SUMD+W*W
  50  A(I,K)=WY*U/SM(I)
      SUMD=DSQRT(SUMD/FLOAT(N))
      IF(SUMD.EQ.ZERO .OR. IS.GT.0) GOTO 100
      ETAD=DSQRT(EPDIFF/SUMD)*ETA(K)
      IF(ETAD.GT.FMIN) ETAD=FMIN
      IF(ETAD.GT.1.D-6) ETAD=1.D-6
      IF(ETAD.LT.EPMIN) ETAD=EPMIN
      IF(ETAD.LT.1.D-8) ETAD=1.D-8
      ETA(K)=ETAD
      IS=1
      IF(SUMD.LT.EPMIN) GOTO 10
 100  CONTINUE
      RETURN
C***********************************************************************
C     END OF DNJAC
C***********************************************************************
      END
      SUBROUTINE BNJAC(N,MBH,MB,ML,MU,T,Y,YP,DEL0,
     1      SM,ETA,EPDIFF,EPMIN,FMIN,A,
     2      NZV,NDV,BVK,IRV,ICV,
     3      NFCNJ,
     4      W3,DEL,W1,W2,FCN,RPAR,IPAR,
     1            IFCN,IFCNP)
C***********************************************************************
C
C     NUMERICAL DIFFERENCE APPROXIMATION OF BANDED JACOBIAN
C     (FEED-BACK CONTROL OF DISCRETIZATION AND ROUNDING ERRORS)
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      EXTERNAL FCN
      DIMENSION Y(N),YP(N),DEL0(N),SM(N),ETA(N),A(MBH,N)
      DIMENSION W1(N),W2(N),W3(N),DEL(N)
      DIMENSION BVK(NDV),IRV(NDV),ICV(NDV)
      DIMENSION RPAR(*),IPAR(*),IFCNP(*)
      DATA ZERO/0.0D0/
C  ---------------
C
      NADROW=MBH-MB
      DO 1 I=1,MB
      DO 1 K=1,N
      A(I+NADROW,K)=ZERO
      IFCNP(I) = 0
    1 CONTINUE
      M=ML+MU+1
      DO 100 JJ=1,M
      IS=0
  10  NFINE=1
      DO 20 K=JJ,N,M
      W2(K)=Y(K)
      W1(K)=SM(K)*ETA(K)
      IF(DEL0(K).GT.ZERO) W1(K)=-W1(K)
      Y(K)=W2(K)+W1(K)
      IFCNP(K) = 1
  20  CONTINUE
      CALL RESLIM (N,NZV,NDV,T,Y,W3,DEL,BVK,IRV,ICV,YP,FCN,RPAR,IPAR,
     1            IFCN,IFCNP)
      NFCNJ=NFCNJ+1
      DO 50 K=JJ,N,M
      Y(K)=W2(K)
      IFCNP(K) = 0
      W1(K)=SM(K)/W1(K)
      SUMD=ZERO
      I1=MAX0(1,K-MU)
      I2=MIN0(N,K+ML)
      MH=MU+1-K
      IF(MH.LT.0)MH=0
      DO 2110 I=I1,I2
      WZ=DEL0(I)
      WY=DEL(I)
      W=DABS(WY)
      WY=WZ-WY
      WZ=DABS(WZ)
      IF(W.LT.WZ) W=WZ
      IF(W.EQ.ZERO) GOTO 2110
      W=WY/W
      SUMD=SUMD+W*W
2110  A(NADROW+MH+I-I1+1,K)=WY*W1(K)/SM(I)
      SUMD=DSQRT(SUMD/FLOAT(N))
      IF(SUMD.EQ.ZERO .OR. IS.GT.0) GOTO 50
      ETAD=DSQRT(EPDIFF/SUMD)*ETA(K)
      IF(ETAD.GT.FMIN) ETAD=FMIN
      IF(ETAD.GT.1.D-6) ETAD=1.D-6
      IF(ETAD.LT.EPMIN) ETAD=EPMIN
      IF(ETAD.LT.1.D-8) ETAD=1.D-8
      ETA(K)=ETAD
      IF(SUMD.LT.EPMIN) NFINE=0
  50  CONTINUE
      IF(NFINE.EQ.1) GOTO 100
      IS=1
      GOTO 10
 100  CONTINUE
      RETURN
C***********************************************************************
C     END OF BNJAC
C***********************************************************************
      END
      SUBROUTINE OTNJAC(NH,NDSB,NUMBLK,T,Y,YP,FUN0,
     1      SCL,ETADIF,A,
     2      NZV,NDV,BVK,IRV,ICV,
     3      NFCNJ,
     4      YH,FUN1,W1,W2,FCN,RPAR,IPAR,
     1            IFCN,IFCNP)
C***********************************************************************
C
C     NUMERICAL DIFFERENCE APPROXIMATION OF DENSE JACOBIAN
C     (FEED-BACK CONTROL OF DISCRETIZATION AND ROUNDING ERRORS)
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      EXTERNAL FCN
      DIMENSION Y(NH),YP(NH),FUN0(NH),SCL(NH),A(NDSB,NDSB,NUMBLK,3)
      DIMENSION W1(NH),W2(NH),YH(NH),FUN1(NH)
      DIMENSION BVK(NDV),IRV(NDV),ICV(NDV)
      DIMENSION RPAR(*),IPAR(*),IFCNP(*)
C-----------------------------------------------------------------------
C  CALCULATE JACOBIAN
C-----------------------------------------------------------------------
      NDSBH=3*NDSB
C  PRPARATIONS
      DO 5110 I=1,NH
      YH(I)=Y(I)
5110  CONTINUE
C
      DO 5155 KH=1,3
      DO 5150 KHH=1,NDSB
      K=(KH-1)*NDSB+KHH
C  VALUE FOR PERTUBATIONS
      WY=ETADIF
C  ACTUAL COLUMN INDICES FOR PERTUBATION
      DO 5120 KK=K,NH,NDSBH
COLD  YH(KK)=Y(KK)+WY
      YH(KK)=Y(KK)+WY*SCL(KK)
C     WRITE(LOUT,*) YH(KK),Y(KK)
5120  CONTINUE
C
C  CALCULATE PERTURBED FUNCTION
      CALL RESLIM (NH,NZV,NDV,T,YH,W2,FUN1,BVK,IRV,ICV,YP,FCN,RPAR,IPAR,
     1            IFCN,IFCNP)
      NFCNJ=NFCNJ+1
C
      DO 5121 KK=K,NH,NDSBH
5121  YH(KK)=Y(KK)
C
      DO 5125 I=1,NH
      W1(I)=(FUN0(I)-FUN1(I))/WY
C     WRITE(LOUT,*) W1(I)
5125  CONTINUE
C
      IBLK0=KH
      NUMBLH=NUMBLK+1
      I=0
      IF(KH.EQ.3) I=NDSB
      DO 5140 IBLKH=IBLK0,NUMBLH,3
      IBLK=IBLKH-1
      ICOL=IBLK*NDSB+KHH
      IF(ICOL.GT.NH) GOTO 5140
      IF(IBLK.LE.0) GOTO 5131
      DO 5130 II=1,NDSB
      I=I+1
COLD  A(II,KHH,IBLK,3)=SCL(ICOL)*W1(I)/SCL(I)
      A(II,KHH,IBLK,3)=          W1(I)/SCL(I)
5130  CONTINUE
5131  CONTINUE
      IF(I.EQ.NH) GOTO 5145
      DO 5132 II=1,NDSB
      I=I+1
COLD  A(II,KHH,IBLK+1,2)=SCL(ICOL)*W1(I)/SCL(I)
      A(II,KHH,IBLK+1,2)=          W1(I)/SCL(I)
      IBLK1=IBLK+1
5132  CONTINUE
      IF(I.EQ.NH) GOTO 5145
      DO 5133 II=1,NDSB
      I=I+1
COLD  A(II,KHH,IBLK+2,1)=SCL(ICOL)*W1(I)/SCL(I)
      A(II,KHH,IBLK+2,1)=          W1(I)/SCL(I)
      IBLK2=IBLK+2
5133  CONTINUE
      IF(I.EQ.NH) GOTO 5145
5140  CONTINUE
5145  CONTINUE
5150  CONTINUE
5155  CONTINUE
      RETURN
C***********************************************************************
C     END OF TNJAC
C***********************************************************************
      END
      SUBROUTINE PNJAC(N,N0,N1,N2,N2M,T,Y,YP,FUN0,
     1      SCL,ETADIF,A,B,C,
     2      NZV,NDV,BVK,IRV,ICV,
     3      NFCNJ,
     4      YH,FUN1,W1,FCN,RPAR,IPAR,
     1            IFCN,IFCNP)
C***********************************************************************
C                                                                      *
C     ************************************************************     *
C     *      NUMERICAL DIFFERENCE APPROXIMATION OF JACOBIAN      *     *
C     *            GIVEN IN PENTA BLOCK DIAGONAL FORM            *     *
C     ************************************************************     *
C                                                                      *
C***********************************************************************
C***********************************************************************
C                                                                      *
C   B B     C                                                          *
C   B B B     C                                                        *
C     B B B     C                                                      *
C       B B       C                                                    *
C   A       B B     C                                                  *
C     A     B B B     C                                                *
C       A     B B B     C                                              *
C         A     B B       C                                            *
C           A       B B                                                *
C             A     B B B                                              *
C               A     B B B                                            *
C                 A     B B                                            *
C                                                                      *
C***********************************************************************
C                                                                      *
C  INPUT PARAMETERS:                                                   *
C                                                                      *
C    N         = DIMENSION OF THE WHOLE SYSTEM N=N0*N1*N2              *
C    N0        = DIMENSION OF QUADRATIC SUB MATRICES                   *
C    N1        = DIMENSION OF QUADRATIC SUB BLOCKS                     *
C    N2        = DIMENSION OF QUADRATIC BLOCKS                         *
C    N2M       = NUMBER OF SIDE BLOCKS                                 *
C    T         = TIME (FOR USE IN SUBROUTINE FCN)                      *
C    Y         = CURRENT VALUE OF VARIABLE                             *
C    YP        = CURRENT VALUE OF FIRST TIME DERIVATIVE OF VARIABLES   *
C    FUN0      = VALUE OF FUNCTION (UNPERTURBED)                       *
C    SCL       = SCALING FACTORS                                       *
C    ETADIF    = VALUE OF PERTURBATION                                 *
C    FCN       = NAME OF CALLED ROUTINE                                *
C    NZV,BVK,IRV,ICV  NOT USED BUT NEEDED FOR FCN                      *
C                                                                      *
C   OUTPUT PARAMETERS:                                                 *
C                                                                      *
C    A(N0,N0,N1,N2M) ENTRIES IN JACOBIAN (LOWER SIDE ROW)              *
C    B(N0,N0,N1,3,N2) ENTRIES IN JACOBIAN (INNER BLOCK TRIDIAGONAL S.) *
C    C(N0,N0,N1,N2M) ENTRIES IN JACOBIAN (UPPER SIDE ROW)              *
C                                                                      *
C                                                                      *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      EXTERNAL FCN
      DIMENSION Y(N),YP(N),YH(N),FUN0(N),FUN1(N),SCL(N)
      DIMENSION B(N0,N0,N1,3,N2),A(N0,N0,N1,N2M),C(N0,N0,N1,N2M)
      DIMENSION W1(N)
      DIMENSION BVK(NDV),IRV(NDV),ICV(NDV)
      DIMENSION RPAR(*),IPAR(*),IFCNP(*)
C***********************************************************************
C     SET VALUES FOR UNPERTURBED FUNCTIONS
C***********************************************************************
      DO 10 I=1,N
      YH(I)=Y(I)
   10 CONTINUE
C***********************************************************************
C     START LOOP
C***********************************************************************
      DO 1000 KH2=1,3
      DO 1000 KH1=1,3
      DO 1000 KH0=1,N0
C***********************************************************************
C
C     PERTURBE FUNCTIONS
C
C***********************************************************************
C***********************************************************************
C     GET INDEX OF FIRST PERTURBED FUNCTION
C***********************************************************************
      K=(KH2-1)*N0*N1+(KH1-1)*N0+KH0
      KFIR=K
C---- WY =IS VALUE FOR PERTURBATION
      WY=ETADIF
C***********************************************************************
C     GET INDEX OF FUNCTIONS PERTURBED AT THE SAME TIME AND PERTURBE
C***********************************************************************
      DO 120 LL=1,N2,3
      NB2=LL+KH2-1
      IF(NB2.GT.N2) GOTO 130
      INC2=(LL-1)*N0*N1
      DO 110 KK=1,N1,3
      NB1=KK+KH1-1
      IF(NB1.GT.N1) GOTO 110
      INC1=(KK-1)*N0
      IPER=KFIR+INC2+INC1
C---- LEAVE LOOP IF END OF SYSTEM IS REACHED
      IF(IPER.GT.N) GOTO 130
      YH(IPER)=Y(IPER)+SCL(IPER)*WY
 110  CONTINUE
 120  CONTINUE
C
 130  CONTINUE
C***********************************************************************
C
C     CALCULATE PERTURBED FUNCTION
C
C***********************************************************************
      CALL RESLIM (N,NZV,NDV,T,YH,W1,FUN1,BVK,IRV,ICV,YP,FCN,RPAR,IPAR,
     1            IFCN,IFCNP)
      NFCNJ=NFCNJ+1
C***********************************************************************
C
C     RESET PERTURBED FUNCTIONS AND STORE ENTRIES OF JACOBIAN
C
C***********************************************************************
      DO 240 LL=1,N2,3
C---- CALCULATE NUMBER OF BIG BLOCK
      NB2=LL+KH2-1
      IF(NB2.GT.N2) GOTO 240
      INC2=(LL-1)*N0*N1
      DO 230 KK=1,N1,3
C---- CALCULATE NUMBER OF SMALL BLOCK
      NB1=KK+KH1-1
      IF(NB1.GT.N1) GOTO 230
      INC1=(KK-1)*N0
C---- CALCULATE NUMBER OF PERTURBED FUNCTION
      IPER=KFIR+INC2+INC1
      IF(IPER.GT.N) GOTO 290
C---- RESET PERTURBED FUNCTION
      YH(IPER)=Y(IPER)
C***********************************************************************
C     CALCULATE INDEX OF FIRST ELEMENTS
C***********************************************************************
      IB2=N0*N1*(NB2-1)+N0*(NB1-1)
      IB1=IB2+N0
      IB3=IB2-N0
      IA =IB2+N0*N1
      IC =IB2-N0*N1
C***********************************************************************
C     STORE ENTRIES OF JACOBIAN
C***********************************************************************
      DO 210 II=1,N0
      IA =IA +1
      IB1=IB1+1
      IB2=IB2+1
      IB3=IB3+1
      IC =IC +1
      IF(NB1.LT.N1)  B(II,KH0,NB1+1,1,NB2)=
     1                               (FUN0(IB1)-FUN1(IB1))/(WY*SCL(IB1))
                     B(II,KH0,NB1,2,NB2)   =
     1                               (FUN0(IB2)-FUN1(IB2))/(WY*SCL(IB2))
      IF(NB1.GT.1)   B(II,KH0,NB1-1,3,NB2)=
     1                               (FUN0(IB3)-FUN1(IB3))/(WY*SCL(IB3))
      IF(NB2.GT.1)   C(II,KH0,NB1,NB2-1) =
     1                               (FUN0(IC)-FUN1(IC))/(WY*SCL(IC))
      IF(NB2.LT.N2) A(II,KH0,NB1,NB2)=
     1                               (FUN0(IA)-FUN1(IA))/(WY*SCL(IA))
 210  CONTINUE
 230  CONTINUE
 240  CONTINUE
 290  CONTINUE
1000  CONTINUE
      RETURN
C***********************************************************************
C     END OF PNJAC
C***********************************************************************
      END
      SUBROUTINE DITMAT(N,G,A,SM,
     1      NZV,NDV,BV,IRV,ICV,NZC,NDC,BC,IRC,ICC,B)
C***********************************************************************
C
C     COMPUTATION OF A DENSE ITERATION MATRIX B
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION A(N,N),B(N,N),SM(N)
      DIMENSION BV(NDV),IRV(NDV),ICV(NDV)
      DIMENSION BC(NDC),IRC(NDC),ICC(NDC)
C***********************************************************************
C
C***********************************************************************
      DO 10 I=1,N
      DO 10 K=1,N
      B(I,K)=G*A(I,K)
  10  CONTINUE
C***********************************************************************
C     ADD VARIABLE ENTRIES OF B
C***********************************************************************
      IF(NZV.EQ.0) GOTO 30
      DO 20 I=1,NZV
      IR=IRV(I)
      IC=ICV(I)
      B(IR,IC)=B(IR,IC)+SM(IC)*BV(I)/SM(IR)
  20  CONTINUE
C***********************************************************************
C     ADD CONSTANT ENTRIES OF B
C***********************************************************************
  30  IF(NZC.EQ.0) GOTO 50
      DO 40 I=1,NZC
      IR=IRC(I)
      IC=ICC(I)
      B(IR,IC)=B(IR,IC)+SM(IC)*BC(I)/SM(IR)
  40  CONTINUE
C***********************************************************************
C     RETURN
C***********************************************************************
  50  RETURN
C***********************************************************************
C     END OF DITMAT
C***********************************************************************
      END
      SUBROUTINE BITMAT(N,ML,MU,MB,MBH,G,A,SM,
     1      NZV,NDV,BV,IRV,ICV,NZC,NDC,BC,IRC,ICC,B)
C***********************************************************************
C
C     COMPUTATION OF A BANDED ITERATION MATRIX B
C     ROW 1 TO ML ARE KEPT FREE
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION A(MB,N),B(MBH,N),SM(N)
      DIMENSION BV(NDV),IRV(NDV),ICV(NDV)
      DIMENSION BC(NDC),IRC(NDC),ICC(NDC)
C***********************************************************************
C
C***********************************************************************
      DO 10 I=1,MB
      DO 10 K=1,N
      B(ML+I,K)=G*A(I,K)
  10  CONTINUE
C***********************************************************************
C     ADD VARIABLE ENTRIES OF B
C***********************************************************************
      IF(NZV.EQ.0) GOTO 30
      DO 20 I=1,NZV
      IR=IRV(I)
      IC=ICV(I)
      IRH=ML+MU+IR-IC+1
      B(IRH,IC)=B(IRH,IC)+SM(IC)*BV(I)/SM(IR)
  20  CONTINUE
C***********************************************************************
C     ADD CONSTANT ENTRIES OF B
C***********************************************************************
  30  IF(NZC.EQ.0) GOTO 50
      DO 40 I=1,NZC
      IR=IRC(I)
      IC=ICC(I)
      IRH=ML+MU+IR-IC+1
      B(IRH,IC)=B(IRH,IC)+SM(IC)*BC(I)/SM(IR)
  40  CONTINUE
C***********************************************************************
C     RETURN
C***********************************************************************
  50  RETURN
C***********************************************************************
C     END OF DITMAT
C***********************************************************************
      END
      SUBROUTINE TITMAT(N,NDSUB,NBLK,G,A,SM,
     1      NZV,NDV,BV,IRV,ICV,NZC,NDC,BC,IRC,ICC,B)
C***********************************************************************
C
C     COMPUTATION OF A TRIDIAGONAL ITERATION MATRIX
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION A(NDSUB,NDSUB,NBLK,3)
      DIMENSION B(NDSUB,NDSUB,NBLK,3)
      DIMENSION SM(N)
      DIMENSION BV(NDV),IRV(NDV),ICV(NDV)
      DIMENSION BC(NDC),IRC(NDC),ICC(NDC)
C***********************************************************************
C
C***********************************************************************
      DO 1 J=2,3
      DO 1 I=1,NDSUB
      DO 1 K=1,NDSUB
      B(I,K,1,J)=G*A(I,K,1,J)
   1  CONTINUE
      NBLKM=NBLK-1
      DO 2 J=1,3
      DO 2 L=2,NBLKM
      DO 2 I=1,NDSUB
      DO 2 K=1,NDSUB
      B(I,K,L,J)=G*A(I,K,L,J)
   2  CONTINUE
      DO 3 J=1,2
      DO 3 I=1,NDSUB
      DO 3 K=1,NDSUB
      B(I,K,NBLK,J)=G*A(I,K,NBLK,J)
   3  CONTINUE
C***********************************************************************
C     ADD VARIABLE ENTRIES OF B
C***********************************************************************
      IF(NZV.EQ.0) GOTO 30
      DO 20 I=1,NZV
      IR=IRV(I)
      IC=ICV(I)
      IRBL=1+(IR-1)/NDSUB
      ICBL=1+(IC-1)/NDSUB
      IRS =IR-(IRBL-1)*NDSUB
      ICS =IC-(ICBL-1)*NDSUB
      INDB=2+ICBL-IRBL
      B(IRS,ICS,IRBL,INDB)=B(IRS,ICS,IRBL,INDB)+SM(IC)*BV(I)/SM(IR)
  20  CONTINUE
C***********************************************************************
C     ADD VARIABLE ENTRIES OF B
C***********************************************************************
  30  IF(NZC.EQ.0) GOTO 50
      DO 40 I=1,NZC
      IR=IRC(I)
      IC=ICC(I)
      IRBL=1+(IR-1)/NDSUB
      ICBL=1+(IC-1)/NDSUB
      IRS =IR-(IRBL-1)*NDSUB
      ICS =IC-(ICBL-1)*NDSUB
      INDB=2+ICBL-IRBL
      B(IRS,ICS,IRBL,INDB)=B(IRS,ICS,IRBL,INDB)+SM(IC)*BC(I)/SM(IR)
  40  CONTINUE
C***********************************************************************
C     RETURN
C***********************************************************************
  50  RETURN
C***********************************************************************
C     END OF TITMAT
C***********************************************************************
      END
      SUBROUTINE PITMAT(N,N0,N1,N2,N2M,G,AJ,BJ,CJ,SM,
     1      NZV,NDV,BV,IRV,ICV,NZC,NDC,BC,IRC,ICC,AIM,BIM,CIM)
C***********************************************************************
C
C     COMPUTATION OF A PENTA BLOCK DIAGONAL ITERATION MATRIX
C     NOTE: THERE IS NO WARNING FLAG, IF ENTRY OF BV OR BC IS IN
C           B(X,X,1,1,X) OR B(X,X,N1,3,X) !!!!!!!!!!!!!!!
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION AJ(N0,N0,N1,N2M)
      DIMENSION BJ(N0,N0,N1,3,N2)
      DIMENSION CJ(N0,N0,N1,N2M)
      DIMENSION AIM(N0,N0,N1,N2M)
      DIMENSION BIM(N0,N0,N1,3,N2)
      DIMENSION CIM(N0,N0,N1,N2M)
      DIMENSION SM(N)
      DIMENSION BV(NDV),IRV(NDV),ICV(NDV)
      DIMENSION BC(NDC),IRC(NDC),ICC(NDC)
C***********************************************************************
C
C***********************************************************************
      N1M=N1-1
      N0N1=N0*N1
C***********************************************************************
C     CALCULATE JACOBIAN PART FOR INNER TRIDIAGONAL MATRIX
C***********************************************************************
      DO 50 L=1,N2
      DO 10 K=2,N1
      DO 10 J=1,N0
      DO 10 I=1,N0
      BIM(I,J,K,1,L)=G*BJ(I,J,K,1,L)
   10 CONTINUE
      DO 20 K=1,N1
      DO 20 J=1,N0
      DO 20 I=1,N0
      BIM(I,J,K,2,L)=G*BJ(I,J,K,2,L)
   20 CONTINUE
      DO 30 K=1,N1M
      DO 30 J=1,N0
      DO 30 I=1,N0
      BIM(I,J,K,3,L)=G*BJ(I,J,K,3,L)
   30 CONTINUE
   50 CONTINUE
C***********************************************************************
C     CALCULATE JACOBIAN PART FOR SUB AND SUPER ROWS
C***********************************************************************
C---- CHECK IF WE HAVE ONLY A BLOCK TRIDIAGONAL SYSTEM
      IF(N2.EQ.1) GOTO 100
      DO 60 L=1,N2M
      DO 60 K=1,N1
      DO 60 J=1,N0
      DO 60 I=1,N0
      AIM(I,J,K,L)=G*AJ(I,J,K,L)
      CIM(I,J,K,L)=G*CJ(I,J,K,L)
   60 CONTINUE
C***********************************************************************
C     ADD VARIABLE ENTRIES OF B
C***********************************************************************
  100 CONTINUE
      IF(NZV.EQ.0) GOTO 200
      DO 190 I=1,NZV
      IR=IRV(I)
      IC=ICV(I)
      IRN2 =1+(IR-1)/N0N1
      ICN2 =1+(IC-1)/N0N1
      IRN1 =1+(IR-(IRN2-1)*N0N1-1)/N0
      ICN1 =1+(IC-(ICN2-1)*N0N1-1)/N0
      IRN0 = IR-(IRN1-1)*N0 - (IRN2-1)*N0N1
      ICN0 = IC-(ICN1-1)*N0 - (ICN2-1)*N0N1
      IF(ICN2.EQ.IRN2) GOTO 130
      IF(ICN2.LT.IRN2) GOTO 120
      IF(ICN2.GT.IRN2) GOTO 140
      GOTO 980
C---- IN THIS CASE ENTRY IS IN THE SUB DIAGONAL
  120 CONTINUE
      AIM(IRN0,ICN0,ICN1,ICN2)=AIM(IRN0,ICN0,ICN1,ICN2)
     1          + SM(IC)*BV(I)/SM(IR)
      GOTO 150
C---- IN THIS CASE ENTRY IS IN THE TRI DIAGONAL SYSTEM
  130 CONTINUE
      INDB=ICN1-IRN1
      IF(IABS(INDB).GT.1) GOTO 980
      INDB=INDB+2
      BIM(IRN0,ICN0,IRN1,INDB,IRN2)=BIM(IRN0,ICN0,IRN1,INDB,IRN2)
     1          + SM(IC)*BV(I)/SM(IR)
      GOTO 150
C---- IN THIS CASE ENTRY IS IN THE SUPER DIAGONAL
  140 CONTINUE
      CIM(IRN0,ICN0,IRN1,IRN2)=CIM(IRN0,ICN0,IRN1,IRN2)
     1          + SM(IC)*BV(I)/SM(IR)
      GOTO 150
  150 CONTINUE
 190  CONTINUE
C***********************************************************************
C     ADD CONSTANT ENTRIES OF B
C***********************************************************************
  200 CONTINUE
      IF(NZC.EQ.0) GOTO 300
      DO 290 I=1,NZC
      IR=IRC(I)
      IC=ICC(I)
      IRN2 =1+(IR-1)/N0N1
      ICN2 =1+(IC-1)/N0N1
      IRN1 =1+(IR-(IRN2-1)*N0N1-1)/N0
      ICN1 =1+(IC-(ICN2-1)*N0N1-1)/N0
      IRN0 = IR-(IRN1-1)*N0 - (IRN2-1)*N0N1
      ICN0 = IC-(ICN1-1)*N0 - (ICN2-1)*N0N1
      IF(ICN2.EQ.IRN2) GOTO 230
      IF(ICN2.LT.IRN2) GOTO 220
      IF(ICN2.GT.IRN2) GOTO 240
      GOTO 990
C---- IN THIS CASE ENTRY IS IN THE SUB DIAGONAL
  220 CONTINUE
      AIM(IRN0,ICN0,ICN1,ICN2)=AIM(IRN0,ICN0,ICN1,ICN2)
     1          + SM(IC)*BC(I)/SM(IR)
      GOTO 250
C---- IN THIS CASE ENTRY IS IN THE TRI DIAGONAL SYSTEM
  230 CONTINUE
      INDB=ICN1-IRN1
      IF(IABS(INDB).GT.1) GOTO 990
      INDB=INDB+2
      BIM(IRN0,ICN0,IRN1,INDB,IRN2)=BIM(IRN0,ICN0,IRN1,INDB,IRN2)
     1          + SM(IC)*BC(I)/SM(IR)
      GOTO 250
C---- IN THIS CASE ENTRY IS IN THE SUPER DIAGONAL
  240 CONTINUE
      CIM(IRN0,ICN0,IRN1,IRN2)=CIM(IRN0,ICN0,IRN1,IRN2)
     1          + SM(IC)*BC(I)/SM(IR)
      GOTO 250
  250 CONTINUE
 290  CONTINUE
C***********************************************************************
C     RETURN
C***********************************************************************
 300  CONTINUE
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
C---- VARIABLE ENTRY WILL DESTROY BLOCK PENTA DIAGONAL STRUCTURE
  980 IERR=-1
      RETURN
C---- CONSTANT ENTRY WILL DESTROY BLOCK PENTA DIAGONAL STRUCTURE
  990 IERR=-1
      RETURN
C***********************************************************************
C     END OF PITMAT
C***********************************************************************
      END
      SUBROUTINE NNJAC(N,N0,N1,N2,N2M,T,Y,YP,FUN0,
     1      SCL,ETADIF,A,B,C,
     2      NZV,NDV,BVK,IRV,ICV,
     3      NFCNJ,
     4      YH,FUN1,W1,FCN,RPAR,IPAR,
     1            IFCN,IFCNP)
C***********************************************************************
C                                                                      *
C     ************************************************************     *
C     *      NUMERICAL DIFFERENCE APPROXIMATION OF JACOBIAN      *     *
C     *      GIVEN IN NONA BLOCK DIAGONAL FORM                   *     *
C     ************************************************************     *
C                                                                      *
C***********************************************************************
C***********************************************************************
C                                                                      *
C   B B       C C                                                      *
C   B B B     C C C                                                    *
C     B B B     C C C                                                  *
C       B B B     C C C                                                *
C         B B       C C                                                *
C   A A       B B       C C                                            *
C   A A A     B B B     C C C                                          *
C     A A A     B B B     C C C                                        *
C       A A A     B B B     C C C                                      *
C         A A       B B       C C                                      *
C             A A       B B                                            *
C             A A A     B B B                                          *
C               A A A     B B B                                        *
C                 A A A     B B B                                      *
C                   A A       B B                                      *
C                                                                      *
C***********************************************************************
C***********************************************************************
C                                                                      *
C  INPUT PARAMETERS:                                                   *
C                                                                      *
C    N         = DIMENSION OF THE WHOLE SYSTEM N=N0*N1*N2              *
C    N0        = DIMENSION OF QUADRATIC SUB MATRICES                   *
C    N1        = DIMENSION OF QUADRATIC SUB BLOCKS                     *
C    N2        = DIMENSION OF QUADRATIC BLOCKS                         *
C    N2M       = NUMBER OF SIDE BLOCKS                                 *
C    T         = TIME (FOR USE IN SUBROUTINE FCN)                      *
C    Y         = CURRENT VALUE OF VARIABLE                             *
C    YP        = CURRENT VALUE OF FIRST TIME DERIVATIVE OF VARIABLES   *
C    FUN0      = VALUE OF FUNCTION (UNPERTURBED)                       *
C    SCL       = SCALING FACTORS                                       *
C    ETADIF    = VALUE OF PERTURBATION                                 *
C    FCN       = NAME OF CALLED ROUTINE                                *
C    NZV,BVK,IRV,ICV  NOT USED BUT NEEDED FOR FCN                      *
C                                                                      *
C   OUTPUT PARAMETERS:                                                 *
C                                                                      *
C    A(N0,N0,N1,3,N2M) ENTRIES IN JACOBIAN (LOWER SIDE ROW)            *
C    B(N0,N0,N1,3,N2) ENTRIES IN JACOBIAN (INNER BLOCK TRIDIAGONAL S.) *
C    C(N0,N0,N1,3,N2M) ENTRIES IN JACOBIAN (UPPER SIDE ROW)            *
C                                                                      *
C                                                                      *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      EXTERNAL FCN
      DIMENSION Y(N),YP(N),YH(N),FUN0(N),FUN1(N),SCL(N)
      DIMENSION B(N0,N0,N1,3,N2),A(N0,N0,N1,3,N2M),C(N0,N0,N1,3,N2M)
      DIMENSION W1(N)
      DIMENSION BVK(NDV),IRV(NDV),ICV(NDV)
      DIMENSION RPAR(*),IPAR(*),IFCNP(N1,N2)
C***********************************************************************
C     SET VALUES FOR UNPERTURBED FUNCTIONS
C***********************************************************************
      DO 10 I=1,N
      YH(I)=Y(I)
   10 CONTINUE
C---- IFCNP(NB1,NB2) = 0
      DO 20 I=1,N1
      DO 20 J=1,N2
      IFCNP(I,J) = 0
   20 CONTINUE
C***********************************************************************
C     START LOOP
C***********************************************************************
      DO 1000 KH2=1,3
      DO 1000 KH1=1,3
      DO 1000 KH0=1,N0
C***********************************************************************
C
C     PERTURBE FUNCTIONS
C
C***********************************************************************
C***********************************************************************
C     GET INDEX OF FIRST PERTURBED FUNCTION
C***********************************************************************
      K=(KH2-1)*N0*N1+(KH1-1)*N0+KH0
      KFIR=K
C---- WY =IS VALUE FOR PERTURBATION
      WY=ETADIF
C***********************************************************************
C     GET INDEX OF FUNCTIONS PERTURBED AT THE SAME TIME AND PERTURBE
C***********************************************************************
      DO 120 LL=1,N2,3
      NB2=LL+KH2-1
      IF(NB2.GT.N2) GOTO 130
      INC2=(LL-1)*N0*N1
      DO 110 KK=1,N1,3
      NB1=KK+KH1-1
      IF(NB1.GT.N1) GOTO 110
      INC1=(KK-1)*N0
C---- IPER=INDICES OF THE PERTURBED FUNCTIONS
      IPER=KFIR+INC2+INC1
C---- IFCNP(NB1,NB2) = 1 FOR PERTURBED BLOCK
      IFCNP(NB1,NB2) = 1
C---- LEAVE LOOP IF END OF SYSTEM IS REACHED
      IF(IPER.GT.N) GOTO 130
      YH(IPER)=Y(IPER)+SCL(IPER)*WY
 110  CONTINUE
 120  CONTINUE
C
 130  CONTINUE
C***********************************************************************
C
C     CALCULATE PERTURBED FUNCTION
C
C***********************************************************************
      CALL RESLIM (N,NZV,NDV,T,YH,W1,FUN1,BVK,IRV,ICV,YP,FCN,RPAR,IPAR,
     1            IFCN,IFCNP)
      NFCNJ=NFCNJ+1
C***********************************************************************
C
C     RESET PERTURBED FUNCTIONS AND STORE ENTRIES OF JACOBIAN
C
C***********************************************************************
      DO 280 LL=1,N2,3
C---- CALCULATE NUMBER OF BIG BLOCK
      NB2=LL+KH2-1
      IF(NB2.GT.N2) GOTO 280
      INC2=(LL-1)*N0*N1
      DO 270 KK=1,N1,3
C---- CALCULATE NUMBER OF SMALL BLOCK
      NB1=KK+KH1-1
      IF(NB1.GT.N1) GOTO 270
      INC1=(KK-1)*N0
C---- CALCULATE NUMBER OF PERTURBED FUNCTION
      IPER=KFIR+INC2+INC1
      IF(IPER.GT.N) GOTO 290
C---- RESET IFCNP(NB1,NB2)
      IFCNP(NB1,NB2) = 0
C---- RESET PERTURBED FUNCTION
      YH(IPER)=Y(IPER)
C***********************************************************************
C     CALCULATE INDEX OF FIRST ELEMENTS
C***********************************************************************
      IB2=N0*N1*(NB2-1)+N0*(NB1-1)
      IB1=IB2+N0
      IB3=IB2-N0
      IA2=IB2+N0*N1
      IA1=IA2+N0
      IA3=IA2-N0
      IC2=IB2-N0*N1
      IC1=IC2+N0
      IC3=IC2-N0
C***********************************************************************
C
C     STORE ENTRIES OF JACOBIAN IN LEFT TRIDIAGONAL ROW
C
C***********************************************************************
      IF(NB2.GE.N2) GOTO 219
C***********************************************************************
C     STORE ENTRIES OF JACOBIAN IN LEFT DIAGONAL ROW
C***********************************************************************
      IF(NB1.GE.N1) GOTO 206
      DO 205 II=1,N0
      JJ =IA1+II
      A(II,KH0,NB1+1,1,NB2)=
     1                     (FUN0(JJ)-FUN1(JJ))/(WY*SCL(JJ))
 205  CONTINUE
 206  CONTINUE
C***********************************************************************
C     STORE ENTRIES OF JACOBIAN IN CENTRAL DIAGONAL ROW
C***********************************************************************
      DO 210 II=1,N0
      JJ =IA2+II
      A(II,KH0,NB1,2,NB2)=
     1                     (FUN0(JJ)-FUN1(JJ))/(WY*SCL(JJ))
 210  CONTINUE
C***********************************************************************
C     STORE ENTRIES OF JACOBIAN IN RIGHT DIAGONAL ROW
C***********************************************************************
      IF(NB1.LE.1) GOTO 216
      DO 215 II=1,N0
      JJ =IA3+II
      A(II,KH0,NB1-1,3,NB2)=
     1                     (FUN0(JJ)-FUN1(JJ))/(WY*SCL(JJ))
 215  CONTINUE
 216  CONTINUE
 219  CONTINUE
C***********************************************************************
C
C     STORE ENTRIES OF JACOBIAN IN MIDDLE TRIDIAGONAL ROW
C
C***********************************************************************
C***********************************************************************
C     STORE ENTRIES OF JACOBIAN IN LEFT DIAGONAL ROW
C***********************************************************************
      IF(NB1.GE.N1) GOTO 221
      DO 220 II=1,N0
      JJ =IB1+II
      B(II,KH0,NB1+1,1,NB2)=
     1                     (FUN0(JJ)-FUN1(JJ))/(WY*SCL(JJ))
 220  CONTINUE
 221  CONTINUE
C***********************************************************************
C     STORE ENTRIES OF JACOBIAN IN CENTRAL DIAGONAL ROW
C***********************************************************************
      DO 225 II=1,N0
      JJ =IB2+II
      B(II,KH0,NB1,2,NB2)   =
     1                     (FUN0(JJ)-FUN1(JJ))/(WY*SCL(JJ))
 225  CONTINUE
C***********************************************************************
C     STORE ENTRIES OF JACOBIAN IN RIGHT DIAGONAL ROW
C***********************************************************************
      IF(NB1.LE.1) GOTO 231
      DO 230 II=1,N0
      JJ =IB3+II
      B(II,KH0,NB1-1,3,NB2)=
     1                     (FUN0(JJ)-FUN1(JJ))/(WY*SCL(JJ))
 230  CONTINUE
 231  CONTINUE
C***********************************************************************
C
C     STORE ENTRIES OF JACOBIAN IN UPPER TRIDIAGONAL ROW
C
C***********************************************************************
      IF(NB2.LE.1) GOTO 249
C***********************************************************************
C     STORE ENTRIES IN LEFT DIAGONAL ROW
C***********************************************************************
      IF(NB1.GE.N1) GOTO 236
      DO 235 II=1,N0
      JJ =IC1+II
      C(II,KH0,NB1+1,1,NB2-1) =
     1                     (FUN0(JJ)-FUN1(JJ))/(WY*SCL(JJ))
 235  CONTINUE
 236  CONTINUE
C***********************************************************************
C     STORE ENTRIES IN CENTRAL DIAGONAL ROW
C***********************************************************************
      DO 240 II=1,N0
      JJ =IC2+II
      C(II,KH0,NB1,2,NB2-1) =
     1                     (FUN0(JJ)-FUN1(JJ))/(WY*SCL(JJ))
 240  CONTINUE
C***********************************************************************
C     STORE ENTRIES IN RIGHT DIAGONAL ROW
C***********************************************************************
      IF(NB1.LE.1) GOTO 246
      DO 245 II=1,N0
      JJ =IC3+II
      C(II,KH0,NB1-1,3,NB2-1) =
     1                     (FUN0(JJ)-FUN1(JJ))/(WY*SCL(JJ))
 245  CONTINUE
 246  CONTINUE
 249  CONTINUE
C***********************************************************************
C     STORE ENTRIES OF JACOBIAN
C***********************************************************************
 270  CONTINUE
 280  CONTINUE
 290  CONTINUE
1000  CONTINUE
      RETURN
C***********************************************************************
C     END OF PNJAC
C***********************************************************************
      END
      SUBROUTINE NITMAT(N,N0,N1,N2,N2M,G,AJ,BJ,CJ,SM,
     1      NZV,NDV,BV,IRV,ICV,NZC,NDC,BC,IRC,ICC,AIM,BIM,CIM)
C***********************************************************************
C
C     COMPUTATION OF A PENTA BLOCK DIAGONAL ITERATION MATRIX
C     NOTE: THERE IS NO WARNING FLAG, IF ENTRY OF BV OR BC IS IN
C           B(X,X,1,1,X) OR B(X,X,N1,3,X) !!!!!!!!!!!!!!!
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION AJ(N0,N0,N1,3,N2M)
      DIMENSION BJ(N0,N0,N1,3,N2)
      DIMENSION CJ(N0,N0,N1,3,N2M)
      DIMENSION AIM(N0,N0,N1,3,N2M)
      DIMENSION BIM(N0,N0,N1,3,N2)
      DIMENSION CIM(N0,N0,N1,3,N2M)
      DIMENSION SM(N)
      DIMENSION BV(NDV),IRV(NDV),ICV(NDV)
      DIMENSION BC(NDC),IRC(NDC),ICC(NDC)
C***********************************************************************
C
C***********************************************************************
      N1M=N1-1
      N0N1=N0*N1
C***********************************************************************
C     CALCULATE JACOBIAN PART FOR INNER TRIDIAGONAL MATRIX
C***********************************************************************
      DO 50 L=1,N2
      DO 10 K=2,N1
      DO 10 J=1,N0
      DO 10 I=1,N0
      BIM(I,J,K,1,L)=G*BJ(I,J,K,1,L)
   10 CONTINUE
      DO 20 K=1,N1
      DO 20 J=1,N0
      DO 20 I=1,N0
      BIM(I,J,K,2,L)=G*BJ(I,J,K,2,L)
   20 CONTINUE
      DO 30 K=1,N1M
      DO 30 J=1,N0
      DO 30 I=1,N0
      BIM(I,J,K,3,L)=G*BJ(I,J,K,3,L)
   30 CONTINUE
   50 CONTINUE
C***********************************************************************
C     CALCULATE JACOBIAN PART FOR SUB AND SUPER ROWS
C***********************************************************************
C---- CHECK IF WE HAVE ONLY A BLOCK TRIDIAGONAL SYSTEM
      IF(N2.EQ.1) GOTO 100
      DO 90 L=1,N2M
      DO 60 K=2,N1
      DO 60 J=1,N0
      DO 60 I=1,N0
      AIM(I,J,K,1,L)=G*AJ(I,J,K,1,L)
      CIM(I,J,K,1,L)=G*CJ(I,J,K,1,L)
   60 CONTINUE
      DO 70 K=1,N1
      DO 70 J=1,N0
      DO 70 I=1,N0
      AIM(I,J,K,2,L)=G*AJ(I,J,K,2,L)
      CIM(I,J,K,2,L)=G*CJ(I,J,K,2,L)
   70 CONTINUE
      DO 80 K=1,N1M
      DO 80 J=1,N0
      DO 80 I=1,N0
      AIM(I,J,K,3,L)=G*AJ(I,J,K,3,L)
      CIM(I,J,K,3,L)=G*CJ(I,J,K,3,L)
   80 CONTINUE
   90 CONTINUE
C***********************************************************************
C     ADD VARIABLE ENTRIES OF B
C***********************************************************************
  100 CONTINUE
      IF(NZV.EQ.0) GOTO 200
      DO 190 I=1,NZV
      IR=IRV(I)
      IC=ICV(I)
      IRN2 =1+(IR-1)/N0N1
      ICN2 =1+(IC-1)/N0N1
      IRN1 =1+(IR-(IRN2-1)*N0N1-1)/N0
      ICN1 =1+(IC-(ICN2-1)*N0N1-1)/N0
      IRN0 = IR-(IRN1-1)*N0 - (IRN2-1)*N0N1
      ICN0 = IC-(ICN1-1)*N0 - (ICN2-1)*N0N1
      IF(ICN2.EQ.IRN2) GOTO 130
      IF(ICN2.LT.IRN2) GOTO 120
      IF(ICN2.GT.IRN2) GOTO 140
      GOTO 980
C---- IN THIS CASE ENTRY IS IN THE SUB DIAGONAL
  120 CONTINUE
      INDB=ICN1-IRN1
      IF(IABS(INDB).GT.1) GOTO 980
      INDB=INDB+2
      AIM(IRN0,ICN0,ICN1,INDB,ICN2)=AIM(IRN0,ICN0,ICN1,INDB,ICN2)
     1          + SM(IC)*BV(I)/SM(IR)
      GOTO 150
C---- IN THIS CASE ENTRY IS IN THE TRI DIAGONAL SYSTEM
  130 CONTINUE
      INDB=ICN1-IRN1
      IF(IABS(INDB).GT.1) GOTO 980
      INDB=INDB+2
      BIM(IRN0,ICN0,IRN1,INDB,IRN2)=BIM(IRN0,ICN0,IRN1,INDB,IRN2)
     1          + SM(IC)*BV(I)/SM(IR)
      GOTO 150
C---- IN THIS CASE ENTRY IS IN THE SUPER DIAGONAL
  140 CONTINUE
      INDB=ICN1-IRN1
      IF(IABS(INDB).GT.1) GOTO 980
      INDB=INDB+2
      CIM(IRN0,ICN0,IRN1,INDB,IRN2)=CIM(IRN0,ICN0,IRN1,INDB,IRN2)
     1          + SM(IC)*BV(I)/SM(IR)
      GOTO 150
  150 CONTINUE
 190  CONTINUE
C***********************************************************************
C     ADD CONSTANT ENTRIES OF B
C***********************************************************************
  200 CONTINUE
      IF(NZC.EQ.0) GOTO 300
      DO 290 I=1,NZC
      IR=IRC(I)
      IC=ICC(I)
      IRN2 =1+(IR-1)/N0N1
      ICN2 =1+(IC-1)/N0N1
      IRN1 =1+(IR-(IRN2-1)*N0N1-1)/N0
      ICN1 =1+(IC-(ICN2-1)*N0N1-1)/N0
      IRN0 = IR-(IRN1-1)*N0 - (IRN2-1)*N0N1
      ICN0 = IC-(ICN1-1)*N0 - (ICN2-1)*N0N1
      IF(ICN2.EQ.IRN2) GOTO 230
      IF(ICN2.LT.IRN2) GOTO 220
      IF(ICN2.GT.IRN2) GOTO 240
      GOTO 990
C---- IN THIS CASE ENTRY IS IN THE SUB DIAGONAL
  220 CONTINUE
      INDB=ICN1-IRN1
      IF(IABS(INDB).GT.1) GOTO 990
      INDB=INDB+2
      AIM(IRN0,ICN0,ICN1,INDB,ICN2)=AIM(IRN0,ICN0,ICN1,INDB,ICN2)
     1          + SM(IC)*BC(I)/SM(IR)
      GOTO 250
C---- IN THIS CASE ENTRY IS IN THE TRI DIAGONAL SYSTEM
  230 CONTINUE
      INDB=ICN1-IRN1
      IF(IABS(INDB).GT.1) GOTO 990
      INDB=INDB+2
      BIM(IRN0,ICN0,IRN1,INDB,IRN2)=BIM(IRN0,ICN0,IRN1,INDB,IRN2)
     1          + SM(IC)*BC(I)/SM(IR)
      GOTO 250
C---- IN THIS CASE ENTRY IS IN THE SUPER DIAGONAL
  240 CONTINUE
      INDB=ICN1-IRN1
      IF(IABS(INDB).GT.1) GOTO 990
      INDB=INDB+2
      CIM(IRN0,ICN0,IRN1,INDB,IRN2)=CIM(IRN0,ICN0,IRN1,INDB,IRN2)
     1          + SM(IC)*BC(I)/SM(IR)
      GOTO 250
  250 CONTINUE
 290  CONTINUE
C***********************************************************************
C     RETURN
C***********************************************************************
 300  CONTINUE
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
C---- VARIABLE ENTRY WILL DESTROY BLOCK PENTA DIAGONAL STRUCTURE
  980 IERR=-1
      RETURN
C---- CONSTANT ENTRY WILL DESTROY BLOCK PENTA DIAGONAL STRUCTURE
  990 IERR=-1
      RETURN
C***********************************************************************
C     END OF NITMAT
C***********************************************************************
      END
      SUBROUTINE JACOB(IANA,IMAT,FCN,JAC,N,T,Y,YP,DEL0,
     1        SM,ETADIF,ETA,EPDIFF,EPMIN,FMIN,
     1        DEL,W1,W2,W3,NZV,NDV,BV,IRV,ICV,
     2        NFCNJ,NDJ,A,RPAR,IPAR,
     1            IFCN,IFCNP)
C***********************************************************************
C                                                                      *
C     ************************************************************     *
C     *                                                          *     *
C     *  MANAGEMENT OF THE EVALUATION OF JACOBIANS (SCALED)      *     *
C     *                                                          *     *
C     *                AUTHOR: ULRICH MAAS  2.10.1987            *     *
C     *                                                          *     *
C     *                LAST CHANGE:  2.10.1987/MAAS              *     *
C     *                                                          *     *
C     ************************************************************     *
C                                                                      *
C***********************************************************************
C                                                                      *
C     NOTE: NO CHECK FOR SUFFICIENT ARRAY STORAGE PERFORMED!!!         *
C                                                                      *
C***********************************************************************
C                                                                      *
C     THIS SUBROUTINE COMPUTES THE JACOBIAN DG/DY FOR A SYSTEM OF THE  *
C     FORM:                                                            *
C                                                                      *
C     G(Y,YP,T) = F(Y,T) - BV(Y,T)*YP                                  *
C                                                                      *
C     OUTPUT IS THE NEGATIVE OF THE SCALED JACOBIAN:                   *
C                                                                      *
C       A(Y,YP,T)= - (SM) * DG/DY * (SM)T                              *
C                                                                      *
C     WHERE (SM)T DENOTES THE TRANSPOSED OF THE SCALING VECTOR         *
C                                                                      *
C                                                                      *
C     PROVIDE A SUBROUTINE OF THE FORM:                                *
C                                                                      *
C     FCN(N,NZV,T,Y,F,BV,IR,IC,RPAR,IPAR)                              *
C                                                                      *
C     WHICH COMPUTES THE RESIDUAL F FOR GIVEN T,Y,YP                   *
C       MEANING OF VARIABLES: SEE BELOW                                *
C                                                                      *
C     BV,YP AND T ARE INCLUDED TO ENABLED HANDLING OF JACOBIANS FOR    *
C     TIME DEPENDENT PROBLEMS. SET NZV=0 AND PASS DUMMY ARRAYS FOR     *
C     YP,BV,IRV,ICV IN YOUR CALLING ROUTINE TO OBTAIN JACOBIANS FOR    *
C     SYSTEMS OF THE FORM G(Y)=F(Y)                                    *
C                                                                      *
C     IN THIS CASE ALSO DIMENSION DUMMY ARRAYS FOR BV,IR,IC IN YOUR    *
C     SUBROUTINE FCN                                                   *
C
C***********************************************************************
C                                                                      *
C     INPUT DATA:                                                      *
C                                                                      *
C      IANA         INFORMATION ABOUT EVALUATION OF JACOBIAN           *
C                     0 NUMERICAL EVALUATION                           *
C                     1 ANALYTICAL EVALUATION                          *
C                                                                      *
C      IMAT(10)     INFORMATION ABOUT MATRIX (SEE BELOW)               *
C                                                                      *
C      FCN          NAME OF SUBROUTINE FOR RESIDUALS                   *
C                                                                      *
C      JAC          NAME OF SUBROUTINE FOR ANALYTICAL EVALUATION       *
C                     (PASS A DUMMY NAME FOR NUMERICAL EVALUATION)     *
C                                                                      *
C      N            DIMENSION OF THE SYSTEM                            *
C                                                                      *
C      T            CURRENT VALUE OF INDEPENDENT VARIABLE              *
C                                                                      *
C      Y            DEPENDENT VARIABLES ARRAY                          *
C                                                                      *
C      YP           ARRAY OF FIRST DERIVATIVES OF DEPENDENT VARIABLES  *
C                                                                      *
C      DEL0         ARRAY OF RESIDUALS (UNSCALED, UNPERTURBED)         *
C                                                                      *
C      SM           ARRAY OF SCALING FACTORS                           *
C                                                                      *
C      NZV          NUMBER OF ENTRIES OF BV                            *
C                                                                      *
C      BV(NZV)      ENTRIES OF VARIABLE PART BV OF LEFT-HAND-SIDE      *
C                                                                      *
C      IRV(NZV)     INTEGER ARRAY CONTAINING ROW-INDICES OF BV         *
C                                                                      *
C      ICV(NZV)     INTEGER ARRAY CONTAINING COLUMN-INDICES OF BV      *
C                                                                      *
C      NFCN         NUMBER OF FUNCTION EVALUATIONS NEEDED (INCREMENTED)*
C                                                                      *
C      RPAR(1)      ARRAY FOR USER COMMUNICATION WITH SUBROUTINE FCN   *
C                                                                      *
C      IPAR(1)      ARRAY FOR USER COMMUNICATION WITH SUBROUTINE FCN   *
C                                                                      *
C                                                                      *
C     OUTPUT DATA:                                                     *
C                                                                      *
C      A            SCALED JACOBIAN                                    *
C                                                                      *
C                                                                      *
C     WORKING ARRAYS:                                                  *
C                                                                      *
C      W1(N),W2(N),W3(N),DEL(N)                                        *
C                                                                      *
C                     DENSE MATRIX: W2 AND DEL NEEDED                  *
C                     BANDED MATRIX: W1,W2,W3, AND DEL NEEDED          *
C                     OTHERS      : W1,W3, AND DEL NEEDED              *
C                                                                      *
C***********************************************************************
C                                                                      *
C     ENTRIES IN IMAT:                                                 *
C                                                                      *
C       IMAT(1): KIND OF MATRIX                                        *
C                                                                      *
C                  IMAT(1)=0  DENSE MATRIX                             *
C                  IMAT(1)=1  BANDED MATRIX                            *
C                  IMAT(1)=2  BLOCK TRIDIAGONAL MATRIX                 *
C                  IMAT(1)=3  BLOCK PENTADIAGONAL MATRIX               *
C                  IMAT(1)=4  BLOCK NONADIAGONAL MATRIX                *
C                                                                      *
C       IMAT(2): DIMENSION OF THE MATRIX                               *
C                                                                      *
C       IMAT(3-6) INFORMATIONS ABOUT SIZE OF MATRIX                    *
C       IF IMAT(1)=0 IMAT(3-6) NOT NEEDED                              *
C       IF IMAT(1)=1 IMAT(3) = LOWER BAND WIDTH                        *
C                    IMAT(4) = UPPER BAND WIDTH                        *
C                    IMAT(5) = NOT NEEDED                              *
C                    IMAT(6) = LEEDING DIMENSION OF MATRIX (>= ML+MU+1)*
C                    JACOB USES ROWS IMAT(6)-(ML+MU) TO IMAT(6)        *
C       IF IMAT(1)=2 IMAT(3) = DIMENSION OF SUBBLOCKS                  *
C                    IMAT(4) = NUMBER OF SUBBLOCKS                     *
C                    IMAT(5) = NOT NEEDED                              *
C                    IMAT(6) = NOT NEEDED                              *
C       IF IMAT(1)=3 IMAT(3) = DIMENSION OF SUBSUBBLOCK                *
C                    IMAT(4) = NUMBER OF SUBSUBBLOCKS                  *
C                    IMAT(5) = NUMBER OF SUBBLOCKS                     *
C                    IMAT(6) = NOT NEEDED                              *
C       IF IMAT(1)=4 IMAT(3) = DIMENSION OF SUBSUBBLOCK                *
C                    IMAT(4) = NUMBER OF SUBSUBBLOCKS                  *
C                    IMAT(5) = NUMBER OF SUBBLOCKS                     *
C                    IMAT(6) = NOT NEEDED                              *
C***********************************************************************
C     SUBROUTINE FOR THE EVALUATION OF THE JACOBIAN (SCALED)           *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      EXTERNAL FCN,JAC
      DIMENSION Y(N),YP(N),DEL0(N),DEL(N),SM(N),ETA(N),
     1          W1(N),W2(N),W3(N)
      DIMENSION A(NDJ)
      DIMENSION BV(NDV),IRV(NDV),ICV(NDV)
      DIMENSION RPAR(*),IPAR(*),IFCNP(*)
      DIMENSION IMAT(10)
C***********************************************************************
C     CHECK FOR ANALYTICAL OR NUMERICAL JACOBIAN
C***********************************************************************
      IERR=0
      IF(IANA.EQ.1) GOTO 1000
      IFCN = 1
C***********************************************************************
C
C     NUMERICAL DIFFERENCE APPROXIMATION OF JACOBIAN
C
C***********************************************************************
      IF(IMAT(1).EQ.0) GOTO 100
      IF(IMAT(1).EQ.1) GOTO 200
      IF(IMAT(1).EQ.2) GOTO 300
      IF(IMAT(1).EQ.3) GOTO 400
      IF(IMAT(1).EQ.4) GOTO 500
      IF(IMAT(1).EQ.5) GOTO 600
      GOTO 910
C***********************************************************************
C     DENSE JACOBIAN
C***********************************************************************
  100 CONTINUE
      CALL DNJAC(N,T,Y,YP,DEL0,DEL,
     1    SM,ETA,EPDIFF,EPMIN,FMIN,A,
     1    NZV,NDV,BV,IRV,ICV,NFCNJ,W2,FCN,RPAR,IPAR,
     1            IFCN,IFCNP)
      GOTO 3000
C***********************************************************************
C     BANDED JACOBIAN
C***********************************************************************
  200 CONTINUE
      ML=IMAT(3)
      MU=IMAT(4)
      MB=ML+MU+1
      MBJ=IMAT(6)
      CALL BNJAC(N,MBJ,MB,ML,MU,T,Y,YP,DEL0,
     1      SM,ETA,EPDIFF,EPMIN,FMIN,A,NZV,NDV,BV,IRV,ICV,
     2      NFCNJ,W3,DEL,W1,W2,FCN,RPAR,IPAR,
     1            IFCN,IFCNP)
      GOTO 3000
C***********************************************************************
C     BLOCK TRIDIAGONAL JACOBIAN
C***********************************************************************
  300 CONTINUE
      ND0=IMAT(3)
      ND1=IMAT(4)
      CALL TNJAC(N,ND0,ND1,T,Y,YP,DEL0,
     1      SM,ETADIF,A,NZV,NDV,BV,IRV,ICV,NFCNJ,
     4      W3,DEL,W1,W2,FCN,RPAR,IPAR,
     1            IFCN,IFCNP)
      GOTO 3000
C***********************************************************************
C     BLOCK PENTADIAGONAL JACOBIAN
C***********************************************************************
  400 CONTINUE
      ND0=IMAT(3)
      ND1=IMAT(4)
      ND2=IMAT(5)
C---- FAKE ARRAY FOR BLOCK TRIDIAGONAL SYSTEM
      ND2M=ND2-1
      IF(ND2.EQ.1) ND2M=1
      NDAC= ND0*ND0*ND1*ND2M
      NDB = ND0*ND0*ND1*ND2*3
      NJ2 = 1+NDAC
      NJ3 = NJ2+NDB
      CALL PNJAC(N,ND0,ND1,ND2,ND2M,T,Y,YP,DEL0,
     1      SM,ETADIF,A(1),A(NJ2),A(NJ3),
     2      NZV,NDV,BV,IRV,ICV,NFCNJ,
     4      W3,DEL,W1,FCN,RPAR,IPAR,
     1            IFCN,IFCNP)
      GOTO 3000
C***********************************************************************
C     BLOCK NONADIAGONAL JACOBIAN
C***********************************************************************
  500 CONTINUE
      ND0=IMAT(3)
      ND1=IMAT(4)
      ND2=IMAT(5)
      ND2M=ND2-1
      IF(ND2.EQ.1) ND2M=1
      NDAC= ND0*ND0*ND1*3*ND2M
      NDB = ND0*ND0*ND1*ND2*3
      NJ2 = 1+NDAC
      NJ3 = NJ2+NDB
      CALL NNJAC(N,ND0,ND1,ND2,ND2M,T,Y,YP,DEL0,
     1      SM,ETADIF,A(1),A(NJ2),A(NJ3),
     2      NZV,NDV,BV,IRV,ICV,NFCNJ,
     4      W3,DEL,W1,FCN,RPAR,IPAR,
     1            IFCN,IFCNP)
      GOTO 3000
C***********************************************************************
C     BLOCK TRIDIAGONAL JACOBIAN WITH CORNERS
C***********************************************************************
  600 CONTINUE
      ND0=IMAT(3)
      ND1=IMAT(4)
      NDT= ND0 * ND0 * ND1 * 3
      CALL CTNJAC(N,ND0,ND1,T,Y,YP,DEL0,
     1      SM,ETADIF,A(1),A(NDT+1),NZV,NDV,BV,IRV,ICV,NFCNJ,
     4      W3,DEL,W1,W2,FCN,RPAR,IPAR,
     1            IFCN,IFCNP)
      GOTO 3000
C***********************************************************************
C                     ANALYTIC EXPRESSION OF JACOBIAN
C***********************************************************************
 1000 CONTINUE
      IFCN = 0
      IF(IMAT(1).EQ.0) GOTO 1100
      IF(IMAT(1).EQ.1) GOTO 1200
      GOTO 920
C***********************************************************************
C     DENSE JACOBIAN
C***********************************************************************
 1100 CONTINUE
      CALL DAJAC(N,T,Y,SM,A,JAC,RPAR,IPAR,IFCN,IFCNP)
      GOTO 3000
C***********************************************************************
C     BANDED JACOBIAN
C***********************************************************************
 1200 CONTINUE
      ML=IMAT(3)
      MU=IMAT(4)
      MB=ML+MU+1
      CALL BAJAC(N,ML,MU,MB,T,Y,SM,A,JAC,RPAR,IPAR,IFCN,IFCNP)
      GOTO 3000
C***********************************************************************
C     REGULAR EXIT
C***********************************************************************
 3000 CONTINUE
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
  910 CONTINUE
      IERR=IMAT(1)
      RETURN
  920 CONTINUE
      IERR=-IMAT(1)
      RETURN
C***********************************************************************
C     END OF JACOB
C***********************************************************************
      END
      SUBROUTINE ITMAT(IMAT,N,G,NDJ,A,SM,
     1        NZV,NDV,BV,IRV,ICV,NZC,NDC,BC,IRC,ICC,NDI,B)
C***********************************************************************
C                                                                      *
C     SUBROUTINE FOR THE EVALUATION OF THE JACOBIAN (SCALED)           *
C                                                                      *
C***********************************************************************
C     SUBROUTINE FOR THE EVALUATION OF THE JACOBIAN (SCALED)           *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION SM(N),A(NDJ),B(NDI)
      DIMENSION BV(NDV),IRV(NDV),ICV(NDV)
      DIMENSION BC(NDC),IRC(NDC),ICC(NDC)
      DIMENSION IMAT(10)
C***********************************************************************
C     INITIALIZE
C***********************************************************************
      IERR=0
C***********************************************************************
C     NUMERICAL DIFFERENCE APPROXIMATION OF JACOBIAN
C***********************************************************************
      IF(IMAT(1).EQ.0) GOTO 100
      IF(IMAT(1).EQ.1) GOTO 200
      IF(IMAT(1).EQ.2) GOTO 300
      IF(IMAT(1).EQ.3) GOTO 400
      IF(IMAT(1).EQ.4) GOTO 500
      IF(IMAT(1).EQ.5) GOTO 600
      GOTO 910
C***********************************************************************
C     DENSE JACOBIAN
C***********************************************************************
  100 CONTINUE
      CALL DITMAT(N,G,A,SM,
     1              NZV,NDV,BV,IRV,ICV,NZC,NDC,BC,IRC,ICC,B)
      GOTO 3000
C***********************************************************************
C     DENSE JACOBIAN
C***********************************************************************
  200 CONTINUE
      ML=IMAT(3)
      MU=IMAT(4)
      MB=ML+MU+1
      MBH=2*ML+MU+1
      CALL BITMAT(N,ML,MU,MB,MBH,G,A,
     1              SM,NZV,NDV,BV,IRV,ICV,NZC,NDC,BC,IRC,ICC,B)
      GOTO 3000
C***********************************************************************
C     BLOCK TRIDIAGONAL JACOBIAN
C***********************************************************************
  300 CONTINUE
      ND0=IMAT(3)
      ND1=IMAT(4)
      CALL TITMAT(N,ND0,ND1,G,A,
     1              SM,NZV,NDV,BV,IRV,ICV,NZC,NDC,BC,IRC,ICC,B)
      GOTO 3000
C***********************************************************************
C     BLOCK PENTADIAGONAL JACOBIAN
C***********************************************************************
  400 CONTINUE
      ND0=IMAT(3)
      ND1=IMAT(4)
      ND2=IMAT(5)
C---- FAKE ARRAY FOR BLOCK TRIDIAGONAL SYSTEM
      ND2M=ND2-1
      IF(ND2.EQ.1) ND2M=1
      NDAC= ND0*ND0*ND1*ND2M
      NDB = ND0*ND0*ND1*ND2*3
      NJ2 = 1+NDAC
      NJ3 = NJ2+NDB
      CALL PITMAT(N,ND0,ND1,ND2,ND2M,G,A(1),A(NJ2),A(NJ3),
     1              SM,NZV,NDV,BV,IRV,ICV,NZC,NDC,BC,IRC,ICC,
     2              B(1),B(NJ2),B(NJ3))
      GOTO 3000
C***********************************************************************
C     BLOCK NONADIAGONAL JACOBIAN
C***********************************************************************
  500 CONTINUE
      ND0=IMAT(3)
      ND1=IMAT(4)
      ND2=IMAT(5)
      ND2M=ND2-1
      IF(ND2.EQ.1) ND2M=1
      NDAC= ND0*ND0*ND1*3*ND2M
      NDB = ND0*ND0*ND1*ND2*3
      NJ2 = 1+NDAC
      NJ3 = NJ2+NDB
CNEW  CALL NITMAT(N,ND0,ND1,ND2,ND2M,G,A(1),A(NJ2),A(NJ3),
CNEW 1              SM,NZV,NDV,BV,IRV,ICV,NZC,NDC,BC,IRC,ICC,
CNEW 2              B(1),B(NJ2),B(NJ3))
      CALL XITMAT(N,ND0,ND1,ND2,ND2M,G,A(1),A(NJ2),A(NJ3),
     1              SM,NZV,NDV,BV,IRV,ICV,NZC,NDC,BC,IRC,ICC,
     2              B)
      GOTO 3000
C***********************************************************************
C     BLOCK TRIDIAGONAL JACOBIAN WITH CORNERS
C***********************************************************************
  600 CONTINUE
      ND0=IMAT(3)
      ND1=IMAT(4)
      NDT= ND0*ND0*ND1*3
      CALL CITMAT(N,ND0,ND1,G,A(1),A(NDT+1),
     1    SM,NZV,NDV,BV,IRV,ICV,NZC,NDC,BC,IRC,ICC,B(1),B(NDT+1))
      GOTO 3000
C***********************************************************************
C     REGULAR EXIT
C***********************************************************************
 3000 CONTINUE
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
  910 CONTINUE
      IERR=IMAT(1)
      RETURN
C***********************************************************************
C     END OF ITMAT
C***********************************************************************
      END
      SUBROUTINE ORHSVG (N,NZV,NDV,NGES,NUMP,NDP,
     1          TH,Y,DEL,W1,W2,BVK,IRV,ICV,
     1                  SM,ETA,KONVQ,FCN,IMAT,RPAR,IPAR,
     1            IFCN,IFCNP)
C***********************************************************************
C
C     EVALUATION OF RIGHT HAND SIDE FOR SENSITIVITY EQUATION
C
C***********************************************************************
C***********************************************************************
C     STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      EXTERNAL FCN
      DIMENSION Y(NGES),DEL(NGES),W1(N),W2(N),SM(N),ETA(N)
      DIMENSION KONVQ(NDP)
      DIMENSION BVK(NDV),IRV(NDV),ICV(NDV)
      DIMENSION RPAR(*),IPAR(*),IFCNP(*)
      DIMENSION IMAT(10)
C***********************************************************************
C     EVALUATION OF RIGHT HAND SIDE FOR SENSITIVITY EQUATION
C***********************************************************************
      NH=NUMP*N
      DO 10 I=1,NH
10    DEL(N+I)=0.D0
C
C  EVALUATE JACOBIAN DF/DY
C
      DO 200 K=1,N
      W=Y(K)
      U=SM(K)*ETA(K)
C     ZXC=DMAX1(DABS(W),1.D-9)
C     U=ZXC*ETA(K)
      Y(K)=Y(K)+U
      CALL FCN (N,NZV,TH,Y,W1,BVK,IRV,ICV,RPAR,IPAR,IFCN,IFCNP)
      Y(K)=W
      DO 100 I=1,N
100   W2(I)=(W1(I)-DEL(I))/U
C
C  MATRIX*VECTOR
C
      DO 500 KK=1,NUMP
      IF(KONVQ(KK).GE.1) GOTO 500
      L=KK*N
      DO 400 I=1,N
400   DEL(L+I)=DEL(L+I)+W2(I)*Y(L+K)
500   CONTINUE
200   CONTINUE
C
C  EVALUATE AND ADD DF/DP
C
      DO 900 KK=1,NUMP
      IF(KONVQ(KK).GE.1) GOTO 900
      L=KK*N
      W=RPAR(KK)
      U=RPAR(KK)*1.D-7
      RPAR(KK)=RPAR(KK)+U
      CALL FCN (N,NZV,TH,Y,W1,BVK,IRV,ICV,RPAR,IPAR,IFCN,IFCNP)
      RPAR(KK)=W
      DO 800 I=1,N
800   DEL(L+I)=DEL(L+I)+(W1(I)-DEL(I))/U
900   CONTINUE
C
      RETURN
      END
      SUBROUTINE SCAJAC(IANA,IMAT,N,SM,NDJ,A)
C***********************************************************************
C                                                                      *
C     ************************************************************     *
C     *                                                          *     *
C     *        SCALING AND DESCALING OF  JACOBIANS               *     *
C     *                                                          *     *
C     *                AUTHOR: ULRICH MAAS                       *     *
C     *                                                          *     *
C     *                LAST CHANGE:  5.15.1987/MAAS              *     *
C     *                                                          *     *
C     ************************************************************     *
C                                                                      *
C***********************************************************************
C                                                                      *
C     NOTE: NO CHECK FOR SUFFICIENT ARRAY STORAGE PERFORMED!!!         *
C                                                                      *
C***********************************************************************
C                                                                      *
C                                                                      *
C     OUTPUT IS THE NEGATIVE OF THE SCALED JACOBIAN:                   *
C                                                                      *
C       A(Y,YP,T)= - (SM) * DG/DY * (SM)T                              *
C                                                                      *
C     WHERE (SM)T DENOTES THE TRANSPOSED OF THE SCALING VECTOR         *
C                                                                      *
C                                                                      *
C                                                                      *
C                                                                      *
C      N            DIMENSION OF THE SYSTEM                            *
C                                                                      *
C                                                                      *
C      SM           ARRAY OF SCALING FACTORS                           *
C                                                                      *
C                                                                      *
C                                                                      *
C     OUTPUT DATA:                                                     *
C                                                                      *
C      A            SCALED OR DESCALED JACOBIAN                        *
C                                                                      *
C                                                                      *
C     WORKING ARRAYS:                                                  *
C                                                                      *
C      W1(N),W2(N),W3(N),DEL(N)                                        *
C                                                                      *
C                                                                      *
C***********************************************************************
C                                                                      *
C     ENTRIES IN IMAT:                                                 *
C                                                                      *
C       IMAT(1): KIND OF MATRIX                                        *
C                                                                      *
C                  IMAT(1)=0  DENSE MATRIX                             *
C                  IMAT(1)=1  BANDED MATRIX                            *
C                  IMAT(1)=2  BLOCK TRIDIAGONAL MATRIX                 *
C                  IMAT(1)=3  BLOCK PENTADIAGONAL MATRIX               *
C                  IMAT(1)=4  BLOCK NONADIAGONAL MATRIX                *
C                                                                      *
C       IMAT(2): DIMENSION OF THE MATRIX                               *
C                                                                      *
C       IMAT(3-6) INFORMATIONS ABOUT SIZE OF MATRIX                    *
C       IF IMAT(1)=0 IMAT(3-6) NOT NEEDED                              *
C       IF IMAT(1)=1 IMAT(3) = LOWER BAND WIDTH                        *
C                    IMAT(4) = UPPER BAND WIDTH                        *
C                    IMAT(5) = NOT NEEDED                              *
C                    IMAT(6) = LEEDING DIMENSION OF MATRIX (>= ML+MU+1)*
C                    JACOB USES ROWS IMAT(6)-(ML+MU) TO IMAT(6)        *
C       IF IMAT(1)=2 IMAT(3) = DIMENSION OF SUBBLOCKS                  *
C                    IMAT(4) = NUMBER OF SUBBLOCKS                     *
C                    IMAT(5) = NOT NEEDED                              *
C                    IMAT(6) = NOT NEEDED                              *
C       IF IMAT(1)=3 IMAT(3) = DIMENSION OF SUBSUBBLOCK                *
C                    IMAT(4) = NUMBER OF SUBSUBBLOCKS                  *
C                    IMAT(5) = NUMBER OF SUBBLOCKS                     *
C                    IMAT(6) = NOT NEEDED                              *
C       IF IMAT(1)=4 IMAT(3) = DIMENSION OF SUBSUBBLOCK                *
C                    IMAT(4) = NUMBER OF SUBSUBBLOCKS                  *
C                    IMAT(5) = NUMBER OF SUBBLOCKS                     *
C                    IMAT(6) = NOT NEEDED                              *
C***********************************************************************
C     SUBROUTINE FOR THE EVALUATION OF THE JACOBIAN (SCALED)           *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION SM(N),A(NDJ)
      DIMENSION IMAT(10)
C***********************************************************************
C
C     SCALING OF JACOBIAN
C
C***********************************************************************
      IF(IMAT(1).EQ.0) GOTO 100
      IF(IMAT(1).EQ.1) GOTO 200
      IF(IMAT(1).EQ.2) GOTO 300
      IF(IMAT(1).EQ.3) GOTO 400
      IF(IMAT(1).EQ.4) GOTO 500
      IF(IMAT(1).EQ.5) GOTO 600
      GOTO 910
C***********************************************************************
C     DENSE JACOBIAN
C***********************************************************************
  100 CONTINUE
      CALL DJSCAL(N,SM,A)
      GOTO 3000
C***********************************************************************
C     BANDED JACOBIAN
C***********************************************************************
  200 CONTINUE
      ML=IMAT(3)
      MU=IMAT(4)
      MB=ML+MU+1
      MBJ=IMAT(6)
      CALL BJSCAL(N,MBJ,MB,ML,MU,SM,A)
      GOTO 3000
C***********************************************************************
C     BLOCK TRIDIAGONAL JACOBIAN
C***********************************************************************
  300 CONTINUE
      ND0=IMAT(3)
      ND1=IMAT(4)
      CALL TJSCAL(N,ND0,ND1,SM,A)
      GOTO 3000
C***********************************************************************
C     BLOCK PENTADIAGONAL JACOBIAN
C***********************************************************************
  400 CONTINUE
      ND0=IMAT(3)
      ND1=IMAT(4)
      ND2=IMAT(5)
C---- FAKE ARRAY FOR BLOCK TRIDIAGONAL SYSTEM
      ND2M=ND2-1
      IF(ND2.EQ.1) ND2M=1
      NDAC= ND0*ND0*ND1*ND2M
      NDB = ND0*ND0*ND1*ND2*3
      NJ2 = 1+NDAC
      NJ3 = NJ2+NDB
      CALL PJSCAL(N,ND0,ND1,ND2,ND2M,SM,A(1),A(NJ2),A(NJ3))
      GOTO 3000
C***********************************************************************
C     BLOCK NONADIAGONAL JACOBIAN
C***********************************************************************
  500 CONTINUE
      ND0=IMAT(3)
      ND1=IMAT(4)
      ND2=IMAT(5)
      ND2M=ND2-1
      IF(ND2.EQ.1) ND2M=1
      NDAC= ND0*ND0*ND1*3*ND2M
      NDB = ND0*ND0*ND1*ND2*3
      NJ2 = 1+NDAC
      NJ3 = NJ2+NDB
      CALL NJSCAL(N,ND0,ND1,ND2,ND2M,SM,A(1),A(NJ2),A(NJ3))
      GOTO 3000
C***********************************************************************
C     BLOCK TRIDIAGONAL JACOBIAN WITH CORNERS
C***********************************************************************
  600 CONTINUE
      ND0=IMAT(3)
      ND1=IMAT(4)
      NDT=    ND0 * ND0 * ND1 * 3
      CALL CJSCAL(N,ND0,ND1,SM,A(1),A(NDT+1))
      GOTO 3000
C***********************************************************************
C     REGULAR EXIT
C***********************************************************************
 3000 CONTINUE
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
  910 CONTINUE
      IERR=IMAT(1)
      RETURN
C 920 CONTINUE
C     IERR=-IMAT(1)
C     RETURN
C***********************************************************************
C     END OF JACOB
C***********************************************************************
      END
      SUBROUTINE DJSCAL(N,SM,A)
C***********************************************************************
C
C     SCALING OF A DENSE JACOBIAN
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION SM(N),A(N,N)
C
C   DENSE JACOBIAN
C   --------------
C
      DO 10 K=1,N
      DO 10 I=1,N
      A(I,K)=SM(K)*A(I,K)/SM(I)
  10  CONTINUE
      RETURN
C***********************************************************************
C     END OF DNJAC
C***********************************************************************
      END
      SUBROUTINE BJSCAL(N,MBH,MB,ML,MU,SM,A)
C***********************************************************************
C
C     SCALING OF A BANDED JACOBIAN
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION SM(N),A(MBH,N)
C  ---------------
C
      NADROW=MBH-MB
      M=ML+MU+1
      DO 10 JJ=1,M
      DO 10 K=JJ,N,M
      I1=MAX0(1,K-MU)
      I2=MIN0(N,K+ML)
      MH=MU+1-K
      IF(MH.LT.0)MH=0
      DO 10 I=I1,I2
      INDEX=NADROW+MH+I-I1+1
      A(INDEX,K)=SM(K)* A(INDEX,K)/SM(I)
  10  CONTINUE
      RETURN
C***********************************************************************
C     END OF BNJAC
C***********************************************************************
      END
      SUBROUTINE TJSCAL(NH,NDSB,NUMBLK,SCL,A)
C***********************************************************************
C
C     SCALING OF A BLOCK TRIDIAGONAL JACOBIAN
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION SCL(NH),A(NDSB,NDSB,NUMBLK,3)
C-----------------------------------------------------------------------
C  CALCULATE JACOBIAN
C-----------------------------------------------------------------------
      DO 10 I=1,NDSB
      DO 10 J=1,NDSB
      DO 10 L=1,NUMBLK
      ICOL=(L-1)*NDSB+J
      IROW=(L-1)*NDSB+I
      A(I,J,L,2)  =  SCL(ICOL)*A(I,J,L,2)/SCL(IROW)
   10 CONTINUE
C
      ILHE=NUMBLK-1
      DO 20 I=1,NDSB
      DO 20 J=1,NDSB
      DO 20 L=1,ILHE
      ICOL= L   *NDSB+J
      IROW=(L-1)*NDSB+I
      A(I,J,L,3)  =  SCL(ICOL)*A(I,J,L,3)/SCL(IROW)
   20 CONTINUE
C
      DO 30 I=1,NDSB
      DO 30 J=1,NDSB
      DO 30 L=2,NUMBLK
      ICOL=(L-2)*NDSB+J
      IROW=(L-1)*NDSB+I
      A(I,J,L,1)  =  SCL(ICOL)*A(I,J,L,1)/SCL(IROW)
   30 CONTINUE
C
      RETURN
C***********************************************************************
C     END OF TNJAC
C***********************************************************************
      END
      SUBROUTINE PJSCAL(N,N0,N1,N2,N2M,SCL,A,B,C)
C***********************************************************************
C                                                                      *
C     ************************************************************     *
C     *      SCALE BLOCK PENTA DIAGONAL JACOBIAN                 *     *
C     ************************************************************     *
C                                                                      *
C***********************************************************************
C***********************************************************************
C                                                                      *
C   B B     C                                                          *
C   B B B     C                                                        *
C     B B B     C                                                      *
C       B B       C                                                    *
C   A       B B     C                                                  *
C     A     B B B     C                                                *
C       A     B B B     C                                              *
C         A     B B       C                                            *
C           A       B B                                                *
C             A     B B B                                              *
C               A     B B B                                            *
C                 A     B B                                            *
C                                                                      *
C***********************************************************************
C                                                                      *
C  INPUT PARAMETERS:                                                   *
C                                                                      *
C    N         = DIMENSION OF THE WHOLE SYSTEM N=N0*N1*N2              *
C    N0        = DIMENSION OF QUADRATIC SUB MATRICES                   *
C    N1        = DIMENSION OF QUADRATIC SUB BLOCKS                     *
C    N2        = DIMENSION OF QUADRATIC BLOCKS                         *
C    N2M       = NUMBER OF SIDE BLOCKS                                 *
C    SCL       = SCALING FACTORS                                       *
C                                                                      *
C                                                                      *
C    A(N0,N0,N1,N2M) ENTRIES IN JACOBIAN (LOWER SIDE ROW)              *
C    B(N0,N0,N1,3,N2) ENTRIES IN JACOBIAN (INNER BLOCK TRIDIAGONAL S.) *
C    C(N0,N0,N1,N2M) ENTRIES IN JACOBIAN (UPPER SIDE ROW)              *
C                                                                      *
C                                                                      *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION SCL(N),B(N0,N0,N1,3,N2),A(N0,N0,N1,N2M),C(N0,N0,N1,N2M)
C***********************************************************************
C     SCALE DIAGONAL NO 1
C***********************************************************************
      DO 20 K=1,N2M
      DO 20 L=1,N1
      DO 20 I=1,N0
      DO 20 J=1,N0
      ICOL= (K-1)*N0*N1 + (L-1) * N0 + J
      IROW=  K   *N0*N1 + (L-1) * N0 + I
      A(I,J,L,K)  =  SCL(ICOL)*A(I,J,L,K)/SCL(IROW)
   20 CONTINUE
C***********************************************************************
C     SCALE DIAGONAL NO 4
C***********************************************************************
      DO 40 K=1,N2
      DO 40 L=2,N1
      DO 40 I=1,N0
      DO 40 J=1,N0
      ICOL= (K-1)*N0*N1 + (L-2) * N0 + J
      IROW= (K-1)*N0*N1 + (L-1) * N0 + I
      B(I,J,L,1,K)  =  SCL(ICOL)*B(I,J,L,1,K)/SCL(IROW)
   40 CONTINUE
C***********************************************************************
C     SCALE DIAGONAL NO 5
C***********************************************************************
      DO 50 K=1,N2
      DO 50 L=1,N1
      DO 50 I=1,N0
      DO 50 J=1,N0
      ICOL= (K-1)*N0*N1 + (L-1) * N0 + J
      IROW= (K-1)*N0*N1 + (L-1) * N0 + I
      B(I,J,L,2,K)  =  SCL(ICOL)*B(I,J,L,2,K)/SCL(IROW)
   50 CONTINUE
C***********************************************************************
C     SCALE DIAGONAL NO 6
C***********************************************************************
      N1M1=N1-1
      DO 60 K=1,N2
      DO 60 L=1,N1M1
      DO 60 I=1,N0
      DO 60 J=1,N0
      ICOL= (K-1)*N0*N1 +  L    * N0 + J
      IROW= (K-1)*N0*N1 + (L-1) * N0 + I
      B(I,J,L,3,K)  =  SCL(ICOL)*B(I,J,L,3,K)/SCL(IROW)
   60 CONTINUE
C***********************************************************************
C     SCALE DIAGONAL NO 8
C***********************************************************************
      DO 80 K=1,N2M
      DO 80 L=1,N1
      DO 80 I=1,N0
      DO 80 J=1,N0
      ICOL=  K   *N0*N1 + (L-1) * N0 + J
      IROW= (K-1)*N0*N1 + (L-1) * N0 + I
      C(I,J,L,K)  =  SCL(ICOL)*C(I,J,L,K)/SCL(IROW)
   80 CONTINUE
      RETURN
C***********************************************************************
C     END OF PNJAC
C***********************************************************************
      END
      SUBROUTINE NJSCAL(N,N0,N1,N2,N2M,SCL,A,B,C)
C***********************************************************************
C                                                                      *
C     ************************************************************     *
C     *      NUMERICAL DIFFERENCE APPROXIMATION OF JACOBIAN      *     *
C     *      GIVEN IN NONA BLOCK DIAGONAL FORM                   *     *
C     ************************************************************     *
C                                                                      *
C***********************************************************************
C***********************************************************************
C                                                                      *
C   B B       C C                                                      *
C   B B B     C C C                                                    *
C     B B B     C C C                                                  *
C       B B B     C C C                                                *
C         B B       C C                                                *
C   A A       B B       C C                                            *
C   A A A     B B B     C C C                                          *
C     A A A     B B B     C C C                                        *
C       A A A     B B B     C C C                                      *
C         A A       B B       C C                                      *
C             A A       B B                                            *
C             A A A     B B B                                          *
C               A A A     B B B                                        *
C                 A A A     B B B                                      *
C                   A A       B B                                      *
C                                                                      *
C***********************************************************************
C***********************************************************************
C                                                                      *
C  INPUT PARAMETERS:                                                   *
C                                                                      *
C    N         = DIMENSION OF THE WHOLE SYSTEM N=N0*N1*N2              *
C    N0        = DIMENSION OF QUADRATIC SUB MATRICES                   *
C    N1        = DIMENSION OF QUADRATIC SUB BLOCKS                     *
C    N2        = DIMENSION OF QUADRATIC BLOCKS                         *
C    N2M       = NUMBER OF SIDE BLOCKS                                 *
C    T         = TIME (FOR USE IN SUBROUTINE FCN)                      *
C    Y         = CURRENT VALUE OF VARIABLE                             *
C    YP        = CURRENT VALUE OF FIRST TIME DERIVATIVE OF VARIABLES   *
C    FUN0      = VALUE OF FUNCTION (UNPERTURBED)                       *
C    SCL       = SCALING FACTORS                                       *
C    ETADIF    = VALUE OF PERTURBATION                                 *
C    FCN       = NAME OF CALLED ROUTINE                                *
C    NZV,BVK,IRV,ICV  NOT USED BUT NEEDED FOR FCN                      *
C                                                                      *
C   OUTPUT PARAMETERS:                                                 *
C                                                                      *
C    A(N0,N0,N1,3,N2M) ENTRIES IN JACOBIAN (LOWER SIDE ROW)            *
C    B(N0,N0,N1,3,N2) ENTRIES IN JACOBIAN (INNER BLOCK TRIDIAGONAL S.) *
C    C(N0,N0,N1,3,N2M) ENTRIES IN JACOBIAN (UPPER SIDE ROW)            *
C                                                                      *
C                                                                      *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION SCL(N)
      DIMENSION B(N0,N0,N1,3,N2),A(N0,N0,N1,3,N2M),C(N0,N0,N1,3,N2M)
C***********************************************************************
C     SET VALUES FOR UNPERTURBED FUNCTIONS
C***********************************************************************
      N1M=N1-1
C***********************************************************************
C     SCALE DIAGONAL NO 1
C***********************************************************************
      DO 10 K=1,N2M
      DO 10 L=2,N1
      DO 10 I=1,N0
      DO 10 J=1,N0
      ICOL= (K-1)*N0*N1 + (L-2) * N0 + J
      IROW=  K   *N0*N1 + (L-1) * N0 + I
      A(I,J,L,1,K)  =  SCL(ICOL)*A(I,J,L,1,K)/SCL(IROW)
   10 CONTINUE
C***********************************************************************
C     SCALE DIAGONAL NO 1
C***********************************************************************
      DO 20 K=1,N2M
      DO 20 L=1,N1
      DO 20 I=1,N0
      DO 20 J=1,N0
      ICOL= (K-1)*N0*N1 + (L-1) * N0 + J
      IROW=  K   *N0*N1 + (L-1) * N0 + I
      A(I,J,L,2,K)  =  SCL(ICOL)*A(I,J,L,2,K)/SCL(IROW)
   20 CONTINUE
C***********************************************************************
C     SCALE DIAGONAL NO 1
C***********************************************************************
      DO 30 K=1,N2M
      DO 30 L=1,N1M
      DO 30 I=1,N0
      DO 30 J=1,N0
      ICOL= (K-1)*N0*N1 +  L    * N0 + J
      IROW=  K   *N0*N1 + (L-1) * N0 + I
      A(I,J,L,3,K)  =  SCL(ICOL)*A(I,J,L,3,K)/SCL(IROW)
   30 CONTINUE
C***********************************************************************
C     SCALE DIAGONAL NO 4
C***********************************************************************
      DO 40 K=1,N2
      DO 40 L=2,N1
      DO 40 I=1,N0
      DO 40 J=1,N0
      ICOL= (K-1)*N0*N1 + (L-2) * N0 + J
      IROW= (K-1)*N0*N1 + (L-1) * N0 + I
      B(I,J,L,1,K)  =  SCL(ICOL)*B(I,J,L,1,K)/SCL(IROW)
   40 CONTINUE
C***********************************************************************
C     SCALE DIAGONAL NO 5
C***********************************************************************
      DO 50 K=1,N2
      DO 50 L=1,N1
      DO 50 I=1,N0
      DO 50 J=1,N0
      ICOL= (K-1)*N0*N1 + (L-1) * N0 + J
      IROW= (K-1)*N0*N1 + (L-1) * N0 + I
      B(I,J,L,2,K)  =  SCL(ICOL)*B(I,J,L,2,K)/SCL(IROW)
   50 CONTINUE
C***********************************************************************
C     SCALE DIAGONAL NO 6
C***********************************************************************
      DO 60 K=1,N2
      DO 60 L=1,N1M
      DO 60 I=1,N0
      DO 60 J=1,N0
      ICOL= (K-1)*N0*N1 +  L    * N0 + J
      IROW= (K-1)*N0*N1 + (L-1) * N0 + I
      B(I,J,L,3,K)  =  SCL(ICOL)*B(I,J,L,3,K)/SCL(IROW)
   60 CONTINUE
C***********************************************************************
C     SCALE DIAGONAL NO 7
C***********************************************************************
      DO 70 K=1,N2M
      DO 70 L=2,N1
      DO 70 I=1,N0
      DO 70 J=1,N0
      ICOL=  K   *N0*N1 + (L-2) * N0 + J
      IROW= (K-1)*N0*N1 + (L-1) * N0 + I
      C(I,J,L,1,K)  =  SCL(ICOL)*C(I,J,L,1,K)/SCL(IROW)
   70 CONTINUE
C***********************************************************************
C     SCALE DIAGONAL NO 8
C***********************************************************************
      DO 80 K=1,N2M
      DO 80 L=1,N1
      DO 80 I=1,N0
      DO 80 J=1,N0
      ICOL=  K   *N0*N1 + (L-1) * N0 + J
      IROW= (K-1)*N0*N1 + (L-1) * N0 + I
      C(I,J,L,2,K)  =  SCL(ICOL)*C(I,J,L,2,K)/SCL(IROW)
   80 CONTINUE
C***********************************************************************
C     SCALE DIAGONAL NO 9
C***********************************************************************
      DO 90 K=1,N2M
      DO 90 L=1,N1M
      DO 90 I=1,N0
      DO 90 J=1,N0
      ICOL=  K   *N0*N1 +  L    * N0 + J
      IROW= (K-1)*N0*N1 + (L-1) * N0 + I
      C(I,J,L,3,K)  =  SCL(ICOL)*C(I,J,L,3,K)/SCL(IROW)
   90 CONTINUE
      RETURN
C***********************************************************************
C     END OF PNJAC
C***********************************************************************
      END
      SUBROUTINE XITMAT(N,N0,N1,N2,N2M,G,AJ,BJ,CJ,SM,
     1      NZV,NDV,BV,IRV,ICV,NZC,NDC,BC,IRC,ICC,Q)
C***********************************************************************
C
C     COMPUTATION OF A PENTA BLOCK DIAGONAL ITERATION MATRIX
C     NOTE: THERE IS NO WARNING FLAG, IF ENTRY OF BV OR BC IS IN
C           B(X,X,1,1,X) OR B(X,X,N1,3,X) !!!!!!!!!!!!!!!
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION AJ(N0,N0,N1,3,N2M)
      DIMENSION BJ(N0,N0,N1,3,N2)
      DIMENSION CJ(N0,N0,N1,3,N2M)
      DIMENSION Q(N0,N0,*)
      DIMENSION SM(N)
      DIMENSION BV(NDV),IRV(NDV),ICV(NDV)
      DIMENSION BC(NDC),IRC(NDC),ICC(NDC)
C***********************************************************************
C
C***********************************************************************
C***********************************************************************
C     SET CORNERS TO ZERO
C***********************************************************************
      DO 41 K=1,N2
      DO 41 I=1,N0
      DO 41 J=1,N0
      BJ(I,J,1 ,1,K)= 0.D0
      BJ(I,J,N1,3,K)= 0.D0
   41 CONTINUE
      DO 42 K=1,N2M
      DO 42 I=1,N0
      DO 42 J=1,N0
      AJ(I,J,1 ,1,K)= 0.D0
      AJ(I,J,N1,3,K)= 0.D0
      CJ(I,J,1 ,1,K)= 0.D0
      CJ(I,J,N1,3,K)= 0.D0
   42 CONTINUE
   99 CONTINUE
C***********************************************************************
C
C     STORE MATRIX A,B,C IN Q
C
C***********************************************************************
      NBA = (N2-1) * N1 - 1
      NBB = (N2-1) * N1
      NBC = (N2-1) * N1 + 1
      NBO =  N2    * N1
      NBP =  N2    * N1
      NBQ =  N2    * N1
      NBU = (N2-1) * N1 + 1
      NBV = (N2-1) * N1
      NBW = (N2-1) * N1 - 1
      INA = 0
      INB = INA + NBA
      INC = INB + NBB
      INO = INC + NBC
      INP = INO + NBO
      INQ = INP + NBP
      INU = INQ + NBQ
      INV = INU + NBU
      INW = INV + NBV
C***********************************************************************
C
C***********************************************************************
C***********************************************************************
C     CALCULATE JACOBIAN PART FOR SUB AND SUPER ROWS
C***********************************************************************
C
C---- STORE ENTRIES OF A
      DO 52 K=1,N2M
      DO 52 L=1,N1
      LAUF=(K-1)*N1 + L - 1
      IF(LAUF.EQ.0) GOTO 52
      DO 51 I=1,N0
      DO 51 J=1,N0
      Q(I,J,INA+LAUF)=G*AJ(I,J,L,1,K)
   51 CONTINUE
   52 CONTINUE
C---- STORE ENTRIES OF B
      DO 54 K=1,N2M
      DO 54 L=1,N1
      LAUF=(K-1)*N1 + L
      DO 54 I=1,N0
      DO 54 J=1,N0
      Q(I,J,INB+LAUF)=G*AJ(I,J,L,2,K)
   54 CONTINUE
C---- STORE ZERO ENTRY
      DO 55 I=1,N0
      DO 55 J=1,N0
      Q(I,J,INC+1   )=G*0.D0
   55 CONTINUE
C---- STORE ENTRIES OF C
      DO 56 K=1,N2M
      DO 56 L=1,N1
      LAUF=(K-1)*N1 + L + 1
      DO 56 I=1,N0
      DO 56 J=1,N0
      Q(I,J,INC+LAUF)=G*AJ(I,J,L,3,K)
   56 CONTINUE
C---- STORE ENTRIES OF O,P,Q
      DO 58 K=1,N2
      DO 58 L=1,N1
       LAUF=(K-1)*N1 + L
      DO 58 I=1,N0
      DO 58 J=1,N0
      Q(I,J,INO+LAUF)=G*BJ(I,J,L,1,K)
      Q(I,J,INP+LAUF)=G*BJ(I,J,L,2,K)
      Q(I,J,INQ+LAUF)=G*BJ(I,J,L,3,K)
   58 CONTINUE
C---- STORE ENTRIES OF U,V
      DO 60 K=1,N2M
      DO 60 L=1,N1
      LAUF=(K-1)*N1 + L
      DO 60 I=1,N0
      DO 60 J=1,N0
      Q(I,J,INU+LAUF)=G*CJ(I,J,L,1,K)
      Q(I,J,INV+LAUF)=G*CJ(I,J,L,2,K)
   60 CONTINUE
C---- STORE ZERO ENTRY IN U
      LAUF=(N2-1)*N1 +    1
      DO 67 I=1,N0
      DO 67 J=1,N0
      Q(I,J,INU+LAUF)=G*0.D0
   67 CONTINUE
C---- STORE ENTRIES OF W
      DO 62 K=1,N2M
      DO 62 L=1,N1
      LAUF=(K-1)*N1 + L
      IF(LAUF.EQ.N2*N1-N1) GOTO 62
      DO 61 I=1,N0
      DO 61 J=1,N0
      Q(I,J,INW+LAUF)=G*CJ(I,J,L,3,K)
   61 CONTINUE
   62 CONTINUE
C
C
      N1M=N1-1
      N0N1=N0*N1
C***********************************************************************
C     ADD VARIABLE ENTRIES OF B
C***********************************************************************
  100 CONTINUE
      IF(NZV.EQ.0) GOTO 200
      DO 190 I=1,NZV
      IR=IRV(I)
      IC=ICV(I)
C---- EVALUATE ROW AND COLUMN NUMBER OF BLOVK
      IRB  =1+(IR-1)/N0
      ICB  =1+(IC-1)/N0
      IRE  = IR-(IRB -1)*N0
      ICE  = IC-(IRB -1)*N0
      IDIF = IRB-ICB
      IF(IDIF.EQ.    0) INDEX = INP + IRB
      IF(IDIF.EQ.    1) INDEX = INO + IRB
      IF(IDIF.EQ.   -1) INDEX = INQ + IRB
      IF(IDIF.EQ. N1  ) INDEX = INB + IRB - N1
      IF(IDIF.EQ. N1+1) INDEX = INA + IRB - N1 - 1
      IF(IDIF.EQ. N1-1) INDEX = INC + IRB - N1 + 1
      IF(IDIF.EQ.-N1  ) INDEX = INV + IRB
      IF(IDIF.EQ.-N1-1) INDEX = INW + IRB
      IF(IDIF.EQ.-N1+1) INDEX = INU + IRB
      Q(IRE,ICE,INDEX)=Q(IRE,ICE,INDEX) + SM(IC)*BV(I)/SM(IR)
 190  CONTINUE
C***********************************************************************
C     ADD CONSTANT ENTRIES OF B
C***********************************************************************
  200 CONTINUE
      IF(NZC.EQ.0) GOTO 300
      DO 290 I=1,NZC
      IR=IRC(I)
      IC=ICC(I)
C---- EVALUATE ROW AND COLUMN NUMBER OF BLOVK
      IRB  =1+(IR-1)/N0
      ICB  =1+(IC-1)/N0
      IRE  = IR-(IRB -1)*N0
      ICE  = IC-(IRB -1)*N0
      IDIF = IRB-ICB
      IF(IDIF.EQ.    0) INDEX = INP + IRB
      IF(IDIF.EQ.    1) INDEX = INO + IRB
      IF(IDIF.EQ.   -1) INDEX = INQ + IRB
      IF(IDIF.EQ. N1  ) INDEX = INB + IRB - N1
      IF(IDIF.EQ. N1+1) INDEX = INA + IRB - N1 - 1
      IF(IDIF.EQ. N1-1) INDEX = INC + IRB - N1 + 1
      IF(IDIF.EQ.-N1  ) INDEX = INV + IRB
      IF(IDIF.EQ.-N1-1) INDEX = INW + IRB
      IF(IDIF.EQ.-N1+1) INDEX = INU + IRB
      Q(IRE,ICE,INDEX)=Q(IRE,ICE,INDEX) + SM(IC)*BC(I)/SM(IR)
 290  CONTINUE
C***********************************************************************
C     RETURN
C***********************************************************************
 300  CONTINUE
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
C---- VARIABLE ENTRY WILL DESTROY BLOCK PENTA DIAGONAL STRUCTURE
C 980 IERR=-1
C     RETURN
C---- CONSTANT ENTRY WILL DESTROY BLOCK PENTA DIAGONAL STRUCTURE
C 990 IERR=-1
C     RETURN
C***********************************************************************
C     END OF XITMAT
C***********************************************************************
      END
      SUBROUTINE SCASET(NEQ,IJOB,ATOL,Y,WT)
C***********************************************************************
C     THIS SUBROUTINE SETS THE SCALING VECTOR
C     WT ACCORDING TO WT(I)=MAX(ABS(Y(I)),ATOL(I))
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C-DIMENSION
      DIMENSION Y(NEQ),WT(NEQ),ATOL(*)
C---- BLOCK FOR ATOL BEING A SCALAR
      IF(IJOB.EQ.0) THEN
        DO 10 I=1,NEQ
          WT(I)=DMAX1(DABS(Y(I)),ATOL(1))
  10    CONTINUE
C---- BLOCK FOR ATOL BEING AN ARRAY
      ELSE
      DO 20 I=1,NEQ
      WT(I)=DMAX1(DABS(Y(I)),ATOL(I))
  20  CONTINUE
      ENDIF
      RETURN
C***********************************************************************
C     END OF SUBROUTINE SMSET
C***********************************************************************
      END
      SUBROUTINE TNJAC(NH,ND0,NB,T,Y,YP,FUN0,
     1      SCL,ETADIF,A,
     2      NZV,NDV,BVK,IRV,ICV,
     3      NFCNJ,
     4      YH,FUN1,W1,W2,FCN,RPAR,IPAR,
     1            IFCN,IFCNP)
C***********************************************************************
C
C     NUMERICAL DIFFERENCE APPROXIMATION OF DENSE JACOBIAN
C     (FEED-BACK CONTROL OF DISCRETIZATION AND ROUNDING ERRORS)
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      EXTERNAL FCN
      DIMENSION Y(ND0,NB),YP(ND0,NB),FUN0(ND0,NB),SCL(ND0,NB),
     1          YH(ND0,NB),FUN1(ND0,NB),
     1          A(ND0,ND0,NB,3)
      DIMENSION W1(NH),W2(NH)
      DIMENSION BVK(NDV),IRV(NDV),ICV(NDV)
C-DIMENSION
      DIMENSION RPAR(*),IPAR(*),IFCNP(ND0,NB)
C-----------------------------------------------------------------------
C  CALCULATE JACOBIAN
C-----------------------------------------------------------------------
C---- STORE Y IN YH
      DO 10 I=1,ND0
      DO 10 J=1,NB
      YH(I,J)=Y(I,J)
      IFCNP(I,J)= 0
   10 CONTINUE
C
      DO 190 IFPB=1,3
      DO 180 IC0=1,ND0
      IFIP=(IFPB-1)*ND0+IC0
C  VALUE FOR PERTUBATIONS
      WY=ETADIF
C***********************************************************************
C     PERTURB Y, COMPUTE FUNCTION AND RESET Y                          *
C***********************************************************************
C---  PERTURB
      DO 110 KK=IFPB,NB,3
      YH(IC0,KK)=Y(IC0,KK)+WY*SCL(IC0,KK)
      IFCNP(IC0,KK) = 1
c     if(kk.eq.nb) write(*,*) 'pert',Y(IC0,KK),YH(IC0,KK),SCL(IC0,KK)
  110 CONTINUE
C---- CALCULATE PERTURBED FUNCTION
      CALL RESLIM (NH,NZV,NDV,T,YH,W2,FUN1,BVK,IRV,ICV,YP,FCN,RPAR,IPAR,
     1            IFCN,IFCNP)
      NFCNJ=NFCNJ+1
C---- RESET
      DO 120 KK=IFPB,NB,3
      YH(IC0,KK)=Y(IC0,KK)
      IFCNP(IC0,KK) = 0
  120 CONTINUE
C***********************************************************************
C
C***********************************************************************
      DO 170 IBLK=IFPB,NB,3
      IBC = IBLK-1
      IBB = IBLK
      IBA = IBLK+1
      ICOL=(IBLK-1)*ND0+IC0
C---- LOWER DIAGONAL
      IF(IBA.LE.NB) THEN
      DO 130 IR0=1,ND0
      A(IR0,IC0,IBA,1) =
     1     (FUN0(IR0,IBA)-FUN1(IR0,IBA))/(SCL(IR0,IBA)*WY)
  130 CONTINUE
      ENDIF
C---- DIAGONAL
      DO 140 IR0=1,ND0
      A(IR0,IC0,IBB,2) =
     1     (FUN0(IR0,IBB)-FUN1(IR0,IBB))/(SCL(IR0,IBB)*WY)
c     if(IBB.eq.nb) write(*,*) 'fpert',FUN0(IC0,IBB),FUN1(IC0,IBB)
  140 CONTINUE
C---- UPPER DIAGONAL
      IF(IBC.GE.1) THEN
      DO 150 IR0=1,ND0
      A(IR0,IC0,IBC,3)=
     1     (FUN0(IR0,IBC)-FUN1(IR0,IBC))/(SCL(IR0,IBC)*WY)
  150 CONTINUE
      ENDIF
 170  CONTINUE
 180  CONTINUE
 190  CONTINUE
c     write(*,'(15(1PE8.1,1X))') A(:,:,1,2)
c     write(*,*) NB,'LLLLLL'
c      stop
      RETURN
C***********************************************************************
C     END OF TNJAC
C***********************************************************************
      END
      SUBROUTINE  CTNJAC(NH,ND0,NB,T,Y,YP,FUN0,
     1      SCL,ETADIF,A,AC,
     2      NZV,NDV,BVK,IRV,ICV,
     3      NFCNJ,
     4      YH,FUN1,W1,W2,FCN,RPAR,IPAR,
     1            IFCN,IFCNP)
C***********************************************************************
C
C     NUMERICAL DIFFERENCE APPROXIMATION OF DENSE JACOBIAN
C     (FEED-BACK CONTROL OF DISCRETIZATION AND ROUNDING ERRORS)
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      EXTERNAL FCN
      DIMENSION Y(ND0,NB),YP(ND0,NB),FUN0(ND0,NB),SCL(ND0,NB),
     1          YH(ND0,NB),FUN1(ND0,NB),
     1          A(ND0,ND0,NB,3),
     1          AC(ND0,ND0,6)
      DIMENSION W1(NH),W2(NH)
      DIMENSION BVK(NDV),IRV(NDV),ICV(NDV)
      DIMENSION RPAR(*),IPAR(*),IFCNP(*)
C-----------------------------------------------------------------------
C  CALCULATE JACOBIAN
C-----------------------------------------------------------------------
C---- STORE Y IN YH
      DO 10 I=1,ND0
      DO 10 J=1,NB
      YH(I,J)=Y(I,J)
   10 CONTINUE
C***********************************************************************
C
C     PERTURB BLOCKS 3 THROUGH NB-2
C
C***********************************************************************
      ILPB = NB-2
      DO 190 IFPB=3,5
      DO 180 IC0=1,ND0
C  VALUE FOR PERTUBATIONS
      WY=ETADIF
C***********************************************************************
C     PERTURB Y, COMPUTE FUNCTION AND RESET Y                          *
C***********************************************************************
C---  PERTURB
      DO 110 KK=IFPB,ILPB,3
      YH(IC0,KK)=Y(IC0,KK)+WY*SCL(IC0,KK)
  110 CONTINUE
C---- CALCULATE PERTURBED FUNCTION
      CALL RESLIM (NH,NZV,NDV,T,YH,W2,FUN1,BVK,IRV,ICV,YP,FCN,RPAR,IPAR,
     1            IFCN,IFCNP)
      NFCNJ=NFCNJ+1
C---- RESET
      DO 120 KK=IFPB,ILPB,3
  120 YH(IC0,KK)=Y(IC0,KK)
C***********************************************************************
C
C***********************************************************************
      DO 170 IBLK=IFPB,ILPB,3
      IBC = IBLK-1
      IBB = IBLK
      IBA = IBLK+1
C---- LOWER DIAGONAL
      IF(IBA.LE.NB) THEN
      DO 130 IR0=1,ND0
      A(IR0,IC0,IBA,1) =
     1     (FUN0(IR0,IBA)-FUN1(IR0,IBA))/(SCL(IR0,IBA)*WY)
  130 CONTINUE
      ENDIF
C---- DIAGONAL
      DO 140 IR0=1,ND0
      A(IR0,IC0,IBB,2) =
     1     (FUN0(IR0,IBB)-FUN1(IR0,IBB))/(SCL(IR0,IBB)*WY)
  140 CONTINUE
C---- UPPER DIAGONAL
      IF(IBC.GE.1) THEN
      DO 150 IR0=1,ND0
      A(IR0,IC0,IBC,3)=
     1     (FUN0(IR0,IBC)-FUN1(IR0,IBC))/(SCL(IR0,IBC)*WY)
  150 CONTINUE
      ENDIF
 170  CONTINUE
 180  CONTINUE
 190  CONTINUE
C
      WY = ETADIF
C***********************************************************************
C
C     PERTURB BLOCK 1
C
C***********************************************************************
      DO 290 IC0=1,ND0
C***********************************************************************
C     PERTURB Y, COMPUTE FUNCTION AND RESET Y                          *
C***********************************************************************
      YH(IC0,1)=Y(IC0,1)+WY*SCL(IC0,1)
      CALL RESLIM (NH,NZV,NDV,T,YH,W2,FUN1,BVK,IRV,ICV,YP,FCN,RPAR,IPAR,
     1            IFCN,IFCNP)
      YH(IC0,1)=Y(IC0,1)
      NFCNJ=NFCNJ+1
C***********************************************************************
C     STORE ENTRIES
C***********************************************************************
C---- A(2)
      DO 210 IR0=1,ND0
      A(IR0,IC0,2,1) = (FUN0(IR0,2)-FUN1(IR0,2))/(SCL(IR0,2)*WY)
  210 CONTINUE
C---- B(1)
      DO 220 IR0=1,ND0
      A(IR0,IC0,1,2) = (FUN0(IR0,1)-FUN1(IR0,1))/(SCL(IR0,1)*WY)
  220 CONTINUE
C---- AC(1)
      DO 230 IR0=1,ND0
      AC(IR0,IC0,1) = (FUN0(IR0,NB)-FUN1(IR0,NB))/(SCL(IR0,NB)*WY)
  230 CONTINUE
C---- AC(2)
      DO 240 IR0=1,ND0
      AC(IR0,IC0,2) = (FUN0(IR0,NB-1)-FUN1(IR0,NB-1))/(SCL(IR0,NB-1)*WY)
  240 CONTINUE
  290 CONTINUE
C***********************************************************************
C
C     PERTURB BLOCK 2
C
C***********************************************************************
      DO 390 IC0=1,ND0
C***********************************************************************
C     PERTURB Y, COMPUTE FUNCTION AND RESET Y                          *
C***********************************************************************
      YH(IC0,2)=Y(IC0,2)+WY*SCL(IC0,2)
      CALL RESLIM (NH,NZV,NDV,T,YH,W2,FUN1,BVK,IRV,ICV,YP,FCN,RPAR,IPAR,
     1            IFCN,IFCNP)
      YH(IC0,2)=Y(IC0,2)
      NFCNJ=NFCNJ+1
C***********************************************************************
C     STORE ENTRIES
C***********************************************************************
C---- A(3)
      DO 310 IR0=1,ND0
      A(IR0,IC0,3,1) = (FUN0(IR0,3)-FUN1(IR0,3))/(SCL(IR0,3)*WY)
  310 CONTINUE
C---- B(2)
      DO 320 IR0=1,ND0
      A(IR0,IC0,2,2) = (FUN0(IR0,2)-FUN1(IR0,2))/(SCL(IR0,2)*WY)
  320 CONTINUE
C---- C(1)
      DO 330 IR0=1,ND0
      A(IR0,IC0,1,3) = (FUN0(IR0,1)-FUN1(IR0,1))/(SCL(IR0,1)*WY)
  330 CONTINUE
C---- AC(3)
      DO 340 IR0=1,ND0
      AC(IR0,IC0,3) = (FUN0(IR0,NB)-FUN1(IR0,NB))/(SCL(IR0,NB)*WY)
  340 CONTINUE
  390 CONTINUE
C***********************************************************************
C
C     PERTURB BLOCK NB-1
C
C***********************************************************************
      DO 490 IC0=1,ND0
C***********************************************************************
C     PERTURB Y, COMPUTE FUNCTION AND RESET Y                          *
C***********************************************************************
      YH(IC0,NB-1)=Y(IC0,NB-1)+WY*SCL(IC0,NB-1)
      CALL RESLIM (NH,NZV,NDV,T,YH,W2,FUN1,BVK,IRV,ICV,YP,FCN,RPAR,IPAR,
     1            IFCN,IFCNP)
      YH(IC0,NB-1)=Y(IC0,NB-1)
      NFCNJ=NFCNJ+1
C***********************************************************************
C     STORE ENTRIES
C***********************************************************************
C---- A(NB)
      DO 410 IR0=1,ND0
      A(IR0,IC0,NB,1) = (FUN0(IR0,NB)-FUN1(IR0,NB))/(SCL(IR0,NB)*WY)
  410 CONTINUE
C---- B(NB-1)
      M1 = NB - 1
      DO 420 IR0=1,ND0
      A(IR0,IC0,M1,2) =(FUN0(IR0,M1)-FUN1(IR0,M1))/(SCL(IR0,M1)*WY)
  420 CONTINUE
C---- C(NB-2)
      M2 = NB - 2
      DO 430 IR0=1,ND0
      A(IR0,IC0,M2,3) =(FUN0(IR0,M2)-FUN1(IR0,M2))/(SCL(IR0,M2)*WY)
  430 CONTINUE
C---- AC(4)
      DO 440 IR0=1,ND0
      AC(IR0,IC0,4) = (FUN0(IR0,1)-FUN1(IR0,1))/(SCL(IR0,1)*WY)
  440 CONTINUE
  490 CONTINUE
C***********************************************************************
C
C     PERTURB BLOCK NB
C
C***********************************************************************
      DO 590 IC0=1,ND0
C***********************************************************************
C     PERTURB Y, COMPUTE FUNCTION AND RESET Y                          *
C***********************************************************************
      YH(IC0,NB)=Y(IC0,NB)+WY*SCL(IC0,NB)
      CALL RESLIM (NH,NZV,NDV,T,YH,W2,FUN1,BVK,IRV,ICV,YP,FCN,RPAR,IPAR,
     1            IFCN,IFCNP)
      YH(IC0,NB)=Y(IC0,NB)
      NFCNJ=NFCNJ+1
C***********************************************************************
C     STORE ENTRIES
C***********************************************************************
C---- B(NB-1)
      M1 = NB
      DO 520 IR0=1,ND0
      A(IR0,IC0,M1,2) =(FUN0(IR0,M1)-FUN1(IR0,M1))/(SCL(IR0,M1)*WY)
  520 CONTINUE
C---- C(NB-2)
      M2 = NB - 1
      DO 530 IR0=1,ND0
      A(IR0,IC0,M2,3) =(FUN0(IR0,M2)-FUN1(IR0,M2))/(SCL(IR0,M2)*WY)
  530 CONTINUE
C---- AC(5)
      DO 540 IR0=1,ND0
      AC(IR0,IC0,5) = (FUN0(IR0,2)-FUN1(IR0,2))/(SCL(IR0,2)*WY)
  540 CONTINUE
C---- AC(6)
      DO 550 IR0=1,ND0
      AC(IR0,IC0,6) = (FUN0(IR0,1)-FUN1(IR0,1))/(SCL(IR0,1)*WY)
  550 CONTINUE
  590 CONTINUE
      RETURN
C***********************************************************************
C     END OF CTNJAC
C***********************************************************************
      END
      SUBROUTINE CITMAT(N,NDSUB,NBLK,G,A,AE,SM,
     1      NZV,NDV,BV,IRV,ICV,NZC,NDC,BC,IRC,ICC,B,BE)
C***********************************************************************
C
C     COMPUTATION OF A TRIDIAGONAL ITERATION MATRIX
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION A(NDSUB,NDSUB,NBLK,3),AE(NDSUB,NDSUB,6)
      DIMENSION B(NDSUB,NDSUB,NBLK,3),BE(NDSUB,NDSUB,6)
      DIMENSION SM(N)
      DIMENSION BV(NDV),IRV(NDV),ICV(NDV)
      DIMENSION BC(NDC),IRC(NDC),ICC(NDC)
C***********************************************************************
C
C***********************************************************************
      DO 1 J=2,3
      DO 1 I=1,NDSUB
      DO 1 K=1,NDSUB
      B(I,K,1,J)=G*A(I,K,1,J)
   1  CONTINUE
      NBLKM=NBLK-1
      DO 2 J=1,3
      DO 2 L=2,NBLKM
      DO 2 I=1,NDSUB
      DO 2 K=1,NDSUB
      B(I,K,L,J)=G*A(I,K,L,J)
   2  CONTINUE
      DO 3 J=1,2
      DO 3 I=1,NDSUB
      DO 3 K=1,NDSUB
      B(I,K,NBLK,J)=G*A(I,K,NBLK,J)
   3  CONTINUE
      DO 4 J=1,6
      DO 4 I=1,NDSUB
      DO 4 K=1,NDSUB
      BE(I,K,J)=G*AE(I,K,J)
   4  CONTINUE
C***********************************************************************
C     ADD VARIABLE ENTRIES OF B
C***********************************************************************
      IF(NZV.EQ.0) GOTO 30
      DO 20 I=1,NZV
      IR=IRV(I)
      IC=ICV(I)
      IRBL=1+(IR-1)/NDSUB
      ICBL=1+(IC-1)/NDSUB
      IRS =IR-(IRBL-1)*NDSUB
      ICS =IC-(ICBL-1)*NDSUB
      INDB=2+ICBL-IRBL
      B(IRS,ICS,IRBL,INDB)=B(IRS,ICS,IRBL,INDB)+SM(IC)*BV(I)/SM(IR)
  20  CONTINUE
C***********************************************************************
C     ADD VARIABLE ENTRIES OF B
C***********************************************************************
  30  IF(NZC.EQ.0) GOTO 50
      DO 40 I=1,NZC
      IR=IRC(I)
      IC=ICC(I)
      IRBL=1+(IR-1)/NDSUB
      ICBL=1+(IC-1)/NDSUB
      IRS =IR-(IRBL-1)*NDSUB
      ICS =IC-(ICBL-1)*NDSUB
      INDB=2+ICBL-IRBL
      B(IRS,ICS,IRBL,INDB)=B(IRS,ICS,IRBL,INDB)+SM(IC)*BC(I)/SM(IR)
  40  CONTINUE
C***********************************************************************
C     RETURN
C***********************************************************************
  50  RETURN
C***********************************************************************
C     END OF TITMAT
C***********************************************************************
      END
      SUBROUTINE CJSCAL(NH,NDSB,NUMBLK,SCL,A,AC)
C***********************************************************************
C
C     SCALING OF A BLOCK TRIDIAGONAL JACOBIAN
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION SCL(NH),A(NDSB,NDSB,NUMBLK,3),AC(NDSB,NDSB,6)
C-----------------------------------------------------------------------
C  CALCULATE JACOBIAN
C-----------------------------------------------------------------------
      DO 10 I=1,NDSB
      DO 10 J=1,NDSB
      DO 10 L=1,NUMBLK
      ICOL=(L-1)*NDSB+J
      IROW=(L-1)*NDSB+I
      A(I,J,L,2)  =  SCL(ICOL)*A(I,J,L,2)/SCL(IROW)
   10 CONTINUE
C
      ILHE=NUMBLK-1
      DO 20 I=1,NDSB
      DO 20 J=1,NDSB
      DO 20 L=1,ILHE
      ICOL= L   *NDSB+J
      IROW=(L-1)*NDSB+I
      A(I,J,L,3)  =  SCL(ICOL)*A(I,J,L,3)/SCL(IROW)
   20 CONTINUE
C
      DO 30 I=1,NDSB
      DO 30 J=1,NDSB
      DO 30 L=2,NUMBLK
      ICOL=(L-2)*NDSB+J
      IROW=(L-1)*NDSB+I
      A(I,J,L,1)  =  SCL(ICOL)*A(I,J,L,1)/SCL(IROW)
   30 CONTINUE
C
C
      DO 40 I=1,NDSB
      DO 40 J=1,NDSB
      ICOL=  J
      IROW = I + (NUMBLK-1) * NDSB
      AC(I,J,1)  =  SCL(ICOL)*AC(I,J,1)/SCL(IROW)
      ICOL=  J
      IROW = I + (NUMBLK-2) * NDSB
      AC(I,J,2)  =  SCL(ICOL)*AC(I,J,2)/SCL(IROW)
      ICOL=  J +              NDSB
      IROW = I + (NUMBLK-1) * NDSB
      AC(I,J,3)  =  SCL(ICOL)*AC(I,J,3)/SCL(IROW)
      ICOL=  J + (NUMBLK-2) * NDSB
      IROW = I
      AC(I,J,4)  =  SCL(ICOL)*AC(I,J,4)/SCL(IROW)
      ICOL=  J + (NUMBLK-1) * NDSB
      IROW = I +              NDSB
      AC(I,J,5)  =  SCL(ICOL)*AC(I,J,5)/SCL(IROW)
      ICOL=  J + (NUMBLK-1) * NDSB
      IROW = I
      AC(I,J,6)  =  SCL(ICOL)*AC(I,J,6)/SCL(IROW)
   40 CONTINUE
C
      RETURN
C***********************************************************************
C     END OF CJSCAL
C***********************************************************************
      END
      SUBROUTINE RHSVG (N,NZV,NDV,NGES,NUMP,NDP,
     1          TH,Y,DEL,W1,W2,BVK,IRV,ICV,
     1                  SM,ETA,KONVQ,FCN,IMAT,RPAR,IPAR,
     1            IFCN,IFCNP)
C***********************************************************************
C
C     EVALUATION OF RIGHT HAND SIDE FOR SENSITIVITY EQUATION
C
C***********************************************************************
C***********************************************************************
C     STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      EXTERNAL FCN
      DIMENSION Y(NGES),DEL(NGES),W1(N),W2(N),SM(N),ETA(N)
      DIMENSION KONVQ(NDP)
      DIMENSION BVK(NDV),IRV(NDV),ICV(NDV)
      DIMENSION RPAR(*),IPAR(*),IFCNP(*)
      DIMENSION IMAT(*)
C***********************************************************************
C
C     EVALUATION OF RIGHT HAND SIDE FOR SENSITIVITY EQUATION
C
C***********************************************************************
C***********************************************************************
C     Set right hand side to zero
C***********************************************************************
      NH=NUMP*N
      DO 10 I=1,NH
10    DEL(N+I)=0.D0
C***********************************************************************
C
C     Evaluate Jacobian times sensitivity coefficients
C
C***********************************************************************
C***********************************************************************
C
C     Dense matrix
C
C***********************************************************************
      IF(IMAT(1).EQ.0) THEN
C***********************************************************************
C     Calculate columns of Jacobian
C***********************************************************************
      DO 190 K=1,N
      W=Y(K)
      U=SM(K)*ETA(K)
      Y(K)=Y(K)+U
      IFCNT = 0
      CALL FCN (N,NZV,TH,Y,W1,BVK,IRV,ICV,RPAR,IPAR,IFCNT,IFCNP)
      Y(K)=W
      DO 110 I=1,N
110   W2(I)=(W1(I)-DEL(I))/U
C***********************************************************************
C     Multiply with vector
C***********************************************************************
      DO 130 KK=1,NUMP
      IF(KONVQ(KK).LT.1) CALL DAXPY(N,Y(KK*N+K),W2,1,DEL(KK*N+1),1)
130   CONTINUE
190   CONTINUE
      ELSEIF(IMAT(1).EQ.1) THEN
C***********************************************************************
C
C     Banded
C
C***********************************************************************
      ML = IMAT(3)
      MU = IMAT(4)
      MBA = ML+MU+1
C***********************************************************************
C     Calculate columns of Jacobian
C***********************************************************************
C---- set YH and IFCNP
      DO 290 KK=1,MBA
      DO 210 I = 1,N
      W1(I)    = Y(I)
      IFCNP(I) = 0
  210 CONTINUE
C---- perturb Y
      DO 220 K = KK,N,MBA
      UU=SM(K)*ETA(K)
      W1(K) = Y(K) + UU
      IFCNP(I) = 1
  220 CONTINUE
      IFCNT = 1
      CALL FCN (N,NZV,TH,W1,W2,BVK,IRV,ICV,RPAR,IPAR,IFCNT,IFCNP)
      DO K = KK,N,MBA
      IFCNP(I) = 0
      ENDDO
C***********************************************************************
C     Multiply with vector
C***********************************************************************
      DO 250 LL=1,NUMP
      IF(KONVQ(LL).LT.1) THEN
      DO 240 K = KK,N,MBA
      IFIRST = MAX(K-MU,1)
      ILAST  = MIN(K+ML,N)
      DO 235 I=IFIRST,ILAST
      DEL(LL*N+I) = DEL(LL*N+I) + Y(LL*N+K) *
     1     (W2(I)-DEL(I))/(W1(K)-Y(K))
  235 CONTINUE
  240 CONTINUE
      ENDIF
250   CONTINUE
290   CONTINUE
      ELSE
        STOP
      ENDIF
C
C
C  EVALUATE AND ADD DF/DP
C
      DO 900 KK=1,NUMP
      IF(KONVQ(KK).GE.1) GOTO 900
      L=KK*N
      W=RPAR(KK)
      U=RPAR(KK)*1.D-7
      RPAR(KK)=RPAR(KK)+U
      IFCNT = 0
      CALL FCN (N,NZV,TH,Y,W1,BVK,IRV,ICV,RPAR,IPAR,IFCNT,IFCNP)
      RPAR(KK)=W
      DO 800 I=1,N
800   DEL(L+I)=DEL(L+I)+(W1(I)-DEL(I))/U
900   CONTINUE
C
      RETURN
      END
