      SUBROUTINE CLSFIL(UNIT)
C***********************************************************************
C     Close file with logical unit number UNIT
C***********************************************************************
      INTEGER UNIT
      CLOSE(UNIT)
      RETURN
      END
      SUBROUTINE OPNFIL(INFO,UNIT,FILE,IERR)
      INTEGER INFO,UNIT,IERR
      CHARACTER*(*) FILE
C***********************************************************************
C
C     Program to Open Files
C
C  Input:
C
C     INFO:   task
C
C               INFO =  0: open a file no matter wheter it is new or old
C               INFO =  1: open an old file, position to the beginning
C               INFO =  2: open an old file, position to the end
C               INFO = -1: open a scratch file (FILE has no meaning)
C
C     UNIT:        logical file number
C     FILE:        file name
C
C  Output:
C
C     IERR:   error flag
C
C               IERR = 1: value of INFO not yet valid
C               IERR = 2: error during open
C
C***********************************************************************
      IERR = 0
      IF(INFO.EQ.0) THEN
      OPEN(UNIT=UNIT,FILE=FILE,ERR=90)
      ELSE IF(INFO.EQ.1) THEN
      OPEN(UNIT=UNIT,FILE=FILE,ERR=90,STATUS='OLD')
      ELSE IF(INFO.EQ.2) THEN
C     OPEN(UNIT=UNIT,FILE=FILE,ERR=90,STATUS='OLD',ACCESS='APPEND')
      GOTO 90
      STOP
      ELSE IF(INFO.EQ.-1) THEN
      OPEN(UNIT=UNIT,ERR=90,STATUS='SCRATCH')
      ELSE
      GOTO 90
      ENDIF
      RETURN
  90  IERR = 1
      RETURN
  92  IERR = 2
      RETURN
      END
