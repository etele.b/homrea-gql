      SUBROUTINE LINSOL(JOB,IMAT,N,ATOL,RTOL,MAXIT,
     1         W1,W2,LWMAX,RWORK,LIW,IWORK,IPIVOT,NDMAB,B,DEL,IFAIL)
C***********************************************************************
C                                                                      *
C     ************************************************************     *
C     *                                                          *     *
C     *  MANAGEMENT OF THE SOLUTION OF LINEAR EQAUATION SYSTEMS  *     *
C     *                                                          *     *
C     *                AUTHOR: ULRICH MAAS  2.10.1987            *     *
C     *                                                          *     *
C     *                LAST CHANGE:  2.10.1987/MAAS              *     *
C     *                                                          *     *
C     ************************************************************     *
C                                                                      *
C***********************************************************************
C                                                                      *
C     NOTE: NO CHECK FOR SUFFICIENT ARRAY STORAGE PERFORMED!!!         *
C                                                                      *
C***********************************************************************
C                                                                      *
C     INPUT DATA:                                                      *
C                                                                      *
C      JOB          INFORMATION ABOUT TASK                             *
C                     0 FOR DECOMPOSITION AND SOLUTION                 *
C                     1 ONLY SOLUTION (FOR SUCCESSIVE CALLS)           *
C                     2 ONLY SOLUTION (WITH INITIAL GUESS GIVEN)       *
C                                                                      *
C      IMAT(10)     INFORMATION ABOUT MATRIX (SEE BELOW)               *
C                                                                      *
C      N            DIMENSION OF THE SYSTEM                            *
C                                                                      *
C      ATOL         ABSOLUTE ERROR TOLERANCE FOR ITERATIVE METHODS     *
C                     (PASS A DUMMY IF SYSTEM IS NOT PENTA OR NONA     *
C                     DIAGONAL BLOCKED)                                *
C                                                                      *
C      RTOL         RELATIVE ERROR TOLERANCE FOR ITERATIVE METHODS     *
C                     (PASS A DUMMY IF SYSTEM IS NOT PENTA OR NONA     *
C                     DIAGONAL BLOCKED)                                *
C                                                                      *
C      MAXIT        MAXIMUM PERMITTED NUMBER OF ITERATIONS             *
C                     (PASS A DUMMY IF SYSTEM IS NOT PENTA OR NONA     *
C                     DIAGONAL BLOCKED)                                *
C                                                                      *
C                                                                      *
C      B,DEL(N)     MATRIX AND VEKTOR                                  *
C                                                                      *
C                                                                      *
C     OUTPUT DATA:                                                     *
C                                                                      *
C      IFAIL        RETURN CODE                                        *
C                     IFAIL < 1 : SUCESSFULL COMPLETION                *
C                     IFAIL > 0 : FAILURE                              *
C                     SEE SUBROUTINES FOR FURTHER INFORMATION          *
C                                                                      *
C      B            MATRIX IN DECOMPOSED FORM                          *
C                     (MAY BE USED FOR FURTHER CALLS WITH JOB=1)       *
C                                                                      *
C      DEL(N)       SOLUTION VECTOR                                    *
C                                                                      *
C     WORKING ARRAYS:                                                  *
C                                                                      *
C      W1(N),W2(N)  REAL WORK SPACE FOR ITERATIVE METHODS              *
C                     (PASS DUMMIES IF MATRIX IS NOT PENTA OR          *
C                     NONA DIAGONAL BLOCKED)                           *
C                                                                      *
C      IPIVOT(N)    INTEGER WORK SPACE FOR DENSE OR BANDED MATRIX      *
C                     (PASS DUMMIES IF MATRIX IS NOT BANDED OR         *
C                     DENSE)                                           *
C***********************************************************************
C                                                                      *
C     ENTRIES IN IMAT:                                                 *
C                                                                      *
C       IMAT(1): KIND OF MATRIX                                        *
C                                                                      *
C                  IMAT(1)=0  DENSE MATRIX                             *
C                  IMAT(1)=1  BANDED MATRIX                            *
C                  IMAT(1)=2  BLOCK TRIDIAGONAL MATRIX                 *
C                  IMAT(1)=3  BLOCK PENTADIAGONAL MATRIX               *
C                  IMAT(1)=4  BLOCK NONADIAGONAL MATRIX                *
C                  IMAT(1)=5  BLOCK TRIDIAGONAL MATRIX + CORNERS       *
C                                                                      *
C       IMAT(2): DIMENSION OF THE MATRIX                               *
C                                                                      *
C       IMAT(3-6) INFORMATIONS ABOUT SIZE OF MATRIX                    *
C       IF IMAT(1)=0 IMAT(3-6) NOT NEEDED                              *
C       IF IMAT(1)=1 IMAT(3) = LOWER BAND WIDTH                        *
C                    IMAT(4) = UPPER BAND WIDTH                        *
C                    IMAT(5) = LEEDING DIMENSION OF MATRIX (>=2ML+MU+1)*
C                    IMAT(6) = NOT NEEDED                              *
C       IF IMAT(1)=2 IMAT(3) = DIMENSION OF SUBBLOCKS                  *
C                    IMAT(4) = NUMBER OF SUBBLOCKS                     *
C                    IMAT(5) = NOT NEEDED                              *
C                    IMAT(6) = NOT NEEDED                              *
C       IF IMAT(1)=3 IMAT(3) = DIMENSION OF SUBSUBBLOCK                *
C                    IMAT(4) = NUMBER OF SUBSUBBLOCKS                  *
C                    IMAT(5) = NUMBER OF SUBBLOCKS                     *
C                    IMAT(6) = NOT NEEDED                              *
C       IF IMAT(1)=4 IMAT(3) = DIMENSION OF SUBSUBBLOCK                *
C                    IMAT(4) = NUMBER OF SUBSUBBLOCKS                  *
C                    IMAT(5) = NUMBER OF SUBBLOCKS                     *
C                    IMAT(6) = NOT NEEDED                              *
C***********************************************************************
C***********************************************************************
C     CHAPTER I.: STORAGE ORGANIZATION                                 *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL DUMP
      EXTERNAL NPRECO,NMULTI,TPRECO,CMULTI
      DIMENSION IMAT(10),IOPT(20)
      DIMENSION DEL(N),W1(N),W2(N),IPIVOT(N)
      DIMENSION B(NDMAB),RWORK(LWMAX),IWORK(LIW)
      DIMENSION XW(1),IWK(1)
      DATA NIW/1/,LIWK/1/
      DATA NFIERR/90/
      DATA XW/1.D0/
      DATA IOPT/  6,1,0,0,0,4,0,13*0/
C     DATA IOPT/106,1,1,0,0,4,6,13*0/
C***********************************************************************
C     CHAPTER II.: INITIALIZE AND CHECK TASK
C***********************************************************************
      DUMP=.FALSE.
      IFAIL=0
      IF(JOB.EQ.1.OR.JOB.EQ.2) GOTO 1000
C***********************************************************************
C                                                                      *
C     CHAPTER III.: DECOMPOSITION AND SOLUTION                         *
C                                                                      *
C***********************************************************************
C***********************************************************************
C     CHAPTER III.0: CHECK FOR TYPE OF MATRIX                          *
C***********************************************************************
      IF(IMAT(1).EQ.0) GOTO 100
      IF(IMAT(1).EQ.1) GOTO 200
      IF(IMAT(1).EQ.2) GOTO 300
      IF(IMAT(1).EQ.3) GOTO 400
      IF(IMAT(1).EQ.4) GOTO 500
      IF(IMAT(1).EQ.5) GOTO 600
      GOTO 910
C***********************************************************************
C     CHAPTER III.1: DENSE MATRIX                                      *
C***********************************************************************
  100 CONTINUE
      CALL LINGL(N,B,IPIVOT,DEL,IFAIL)
      IF(IFAIL.GT.0) GOTO 950
      GOTO 3000
C***********************************************************************
C     CHAPTER III.2: BANDED MATRIX                                     *
C***********************************************************************
  200 CONTINUE
      ML=IMAT(3)
      MU=IMAT(4)
      MB=ML+MU+1
      MBH=IMAT(5)
      CALL DGBFA(B,MBH,N,ML,MU,IPIVOT,IFAIL)
      IF(IFAIL.GT.0) GOTO 950
      CALL DGBSL(B,MBH,N,ML,MU,IPIVOT,DEL,0)
      GOTO 3000
C***********************************************************************
C     CHAPTER III.3: BLOCK TRIDIAGONAL MATRIX                          *
C***********************************************************************
  300 CONTINUE
      N0=IMAT(3)
      N1=IMAT(4)
      CALL DGTFA(N1,N0,B,IFAIL)
      IF(IFAIL.GT.0) GOTO 950
      CALL DGTSL(N1,N0,B,DEL)
      GOTO 3000
C***********************************************************************
C     CHAPTER III.4: BLOCK PENTADIAGONAL MATRIX                        *
C***********************************************************************
  400 CONTINUE
      N0=IMAT(3)
      N1=IMAT(4)
      N2=IMAT(5)
      NB=N1*N2
C---- FAKE ARRAY FOR BLOCK TRIDIAGONAL SYSTEM
      N2M=N2-1
      IF(N2.EQ.1) N2M=1
      NDAC= N0*N0*(NB-N1)
      NDB = N0*N0*NB*3
      NJ2 = 1+NDAC
      NJ3 = NJ2+NDB
      CALL DPITSL(N,N0,N1,N2,N2M,
     1               B(1),B(NJ2),B(NJ3),DEL,W1,W2,
     1               RTOL,ATOL,MAXIT,1,IFAIL)
      IF(IFAIL.GT.0) GOTO 950
      GOTO 3000
C***********************************************************************
C
C     CHAPTER III.5: BLOCK NONADIAGONAL MATRIX                         *
C
C***********************************************************************
  500 CONTINUE
      N0=IMAT(3)
      N1=IMAT(4)
      N2=IMAT(5)
      N2M=N2-1
      IF(N2.EQ.1) N2M=1
      NB=N1*N2
      NDAC= N0*N0*(NB-N1)*3
      NDB = N0*N0*NB*3
      NJ2 = 1+NDAC
      NJ3 = NJ2+NDB
      NDJA     = 2*NDAC+ NDB
      MAXIT = 300
C***********************************************************************
C     CHAPTER  BLOCK FOR GS                                            *
C***********************************************************************
C     IJGS= 1
C     CALL DNITSL(N,N0,N1,N2,N2M,
C    1           B(1),B(NJ2),B(NJ3),DEL,W1,W2,
C    1         RTOL,ATOL,MAXIT,IJGS,IFAIL)
C     IF(IFAIL.GT.0) GOTO 950
C***********************************************************************
C     CHAPTER  BLOCK FOR CONJUGATE GRADIENT                            *
C***********************************************************************
C     CALL PCGSOL(N,N0,N1,N2,N2M,
C    1               B(1),B(NJ2),B(NJ3),DEL,Q,W1,W2,HELP1,HELP2,HELP3,
C    1               RTOL,ATOL,MAXIT,IJGS,IFAIL)
C     IF(IFAIL.GT.0) GOTO 950
C***********************************************************************
C     CHAPTER  BLOCK FOR INCOMPLETE CROUT F. + GAUSS-SEIDEL            *
C***********************************************************************
      IF(IMAT(9).EQ.0) THEN
      CALL NLUSL(N,N0,N1,NB,B,DEL,
     1     W1,W2,RTOL,ATOL,MAXIT,1,IFAIL)
      IF(IFAIL.GT.0) GOTO 950
C***********************************************************************
C     CHAPTER  BLOCK FOR GOOD BROYDEN                                  *
C***********************************************************************
      ELSE
C     EPS   =  1.D-4
      EPS   =  RTOL
      NB = N1*N2
      IJPRE = IMAT(9)
C     IF(IJPRE.LE.0) THEN
C     IMAT(9)=IABS(IMAT(9))
C     IJPRE = IMAT(9)
      INDH = 1
      CALL DNPREI(IJPRE,N0,N1,NB,N,NDMAB,B,LWMAX,NDP,RWORK(INDH),
     1        IPIVOT,IFAIL)
      IF(IFAIL.NE.0) WRITE(6,*) ' PPP IFAIL',IFAIL
C     ENDIF
      IMAT(10)=NDP
      INDRWK = INDH + NDP
      LRWK = LWMAX - INDRWK + 1
      ITASK=0
      CALL ITZIB2(ITASK,N,MAXIT,EPS,XW,NMULTI,NPRECO,NDMAB,B,IMAT,W1,
     1               W2,DEL,IOPT,LRWK,RWORK(INDRWK),NRW,
     1               LIWK,IWK,NIW,
     1               N,IPIVOT,NDMAB,RWORK(INDH),
     1               DELNRM,SOLNRM,DIFNRM,IERR )
      IFAIL = - IERR
      IF(IFAIL.GT.0) GOTO 950
C***********************************************************************
C     COMPUTE ERROR FOR TESTING                                        *
C***********************************************************************
C     INFO= 0
C     WRITE(6,*) DELNRM,SOLNRM,DIFNRM
C     CALL NONMUL(N0,N1,NB,B,W1,W2,INFO)
C     ERR = DSQRT(DIFNO(N,DEL,1,W2,1))/FLOAT(N)
C     WRITE(6,*) ' ERROR ', ERR
C***********************************************************************
C     STORE SOLUTION BACK IN DEL                                       *
C***********************************************************************
      CALL DCOPY(N,W1,1,DEL,1)
      ENDIF
      GOTO 3000
C***********************************************************************
C
C     CHAPTER III.5: BLOCK TRIDIAGONAL MATRIX PLUS CORNERS             *
C
C***********************************************************************
  600 CONTINUE
      N0=IMAT(3)
      N1=IMAT(4)
      NB=N1
      NDT = N0*N0*NB*3
      NDC = N0*N0*6
      MAXIT = 500
C***********************************************************************
C     CHAPTER  BLOCK FOR GS                                            *
C***********************************************************************
      IF(IMAT(9).EQ.0) THEN
      IJGS= 1
      CALL DCITSL(N,N0,N1,
     1           B(1),B(NDT+1),DEL,W1,W2,
     1         RTOL,ATOL,MAXIT,IJGS,IFAIL)
      IF(IFAIL.GT.0) GOTO 950
      GOTO 3000
C***********************************************************************
C     CHAPTER  BLOCK FOR GOOD BROYDEN                                  *
C***********************************************************************
      ELSE
      EPS   =  RTOL
      IJPRE = IMAT(9)
      INDH = 1
      CALL DTPREI(IJPRE,N0,NB,N,NDT,B,LWMAX,NDP,RWORK(INDH),
     1        IPIVOT,IFAIL)
      IF(IFAIL.NE.0) WRITE(6,*) ' PPP IFAIL',IFAIL
      IMAT(10)=NDP
      INDRWK = INDH + NDP
      LRWK = LWMAX - INDRWK + 1
      ITASK=0
      NDMAB=NDT+6*N0*N0
      CALL ITZIB2(ITASK,N,MAXIT,EPS,XW,CMULTI,TPRECO,NDMAB,B,IMAT,
     1 W1,W2,DEL,IOPT,LRWK,RWORK(INDRWK),NRW,LIWK,IWK,NIW,
     1 N,IPIVOT,NDT,RWORK(INDH),DELNRM,SOLNRM,DIFNRM,IERR )
      IFAIL = - IERR
      IF(IFAIL.GT.0) GOTO 950
C***********************************************************************
C     COMPUTE ERROR FOR TESTING                                        *
C***********************************************************************
C     INFO= 0
C     WRITE(6,*) DELNRM,SOLNRM,DIFNRM
C     CALL NONMUL(N0,N1,NB,B,W1,W2,INFO)
C     ERR = DSQRT(DIFNO(N,DEL,1,W2,1))/FLOAT(N)
C     WRITE(6,*) ' ERROR ', ERR
C***********************************************************************
C     STORE SOLUTION BACK IN DEL                                       *
C***********************************************************************
      CALL DCOPY(N,W1,1,DEL,1)
      ENDIF
      GOTO 3000
C***********************************************************************
C                                                                      *
C     CHAPTER IV.: ONLY SOLUTION (MATRIX GIVEN IN DECOMPOSED FORM)     *
C                                                                      *
C***********************************************************************
 1000 CONTINUE
C***********************************************************************
C     CHAPTER IV.0: CHECK FOR TYPE OF MATRIX                           *
C***********************************************************************
      IF(IMAT(1).EQ.0) GOTO 1100
      IF(IMAT(1).EQ.1) GOTO 1200
      IF(IMAT(1).EQ.2) GOTO 1300
      IF(IMAT(1).EQ.3) GOTO 1400
      IF(IMAT(1).EQ.4) GOTO 1500
      IF(IMAT(1).EQ.5) GOTO 1600
      GOTO 910
C***********************************************************************
C     CHAPTER IV.1: DENSE MATRIX                                       *
C***********************************************************************
 1100 CONTINUE
      CALL SUBST (N,B,IPIVOT,DEL)
      GOTO 3000
C***********************************************************************
C     CHAPTER IV.1: BANDED MATRIX                                      *
C***********************************************************************
 1200 CONTINUE
      ML=IMAT(3)
      MU=IMAT(4)
      MB=ML+MU+1
      MBH=IMAT(5)
      CALL DGBSL(B,MBH,N,ML,MU,IPIVOT,DEL,0)
      GOTO 3000
C***********************************************************************
C     CHAPTER IV.2: TRIDIAGONAL MATRIX                                 *
C***********************************************************************
 1300 CONTINUE
      N0=IMAT(3)
      N1=IMAT(4)
      CALL DGTSL(N1,N0,B,DEL)
      GOTO 3000
C***********************************************************************
C     CHAPTER IV.3: PENTADIAGONAL MATRIX                               *
C***********************************************************************
 1400 CONTINUE
      N0=IMAT(3)
      N1=IMAT(4)
      N2=IMAT(5)
      NB=N1*N2
C---- FAKE ARRAY FOR BLOCK TRIDIAGONAL SYSTEM
      N2M=N2-1
      IF(N2.EQ.1) N2M=1
      NDAC= N0*N0*(NB-N1)
      NDB = N0*N0*NB*3
      NJ2 = 1+NDAC
      NJ3 = NJ2+NDB
      IJGS=3
      CALL DPITSL(N,N0,N1,N2,N2M,
     1           B(1),B(NJ2),B(NJ3),DEL,W1,W2,
     1         RTOL,ATOL,MAXIT,IJGS,IFAIL)
      IF(IFAIL.GT.0) GOTO 950
      GOTO 3000
C***********************************************************************
C     CHAPTER IV.4: NONADIAGONAL MATRIX                                *
C***********************************************************************
 1500 CONTINUE
      N0=IMAT(3)
      N1=IMAT(4)
      N2=IMAT(5)
      NB=N1*N2
      N2M=N2-1
      IF(N2.EQ.1) N2M=1
      NDAC= N0*N0*(NB-N1)*3
      NDB = N0*N0*NB*3
      NJ2 = 1+NDAC
      NJ3 = NJ2+NDB
      IJGS= 3
      IJPRE=IMAT(9)
      MAXIT = 100
C     EPS   =  1.D-4
      EPS   =  RTOL
C***********************************************************************
C     CHAPTER                                                          *
C***********************************************************************
C     CALL DNITSL(N,N0,N1,N2,N2M,
C    1           B(1),B(NJ2),B(NJ3),DEL,W1,W2,
C    1         RTOL,ATOL,MAXIT,IJGS,IFAIL)
C     IF(IFAIL.GT.0) GOTO 950
C***********************************************************************
C     CHAPTER                                                          *
C***********************************************************************
      IF(IMAT(9).EQ.0) THEN
      CALL NLUSL(N,N0,N1,NB,B,DEL,
     1     W1,W2,RTOL,ATOL,MAXIT,3,IFAIL)
      IF(IFAIL.GT.0) GOTO 950
C***********************************************************************
C     CHAPTER                                                          *
C***********************************************************************
      ELSE
      NDP = IMAT(10)
      INDH = 1
      INDRWK = INDH + NDP
      LRWK = LWMAX - INDRWK + 1
      ITASK=0
      CALL ITZIB2(ITASK,N,MAXIT,EPS,XW,NMULTI,NPRECO,NDMAB,B,IMAT,W1,
     1               W2,DEL,IOPT,LRWK,RWORK(INDRWK),NRW,
     1               LIWK,IWK,NIW,
     1               N,IPIVOT,NDMAB,RWORK(INDH),
     1               DELNRM,SOLNRM,DIFNRM,IERR )
      IFAIL = - IERR
      IF(IFAIL.GT.0) GOTO 950
C***********************************************************************
C     COMPUTE ERROR                                                    *
C***********************************************************************
C     INFO= 0
C     WRITE(6,*) DELNRM,SOLNRM,DIFNRM
C     CALL NONMUL(N0,N1,NB,B,W1,W2,INFO)
C     ERR = DSQRT(DIFNO(N,DEL,1,W2,1))/FLOAT(N)
C     WRITE(6,*) '  ERROR ' ,ERROR
C***********************************************************************
C     STORE BACK SOLUTION IN DEL                                       *
C***********************************************************************
      CALL DCOPY(N,W1,1,DEL,1)
      ENDIF
      GOTO 3000
C***********************************************************************
C
C     CHAPTER III.5: BLOCK TRIDIAGONAL MATRIX PLUS CORNERS             *
C
C***********************************************************************
 1600 CONTINUE
      N0=IMAT(3)
      N1=IMAT(4)
      NB=N1
      NDT = N0*N0*NB*3
      NDC = N0*N0*6
      MAXIT = 500
C***********************************************************************
C     CHAPTER  BLOCK FOR GS                                            *
C***********************************************************************
      IF(IMAT(9).EQ.0) THEN
      IJGS= 3
      IF(JOB.EQ.2) IJGS = 4
      CALL DCITSL(N,N0,N1,
     1           B(1),B(NDT+1),DEL,W1,W2,
     1         RTOL,ATOL,MAXIT,IJGS,IFAIL)
      IF(IFAIL.GT.0) GOTO 950
      GOTO 3000
C***********************************************************************
C     CHAPTER  BLOCK FOR GOOD BROYDEN                                  *
C***********************************************************************
      ELSE
      EPS   =  RTOL
      IJPRE = IMAT(9)
      INDH = 1
      NDP = IMAT(10)
      INDRWK = INDH + NDP
      LRWK = LWMAX - INDRWK + 1
      ITASK=0
      NDMAB=NDT+6*N0*N0
      CALL ITZIB2(ITASK,N,MAXIT,EPS,XW,CMULTI,TPRECO,NDMAB,B,IMAT,
     1 W1,W2,DEL,IOPT,LRWK,RWORK(INDRWK),NRW,LIWK,IWK,NIW,
     1 N,IPIVOT,NDT,RWORK(INDH),DELNRM,SOLNRM,DIFNRM,IERR )
      IFAIL = - IERR
      IF(IFAIL.GT.0) GOTO 950
C***********************************************************************
C     COMPUTE ERROR FOR TESTING                                        *
C***********************************************************************
C     INFO= 0
C     WRITE(6,*) DELNRM,SOLNRM,DIFNRM
C     CALL NONMUL(N0,N1,NB,B,W1,W2,INFO)
C     ERR = DSQRT(DIFNO(N,DEL,1,W2,1))/FLOAT(N)
C     WRITE(6,*) ' ERROR ', ERR
C***********************************************************************
C     STORE SOLUTION BACK IN DEL                                       *
C***********************************************************************
      CALL DCOPY(N,W1,1,DEL,1)
      GOTO 3000
      ENDIF
C***********************************************************************
C                                                                      *
C     REGULAR EXIT                                                     *
C                                                                      *
C***********************************************************************
 3000 CONTINUE
      RETURN
C***********************************************************************
C                                                                      *
C     CHAPTER VI.: ERROR EXITS                                         *
C                                                                      *
C***********************************************************************
C---- TYPE OF MATRIX NOT SUPPORTED
  910 CONTINUE
      IFAIL=IMAT(1)
      RETURN
C---- ERROR DURING DECOMPOSITION
  950 CONTINUE
      IF(DUMP) THEN
      CALL ERRLIN(1,NFIERR,IMAT,N,ATOL,RTOL,MAXIT,
     1      W1,W2,IPIVOT,NDMAB,B,DEL,IFAIL)
      STOP
      ENDIF
      RETURN
C***********************************************************************
C                                                                      *
C     END OF LINSOL                                                    *
C                                                                      *
C***********************************************************************
      END
      SUBROUTINE MATNOR(JOB,IMAT,N,NDA,A,ANORM,IFAIL)
C***********************************************************************
C                                                                      *
C     ************************************************************     *
C     *                                                          *     *
C     *       MANAGEMENT OF THE CALULATION OF MATRIX NORMS       *     *
C     *                                                          *     *
C     *                AUTHOR: ULRICH MAAS  2.10.1987            *     *
C     *                                                          *     *
C     *                LAST CHANGE:  2.10.1987/MAAS              *     *
C     *                                                          *     *
C     ************************************************************     *
C                                                                      *
C***********************************************************************
C                                                                      *
C     NOTE: NO CHECK FOR SUFFICIENT ARRAY STORAGE PERFORMED!!!         *
C                                                                      *
C***********************************************************************
C                                                                      *
C     INPUT DATA:                                                      *
C                                                                      *
C      JOB          INFORMATION ABOUT KIND OF NORM                     *
C                    JOB=1  FOR SCHUR NORM                             *
C                    JOB=2  FOR MAXIMUM NORM                           *
C                    JOB=3  FOR ROW SUM NORM                           *
C                    ALL OTHER VALUES OF JOB WILL LEAD TO SCHUR NORM   *
C                                                                      *
C      IMAT(10)     INFORMATION ABOUT MATRIX (SEE BELOW)               *
C                                                                      *
C      N            DIMENSION OF THE SYSTEM                            *
C                                                                      *
C      B            MATRIX                                             *
C                                                                      *
C                                                                      *
C     OUTPUT DATA:                                                     *
C                                                                      *
C      IFAIL        RETURN CODE                                        *
C                     IFAIL < 1 : SUCESSFULL COMPLETION                *
C                     IFAIL > 0 : FAILURE (IFAIL=IMAT(1))              *
C                     SEE SUBROUTINES FOR FURTHER INFORMATION          *
C                                                                      *
C      ANORM        NORM OF MATRIX                                     *
C                                                                      *
C***********************************************************************
C                                                                      *
C     ENTRIES IN IMAT:                                                 *
C                                                                      *
C       IMAT(1): KIND OF MATRIX                                        *
C                                                                      *
C                  IMAT(1)=0  DENSE MATRIX                             *
C                  IMAT(1)=1  BANDED MATRIX                            *
C                  IMAT(1)=2  BLOCK TRIDIAGONAL MATRIX                 *
C                  IMAT(1)=3  BLOCK PENTADIAGONAL MATRIX               *
C                  IMAT(1)=4  BLOCK NONADIAGONAL MATRIX                *
C                                                                      *
C       IMAT(2): DIMENSION OF THE MATRIX                               *
C                                                                      *
C       IMAT(3-6) INFORMATIONS ABOUT SIZE OF MATRIX                    *
C       IF IMAT(1)=0 IMAT(3-6) NOT NEEDED                              *
C       IF IMAT(1)=1 IMAT(3) = LOWER BAND WIDTH                        *
C                    IMAT(4) = UPPER BAND WIDTH                        *
C                    IMAT(5) = LEEDING DIMENSION OF MATRIX (>=2ML+MU+1)*
C       IF IMAT(1)=2 IMAT(3) = DIMENSION OF SUBBLOCKS                  *
C                    IMAT(4) = NUMBER OF SUBBLOCKS                     *
C                    IMAT(5) = NOT NEEDED                              *
C       IF IMAT(1)=3 IMAT(3) = DIMENSION OF SUBSUBBLOCK                *
C                    IMAT(4) = NUMBER OF SUBSUBBLOCKS                  *
C                    IMAT(5) = NUMBER OF SUBBLOCKS                     *
C       IF IMAT(1)=4 IMAT(3) = DIMENSION OF SUBSUBBLOCK                *
C                    IMAT(4) = NUMBER OF SUBSUBBLOCKS                  *
C                    IMAT(5) = NUMBER OF SUBBLOCKS                     *
C***********************************************************************
C***********************************************************************
C     CHAPTER I.: STORAGE ORGANIZATION                                 *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION IMAT(10),A(NDA)
C***********************************************************************
C     CHECK TYPE
C***********************************************************************
      ANORM=0.0D0
      IF(IMAT(1).EQ.0) GOTO 100
      IF(IMAT(1).EQ.1) GOTO 200
      IF(IMAT(1).EQ.2) GOTO 300
      GOTO 910
C***********************************************************************
C     DENSE MATRIX
C***********************************************************************
  100 CONTINUE
      CALL DDNORM(N,N,A,ANORM,JOB)
      GOTO 3000
C***********************************************************************
C     BANDED MATRIX
C***********************************************************************
  200 CONTINUE
      ML=IMAT(3)
      MU=IMAT(4)
      MB=ML+MU+1
      CALL DBNORM(N,MB,ML,MU,A,ANORM,JOB)
      GOTO 3000
C***********************************************************************
C     BLOCK TRIDIAGONAL MATRIX
C***********************************************************************
  300 CONTINUE
      N0=IMAT(3)
      N1=IMAT(4)
      CALL DTNORM(N1,N0,A,ANORM,JOB)
      GOTO 3000
C***********************************************************************
C     REGULAR EXIT
C***********************************************************************
 3000 CONTINUE
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
C---- TYPE OF MATRIX NOT SUPPORTED
  910 CONTINUE
      IFAIL=IMAT(1)
      RETURN
C***********************************************************************
C     END OF MATNOR
C***********************************************************************
      END
      SUBROUTINE DDNORM(NR,NC,A,ANORM,JOB)
C***********************************************************************
C
C     NORM FOR DENSE MATRIX
C
C***********************************************************************
C     INPUT:
C     NC          : NUMBER OF COLUMNS
C     NR          : NUMBER OF ROWS
C     A(NR,NC)    : MATRIX
C     JOB         :  DECISSION ABOUT TASK
C                    JOB=1  FOR SCHUR NORM
C                    JOB=2  FOR MAXIMUM NORM
C                    JOB=3  FOR ROW SUM NORM
C                    ALL OTHER VALUES OF JOB WILL LEAD TO SCHUR NORM
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION A(NR,NC)
      IF(JOB.EQ.1) GOTO 100
      IF(JOB.EQ.2) GOTO 200
      IF(JOB.EQ.3) GOTO 300
C***********************************************************************
C     MAXIMUM NORM
C***********************************************************************
  100 CONTINUE
      ANORM=0.0D0
      DO 110 IR=1,NR
      DO 110 IC=1,NC
      ANORM= ANORM + A(IR,IC)*A(IR,IC)
  110 CONTINUE
      ANORM=DSQRT(ANORM)
      GOTO 500
C***********************************************************************
C     MAXIMUM NORM
C***********************************************************************
  200 CONTINUE
      ANORM=0.0D0
      DO 210 IR=1,NR
      DO 210 IC=1,NC
      ANORM=DMAX1(DABS(A(IR,IC)),ANORM)
  210 CONTINUE
      GOTO 500
C***********************************************************************
C     MAXIMUM ROW NORM
C***********************************************************************
 300  CONTINUE
      ANORM=0.0D0
      DO 330 IR=1,NR
      ROW=0.0D0
      DO 320 IC=1,NC
      ROW=ROW+DABS(A(IR,IC))
 320  CONTINUE
      ANORM=DMAX1(ANORM,ROW)
 330  CONTINUE
      GOTO 500
C***********************************************************************
C     RETURN
C***********************************************************************
  500 CONTINUE
      RETURN
C***********************************************************************
C     END OF DDNORM
C***********************************************************************
      END
      SUBROUTINE DBNORM(N,MB,ML,MU,A,ANORM,JOB)
C***********************************************************************
C
C     NORM FOR BANDED MATRIX
C
C***********************************************************************
C     INPUT:
C     N           : NUMBER OF COLUMNS
C     MB          : NUMBER OF ROWS
C     ML          : NUMBER OF LOWER DIAGONALS
C     MU          : NUMBER OF UPPER DIAGONALS
C     A(MB,N)     : MATRIX
C     JOB         :  DECISSION ABOUT TASK
C                    JOB=1  FOR SCHUR NORM
C                    JOB=2  FOR MAXIMUM NORM
C                    JOB=3  FOR ROW SUM NORM
C                    ALL OTHER VALUES OF JOB WILL LEAD TO SCHUR NORM
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION A(MB,N)
      IF(JOB.EQ.1) GOTO 100
      IF(JOB.EQ.2) GOTO 200
      IF(JOB.EQ.3) GOTO 300
C***********************************************************************
C     SCHUR NORM
C***********************************************************************
  100 CONTINUE
      ANORM=0.0D0
      DO 120 IR=1,MB
      IC1= MAX0(1,MU+2-IR)
      ICN= MIN0(N,N-ML+MB-IR)
      DO 120 IC=IC1,ICN
      ANORM=ANORM+ A(IR,IC) * A(IR,IC)
  120 CONTINUE
      ANORM=DSQRT(ANORM)
      GOTO 500
C***********************************************************************
C     MAXIMUM NORM
C***********************************************************************
  200 CONTINUE
      ANORM=0.0D0
      DO 220 IR=1,MB
      IC1= MAX0(1,MU+2-IR)
      ICN= MIN0(N,N-ML+MB-IR)
      DO 220 IC=IC1,ICN
      ANORM=DMAX1(DABS(A(IR,IC)),ANORM)
  220 CONTINUE
      GOTO 500
C***********************************************************************
C     MAXIMUM ROW NORM
C***********************************************************************
  300 CONTINUE
      ANORM=0.0D0
      NFIR=MU+1
      NLAS=MU+N
      DO 330 NL=NFIR,NLAS
      NCF= 1+MAX0(0,NL-MB)
      NCL= MIN0(N,NL)
      ROW=0.0D0
      DO 320 IC=NCF,NCL
      IR=NL-IC+1
      ACT=A(IR,IC)
      ROW=ROW+DABS(ACT)
  320 CONTINUE
      ANORM=DMAX1(ANORM,ROW)
  330 CONTINUE
      GOTO 500
C***********************************************************************
C     RETURN
C***********************************************************************
  500 CONTINUE
      RETURN
C***********************************************************************
C     END OF DBNORM
C***********************************************************************
      END
      SUBROUTINE DTNORM(NB,N,A,ANORM,JOB)
C***********************************************************************
C
C     NORMS FOR TRIDIAGONAL MATRIX
C
C***********************************************************************
C     INPUT:
C     N           : DIMENSION OF SUB MATRIX
C     NB          : NUMBER OF BLOCKS
C     A(N,N,NB,3) : MATRIX
C     JOB         :  DECISSION ABOUT TASK
C                    JOB=1  FOR SCHUR NORM
C                    JOB=2  FOR MAXIMUM NORM
C                    JOB=3  FOR ROW SUM NORM
C                    ALL OTHER VALUES OF JOB WILL LEAD TO SCHUR NORM
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION A(N,N,NB,3)
      IF(JOB.EQ.1) GOTO 100
      IF(JOB.EQ.2) GOTO 200
      IF(JOB.EQ.3) GOTO 300
C***********************************************************************
C     SCHUR NORM
C***********************************************************************
  100 CONTINUE
      ANORM=0.0D0
      DO 110 J=2,3
      DO 110 I=1,N
      DO 110 K=1,N
      ANORM=ANORM+ A(I,K,1,J) * A(I,K,1,J)
  110 CONTINUE
      NBM=NB-1
      DO 120 J=1,3
      DO 120 L=2,NBM
      DO 120 I=1,N
      DO 120 K=1,N
      ANORM=ANORM + A(I,K,L,J) * A(I,K,L,J)
  120 CONTINUE
      DO 130 J=1,2
      DO 130 I=1,N
      DO 130 K=1,N
      ANORM=ANORM + A(I,K,NB,J) * A(I,K,NB,J)
  130 CONTINUE
      ANORM=DSQRT(ANORM)
      GOTO 500
C***********************************************************************
C     MAXIMUM NORM
C***********************************************************************
  200 CONTINUE
      ANORM=0.0D0
      DO 210 J=2,3
      DO 210 I=1,N
      DO 210 K=1,N
      ANORM=DMAX1(DABS(A(I,K,1,J)),ANORM)
  210 CONTINUE
      NBM=NB-1
      DO 220 J=1,3
      DO 220 L=2,NBM
      DO 220 I=1,N
      DO 220 K=1,N
      ANORM=DMAX1(DABS(A(I,K,L,J)),ANORM)
  220 CONTINUE
      DO 230 J=1,2
      DO 230 I=1,N
      DO 230 K=1,N
      ANORM=DMAX1(DABS(A(I,K,NB,J)),ANORM)
  230 CONTINUE
      GOTO 500
C***********************************************************************
C     ROW SUM NORM
C***********************************************************************
  300 CONTINUE
      ANORM=0.0D0
      DO 320 I=1,N
      ROW=0.0D0
      DO 310 K=1,N
      ROW=ROW+DABS(A(I,K,1,2))
      ROW=ROW+DABS(A(I,K,1,3))
  310 CONTINUE
      ANORM=DMAX1(ANORM,ROW)
  320 CONTINUE
      NBM=NB-1
      DO 340 J=2,NBM
      DO 340 I=1,N
      ROW=0.0D0
      DO 330 K=1,N
      ROW=ROW+DABS(A(I,K,J,1))
      ROW=ROW+DABS(A(I,K,J,2))
      ROW=ROW+DABS(A(I,K,J,3))
  330 CONTINUE
      ANORM=DMAX1(ANORM,ROW)
  340 CONTINUE
      DO 360 I=1,N
      ROW=0.0D0
      DO 350 K=1,N
      ROW=ROW+DABS(A(I,K,NB,1))
      ROW=ROW+DABS(A(I,K,NB,2))
  350 CONTINUE
      ANORM=DMAX1(ANORM,ROW)
  360 CONTINUE
      GOTO 500
C***********************************************************************
C     RETURN
C***********************************************************************
  500 CONTINUE
      RETURN
C***********************************************************************
C     END OF DTNORM
C***********************************************************************
      END
      SUBROUTINE DGTFA(NB,N,A,ILL)
C***********************************************************************
C                                                                      *
C     *************************************************************
C     *                                                           *
C     *  FACTORIZATION OF A BLOCK-TRIDIAGONAL MATRIX BY LU DEC.   *
C     *                      U.MAAS                               *
C     *                     6.11.1986                             *
C     *           LAST CHANGE: U.MAAS 6.1.1.1986                  *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C                                                                      C
C     INPUT:                                                           C
C                                                                      C
C     N          DIMENSION OF SUB MATRIX                               C
C     NB         NUMBER OF BLOCKS                                      C
C     A(N,N,NB)  STORED IN A(I,J,K,1)                                  C
C     B(N,N,NB)  STORED IN A(I,J,K,2)                                  C
C     C(N,N,NB)  STORED IN A(I,J,K,3)                                  C
C                                                                      C
C     OUTPUT:                                                          C
C                                                                      C
C                                                                      *
C     A (UNCHANGED)          (STORED IN A(I,J,K,1)                     *
C     INVERSE OF L           (STORED IN A(I,J,K,2)                     *
C     U                      (STORED IN A(I,J,K,2)                     *
C                                                                      *
C     ILL = 0 FOR REGULAR EXIT                                         C
C     ILL = 1 FOR SINGULARITY                                          C
C
C     NOTE: A(N,N,NB,3) USED AS WORKING AREA                           *
C                                                                      *
C***********************************************************************
C                                                                      *
C     < B(1)  C(1)                         >                           *
C     < A(2)  B(2)  C(2)                   >                           *
C     <       A(3)  B(3)  C(3)             >                           *
C     <              .     .     .         >                           *
C     <                    .     .     .   >                           *
C     <                         A(NB) B(NB)>                           *
C                                                                      *
C                                                                      *
C    <L(1)                         >   < 1    U(1)                   > *
C    <A(2)  L(2)                   >   <       1    U(2)             > *
C    <       .     .     .         >   <             .     .         > *
C    <             .     .     .   >   <                   .      .  > *
C    <                  A(NB) L(NB)>   <                          1  > *
C                                                                      *
C                                                                      *
C     A(I),B(I),C(I),I=1,...,L ARE N*N SUB-MATRICES, THE RIGHT HAND    *
C     SIDES Z(I),I=1,...,L ARE N-DIMENSIONAL SUB-VECTORS. THE SOLUTION *
C     VECTOR APPEARS IN X, THE SYSTEM MATRIX IS DESTROYED.             *
C                                                                      *
C***********************************************************************
C                                                                      C
C                                                                      C
C***********************************************************************
C                                                                      C
C***********************************************************************
C
C                                                                      C
C***********************************************************************
C     STORAGE ORGANIZATION                                             C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION A(N,N,NB,3)
      ILL=0
C***********************************************************************
C     CALCULATE FOR I=1
C***********************************************************************
      CALL DGQINV(N,A(1,1,1,2),A(1,1,NB,3),ILL)
      IF(ILL.GT.0) THEN
        ILL = 1
        GOTO 900
      ENDIF
      CALL DGQMMM(N,A(1,1,1,2),A(1,1,1,3),A(1,1,NB,3),0)
      CALL DGQMEQ(N,A(1,1,NB,3),A(1,1,1,3))
C***********************************************************************
C     CALCULATE FOR I= 2 TO (NB-1)                                     C
C***********************************************************************
C
C     CALCULATE W(I) FOR I= 2,...,NB-1
C     CALCULATE G(I) FOR I= 2,...,NB
C
C***********************************************************************
      NBM=NB-1
      DO 1000 KK=2,NBM
      KKM1=KK-1
      CALL DGQMMM(N,A(1,1,KK,1),A(1,1,KKM1,3),A(1,1,KK,2),-1)
      CALL DGQINV(N,A(1,1,KK,2),A(1,1,NB,3),ILL)
      IF(ILL.GT.0) THEN
        ILL = KK
        GOTO 900
      ENDIF
      CALL DGQMMM(N,A(1,1,KK,2),A(1,1,KK,3),A(1,1,NB,3),0)
      CALL DGQMEQ(N,A(1,1,NB,3),A(1,1,KK,3))
 1000 CONTINUE
C***********************************************************************
C     CALCULATE FOR I= NB                                              C
C***********************************************************************
      CALL DGQMMM(N,A(1,1,NB,1),A(1,1,NBM,3),A(1,1,NB,2),-1)
      CALL DGQINV(N,A(1,1,NB,2),A(1,1,NB,3),ILL)
      IF(ILL.GT.0) THEN
        ILL = NB
        GOTO 900
      ENDIF
      RETURN
  900 CONTINUE
      RETURN
C***********************************************************************
C     END OF DGTFA
C***********************************************************************
      END
      SUBROUTINE DGTSL(NB,N,A,Z)
C***********************************************************************
C                                                                      *
C     *************************************************************
C     *                                                           *
C     *   SOLUTION OF OF A BLOCK-TRIDIAGONAL EQUATION SYSTEM      *
C     *       USING THE RESULTS COMPUTED BY DGTFA                 *
C     *                      U.MAAS                               *
C     *                     6.11.1986                             *
C     *           LAST CHANGE: U.MAAS 6.1.1.1986                  *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C                                                                      *
C     INPUT:
C                                                                      *
C     N           DIMENSION OF SUB MATRIX                              *
C     NB          NUMBER OF BLOCKS                                     *
C     A(N,N,NB,3) SEE SUBROUTINE DGTFA                                 *
C     Z(N,1)                                                           *
C                                                                      *
C                                                                      C
C     OUTPUT:                                                          C
C                                                                      C
C     Z(N,1)  SOLUTION VECTOR                                          C
C                                                                      C
C***********************************************************************
C
C                                                                      C
C***********************************************************************
C     STORAGE ORGANIZATION                                             C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION A(N,N,NB,3),Z(N,NB)
C***********************************************************************
C     CALCULATE G(1) (STORED IN X(1))
C***********************************************************************
      DO 10 K=1,N
   10 A(K,1,NB,3)=Z(K,1)
      CALL DGQMMV(N,A(1,1,1,2),A(1,1,NB,3),Z(1,1),0)
C***********************************************************************
C     CALCULATE G(I) FOR I= 2 TO NB (STORED IN X(I)                    C
C***********************************************************************
      DO 30 I=2,NB
      IM1 = I-1
      DO 20 K=1,N
   20 A(K,1,NB,3)=Z(K,I)
      CALL DGQMMV(N,A(1,1,I,1),Z(1,IM1),A(1,1,NB,3),-1)
      CALL DGQMMV(N,A(1,1,I,2),A(1,1,NB,3),Z(1,I),0)
   30 CONTINUE
C***********************************************************************
C     CALCULATE X(NB)
C***********************************************************************
C---- X(NB) IS ALREADY STORED
C***********************************************************************
C     CALCULATE X(I), I=1,NB-1
C***********************************************************************
      DO 300 K=2,NB
      I=NB-K+1
      IP1=I+1
      CALL DGQMMV(N,A(1,1,I,3),Z(1,IP1),Z(1,I),-1)
  300 CONTINUE
      RETURN
C***********************************************************************
C     END OF DGTSL
C***********************************************************************
      END
      SUBROUTINE DGQMMM(N,A,B,X,INFO)
C***********************************************************************
C                                                                      *
C     *************************************************************
C     *                                                           *
C     *         MULTIPLICATION OF TWO N*N-MATRICES                *
C     *                                                           *
C     *                                                           *
C     *                                                           *
C     *************************************************************
C
C
C          INFO >  0      {X} = {X} +  {A}  * {B}
C
C          INFO =  0      {X} =        {A}  * {B}
C
C          INFO <  0      {X} = {X} -  {A}  * {B}
C
C***********************************************************************
      DOUBLE PRECISION A(N,N),B(N,N),X(N,N)
C***********************************************************************
C_rst NEW AND FASTER VERSION, FOR OLD COMMENT THIS LINE OUT
C***********************************************************************
C      GOTO 2000
C
      IF (INFO .LT. 0) THEN
         X = X - MATMUL(A,B)
         GOTO 100
      ELSEIF (INFO .EQ. 0) THEN
         X = MATMUL(A,B)
         GOTO 100
      ELSEIF (INFO .GT. 0) THEN
         X = X + MATMUL(A,B)
         GOTO 100
      ENDIF
C
C***********************************************************************
C_rst OLD VERSION
 2000 CONTINUE
      IF(INFO) 10,20,30
   10 CONTINUE
      DO 11 I=1,N
      DO 11 J=1,N
      DO 11 K=1,N
      X(I,J)=X(I,J)-A(I,K)*B(K,J)
   11 CONTINUE
      GOTO 100
   20 CONTINUE
      DO 21 I=1,N
      DO 21 J=1,N
      X(I,J)=0.0D0
      DO 21 K=1,N
      X(I,J)=X(I,J)+A(I,K)*B(K,J)
   21 CONTINUE
      GOTO 100
   30 CONTINUE
      DO 31 I=1,N
      DO 31 J=1,N
      DO 31 K=1,N
      X(I,J)=X(I,J)+A(I,K)*B(K,J)
   31 CONTINUE
      GOTO 100
  100 CONTINUE
      RETURN
C***********************************************************************
C     END OF -DGQMMM-
C***********************************************************************
      END
      SUBROUTINE DTQMMV(N,A,V,X,INFO)
C***********************************************************************
C                                                                      *
C     *************************************************************
C     *                                                           *
C     * MULTIPLICATION OF THE TRANSPOSED OF A MATRIX
C     *                        WITH A VECTOR
C     *                                                           *
C     *                                                           *
C     *                                                           *
C     *                                                           *
C     *                                                           *
C     *************************************************************
C
C          INFO >  0      X = X +  {A}T * V
C
C          INFO =  0      X =      {A}T * V
C
C          INFO <  0      X = X -  {A}T * V
C
C***********************************************************************
      DOUBLE PRECISION A(N,N),V(N),X(N)
      IF(INFO) 10,20,30
   10 CONTINUE
      DO 11 I=1,N
      DO 11 K=1,N
      X(I)=X(I)-A(K,I)*V(K)
   11 CONTINUE
      GOTO 100
   20 CONTINUE
      DO 21 I=1,N
      X(I)=0.0D0
      DO 21 K=1,N
      X(I)=X(I)+A(K,I)*V(K)
   21 CONTINUE
      GOTO 100
   30 CONTINUE
      DO 31 I=1,N
      DO 31 K=1,N
      X(I)=X(I)+A(K,I)*V(K)
   31 CONTINUE
      GOTO 100
 100  CONTINUE
      RETURN
C***********************************************************************
C     END OF -DGQMMV-
C***********************************************************************
      END
      SUBROUTINE DTTMUL(NSUB,NBLK,A,V,Z,INFO)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *  MULTIPLICATION OF THE TRANSPOSED OF A TRIDIAGONAL MATRIX *
C     *                      WITH A VECTOR                        *
C     *                      U.MAAS                               *
C     *                    17.4.1986                              *
C     *           LAST CHANGE: U.MAAS APR.20,1986                 *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C                                                                      *
C     < B(1)  C(1)                         >   < V(1) >   < Z(1) >     *
C     < A(2)  B(2)  C(2)                   >   < V(2) >   < Z(2) >     *
C     <       A(3)  B(3)  C(3)             > * < V(3) > = < Z(3) >     *
C     <              .     .     .         >   <  .   >   <  .   >     *
C     <                    .     .     .   >   <  .   >   <  .   >     *
C     <                         A(NB) B(NB)>   < V(NB)>   < Z(NB)>     *
C                                                                      *
C                                                                      *
C                                                                      *
C***********************************************************************
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION A(NSUB,NSUB,NBLK,3),V(NSUB,NBLK),Z(NSUB,NBLK)
      DATA ZERO/0.0D0/
C***********************************************************************
C     CHECK                                                            C
C***********************************************************************
      IF(NBLK.LT.3) GOTO 9100
C***********************************************************************
C     SET SOLUTION VECTOR X TO ZERO                                    C
C***********************************************************************
                    JOB=-1
      IF(INFO.GE.0) JOB=1
      IF(INFO.NE.0) GOTO 15
      DO 10 I=1,NSUB
      DO 10 J=1,NBLK
  10  Z(I,J)=ZERO
C***********************************************************************
C     CALCULATE FIRST BLOCK                                            C
C***********************************************************************
  15  CONTINUE
      CALL DTQMMV(NSUB,A(1,1,1,2),V(1,1),Z(1,1),JOB)
      CALL DTQMMV(NSUB,A(1,1,2,1),V(1,2),Z(1,1),JOB)
C***********************************************************************
C     CALCULATE SECOND TO ONE BEFORE LAST                              C
C***********************************************************************
      NBM=NBLK-1
      DO 20 K=2,NBM
      KM1=K-1
      KP1=K+1
      CALL DTQMMV(NSUB,A(1,1,KM1,3),V(1,KM1),Z(1,K),JOB)
      CALL DTQMMV(NSUB,A(1,1,K,2),V(1,K),Z(1,K),JOB)
      CALL DTQMMV(NSUB,A(1,1,KP1,1),V(1,KP1),Z(1,K),JOB)
  20  CONTINUE
C***********************************************************************
C     CALCULATE LAST BLOCK                                            C
C***********************************************************************
      CALL DTQMMV(NSUB,A(1,1,NBM, 3),V(1,NBM),Z(1,NBLK),JOB)
      CALL DTQMMV(NSUB,A(1,1,NBLK,2),V(1,NBLK),Z(1,NBLK),JOB)
      RETURN
C***********************************************************************
C     ERROR EXITS                                                     C
C***********************************************************************
C----- LESS THAN 3 SUBMATRICES
 9100 INFO=-1
      RETURN
C***********************************************************************
C     END OF TRIMUL                                                   C
C***********************************************************************
      END
      SUBROUTINE DGQMEQ(N,A,B)
C***********************************************************************
C                                                                      *
C     *************************************************************
C     *                                                           *
C     *         SET N*N MATRIX B= N*NMATRIX A                     *
C     *                                                           *
C     *                                                           *
C     *************************************************************
C
C
C***********************************************************************
      DOUBLE PRECISION A(N,N),B(N,N)
C     DO 1 J=1,N
C     DO 1 I=1,N
C     B(I,J)=A(I,J)
C  1  CONTINUE
      NQ=N*N
      CALL DCOPY(NQ,A,1,B,1)
      RETURN
C***********************************************************************
C     END OF -DGQMEQ-
C***********************************************************************
      END
      SUBROUTINE QMATPO(N,A,LDA)
C     ..
C
C  Purpose
C  =======
C
C  QMATPO transposes the quadratic matrix A
C
C  Arguments
C  =========
C
C  M       (input) INTEGER
C          The number of rows of the matrix A.  M >= 0.
C
C  N       (input) INTEGER
C          The number of columns of the matrix A.  N >= 0.
C
C  A       (input) DOUBLE PRECISION array, dimension (LDA,N)
C          The m by n matrix A.  If UPLO = 'U', only the upper triangle
C          or trapezoid is accessed; if UPLO = 'L', only the lower
C          triangle or trapezoid is accessed.
C
C  LDA     (input) INTEGER
C          The leading dimension of the array A.  LDA >= max(1,M).
C
C  =====================================================================
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION   A(LDA,*)
C     .. Executable Statements ..
C
      IF(N.GT.LDA) STOP
      DO 10 J = 1, N
      DO 10 I = 1, J
      AIJ = A(I,J)
      A(I,J) = A(J,I)
      A(J,I) = AIJ
   10 CONTINUE
      RETURN
      END
      SUBROUTINE DGQINV(N,A,H,ILL)
C***********************************************************************
C                                                                      *
C     *************************************************************
C     *                                                           *
C     *   INVERSION OF A MATRIX BY 'AUGMENTED MATRIX METHOD'      *
C     *                      J.WARNATZ                            *
C     *                      27.9.1979                            *
C     *           LAST CHANGE: U.MAAS/NOV.10,1986                 *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C     N   : LINE NUMBER OF N*N MATRIX  A                               *
C     ILL : ILL=0 FOR CORRECT, ILL=1 FOR INCORRECT RETURN              *
C***********************************************************************
      DOUBLE PRECISION A(N,N),H(N,N),AZ,AA
      IF(N.EQ.1)                      GOTO 9100
      ILL=0
C***********************************************************************
C     CALCULATION OF LOWER LIMIT FOR PIVOT ELEMENTS
C***********************************************************************
      AA=0.0D0
C     DO 50 I=1,N
C     DO 50 J=1,N
C     AA=DMAX1(AA,DABS(A(I,J)))
C  50 CONTINUE
      AA=0.0D0
C***********************************************************************
C     CONSTRUCTION OF UNITY MATRIX
C***********************************************************************
      DO 80 J=1,N
      DO 80 I=1,N
      H(I,J)=0.0D0
   80 CONTINUE
      DO 90 J=1,N
      H(J,J)=1.0D0
   90 CONTINUE
C***********************************************************************
C
C     FIRST SEMI-DIAGONALIZATION
C
C***********************************************************************
  100 CONTINUE
      L=0
  110 CONTINUE
      L=L+1
      LP1=L+1
C***********************************************************************
C     SEARCH FOR THE LINE, WHERE A IS GREATER THAN AA
C***********************************************************************
      IF(DABS(A(L,L)).GT.AA)  GO TO 150
      IF(L.EQ.N) GOTO 9200
      DO 120 I=LP1,N
      II=I
      IF(DABS(A(I,L)).GT.AA)  GO TO 130
  120 CONTINUE
      GOTO 9200
  130 CONTINUE
C***********************************************************************
C     ADD THAT LINE TO THE LINE L
C***********************************************************************
      DO 140 J=L,N
  140 A(L,J)=A(L,J)+A(II,J)
      DO 145 J=1,N
  145 H(L,J)=H(L,J)+H(II,J)
  150 CONTINUE
C***********************************************************************
C     NORM ROW L SO THAT FIRST ELEMENT IS ONE
C***********************************************************************
      AZ=A(L,L)
      DO 160 J=L,N
  160 A(L,J)=A(L,J)/AZ
      DO 165 J=1,N
  165 H(L,J)=H(L,J)/AZ
      IF(L.GE.N)  GO TO 200
C***********************************************************************
C     NORM ROWS L+1 TO N AND SUBTRACT FROM ROW L
C***********************************************************************
      DO 190 I=LP1,N
      AZ=A(I,L)
      DO 180 J=L,N
  180 A(I,J)=A(I,J)-A(L,J)*AZ
      DO 185 J=1,N
  185 H(I,J)=H(I,J)-H(L,J)*AZ
  190 CONTINUE
      GOTO 110
C***********************************************************************
C
C     SECOND SEMI-DIAGONALIZATION
C
C***********************************************************************
  200 CONTINUE
      L=N
  210 CONTINUE
      DO 230 K=2,L
      I=L-K+1
      AZ=A(I,L)
      DO 220 J=L,N
  220 A(I,J)=A(I,J)-A(L,J)*AZ
      DO 225 J=1,N
  225 H(I,J)=H(I,J)-H(L,J)*AZ
  230 CONTINUE
      L=L-1
      IF(L.EQ.1) GOTO 300
      GOTO 210
C***********************************************************************
C     BACK TRANSPORT OF THE MATRIX
C***********************************************************************
  300 CONTINUE
      DO 310 I=1,N
      DO 310 J=1,N
  310 A(I,J)=H(I,J)
      RETURN
C***********************************************************************
C     IRREGULAR EXITS
C***********************************************************************
C---- 1,1 MATRIX
 9100 CONTINUE
      A(1,1)=1.0D0/A(1,1)
      RETURN
 9200 CONTINUE
      ILL=1
      RETURN
C***********************************************************************
C     END OF -DGQINV-
C***********************************************************************
      END
      SUBROUTINE DGQMMV(N,A,V,X,INFO)
C***********************************************************************
C                                                                      *
C     *************************************************************
C     *                                                           *
C     *     MULTIPLICATION OF A MATRIX WITH A VECTOR              *
C     *                                                           *
C     *                                                           *
C     *                                                           *
C     *                                                           *
C     *                                                           *
C     *************************************************************
C
C          INFO >  0      X = X +  {A} * V
C
C          INFO =  0      X =      {A} * V
C
C          INFO <  0      X = X -  {A} * V
C
C***********************************************************************
      DOUBLE PRECISION A(N,N),V(N),X(N)
C***********************************************************************
C_rst NEW AND FASTER VERSION, FOR OLD COMMENT THIS LINE OUT
C***********************************************************************
C      GOTO 2000
C
      IF (INFO .LT. 0) THEN
         X = X - MATMUL(A,V)
         GOTO 100
      ELSEIF (INFO .EQ. 0) THEN
         X = MATMUL(A,V)
         GOTO 100
      ELSEIF (INFO .GT. 0) THEN
         X = X + MATMUL(A,V)
         GOTO 100
      ENDIF
C
C***********************************************************************
C_rst OLD VERSION
 2000 CONTINUE
      IF(INFO) 10,20,30
   10 CONTINUE
      DO 11 I=1,N
      DO 11 K=1,N
      X(I)=X(I)-A(I,K)*V(K)
   11 CONTINUE
      GOTO 100
   20 CONTINUE
      DO 21 I=1,N
      X(I)=0.0D0
      DO 21 K=1,N
      X(I)=X(I)+A(I,K)*V(K)
   21 CONTINUE
      GOTO 100
   30 CONTINUE
      DO 31 I=1,N
      DO 31 K=1,N
      X(I)=X(I)+A(I,K)*V(K)
   31 CONTINUE
      GOTO 100
 100  CONTINUE
      RETURN
C***********************************************************************
C     END OF -DGQMMV-
C***********************************************************************
      END
      SUBROUTINE MONMUL(NSUB,NBLK,A,V,Z,INFO)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     * MULTIPLICATION OF A BLOCK DIAGONAL MATRIX WITH A VECTOR   *
C     *                      U.MAAS                               *
C     *                    17.4.1986                              *
C     *           LAST CHANGE: U.MAAS APR.20,1986                 *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C                                                                      *
C     < A(1)                               >   < V(1) >   < Z(1) >     *
C     <       A(2)                         >   < V(2) >   < Z(2) >     *
C     <             A(3)                   > * < V(3) > = < Z(3) >     *
C     <                    .               >   <  .   >   <  .   >     *
C     <                          .         >   <  .   >   <  .   >     *
C     <                               A(NB)>   < V(NB)>   < Z(NB)>     *
C                                                                      *
C                                                                      *
C                                                                      *
C***********************************************************************
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION A(NSUB,NSUB,NBLK),V(NSUB,NBLK),Z(NSUB,NBLK)
      DATA ZERO/0.0D0/
C***********************************************************************
C     SET SOLUTION VECTOR X TO ZERO                                    C
C***********************************************************************
                    JOB=-1
      IF(INFO.GE.0) JOB=1
      IF(INFO.NE.0) GOTO 15
      DO 10 I=1,NSUB
      DO 10 J=1,NBLK
  10  Z(I,J)=ZERO
C***********************************************************************
C     CALCULATE
C***********************************************************************
  15  CONTINUE
      DO 20 K=1,NBLK
      CALL DGQMMV(NSUB,A(1,1,K),V(1,K),Z(1,K),JOB)
  20  CONTINUE
C***********************************************************************
C     REGULAR EXIT                                                    C
C***********************************************************************
      RETURN
C***********************************************************************
C     ERROR EXITS                                                     C
C***********************************************************************
C***********************************************************************
C     END OF MONMUL                                                   C
C***********************************************************************
      END
      SUBROUTINE TRIMUL(NSUB,NBLK,A,V,Z,INFO)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *   MULTIPLICATION OF A TRIDIAGONAL MATRIX WITH A VECTOR    *
C     *                      U.MAAS                               *
C     *                    17.4.1986                              *
C     *           LAST CHANGE: U.MAAS APR.20,1986                 *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C                                                                      *
C     < B(1)  C(1)                         >   < V(1) >   < Z(1) >     *
C     < A(2)  B(2)  C(2)                   >   < V(2) >   < Z(2) >     *
C     <       A(3)  B(3)  C(3)             > * < V(3) > = < Z(3) >     *
C     <              .     .     .         >   <  .   >   <  .   >     *
C     <                    .     .     .   >   <  .   >   <  .   >     *
C     <                         A(NB) B(NB)>   < V(NB)>   < Z(NB)>     *
C                                                                      *
C                                                                      *
C                                                                      *
C***********************************************************************
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION A(NSUB,NSUB,NBLK,3),V(NSUB,NBLK),Z(NSUB,NBLK)
      DATA ZERO/0.0D0/
C***********************************************************************
C     CHECK                                                            C
C***********************************************************************
      IF(NBLK.LT.3) GOTO 9100
C***********************************************************************
C     SET SOLUTION VECTOR X TO ZERO                                    C
C***********************************************************************
                    JOB=-1
      IF(INFO.GE.0) JOB=1
      IF(INFO.NE.0) GOTO 15
      DO 10 I=1,NSUB
      DO 10 J=1,NBLK
  10  Z(I,J)=ZERO
C***********************************************************************
C     CALCULATE FIRST BLOCK                                            C
C***********************************************************************
  15  CONTINUE
      CALL DGQMMV(NSUB,A(1,1,1,2),V(1,1),Z(1,1),JOB)
      CALL DGQMMV(NSUB,A(1,1,1,3),V(1,2),Z(1,1),JOB)
C***********************************************************************
C     CALCULATE SECOND TO ONE BEFORE LAST                              C
C***********************************************************************
      NBM=NBLK-1
      DO 20 K=2,NBM
      KM1=K-1
      KP1=K+1
      CALL DGQMMV(NSUB,A(1,1,K,1),V(1,KM1),Z(1,K),JOB)
      CALL DGQMMV(NSUB,A(1,1,K,2),V(1,K),Z(1,K),JOB)
      CALL DGQMMV(NSUB,A(1,1,K,3),V(1,KP1),Z(1,K),JOB)
  20  CONTINUE
C***********************************************************************
C     CALCULATE LAST BLOCK                                            C
C***********************************************************************
      CALL DGQMMV(NSUB,A(1,1,NBLK,1),V(1,NBM),Z(1,NBLK),JOB)
      CALL DGQMMV(NSUB,A(1,1,NBLK,2),V(1,NBLK),Z(1,NBLK),JOB)
      RETURN
C***********************************************************************
C     ERROR EXITS                                                     C
C***********************************************************************
C----- LESS THAN 3 SUBMATRICES
 9100 INFO=-1
      RETURN
C***********************************************************************
C     END OF TRIMUL                                                   C
C***********************************************************************
      END
      SUBROUTINE PENMUL(N0,N1,N2,N2M,A,B,C,V,Z,INFO)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *   MULTIPLICATION OF A PENTADIAGONAL MATRIX WITH A VECTOR  *
C     *                      U.MAAS                               *
C     *                    17.4.1986                              *
C     *           LAST CHANGE: U.MAAS APR.20,1986                 *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C    STRUCTURE OF THE MATRIX                                           *
C     < X X    |X      |       |           >   <      >   <      >     *
C     < X X X  |  X    |       |           >   <      >   <      >     *
C     <   X X X|    X  |       |           > * <      > = <      >     *
C     <     X X|      X|       |           >   <      >   <      >     *
C     < X      |X X    |X      |           >   <      >   <      >     *
C     <   X    |X X X  |  X    |           >   <      >   <      >     *
C     <     X  |  X X X|    X  |           >   <      >   <      >     *
C     <       X|    X X|      X|           >   <      >   <      >     *
C     <        |X      |X X    |           >   <      >   <      >     *
C     <        |  X    |X X X  |           >   <      >   <      >     *
C     <        |    X  |  X X X|           >   <      >   <      >     *
C     <        |      X|    X X|           >   <      >   <      >     *
C                                                                      *
C   N0 = DIMENSION OF THE SUBBLOCKS X                                  *
C   N1 = NUMBER OF BLOCKS IN THE TRIDIAGONAL SUBSYSTEMS                *
C   N2 = NUMBER OF TRIDIAGONAL SUBSYSTEMS                              *
C   N2M= NUMBER OF BLOCKS IN SUB AND SUPER DIAGONALS                   *
C                                                                      *
C***********************************************************************
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION B(N0,N0,N1,3,N2),V(N0,N1,N2),Z(N0,N1,N2)
      DIMENSION A(N0,N0,N1,N2M)
      DIMENSION C(N0,N0,N1,N2M)
      DATA ZERO/0.0D0/
C***********************************************************************
C     CHECK                                                            C
C***********************************************************************
      IF(N2.LT.2) GOTO 9100
C***********************************************************************
C     SET SOLUTION VECTOR X TO ZERO                                    C
C***********************************************************************
                    JOB=-1
      IF(INFO.GE.0) JOB=1
      IF(INFO.NE.0) GOTO 15
      DO 10 I=1,N0
      DO 10 J=1,N1
      DO 10 K=1,N2
  10  Z(I,J,K)=ZERO
C***********************************************************************
C     MULTIPLICATE TRIDIAGONAL SUBMATRICES                             C
C***********************************************************************
  15  CONTINUE
      DO 100 L=1,N2
      CALL TRIMUL(N0,N1,B(1,1,1,1,L),V(1,1,L),Z(1,1,L),JOB)
  100 CONTINUE
C***********************************************************************
C     MULTIPLICATE SUPER AND SUB MATRICES                              C
C***********************************************************************
      DO 200 L=1,N2M
      CALL MONMUL(N0,N1,A(1,1,1,L),V(1,1,L),Z(1,1,L+1),JOB)
  200 CONTINUE
      DO 300 L=1,N2M
      CALL MONMUL(N0,N1,C(1,1,1,L),V(1,1,L+1),Z(1,1,L),JOB)
  300 CONTINUE
C***********************************************************************
C     CALCULATE LAST BLOCK                                            C
C***********************************************************************
      RETURN
C***********************************************************************
C     ERROR EXITS                                                     C
C***********************************************************************
C----- LESS THAN 3 SUBMATRICES
 9100 INFO=-1
      RETURN
C***********************************************************************
C     END OF PENMUL                                                   C
C***********************************************************************
      END
      SUBROUTINE DNMMUL(N0,N1,N2,N2M,A,B,C,V,Z,INFO)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *   MULTIPLICATION OF A NONA DIAGONAL MATRIX WITH A VECTOR  *
C     *                      U.MAAS                               *
C     *                    17.4.1989                              *
C     *           LAST CHANGE: U.MAAS APR.20,1989                 *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C    STRUCTURE OF THE MATRIX                                           *
C     < X X    |X X    |       |           >   <      >   <      >     *
C     < X X X  |X X X  |       |           >   <      >   <      >     *
C     <   X X X|  X X X|       |           > * <      > = <      >     *
C     <     X X|    X X|       |           >   <      >   <      >     *
C     < X X    |X X    |X X    |           >   <      >   <      >     *
C     < X X X  |X X X  |X X X  |           >   <      >   <      >     *
C     <   X X X|  X X X|  X X X|           >   <      >   <      >     *
C     <     X X|    X X|    X X|           >   <      >   <      >     *
C     <        |X X    |X X    |           >   <      >   <      >     *
C     <        |X X X  |X X X  |           >   <      >   <      >     *
C     <        |  X X X|  X X X|           >   <      >   <      >     *
C     <        |    X X|    X X|           >   <      >   <      >     *
C                                                                      *
C   N0 = DIMENSION OF THE SUBBLOCKS X                                  *
C   N1 = NUMBER OF BLOCKS IN THE TRIDIAGONAL SUBSYSTEMS                *
C   N2 = NUMBER OF TRIDIAGONAL SUBSYSTEMS                              *
C   N2M= NUMBER OF BLOCKS IN SUB AND SUPER DIAGONALS                   *
C                                                                      *
C***********************************************************************
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION B(N0,N0,N1,3,N2),V(N0,N1,N2),Z(N0,N1,N2)
      DIMENSION A(N0,N0,N1,3,N2M)
      DIMENSION C(N0,N0,N1,3,N2M)
      DATA ZERO/0.0D0/
C***********************************************************************
C     CHECK                                                            C
C***********************************************************************
      IF(N2.LT.2) GOTO 9100
C***********************************************************************
C     SET SOLUTION VECTOR X TO ZERO                                    C
C***********************************************************************
                    JOB=-1
      IF(INFO.GE.0) JOB=1
      IF(INFO.NE.0) GOTO 15
      DO 10 I=1,N0
      DO 10 J=1,N1
      DO 10 K=1,N2
  10  Z(I,J,K)=ZERO
C***********************************************************************
C     MULTIPLICATE TRIDIAGONAL SUBMATRICES                             C
C***********************************************************************
  15  CONTINUE
      DO 100 L=1,N2
      CALL TRIMUL(N0,N1,B(1,1,1,1,L),V(1,1,L),Z(1,1,L),JOB)
  100 CONTINUE
C***********************************************************************
C     MULTIPLICATE SUPER AND SUB MATRICES                              C
C***********************************************************************
      DO 200 L=1,N2M
      CALL TRIMUL(N0,N1,A(1,1,1,1,L),V(1,1,L),Z(1,1,L+1),JOB)
  200 CONTINUE
      DO 300 L=1,N2M
      CALL TRIMUL(N0,N1,C(1,1,1,1,L),V(1,1,L+1),Z(1,1,L),JOB)
  300 CONTINUE
C***********************************************************************
C     CALCULATE LAST BLOCK                                            C
C***********************************************************************
      RETURN
C***********************************************************************
C     ERROR EXITS                                                     C
C***********************************************************************
C----- LESS THAN 3 SUBMATRICES
 9100 INFO=-1
      RETURN
C***********************************************************************
C     END OF NONMUL                                                   C
C***********************************************************************
      END
      SUBROUTINE DNTMUL(N0,N1,N2,N2M,A,B,C,V,Z,INFO)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *   MULT. OF TRANSP OF A NONA DIAGONAL MATRIX WITH A VECTOR  *
C     *                      U.MAAS                               *
C     *                    17.4.1989                              *
C     *           LAST CHANGE: U.MAAS APR.20,1989                 *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C    STRUCTURE OF THE MATRIX                                           *
C     < X X    |X X    |       |           >   <      >   <      >     *
C     < X X X  |X X X  |       |           >   <      >   <      >     *
C     <   X X X|  X X X|       |           > * <      > = <      >     *
C     <     X X|    X X|       |           >   <      >   <      >     *
C     < X X    |X X    |X X    |           >   <      >   <      >     *
C     < X X X  |X X X  |X X X  |           >   <      >   <      >     *
C     <   X X X|  X X X|  X X X|           >   <      >   <      >     *
C     <     X X|    X X|    X X|           >   <      >   <      >     *
C     <        |X X    |X X    |           >   <      >   <      >     *
C     <        |X X X  |X X X  |           >   <      >   <      >     *
C     <        |  X X X|  X X X|           >   <      >   <      >     *
C     <        |    X X|    X X|           >   <      >   <      >     *
C                                                                      *
C   N0 = DIMENSION OF THE SUBBLOCKS X                                  *
C   N1 = NUMBER OF BLOCKS IN THE TRIDIAGONAL SUBSYSTEMS                *
C   N2 = NUMBER OF TRIDIAGONAL SUBSYSTEMS                              *
C   N2M= NUMBER OF BLOCKS IN SUB AND SUPER DIAGONALS                   *
C                                                                      *
C***********************************************************************
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION B(N0,N0,N1,3,N2),V(N0,N1,N2),Z(N0,N1,N2)
      DIMENSION A(N0,N0,N1,3,N2M)
      DIMENSION C(N0,N0,N1,3,N2M)
      DATA ZERO/0.0D0/
C***********************************************************************
C     CHECK                                                            C
C***********************************************************************
      IF(N2.LT.2) GOTO 9100
C***********************************************************************
C     SET SOLUTION VECTOR X TO ZERO                                    C
C***********************************************************************
                    JOB=-1
      IF(INFO.GE.0) JOB=1
      IF(INFO.NE.0) GOTO 15
      DO 10 I=1,N0
      DO 10 J=1,N1
      DO 10 K=1,N2
  10  Z(I,J,K)=ZERO
C***********************************************************************
C     MULTIPLICATE TRIDIAGONAL SUBMATRICES                             C
C***********************************************************************
  15  CONTINUE
      DO 100 L=1,N2
      CALL DTTMUL(N0,N1,B(1,1,1,1,L),V(1,1,L),Z(1,1,L),JOB)
  100 CONTINUE
C***********************************************************************
C     MULTIPLICATE SUPER AND SUB MATRICES                              C
C***********************************************************************
      DO 200 L=1,N2M
      CALL DTTMUL(N0,N1,C(1,1,1,1,L),V(1,1,L),Z(1,1,L+1),JOB)
  200 CONTINUE
      DO 300 L=1,N2M
      CALL DTTMUL(N0,N1,A(1,1,1,1,L),V(1,1,L+1),Z(1,1,L),JOB)
  300 CONTINUE
C***********************************************************************
C     CALCULATE LAST BLOCK                                            C
C***********************************************************************
      RETURN
C***********************************************************************
C     ERROR EXITS                                                     C
C***********************************************************************
C----- LESS THAN 3 SUBMATRICES
 9100 INFO=-1
      RETURN
C***********************************************************************
C     END OF NONMUL                                                   C
C***********************************************************************
      END
      SUBROUTINE DPITSL(NV,N0,N1,N2,N2M,A,B,C,Z,X2,X1,
     1         RTOL,ATOL,MAXIT,INFO,IFAIL)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *   SOLUTION OF A       PENTADIAGONAL MATRIX WITH A VECTOR  *
C     *                      U.MAAS                               *
C     *                    17.4.1986                              *
C     *           LAST CHANGE: U.MAAS APR.20,1986                 *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C    STRUCTURE OF THE MATRIX                                           *
C     < X X    |X      |       |           >   <      >   <      >     *
C     < X X X  |  X    |       |           >   <      >   <      >     *
C     <   X X X|    X  |       |           > * <      > = <      >     *
C     <     X X|      X|       |           >   <      >   <      >     *
C     < X      |X X    |X      |           >   <      >   <      >     *
C     <   X    |X X X  |  X    |           >   <      >   <      >     *
C     <     X  |  X X X|    X  |           >   <      >   <      >     *
C     <       X|    X X|      X|           >   <      >   <      >     *
C     <        |X      |X X    |           >   <      >   <      >     *
C     <        |  X    |X X X  |           >   <      >   <      >     *
C     <        |    X  |  X X X|           >   <      >   <      >     *
C     <        |      X|    X X|           >   <      >   <      >     *
C                                                                      *
C   N0 = DIMENSION OF THE SUBBLOCKS X                                  *
C   N1 = NUMBER OF BLOCKS IN THE TRIDIAGONAL SUBSYSTEMS                *
C   N2 = NUMBER OF TRIDIAGONAL SUBSYSTEMS                              *
C   N2M= NUMBER OF BLOCKS IN SUB AND SUPER DIAGONALS                   *
C     INFO = 1 SOLUTION WITH INITIAL GUESS = ZERO                      *
C     INFO = 2 SOLUTION WITH INITIAL GUESS GIVEN                       *
C     INFO = 3 SOLUTION WITH DECOMPOSITION GIVEN                       *
C     IF INFO IS NEGATIVE METHOD OF JACOBI IS USED                     *
C     IF INFO IS POSITIVE GS- METHOD IS USED                           *
C     RETURN CODES (IFAIL)
C     IFAIL =  0 SYSTEM IS A TRIDIAGONAL MATRIX
C     IFAIL =  1 ERROR DURING DECOMPOSITION
C     IFAIL =  2 NOT SUCCEDED WITHIN MAXIT ITERATIONS
C     IFAIL =  9 BAD SYSTEM (NOT PENTADIAG.)
C     IFAIL <  0 SUCCESSFUL RETURN, IFAIL GIVES NUMBER OF ITERATIONS
C***********************************************************************
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION B(N0,N0,N1,3,N2),Z(N0,N1,N2)
      DIMENSION X1(N0,N1,N2),X2(N0,N1,N2)
      DIMENSION A(N0,N0,N1,N2M)
      DIMENSION C(N0,N0,N1,N2M)
      DATA ZERO/0.0D0/
C     DATA OMEGA/0.5D0/
C***********************************************************************
C
C     INITIALIZE AND CHECK                                             C
C
C***********************************************************************
      IF(INFO.EQ.3.OR.INFO.EQ.-3) GOTO 50
C***********************************************************************
C     DECOMPOSE EACH TRIDIAGONAL SUBMATRIX
C***********************************************************************
      DO 10 L=1,N2
      CALL DGTFA(N1,N0,B(1,1,1,1,L),ILL)
      IF(ILL.NE.0) GOTO 9200
   10 CONTINUE
C***********************************************************************
C     SET INITIAL GUESS
C***********************************************************************
   50 CONTINUE
      IF(N2.EQ.1) GOTO 400
      IF(N2.LE.0) GOTO 9100
      IF(IABS(INFO).EQ.2) GOTO 100
C---- SET INITIAL GUESS TO ZERO
      DO 60 K=1,N2
      DO 60 J=1,N1
      DO 60 I=1,N0
      X1(I,J,K)=ZERO
   60 CONTINUE
C***********************************************************************
C
C     DECIDE ABOUT ITERATION METHOD                                    C
C
C***********************************************************************
  100 CONTINUE
      IF(INFO.GT.0) GOTO 300
      GOTO 200
C***********************************************************************
C
C     ITERATION FOR JACOBIAN METHOD OR JOR                             C
C
C***********************************************************************
  200 CONTINUE
      NITER=0
C***********************************************************************
C     ITERATION LOOP
C***********************************************************************
  250 CONTINUE
      NITER=NITER+1
      IF(NITER.GT.MAXIT) GOTO 950
C---- CALCULATE RIGHT HAND SIDE
      DO 260 K=1,N2
      DO 260 J=1,N1
      DO 260 I=1,N0
      X2(I,J,K)=Z(I,J,K)
 260  CONTINUE
      DO 270 L=1,N2M
      CALL MONMUL(N0,N1,A(1,1,1,L),X1(1,1,L),X2(1,1,L+1),-1)
      CALL MONMUL(N0,N1,C(1,1,1,L),X1(1,1,L+1),X2(1,1,L),-1)
 270  CONTINUE
C---- SOLVE EQUATION SYSTEM
      DO 280 L=1,N2
      CALL DGTSL(N1,N0,B(1,1,1,1,L),X2(1,1,L))
  280 CONTINUE
C---- STORE NEW SOLUTION AND CALCULATE ERROR
      ERR=ZERO
      DO 290 K=1,N2
      DO 290 J=1,N1
      DO 290 I=1,N0
C---- OMEGA
C     X2(I,J,K)=OMEGA*X2(I,J,K)+ (1.0D0-OMEGA) * X1(I,J,K)
      D=X2(I,J,K)-X1(I,J,K)
CSCAL U=D/DMAX1(ATOL,DABS(X2(I,J,K)))
CSCAL ERR=ERR+U*U
      ERR=ERR+D*D
      X1(I,J,K)=X2(I,J,K)
 290  CONTINUE
      ERR=DSQRT(ERR/FLOAT(NV))
      IF(ERR.LT.RTOL) GOTO 600
      GOTO 250
C***********************************************************************
C
C     BLOCK FOR GS AND SOR METHOD
C
C***********************************************************************
  300 CONTINUE
      NITER=0
C***********************************************************************
C     PERFORM ITERATION
C***********************************************************************
  320 CONTINUE
      NITER=NITER+1
      IF(NITER.GT.MAXIT) GOTO 950
      ERR=ZERO
C---- CALCULATE RIGHT HAND SIDE FOR FIRST ROW
      DO 330 K=1,N2
      DO 330 J=1,N1
      DO 330 I=1,N0
      X2(I,J,K)=Z(I,J,K)
  330 CONTINUE
      CALL MONMUL(N0,N1,C(1,1,1,1),X1(1,1,2),X2(1,1,1),-1)
      CALL DGTSL(N1,N0,B(1,1,1,1,1),X2(1,1,1))
C---- COMPUTE ERROR AND STORE NEW X
      DO 340 I=1,N0
      DO 340 J=1,N1
C---- OMEGA
C     X2(I,J,1)=OMEGA*X2(I,J,1)+ (1.0D0-OMEGA) * X1(I,J,1)
C----
      D=X2(I,J,1)-X1(I,J,1)
CSCAL U=D/DMAX1(ATOL,DABS(X2(I,J,1)))
CSCAL ERR=ERR+U*U
      ERR=ERR+D*D
      X1(I,J,1)=X2(I,J,1)
 340  CONTINUE
C---- CALCULATE RIGHT HAND SIDE FOR SECOND TO ONE BEFORE LAST ROW
      DO 360 L=2,N2M
      CALL MONMUL(N0,N1,A(1,1,1,L-1),X1(1,1,L-1),X2(1,1,L),-1)
      CALL MONMUL(N0,N1,C(1,1,1,L),X1(1,1,L+1),X2(1,1,L),-1)
      CALL DGTSL(N1,N0,B(1,1,1,1,L),X2(1,1,L))
C---- COMPUTE ERROR AND STORE NEW X
      DO 350 I=1,N0
      DO 350 J=1,N1
C---- OMEGA
C     X2(I,J,L)=OMEGA*X2(I,J,L)+ (1.0D0-OMEGA) * X1(I,J,L)
C----
      D=X2(I,J,L)-X1(I,J,L)
CSCAL U=D/DMAX1(ATOL,DABS(X2(I,J,L)))
CSCAL ERR=ERR+U*U
      ERR=ERR+D*D
      X1(I,J,L)=X2(I,J,L)
 350  CONTINUE
 360  CONTINUE
C---- COMPUTE LAST ROW
      CALL MONMUL(N0,N1,A(1,1,1,N2M),X1(1,1,N2M),X2(1,1,N2),-1)
      CALL DGTSL(N1,N0,B(1,1,1,1,N2),X2(1,1,N2))
C---- COMPUTE ERROR AND STORE NEW X
      DO 370 I=1,N0
      DO 370 J=1,N1
C---- OMEGA
C     X2(I,J,N2)=OMEGA*X2(I,J,L)+ (1.0D0-OMEGA) * X1(I,J,N2)
C----
      D=X2(I,J,N2)-X1(I,J,N2)
CSCAL U=D/DMAX1(ATOL,DABS(X2(I,J,N2)))
CSCAL ERR=ERR+U*U
      ERR=ERR+D*D
      X1(I,J,N2)=X2(I,J,N2)
 370  CONTINUE
C---- CALCULATE OVERALL ERROR
      ERR=DSQRT(ERR/FLOAT(NV))
      IF(ERR.LT.RTOL) GOTO 600
      GOTO 320
C***********************************************************************
C      TRI DIAGONAL SYSTEM                                            C
C***********************************************************************
  400 NITER=0
      CALL DGTSL(N1,N0,B(1,1,1,1,1),Z(1,1,1))
      RETURN
C***********************************************************************
C     CALCULATE LAST BLOCK                                            C
C***********************************************************************
  600 CONTINUE
      IFAIL=-NITER
C---- STORE SOLUTION IN Z
      DO 610 K=1,N2
      DO 610 J=1,N1
      DO 610 I=1,N0
      Z(I,J,K)=X1(I,J,K)
  610 CONTINUE
      RETURN
C***********************************************************************
C     ERROR EXITS                                                     C
C***********************************************************************
C----- LESS THAN 3 SUBMATRICES
 9100 IFAIL= 9
      RETURN
 9200 IFAIL= 1
      RETURN
  950 IFAIL= 2
      RETURN
C***********************************************************************
C     END OF DPITSL                                                   C
C***********************************************************************
      END
      SUBROUTINE DNITSL(NV,N0,N1,N2,N2M,A,B,C,Z,X2,X1,
     1         RTOL,ATOL,MAXIT,INFO,IFAIL)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *   SOLUTION OF A       PENTADIAGONAL MATRIX WITH A VECTOR  *
C     *                      U.MAAS                               *
C     *                    17.4.1986                              *
C     *           LAST CHANGE: U.MAAS APR.20,1986                 *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C    STRUCTURE OF THE MATRIX                                           *
C     < X X    |X      |       |           >   <      >   <      >     *
C     < X X X  |  X    |       |           >   <      >   <      >     *
C     <   X X X|    X  |       |           > * <      > = <      >     *
C     <     X X|      X|       |           >   <      >   <      >     *
C     < X      |X X    |X      |           >   <      >   <      >     *
C     <   X    |X X X  |  X    |           >   <      >   <      >     *
C     <     X  |  X X X|    X  |           >   <      >   <      >     *
C     <       X|    X X|      X|           >   <      >   <      >     *
C     <        |X      |X X    |           >   <      >   <      >     *
C     <        |  X    |X X X  |           >   <      >   <      >     *
C     <        |    X  |  X X X|           >   <      >   <      >     *
C     <        |      X|    X X|           >   <      >   <      >     *
C                                                                      *
C   N0 = DIMENSION OF THE SUBBLOCKS X                                  *
C   N1 = NUMBER OF BLOCKS IN THE TRIDIAGONAL SUBSYSTEMS                *
C   N2 = NUMBER OF TRIDIAGONAL SUBSYSTEMS                              *
C   N2M= NUMBER OF BLOCKS IN SUB AND SUPER DIAGONALS                   *
C     INFO = 1 SOLUTION WITH INITIAL GUESS = ZERO                      *
C     INFO = 2 SOLUTION WITH INITIAL GUESS GIVEN                       *
C     INFO = 3 SOLUTION WITH DECOMPOSITION GIVEN                       *
C     INFO = 4 SOLUTION WITH DECOMPOSITION AND INITIAL GUESS GIVEN     *
C     IF INFO IS NEGATIVE METHOD OF JACOBI IS USED                     *
C     IF INFO IS POSITIVE GS- METHOD IS USED                           *
C     RETURN CODES (IFAIL)
C     IFAIL =  0 SYSTEM IS A TRIDIAGONAL MATRIX
C     IFAIL =  1 ERROR DURING DECOMPOSITION
C     IFAIL =  2 NOT SUCCEDED WITHIN MAXIT ITERATIONS
C     IFAIL =  9 BAD SYSTEM (NOT PENTADIAG.)
C     IFAIL <  0 SUCCESSFUL RETURN, IFAIL GIVES NUMBER OF ITERATIONS
C***********************************************************************
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION B(N0,N0,N1,3,N2),Z(N0,N1,N2)
      DIMENSION X1(N0,N1,N2),X2(N0,N1,N2)
      DIMENSION A(N0,N0,N1,3,N2M)
      DIMENSION C(N0,N0,N1,3,N2M)
C     DIMENSION ERRO(30)
      DATA ZERO/0.0D0/
C     DATA OMEGA/0.5D0/
C***********************************************************************
C
C     INITIALIZE AND CHECK                                             C
C
C***********************************************************************
C     IIIMAX=2
C     LLLMAX=6
C     KKKMAX=3
C     GOTO 950
      IF(INFO.GE.3.OR.INFO.LE.-3) GOTO 50
C***********************************************************************
C     DECOMPOSE EACH TRIDIAGONAL SUBMATRIX
C***********************************************************************
      DO 10 L=1,N2
      CALL DGTFA(N1,N0,B(1,1,1,1,L),ILL)
      IF(ILL.NE.0) GOTO 9200
   10 CONTINUE
C***********************************************************************
C     SET INITIAL GUESS
C***********************************************************************
   50 CONTINUE
      IF(N2.EQ.1) GOTO 400
      IF(N2.LE.0) GOTO 9100
      IF(IABS(INFO).EQ.2.OR.IABS(INFO).EQ.4) GOTO 100
C---- SET INITIAL GUESS TO ZERO
      DO 60 K=1,N2
      DO 60 J=1,N1
      DO 60 I=1,N0
      X1(I,J,K)=ZERO
   60 CONTINUE
C***********************************************************************
C
C     DECIDE ABOUT ITERATION METHOD                                    C
C
C***********************************************************************
  100 CONTINUE
      IF(INFO.GT.0) GOTO 300
      GOTO 200
C***********************************************************************
C
C     ITERATION FOR JACOBIAN METHOD OR JOR                             C
C
C***********************************************************************
  200 CONTINUE
      INFTRI=-1
      NITER=0
C***********************************************************************
C     ITERATION LOOP
C***********************************************************************
  250 CONTINUE
      NITER=NITER+1
      IF(NITER.GT.MAXIT) GOTO 950
C---- CALCULATE RIGHT HAND SIDE
      DO 260 K=1,N2
      DO 260 J=1,N1
      DO 260 I=1,N0
      X2(I,J,K)=Z(I,J,K)
 260  CONTINUE
      DO 270 L=1,N2M
      CALL TRIMUL(N0,N1,A(1,1,1,1,L),X1(1,1,L),X2(1,1,L+1),INFTRI)
      CALL TRIMUL(N0,N1,C(1,1,1,1,L),X1(1,1,L+1),X2(1,1,L),INFTRI)
 270  CONTINUE
C---- SOLVE EQUATION SYSTEM
      DO 280 L=1,N2
      CALL DGTSL(N1,N0,B(1,1,1,1,L),X2(1,1,L))
  280 CONTINUE
C---- STORE NEW SOLUTION AND CALCULATE ERROR
CREL  RELEUK=ZERO
CREL  RELMAX=ZERO
      ABSMAX=ZERO
      ABSEUK=ZERO
      DO 290 K=1,N2
      DO 290 J=1,N1
      DO 290 I=1,N0
C---- OMEGA
C     X2(I,J,K)=OMEGA*X2(I,J,K)+ (1.0D0-OMEGA) * X1(I,J,K)
      D=X2(I,J,K)-X1(I,J,K)
CREL  UNEW=D/DMAX1(ATOL,DABS(X2(I,J,1)))
CREL  RELMAX=DMAX1(RELMAX,UNEW)
CREL  RELEUK=RELEUK+UNEW*UNEW
      ABSMAX=DMAX1(ABSMAX,DABS(D))
      ABSEUK=ABSEUK+D*D
      X1(I,J,K)=X2(I,J,K)
 290  CONTINUE
C---- CALCULATE OVERALL ERROR AND PRINT
CREL  RELEUK=DSQRT(RELEUK/FLOAT(NV))
CREL  WRITE(6,842) RELMAX,RELEUK,ABSMAX,ABSEUK
      ABSEUK=DSQRT(ABSEUK/FLOAT(NV))
      WRITE(6,842) ABSMAX,ABSEUK
C     ERRO(NITER)=ABSMAX
      IF(NITER.EQ.1) THEN
        ABSEU0=ABSEUK
        ABSMA0=ABSMAX
      ENDIF
C---- ABEND IF SOLUTION SEEMS TO DIVERGE
      IF(ABSMAX.GT.1.D3*ABSMA0) GOTO 950
C---- ACCEPT SOLUTION IF ERRORS/INITIAL.ERRORS ARE SMALLER THEN RTOL
      IF((ABSMAX/ABSMA0).LT.RTOL.AND.(ABSEUK/ABSEU0).LT.RTOL) GOTO 600
C---- ACCEPT SOLUTION IF ERRORS ARE SMALLER THEN ATOL
      IF(ABSMAX.LT.ATOL.AND.ABSEUK.LT.ATOL.AND.NITER.GT.1) GOTO 600
C     IF(ERR.LT.RTOL) GOTO 600
      GOTO 250
C***********************************************************************
C
C     BLOCK FOR GS AND SOR METHOD
C
C***********************************************************************
  300 CONTINUE
      NITER=0
      INFTRI=-1
      ISWEEP = 1
      OMEGA= 1.0D0
C     WRITE(6,*) '  OMEGA ? '
C     READ(*,*) OMEGA
C     WRITE(6,*) ' CHANGE DIRECTION? 1 FRW 2 BCKW  NEGATIVE FOR CHANGE'
C     READ(*,*) ICOSWE
C     IF( ABS(ICOSWE).EQ.2) ISWEEP=1
C     IF( ABS(ICOSWE).EQ.1) ISWEEP=-1
C***********************************************************************
C     PERFORM ITERATION
C***********************************************************************
  320 CONTINUE
      NITER=NITER+1
      IF(NITER.GT.MAXIT) GOTO 950
C     ISWEEP= - ISWEEP
C     IF(ICOSWE.LT.0) ISWEEP= - ISWEEP
      IF(ISWEEP.GT.0) THEN
         IFIRST = 1
         ILAST  = N2
      ELSE
         IFIRST = N2
         ILAST  = 1
      ENDIF
C---- SET ERRORS TO ZERO
      ERR=ZERO
CREL  RELEUK=ZERO
CREL  RELMAX=ZERO
      ABSMAX=ZERO
      ABSEUK=ZERO
C---- CALCULATE STARTING RIGHT HAND SIDE
      DO 330 K=1,N2
      DO 330 J=1,N1
      DO 330 I=1,N0
      X2(I,J,K)=Z(I,J,K)
  330 CONTINUE
C
C---- COMPUTE FIRST BLOCK CONSIDERED
C
      IF(ISWEEP.EQ.1) THEN
          CALL TRIMUL(N0,N1,C(1,1,1,1,1),X1(1,1,2),X2(1,1,1),INFTRI)
          CALL DGTSL(N1,N0,B(1,1,1,1,1),X2(1,1,1))
      ELSE
         CALL TRIMUL(N0,N1,A(1,1,1,1,N2M),X1(1,1,N2M),X2(1,1,N2),INFTRI)
         CALL DGTSL(N1,N0,B(1,1,1,1,N2),X2(1,1,N2))
      ENDIF
C---- COMPUTE ERROR AND STORE NEW X
      DO 340 I=1,N0
      DO 340 J=1,N1
      D=X2(I,J,IFIRST)-X1(I,J,IFIRST)
CREL  UNEW=D/DMAX1(ATOL,DABS(X2(I,J,IFIRST)))
CREL  RELMAX=DMAX1(RELMAX,UNEW)
CREL  RELEUK=RELEUK+UNEW*UNEW
      IF(DABS(D).GT.ABSMAX) THEN
      IIIMAX = I
      LLLMAX = J
      KKKMAX = IFIRST
      ENDIF
      ABSMAX=DMAX1(ABSMAX,DABS(D))
      ABSEUK=ABSEUK+D*D
C---- SOR
      X1(I,J,IFIRST)=      X1(I,J,IFIRST)+ OMEGA * D
C----
CSOR  X1(I,J,IFIRST)=X2(I,J,IFIRST)
 340  CONTINUE
C
C---- CALCULATE RIGHT HAND SIDE FOR SECOND TO ONE BEFORE LAST ROW
C
      L=IFIRST
      DO 360 LLL=2,N2M
      L= L + ISWEEP
      CALL TRIMUL(N0,N1,A(1,1,1,1,L-1),X1(1,1,L-1),X2(1,1,L),INFTRI)
      CALL TRIMUL(N0,N1,C(1,1,1,1,L),X1(1,1,L+1),X2(1,1,L),INFTRI)
      CALL DGTSL(N1,N0,B(1,1,1,1,L),X2(1,1,L))
C---- COMPUTE ERROR AND STORE NEW X
      DO 350 I=1,N0
      DO 350 J=1,N1
      D=X2(I,J,L)-X1(I,J,L)
CREL  UNEW=D/DMAX1(ATOL,DABS(X2(I,J,L)))
CREL  RELMAX=DMAX1(RELMAX,UNEW)
CREL  RELEUK=RELEUK+UNEW*UNEW
      IF(DABS(D).GT.ABSMAX) THEN
      IIIMAX = I
      LLLMAX = J
      KKKMAX = L
      ENDIF
      ABSMAX=DMAX1(ABSMAX,DABS(D))
      ABSEUK=ABSEUK+D*D
C---- SOR
      X1(I,J,L)=      X1(I,J,L)+        OMEGA * D
C----
CSOR  X1(I,J,L)=X2(I,J,L)
 350  CONTINUE
 360  CONTINUE
C
C---- COMPUTE LAST  BLOCK CONSIDERED
C
      IF(ISWEEP.EQ.1) THEN
        CALL TRIMUL(N0,N1,A(1,1,1,1,N2M),X1(1,1,N2M),X2(1,1,N2),INFTRI)
        CALL DGTSL(N1,N0,B(1,1,1,1,N2),X2(1,1,N2))
      ELSE
          CALL TRIMUL(N0,N1,C(1,1,1,1,1),X1(1,1,2),X2(1,1,1),INFTRI)
          CALL DGTSL(N1,N0,B(1,1,1,1,1),X2(1,1,1))
      ENDIF
C---- COMPUTE ERROR AND STORE NEW X
      DO 370 I=1,N0
      DO 370 J=1,N1
      D=X2(I,J,ILAST)-X1(I,J,ILAST)
CREL  UNEW=D/DMAX1(ATOL,DABS(X2(I,J,ILAST)))
CREL  RELMAX=DMAX1(RELMAX,UNEW)
CREL  RELEUK=RELEUK+UNEW*UNEW
      IF(DABS(D).GT.ABSMAX) THEN
      IIIMAX = I
      LLLMAX = J
      KKKMAX = ILAST
      ENDIF
      ABSMAX=DMAX1(ABSMAX,DABS(D))
      ABSEUK=ABSEUK+D*D
C---- SOR
      X1(I,J,ILAST)=      X1(I,J,ILAST)+  OMEGA * D
C----
CSOR  X1(I,J,ILAST)=X2(I,J,ILAST)
 370  CONTINUE
C---- CALCULATE OVERALL ERROR
CREL  RELEUK=DSQRT(RELEUK/FLOAT(NV))
      ABSEUK=DSQRT(ABSEUK/FLOAT(NV))
CREL  WRITE(6,842) RELMAX,RELEUK,ABSMAX,ABSEUK
C842  FORMAT(1X,'RMA=',1PE9.2,1X,'REU=',1PE9.2,1X,'AMA=',1PE9.2,'AEU=',
C    1    1PE9.2)
      WRITE(6,842) ABSMAX,IIIMAX,LLLMAX,KKKMAX,ABSEUK
 842  FORMAT(1X,' MAX. ERR.: ',1PE9.2,1X,'(',I2,',',I2,',',I2,')',4X,
     1       1X,' EUK. ERR.: ',1PE9.2)
C     ERRO(NITER)=ABSMAX
      IF(NITER.EQ.1) THEN
        ABSEU0=ABSEUK
        ABSMA0=ABSMAX
      ENDIF
C---- ABEND IF SOLUTION SEEMS TO DIVERGE
      IF(ABSMAX.GT.1.D3*ABSMA0) GOTO 950
C---- ACCEPT SOLUTION IF ERRORS/INITIAL.ERRORS ARE SMALLER THEN RTOL
      IF((ABSMAX/ABSMA0).LT.RTOL.AND.(ABSEUK/ABSEU0).LT.RTOL) GOTO 600
C---- ACCEPT SOLUTION IF ERRORS ARE SMALLER THEN ATOL
      IF(ABSMAX.LT.ATOL.AND.ABSEUK.LT.ATOL.AND.NITER.GT.1)   GOTO 600
      GOTO 320
C***********************************************************************
C      TRI DIAGONAL SYSTEM                                            C
C***********************************************************************
  400 NITER=0
      CALL DGTSL(N1,N0,B(1,1,1,1,1),Z(1,1,1))
      RETURN
C***********************************************************************
C     STORE SOLUTION                                                  C
C***********************************************************************
  600 CONTINUE
      IFAIL=-NITER
C---- STORE SOLUTION IN Z
      DO 610 K=1,N2
      DO 610 J=1,N1
      DO 610 I=1,N0
      Z(I,J,K)=X1(I,J,K)
  610 CONTINUE
      RETURN
C***********************************************************************
C     ERROR EXITS                                                     C
C***********************************************************************
C----- LESS THAN 3 SUBMATRICES
 9100 IFAIL= 9
      RETURN
 9200 IFAIL= 1
      RETURN
  950 IFAIL= 2
C     WRITE(6,888) (ERRO(I),I=1,NITER)
C 888 FORMAT(3X,' CORRECTIONS DURING ITERATION ',/,1(3X,1PE11.3))
  889  FORMAT(13(1PE9.2))
      WRITE(80,*) ' A(',IIIMAX,',J,',LLLMAX,',1,',KKKMAX-1,')'
      WRITE(80,889) (A(IIIMAX,J,LLLMAX,1,KKKMAX-1),J=1,N0)
      WRITE(80,*) ' A(',IIIMAX,',J,',LLLMAX,',2,',KKKMAX-1,')'
      WRITE(80,889) (A(IIIMAX,J,LLLMAX,2,KKKMAX-1),J=1,N0)
      WRITE(80,*) ' A(',IIIMAX,',J,',LLLMAX,',3,',KKKMAX-1,')'
      WRITE(80,889) (A(IIIMAX,J,LLLMAX,3,KKKMAX-1),J=1,N0)
      WRITE(80,*) ' B(',IIIMAX,',J,',LLLMAX,',1,',KKKMAX  ,')'
      WRITE(80,889) (B(IIIMAX,J,LLLMAX,1,KKKMAX  ),J=1,N0)
      WRITE(80,*) ' B(',IIIMAX,',J,',LLLMAX,',2,',KKKMAX  ,')'
      WRITE(80,889) (B(IIIMAX,J,LLLMAX,2,KKKMAX  ),J=1,N0)
      WRITE(80,*) ' B(',IIIMAX,',J,',LLLMAX,',3,',KKKMAX  ,')'
      WRITE(80,889) (B(IIIMAX,J,LLLMAX,3,KKKMAX  ),J=1,N0)
      WRITE(80,*) ' C(',IIIMAX,',J,',LLLMAX,',1,',KKKMAX  ,')'
      WRITE(80,889) (C(IIIMAX,J,LLLMAX,1,KKKMAX  ),J=1,N0)
      WRITE(80,*) ' C(',IIIMAX,',J,',LLLMAX,',2,',KKKMAX  ,')'
      WRITE(80,889) (C(IIIMAX,J,LLLMAX,2,KKKMAX  ),J=1,N0)
      WRITE(80,*) ' C(',IIIMAX,',J,',LLLMAX,',2,',KKKMAX  ,')'
      WRITE(80,889) (C(IIIMAX,J,LLLMAX,3,KKKMAX  ),J=1,N0)
      RETURN
C***********************************************************************
C     END OF NITSL                                                   C
C***********************************************************************
      END
      SUBROUTINE ERRLIN(JOB,NFIERR,IMAT,N,ATOL,RTOL,MAXIT,
     1         W1,W2,IPIVOT,NDMAB,B,DEL,IFAIL)
C***********************************************************************
C                                                                      *
C     ************************************************************     *
C     *                                                          *     *
C     *  MANAGEMENT OF ERRORS AND FAILURES IN LINEAR ALGEBRA     *     *
C     *                                                          *     *
C     *                AUTHOR: ULRICH MAAS  4/25/1989            *     *
C     *                                                          *     *
C     *                LAST CHANGE:  4/25/1989/MAAS              *     *
C     *                                                          *     *
C     ************************************************************     *
C                                                                      *
C***********************************************************************
C                                                                      *
C     NOTE: NO CHECK FOR SUFFICIENT ARRAY STORAGE PERFORMED!!!         *
C                                                                      *
C***********************************************************************
C                                                                      *
C     INPUT DATA:                                                      *
C                                                                      *
C      JOB          INFORMATION ABOUT TASK                             *
C                     0 FOR STANDAR ERROR HANDLING                     *
C                     1 FOR OUTPUT OF THE LINEAR EQUATION SYSTEM       *
C                       ON A RESART FILE
C                                                                      *
C      IMAT(10)     INFORMATION ABOUT MATRIX (SEE BELOW)               *
C                                                                      *
C      N            DIMENSION OF THE SYSTEM                            *
C                                                                      *
C      ATOL         ABSOLUTE ERROR TOLERANCE FOR ITERATIVE METHODS     *
C                     (PASS A DUMMY IF SYSTEM IS NOT PENTA OR NONA     *
C                     DIAGONAL BLOCKED)                                *
C                                                                      *
C      RTOL         RELATIVE ERROR TOLERANCE FOR ITERATIVE METHODS     *
C                     (PASS A DUMMY IF SYSTEM IS NOT PENTA OR NONA     *
C                     DIAGONAL BLOCKED)                                *
C                                                                      *
C      MAXIT        MAXIMUM PERMITTED NUMBER OF ITERATIONS             *
C                     (PASS A DUMMY IF SYSTEM IS NOT PENTA OR NONA     *
C                     DIAGONAL BLOCKED)                                *
C                                                                      *
C                                                                      *
C      B,DEL(N)     MATRIX AND VEKTOR                                  *
C                                                                      *
C                                                                      *
C     OUTPUT DATA:                                                     *
C                                                                      *
C      IFAIL        RETURN CODE                                        *
C                     IFAIL < 1 : SUCESSFULL COMPLETION                *
C                     IFAIL > 0 : FAILURE                              *
C                     SEE SUBROUTINES FOR FURTHER INFORMATION          *
C                                                                      *
C      B            MATRIX IN DECOMPOSED FORM                          *
C                     (MAY BE USED FOR FURTHER CALLS WITH JOB=1)       *
C                                                                      *
C      DEL(N)       SOLUTION VECTOR                                    *
C                                                                      *
C     WORKING ARRAYS:                                                  *
C                                                                      *
C      W1(N),W2(N)  REAL WORK SPACE FOR ITERATIVE METHODS              *
C                     (PASS DUMMIES IF MATRIX IS NOT PENTA OR          *
C                     NONA DIAGONAL BLOCKED)                           *
C                                                                      *
C      IPIVOT(N)    INTEGER WORK SPACE FOR DENSE OR BANDED MATRIX      *
C                     (PASS DUMMIES IF MATRIX IS NOT BANDED OR         *
C                     DENSE)                                           *
C***********************************************************************
C                                                                      *
C     ENTRIES IN IMAT:                                                 *
C                                                                      *
C       IMAT(1): KIND OF MATRIX                                        *
C                                                                      *
C                  IMAT(1)=0  DENSE MATRIX                             *
C                  IMAT(1)=1  BANDED MATRIX                            *
C                  IMAT(1)=2  BLOCK TRIDIAGONAL MATRIX                 *
C                  IMAT(1)=3  BLOCK PENTADIAGONAL MATRIX               *
C                  IMAT(1)=4  BLOCK NONADIAGONAL MATRIX                *
C                                                                      *
C       IMAT(2): DIMENSION OF THE MATRIX                               *
C                                                                      *
C       IMAT(3-6) INFORMATIONS ABOUT SIZE OF MATRIX                    *
C       IF IMAT(1)=0 IMAT(3-6) NOT NEEDED                              *
C       IF IMAT(1)=1 IMAT(3) = LOWER BAND WIDTH                        *
C                    IMAT(4) = UPPER BAND WIDTH                        *
C                    IMAT(5) = LEEDING DIMENSION OF MATRIX (>=2ML+MU+1)*
C                    IMAT(6) = NOT NEEDED                              *
C       IF IMAT(1)=2 IMAT(3) = DIMENSION OF SUBBLOCKS                  *
C                    IMAT(4) = NUMBER OF SUBBLOCKS                     *
C                    IMAT(5) = NOT NEEDED                              *
C                    IMAT(6) = NOT NEEDED                              *
C       IF IMAT(1)=3 IMAT(3) = DIMENSION OF SUBSUBBLOCK                *
C                    IMAT(4) = NUMBER OF SUBSUBBLOCKS                  *
C                    IMAT(5) = NUMBER OF SUBBLOCKS                     *
C                    IMAT(6) = NOT NEEDED                              *
C       IF IMAT(1)=4 IMAT(3) = DIMENSION OF SUBSUBBLOCK                *
C                    IMAT(4) = NUMBER OF SUBSUBBLOCKS                  *
C                    IMAT(5) = NUMBER OF SUBBLOCKS                     *
C                    IMAT(6) = NOT NEEDED                              *
C***********************************************************************
C***********************************************************************
C     CHAPTER I.: STORAGE ORGANIZATION                                 *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION IMAT(10)
      DIMENSION DEL(N),W1(N),W2(N),IPIVOT(N)
      DIMENSION B(NDMAB)
C***********************************************************************
C     CHAPTER II.: INITIALIZE AND CHECK TASK
C***********************************************************************
C***********************************************************************
C                                                                      *
C     CHAPTER III.: PRINT OUT ALL NECESSARY INFORMATIONS               *
C                                                                      *
C***********************************************************************
      IF(JOB.EQ.1) THEN
      WRITE(NFIERR) (IMAT(I),I=1,10)
      WRITE(NFIERR) N,NDMAB
      WRITE(NFIERR) ATOL,RTOL,MAXIT,NDMAB
      WRITE(NFIERR) (B(I),I=1,NDMAB)
      WRITE(NFIERR) (W1(I),I=1,N)
      WRITE(NFIERR) (W2(I),I=1,N)
      WRITE(NFIERR) (DEL(I),I=1,N)
      ENDIF
C***********************************************************************
C     REGULAR EXIT                                                     *
C***********************************************************************
      RETURN
C***********************************************************************
C                                                                      *
C     END OF ERRLIN                                                    *
C                                                                      *
C***********************************************************************
      END
      SUBROUTINE ICCFAP(N,NQ,N1,NB,A,IFAIL)
C***********************************************************************
C                                                                      *
C     *************************************************************
C     *                                                           *
C     *          INCOMPLETE  CROUT FACTORIZATION                  *
C     *          OF A BLOCK-NONADIAGONAL MATRIX                   *
C     *                                                           *
C     *                      U.MAAS                               *
C     *                     05/21/1989                            *
C     *           LAST CHANGE: U.MAAS 05/21/1989                  *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C                                                                      *
C     INPUT:
C                                                                      *
C     N           DIMENSION OF SUB MATRIX                              *
C     NB          NUMBER OF BLOCKS                                     *
C     A(N,N,NB,3) SEE SUBROUTINE DGTFA                                 *
C     Z(N,1)                                                           *
C                                                                      *
C                                                                      C
C     OUTPUT:                                                          C
C                                                                      C
C     Z(N,1)  SOLUTION VECTOR                                          C
C                                                                      C
C***********************************************************************
C
C***********************************************************************
C      1 1           1 1 1                                             C
C      | |           | | |                                             C
C      P Q           U V W                                             C
C   2--O P Q           U V W                                           C
C        O P Q           U V W                                         C
C          O P Q           U V W                                       C
C            O P Q           U V W                                     C
C              O P Q           U V W                                   C
C                O P Q           U V W                                 C
C   1--C           O P Q           U V W                               C
C   1--B C           O P Q           U V W                             C
C   1--A B C           O P Q           U V W                           C
C        A B C           O P Q           U V W                         C
C          A B C           O P Q           U V W                       C
C            A B C           O P Q           U V W                     C
C              A B C           O P Q           U V W                   C
C                A B C           O P Q           U V W                 C
C                  A B C           O P Q           U V W               C
C                    A B C           O P Q           U V W             C
C                      A B C           O P Q           U V W           C
C                        A B C           O P Q           U V W - N-N1-1C
C                          A B C           O P Q           U V - N-N1  C
C                            A B C           O P Q           U - N-N1+1C
C                              A B C           O P Q                   C
C                                A B C           O P Q                 C
C                                  A B C           O P Q               C
C                                    A B C           O P Q             C
C                                      A B C           O P Q           C
C                                        A B C           O P Q---N-1   C
C                                          A B C           O P---N     C
C                                          | | |           |           C
C                                          | | N-N1+1      N           C
C                                          | N-N1                      C
C                                          N-N1-1                      C
C                                                                      C
C                                                                      C
C***********************************************************************
C     STORAGE ORGANIZATION                                             C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION A(NQ,*)
C
      DOUBLE PRECISION, DIMENSION(N) :: RWORK
      INTEGER, DIMENSION(N) :: IWORK
C***********************************************************************
C
C     INITIALIZE (NEW STORAGE FORM)
C
C***********************************************************************
      IFAIL=0
      NBA =  NB - N1 - 1
      NBB =  NB - N1
      NBC =  NB - N1 + 1
      NBO =  NB
      NBP =  NB
      NBQ =  NB
      NBU =  NB - N1 + 1
      NBV =  NB - N1
      NBW =  NB - N1 - 1
      INA = 0
      INB = INA + NBA
      INC = INB + NBB
      INO = INC + NBC
      INP = INO + NBO
      INQ = INP + NBP
      INU = INQ + NBQ
      INV = INU + NBU
      INW = INV + NBV
C---- ENTRIES IN OFF BLOCKS ----- NOT NEEDED
      NBD =  NB - N1 + 2
      NBN =  NB
      NBR =  NB
      NBT =  NB - N1 + 2
      IND = INW + NBW
      INN = IND + NBD
      INR = INN + NBN
      INT = INR + NBR
C***********************************************************************
C     ROW 1
C
C***********************************************************************
      K=1
C***********************************************************************
C     COMPUTE M(K,K)
C***********************************************************************
C---- UNCHANGED
C***********************************************************************
C
C***********************************************************************
C***********************************************************************
C     COMPUTE INVERSE OF M(K,K) AND STORE IN M(K,K)
C***********************************************************************
      IDUM=INO+1
C_rst USE DGEINV INSTEAD OF DGQINV -> NUMERICALLY MORE STABLE
C      CALL DGQINV(N,A(1,INP+K),A(1,IDUM),ILL)
      CALL DGEINV(N,A(1,INP+K),RWORK,IWORK,ILL)
      IF(ILL.NE.0) GOTO 910
C     IF(ILL.NE.0) WRITE(6,*) '   ERROR DURING DECOMP. IN ICCFA'
C_rst COPY OF MATRIX IS NEEDLESS
C      CALL DGQMEQ(N,A(1,IDUM),A(1,INP+K))
C***********************************************************************
C     COMPUTE U(K,K+1)
C***********************************************************************
C----
      CALL DGQMMM(N,A(1,INP+K     ),A(1,INQ+K     ),A(1,IDUM    ), 0)
      CALL DGQMEQ(N,A(1,IDUM),A(1,INQ+K))
C***********************************************************************
C     COMPUTE U(K,K+N-1)
C***********************************************************************
C----
      CALL DGQMMM(N,A(1,INP+K     ),A(1,INU+K     ),A(1,IDUM    ), 0)
      CALL DGQMEQ(N,A(1,IDUM),A(1,INU+K))
C***********************************************************************
C     COMPUTE U(K,K+N)
C***********************************************************************
C----
      CALL DGQMMM(N,A(1,INP+K     ),A(1,INV+K     ),A(1,IDUM    ), 0)
      CALL DGQMEQ(N,A(1,IDUM),A(1,INV+K))
C***********************************************************************
C     COMPUTE U(K,K+N+1)
C***********************************************************************
C----
      CALL DGQMMM(N,A(1,INP+K     ),A(1,INW+K     ),A(1,IDUM    ), 0)
      CALL DGQMEQ(N,A(1,IDUM),A(1,INW+K))
C***********************************************************************
C     ROWS 2,...,N1-1
C
C***********************************************************************
      N1M=N1-1
      DO 1000 K=2,N1M
C***********************************************************************
C     COMPUTE O
C***********************************************************************
C---- UNCHANGED
C***********************************************************************
C     COMPUTE M(K,K)
C***********************************************************************
C---- AT FIRST M(KK)=A(K,K)
      CALL DGQMMM(N,A(1,INO+K     ),A(1,INQ+K-1   ),A(1,INP+K),-1)
C***********************************************************************
C
C***********************************************************************
C***********************************************************************
C     COMPUTE INVERSE OF M(K,K) AND STORE IN M(K,K)
C***********************************************************************
      IDUM=INO+1
C_rst USE DGEINV INSTEAD OF DGQINV -> NUMERICALLY MORE STABLE
C      CALL DGQINV(N,A(1,INP+K),A(1,IDUM),ILL)
      CALL DGEINV(N,A(1,INP+K),RWORK,IWORK,ILL)
C     IF(ILL.NE.0) WRITE(6,*) '   ERROR DURING DECOMP. IN ICCFA'
      IF(ILL.NE.0) GOTO 920
C_rst COPY OF MATRIX IS NEEDLESS
C      CALL DGQMEQ(N,A(1,IDUM),A(1,INP+K))
C***********************************************************************
C     COMPUTE U(K,K+1)
C***********************************************************************
C----
      CALL DGQMMM(N,A(1,INP+K     ),A(1,INQ+K     ),A(1,IDUM    ), 0)
      CALL DGQMEQ(N,A(1,IDUM),A(1,INQ+K))
C***********************************************************************
C     COMPUTE U(K,K+N-1)
C***********************************************************************
C----
      CALL DGQMMM(N,A(1,INO+K     ),A(1,INV+K-1   ),A(1,INU+K   ),-1)
      CALL DGQMMM(N,A(1,INP+K     ),A(1,INU+K     ),A(1,IDUM    ), 0)
      CALL DGQMEQ(N,A(1,IDUM),A(1,INU+K))
C***********************************************************************
C     COMPUTE U(K,K+N)
C***********************************************************************
C----
      CALL DGQMMM(N,A(1,INO+K     ),A(1,INW+K-1   ),A(1,INV+K   ),-1)
      CALL DGQMMM(N,A(1,INP+K     ),A(1,INV+K     ),A(1,IDUM    ), 0)
      CALL DGQMEQ(N,A(1,IDUM),A(1,INV+K))
C***********************************************************************
C     COMPUTE U(K,K+N+1)
C***********************************************************************
C----
      CALL DGQMMM(N,A(1,INP+K     ),A(1,INW+K     ),A(1,IDUM    ), 0)
      CALL DGQMEQ(N,A(1,IDUM),A(1,INW+K))
 1000 CONTINUE
C***********************************************************************
C     ROW N1
C
C***********************************************************************
      K=N1
C***********************************************************************
C     COMPUTE C
C***********************************************************************
C---- UNCHANGED
C***********************************************************************
C     COMPUTE O
C***********************************************************************
C---- UNCHANGED
C***********************************************************************
C     COMPUTE M(K,K)
C***********************************************************************
C---- AT FIRST M(KK)=A(K,K)
      CALL DGQMMM(N,A(1,INC+K-N1+1),A(1,INU+K-N1+1),A(1,INP+K),-1)
      CALL DGQMMM(N,A(1,INO+K     ),A(1,INQ+K-1   ),A(1,INP+K),-1)
C***********************************************************************
C
C***********************************************************************
C***********************************************************************
C     COMPUTE INVERSE OF M(K,K) AND STORE IN M(K,K)
C***********************************************************************
      IDUM=INO+1
C_rst USE DGEINV INSTEAD OF DGQINV -> NUMERICALLY MORE STABLE
C      CALL DGQINV(N,A(1,INP+K),A(1,IDUM),ILL)
      CALL DGEINV(N,A(1,INP+K),RWORK,IWORK,ILL)
C     IF(ILL.NE.0) WRITE(6,*) '   ERROR DURING DECOMP. IN ICCFA'
      IF(ILL.NE.0) GOTO 930
C_rst COPY OF MATRIX IS NEEDLESS
C      CALL DGQMEQ(N,A(1,IDUM),A(1,INP+K))
C***********************************************************************
C     COMPUTE U(K,K+1)
C***********************************************************************
C----
      CALL DGQMMM(N,A(1,INC+K-N1+1),A(1,INV+K-N1+1),A(1,INQ+K   ),-1)
      CALL DGQMMM(N,A(1,INP+K     ),A(1,INQ+K     ),A(1,IDUM    ), 0)
      CALL DGQMEQ(N,A(1,IDUM),A(1,INQ+K))
C***********************************************************************
C     COMPUTE U(K,K+N-1)
C***********************************************************************
C----
      CALL DGQMMM(N,A(1,INO+K     ),A(1,INV+K-1   ),A(1,INU+K   ),-1)
      CALL DGQMMM(N,A(1,INP+K     ),A(1,INU+K     ),A(1,IDUM    ), 0)
      CALL DGQMEQ(N,A(1,IDUM),A(1,INU+K))
C***********************************************************************
C     COMPUTE U(K,K+N)
C***********************************************************************
C----
      CALL DGQMMM(N,A(1,INO+K     ),A(1,INW+K-1   ),A(1,INV+K   ),-1)
      CALL DGQMMM(N,A(1,INP+K     ),A(1,INV+K     ),A(1,IDUM    ), 0)
      CALL DGQMEQ(N,A(1,IDUM),A(1,INV+K))
C***********************************************************************
C     COMPUTE U(K,K+N+1)
C***********************************************************************
C----
      CALL DGQMMM(N,A(1,INP+K     ),A(1,INW+K     ),A(1,IDUM    ), 0)
      CALL DGQMEQ(N,A(1,IDUM),A(1,INW+K))
C***********************************************************************
C     ROW N1+ 1
C
C***********************************************************************
      K=N1+1
C***********************************************************************
C     COMPUTE A
C***********************************************************************
C---- ENTRY A
C***********************************************************************
C     COMPUTE B
C***********************************************************************
C---- UNCHANGED
C***********************************************************************
C     COMPUTE C
C***********************************************************************
C---- AT FIRST M(K+N1-1,K)=A(K+N1-1,K)
      CALL DGQMMM(N,A(1,INB+K-N1),A(1,INQ+K-N1   ),A(1,INC+K-N1+1 ),-1)
C***********************************************************************
C     COMPUTE O
C***********************************************************************
C---- AT FIRST M(K+1,K)=A(K+1,,K)
      CALL DGQMMM(N,A(1,INB+K-N1  ),A(1,INU+K-N1  ),A(1,INO+K   ),-1)
C***********************************************************************
C     COMPUTE M(K,K)
C***********************************************************************
C---- AT FIRST M(KK)=A(K,K)
      CALL DGQMMM(N,A(1,INB+K-N1  ),A(1,INV+K-N1  ),A(1,INP+K),-1)
      CALL DGQMMM(N,A(1,INC+K-N1+1),A(1,INU+K-N1+1),A(1,INP+K),-1)
      CALL DGQMMM(N,A(1,INO+K     ),A(1,INQ+K-1   ),A(1,INP+K),-1)
C***********************************************************************
C
C***********************************************************************
C***********************************************************************
C     COMPUTE INVERSE OF M(K,K) AND STORE IN M(K,K)
C***********************************************************************
      IDUM=INO+1
C_rst USE DGEINV INSTEAD OF DGQINV -> NUMERICALLY MORE STABLE
C      CALL DGQINV(N,A(1,INP+K),A(1,IDUM),ILL)
      CALL DGEINV(N,A(1,INP+K),RWORK,IWORK,ILL)
C     IF(ILL.NE.0) WRITE(6,*) '   ERROR DURING DECOMP. IN ICCFA'
      IF(ILL.NE.0) GOTO 940
C_rst COPY OF MATRIX IS NEEDLESS
C      CALL DGQMEQ(N,A(1,IDUM),A(1,INP+K))
C***********************************************************************
C     COMPUTE U(K,K+1)
C***********************************************************************
C----
      CALL DGQMMM(N,A(1,INB+K-N1  ),A(1,INW+K-N1  ),A(1,INQ+K   ),-1)
      CALL DGQMMM(N,A(1,INC+K-N1+1),A(1,INV+K-N1+1),A(1,INQ+K   ),-1)
      CALL DGQMMM(N,A(1,INP+K     ),A(1,INQ+K     ),A(1,IDUM    ), 0)
      CALL DGQMEQ(N,A(1,IDUM),A(1,INQ+K))
C***********************************************************************
C     COMPUTE U(K,K+N-1)
C***********************************************************************
C----
      CALL DGQMMM(N,A(1,INO+K     ),A(1,INV+K-1   ),A(1,INU+K   ),-1)
      CALL DGQMMM(N,A(1,INP+K     ),A(1,INU+K     ),A(1,IDUM    ), 0)
      CALL DGQMEQ(N,A(1,IDUM),A(1,INU+K))
C***********************************************************************
C     COMPUTE U(K,K+N)
C***********************************************************************
C----
      CALL DGQMMM(N,A(1,INO+K     ),A(1,INW+K-1   ),A(1,INV+K   ),-1)
      CALL DGQMMM(N,A(1,INP+K     ),A(1,INV+K     ),A(1,IDUM    ), 0)
      CALL DGQMEQ(N,A(1,IDUM),A(1,INV+K))
C***********************************************************************
C     COMPUTE U(K,K+N+1)
C***********************************************************************
C----
      CALL DGQMMM(N,A(1,INP+K     ),A(1,INW+K     ),A(1,IDUM    ), 0)
      CALL DGQMEQ(N,A(1,IDUM),A(1,INW+K))
C***********************************************************************
C
C     LOOP FOR ROW N1+2,....,NB
C
C***********************************************************************
      ILA1=N1+2
      ILA2=NB-N1-1
      DO 2000 K=ILA1,ILA2
C***********************************************************************
C     COMPUTE A
C***********************************************************************
C---- ENTRY A
C***********************************************************************
C     COMPUTE B
C***********************************************************************
C---- AT FIRST M(K+N1,K)=A(K+N1,K)
      CALL DGQMMM(N,A(1,INA+K-N1-1  ),A(1,INQ+K-N1-1 ),A(1,INB+K-N1),-1)
C***********************************************************************
C     COMPUTE C
C***********************************************************************
C---- AT FIRST M(K+N1-1,K)=A(K+N1-1,K)
      CALL DGQMMM(N,A(1,INB+K-N1),A(1,INQ+K-N1   ),A(1,INC+K-N1+1 ),-1)
C***********************************************************************
C     COMPUTE O
C***********************************************************************
C---- AT FIRST M(K+1,K)=A(K+1,,K)
      CALL DGQMMM(N,A(1,INA+K-N1-1),A(1,INV+K-N1-1),A(1,INO+K   ),-1)
      CALL DGQMMM(N,A(1,INB+K-N1  ),A(1,INU+K-N1  ),A(1,INO+K   ),-1)
C***********************************************************************
C     COMPUTE M(K,K)
C***********************************************************************
C---- AT FIRST M(KK)=A(K,K)
      CALL DGQMMM(N,A(1,INA+K-N1-1),A(1,INW+K-N1-1),A(1,INP+K),-1)
      CALL DGQMMM(N,A(1,INB+K-N1  ),A(1,INV+K-N1  ),A(1,INP+K),-1)
      CALL DGQMMM(N,A(1,INC+K-N1+1),A(1,INU+K-N1+1),A(1,INP+K),-1)
      CALL DGQMMM(N,A(1,INO+K     ),A(1,INQ+K-1   ),A(1,INP+K),-1)
C***********************************************************************
C
C***********************************************************************
C***********************************************************************
C     COMPUTE INVERSE OF M(K,K) AND STORE IN M(K,K)
C***********************************************************************
      IDUM=INO+1
C_rst USE DGEINV INSTEAD OF DGQINV -> NUMERICALLY MORE STABLE
C      CALL DGQINV(N,A(1,INP+K),A(1,IDUM),ILL)
      CALL DGEINV(N,A(1,INP+K),RWORK,IWORK,ILL)
C     IF(ILL.NE.0) WRITE(6,*) '   ERROR DURING DECOMP. IN ICCFA'
      IF(ILL.NE.0) GOTO 950
C_rst COPY OF MATRIX IS NEEDLESS
C      CALL DGQMEQ(N,A(1,IDUM),A(1,INP+K))
C***********************************************************************
C     COMPUTE U(K,K+1)
C***********************************************************************
C----
      CALL DGQMMM(N,A(1,INB+K-N1  ),A(1,INW+K-N1  ),A(1,INQ+K   ),-1)
      CALL DGQMMM(N,A(1,INC+K-N1+1),A(1,INV+K-N1+1),A(1,INQ+K   ),-1)
      CALL DGQMMM(N,A(1,INP+K     ),A(1,INQ+K     ),A(1,IDUM    ), 0)
      CALL DGQMEQ(N,A(1,IDUM),A(1,INQ+K))
C***********************************************************************
C     COMPUTE U(K,K+N-1)
C***********************************************************************
C----
      CALL DGQMMM(N,A(1,INO+K     ),A(1,INV+K-1   ),A(1,INU+K   ),-1)
      CALL DGQMMM(N,A(1,INP+K     ),A(1,INU+K     ),A(1,IDUM    ), 0)
      CALL DGQMEQ(N,A(1,IDUM),A(1,INU+K))
C***********************************************************************
C     COMPUTE U(K,K+N)
C***********************************************************************
C----
      CALL DGQMMM(N,A(1,INO+K     ),A(1,INW+K-1   ),A(1,INV+K   ),-1)
      CALL DGQMMM(N,A(1,INP+K     ),A(1,INV+K     ),A(1,IDUM    ), 0)
      CALL DGQMEQ(N,A(1,IDUM),A(1,INV+K))
C***********************************************************************
C     COMPUTE U(K,K+N+1)
C***********************************************************************
C----
      CALL DGQMMM(N,A(1,INP+K     ),A(1,INW+K     ),A(1,IDUM    ), 0)
      CALL DGQMEQ(N,A(1,IDUM),A(1,INW+K))
 2000 CONTINUE
C***********************************************************************
C
C     ROW NB-N1
C
C***********************************************************************
      K=NB-N1
C***********************************************************************
C     COMPUTE A
C***********************************************************************
C---- ENTRY A
C***********************************************************************
C     COMPUTE B
C***********************************************************************
C---- AT FIRST M(K+N1,K)=A(K+N1,K)
      CALL DGQMMM(N,A(1,INA+K-N1-1  ),A(1,INQ+K-N1-1 ),A(1,INB+K-N1),-1)
C***********************************************************************
C     COMPUTE C
C***********************************************************************
C---- AT FIRST M(K+N1-1,K)=A(K+N1-1,K)
      CALL DGQMMM(N,A(1,INB+K-N1),A(1,INQ+K-N1   ),A(1,INC+K-N1+1 ),-1)
C***********************************************************************
C     COMPUTE O
C***********************************************************************
C---- AT FIRST M(K+1,K)=A(K+1,,K)
      CALL DGQMMM(N,A(1,INA+K-N1-1),A(1,INV+K-N1-1),A(1,INO+K   ),-1)
      CALL DGQMMM(N,A(1,INB+K-N1  ),A(1,INU+K-N1  ),A(1,INO+K   ),-1)
C***********************************************************************
C     COMPUTE M(K,K)
C***********************************************************************
C---- AT FIRST M(KK)=A(K,K)
      CALL DGQMMM(N,A(1,INA+K-N1-1),A(1,INW+K-N1-1),A(1,INP+K),-1)
      CALL DGQMMM(N,A(1,INB+K-N1  ),A(1,INV+K-N1  ),A(1,INP+K),-1)
      CALL DGQMMM(N,A(1,INC+K-N1+1),A(1,INU+K-N1+1),A(1,INP+K),-1)
      CALL DGQMMM(N,A(1,INO+K     ),A(1,INQ+K-1   ),A(1,INP+K),-1)
C***********************************************************************
C
C***********************************************************************
C***********************************************************************
C     COMPUTE INVERSE OF M(K,K) AND STORE IN M(K,K)
C***********************************************************************
      IDUM=INO+1
C_rst USE DGEINV INSTEAD OF DGQINV -> NUMERICALLY MORE STABLE
C      CALL DGQINV(N,A(1,INP+K),A(1,IDUM),ILL)
      CALL DGEINV(N,A(1,INP+K),RWORK,IWORK,ILL)
C     IF(ILL.NE.0) WRITE(6,*) '   ERROR DURING DECOMP. IN ICCFA'
      IF(ILL.NE.0) GOTO 960
C_rst COPY OF MATRIX IS NEEDLESS
C      CALL DGQMEQ(N,A(1,IDUM),A(1,INP+K))
C***********************************************************************
C     COMPUTE U(K,K+1)
C***********************************************************************
C----
      CALL DGQMMM(N,A(1,INB+K-N1  ),A(1,INW+K-N1  ),A(1,INQ+K   ),-1)
      CALL DGQMMM(N,A(1,INC+K-N1+1),A(1,INV+K-N1+1),A(1,INQ+K   ),-1)
      CALL DGQMMM(N,A(1,INP+K     ),A(1,INQ+K     ),A(1,IDUM    ), 0)
      CALL DGQMEQ(N,A(1,IDUM),A(1,INQ+K))
C***********************************************************************
C     COMPUTE U(K,K+N-1)
C***********************************************************************
C----
      CALL DGQMMM(N,A(1,INO+K     ),A(1,INV+K-1   ),A(1,INU+K   ),-1)
      CALL DGQMMM(N,A(1,INP+K     ),A(1,INU+K     ),A(1,IDUM    ), 0)
      CALL DGQMEQ(N,A(1,IDUM),A(1,INU+K))
C***********************************************************************
C     COMPUTE U(K,K+N)
C***********************************************************************
C----
      CALL DGQMMM(N,A(1,INO+K     ),A(1,INW+K-1   ),A(1,INV+K   ),-1)
      CALL DGQMMM(N,A(1,INP+K     ),A(1,INV+K     ),A(1,IDUM    ), 0)
      CALL DGQMEQ(N,A(1,IDUM),A(1,INV+K))
C***********************************************************************
C
C     ROW NB-N1+1
C
C***********************************************************************
      K=NB-N1+1
C***********************************************************************
C     COMPUTE A
C***********************************************************************
C---- ENTRY A
C***********************************************************************
C     COMPUTE B
C***********************************************************************
C---- AT FIRST M(K+N1,K)=A(K+N1,K)
      CALL DGQMMM(N,A(1,INA+K-N1-1  ),A(1,INQ+K-N1-1 ),A(1,INB+K-N1),-1)
C***********************************************************************
C     COMPUTE C
C***********************************************************************
C---- AT FIRST M(K+N1-1,K)=A(K+N1-1,K)
      CALL DGQMMM(N,A(1,INB+K-N1),A(1,INQ+K-N1   ),A(1,INC+K-N1+1 ),-1)
C***********************************************************************
C     COMPUTE O
C***********************************************************************
C---- AT FIRST M(K+1,K)=A(K+1,,K)
      CALL DGQMMM(N,A(1,INA+K-N1-1),A(1,INV+K-N1-1),A(1,INO+K   ),-1)
      CALL DGQMMM(N,A(1,INB+K-N1  ),A(1,INU+K-N1  ),A(1,INO+K   ),-1)
C***********************************************************************
C     COMPUTE M(K,K)
C***********************************************************************
C---- AT FIRST M(KK)=A(K,K)
      CALL DGQMMM(N,A(1,INA+K-N1-1),A(1,INW+K-N1-1),A(1,INP+K),-1)
      CALL DGQMMM(N,A(1,INB+K-N1  ),A(1,INV+K-N1  ),A(1,INP+K),-1)
      CALL DGQMMM(N,A(1,INC+K-N1+1),A(1,INU+K-N1+1),A(1,INP+K),-1)
      CALL DGQMMM(N,A(1,INO+K     ),A(1,INQ+K-1   ),A(1,INP+K),-1)
C***********************************************************************
C
C***********************************************************************
C***********************************************************************
C     COMPUTE INVERSE OF M(K,K) AND STORE IN M(K,K)
C***********************************************************************
      IDUM=INO+1
C_rst USE DGEINV INSTEAD OF DGQINV -> NUMERICALLY MORE STABLE
C      CALL DGQINV(N,A(1,INP+K),A(1,IDUM),ILL)
      CALL DGEINV(N,A(1,INP+K),RWORK,IWORK,ILL)
C     IF(ILL.NE.0) WRITE(6,*) '   ERROR DURING DECOMP. IN ICCFA'
      IF(ILL.NE.0) GOTO 970
C_rst COPY OF MATRIX IS NEEDLESS
C      CALL DGQMEQ(N,A(1,IDUM),A(1,INP+K))
C***********************************************************************
C     COMPUTE U(K,K+1)
C***********************************************************************
C----
      CALL DGQMMM(N,A(1,INB+K-N1  ),A(1,INW+K-N1  ),A(1,INQ+K   ),-1)
      CALL DGQMMM(N,A(1,INC+K-N1+1),A(1,INV+K-N1+1),A(1,INQ+K   ),-1)
      CALL DGQMMM(N,A(1,INP+K     ),A(1,INQ+K     ),A(1,IDUM    ), 0)
      CALL DGQMEQ(N,A(1,IDUM),A(1,INQ+K))
C***********************************************************************
C     COMPUTE U(K,K+N-1)
C***********************************************************************
C----
      CALL DGQMMM(N,A(1,INO+K     ),A(1,INV+K-1   ),A(1,INU+K   ),-1)
      CALL DGQMMM(N,A(1,INP+K     ),A(1,INU+K     ),A(1,IDUM    ), 0)
      CALL DGQMEQ(N,A(1,IDUM),A(1,INU+K))
C***********************************************************************
C
C     ROWS NB-N1+2,...,NB-1
C
C***********************************************************************
      ILA1=NB-N1+2
      ILA2=NB-1
      DO 3000 K=ILA1,ILA2
C***********************************************************************
C     COMPUTE A
C***********************************************************************
C---- ENTRY A
C***********************************************************************
C     COMPUTE B
C***********************************************************************
C---- AT FIRST M(K+N1,K)=A(K+N1,K)
      CALL DGQMMM(N,A(1,INA+K-N1-1  ),A(1,INQ+K-N1-1 ),A(1,INB+K-N1),-1)
C***********************************************************************
C     COMPUTE C
C***********************************************************************
C---- AT FIRST M(K+N1-1,K)=A(K+N1-1,K)
      CALL DGQMMM(N,A(1,INB+K-N1),A(1,INQ+K-N1   ),A(1,INC+K-N1+1 ),-1)
C***********************************************************************
C     COMPUTE O
C***********************************************************************
C---- AT FIRST M(K+1,K)=A(K+1,,K)
      CALL DGQMMM(N,A(1,INA+K-N1-1),A(1,INV+K-N1-1),A(1,INO+K   ),-1)
      CALL DGQMMM(N,A(1,INB+K-N1  ),A(1,INU+K-N1  ),A(1,INO+K   ),-1)
C***********************************************************************
C     COMPUTE M(K,K)
C***********************************************************************
C---- AT FIRST M(KK)=A(K,K)
      CALL DGQMMM(N,A(1,INA+K-N1-1),A(1,INW+K-N1-1),A(1,INP+K),-1)
      CALL DGQMMM(N,A(1,INB+K-N1  ),A(1,INV+K-N1  ),A(1,INP+K),-1)
      CALL DGQMMM(N,A(1,INC+K-N1+1),A(1,INU+K-N1+1),A(1,INP+K),-1)
      CALL DGQMMM(N,A(1,INO+K     ),A(1,INQ+K-1   ),A(1,INP+K),-1)
C***********************************************************************
C
C***********************************************************************
C***********************************************************************
C     COMPUTE INVERSE OF M(K,K) AND STORE IN M(K,K)
C***********************************************************************
      IDUM=INO+1
C_rst USE DGEINV INSTEAD OF DGQINV -> NUMERICALLY MORE STABLE
C      CALL DGQINV(N,A(1,INP+K),A(1,IDUM),ILL)
      CALL DGEINV(N,A(1,INP+K),RWORK,IWORK,ILL)
C     IF(ILL.NE.0) WRITE(6,*) '   ERROR DURING DECOMP. IN ICCFA'
      IF(ILL.NE.0) GOTO 980
C_rst COPY OF MATRIX IS NEEDLESS
C      CALL DGQMEQ(N,A(1,IDUM),A(1,INP+K))
C***********************************************************************
C     COMPUTE U(K,K+1)
C***********************************************************************
C----
      CALL DGQMMM(N,A(1,INB+K-N1  ),A(1,INW+K-N1  ),A(1,INQ+K   ),-1)
      CALL DGQMMM(N,A(1,INC+K-N1+1),A(1,INV+K-N1+1),A(1,INQ+K   ),-1)
      CALL DGQMMM(N,A(1,INP+K     ),A(1,INQ+K     ),A(1,IDUM    ), 0)
      CALL DGQMEQ(N,A(1,IDUM),A(1,INQ+K))
C***********************************************************************
C     COMPUTE U(K,K+N-1)
C***********************************************************************
 3000 CONTINUE
C***********************************************************************
C
C     ROW  NB
C
C***********************************************************************
      K=NB
C***********************************************************************
C     COMPUTE A
C***********************************************************************
C---- ENTRY A
C***********************************************************************
C     COMPUTE B
C***********************************************************************
C---- AT FIRST M(K+N1,K)=A(K+N1,K)
      CALL DGQMMM(N,A(1,INA+K-N1-1  ),A(1,INQ+K-N1-1 ),A(1,INB+K-N1),-1)
C***********************************************************************
C     COMPUTE C
C***********************************************************************
C---- AT FIRST M(K+N1-1,K)=A(K+N1-1,K)
      CALL DGQMMM(N,A(1,INB+K-N1),A(1,INQ+K-N1   ),A(1,INC+K-N1+1 ),-1)
C***********************************************************************
C     COMPUTE O
C***********************************************************************
C---- AT FIRST M(K+1,K)=A(K+1,,K)
      CALL DGQMMM(N,A(1,INA+K-N1-1),A(1,INV+K-N1-1),A(1,INO+K   ),-1)
      CALL DGQMMM(N,A(1,INB+K-N1  ),A(1,INU+K-N1  ),A(1,INO+K   ),-1)
C***********************************************************************
C     COMPUTE M(K,K)
C***********************************************************************
C---- AT FIRST M(KK)=A(K,K)
      CALL DGQMMM(N,A(1,INA+K-N1-1),A(1,INW+K-N1-1),A(1,INP+K),-1)
      CALL DGQMMM(N,A(1,INB+K-N1  ),A(1,INV+K-N1  ),A(1,INP+K),-1)
      CALL DGQMMM(N,A(1,INC+K-N1+1),A(1,INU+K-N1+1),A(1,INP+K),-1)
      CALL DGQMMM(N,A(1,INO+K     ),A(1,INQ+K-1   ),A(1,INP+K),-1)
C***********************************************************************
C
C***********************************************************************
C***********************************************************************
C     COMPUTE INVERSE OF M(K,K) AND STORE IN M(K,K)
C***********************************************************************
      IDUM=INO+1
C_rst USE DGEINV INSTEAD OF DGQINV -> NUMERICALLY MORE STABLE
C      CALL DGQINV(N,A(1,INP+K),A(1,IDUM),ILL)
      CALL DGEINV(N,A(1,INP+K),RWORK,IWORK,ILL)
      IF(ILL.NE.0) GOTO 990
C_rst COPY OF MATRIX IS NEEDLESS
C      CALL DGQMEQ(N,A(1,IDUM),A(1,INP+K))
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
  910 CONTINUE
      IFAIL=1
      GOTO 999
  920 CONTINUE
      IFAIL=2
      GOTO 999
  930 CONTINUE
      IFAIL=3
      GOTO 999
  940 CONTINUE
      IFAIL=4
      GOTO 999
  950 CONTINUE
      IFAIL=5
      GOTO 999
  960 CONTINUE
      IFAIL=6
      GOTO 999
  970 CONTINUE
      IFAIL=7
      GOTO 999
  980 CONTINUE
      IFAIL=8
      GOTO 999
  990 CONTINUE
      IFAIL=9
      GOTO 999
  999 CONTINUE
      RETURN
C***********************************************************************
C     END OF XXXXX
C***********************************************************************
      END
      SUBROUTINE NLUSL(NV,N,N1,NB,A,Z,X,Y,RTOL,ATOL,MAXIT,INFO,IFAIL)
C***********************************************************************
C                                                                      *
C     *************************************************************
C     *                                                           *
C     *   SOLUTION OF OF A BLOCK-NONADIAGONAL EQUATION SYSTEM     *
C     *                      U.MAAS                               *
C     *                     5/28/1989                             *
C     *           LAST CHANGE: U.MAAS 5/28/1989                   *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C                                                                      *
C     INPUT:
C                                                                      *
C     N           DIMENSION OF SUB MATRIX                              *
C     NB          NUMBER OF BLOCKS                                     *
C     A           MATRIX                                               *
C     Z(N,1)                                                           *
C     X(N,1) SOLUTION VECTOR                                           *
C     Y(N,1) WORK ARRAY                                                C
C     OUTPUT:                                                          C
C                                                                      C
C     Z(N,1)  SOLUTION VECTOR                                          C
C                                                                      C
C***********************************************************************
C
C                                                                      C
C***********************************************************************
C     STORAGE ORGANIZATION                                             C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION A(N,N,*),Z(N,NB),X(N,NB),Y(N,NB)
C***********************************************************************
C
C     INITIALIZE (NEW STORAGE FORM)
C
C***********************************************************************
      NBA =  NB - N1 - 1
      NBB =  NB - N1
      NBC =  NB - N1 + 1
      NBO =  NB
      NBP =  NB
      NBQ =  NB
      NBU =  NB - N1 + 1
      NBV =  NB - N1
      NBW =  NB - N1 - 1
      INA = 0
      INB = INA + NBA
      INC = INB + NBB
      INO = INC + NBC
      INP = INO + NBO
      INQ = INP + NBP
      INU = INQ + NBQ
      INV = INU + NBU
      INW = INV + NBV
C***********************************************************************
C
C***********************************************************************
      NQ=N*N
      IDUM=INO+1
C***********************************************************************
C     SET INITIAL GUESS
C***********************************************************************
      IF(IABS(INFO).NE.2.AND.IABS(INFO).NE.4) THEN
        DO 10 J=1,N
        DO 10 I=1,NB
          X(J,I)=0.D0
   10   CONTINUE
      ENDIF
C***********************************************************************
C     DO INCOMPLETE FACTORIZATION, IF NOT YET DONE\
C***********************************************************************
      IF(INFO.LE.2.AND.INFO.GE.-2) THEN
         CALL ICCFAP(N,NQ,N1,NB,A,ILL)
         IF(ILL.GT.0) GOTO 9200
      ENDIF
C***********************************************************************
C
C     ITERATION LOOP
C
C***********************************************************************
      NITER=0
    1 CONTINUE
      NITER=NITER+1
      IF(NITER.GT.MAXIT) THEN
         WRITE (6,841) NITER, MAXIT 
 841     FORMAT ('NLUSL: NITER = ',I4,' > MAXIT = ',I4)
         GOTO 950
      ENDIF
C***********************************************************************
C     CALCULATE RIGHT HAND SIDE                                        *
C***********************************************************************
      DO 110 J=1,N
      DO 110 I=1,NB
      Y(J,I)=Z(J,I)
  110 CONTINUE
      DO 20 K=1,NB
      IF(K.GT.N1+1) THEN
C     CALL DGQMMM(N,A(1,1,INA+K-N1-1),A(1,1,INU+K-N1-1),A(1,1,IDUM), 0)
C     CALL DGQMMV(N,A(1,1,IDUM),X(1,K-2),Y(1,K),+1)
      CALL DGQMMV(N,A(1,1,INU+K-N1-1),X(1,K-2),A(1,1,IDUM), 0)
      CALL DGQMMV(N,A(1,1,INA+K-N1-1),A(1,1,IDUM),Y(1,K),+1)
      ENDIF
      IF(K.GT.N1-1.AND.K.LE.NB-2) THEN
C     CALL DGQMMM(N,A(1,1,INC+K-N1+1),A(1,1,INW+K-N1+1),A(1,1,IDUM), 0)
C     CALL DGQMMV(N,A(1,1,IDUM),X(1,K+2),Y(1,K),+1)
      CALL DGQMMV(N,A(1,1,INW+K-N1+1),X(1,K+2),A(1,1,IDUM), 0)
      CALL DGQMMV(N,A(1,1,INC+K-N1+1),A(1,1,IDUM),Y(1,K),+1)
      ENDIF
      IF(K.GT.N1-1) THEN
C     CALL DGQMMM(N,A(1,1,INC+K-N1+1),A(1,1,INQ+K-N1+1),A(1,1,IDUM), 0)
C     CALL DGQMMV(N,A(1,1,IDUM),X(1,K-N1+2),Y(1,K),+1)
      CALL DGQMMV(N,A(1,1,INQ+K-N1+1),X(1,K-N1+2),A(1,1,IDUM), 0)
      CALL DGQMMV(N,A(1,1,INC+K-N1+1),A(1,1,IDUM),Y(1,K),+1)
      ENDIF
      IF(K.GT.1.AND.K.LE.NB-N1+2) THEN
C     CALL DGQMMM(N,A(1,1,INO+K     ),A(1,1,INU+K-1   ),A(1,1,IDUM), 0)
C     CALL DGQMMV(N,A(1,1,IDUM),X(1,K+N1-2),Y(1,K),+1)
      CALL DGQMMV(N,A(1,1,INU+K-1   ),X(1,K+N1-2),A(1,1,IDUM), 0)
      CALL DGQMMV(N,A(1,1,INO+K     ),A(1,1,IDUM),Y(1,K),+1)
      ENDIF
   20 CONTINUE
C***********************************************************************
C
C     CALCULATE  L X'        = Z                                       C
C
C***********************************************************************
C***********************************************************************
C     CALCULATE X(1)                                                   *
C***********************************************************************
      I=1
C---- STORE Z IN FIRST BLOCK OF O-ROW (THIS IS A DUMMY BLOCK)
      DO 315 J=1,N
      A(J,1,IDUM)=Y(J,I)
  315 CONTINUE
      CALL DGQMMV(N,A(1,1,INP+I      ),A(1,1,IDUM),Y(1,I),0)
C***********************************************************************
C     CALCULATE X(2),...,X(N1-1)       (CONSIDERING O)                 *
C***********************************************************************
      N1MI1=N1-1
      DO 320 I=2,N1MI1
      DO 325 J=1,N
      A(J,1,IDUM)=Y(J,I)
  325 CONTINUE
      CALL DGQMMV(N,A(1,1,INO+I      ),Y(1,I-1   ),A(1,1,IDUM),-1)
      CALL DGQMMV(N,A(1,1,INP+I      ),A(1,1,IDUM),Y(1,I),0)
  320 CONTINUE
C***********************************************************************
C     CALCULATE X(N1)                  (CONSIDERING O,C)               *
C***********************************************************************
      I=N1
C---- STORE Y IN FIRST BLOCK OF O-ROW (THIS IS A DUMMY BLOCK)
      IDUM=INO+1
      DO 330 J=1,N
      A(J,1,IDUM)=Y(J,I)
  330 CONTINUE
      CALL DGQMMV(N,A(1,1,INO+I      ),Y(1,I-1   ),A(1,1,IDUM),-1)
      CALL DGQMMV(N,A(1,1,INC+I-N1+1 ),Y(1,I-N1+1),A(1,1,IDUM),-1)
      CALL DGQMMV(N,A(1,1,INP+I      ),A(1,1,IDUM),Y(1,I),0)
C***********************************************************************
C     CALCULATE X(N1+1)                (CONSIDERING O,C,B)             *
C***********************************************************************
      I=N1+1
C---- STORE Y IN FIRST BLOCK OF O-ROW (THIS IS A DUMMY BLOCK)
      IDUM=INO+1
      DO 335 J=1,N
      A(J,1,IDUM)=Y(J,I)
  335 CONTINUE
      CALL DGQMMV(N,A(1,1,INO+I      ),Y(1,I-1   ),A(1,1,IDUM),-1)
      CALL DGQMMV(N,A(1,1,INC+I-N1+1 ),Y(1,I-N1+1),A(1,1,IDUM),-1)
      CALL DGQMMV(N,A(1,1,INB+I-N1   ),Y(1,I-N1  ),A(1,1,IDUM),-1)
      CALL DGQMMV(N,A(1,1,INP+I      ),A(1,1,IDUM),Y(1,I),0)
C***********************************************************************
C     CALCULATE X(N1+2),...,X(NB)      (CONSIDERING O,C,B,A)           *
C***********************************************************************
      N1PLU2=N1+2
      DO 350 I=N1PLU2,NB
C---- STORE Z IN FIRST BLOCK OF O-ROW (THIS IS A DUMMY BLOCK)
      IDUM=INO+1
      DO 340 J=1,N
      A(J,1,IDUM)=Y(J,I)
  340 CONTINUE
      CALL DGQMMV(N,A(1,1,INO+I      ),Y(1,I-1   ),A(1,1,IDUM),-1)
      CALL DGQMMV(N,A(1,1,INC+I-N1+1 ),Y(1,I-N1+1),A(1,1,IDUM),-1)
      CALL DGQMMV(N,A(1,1,INB+I-N1   ),Y(1,I-N1  ),A(1,1,IDUM),-1)
      CALL DGQMMV(N,A(1,1,INA+I-N1-1 ),Y(1,I-N1-1),A(1,1,IDUM),-1)
      CALL DGQMMV(N,A(1,1,INP+I      ),A(1,1,IDUM),Y(1,I),0)
  350 CONTINUE
C***********************************************************************
C     CALCULATE  U * X = X'
C***********************************************************************
C***********************************************************************
C     CALCULATE X(NB)                                                  *
C***********************************************************************
C---- X(NB) REMAINS UNCHANGED
C***********************************************************************
C     CALCULATE X(NB-N1+1)                                             *
C***********************************************************************
C---- BLOCK WITH CU: UWES VERSION
CU    NBM1= NB-1
CU    N1M2= N1-2
CU    DO 230 IXXX=1,N1M2
CU    I=NB-IXXX
CU    CALL DGQMMV(N,A(1,1,INQ+I ),Y(1,I+1   ),Y(1,I),-1)
CU230 CONTINUE
CXXXXXX
C     DO 230 I=NB-1,NB-N1+2,-1
      I=NB
      ILA1=NB-N1+2
      ILA2=NB-1
      DO 230 III=ILA1,ILA2
      I=I-1
CXXXXXX
      CALL DGQMMV(N,A(1,1,INQ+I ),Y(1,I+1   ),Y(1,I),-1)
  230 CONTINUE
      IF(I.NE.NB-N1+2) WRITE(6,*) ' ERROR 1 '
C***********************************************************************
C     CALCULATE X(NB-N1+1)                                             *
C***********************************************************************
      I=NB-N1+1
      CALL DGQMMV(N,A(1,1,INQ+I ),Y(1,I+1   ),Y(1,I),-1)
      CALL DGQMMV(N,A(1,1,INU+I ),Y(1,I+N1-1),Y(1,I),-1)
C***********************************************************************
C     CALCULATE X(NB-N1)                                               *
C***********************************************************************
      I=NB-N1
      CALL DGQMMV(N,A(1,1,INQ+I ),Y(1,I+1   ),Y(1,I),-1)
      CALL DGQMMV(N,A(1,1,INU+I ),Y(1,I+N1-1),Y(1,I),-1)
      CALL DGQMMV(N,A(1,1,INV+I ),Y(1,I+N1  ),Y(1,I),-1)
C***********************************************************************
C     CALCULATE X(NB-N1-1),...,X(1)                                    *
C***********************************************************************
C---- CU MEANS UWES VERSION
CU    NXNX= NB-N1-1
CU    DO 235 IXXX=1,NXNX
CU    I = NB-N1-IXXX
CXXXXXXXXXXXXX
C     DO 235 I=NB-N1-1,1,-1
      I=NB-N1
      ILA2=NB-N1-1
      DO 235 III=1,ILA2
      I=I-1
CXXXXXXXXXXXXX
      CALL DGQMMV(N,A(1,1,INQ+I ),Y(1,I+1   ),Y(1,I),-1)
      CALL DGQMMV(N,A(1,1,INU+I ),Y(1,I+N1-1),Y(1,I),-1)
      CALL DGQMMV(N,A(1,1,INV+I ),Y(1,I+N1  ),Y(1,I),-1)
      CALL DGQMMV(N,A(1,1,INW+I ),Y(1,I+N1+1),Y(1,I),-1)
  235 CONTINUE
      IF(I.NE.1) WRITE(6,*) ' ERROR 2 '
C
      ABSMAX=0.D0
      ABSEUK=0.D0
      DO 680 K=1,NB
      DO 680 I=1,N
      D=Y(I,K)-X(I,K)
      X(I,K)=Y(I,K)
      ABSEUK=ABSEUK+D*D
      IF(DABS(D).GT.ABSMAX) THEN
      KKKMAX = (K-1) /N1  + 1
      LEFT   = K-(KKKMAX-1)*N1
      LLLMAX = LEFT
      IIIMAX = I
      ABSMAX = DMAX1(ABSMAX,DABS(D))
      ENDIF
 680  CONTINUE
C---- CALCULATE OVERALL ERROR
      ABSEUK=DSQRT(ABSEUK/FLOAT(N*NB))
      WRITE(6,842) ABSMAX,IIIMAX,LLLMAX,KKKMAX,ABSEUK
 842  FORMAT(1X,' MAX. ERR.: ',1PE9.2,1X,'(',I2,',',I2,',',I2,')',4X,
     1       1X,' EUK. ERR.: ',1PE9.2)
      IF(NITER.EQ.1) THEN
        ABSEU0=ABSEUK
        ABSMA0=ABSMAX
      ENDIF
C---- ABEND IF SOLUTION SEEMS TO DIVERGE
      IF(ABSMAX.GT.1.D3*ABSMA0) GOTO 950
C_rst IF(ABSMAX.GT.1.D3*ABSMA0.OR.ABSMAX.GT.1.D6) GOTO 950
CUWE  IF(ABSMAX.GT.1.D3*ABSMA0.AND.ABSMAX.GT.1.D3*ATOL) GOTO 950
C---- ACCEPT SOLUTION IF ERRORS/INITIAL.ERRORS ARE SMALLER THEN RTOL
      IF((ABSMAX/ABSMA0).LT.RTOL.AND.(ABSEUK/ABSEU0).LT.RTOL) GOTO 600
C---- ACCEPT SOLUTION IF ERRORS ARE SMALLER THEN ATOL
      IF(ABSMAX.LT.ATOL.AND.ABSEUK.LT.ATOL.AND.NITER.GT.1)   GOTO 600
      GOTO 1
C***********************************************************************
C     SOLUTION EXIT
C***********************************************************************
  600 CONTINUE
      IFAIL=-NITER
      DO 610 I=1,N
      DO 610 J=1,NB
      Z(I,J)=X(I,J)
  610 CONTINUE
      RETURN
C***********************************************************************
C     ERROR EXITS                                                     C
C***********************************************************************
C----- LESS THAN 3 SUBMATRICES
C9100 IFAIL= 9
C     RETURN
 9200 IFAIL= 1
      RETURN
  950 IFAIL= 2
      RETURN
C***********************************************************************
C     END OF XXXXX
C***********************************************************************
      END
      INTEGER FUNCTION INDMAX(N,DX,INCX)
C
C     FINDS THE INDEX OF ELEMENT HAVING MAX. VALUE.
C     BY U.MAAS (AFTER JACK DONGARRA, LINPACK, 3/11/78.)
C
      DOUBLE PRECISION DX(*),DMAX
      INTEGER I,INCX,IX,N
C
      INDMAX = 0
      IF( N .LT. 1 ) RETURN
      INDMAX = 1
      IF(N.EQ.1)RETURN
      IF(INCX.EQ.1)GO TO 20
C
C        CODE FOR INCREMENT NOT EQUAL TO 1
C
      IX = 1
      DMAX =  DX(1)
      IX = IX + INCX
      DO 10 I = 2,N
         IF( DX(IX).LE.DMAX) GO TO 5
         INDMAX = I
         DMAX = DX(IX)
    5    IX = IX + INCX
   10 CONTINUE
      RETURN
C
C        CODE FOR INCREMENT EQUAL TO 1
C
   20 DMAX = DX(1)
      DO 30 I = 2,N
         IF(DX(I).LE.DMAX) GO TO 30
         INDMAX = I
         DMAX = DX(I)
   30 CONTINUE
      RETURN
      END
      INTEGER FUNCTION INDMIN(N,DX,INCX)
C
C     FINDS THE INDEX OF ELEMENT HAVING MIN. VALUE.
C     BY U.MAAS (AFTER JACK DONGARRA, LINPACK, 3/11/78.)
C
      DOUBLE PRECISION DX(*),DMIN
      INTEGER I,INCX,IX,N
C
      INDMIN = 0
      IF( N .LT. 1 ) RETURN
      INDMIN = 1
      IF(N.EQ.1)RETURN
      IF(INCX.EQ.1)GO TO 20
C
C        CODE FOR INCREMENT NOT EQUAL TO 1
C
      IX = 1
      DMIN =  DX(1)
      IX = IX + INCX
      DO 10 I = 2,N
         IF( DX(IX).GE.DMIN) GO TO 5
         INDMIN = I
         DMIN = DX(IX)
    5    IX = IX + INCX
   10 CONTINUE
      RETURN
C
C        CODE FOR INCREMENT EQUAL TO 1
C
   20 DMIN = DX(1)
      DO 30 I = 2,N
         IF(DX(I).GE.DMIN) GO TO 30
         INDMIN = I
         DMIN = DX(I)
   30 CONTINUE
      RETURN
      END
      DOUBLE PRECISION FUNCTION DVAMAX(N,DX,INCX)
C
C     FINDS THE  ELEMENT HAVING MAX. VALUE.
C     BY U.MAAS (AFTER JACK DONGARRA, LINPACK, 3/11/78.)
C
      DOUBLE PRECISION DX(*)
      INTEGER I,INCX,IX,N
C
      IF( N .LT. 1 ) RETURN
      DVAMAX = DX(1)
      IF(N.EQ.1)RETURN
      IF(INCX.EQ.1)GO TO 20
C
C        CODE FOR INCREMENT NOT EQUAL TO 1
C
      IX = 1
      IX = IX + INCX
      DO 10 I = 2,N
         DVAMAX = DMAX1(DX(IX),DVAMAX)
    5    IX = IX + INCX
   10 CONTINUE
      RETURN
C
C        CODE FOR INCREMENT EQUAL TO 1
C
   20 DVAMAX = DX(1)
      DO 30 I = 2,N
         DVAMAX = DMAX1(DVAMAX,DX(I))
   30 CONTINUE
      RETURN
      END
      DOUBLE PRECISION FUNCTION DVAMIN(N,DX,INCX)
C
C     FINDS THE  ELEMENT HAVING MIN. VALUE.
C     BY U.MAAS (AFTER JACK DONGARRA, LINPACK, 3/11/78.)
C
      DOUBLE PRECISION DX(*)
      INTEGER I,INCX,IX,N
C
      IF( N .LT. 1 ) RETURN
      DVAMIN = DX(1)
      IF(N.EQ.1)RETURN
      IF(INCX.EQ.1)GO TO 20
C
C        CODE FOR INCREMENT NOT EQUAL TO 1
C
      IX = 1
      IX = IX + INCX
      DO 10 I = 2,N
         DVAMIN = DMIN1(DX(IX),DVAMIN)
    5    IX = IX + INCX
   10 CONTINUE
      RETURN
C
C        CODE FOR INCREMENT EQUAL TO 1
C
   20 CONTINUE
      DO 30 I = 2,N
         DVAMIN = DMIN1(DVAMIN,DX(I))
   30 CONTINUE
      RETURN
      END
      REAL FUNCTION VAMAX(N,DX,INCX)
C
C     FINDS THE  ELEMENT HAVING MAX. VALUE.
C     BY U.MAAS (AFTER JACK DONGARRA, LINPACK, 3/11/78.)
C
      REAL DX(1)
      INTEGER I,INCX,IX,N
C
      IF( N .LT. 1 ) RETURN
      VAMAX = DX(1)
      IF(N.EQ.1)RETURN
      IF(INCX.EQ.1)GO TO 20
C
C        CODE FOR INCREMENT NOT EQUAL TO 1
C
      IX = 1
      IX = IX + INCX
      DO 10 I = 2,N
         VAMAX = AMAX1(DX(IX),VAMAX)
    5    IX = IX + INCX
   10 CONTINUE
      RETURN
C
C        CODE FOR INCREMENT EQUAL TO 1
C
   20 VAMAX = DX(1)
      DO 30 I = 2,N
         VAMAX = AMAX1(VAMAX,DX(I))
   30 CONTINUE
      RETURN
      END
      REAL FUNCTION VAMIN(N,DX,INCX)
C
C     FINDS THE  ELEMENT HAVING MIN. VALUE.
C     BY U.MAAS (AFTER JACK DONGARRA, LINPACK, 3/11/78.)
C
      REAL DX(1)
      INTEGER I,INCX,IX,N
C
      IF( N .LT. 1 ) RETURN
      VAMIN = DX(1)
      IF(N.EQ.1)RETURN
      IF(INCX.EQ.1)GO TO 20
C
C        CODE FOR INCREMENT NOT EQUAL TO 1
C
      IX = 1
      IX = IX + INCX
      DO 10 I = 2,N
         VAMIN = AMIN1(DX(IX),VAMIN)
    5    IX = IX + INCX
   10 CONTINUE
      RETURN
C
C        CODE FOR INCREMENT EQUAL TO 1
C
   20 CONTINUE
      DO 30 I = 2,N
         VAMIN = AMIN1(VAMIN,DX(I))
   30 CONTINUE
      RETURN
      END
CENDOWN
C**********************************************************************
CLINPACKLINPACKLINPACKLINPACKLINPACKLINPACKLINPACKLINPACKLINPACKLINPACK
C**********************************************************************
      SUBROUTINE LINGL(N,A,IPI,X,IFAIL)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C     SOLUTION OF A SYSTEM OF LINEAR EQUATIONS, A*X = B, BY GAUSSIAN
C     ELIMINATION WITH PARTIAL PIVOTING.
      DOUBLE PRECISION A(N,N),X(N)
      INTEGER IPI(N)
C
      DATA  ZERO/0.D0/
C
C     TRIANGULAR DECOMPOSITION
      DO 30 K=1,N
      P=ZERO
      DO 40 J=K,N
      IF (DABS(A(J,K))-DABS(P)) 40,40,50
 50   P=A(J,K)
      I=J
 40   CONTINUE
C
      IF(DABS(P))60,60,70
C
C     FAILURE EXIT
 60   IFAIL=1
      RETURN
C
 70   R=X(I)
      IF (I-K) 80,90,80
 80   X(I)=X(K)
      DO 100 J=1,N
      Q=A(I,J)
      A(I,J)=A(K,J)
 100  A(K,J)=Q
 90   IPI(K)=I
      X(K)=R
      K1=K+1
      IF(K1-N) 105,105,30
 105  DO 110 I=K1,N
      Q=A(I,K)
      IF (Q.EQ.ZERO) GOTO 110
      Q=Q/P
      A(I,K)=Q
      X(I)=X(I)-Q*R
      IF(K1-N) 115,115,110
 115  DO 120 J=K1,N
 120  A(I,J)=A(I,J)-Q*A(K,J)
 110  CONTINUE
  30  CONTINUE
C
      K=N
 140  Q=X(K)
      IF (K.EQ.N) GOTO 125
      K1=K+1
      DO 130 I=K1,N
 130  Q=Q-A(K,I)*X(I)
 125  X(K)=Q/A(K,K)
      K=K-1
      IF (K) 150,150,140
 150  RETURN
      END
      SUBROUTINE SUBST (N,A,IPI,X)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C     SOLUTION OF A SYSTEM OF LINEAR EQUATIONS, A*X = B, BY GAUSSIAN
C     ELIMINATION WITH THE MATRIX IN DECOMPOSED FORM. THE LATTER,
C     E.G. IS PRODUCED BY A CALL OF PROCEDURE LINGL PROVIDED THE
C     EXIT IFAIL IS NOT USED
      DOUBLE PRECISION A(N,N),X(N)
      INTEGER IPI(N)
      DO 1 K=1,N
      I=IPI(K)
      J=K-1
      Q=X(I)
      IF(I.EQ.K) GOTO 10
      X(I)=X(K)
      X(K)=Q
10    IF(J.EQ.0) GOTO 1
      DO 11 I=1,J
11    Q=Q-A(K,I)*X(I)
1     X(K)=Q
      N1=N+1
      DO 2 KK=1,N
      K=N1-KK
      Q=X(K)
      IF(KK.EQ.1) GOTO 2
      K1=K+1
      DO 21 I=K1,N
21    Q=Q-A(K,I)*X(I)
2     X(K)=Q/A(K,K)
      RETURN
C***********************************************************************
C     END OF FUNCTION OVNORM
C***********************************************************************
      END
      DOUBLE PRECISION FUNCTION WVNORM(NEQ,V,WT)
C***********************************************************************
C                                                                      *
C     ************************************************************     *
C     *                                                          *     *
C     *              WEIGHTED NORM OF A VECTOR                   *     *
C     *                                                          *     *
C     ************************************************************     *
C                                                                      *
C***********************************************************************
C
C     INPUT:
C        NEQ      LENGHT OF VECTOR
C        V(NEQ)   VECTOR
C        WT(NEQ)  WEIGHTING FACTORS
C
C     OUTPUT:
C        WVNORM   SQRT( 1/NEQ * SUM( (V/WT)**2 ))
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION V(NEQ),WT(NEQ)
      VNORM = 0.0D0
      SUM = 0.0D0
      DO 20 I = 1,NEQ
20    SUM = SUM + (V(I)/WT(I))**2
      WVNORM = DSQRT(SUM/FLOAT(NEQ))
      RETURN
C***********************************************************************
C     END OF FUNCTION OVNORM
C***********************************************************************
      END
      DOUBLE PRECISION FUNCTION OVNORM(NEQ,V)
C***********************************************************************
C                                                                      *
C     ************************************************************     *
C     *                                                          *     *
C     *                    NORM OF A VECTOR                      *     *
C     *                                                          *     *
C     ************************************************************     *
C                                                                      *
C***********************************************************************
C
C     INPUT:
C        NEQ      LENGHT OF VECTOR
C        V(NEQ)   VECTOR
C
C     OUTPUT:
C        WVNORM   SQRT( 1/NEQ * SUM( V**2 ))
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION V(NEQ)
      VNORM = 0.0D0
      SUM = 0.0D0
      DO 20 I = 1,NEQ
20    SUM = SUM + V(I)**2
      OVNORM = DSQRT(SUM/FLOAT(NEQ))
      RETURN
C***********************************************************************
C     END OF FUNCTION OVNORM
C***********************************************************************
      END
      SUBROUTINE DGBFA(ABD,LDA,N,ML,MU,IPVT,INFO)
      INTEGER LDA,N,ML,MU,IPVT(*),INFO
      DOUBLE PRECISION ABD(LDA,*)
C
C     DGBFA FACTORS A DOUBLE PRECISION BAND MATRIX BY ELIMINATION.
C
C     DGBFA IS USUALLY CALLED BY DGBCO, BUT IT CAN BE CALLED
C     DIRECTLY WITH A SAVING IN TIME IF  RCOND  IS NOT NEEDED.
C
C     ON ENTRY
C
C        ABD     DOUBLE PRECISION(LDA, N)
C                CONTAINS THE MATRIX IN BAND STORAGE.  THE COLUMNS
C                OF THE MATRIX ARE STORED IN THE COLUMNS OF  ABD  AND
C                THE DIAGONALS OF THE MATRIX ARE STORED IN ROWS
C                ML+1 THROUGH 2*ML+MU+1 OF  ABD .
C                SEE THE COMMENTS BELOW FOR DETAILS.
C
C        LDA     INTEGER
C                THE LEADING DIMENSION OF THE ARRAY  ABD .
C                LDA MUST BE .GE. 2*ML + MU + 1 .
C
C        N       INTEGER
C                THE ORDER OF THE ORIGINAL MATRIX.
C
C        ML      INTEGER
C                NUMBER OF DIAGONALS BELOW THE MAIN DIAGONAL.
C                0 .LE. ML .LT. N .
C
C        MU      INTEGER
C                NUMBER OF DIAGONALS ABOVE THE MAIN DIAGONAL.
C                0 .LE. MU .LT. N .
C                MORE EFFICIENT IF  ML .LE. MU .
C     ON RETURN
C
C        ABD     AN UPPER TRIANGULAR MATRIX IN BAND STORAGE AND
C                THE MULTIPLIERS WHICH WERE USED TO OBTAIN IT.
C                THE FACTORIZATION CAN BE WRITTEN  A = L*U  WHERE
C                L  IS A PRODUCT OF PERMUTATION AND UNIT LOWER
C                TRIANGULAR MATRICES AND  U  IS UPPER TRIANGULAR.
C
C        IPVT    INTEGER(N)
C                AN INTEGER VECTOR OF PIVOT INDICES.
C
C        INFO    INTEGER
C                = 0  NORMAL VALUE.
C                = K  IF  U(K,K) .EQ. 0.0 .  THIS IS NOT AN ERROR
C                     CONDITION FOR THIS SUBROUTINE, BUT IT DOES
C                     INDICATE THAT DGBSL WILL DIVIDE BY ZERO IF
C                     CALLED.  USE  RCOND  IN DGBCO FOR A RELIABLE
C                     INDICATION OF SINGULARITY.
C
C     BAND STORAGE
C
C           IF  A  IS A BAND MATRIX, THE FOLLOWING PROGRAM SEGMENT
C           WILL SET UP THE INPUT.
C
C                   ML = (BAND WIDTH BELOW THE DIAGONAL)
C                   MU = (BAND WIDTH ABOVE THE DIAGONAL)
C                   M = ML + MU + 1
C                   DO 20 J = 1, N
C                      I1 = MAX0(1, J-MU)
C                      I2 = MIN0(N, J+ML)
C                      DO 10 I = I1, I2
C                         K = I - J + M
C                         ABD(K,J) = A(I,J)
C                10    CONTINUE
C                20 CONTINUE
C
C           THIS USES ROWS  ML+1  THROUGH  2*ML+MU+1  OF  ABD .
C           IN ADDITION, THE FIRST  ML  ROWS IN  ABD  ARE USED FOR
C           ELEMENTS GENERATED DURING THE TRIANGULARIZATION.
C           THE TOTAL NUMBER OF ROWS NEEDED IN  ABD  IS  2*ML+MU+1 .
C           THE  ML+MU BY ML+MU  UPPER LEFT TRIANGLE AND THE
C           ML BY ML  LOWER RIGHT TRIANGLE ARE NOT REFERENCED.
C
C     LINPACK. THIS VERSION DATED 08/14/78 .
C     CLEVE MOLER, UNIVERSITY OF NEW MEXICO, ARGONNE NATIONAL LAB.
C
C     SUBROUTINES AND FUNCTIONS
C
C     BLAS DAXPY,DSCAL,IDAMAX
C     FORTRAN MAX0,MIN0
C
C     INTERNAL VARIABLES
C
      DOUBLE PRECISION T
      INTEGER I,IDAMAX,I0,J,JU,JZ,J0,J1,K,KP1,L,LM,M,MM,NM1
C
C
      M = ML + MU + 1
      INFO = 0
C
C     ZERO INITIAL FILL-IN COLUMNS
C
      J0 = MU + 2
      J1 = MIN0(N,M) - 1
      IF (J1 .LT. J0) GO TO 30
      DO 20 JZ = J0, J1
         I0 = M + 1 - JZ
         DO 10 I = I0, ML
            ABD(I,JZ) = 0.0D0
   10    CONTINUE
   20 CONTINUE
   30 CONTINUE
      JZ = J1
      JU = 0
C
C     GAUSSIAN ELIMINATION WITH PARTIAL PIVOTING
C
      NM1 = N - 1
      IF (NM1 .LT. 1) GO TO 130
      DO 120 K = 1, NM1
         KP1 = K + 1
C
C        ZERO NEXT FILL-IN COLUMN
C
         JZ = JZ + 1
         IF (JZ .GT. N) GO TO 50
         IF (ML .LT. 1) GO TO 50
            DO 40 I = 1, ML
               ABD(I,JZ) = 0.0D0
   40       CONTINUE
   50    CONTINUE
C
C        FIND L = PIVOT INDEX
C
         LM = MIN0(ML,N-K)
         L = IDAMAX(LM+1,ABD(M,K),1) + M - 1
         IPVT(K) = L + K - M
C
C        ZERO PIVOT IMPLIES THIS COLUMN ALREADY TRIANGULARIZED
C
         IF (ABD(L,K) .EQ. 0.0D0) GO TO 100
C
C           INTERCHANGE IF NECESSARY
C
            IF (L .EQ. M) GO TO 60
               T = ABD(L,K)
               ABD(L,K) = ABD(M,K)
               ABD(M,K) = T
   60       CONTINUE
C
C           COMPUTE MULTIPLIERS
C
            T = -1.0D0/ABD(M,K)
            CALL DSCAL(LM,T,ABD(M+1,K),1)
C
C           ROW ELIMINATION WITH COLUMN INDEXING
C
            JU = MIN0(MAX0(JU,MU+IPVT(K)),N)
            MM = M
            IF (JU .LT. KP1) GO TO 90
            DO 80 J = KP1, JU
               L = L - 1
               MM = MM - 1
               T = ABD(L,J)
               IF (L .EQ. MM) GO TO 70
                  ABD(L,J) = ABD(MM,J)
                  ABD(MM,J) = T
   70          CONTINUE
               CALL DAXPY(LM,T,ABD(M+1,K),1,ABD(MM+1,J),1)
   80       CONTINUE
   90       CONTINUE
         GO TO 110
  100    CONTINUE
            INFO = K
  110    CONTINUE
  120 CONTINUE
  130 CONTINUE
      IPVT(N) = N
      IF (ABD(M,N) .EQ. 0.0D0) INFO = N
      RETURN
      END
      SUBROUTINE DGBSL(ABD,LDA,N,ML,MU,IPVT,B,JOB)
      INTEGER LDA,N,ML,MU,IPVT(*),JOB
      DOUBLE PRECISION ABD(LDA,*),B(*)
C
C     DGBSL SOLVES THE DOUBLE PRECISION BAND SYSTEM
C     A * X = B  OR  TRANS(A) * X = B
C     USING THE FACTORS COMPUTED BY DGBCO OR DGBFA.
C
C     ON ENTRY
C
C        ABD     DOUBLE PRECISION(LDA, N)
C                THE OUTPUT FROM DGBCO OR DGBFA.
C
C        LDA     INTEGER
C                THE LEADING DIMENSION OF THE ARRAY  ABD .
C
C        N       INTEGER
C                THE ORDER OF THE ORIGINAL MATRIX.
C
C        ML      INTEGER
C                NUMBER OF DIAGONALS BELOW THE MAIN DIAGONAL.
C
C        MU      INTEGER
C                NUMBER OF DIAGONALS ABOVE THE MAIN DIAGONAL.
C
C        IPVT    INTEGER(N)
C                THE PIVOT VECTOR FROM DGBCO OR DGBFA.
C
C        B       DOUBLE PRECISION(N)
C                THE RIGHT HAND SIDE VECTOR.
C
C        JOB     INTEGER
C                = 0         TO SOLVE  A*X = B ,
C                = NONZERO   TO SOLVE  TRANS(A)*X = B , WHERE
C                            TRANS(A)  IS THE TRANSPOSE.
C
C     ON RETURN
C
C        B       THE SOLUTION VECTOR  X .
C
C     ERROR CONDITION
C
C        A DIVISION BY ZERO WILL OCCUR IF THE INPUT FACTOR CONTAINS A
C        ZERO ON THE DIAGONAL.  TECHNICALLY THIS INDICATES SINGULARITY
C        BUT IT IS OFTEN CAUSED BY IMPROPER ARGUMENTS OR IMPROPER
C        SETTING OF LDA .  IT WILL NOT OCCUR IF THE SUBROUTINES ARE
C        CALLED CORRECTLY AND IF DGBCO HAS SET RCOND .GT. 0.0
C        OR DGBFA HAS SET INFO .EQ. 0 .
C
C     TO COMPUTE  INVERSE(A) * C  WHERE  C  IS A MATRIX
C     WITH  P  COLUMNS
C           CALL DGBCO(ABD,LDA,N,ML,MU,IPVT,RCOND,Z)
C           IF (RCOND IS TOO SMALL) GO TO ...
C           DO 10 J = 1, P
C              CALL DGBSL(ABD,LDA,N,ML,MU,IPVT,C(1,J),0)
C        10 CONTINUE
C
C     LINPACK. THIS VERSION DATED 08/14/78 .
C     CLEVE MOLER, UNIVERSITY OF NEW MEXICO, ARGONNE NATIONAL LAB.
C
C     SUBROUTINES AND FUNCTIONS
C
C     BLAS DAXPY,DDOT
C     FORTRAN MIN0
C
C     INTERNAL VARIABLES
C
      DOUBLE PRECISION DDOT,T
      INTEGER K,KB,L,LA,LB,LM,M,NM1
C
      M = MU + ML + 1
      NM1 = N - 1
      IF (JOB .NE. 0) GO TO 50
C
C        JOB = 0 , SOLVE  A * X = B
C        FIRST SOLVE L*Y = B
C
         IF (ML .EQ. 0) GO TO 30
         IF (NM1 .LT. 1) GO TO 30
            DO 20 K = 1, NM1
               LM = MIN0(ML,N-K)
               L = IPVT(K)
               T = B(L)
               IF (L .EQ. K) GO TO 10
                  B(L) = B(K)
                  B(K) = T
   10          CONTINUE
               CALL DAXPY(LM,T,ABD(M+1,K),1,B(K+1),1)
   20       CONTINUE
   30    CONTINUE
C
C        NOW SOLVE  U*X = Y
C
         DO 40 KB = 1, N
            K = N + 1 - KB
            B(K) = B(K)/ABD(M,K)
            LM = MIN0(K,M) - 1
            LA = M - LM
            LB = K - LM
            T = -B(K)
            CALL DAXPY(LM,T,ABD(LA,K),1,B(LB),1)
   40    CONTINUE
      GO TO 100
   50 CONTINUE
C
C        JOB = NONZERO, SOLVE  TRANS(A) * X = B
C        FIRST SOLVE  TRANS(U)*Y = B
C
         DO 60 K = 1, N
            LM = MIN0(K,M) - 1
            LA = M - LM
            LB = K - LM
            T = DDOT(LM,ABD(LA,K),1,B(LB),1)
            B(K) = (B(K) - T)/ABD(M,K)
   60    CONTINUE
C
C        NOW SOLVE TRANS(L)*X = Y
C
         IF (ML .EQ. 0) GO TO 90
         IF (NM1 .LT. 1) GO TO 90
            DO 80 KB = 1, NM1
               K = N - KB
               LM = MIN0(ML,N-K)
               B(K) = B(K) + DDOT(LM,ABD(M+1,K),1,B(K+1),1)
               L = IPVT(K)
               IF (L .EQ. K) GO TO 70
                  T = B(L)
                  B(L) = B(K)
                  B(K) = T
   70          CONTINUE
   80       CONTINUE
   90    CONTINUE
  100 CONTINUE
      RETURN
      END
      SUBROUTINE DGEFA(A,LDA,N,IPVT,INFO)
      INTEGER LDA,N,IPVT(*),INFO
      DOUBLE PRECISION A(LDA,*)
C
C     DGEFA FACTORS A DOUBLE PRECISION MATRIX BY GAUSSIAN ELIMINATION.
C
C     DGEFA IS USUALLY CALLED BY DGECO, BUT IT CAN BE CALLED
C     DIRECTLY WITH A SAVING IN TIME IF  RCOND  IS NOT NEEDED.
C     (TIME FOR DGECO) = (1 + 9/N)*(TIME FOR DGEFA) .
C
C     ON ENTRY
C
C        A       DOUBLE PRECISION(LDA, N)
C                THE MATRIX TO BE FACTORED.
C
C        LDA     INTEGER
C                THE LEADING DIMENSION OF THE ARRAY  A .
C
C        N       INTEGER
C                THE ORDER OF THE MATRIX  A .
C
C     ON RETURN
C
C        A       AN UPPER TRIANGULAR MATRIX AND THE MULTIPLIERS
C                WHICH WERE USED TO OBTAIN IT.
C                THE FACTORIZATION CAN BE WRITTEN  A = L*U  WHERE
C                L  IS A PRODUCT OF PERMUTATION AND UNIT LOWER
C                TRIANGULAR MATRICES AND  U  IS UPPER TRIANGULAR.
C
C        IPVT    INTEGER(N)
C                AN INTEGER VECTOR OF PIVOT INDICES.
C
C        INFO    INTEGER
C                = 0  NORMAL VALUE.
C                = K  IF  U(K,K) .EQ. 0.0 .  THIS IS NOT AN ERROR
C                     CONDITION FOR THIS SUBROUTINE, BUT IT DOES
C                     INDICATE THAT DGESL OR DGEDI WILL DIVIDE BY ZERO
C                     IF CALLED.  USE  RCOND  IN DGECO FOR A RELIABLE
C                     INDICATION OF SINGULARITY.
C
C     LINPACK. THIS VERSION DATED 08/14/78 .
C     CLEVE MOLER, UNIVERSITY OF NEW MEXICO, ARGONNE NATIONAL LAB.
C
C     SUBROUTINES AND FUNCTIONS
C
C     BLAS DAXPY,DSCAL,IDAMAX
C
C     INTERNAL VARIABLES
C
      DOUBLE PRECISION T
      INTEGER IDAMAX,J,K,KP1,L,NM1
C
C
C     GAUSSIAN ELIMINATION WITH PARTIAL PIVOTING
C
      INFO = 0
      NM1 = N - 1
      IF (NM1 .LT. 1) GO TO 70
      DO 60 K = 1, NM1
         KP1 = K + 1
C
C        FIND L = PIVOT INDEX
C
         L = IDAMAX(N-K+1,A(K,K),1) + K - 1
         IPVT(K) = L
C
C        ZERO PIVOT IMPLIES THIS COLUMN ALREADY TRIANGULARIZED
C
         IF (A(L,K) .EQ. 0.0D0) GO TO 40
C
C           INTERCHANGE IF NECESSARY
C
            IF (L .EQ. K) GO TO 10
               T = A(L,K)
               A(L,K) = A(K,K)
               A(K,K) = T
   10       CONTINUE
C
C           COMPUTE MULTIPLIERS
C
            T = -1.0D0/A(K,K)
            CALL DSCAL(N-K,T,A(K+1,K),1)
C
C           ROW ELIMINATION WITH COLUMN INDEXING
C
            DO 30 J = KP1, N
               T = A(L,J)
               IF (L .EQ. K) GO TO 20
                  A(L,J) = A(K,J)
                  A(K,J) = T
   20          CONTINUE
               CALL DAXPY(N-K,T,A(K+1,K),1,A(K+1,J),1)
   30       CONTINUE
         GO TO 50
   40    CONTINUE
            INFO = K
   50    CONTINUE
   60 CONTINUE
   70 CONTINUE
      IPVT(N) = N
      IF (A(N,N) .EQ. 0.0D0) INFO = N
      RETURN
      END
      subroutine dgeco(a,lda,n,ipvt,rcond,z)
      integer lda,n,ipvt(*)
      double precision a(lda,*),z(*)
      double precision rcond
c
c     dgeco factors a double precision matrix by gaussian elimination
c     and estimates the condition of the matrix.
c
c     if  rcond  is not needed, dgefa is slightly faster.
c     to solve  a*x = b , follow dgeco by dgesl.
c     to compute  inverse(a)*c , follow dgeco by dgesl.
c     to compute  determinant(a) , follow dgeco by dgedi.
c     to compute  inverse(a) , follow dgeco by dgedi.
c
c     on entry
c
c        a       double precision(lda, n)
c                the matrix to be factored.
c
c        lda     integer
c                the leading dimension of the array  a .
c
c        n       integer
c                the order of the matrix  a .
c
c     on return
c
c        a       an upper triangular matrix and the multipliers
c                which were used to obtain it.
c                the factorization can be written  a = l*u  where
c                l  is a product of permutation and unit lower
c                triangular matrices and  u  is upper triangular.
c
c        ipvt    integer(n)
c                an integer vector of pivot indices.
c
c        rcond   double precision
c                an estimate of the reciprocal condition of  a .
c                for the system  a*x = b , relative perturbations
c                in  a  and  b  of size  epsilon  may cause
c                relative perturbations in  x  of size  epsilon/rcond .
c                if  rcond  is so small that the logical expression
c                           1.0 + rcond .eq. 1.0
c                is true, then  a  may be singular to working
c                precision.  in particular,  rcond  is zero  if
c                exact singularity is detected or the estimate
c                underflows.
c
c        z       double precision(n)
c                a work vector whose contents are usually unimportant.
c                if  a  is close to a singular matrix, then  z  is
c                an approximate null vector in the sense that
c                norm(a*z) = rcond*norm(a)*norm(z) .
c
c     linpack. this version dated 08/14/78 .
c     cleve moler, university of new mexico, argonne national lab.
c
c     subroutines and functions
c
c     linpack dgefa
c     blas daxpy,ddot,dscal,dasum
c     fortran dabs,dmax1,dsign
c
c     internal variables
c
      double precision ddot,ek,t,wk,wkm
      double precision anorm,s,dasum,sm,ynorm
      integer info,j,k,kb,kp1,l
c
c
c     compute 1-norm of a
c
      anorm = 0.0d0
      do 10 j = 1, n
         anorm = dmax1(anorm,dasum(n,a(1,j),1))
   10 continue
c
c     factor
c
      call dgefa(a,lda,n,ipvt,info)
c
c     rcond = 1/(norm(a)*(estimate of norm(inverse(a)))) .
c     estimate = norm(z)/norm(y) where  a*z = y  and  trans(a)*y = e .
c     trans(a)  is the transpose of a .  the components of  e  are
c     chosen to cause maximum local growth in the elements of w  where
c     trans(u)*w = e .  the vectors are frequently rescaled to avoid
c     overflow.
c
c     solve trans(u)*w = e
c
      ek = 1.0d0
      do 20 j = 1, n
         z(j) = 0.0d0
   20 continue
      do 100 k = 1, n
         if (z(k) .ne. 0.0d0) ek = dsign(ek,-z(k))
         if (dabs(ek-z(k)) .le. dabs(a(k,k))) go to 30
            s = dabs(a(k,k))/dabs(ek-z(k))
            call dscal(n,s,z,1)
            ek = s*ek
   30    continue
         wk = ek - z(k)
         wkm = -ek - z(k)
         s = dabs(wk)
         sm = dabs(wkm)
         if (a(k,k) .eq. 0.0d0) go to 40
            wk = wk/a(k,k)
            wkm = wkm/a(k,k)
         go to 50
   40    continue
            wk = 1.0d0
            wkm = 1.0d0
   50    continue
         kp1 = k + 1
         if (kp1 .gt. n) go to 90
            do 60 j = kp1, n
               sm = sm + dabs(z(j)+wkm*a(k,j))
               z(j) = z(j) + wk*a(k,j)
               s = s + dabs(z(j))
   60       continue
            if (s .ge. sm) go to 80
               t = wkm - wk
               wk = wkm
               do 70 j = kp1, n
                  z(j) = z(j) + t*a(k,j)
   70          continue
   80       continue
   90    continue
         z(k) = wk
  100 continue
      s = 1.0d0/dasum(n,z,1)
      call dscal(n,s,z,1)
c
c     solve trans(l)*y = w
c
      do 120 kb = 1, n
         k = n + 1 - kb
         if (k .lt. n) z(k) = z(k) + ddot(n-k,a(k+1,k),1,z(k+1),1)
         if (dabs(z(k)) .le. 1.0d0) go to 110
            s = 1.0d0/dabs(z(k))
            call dscal(n,s,z,1)
  110    continue
         l = ipvt(k)
         t = z(l)
         z(l) = z(k)
         z(k) = t
  120 continue
      s = 1.0d0/dasum(n,z,1)
      call dscal(n,s,z,1)
c
      ynorm = 1.0d0
c
c     solve l*v = y
c
      do 140 k = 1, n
         l = ipvt(k)
         t = z(l)
         z(l) = z(k)
         z(k) = t
         if (k .lt. n) call daxpy(n-k,t,a(k+1,k),1,z(k+1),1)
         if (dabs(z(k)) .le. 1.0d0) go to 130
            s = 1.0d0/dabs(z(k))
            call dscal(n,s,z,1)
            ynorm = s*ynorm
  130    continue
  140 continue
      s = 1.0d0/dasum(n,z,1)
      call dscal(n,s,z,1)
      ynorm = s*ynorm
c
c     solve  u*z = v
c
      do 160 kb = 1, n
         k = n + 1 - kb
         if (dabs(z(k)) .le. dabs(a(k,k))) go to 150
            s = dabs(a(k,k))/dabs(z(k))
            call dscal(n,s,z,1)
            ynorm = s*ynorm
  150    continue
         if (a(k,k) .ne. 0.0d0) z(k) = z(k)/a(k,k)
         if (a(k,k) .eq. 0.0d0) z(k) = 1.0d0
         t = -z(k)
         call daxpy(k-1,t,a(1,k),1,z(1),1)
  160 continue
c     make znorm = 1.0
      s = 1.0d0/dasum(n,z,1)
      call dscal(n,s,z,1)
      ynorm = s*ynorm
c
      if (anorm .ne. 0.0d0) rcond = ynorm/anorm
      if (anorm .eq. 0.0d0) rcond = 0.0d0
      return
      end
      SUBROUTINE DGESL(A,LDA,N,IPVT,B,JOB)
      INTEGER LDA,N,IPVT(*),JOB
      DOUBLE PRECISION A(LDA,*),B(*)
C
C     DGESL SOLVES THE DOUBLE PRECISION SYSTEM
C     A * X = B  OR  TRANS(A) * X = B
C     USING THE FACTORS COMPUTED BY DGECO OR DGEFA.
C
C     ON ENTRY
C
C        A       DOUBLE PRECISION(LDA, N)
C                THE OUTPUT FROM DGECO OR DGEFA.
C
C        LDA     INTEGER
C                THE LEADING DIMENSION OF THE ARRAY  A .
C
C        N       INTEGER
C                THE ORDER OF THE MATRIX  A .
C
C        IPVT    INTEGER(N)
C                THE PIVOT VECTOR FROM DGECO OR DGEFA.
C
C        B       DOUBLE PRECISION(N)
C                THE RIGHT HAND SIDE VECTOR.
C
C        JOB     INTEGER
C                = 0         TO SOLVE  A*X = B ,
C                = NONZERO   TO SOLVE  TRANS(A)*X = B  WHERE
C                            TRANS(A)  IS THE TRANSPOSE.
C
C     ON RETURN
C
C        B       THE SOLUTION VECTOR  X .
C
C     ERROR CONDITION
C
C        A DIVISION BY ZERO WILL OCCUR IF THE INPUT FACTOR CONTAINS A
C        ZERO ON THE DIAGONAL.  TECHNICALLY THIS INDICATES SINGULARITY
C        BUT IT IS OFTEN CAUSED BY IMPROPER ARGUMENTS OR IMPROPER
C        SETTING OF LDA .  IT WILL NOT OCCUR IF THE SUBROUTINES ARE
C        CALLED CORRECTLY AND IF DGECO HAS SET RCOND .GT. 0.0
C        OR DGEFA HAS SET INFO .EQ. 0 .
C
C     TO COMPUTE  INVERSE(A) * C  WHERE  C  IS A MATRIX
C     WITH  P  COLUMNS
C           CALL DGECO(A,LDA,N,IPVT,RCOND,Z)
C           IF (RCOND IS TOO SMALL) GO TO ...
C           DO 10 J = 1, P
C              CALL DGESL(A,LDA,N,IPVT,C(1,J),0)
C        10 CONTINUE
C
C     LINPACK. THIS VERSION DATED 08/14/78 .
C     CLEVE MOLER, UNIVERSITY OF NEW MEXICO, ARGONNE NATIONAL LAB.
C
C     SUBROUTINES AND FUNCTIONS
C
C     BLAS DAXPY,DDOT
C
C     INTERNAL VARIABLES
C
      DOUBLE PRECISION DDOT,T
      INTEGER K,KB,L,NM1
C
      NM1 = N - 1
      IF (JOB .NE. 0) GO TO 50
C
C        JOB = 0 , SOLVE  A * X = B
C        FIRST SOLVE  L*Y = B
C
         IF (NM1 .LT. 1) GO TO 30
         DO 20 K = 1, NM1
            L = IPVT(K)
            T = B(L)
            IF (L .EQ. K) GO TO 10
               B(L) = B(K)
               B(K) = T
   10       CONTINUE
            CALL DAXPY(N-K,T,A(K+1,K),1,B(K+1),1)
   20    CONTINUE
   30    CONTINUE
C
C        NOW SOLVE  U*X = Y
C
         DO 40 KB = 1, N
            K = N + 1 - KB
            B(K) = B(K)/A(K,K)
            T = -B(K)
            CALL DAXPY(K-1,T,A(1,K),1,B(1),1)
   40    CONTINUE
      GO TO 100
   50 CONTINUE
C
C        JOB = NONZERO, SOLVE  TRANS(A) * X = B
C        FIRST SOLVE  TRANS(U)*Y = B
C
         DO 60 K = 1, N
            T = DDOT(K-1,A(1,K),1,B(1),1)
            B(K) = (B(K) - T)/A(K,K)
   60    CONTINUE
C
C        NOW SOLVE TRANS(L)*X = Y
C
         IF (NM1 .LT. 1) GO TO 90
         DO 80 KB = 1, NM1
            K = N - KB
            B(K) = B(K) + DDOT(N-K,A(K+1,K),1,B(K+1),1)
            L = IPVT(K)
            IF (L .EQ. K) GO TO 70
               T = B(L)
               B(L) = B(K)
               B(K) = T
   70       CONTINUE
   80    CONTINUE
   90    CONTINUE
  100 CONTINUE
      RETURN
      END
      SUBROUTINE DGEDI(A,LDA,N,IPVT,DET,WORK,JOB)
      INTEGER LDA,N,IPVT(*),JOB
      DOUBLE PRECISION A(LDA,*),DET(2),WORK(*)
C
C     dgedi computes the determinant and inverse of a matrix
C     using the factors computed by dgeco or dgefa.
C
C     on entry
C
C        a       double precision(lda, n)
C                the output from dgeco or dgefa.
C
C        lda     integer
C                the leading dimension of the array  a .
C
C        n       integer
C                the order of the matrix  a .
C
C        ipvt    integer(n)
C                the pivot vector from dgeco or dgefa.
C
C        work    double precision(n)
C                work vector.  contents destroyed.
C
C        job     integer
C                = 11   both determinant and inverse.
C                = 01   inverse only.
C                = 10   determinant only.
C
C     on return
C
C        a       inverse of original matrix if requested.
C                otherwise unchanged.
C
C        det     double precision(2)
C                determinant of original matrix if requested.
C                otherwise not referenced.
C                determinant = det(1) * 10.0**det(2)
C                with  1.0 .le. dabs(det(1)) .lt. 10.0
C                or  det(1) .eq. 0.0 .
C
C     error condition
C
C        a division by zero will occur if the input factor contains
C        a zero on the diagonal and the inverse is requested.
C        it will not occur if the subroutines are called correctly
C        and if dgeco has set rcond .gt. 0.0 or dgefa has set
C        info .eq. 0 .
C
C     linpack. this version dated 08/14/78 .
C     cleve moler, university of new mexico, argonne national lab.
C
C     subroutines and functions
C
C     blas daxpy,dscal,dswap
C     fortran dabs,mod
C
C     internal variables
C
      DOUBLE PRECISION T
      DOUBLE PRECISION TEN
      INTEGER I,J,K,KB,KP1,L,NM1
C
C
C     compute determinant
C
      IF (JOB/10 .EQ. 0) GO TO 70
         DET(1) = 1.0D0
         DET(2) = 0.0D0
         TEN = 10.0D0
         DO 50 I = 1, N
            IF (IPVT(I) .NE. I) DET(1) = -DET(1)
            DET(1) = A(I,I)*DET(1)
C        ...exit
            IF (DET(1) .EQ. 0.0D0) GO TO 60
   10       IF (DABS(DET(1)) .GE. 1.0D0) GO TO 20
               DET(1) = TEN*DET(1)
               DET(2) = DET(2) - 1.0D0
            GO TO 10
   20       CONTINUE
   30       IF (DABS(DET(1)) .LT. TEN) GO TO 40
               DET(1) = DET(1)/TEN
               DET(2) = DET(2) + 1.0D0
            GO TO 30
   40       CONTINUE
   50    CONTINUE
   60    CONTINUE
   70 CONTINUE
C
C     compute inverse(u)
C
      IF (MOD(JOB,10) .EQ. 0) GO TO 150
         DO 100 K = 1, N
            A(K,K) = 1.0D0/A(K,K)
            T = -A(K,K)
            CALL DSCAL(K-1,T,A(1,K),1)
            KP1 = K + 1
            IF (N .LT. KP1) GO TO 90
            DO 80 J = KP1, N
               T = A(K,J)
               A(K,J) = 0.0D0
               CALL DAXPY(K,T,A(1,K),1,A(1,J),1)
   80       CONTINUE
   90       CONTINUE
  100    CONTINUE
C
C        form inverse(u)*inverse(l)
C
         NM1 = N - 1
         IF (NM1 .LT. 1) GO TO 140
         DO 130 KB = 1, NM1
            K = N - KB
            KP1 = K + 1
            DO 110 I = KP1, N
               WORK(I) = A(I,K)
               A(I,K) = 0.0D0
  110       CONTINUE
            DO 120 J = KP1, N
               T = WORK(J)
               CALL DAXPY(N,T,A(1,J),1,A(1,K),1)
  120       CONTINUE
            L = IPVT(K)
            IF (L .NE. K) CALL DSWAP(N,A(1,K),1,A(1,L),1)
  130    CONTINUE
  140    CONTINUE
  150 CONTINUE
      RETURN
      END
      SUBROUTINE DGBDI(ABD,LDA,N,ML,MU,IPVT,DET)
      INTEGER LDA,N,ML,MU,IPVT(*)
      DOUBLE PRECISION ABD(LDA,*),DET(2)
C
C     DGBDI COMPUTES THE DETERMINANT OF A BAND MATRIX
C     USING THE FACTORS COMPUTED BY DGBCO OR DGBFA.
C     IF THE INVERSE IS NEEDED, USE DGBSL  N  TIMES.
C
C     ON ENTRY
C
C        ABD     DOUBLE PRECISION(LDA, N)
C                THE OUTPUT FROM DGBCO OR DGBFA.
C
C        LDA     INTEGER
C                THE LEADING DIMENSION OF THE ARRAY  ABD .
C
C        N       INTEGER
C                THE ORDER OF THE ORIGINAL MATRIX.
C
C        ML      INTEGER
C                NUMBER OF DIAGONALS BELOW THE MAIN DIAGONAL.
C
C        MU      INTEGER
C                NUMBER OF DIAGONALS ABOVE THE MAIN DIAGONAL.
C
C        IPVT    INTEGER(N)
C                THE PIVOT VECTOR FROM DGBCO OR DGBFA.
C
C     ON RETURN
C
C        DET     DOUBLE PRECISION(2)
C                DETERMINANT OF ORIGINAL MATRIX.
C                DETERMINANT = DET(1) * 10.0**DET(2)
C                WITH  1.0 .LE. DABS(DET(1)) .LT. 10.0
C                OR  DET(1) = 0.0 .
C
C     LINPACK. THIS VERSION DATED 08/14/78 .
C     CLEVE MOLER, UNIVERSITY OF NEW MEXICO, ARGONNE NATIONAL LAB.
C
C     SUBROUTINES AND FUNCTIONS
C
C     FORTRAN DABS
C
C     INTERNAL VARIABLES
C
      DOUBLE PRECISION TEN
      INTEGER I,M
C
C
      M = ML + MU + 1
      DET(1) = 1.0D0
      DET(2) = 0.0D0
      TEN = 10.0D0
      DO 50 I = 1, N
         IF (IPVT(I) .NE. I) DET(1) = -DET(1)
         DET(1) = ABD(M,I)*DET(1)
C     ...EXIT
         IF (DET(1) .EQ. 0.0D0) GO TO 60
   10    IF (DABS(DET(1)) .GE. 1.0D0) GO TO 20
            DET(1) = TEN*DET(1)
            DET(2) = DET(2) - 1.0D0
         GO TO 10
   20    CONTINUE
   30    IF (DABS(DET(1)) .LT. TEN) GO TO 40
            DET(1) = DET(1)/TEN
            DET(2) = DET(2) + 1.0D0
         GO TO 30
   40    CONTINUE
   50 CONTINUE
   60 CONTINUE
      RETURN
      END
C-----------------------------------------------------------------------
      SUBROUTINE DQRDC(X,LDX,N,P,QRAUX,JPVT,WORK,JOB)
      INTEGER LDX,N,P,JOB
      INTEGER JPVT(*)
      DOUBLE PRECISION X(LDX,*),QRAUX(*),WORK(*)
C
C     dqrdc uses householder transformations to compute the qr
C     factorization of an n by p matrix x.  column pivoting
C     based on the 2-norms of the reduced columns may be
C     performed at the users option.
C
C     on entry
C
C        x       double precision(ldx,p), where ldx .ge. n.
C                x contains the matrix whose decomposition is to be
C                computed.
C
C        ldx     integer.
C                ldx is the leading dimension of the array x.
C
C        n       integer.
C                n is the number of rows of the matrix x.
C
C        p       integer.
C                p is the number of columns of the matrix x.
C
C        jpvt    integer(p).
C                jpvt contains integers that control the selection
C                of the pivot columns.  the k-th column x(k) of x
C                is placed in one of three classes according to the
C                value of jpvt(k).
C
C                   if jpvt(k) .gt. 0, then x(k) is an initial
C                                      column.
C
C                   if jpvt(k) .eq. 0, then x(k) is a free column.
C
C                   if jpvt(k) .lt. 0, then x(k) is a final column.
C
C                before the decomposition is computed, initial columns
C                are moved to the beginning of the array x and final
C                columns to the end.  both initial and final columns
C                are frozen in place during the computation and only
C                free columns are moved.  at the k-th stage of the
C                reduction, if x(k) is occupied by a free column
C                it is interchanged with the free column of largest
C                reduced norm.  jpvt is not referenced if
C                job .eq. 0.
C
C        work    double precision(p).
C                work is a work array.  work is not referenced if
C                job .eq. 0.
C
C        job     integer.
C                job is an integer that initiates column pivoting.
C                if job .eq. 0, no pivoting is done.
C                if job .ne. 0, pivoting is done.
C
C     on return
C
C        x       x contains in its upper triangle the upper
C                triangular matrix r of the qr factorization.
C                below its diagonal x contains information from
C                which the orthogonal part of the decomposition
C                can be recovered.  note that if pivoting has
C                been requested, the decomposition is not that
C                of the original matrix x but that of x
C                with its columns permuted as described by jpvt.
C
C        qraux   double precision(p).
C                qraux contains further information required to recover
C                the orthogonal part of the decomposition.
C
C        jpvt    jpvt(k) contains the index of the column of the
C                original matrix that has been interchanged into
C                the k-th column, if pivoting was requested.
C
C     linpack. this version dated 08/14/78 .
C     g.w. stewart, university of maryland, argonne national lab.
C
C     dqrdc uses the following functions and subprograms.
C
C     blas daxpy,ddot,dscal,dswap,dnrm2
C     fortran dabs,dmax1,min0,dsqrt
C
C     internal variables
C
      INTEGER J,JP,L,LP1,LUP,MAXJ,PL,PU
      DOUBLE PRECISION MAXNRM,DNRM2,TT
      DOUBLE PRECISION DDOT,NRMXL,T
      LOGICAL NEGJ,SWAPJ
C
C
      PL = 1
      PU = 0
      IF (JOB .EQ. 0) GO TO 60
C
C        pivoting has been requested.  rearrange the columns
C        according to jpvt.
C
         DO 20 J = 1, P
            SWAPJ = JPVT(J) .GT. 0
            NEGJ = JPVT(J) .LT. 0
            JPVT(J) = J
            IF (NEGJ) JPVT(J) = -J
            IF (.NOT.SWAPJ) GO TO 10
               IF (J .NE. PL) CALL DSWAP(N,X(1,PL),1,X(1,J),1)
               JPVT(J) = JPVT(PL)
               JPVT(PL) = J
               PL = PL + 1
   10       CONTINUE
   20    CONTINUE
         PU = P
         DO 50 JJ = 1, P
            J = P - JJ + 1
            IF (JPVT(J) .GE. 0) GO TO 40
               JPVT(J) = -JPVT(J)
               IF (J .EQ. PU) GO TO 30
                  CALL DSWAP(N,X(1,PU),1,X(1,J),1)
                  JP = JPVT(PU)
                  JPVT(PU) = JPVT(J)
                  JPVT(J) = JP
   30          CONTINUE
               PU = PU - 1
   40       CONTINUE
   50    CONTINUE
   60 CONTINUE
C
C     compute the norms of the free columns.
C
      IF (PU .LT. PL) GO TO 80
      DO 70 J = PL, PU
         QRAUX(J) = DNRM2(N,X(1,J),1)
         WORK(J) = QRAUX(J)
   70 CONTINUE
   80 CONTINUE
C
C     perform the householder reduction of x.
C
      LUP = MIN0(N,P)
      DO 200 L = 1, LUP
         IF (L .LT. PL .OR. L .GE. PU) GO TO 120
C
C           locate the column of largest norm and bring it
C           into the pivot position.
C
            MAXNRM = 0.0D0
            MAXJ = L
            DO 100 J = L, PU
               IF (QRAUX(J) .LE. MAXNRM) GO TO 90
                  MAXNRM = QRAUX(J)
                  MAXJ = J
   90          CONTINUE
  100       CONTINUE
            IF (MAXJ .EQ. L) GO TO 110
               CALL DSWAP(N,X(1,L),1,X(1,MAXJ),1)
               QRAUX(MAXJ) = QRAUX(L)
               WORK(MAXJ) = WORK(L)
               JP = JPVT(MAXJ)
               JPVT(MAXJ) = JPVT(L)
               JPVT(L) = JP
  110       CONTINUE
  120    CONTINUE
         QRAUX(L) = 0.0D0
         IF (L .EQ. N) GO TO 190
C
C           compute the householder transformation for column l.
C
            NRMXL = DNRM2(N-L+1,X(L,L),1)
            IF (NRMXL .EQ. 0.0D0) GO TO 180
               IF (X(L,L) .NE. 0.0D0) NRMXL = DSIGN(NRMXL,X(L,L))
               CALL DSCAL(N-L+1,1.0D0/NRMXL,X(L,L),1)
               X(L,L) = 1.0D0 + X(L,L)
C
C              apply the transformation to the remaining columns,
C              updating the norms.
C
               LP1 = L + 1
               IF (P .LT. LP1) GO TO 170
               DO 160 J = LP1, P
                  T = -DDOT(N-L+1,X(L,L),1,X(L,J),1)/X(L,L)
                  CALL DAXPY(N-L+1,T,X(L,L),1,X(L,J),1)
                  IF (J .LT. PL .OR. J .GT. PU) GO TO 150
                  IF (QRAUX(J) .EQ. 0.0D0) GO TO 150
                     TT = 1.0D0 - (DABS(X(L,J))/QRAUX(J))**2
                     TT = DMAX1(TT,0.0D0)
                     T = TT
                     TT = 1.0D0 + 0.05D0*TT*(QRAUX(J)/WORK(J))**2
                     IF (TT .EQ. 1.0D0) GO TO 130
                        QRAUX(J) = QRAUX(J)*DSQRT(T)
                     GO TO 140
  130                CONTINUE
                        QRAUX(J) = DNRM2(N-L,X(L+1,J),1)
                        WORK(J) = QRAUX(J)
  140                CONTINUE
  150             CONTINUE
  160          CONTINUE
  170          CONTINUE
C
C              save the transformation.
C
               QRAUX(L) = X(L,L)
               X(L,L) = -NRMXL
  180       CONTINUE
  190    CONTINUE
  200 CONTINUE
      RETURN
      END
      SUBROUTINE DQRSL(X,LDX,N,K,QRAUX,Y,QY,QTY,B,RSD,XB,JOB,INFO)
      INTEGER LDX,N,K,JOB,INFO
      DOUBLE PRECISION X(LDX,*),QRAUX(*),Y(*),QY(*),QTY(*),B(*),RSD(*),
     *                 XB(*)
C
C     dqrsl applies the output of dqrdc to compute coordinate
C     transformations, projections, and least squares solutions.
C     for k .le. min(n,p), let xk be the matrix
C
C            xk = (x(jpvt(1)),x(jpvt(2)), ... ,x(jpvt(k)))
C
C     formed from columnns jpvt(1), ... ,jpvt(k) of the original
C     n x p matrix x that was input to dqrdc (if no pivoting was
C     done, xk consists of the first k columns of x in their
C     original order).  dqrdc produces a factored orthogonal matrix q
C     and an upper triangular matrix r such that
C
C              xk = q * (r)
C                       (0)
C
C     this information is contained in coded form in the arrays
C     x and qraux.
C
C     on entry
C
C        x      double precision(ldx,p).
C               x contains the output of dqrdc.
C
C        ldx    integer.
C               ldx is the leading dimension of the array x.
C
C        n      integer.
C               n is the number of rows of the matrix xk.  it must
C               have the same value as n in dqrdc.
C
C        k      integer.
C               k is the number of columns of the matrix xk.  k
C               must nnot be greater than min(n,p), where p is the
C               same as in the calling sequence to dqrdc.
C
C        qraux  double precision(p).
C               qraux contains the auxiliary output from dqrdc.
C
C        y      double precision(n)
C               y contains an n-vector that is to be manipulated
C               by dqrsl.
C
C        job    integer.
C               job specifies what is to be computed.  job has
C               the decimal expansion abcde, with the following
C               meaning.
C
C                    if a.ne.0, compute qy.
C                    if b,c,d, or e .ne. 0, compute qty.
C                    if c.ne.0, compute b.
C                    if d.ne.0, compute rsd.
C                    if e.ne.0, compute xb.
C
C               note that a request to compute b, rsd, or xb
C               automatically triggers the computation of qty, for
C               which an array must be provided in the calling
C               sequence.
C
C     on return
C
C        qy     double precision(n).
C               qy conntains q*y, if its computation has been
C               requested.
C
C        qty    double precision(n).
C               qty contains trans(q)*y, if its computation has
C               been requested.  here trans(q) is the
C               transpose of the matrix q.
C
C        b      double precision(k)
C               b contains the solution of the least squares problem
C
C                    minimize norm2(y - xk*b),
C
C               if its computation has been requested.  (note that
C               if pivoting was requested in dqrdc, the j-th
C               component of b will be associated with column jpvt(j)
C               of the original matrix x that was input into dqrdc.)
C
C        rsd    double precision(n).
C               rsd contains the least squares residual y - xk*b,
C               if its computation has been requested.  rsd is
C               also the orthogonal projection of y onto the
C               orthogonal complement of the column space of xk.
C
C        xb     double precision(n).
C               xb contains the least squares approximation xk*b,
C               if its computation has been requested.  xb is also
C               the orthogonal projection of y onto the column space
C               of x.
C
C        info   integer.
C               info is zero unless the computation of b has
C               been requested and r is exactly singular.  in
C               this case, info is the index of the first zero
C               diagonal element of r and b is left unaltered.
C
C     the parameters qy, qty, b, rsd, and xb are not referenced
C     if their computation is not requested and in this case
C     can be replaced by dummy variables in the calling program.
C     to save storage, the user may in some cases use the same
C     array for different parameters in the calling sequence.  a
C     frequently occuring example is when one wishes to compute
C     any of b, rsd, or xb and does not need y or qty.  in this
C     case one may identify y, qty, and one of b, rsd, or xb, while
C     providing separate arrays for anything else that is to be
C     computed.  thus the calling sequence
C
C          call dqrsl(x,ldx,n,k,qraux,y,dum,y,b,y,dum,110,info)
C
C     will result in the computation of b and rsd, with rsd
C     overwriting y.  more generally, each item in the following
C     list contains groups of permissible identifications for
C     a single callinng sequence.
C
C          1. (y,qty,b) (rsd) (xb) (qy)
C
C          2. (y,qty,rsd) (b) (xb) (qy)
C
C          3. (y,qty,xb) (b) (rsd) (qy)
C
C          4. (y,qy) (qty,b) (rsd) (xb)
C
C          5. (y,qy) (qty,rsd) (b) (xb)
C
C          6. (y,qy) (qty,xb) (b) (rsd)
C
C     in any group the value returned in the array allocated to
C     the group corresponds to the last member of the group.
C
C     linpack. this version dated 08/14/78 .
C     g.w. stewart, university of maryland, argonne national lab.
C
C     dqrsl uses the following functions and subprograms.
C
C     blas daxpy,dcopy,ddot
C     fortran dabs,min0,mod
C
C     internal variables
C
      INTEGER I,J,JJ,JU,KP1
      DOUBLE PRECISION DDOT,T,TEMP
      LOGICAL CB,CQY,CQTY,CR,CXB
C
C
C     set info flag.
C
      INFO = 0
C
C     determine what is to be computed.
C
      CQY = JOB/10000 .NE. 0
      CQTY = MOD(JOB,10000) .NE. 0
      CB = MOD(JOB,1000)/100 .NE. 0
      CR = MOD(JOB,100)/10 .NE. 0
      CXB = MOD(JOB,10) .NE. 0
      JU = MIN0(K,N-1)
C
C     special action when n=1.
C
      IF (JU .NE. 0) GO TO 40
         IF (CQY) QY(1) = Y(1)
         IF (CQTY) QTY(1) = Y(1)
         IF (CXB) XB(1) = Y(1)
         IF (.NOT.CB) GO TO 30
            IF (X(1,1) .NE. 0.0D0) GO TO 10
               INFO = 1
            GO TO 20
   10       CONTINUE
               B(1) = Y(1)/X(1,1)
   20       CONTINUE
   30    CONTINUE
         IF (CR) RSD(1) = 0.0D0
      GO TO 250
   40 CONTINUE
C
C        set up to compute qy or qty.
C
         IF (CQY) CALL DCOPY(N,Y,1,QY,1)
         IF (CQTY) CALL DCOPY(N,Y,1,QTY,1)
         IF (.NOT.CQY) GO TO 70
C
C           compute qy.
C
            DO 60 JJ = 1, JU
               J = JU - JJ + 1
               IF (QRAUX(J) .EQ. 0.0D0) GO TO 50
                  TEMP = X(J,J)
                  X(J,J) = QRAUX(J)
                  T = -DDOT(N-J+1,X(J,J),1,QY(J),1)/X(J,J)
                  CALL DAXPY(N-J+1,T,X(J,J),1,QY(J),1)
                  X(J,J) = TEMP
   50          CONTINUE
   60       CONTINUE
   70    CONTINUE
         IF (.NOT.CQTY) GO TO 100
C
C           compute trans(q)*y.
C
            DO 90 J = 1, JU
               IF (QRAUX(J) .EQ. 0.0D0) GO TO 80
                  TEMP = X(J,J)
                  X(J,J) = QRAUX(J)
                  T = -DDOT(N-J+1,X(J,J),1,QTY(J),1)/X(J,J)
                  CALL DAXPY(N-J+1,T,X(J,J),1,QTY(J),1)
                  X(J,J) = TEMP
   80          CONTINUE
   90       CONTINUE
  100    CONTINUE
C
C        set up to compute b, rsd, or xb.
C
         IF (CB) CALL DCOPY(K,QTY,1,B,1)
         KP1 = K + 1
         IF (CXB) CALL DCOPY(K,QTY,1,XB,1)
         IF (CR .AND. K .LT. N) CALL DCOPY(N-K,QTY(KP1),1,RSD(KP1),1)
         IF (.NOT.CXB .OR. KP1 .GT. N) GO TO 120
            DO 110 I = KP1, N
               XB(I) = 0.0D0
  110       CONTINUE
  120    CONTINUE
         IF (.NOT.CR) GO TO 140
            DO 130 I = 1, K
               RSD(I) = 0.0D0
  130       CONTINUE
  140    CONTINUE
         IF (.NOT.CB) GO TO 190
C
C           compute b.
C
            DO 170 JJ = 1, K
               J = K - JJ + 1
               IF (X(J,J) .NE. 0.0D0) GO TO 150
                  INFO = J
C           ......exit
                  GO TO 180
  150          CONTINUE
               B(J) = B(J)/X(J,J)
               IF (J .EQ. 1) GO TO 160
                  T = -B(J)
                  CALL DAXPY(J-1,T,X(1,J),1,B,1)
  160          CONTINUE
  170       CONTINUE
  180       CONTINUE
  190    CONTINUE
         IF (.NOT.CR .AND. .NOT.CXB) GO TO 240
C
C           compute rsd or xb as required.
C
            DO 230 JJ = 1, JU
               J = JU - JJ + 1
               IF (QRAUX(J) .EQ. 0.0D0) GO TO 220
                  TEMP = X(J,J)
                  X(J,J) = QRAUX(J)
                  IF (.NOT.CR) GO TO 200
                     T = -DDOT(N-J+1,X(J,J),1,RSD(J),1)/X(J,J)
                     CALL DAXPY(N-J+1,T,X(J,J),1,RSD(J),1)
  200             CONTINUE
                  IF (.NOT.CXB) GO TO 210
                     T = -DDOT(N-J+1,X(J,J),1,XB(J),1)/X(J,J)
                     CALL DAXPY(N-J+1,T,X(J,J),1,XB(J),1)
  210             CONTINUE
                  X(J,J) = TEMP
  220          CONTINUE
  230       CONTINUE
  240    CONTINUE
  250 CONTINUE
      RETURN
      END
      SUBROUTINE  ICOPY(N,DX,INCX,DY,INCY)
C
C     copies a vector, x, to a vector, y.
C     uses unrolled loops for increments equal to one.
C     jack dongarra, linpack, 3/11/78.
C
      INTEGER DX(*),DY(*)
      INTEGER I,INCX,INCY,IX,IY,M,MP1,N
C
      IF(N.LE.0)RETURN
      IF(INCX.EQ.1.AND.INCY.EQ.1)GO TO 20
C
C        code for unequal increments or equal increments
C          not equal to 1
C
      IX = 1
      IY = 1
      IF(INCX.LT.0)IX = (-N+1)*INCX + 1
      IF(INCY.LT.0)IY = (-N+1)*INCY + 1
      DO 10 I = 1,N
        DY(IY) = DX(IX)
        IX = IX + INCX
        IY = IY + INCY
   10 CONTINUE
      RETURN
C
C        code for both increments equal to 1
C
C
C        clean-up loop
C
   20 M = MOD(N,7)
      IF( M .EQ. 0 ) GO TO 40
      DO 30 I = 1,M
        DY(I) = DX(I)
   30 CONTINUE
      IF( N .LT. 7 ) RETURN
   40 MP1 = M + 1
      DO 50 I = MP1,N,7
        DY(I) = DX(I)
        DY(I + 1) = DX(I + 1)
        DY(I + 2) = DX(I + 2)
        DY(I + 3) = DX(I + 3)
        DY(I + 4) = DX(I + 4)
        DY(I + 5) = DX(I + 5)
        DY(I + 6) = DX(I + 6)
   50 CONTINUE
      RETURN
      END
      SUBROUTINE  IDCOPY(N,DX,INCX,DY,INCY)
C
C     copies a vector, x, to a vector, y.
C     uses unrolled loops for increments equal to one.
C     jack dongarra, linpack, 3/11/78.
C     CHANGED TO COPYING   INTEGER ---> DOUBLE PRECISION BY U.MAAS
C
      INTEGER          DX(*)
      DOUBLE PRECISION DY(1)
      INTEGER I,INCX,INCY,IX,IY,M,MP1,N
C
      IF(N.LE.0)RETURN
      IF(INCX.EQ.1.AND.INCY.EQ.1)GO TO 20
C
C        code for unequal increments or equal increments
C          not equal to 1
C
      IX = 1
      IY = 1
      IF(INCX.LT.0)IX = (-N+1)*INCX + 1
      IF(INCY.LT.0)IY = (-N+1)*INCY + 1
      DO 10 I = 1,N
        DY(IY) = FLOAT(DX(IX))
        IX = IX + INCX
        IY = IY + INCY
   10 CONTINUE
      RETURN
C
C        code for both increments equal to 1
C
C
C        clean-up loop
C
   20 M = MOD(N,7)
      IF( M .EQ. 0 ) GO TO 40
      DO 30 I = 1,M
        DY(I) = FLOAT(DX(I))
   30 CONTINUE
      IF( N .LT. 7 ) RETURN
   40 MP1 = M + 1
      DO 50 I = MP1,N,7
        DY(I)     = FLOAT(DX(I    ))
        DY(I + 1) = FLOAT(DX(I + 1))
        DY(I + 2) = FLOAT(DX(I + 2))
        DY(I + 3) = FLOAT(DX(I + 3))
        DY(I + 4) = FLOAT(DX(I + 4))
        DY(I + 5) = FLOAT(DX(I + 5))
        DY(I + 6) = FLOAT(DX(I + 6))
   50 CONTINUE
      RETURN
      END
CNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEW
      SUBROUTINE DGEINV(N,A,W,IPVT,INFO)
C***********************************************************************
C                                                                      *
C     *************************************************************
C     *                                                           *
C     *   INVERSION OF A MATRIX BY USING THE LINPACK ROUTINES     *
C     *                                                           *
C     *           LAST CHANGE: U.MAAS/NOV.3 ,1989                 *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C     ILL : ILL=0 FOR CORRECT, ILL>1 FOR INCORRECT RETURN              *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION A(N,N),W(N),DET(2)
      INTEGER          IPVT(N)
      CALL DGEFA(A,N,N,IPVT,INFO)
      IF(INFO.GT.0) RETURN
      CALL DGEDI(A,N,N,IPVT,DET,W,01)
      RETURN
C***********************************************************************
C     END OF -DGEINV-
C***********************************************************************
      END
      SUBROUTINE STRUMA(INFO,LDA,N,M,A,PRLIM,NFILE6)
C***********************************************************************
C                                                                      *
C     *************************************************************
C     *                                                           *
C     *   PRINT THE STRUCTURE OF A MATRIX                         *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION A(LDA,M)
      CHARACTER*1 S(78)
      DATA L1M/78/,L2M/17/
C***********************************************************************
C     CALCULATION OF LOWER LIMIT FOR PRINTING
C***********************************************************************
      IF(PRLIM.GE.0.D0) THEN
      HELP=0.D0
      DO 10 I=1,N
      DO 10 J=1,M
      HELP =DMAX1(DABS(A(I,J)),HELP)
   10 CONTINUE
      ELSE
      HELP=1.D0
      ENDIF
      PRILIM=    HELP * DABS(PRLIM)
      SSSLIM= 1.D-5  * PRILIM
      ESSLIM= 1.D-10 * PRILIM
C***********************************************************************
C     PRINT ONLY SYMBOLS
C***********************************************************************
      IF(INFO.EQ.1) THEN
      IF(M.GT.L1M) RETURN
      WRITE(NFILE6,80) ' ','.',('_',J=1,M),'.'
      DO 100 I=1,N
      DO 110 J=1,M
                                 S(J) = ' '
      IF(DABS(A(I,J)).GT.ESSLIM) S(J) = '.'
      IF(DABS(A(I,J)).GT.SSSLIM) S(J) = '+'
      IF(DABS(A(I,J)).GT.PRILIM) S(J) = 'X'
  110 CONTINUE
      WRITE(NFILE6,80) ' ','|',(S(J),J=1,M),'|'
  100 CONTINUE
      WRITE(NFILE6,80) ' ','.',('-',J=1,M),'.'
      ENDIF
C***********************************************************************
C     CALCULATION OF LOWER LIMIT FOR PIVOT ELEMENTS
C***********************************************************************
      IF(INFO.EQ.2) THEN
      IF(M.GT.L2M) RETURN
      DO 200 I=1,N
      WRITE(NFILE6,81) (A(I,J),J=1,M)
  200 CONTINUE
      ENDIF
      RETURN
   80 FORMAT(80A1)
   81 FORMAT(1X,17(1PE7.0))
C***********************************************************************
C     END OF -STRUMA-
C***********************************************************************
      END
      DOUBLE PRECISION FUNCTION DIFNO(N,DX,INCX,DY,INCY)
C
C     FORMS THE  |DX-DY|2
C     USES UNROLLED LOOPS FOR INCREMENTS EQUAL TO ONE.
C
      DOUBLE PRECISION DX(1),DY(1),DTEMP
      INTEGER I,INCX,INCY,IX,IY,M,MP1,N
C
      DIFNO= 0.0D0
      DTEMP = 0.0D0
      IF(N.LE.0)RETURN
      IF(INCX.EQ.1.AND.INCY.EQ.1)GO TO 20
C
C        CODE FOR UNEQUAL INCREMENTS OR EQUAL INCREMENTS
C          NOT EQUAL TO 1
C
      IX = 1
      IY = 1
      IF(INCX.LT.0)IX = (-N+1)*INCX + 1
      IF(INCY.LT.0)IY = (-N+1)*INCY + 1
      DO 10 I = 1,N
        DTEMP = DTEMP + (DX(IX)-DY(IY))**2
        IX = IX + INCX
        IY = IY + INCY
   10 CONTINUE
      DIFNO= DTEMP
      RETURN
C
C        CODE FOR BOTH INCREMENTS EQUAL TO 1
C
C
C        CLEAN-UP LOOP
C
   20 M = MOD(N,5)
      IF( M .EQ. 0 ) GO TO 40
      DO 30 I = 1,M
        DTEMP = DTEMP + (DX(I)-DY(I))**2
   30 CONTINUE
      IF( N .LT. 5 ) GO TO 60
   40 MP1 = M + 1
      DO 50 I = MP1,N,5
        DTEMP = DTEMP + (DX(I)-DY(I))**2  + (DX(I + 1)-DY(I + 1))**2  +
     *   (DX(I + 2)-DY(I + 2))**2 + (DX(I + 3)-DY(I + 3))**2
     *   + (DX(I + 4)-DY(I + 4))**2
   50 CONTINUE
   60 DIFNO= DTEMP
      RETURN
      END
      SUBROUTINE NMULTI(N,V,Z,NDM,U,IMAT,ISYM)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION IMAT(*),U(*),V(N),Z(N)
      N0=IMAT(3)
      N1=IMAT(4)
      N2=IMAT(5)
      INFO= 0
      NB= N1*N2
      CALL NONMUL(N0,N1,NB,U,V,Z,INFO)
      RETURN
      END
      SUBROUTINE CMULTI(N,V,Z,NDM,U,IMAT,ISYM)
C***********************************************************************
C     MULTIPL OF TRI W CORN
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION IMAT(*),U(NDM),V(N),Z(N)
      N0=IMAT(3)
      N1=IMAT(4)
      INDM1=1+N0*N0*N1*3
      INDM2=INDM1+N0*N0
      INDM3=INDM2+N0*N0
      INDM4=INDM3+N0*N0
      INDM5=INDM4+N0*N0
      INDM6=INDM5+N0*N0
      INDV1=1
      INDV2=1+N0
      INDV3=(N1-2)*N0 + 1
      INDV4=(N1-1)*N0 + 1
      INFO= 0
      CALL TRIMUL(N0,N1,U,V,Z,INFO)
C---- CORNER 1
      CALL DGQMMV(N0,U(INDM1),V(INDV1),Z(INDV4),1)
      CALL DGQMMV(N0,U(INDM2),V(INDV1),Z(INDV3),1)
      CALL DGQMMV(N0,U(INDM3),V(INDV2),Z(INDV4),1)
      CALL DGQMMV(N0,U(INDM4),V(INDV3),Z(INDV1),1)
      CALL DGQMMV(N0,U(INDM5),V(INDV4),Z(INDV2),1)
      CALL DGQMMV(N0,U(INDM6),V(INDV4),Z(INDV1),1)
C                                                                      *
      RETURN
      END
      SUBROUTINE TPRECO(N,R,Z,NDA,A,IMAT,ISYM,
     1     NDP,H,LIWKP,IPIVOT)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION R(N),Z(N),IMAT(10),A(NDA),H(NDP),IPIVOT(LIWKP)
C**********************************************************************
C
C***********************************************************************
      N0  = IMAT(3)
      N1  = IMAT(4)
      NDM = IMAT(8)
      JOB = IMAT(9)
      CALL DTPREC(JOB,N0,N1,N,NDM,A,NDP,H,IPIVOT,R,Z,IFAIL)
      IF(IFAIL.NE.0) WRITE(6,*) ' PREC IFAIL', IFAIL
C***********************************************************************
C     REGULAR EXIT
C***********************************************************************
      RETURN
      END
      SUBROUTINE NPRECO(N,R,Z,NDA,A,IMAT,ISYM,
     1     NDP,H,LIWKP,IPIVOT)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION R(N),Z(N),IMAT(10),A(NDA),H(NDP),IPIVOT(LIWKP)
C**********************************************************************
C     DECOMPOSE EACH TRIDIAGONAL SUBMATRIX
C***********************************************************************
      N0  = IMAT(3)
      N1  = IMAT(4)
      N2  = IMAT(5)
      NDM = IMAT(8)
      JOB = IMAT(9)
      NB=N2*N1
      CALL DNPREC(JOB,N0,N1,NB,N,NDM,A,NDP,H,IPIVOT,R,Z,IFAIL)
      IF(IFAIL.NE.0) WRITE(6,*) ' PREC IFAIL', IFAIL
C***********************************************************************
C     REGULAR EXIT
C***********************************************************************
      RETURN
      END
      SUBROUTINE NONMUL(N,N1,NB,A,X,Y,JOB)
C***********************************************************************
C                                                                      *
C     *************************************************************
C     *                                                           *
C     *   SOLUTION OF OF A BLOCK-NONADIAGONAL EQUATION SYSTEM     *
C     *                      U.MAAS                               *
C     *                     5/28/1989                             *
C     *           LAST CHANGE: U.MAAS 5/28/1989                   *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C                                                                      *
C     INPUT:
C                                                                      *
C     N           DIMENSION OF SUB MATRIX                              *
C     NB          NUMBER OF BLOCKS                                     *
C     A           MATRIX                                               *
C     X(N,*)      VECTOR                                               *
C     OUTPUT:                                                          C
C                                                                      C
C     Y(N,1)  SOLUTION VECTOR                                          C
C                                                                      C
C***********************************************************************
C
C                                                                      C
C***********************************************************************
C     STORAGE ORGANIZATION                                             C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION A(N,N,*),X(N,NB),Y(N,NB)
C***********************************************************************
C
C     INITIALIZE (NEW STORAGE FORM)
C
C***********************************************************************
      NBA =  NB - N1 - 1
      NBB =  NB - N1
      NBC =  NB - N1 + 1
      NBO =  NB
      NBP =  NB
      NBQ =  NB
      NBU =  NB - N1 + 1
      NBV =  NB - N1
      NBW =  NB - N1 - 1
      INA = 0
      INB = INA + NBA
      INC = INB + NBB
      INO = INC + NBC
      INP = INO + NBO
      INQ = INP + NBP
      INU = INQ + NBQ
      INV = INU + NBU
      INW = INV + NBV
C***********************************************************************
C
C***********************************************************************
      NQ=N*N
      IDUM=INO+1
C***********************************************************************
C     SET INITIAL GUESS AND IJO
C***********************************************************************
      IF(JOB.EQ.0) THEN
        DO 10 J=1,N
        DO 10 I=1,NB
          Y(J,I)=0.D0
   10   CONTINUE
        IJO = 1
        GOTO 100
      ENDIF
      IF(JOB.EQ.1) THEN
        IJO = 1
        GOTO 100
      ENDIF
      IF(JOB.EQ.-1) THEN
        IJO = -1
        GOTO 100
      ENDIF
      GOTO 9000
C***********************************************************************
C
C     SET Y TO ZERO FOR JOB .EQ. 0
C
C***********************************************************************
  100 CONTINUE
C***********************************************************************
C
C     CALCULATE  L X'        = Z                                       C
C
C***********************************************************************
C***********************************************************************
C     CALCULATE A*X                                                    *
C***********************************************************************
      NFA= N1+2
      DO 110 I=NFA,NB
      CALL DGQMMV(N,A(1,1,INA+I-N1-1),X(1,I-N1-1),Y(1,I),IJO)
  110 CONTINUE
C***********************************************************************
C     CALCULATE B*X                                                    *
C***********************************************************************
      NFB= N1+1
      DO 120 I=NFB,NB
      CALL DGQMMV(N,A(1,1,INB+I-N1),X(1,I-N1),Y(1,I),IJO)
  120 CONTINUE
C***********************************************************************
C     CALCULATE C*X                                                    *
C***********************************************************************
      NFC= N1
      DO 130 I=NFC,NB
      CALL DGQMMV(N,A(1,1,INC+I-N1+1),X(1,I-N1+1),Y(1,I),IJO)
  130 CONTINUE
C***********************************************************************
C     CALCULATE O*X                                                    *
C***********************************************************************
      DO 140 I=2,NB
      CALL DGQMMV(N,A(1,1,INO+I),X(1,I-1),Y(1,I),IJO)
  140 CONTINUE
C***********************************************************************
C     CALCULATE P*X                                                    *
C***********************************************************************
      DO 150 I=1,NB
      CALL DGQMMV(N,A(1,1,INP+I),X(1,I),Y(1,I),IJO)
  150 CONTINUE
C***********************************************************************
C     CALCULATE Q*X                                                    *
C***********************************************************************
      NLQ = NB-1
      DO 160 I=1,NLQ
      CALL DGQMMV(N,A(1,1,INQ+I),X(1,I+1),Y(1,I),IJO)
  160 CONTINUE
C***********************************************************************
C     CALCULATE U*X                                                    *
C***********************************************************************
      NLU=NB-N1+1
      DO 170 I=1,NLU
      CALL DGQMMV(N,A(1,1,INU+I),X(1,I+N1-1),Y(1,I),IJO)
  170 CONTINUE
C***********************************************************************
C     CALCULATE V*X                                                    *
C***********************************************************************
      NLV=NB-N1
      DO 180 I=1,NLV
      CALL DGQMMV(N,A(1,1,INV+I),X(1,I+N1),Y(1,I),IJO)
  180 CONTINUE
C***********************************************************************
C     CALCULATE W*X                                                    *
C***********************************************************************
      NLW=NB-N1-1
      DO 190 I=1,NLW
      CALL DGQMMV(N,A(1,1,INW+I),X(1,I+N1+1),Y(1,I),IJO)
  190 CONTINUE
      RETURN
C***********************************************************************
C     ERROR EXITS                                                     C
C***********************************************************************
C----- LESS THAN 3 SUBMATRICES
 9000 IFAIL= 1
      RETURN
C***********************************************************************
C     END OF NONMUL
C***********************************************************************
      END
      SUBROUTINE TRIRSU(N,NB,A,X,JOB)
C***********************************************************************
C                                                                      *
C     *************************************************************
C     *                                                           *
C     *   DETERMINE SUM OVER ABS. VALUES OF THE COLUMNS           *
C     *                      U.MAAS                               *
C     *                     5/28/1989                             *
C     *           LAST CHANGE: U.MAAS 5/28/1989                   *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C                                                                      *
C     INPUT:
C                                                                      *
C     N           DIMENSION OF SUB MATRIX                              *
C     NB          NUMBER OF BLOCKS                                     *
C     A           MATRIX                                               *
C     OUTPUT:                                                          C
C                                                                      C
C     X(N,1)  VECTOR OF SUMS                                           C
C                                                                      C
C***********************************************************************
C
C                                                                      C
C***********************************************************************
C     STORAGE ORGANIZATION                                             C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION A(N,N,*),X(N,NB)
C***********************************************************************
C
C     INITIALIZE (NEW STORAGE FORM)
C
C***********************************************************************
      NBO =  NB
      NBP =  NB
      NBQ =  NB
      INO = 0
      INP = INO + NBO
      INQ = INP + NBP
C***********************************************************************
C
C***********************************************************************
      NQ=N*N
      IDUM=INO+1
C***********************************************************************
C     SET INITIAL X TO ZERO
C***********************************************************************
      DO 10 I=1,N
      DO 10 J=1,NB
      X(I,J)= 0.D0
   10 CONTINUE
C***********************************************************************
C
C     SET Y TO ZERO FOR JOB .EQ. 0
C
C***********************************************************************
  100 CONTINUE
C***********************************************************************
C     CALCULATE O*X                                                    *
C***********************************************************************
      DO 140 I=2,NB
      CALL DENRSU(N,N,A(1,1,INO+I),X(1,I))
  140 CONTINUE
C***********************************************************************
C     CALCULATE P*X                                                    *
C***********************************************************************
      DO 150 I=1,NB
      CALL DENRSU(N,N,A(1,1,INP+I),X(1,I))
  150 CONTINUE
C***********************************************************************
C     CALCULATE Q*X                                                    *
C***********************************************************************
      NLQ = NB-1
      DO 160 I=1,NLQ
      CALL DENRSU(N,N,A(1,1,INQ+I),X(1,I))
  160 CONTINUE
      RETURN
C***********************************************************************
C     END OF TRIRSU
C***********************************************************************
      END
      SUBROUTINE NONRSU(N,N1,NB,A,X,JOB)
C***********************************************************************
C                                                                      *
C     *************************************************************
C     *                                                           *
C     *   DETERMINE SUM OVER ABS. VALUES OF THE COLUMNS           *
C     *                      U.MAAS                               *
C     *                     5/28/1989                             *
C     *           LAST CHANGE: U.MAAS 5/28/1989                   *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C                                                                      *
C     INPUT:
C                                                                      *
C     N           DIMENSION OF SUB MATRIX                              *
C     NB          NUMBER OF BLOCKS                                     *
C     A           MATRIX                                               *
C     OUTPUT:                                                          C
C                                                                      C
C     X(N,1)  VECTOR OF SUMS                                           C
C                                                                      C
C***********************************************************************
C
C                                                                      C
C***********************************************************************
C     STORAGE ORGANIZATION                                             C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION A(N,N,*),X(N,NB)
C***********************************************************************
C
C     INITIALIZE (NEW STORAGE FORM)
C
C***********************************************************************
      NBA =  NB - N1 - 1
      NBB =  NB - N1
      NBC =  NB - N1 + 1
      NBO =  NB
      NBP =  NB
      NBQ =  NB
      NBU =  NB - N1 + 1
      NBV =  NB - N1
      NBW =  NB - N1 - 1
      INA = 0
      INB = INA + NBA
      INC = INB + NBB
      INO = INC + NBC
      INP = INO + NBO
      INQ = INP + NBP
      INU = INQ + NBQ
      INV = INU + NBU
      INW = INV + NBV
C***********************************************************************
C
C***********************************************************************
      NQ=N*N
      IDUM=INO+1
C***********************************************************************
C     SET INITIAL X TO ZERO
C***********************************************************************
      DO 10 I=1,N
      DO 10 J=1,NB
      X(I,J)= 0.D0
   10 CONTINUE
C***********************************************************************
C
C     SET Y TO ZERO FOR JOB .EQ. 0
C
C***********************************************************************
  100 CONTINUE
C***********************************************************************
C     CALCULATE A*X                                                    *
C***********************************************************************
      NFA= N1+2
      DO 110 I=NFA,NB
      CALL DENRSU(N,N,A(1,1,INA+I-N1-1),X(1,I))
  110 CONTINUE
C***********************************************************************
C     CALCULATE B*X                                                    *
C***********************************************************************
      NFB= N1+1
      DO 120 I=NFB,NB
      CALL DENRSU(N,N,A(1,1,INB+I-N1),X(1,I))
  120 CONTINUE
C***********************************************************************
C     CALCULATE C*X                                                    *
C***********************************************************************
      NFC= N1
      DO 130 I=NFC,NB
      CALL DENRSU(N,N,A(1,1,INC+I-N1+1),X(1,I))
  130 CONTINUE
C***********************************************************************
C     CALCULATE O*X                                                    *
C***********************************************************************
      DO 140 I=2,NB
      CALL DENRSU(N,N,A(1,1,INO+I),X(1,I))
  140 CONTINUE
C***********************************************************************
C     CALCULATE P*X                                                    *
C***********************************************************************
      DO 150 I=1,NB
      CALL DENRSU(N,N,A(1,1,INP+I),X(1,I))
  150 CONTINUE
C***********************************************************************
C     CALCULATE Q*X                                                    *
C***********************************************************************
      NLQ = NB-1
      DO 160 I=1,NLQ
      CALL DENRSU(N,N,A(1,1,INQ+I),X(1,I))
  160 CONTINUE
C***********************************************************************
C     CALCULATE U*X                                                    *
C***********************************************************************
      NLU=NB-N1+1
      DO 170 I=1,NLU
      CALL DENRSU(N,N,A(1,1,INU+I),X(1,I))
  170 CONTINUE
C***********************************************************************
C     CALCULATE V*X                                                    *
C***********************************************************************
      NLV=NB-N1
      DO 180 I=1,NLV
      CALL DENRSU(N,N,A(1,1,INV+I),X(1,I))
  180 CONTINUE
C***********************************************************************
C     CALCULATE W*X                                                    *
C***********************************************************************
      NLW=NB-N1-1
      DO 190 I=1,NLW
      CALL DENRSU(N,N,A(1,1,INW+I),X(1,I))
  190 CONTINUE
      RETURN
C***********************************************************************
C     END OF NONMUL
C***********************************************************************
      END
      SUBROUTINE DENRSU(NR,NC,A,X)
C***********************************************************************
C
C     NORM FOR DENSE MATRIX
C
C***********************************************************************
C     INPUT:
C     NC          : NUMBER OF COLUMNS
C     NR          : NUMBER OF ROWS
C     A(NR,NC)    : MATRIX
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION A(NR,NC),X(NR)
C***********************************************************************
C     MAXIMUM ROW NORM
C***********************************************************************
      DO 20 IR=1,NR
      DO 10 IC=1,NC
      X(IR)=X(IR)+DABS(A(IR,IC))
   10 CONTINUE
   20 CONTINUE
C***********************************************************************
C     RETURN
C***********************************************************************
      RETURN
C***********************************************************************
C     END OF
C***********************************************************************
      END
      SUBROUTINE DNPREI(JOB,N0,N1,NB,N,NDM,A,NDPMAX,NDP,H,
     1        IPIVOT,IFAIL)
C***********************************************************************
C                                                                      *
C     ************************************************************     *
C     *                                                          *     *
C     *  COMPUTATION AND FACTORIZATION OF PRECONDITIONERS FOR    *     *
C     *    BLOCK-NONADIAGONAL MATRICES                           *     *
C     *                AUTHOR: ULRICH MAAS  2.10.1987            *     *
C     *                                                          *     *
C     *                LAST CHANGE:  2.10.1987/MAAS              *     *
C     *                                                          *     *
C     ************************************************************     *
C                                                                      *
C***********************************************************************
C                                                                      *
C     NOTE: NO CHECK FOR SUFFICIENT ARRAY STORAGE PERFORMED!!!         *
C                                                                      *
C***********************************************************************
C                                                                      *
C     INPUT DATA:                                                      *
C                                                                      *
C      JOB          INFORMATION ABOUT TASK                             *
C                     1 NO PRECONDITIONER (H=I)                        *
C                     2 PRECONDITIONER = ROW SUM                       *
C                     3 PRECONDITIONER = INNER DIAGONAL BLOCKS         *
C                     4 PRECONDITIONER = INNER TRIDIAGONAL BLOCKS      *
C                     5 PRECONDITIONER = INCOMPLETE FACTORIZATION      *
C                                                                      *
C      JOB          INFORMATION ABOUT TASK                             *
C                                                                      *
C      N            DIMENSION OF THE SYSTEM                            *
C                                                                      *
C                                                                      *
C      A            ORIGINAL MATRIX                                    *
C                                                                      *
C                                                                      *
C     OUTPUT DATA:                                                     *
C                                                                      *
C      IFAIL        RETURN CODE                                        *
C                     IFAIL = 0 : SUCESSFULL COMPLETION                *
C                     IFAIL < 0 : FAILURE                              *
C                     SEE SUBROUTINES FOR FURTHER INFORMATION          *
C                                                                      *
C      H            PRECONDITIONER IN DECOMPOSED FORM                  *
C                                                                      *
C     WORKING ARRAYS:                                                  *
C                                                                      *
C                                                                      *
C      IPIVOT(N)    INTEGER WORK SPACE                                 *
C***********************************************************************
C                                                                      *
C***********************************************************************
C***********************************************************************
C     CHAPTER I.: STORAGE ORGANIZATION                                 *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION IPIVOT(N)
      DIMENSION A(NDM),H(NDPMAX)
C***********************************************************************
C     CHAPTER II.: INITIALIZE AND CHECK TASK
C***********************************************************************
      IFAIL=0
C***********************************************************************
C                                                                      *
C     CHAPTER III.: CHECK FOR KIND OF PRECONDITIONER                   *
C                                                                      *
C***********************************************************************
C***********************************************************************
C     CHAPTER III.0: CHECK FOR TYPE OF MATRIX                          *
C***********************************************************************
      IF(JOB.EQ.1) GOTO 100
      IF(JOB.EQ.2) GOTO 200
      IF(JOB.EQ.3) GOTO 300
      IF(JOB.EQ.4) GOTO 400
      IF(JOB.EQ.5) GOTO 500
      GOTO 910
C***********************************************************************
C     CHAPTER II.1: NO PRECONDITIONER                                  *
C***********************************************************************
  100 CONTINUE
      NDP=1
      GOTO 700
C***********************************************************************
C     CHAPTER II.2: PRECONDITIONER = ROW SUM                           *
C***********************************************************************
  200 CONTINUE
      NDP = N
      IF(N.GT.NDPMAX) GOTO 910
      CALL NONRSU(N0,N1,NB,A,H,JOB)
      GOTO 700
C***********************************************************************
C     CHAPTER II.3: PRECONDITIONER = INNER DIAGONAL BLOCKS             *
C***********************************************************************
  300 CONTINUE
      NDP = NB*N0*N0
      IF(NDP.GT.NDPMAX) GOTO 910
      NANF= 1
      NEND= NB*N0*N0
      DO 310 I=NANF,NEND
      H(I)=A(I+(NB-N1)*N0*N0*3+NB*N0*N0)
  310 CONTINUE
      DO 320 I= 1,NB
      IFIR=(I-1)*N0*N0+1
      IP  =(I-1)*N0    +1
      CALL DGEFA(H(IFIR),N0,N0,IPIVOT(IP),ILL)
      IF(ILL.GT.0) GOTO 920
  320 CONTINUE
      GOTO 700
C***********************************************************************
C     CHAPTER II.3: PRECONDITIONER = INNER TRIDIAGONAL                 *
C***********************************************************************
  400 CONTINUE
      NDP =3*NB*N0*N0
      IF(NDP.GT.NDPMAX) GOTO 910
      NANF=          N0*N0 + 1
      NEND=3*NB*N0*N0
      DO 410 I=NANF,NEND
      H(I)=A(I+(NB-N1)*N0*N0*3)
  410 CONTINUE
      CALL DGTFA(NB,N0,H,ILL)
      IF(ILL.NE.0) GOTO 920
      GOTO 700
C***********************************************************************
C     CHAPTER II.3: PRECONDITIONER = INNER TRIDIAGONAL                 *
C***********************************************************************
  500 CONTINUE
      NDP =  NDM
      IF(NDP.GT.NDPMAX) GOTO 910
      DO 510 I=1,NDM
      H(I)=A(I)
  510 CONTINUE
      NQ=N0*N0
      CALL ICCFAP(N0,NQ,N1,NB,H,ILL)
      IF(ILL.NE.0) GOTO 920
      GOTO 700
C***********************************************************************
C                                                                      *
C     REGULAR EXIT                                                     *
C                                                                      *
C***********************************************************************
  700 CONTINUE
      RETURN
C***********************************************************************
C                                                                      *
C     CHAPTER VI.: ERROR EXITS                                         *
C                                                                      *
C***********************************************************************
C---- NOT ENOUGH STORAGE FOR PRECONDITIONER
  910 CONTINUE
      IFAIL=-8
      RETURN
C---- ERROR DURING DECOMPOSITION
  920 CONTINUE
      IFAIL=-4
      RETURN
C***********************************************************************
C                                                                      *
C     END OF LINSOL                                                    *
C                                                                      *
C***********************************************************************
      END
      SUBROUTINE DNPREC(JOB,N0,N1,NB,N,NDM,A,NDP,H,
     1        IPIVOT,R,Z,IFAIL)
C***********************************************************************
C                                                                      *
C     ************************************************************     *
C     *                                                          *     *
C     *  COMPUTATION AND FACTORIZATION OF PRECONDITIONERS FOR    *     *
C     *    BLOCK-NONADIAGONAL MATRICES                           *     *
C     *                AUTHOR: ULRICH MAAS  2.10.1987            *     *
C     *                                                          *     *
C     *                LAST CHANGE:  2.10.1987/MAAS              *     *
C     *                                                          *     *
C     ************************************************************     *
C                                                                      *
C***********************************************************************
C                                                                      *
C     NOTE: NO CHECK FOR SUFFICIENT ARRAY STORAGE PERFORMED!!!         *
C                                                                      *
C***********************************************************************
C                                                                      *
C     INPUT DATA:                                                      *
C                                                                      *
C      JOB          INFORMATION ABOUT TASK                             *
C                     1 NO PRECONDITIONER (H=I)                        *
C                     2 PRECONDITIONER = ROW SUM                       *
C                     3 PRECONDITIONER = INNER DIAGONAL BLOCKS         *
C                     4 PRECONDITIONER = INNER TRIDIAGONAL BLOCKS      *
C                     5 PRECONDITIONER = INCOMPLETE FACTORIZATION      *
C                                                                      *
C      JOB          INFORMATION ABOUT TASK                             *
C                                                                      *
C      N            DIMENSION OF THE SYSTEM                            *
C                                                                      *
C                                                                      *
C      A            ORIGINAL MATRIX                                    *
C                                                                      *
C                                                                      *
C     OUTPUT DATA:                                                     *
C                                                                      *
C      IFAIL        RETURN CODE                                        *
C                     IFAIL = 0 : SUCESSFULL COMPLETION                *
C                     IFAIL < 0 : FAILURE                              *
C                     SEE SUBROUTINES FOR FURTHER INFORMATION          *
C                                                                      *
C      H            PRECONDITIONER IN DECOMPOSED FORM                  *
C                                                                      *
C     WORKING ARRAYS:                                                  *
C                                                                      *
C                                                                      *
C      IPIVOT(N)    INTEGER WORK SPACE                                 *
C***********************************************************************
C                                                                      *
C***********************************************************************
C***********************************************************************
C     CHAPTER I.: STORAGE ORGANIZATION                                 *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION IPIVOT(N)
      DIMENSION A(NDM),H(NDP)
      DIMENSION R(N),Z(N)
C***********************************************************************
C     CHAPTER II.: INITIALIZE AND CHECK TASK
C***********************************************************************
      IFAIL=0
C***********************************************************************
C                                                                      *
C     CHAPTER III.: CHECK FOR KIND OF PRECONDITIONER                   *
C                                                                      *
C***********************************************************************
C***********************************************************************
C     CHAPTER III.0: CHECK FOR TYPE OF MATRIX                          *
C***********************************************************************
      IF(JOB.EQ.1) GOTO 100
      IF(JOB.EQ.2) GOTO 200
      IF(JOB.EQ.3) GOTO 300
      IF(JOB.EQ.4) GOTO 400
      IF(JOB.EQ.5) GOTO 500
      GOTO 910
C***********************************************************************
C     CHAPTER II.1: NO PRECONDITIONER                                  *
C***********************************************************************
  100 CONTINUE
      DO 110 I= 1,N
      Z(I)= R(I)
  110 CONTINUE
      GOTO 700
C***********************************************************************
C     CHAPTER II.2: PRECONDITIONER = ROW SUM                           *
C***********************************************************************
  200 CONTINUE
      DO 210 I= 1,N
      Z(I)= R(I)/H(I)
  210 CONTINUE
      GOTO 700
C***********************************************************************
C     CHAPTER II.3: PRECONDITIONER = INNER DIAGONAL BLOCKS             *
C***********************************************************************
  300 CONTINUE
      DO 310 I= 1,N
      Z(I)= R(I)
  310 CONTINUE
      DO 320 I= 1,NB
      IFIR=(I-1)*N0*N0+1
      IP  =(I-1)*N0    +1
      CALL DGESL(H(IFIR),N0,N0,IPIVOT(IP),Z(IP),0)
  320 CONTINUE
      GOTO 700
C***********************************************************************
C     CHAPTER II.3: PRECONDITIONER = INNER TRIDIAGONAL                 *
C***********************************************************************
  400 CONTINUE
      DO 410 I= 1,N
      Z(I)= R(I)
  410 CONTINUE
      CALL DGTSL(NB,N0,H,Z)
      GOTO 700
C***********************************************************************
C     CHAPTER II.3: PRECONDITIONER = INNER TRIDIAGONAL                 *
C***********************************************************************
  500 CONTINUE
      CALL DGINSL(N,N0,N1,NB,H,R,Z,INFO,IFAIL)
      GOTO 700
C***********************************************************************
C                                                                      *
C     REGULAR EXIT                                                     *
C                                                                      *
C***********************************************************************
  700 CONTINUE
      RETURN
C***********************************************************************
C                                                                      *
C     CHAPTER VI.: ERROR EXITS                                         *
C                                                                      *
C***********************************************************************
C---- NOT ENOUGH STORAGE FOR PRECONDITIONER
  910 CONTINUE
      IFAIL=-8
      RETURN
C***********************************************************************
C                                                                      *
C     END OF LINSOL                                                    *
C                                                                      *
C***********************************************************************
      END
      SUBROUTINE DGINSL(NV,N,N1,NB,A,X,Y,INFO,IFAIL)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *       SOLUTION OF THE EQUATION L * U * Y = X              *
C     *                      U.MAAS                               *
C     *                    10/19/1990                             *
C     *           LAST CHANGE: U.MAAS 10/19/1990                  *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C                                                                      *
C     INPUT:
C                                                                      *
C     N           DIMENSION OF SUB MATRIX                              *
C     NB          NUMBER OF BLOCKS                                     *
C     A           MATRIX                                               *
C     Z(N,1)                                                           *
C     X(N,1) SOLUTION VECTOR                                           *
C     Y(N,1) WORK ARRAY                                                C
C     OUTPUT:                                                          C
C                                                                      C
C     Z(N,1)  SOLUTION VECTOR                                          C
C***********************************************************************
C***********************************************************************
C        1           1 1 1                                             C
C        |           | | |                                             C
C      1 Q           U V W                                             C
C        1 Q           U V W                                           C
C          1 Q           U V W                                         C
C            1 Q           U V W                                       C
C              1 Q           U V W                                     C
C                1 Q           U V W                                   C
C                  1 Q           U V W                                 C
C                    1 Q           U V W                               C
C                      1 Q           U V W                             C
C                        1 Q           U V W                           C
C                          1 Q           U V W                         C
C                            1 Q           U V W                       C
C                              1 Q           U V W                     C
C                                1 Q           U V W                   C
C                                  1 Q           U V W                 C
C                                    1 Q           U V W               C
C                                      1 Q           U V W - N-N1-1    C
C                                        1 Q           U V - N-N1      C
C                                          1 Q           U - N-N1+1    C
C                                            1 Q                       C
C                                              1 Q                     C
C     STORAGE AND STRUCTURE OF MATRIX U          1 Q                   C
C                                                  1 Q                 C
C                                                    1 Q               C
C                                                      1 Q---N-1       C
C                                                        1---N         C
C                                                                      C
C                                                                      C
C                                                                      C
C***********************************************************************
C      1                                                               C
C      |                                                               C
C      P                                                               C
C   2--O P                                                             C
C        O P                  STORAGE AND STRUCTURE OF MATRIX L        C
C          O P                                                         C
C            O P                                                       C
C              O P                                                     C
C                O P                                                   C
C   1--C           O P                                                 C
C   1--B C           O P                                               C
C   1--A B C           O P                                             C
C        A B C           O P                                           C
C          A B C           O P                                         C
C            A B C           O P                                       C
C              A B C           O P                                     C
C                A B C           O P                                   C
C                  A B C           O P                                 C
C                    A B C           O P                               C
C                      A B C           O P                             C
C                        A B C           O P                           C
C                          A B C           O P                         C
C                            A B C           O P                       C
C                              A B C           O P                     C
C                                A B C           O P                   C
C                                  A B C           O P                 C
C                                    A B C           O P               C
C                                      A B C           O P---N         C
C                                      | | |           |               C
C                                      | | N-N1+1      N               C
C                                      | N-N1                          C
C                                      N-N1-1                          C
C                                                                      C
C***********************************************************************
C
C                                                                      C
C***********************************************************************
C     STORAGE ORGANIZATION                                             C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION A(N,N,*),X(N,NB),Y(N,NB)
C***********************************************************************
C
C     INITIALIZE (NEW STORAGE FORM)
C
C***********************************************************************
      NBA =  NB - N1 - 1
      NBB =  NB - N1
      NBC =  NB - N1 + 1
      NBO =  NB
      NBP =  NB
      NBQ =  NB
      NBU =  NB - N1 + 1
      NBV =  NB - N1
      NBW =  NB - N1 - 1
      INA = 0
      INB = INA + NBA
      INC = INB + NBB
      INO = INC + NBC
      INP = INO + NBO
      INQ = INP + NBP
      INU = INQ + NBQ
      INV = INU + NBU
      INW = INV + NBV
C***********************************************************************
C
C***********************************************************************
      NQ=N*N
      IDUM=INO+1
C***********************************************************************
C     CALCULATE RIGHT HAND SIDE                                        *
C***********************************************************************
      DO 110 J=1,N
      DO 110 I=1,NB
      Y(J,I)=X(J,I)
  110 CONTINUE
C***********************************************************************
C
C     CALCULATE  L X'        = Z                                       C
C
C***********************************************************************
C***********************************************************************
C     CALCULATE X(1)                                                   *
C***********************************************************************
      I=1
C---- STORE Z IN FIRST BLOCK OF O-ROW (THIS IS A DUMMY BLOCK)
      DO 315 J=1,N
      A(J,1,IDUM)=Y(J,I)
  315 CONTINUE
      CALL DGQMMV(N,A(1,1,INP+I      ),A(1,1,IDUM),Y(1,I),0)
C***********************************************************************
C     CALCULATE X(2),...,X(N1-1)       (CONSIDERING O)                 *
C***********************************************************************
      N1MI1=N1-1
      DO 320 I=2,N1MI1
      DO 325 J=1,N
      A(J,1,IDUM)=Y(J,I)
  325 CONTINUE
      CALL DGQMMV(N,A(1,1,INO+I      ),Y(1,I-1   ),A(1,1,IDUM),-1)
      CALL DGQMMV(N,A(1,1,INP+I      ),A(1,1,IDUM),Y(1,I),0)
  320 CONTINUE
C***********************************************************************
C     CALCULATE X(N1)                  (CONSIDERING O,C)               *
C***********************************************************************
      I=N1
C---- STORE Y IN FIRST BLOCK OF O-ROW (THIS IS A DUMMY BLOCK)
      IDUM=INO+1
      DO 330 J=1,N
      A(J,1,IDUM)=Y(J,I)
  330 CONTINUE
      CALL DGQMMV(N,A(1,1,INO+I      ),Y(1,I-1   ),A(1,1,IDUM),-1)
      CALL DGQMMV(N,A(1,1,INC+I-N1+1 ),Y(1,I-N1+1),A(1,1,IDUM),-1)
      CALL DGQMMV(N,A(1,1,INP+I      ),A(1,1,IDUM),Y(1,I),0)
C***********************************************************************
C     CALCULATE X(N1+1)                (CONSIDERING O,C,B)             *
C***********************************************************************
      I=N1+1
C---- STORE Y IN FIRST BLOCK OF O-ROW (THIS IS A DUMMY BLOCK)
      IDUM=INO+1
      DO 335 J=1,N
      A(J,1,IDUM)=Y(J,I)
  335 CONTINUE
      CALL DGQMMV(N,A(1,1,INO+I      ),Y(1,I-1   ),A(1,1,IDUM),-1)
      CALL DGQMMV(N,A(1,1,INC+I-N1+1 ),Y(1,I-N1+1),A(1,1,IDUM),-1)
      CALL DGQMMV(N,A(1,1,INB+I-N1   ),Y(1,I-N1  ),A(1,1,IDUM),-1)
      CALL DGQMMV(N,A(1,1,INP+I      ),A(1,1,IDUM),Y(1,I),0)
C***********************************************************************
C     CALCULATE X(N1+2),...,X(NB)      (CONSIDERING O,C,B,A)           *
C***********************************************************************
      N1PLU2=N1+2
      DO 350 I=N1PLU2,NB
C---- STORE Z IN FIRST BLOCK OF O-ROW (THIS IS A DUMMY BLOCK)
      IDUM=INO+1
      DO 340 J=1,N
      A(J,1,IDUM)=Y(J,I)
  340 CONTINUE
      CALL DGQMMV(N,A(1,1,INO+I      ),Y(1,I-1   ),A(1,1,IDUM),-1)
      CALL DGQMMV(N,A(1,1,INC+I-N1+1 ),Y(1,I-N1+1),A(1,1,IDUM),-1)
      CALL DGQMMV(N,A(1,1,INB+I-N1   ),Y(1,I-N1  ),A(1,1,IDUM),-1)
      CALL DGQMMV(N,A(1,1,INA+I-N1-1 ),Y(1,I-N1-1),A(1,1,IDUM),-1)
      CALL DGQMMV(N,A(1,1,INP+I      ),A(1,1,IDUM),Y(1,I),0)
  350 CONTINUE
C***********************************************************************
C     CALCULATE  U * X = X'
C***********************************************************************
C***********************************************************************
C     CALCULATE X(NB)                                                  *
C***********************************************************************
C---- X(NB) REMAINS UNCHANGED
C***********************************************************************
C     CALCULATE X(NB-N1+1)                                             *
C***********************************************************************
      I=NB
      ILA1=NB-N1+2
      ILA2=NB-1
      DO 230 III=ILA1,ILA2
      I=I-1
      CALL DGQMMV(N,A(1,1,INQ+I ),Y(1,I+1   ),Y(1,I),-1)
  230 CONTINUE
      IF(I.NE.NB-N1+2) WRITE(6,*) ' ERROR 1 '
C***********************************************************************
C     CALCULATE X(NB-N1+1)                                             *
C***********************************************************************
      I=NB-N1+1
      CALL DGQMMV(N,A(1,1,INQ+I ),Y(1,I+1   ),Y(1,I),-1)
      CALL DGQMMV(N,A(1,1,INU+I ),Y(1,I+N1-1),Y(1,I),-1)
C***********************************************************************
C     CALCULATE X(NB-N1)                                               *
C***********************************************************************
      I=NB-N1
      CALL DGQMMV(N,A(1,1,INQ+I ),Y(1,I+1   ),Y(1,I),-1)
      CALL DGQMMV(N,A(1,1,INU+I ),Y(1,I+N1-1),Y(1,I),-1)
      CALL DGQMMV(N,A(1,1,INV+I ),Y(1,I+N1  ),Y(1,I),-1)
C***********************************************************************
C     CALCULATE X(NB-N1-1),...,X(1)                                    *
C***********************************************************************
      I=NB-N1
      ILA2=NB-N1-1
      DO 235 III=1,ILA2
      I=I-1
      CALL DGQMMV(N,A(1,1,INQ+I ),Y(1,I+1   ),Y(1,I),-1)
      CALL DGQMMV(N,A(1,1,INU+I ),Y(1,I+N1-1),Y(1,I),-1)
      CALL DGQMMV(N,A(1,1,INV+I ),Y(1,I+N1  ),Y(1,I),-1)
      CALL DGQMMV(N,A(1,1,INW+I ),Y(1,I+N1+1),Y(1,I),-1)
  235 CONTINUE
      IF(I.NE.1) WRITE(6,*) ' ERROR 2 '
C
C***********************************************************************
C     SOLUTION EXIT
C***********************************************************************
  600 CONTINUE
      RETURN
C***********************************************************************
C     ERROR EXITS                                                     C
C***********************************************************************
C----- LESS THAN 3 SUBMATRICES
C9100 IFAIL= 9
C     RETURN
C9200 IFAIL= 1
C     RETURN
C 950 IFAIL= 2
C     RETURN
C***********************************************************************
C     END OF XXXXX
C***********************************************************************
      END
      SUBROUTINE ITZIB2(ITASK,N,MAXIT,EPS,XW,MULJAC,PRECON,
     $                 NELT,A,IA,SOL,DEL,RHS,IOPT,
     $                 LRWK,RWK,NRW,LIWK,IWK,NIW,
     $                 LIWKU,IWKU,LRWKU,RWKU,
     $                 DELNRM,SOLNRM,DIFNRM,IERR)
C*    BEGIN PROLOGUE ITZIB2
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INTEGER N,MAXIT
      DOUBLE PRECISION EPS,XW(*)
      EXTERNAL MULJAC,PRECON
      INTEGER NELT
      DOUBLE PRECISION A(NELT)
      INTEGER IA(*)
      DOUBLE PRECISION SOL(N),DEL(N),RHS(N)
      INTEGER IOPT(10)
      INTEGER LRWK
      DOUBLE PRECISION RWK(LRWK)
      INTEGER NRW
      INTEGER LIWK
      INTEGER IWK(LIWK)
      INTEGER NIW
      INTEGER LIWKU
      INTEGER IWKU(LIWKU)
      INTEGER LRWKU
      DOUBLE PRECISION RWKU(LRWKU)
      DOUBLE PRECISION DELNRM,SOLNRM,DIFNRM
      INTEGER IERR
C     ____________________________________________________________
C
C*    TITLE
C
C     GOOD BROYDEN -
C     ITERATIVE SOLUTION OF A LINEAR SYSTEM (IMPLICITLY GIVEN MATRIX
C     BY SUBROUTINE MULJAC - COMPUTES A*VECTOR)
C
C*    METHOD            ITZIB2
C                       GOOD BROYDEN
C*    AUTHORS           P. DEUFLHARD, R. FREUND, A. WALTER
C*    PROGRAM BY        L. WEIMANN
C*    VERSION           1.0
C*    ENVIRONMENT       FORTRAN 77
C                       DOUBLE PRECISION
C*    LATEST REVISION   AUGUST 1990
C*    CATEGORY          ??? - LARGE SYSTEMS OF LINEAR EQUATIONS
C*    LIBRARY           CODELIB
C*    COPYRIGHT         KONRAD ZUSE ZENTRUM FUER
C                       INFORMATIONSTECHNIK BERLIN
C                       HEILBRONNER STR. 10, D-1000 BERLIN 31
C                       PHONE 030/89604-0, TELEFAX 030/89604-125
C
C     ____________________________________________________________
C
C*    REFERENCES:
C     ===========
C
C       (1) P. DEUFLHARD, R. FREUND, A. WALTER:
C           FAST SECANT METHODS FOR THE ITERATIVE SOLUTION OF
C           LARGE NONSYMMETRIC LINEAR SYSTEMS.
C           ZIB, PREPRINT SC 90-5 (JULY 1990)
C
C
C*    PARAMETERS LIST DESCRIPTION
C     ===========================
C
C*    EXTERNAL SUBROUTINES
C     ====================
C
C       MULJAC( N, X, Y, NELT, A, IA, ISYM )
C                EXT    JACOBIAN * VECTOR PRODUCT SUBROUTINE
C                       (PARAMETERS LIST ACCORDING TO SLAP CONVENTION
C                        FOR MATVEC)
C         N        INT    THE NUMBER OF VECTOR COMPONENTS
C         X(N)     DOUBLE THE VECTOR TO BE MULTIPLIED BY THE JACOBIAN
C                         (INPUT)
C         Y(N)     DOUBLE THE ARRAY TO GET THE RESULT VECTOR
C                         JACOBIAN * X (OUTPUT)
C         NELT     INT    THE NUMBER OF (NONZERO) JACOBIAN ELEMENTS
C                         CONTAINED IN  A
C         IA(1)    INT    INFORMATION FOR  THE JACOBIAN
C         A(1)     DOUBLE THE REAL VALUES OF THE JACOBIAN
C         ISYM     INT    DUMMY - SYMMETRY FLAG FOR SLAP COMPATIBILITY
C
C
C       PRECON(N, R, Z, NELT, IA,  A, ISYM, RWKU, IWKU)
C                EXT    PRECONDITIONING SOLVER SUBROUTINE FOR SYSTEM
C                       M * Z = R (PARAMETERS LIST ACCORDING TO
C                       SLAP CONVENTION FOR MSOLVE)
C         N        INT    THE NUMBER OF VECTOR COMPONENTS
C         R(N)     DOUBLE THE RIGHT HAND SIDE OF THE SYSTEM (INPUT)
C         Z(N)     DOUBLE THE ARRAY TO GET THE SOLUTION VECTOR (OUTPUT)
C         NELT     INT    THE NUMBER OF JACOBIAN ELEMENTS
C         IA(1)    INT    THE ROW INDICES OF THE JACOBIAN
C         A(1)     DOUBLE THE REAL VALUES OF THE JACOBIAN
C         ISYM     INT    DUMMY - SYMMETRY FLAG FOR SLAP COMPATIBILITY
C         RWKU(*)  DOUBLE USER WORKSPACE WHICH HOLDS NECESSARY PRECON-
C                         DITIONING INFORMATION AND /OR WORKSPACE
C                         TO PRECON.
C         IWKU(*)  INT    USER WORKSPACE (SAME PURPOSE AS RWKU(*))
C
C*    INPUT PARAMETERS (* MARKS TRANSIENT PARAMETERS)
C     ===============================================
C
C     N          INT     NUMBER OF VARIABLES AND NONLINEAR EQUATIONS
C   * MAXIT      INT     MAXIMUM NUMBER OF ITERATIONS ALLOWED
C     EPS        DOUBLE  REQUIRED (RELATIVE) PRECISION
C     XW(1)      DOUBLE  SCALING VECTOR
C     NELT       INT     NUMBER OF ENTRIES IN A
C                        STORED IN SLAP FORMAT IN THE ARRAYS A, IA, JA
C     A(1)       DOUBLE  JACOBIAN NONZERO REAL VALUES
C     IA(1)      INT     THE CORRESPONDING ROW INDICES TO THE REAL
C                        VALUES STORED IN A
C   * SOL(N)     DOUBLE  THE ITERATION STARTING VECTOR
C     RHS(N)     DOUBLE  THE RIGHT HAND SIDE VECTOR
C     IOPT(10)   INT     OPTIONS ARRAY (SEE BELOW FOR USAGE)
C
C*    OUTPUT PARAMETERS
C     =================
C
C     MAXIT      INT     NUMBER OF ITERATIONS DONE
C     EPS        DOUBLE  ACHIEVED (RELATIVE) PRECISION
C     SOL(N)     DOUBLE  THE SOLUTION OR FINAL ITERATE VECTOR
C     DEL(N)     DOUBLE  THE DIFFERENCE SOLFINAL(N)-SOLSTART(N)
C     DELNRM     DOUBLE  THE NORM2 OF SOLFINAL(N)-SOLSTART(N)
C     SOLNRM     DOUBLE  THE NORM2 OF SOLFINAL(N)
C     DIFNRM     DOUBLE  THE NORM2 OF THE FINAL CORRECTION OF SOL(N)
C     IERR       INT     ERROR CODE - A ZERO RETURN SIGNALS OK.
C                        SEE BELOW FOR NONZERO ERROR CODES.
C
C*    WORKSPACE PARAMETERS
C     ====================
C
C     LRWK           INT    DECLARED DIMENSION OF REAL WORKSPACE
C                           REQUIRED MINIMUM: KMAX*(N+2)+4*N+7
C     RWK(LRWK)      DOUBLE REAL WORKSPACE
C     NRW            INT    (OUT) REAL WORKSPACE USED
C     LIWK           INT    DECLARED DIMENSION OF INTEGER WORKSPACE
C                           (DUMMY)
C     IWK(LIWK)      INT    INTEGER WORKSPACE (DUMMY)
C     NIW            INT    (OUT) INTEGER WORKSPACE USED (DUMMY)
C     LIWKP          INT    DECLARED DIMENSION OF THE STANDARD
C                           PRECONDITIONER INTEGER WORKSPACE.
C     IWKP(LIWKP)    INT    STANDARD PRECONDITIONER INTEGER WORKSPACE
C     LRWKP          INT    DECLARED DIMENSION OF STANDARD
C                           PRECONDITIONER REAL WORKSPACE.
C     RWKP(LRWKP)    DOUBLE STANDARD PRECONDITIONER REAL WORKSPACE
C     LIWKU          INT    DECLARED DIMENSION OF USER FUNCTION
C                           INTEGER WORKSPACE.
C     IWKU(LIWKU)    INT    USER FUNCTION INTEGER WORKSPACE
C     LRWKU          INT    DECLARED DIMENSION OF USER FUNCTION
C                           REAL WORKSPACE.
C     RWKU(LRWKU)    DOUBLE USER FUNCTION REAL WORKSPACE
C
C     USAGE OF OPTIONS ARRAY IOPT:
C     ============================
C
C     POS.    DESCRIPTION
C       1     PRINT PARAMETER FOR ITERATIVE LINEAR SOLVER
C             TO PRINT THE ITERATION MONITOR AND MESSAGES, IT MUST BE
C             SET TO A POSITIVE VALUE AS FOLLWOS:
C             MUST BE SET TO ITDEL*100+IPUNIT, WHERE
C             ITDEL IS THE DELTA BETWEEN TWO ITERATES TO BE
C             PRINTED, AND IPUNIT IS THE PRINT UNIT
C             TO PRINT ONLY ERROR TERMINATION AND WARNING MESSAGES, IT
C             MUST BE SET TO ZERO.
C             TO SUPPRESS ANY PRINTING, IT MUST BE SET TO A NEGATIVE
C             NUMBER.
C       2     TYPE OF STOP CRITERIUM TO BE USED (THE DEFINITION OF
C             CORRS AND CORRDS DEPENDS ON THE SETTING OF IOPT(9)):
C             1,2: STOP, IF CORRS < EPS
C             3  : STOP, IF CORRS < EPS OR CORRDS < 0.25
C             ON OUTPUT, THIS FIELD CONTAINS THE INFORMATION, WHICH
C             CRITERIUM LEADS TO CONVERGENCE STOP (IF ON INPUT 3):
C             1: CORRS < EPS HAS BEEN SATISFIED
C             2: CORRDS < 0.25 HAS BEEN SATISFIED
C       3     PRINT MONITOR OPTION:
C             0: STANDARD PRINT MONITOR
C             1: TEST PRINT MONITOR FOR SPECIAL PURPOSES
C       4..5  RESERVED
C       6     MAXIMUM NUMBER OF LATEST ITERATES TO BE SOLVER BY THE
C             LINEAR SOLVER (= LENGTH OF WINDOW).
C       7     PLOT INFORMATION UNIT NUMBER
C       8..10 RESERVED
C
C     ERROR CODES (STORED TO IERR):
C     =============================
C
C      -1     ITERATION NUMBER LIMIT (AS SET BY MAXIT) EXCEEDED
C      -2     ITERATION DIVERGES: THE ERROR ESTIMATE EXCEEDS THE GREAT
C             VALUE
C      -3     ITERATION DIVERGES: BAD TAU VALUES AND NORMS OF
C             CORRECTIONS OSCILLATE IN A TWO-ITERATES-CYCLE.
C      -4     ITERATION CANNOT CONTINUE: BAD TAU VALUE IMMEDIATE
C             AT START OR RESTART.
C      -10    INSUFFICIENT WORKSPACE
C
C     ____________________________________________________________
C*    END PROLOGUE
C     PARAMETER (L=3)
      INTEGER MINL, MAXLIN, IFINI, IPUNIT, IPRINT, KMAX,
     $        L1, L2, L4, L5, L6, L7, L8, L9, L10
      EXTERNAL ITZIBI
      L= 3
      IERR  = 0
      IFINI = IOPT(2)
      MAXLIN = IOPT(6)
      IPUNIT=0
      IPRINT=IOPT(1)
      IF (IPRINT.GT.0) THEN
        IPUNIT=MOD(IPRINT,100)
        IPRINT=(IPRINT-IPUNIT)/100
      ENDIF
C
      MINL=10
      KMAX=MINL
      NRW = (N+2)*MINL+4*N+L+4
      IF(NRW.LT.LRWK)
     $  KMAX=IDINT( DBLE(FLOAT(LRWK-4*N-L-4)) / DBLE(FLOAT(N+2)) )
      IF (MAXLIN.GT.0) KMAX=MIN0(KMAX,MAXLIN)
C***********************************************************************
C     BUILD UP REAL WORK SPACE
C***********************************************************************
      L1  = 1
      L2  = L1 + N*KMAX
      L4  = L2 + N
      L5  = L4 + N
      L6  = L5 + N
      L7  = L6 + N
      L8  = L7 + KMAX
      L9  = L8 + KMAX
      L10 = L9 + L
      L11 = L10 + 4
      NRW = L11 - 1
      IF (NRW.GT.LRWK) GOTO 910
C***********************************************************************
C     BUILD UP INTEGER WORK SPACE
C***********************************************************************
      NIW = 1
      IF (NIW.GT.LIWK) GOTO 910
C***********************************************************************
C     BUILD UP INTEGER WORK SPACE
C***********************************************************************
      IF (IPRINT.GT.0) THEN
        WRITE(IPUNIT,8010) KMAX
8010    FORMAT(6X,'MAXDELTA = ',I5)
      ENDIF
      CALL ITZIBI(ITASK,N,MAXIT,EPS,XW,MULJAC,PRECON,
     $            NELT,A,IA,SOL,DEL,RHS,IOPT,.FALSE.,IERR,
     $            KMAX,RWK(L1),RWK(L2),RWK(L4),RWK(L5),
     $            RWK(L6),RWK(L7),RWK(L8),IOPT(9),IOPT(10),
     $            L,RWK(L9),RWK(L10),RWK(L10+1),RWK(L10+2),RWK(L10+3),
     $            LIWKU,IWKU,LRWKU,RWKU,DELNRM,SOLNRM,DIFNRM)
C
C     END OF SUBROUTINE ITZIB2
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
C---- WORK SPACE EXHAUSTED
  910 CONTINUE
      IERR = -10
      IF(NIW.GT.LIWK) WRITE(IPUNIT,911) LIWK,NIW
  911 FORMAT(1X,' +++++ INTEGER WORK-SPACE EXHAUSTED IN ITZIB +++++',/,
     1       1X,' GIVEN SPACE:',I8,'     NEEDED SPACE:',I8)
      IF(NRW.GT.LRWK) WRITE(IPUNIT,912) LRWK,NRW
  912 FORMAT(1X,' +++++ REAL WORK-SPACE EXHAUSTED IN ITZIB +++++',/,
     1       1X,' GIVEN SPACE:',I8,'     NEEDED SPACE:',I8)
      GOTO 999
  999 CONTINUE
      RETURN
      END
C
      SUBROUTINE ITZIBI(ITASK,N,MAXIT,EPS,XW,MULJAC,PRECON,NELT,A,IA,
     $                 SOL,DEL,RHS,IOPT,QCONT,IERR,KMAX,DELTA,DELTAK,
     $                 Q,Z,SITER,T,SIGMA,
     $                 ITER,K,L,SIGH,SIGMAK,TAUN1,TAUN2,SOLNRA,
     $                 LIWKP,IWKP,LRWKP,RWKP,
     $                 DELNRM,SOLNRM,DIFNRM)
C*    BEGIN PROLOGUE ITZIBI
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INTEGER N,MAXIT
      DOUBLE PRECISION EPS,XW(*)
      EXTERNAL MULJAC,PRECON
      INTEGER NELT
      DOUBLE PRECISION A(NELT)
      INTEGER IA(*)
      INTEGER IOPT(10)
      LOGICAL QCONT
      INTEGER IERR,KMAX
      DOUBLE PRECISION SOL(N),DEL(N),RHS(N),DELTA(N,KMAX),DELTAK(N),
     $                 Q(N),Z(N),SITER(N),T(KMAX),
     $                 SIGMA(KMAX)
      INTEGER ITER,K,L
      DOUBLE PRECISION SIGH(L),SIGMAK,TAUN1,TAUN2,SOLNRA
      INTEGER LIWKP
      INTEGER IWKP(LIWKP)
      INTEGER LRWKP
      DOUBLE PRECISION RWKP(LRWKP)
      DOUBLE PRECISION DELNRM,SOLNRM,DIFNRM
C     ____________________________________________________________
C
C*    INTERNAL CORE ROUTINE
C
C     ITERATIVE SOLUTION OF A LINEAR SYSTEM (IMPLICIT GIVEN MATRIX
C     BY SUBROUTINE MULJAC - COMPUTES A*VECTOR)
C
C     ____________________________________________________________
C*    END PROLOGUE
      DOUBLE PRECISION ZERO,ONE,GREAT,RHOS,TAUMIN,TAUMAX,TAUEQU
      EXTERNAL SPRODI
      INTEGER I,J,K1,IFINI,IPRINT,ISYM,IPUNIT,IPLOT,ITERM
      DOUBLE PRECISION DN,FAKTOR,FAKT1,DIFLIM,
     $                 SPRODI,SIGMAP,GAMMA,TAU,TK,
     $                 SIGQN,DIFQN,
     $                 TKRMI,TKRMA
      DOUBLE PRECISION SIGNEW,SIGQ1
      LOGICAL QSTOP,QSTOPK
C*    BEGIN
C
C  INITIATION
C------------
C
C     PARAMETER( ZERO=0.0D0, ONE = 1.0D0, GREAT=1.0D+35 )
      DATA  ZERO/0.0D0/, ONE/1.0D0/, GREAT/1.0D+35/
      DATA  TAUMIN/1.0D-8/, TAUMAX/1.0D2/ , TAUEQU/1.0D-2/
      DATA  RHOS/4.0D0/
C     SECURITY FACTOR RHOS FOR TERMINATION OF GOOD BROYDEN (.GE. 1)
      IERR=10000
      ISYM=0
      IPROPT=IOPT(3)
      DIFLIM=1.0D6
CV1:
CV1:      TKRMI=TAUMIN
      TKRMA=TAUMAX
CV1:
      TKRMI=ONE
CV1:      TKRMA=ONE
CV1:
      SIGQN=GREAT
      DIFQN=GREAT
      DIFNRM=GREAT
      SOLNRM=GREAT
      TAU=GREAT
      QSTOP = .FALSE.
      QSTOPK = .FALSE.
      IPRINT=IOPT(1)
      IFINI =IOPT(2)
      IPLOT=IOPT(7)
      IF (IPRINT.GT.0) THEN
        IPUNIT=MOD(IPRINT,100)
        IPRINT=(IPRINT-IPUNIT)/100
      ENDIF
      DN = DBLE(FLOAT(N))
C
C  INITIAL PREPARATIONS
C----------------------
C***********************************************************************
C  ITASK DECIDES ABOUT INITIALIZATION
C***********************************************************************
C***********************************************************************
C  FOR ITASK = ZERO SET DEL TO ZERO AND COMPUTE INTIAL SOL FROM PRECOND.
C***********************************************************************
      IF(ITASK.EQ.0) THEN
      DO 222 I=1,N
        DEL(I)=0.D0
222   CONTINUE
      CALL PRECON(N,RHS,SOL,NELT,A,IA,ISYM,LRWKP,RWKP,LIWKP,IWKP)
      ENDIF
C***********************************************************************
C  FOR ITASK = 1 SET DEL TO ZERO
C***********************************************************************
      IF(ITASK.EQ.1) THEN
      DO 322 I=1,N
        DEL(I)=0.D0
322   CONTINUE
      ENDIF
C***********************************************************************
C
C***********************************************************************
      DO 2 I=1,N
        SITER(I)=SOL(I)
        SOL(I)=SOL(I)-DEL(I)
2     CONTINUE
C
C     CONTINUATION ENTRY
C
      IF (QCONT) THEN
        SOLNRM=SOLNRA
        GOTO 20
      ENDIF
C
C   INITIATION FOR NEW ITERATION RUN
C   --------------------------------
C
      TAUN1=GREAT
      TAUN2=GREAT
C
C  INITIAL PRINT
C
      IF (IPRINT.GT.0) THEN
        SOLNRM = DSQRT( SPRODI(N, SOL, SOL, XW)/DN )
        IF(IPROPT.EQ.0) WRITE(IPUNIT,810)
        IF(IPROPT.EQ.1) WRITE(IPUNIT,820)
      ENDIF
      ITER = 0
C
C  START / ENTRY FOR RESTART OF ITERATION
C----------------------------------------
C
3     CONTINUE
C
C     K := 0
      K = 0
C
C------------------------------------------------------------
C     --- R0 := B-A*X0   ===  Z := RHS-A*SITER ---
C     --- DELTA0 := H0*R0  ===  SOLVE A(PRECON)*DELTAK = Z ---
C     --- SIGMA0 := DELTA0(T)*DELTA0 === SIGMAK := DELTAK(T)*DELTAK ---
C------------------------------------------------------------
C
      CALL MULJAC(N, SITER, Z, NELT, A,IA, ISYM)
C
      DO 10 I=1,N
        Z(I) = RHS(I) - Z(I)
10    CONTINUE
C
      CALL PRECON(N,Z,DELTAK,NELT,A,IA,ISYM,LRWKP,RWKP,LIWKP,IWKP)
C
      SIGMAK = SPRODI(N, DELTAK, DELTAK, XW)
      IF (SIGMAK.EQ.ZERO) THEN
        EPS=ZERO
        GOTO 1000
      ENDIF
C
      CALL ITZMID(L,ITER,SIGH,SIGMAK,SIGQN,.FALSE.)
C
C  MAIN ITERATION LOOP
C---------------------
C
20    IF ( .NOT. QSTOPK .OR. .NOT. QSTOP ) THEN
C
        IF (ITER.GE.MAXIT) THEN
          IERR = -1
          GOTO 998
        ENDIF
        K1=K+1
C
C------------------------------------------------------------
C    --- QK := A*DELTAK  ===  Q := A*DELTAK ---
C    --- Z0QUER := H0*QK  ===  SOLVE A(PRECON)*Z = Q ---
C------------------------------------------------------------
C
        CALL MULJAC(N, DELTAK, Q, NELT, A, IA, ISYM)
C
        CALL PRECON(N,Q,Z,NELT,A,IA,ISYM,LRWKP,RWKP,LIWKP,IWKP)
C
C  UPDATE LOOP
C-------------
C
        KM1= K-1
        IF(KM1.GE.1) THEN
        DO 100 I=1,KM1
          FAKTOR = SPRODI(N, DELTA(1,I), Z, XW) / SIGMA(I)
          FAKT1 = ONE-T(I)
          DO 110 J=1,N
            Z(J) = Z(J) + FAKTOR * (DELTA(J,I+1) - FAKT1*DELTA(J,I))
110       CONTINUE
100     CONTINUE
        ENDIF
        IF (K.NE.0) THEN
          FAKTOR = SPRODI(N, DELTA(1,K), Z, XW) / SIGMA(K)
          FAKT1 = ONE-T(K)
          DO 120 J=1,N
            Z(J) = Z(J) + FAKTOR * (DELTAK(J) - FAKT1*DELTA(J,K))
120       CONTINUE
        ENDIF
C
C----------------------------------------------------------
C    --- ZK := ZQUERK ===  Z NOW CORRESPONDS TO ZK ---
C    --- GAMMAK := DELTAK(T)*ZK ---
C    --- TAUK := SIGMAK/GAMMAK ---
C----------------------------------------------------------
C
        GAMMA = SPRODI(N, DELTAK, Z, XW)
        IF (GAMMA.NE.ZERO) TAU = SIGMAK / GAMMA
        TK = TAU
C
C  CHECK FOR RESTART CONDITION
C-----------------------------
C
        IF (GAMMA.EQ.ZERO .OR. TAU.LT.TAUMIN .OR. TAU.GT.TAUMAX) THEN
          IF (K.EQ.1 .AND. GAMMA.NE.ZERO ) THEN
            IF ( DABS(TAUN2-TAU) .LT. TAUEQU*DABS(TAU) ) THEN
              IERR=-3
              GOTO 998
            ENDIF
            TAUN2 = TAUN1
            TAUN1 = TAU
          ENDIF
          IF(IPRINT.GT.0.AND.GAMMA.NE.ZERO) WRITE(IPUNIT,830) TAU,ITER,K
          IF(IPRINT.GE.0.AND.GAMMA.EQ.ZERO) WRITE(IPUNIT,835)
          IF (K.EQ.0) THEN
            IF (GAMMA.EQ.ZERO) THEN
              IF (IPRINT.GE.0)  WRITE(IPUNIT,840)
              IERR = -4
              GOTO 998
            ENDIF
            IF (IPRINT.GT.0) WRITE(IPUNIT,850) TAU,ITER,K
            IF (TAU.LT.TAUMIN) TK = TKRMI
            IF (TAU.GT.TAUMAX) TK = TKRMA
            WRITE(IPUNIT,860) TK
          ELSE
            GOTO 9990
          ENDIF
        ENDIF
C
C----------------------------------------------------------
C    --- X(K+1) := XK + TK*DELTAK ---
C    --- DELTA(K+1) := DELTAK - TAUK*ZK (IF TK=TAUK) ---
C                        - OR -
C    --- DELTA(K+1) := (1-TK+TAUK)*DELTAK - TAUK*ZK (IF TK<>TAUK)
C    --- SIGMA(K+1) := DELTA(K+1)(T)*DELTA(K+1) ---
C----------------------------------------------------------
C
C  UPDATE ITERATE
C----------------
C
        DO 130 J=1,N
          DEL(J) = DEL(J) + TK*DELTAK(J)
          SITER(J) = SOL(J) + DEL(J)
130     CONTINUE
C
C  COMPUTE NORMS FOR CONVERGE CHECK
C
        SOLNRA = SOLNRM
        SOLNRM = DSQRT( SPRODI(N, SITER, SITER, XW)/DN )
        DELNRM = DSQRT (SPRODI(N, DEL, DEL, XW)/DN )
        DIFNRM = DABS(TK)*DSQRT(SIGMAK/DN)
C
C  SAVE INFORMATION TO PERFORM NEXT UPDATE LOOP
C
        IF (K1.LE.KMAX) THEN
          DO 6 I=1,N
            DELTA(I,K1)=DELTAK(I)
6         CONTINUE
          SIGMA(K1) = SIGMAK
          T(K1) = TK
        ENDIF
C
C  NEW DELTA
C
         IF (TK.EQ.TAU) THEN
           DO 140 J=1,N
             DELTAK(J) = DELTAK(J) -  TAU * Z(J)
140        CONTINUE
         ELSE
           FAKTOR = ONE - TK + TAU
           DO 145 J=1,N
             DELTAK(J) = FAKTOR*DELTAK(J) -  TAU * Z(J)
145        CONTINUE
         ENDIF
C
C  NEW SIGMA
C
        SIGMAP = SIGMAK
        SIGMAK = SPRODI(N, DELTAK, DELTAK, XW)
        SIGNEW = DSQRT(SIGMAK/DN)
C
        CALL ITZMID(L,ITER,SIGH,SIGMAK,SIGQN,.TRUE.)
C
        SIGQ1=DSQRT(SIGQN/DN)
C
C
C       WRITE GRAPHICS DATA
C
        IF (IPLOT.GT.0)
     $   WRITE(IPLOT,890) ITER,K,DIFNRM,SIGNEW,SIGQ1,SIGQ1,SIGQ1,SOLNRM
C
        DIFQN=RHOS*SIGQ1
C
C       --- PRINT MONITOR ---
C
        IF (IPRINT.GT.0) THEN
          IF (MOD(K,IPRINT).EQ.0) THEN
            IF (IPROPT.EQ.0)
     $          WRITE(IPUNIT,870) ITER, DIFNRM, SOLNRA, DIFQN, SOLNRM,
     $                              TAU
            IF (IPROPT.EQ.1)
     $          WRITE(IPUNIT,870) ITER, DIFNRM, DELNRM, SOLNRA,
     $                              DIFQN, DELNRM, SOLNRM, TAU
          ENDIF
        ENDIF
C
C  CHECK FOR TERMINATION
C-----------------------
C
        QSTOPK=QSTOP
C??!    QSTOPK=.TRUE.
        ITERM = 0
        IF (SIGMAK.EQ.ZERO) QSTOPK=.TRUE.
        IF (ITER.GE.L-1) THEN
          IF (DIFQN.GT.GREAT*SOLNRM) IERR=-2
          IF (DIFQN.GT.DIFLIM) IERR=-2
          IF (IERR.EQ.-2) GOTO 998
          QSTOP = DIFQN .LE. EPS*SOLNRM
          IF (QSTOP) ITERM = 1
          IF (IFINI.EQ.3)  QSTOP = DIFQN .LE. 0.25D0 * DELNRM .OR. QSTOP
          IF (ITERM.EQ.0 .AND. QSTOP) ITERM = 2
        ELSE
          QSTOP=.FALSE.
        ENDIF
        IF (SIGMAK.EQ.ZERO) QSTOP=.TRUE.
C
C  DECISION FOR NEXT STEP (PROCEED OR RESTART DUE TO KMAX)
        K = K+1
        ITER = ITER+1
        IF (K.LE.KMAX) GOTO 20
C
C --- END OF ITERATION LOOP ---
C
        IF (IPRINT.GT.0)
     $    WRITE(IPUNIT,*) ' >>> RESTART DUE TO K > KMAX'
C
C --- RESTART ---
C
9990    CONTINUE
        GOTO 3
      ENDIF
C
C  SOLUTION EXIT
C
1000  CONTINUE
      IERR = 0
      IF (IPRINT.GE.0)  WRITE(IPUNIT,*) ' > SOLUTION FOUND, ITER=',ITER
      GOTO 999
C
C  EMERGENCY TERMINATION
C
998   CONTINUE
      IF (IPRINT.GE.0) THEN
        IF (IERR.EQ.-1) WRITE(IPUNIT,99801)
99801   FORMAT(' >> ITER. NUMBER LIMIT REACHED')
        IF (IERR.EQ.-2)  WRITE(IPUNIT,99802)
99802   FORMAT(' >> TERMINATION, SINCE ITERATION DIVERGES')
        IF (IERR.EQ.-3)  WRITE(IPUNIT,99803) TAUN1,TAU
99803   FORMAT(' >> TERM., SINCE BAD TAU OSCILLATES, ',
     $         ' TAUI = ',D9.2,' , ',D9.2)
        IF (IERR.EQ.-4)  WRITE(IPUNIT,99804)
99804   FORMAT(' >> TERMINATION, SINCE NEGATIVE TAU AT RESTART')
      ENDIF
C
C  FINAL UPDATE OF RETURN ARGUMENTS
C
999   CONTINUE
      DO 9991 J=1,N
        SOL(J)=SITER(J)
9991  CONTINUE
      IF (SIGMAK.EQ.ZERO) DIFNRM=ZERO
C
      EPS = DIFQN/SOLNRM
      DIFNRM = DABS(TAU)*DSQRT(SIGMAP/DN)
      MAXIT = ITER
      IOPT(2) = ITERM
      SOLNRA = SOLNRM
C
      RETURN
C***********************************************************************
C     FORMAT STATEMENTS
C***********************************************************************
  810 FORMAT(2X,' IT',7X,'COR',7X,'SOL',1X,'ESTABSERR',6X,'SOLQ',
     1         7X,'TAU')
  820 FORMAT(2X,' IT',7X,'COR',7X,'DEL',7X,'SOL',1X,'ESTABSERR',
     1           6X,'DELQ',6X,'SOLQ',7X,'TAU')
  830 FORMAT(' >>> RESTART REQUIRED DUE TO TAUK = ',D10.3,
     1           '( ITER=',I5,',K=',I3,' )')
  835 FORMAT(' >>> RESTART REQUIRED DUE TO GAMMAK = 0.0D0')
  840 FORMAT(' >>> TERMINATION - RESTART NOT POSSIBLE')
  850 FORMAT(' >>> RESTART CONDITION IGNORED - TAU = ',D10.3,
     1             '( ITER=',I5,',K=',I3,' )')
  860 FORMAT (' >>> TK RESET TO ',D10.3)
  890 FORMAT(I4,I3,6(1PD12.3))
  870 FORMAT(1X,I4,7(1X,D9.2))
C***********************************************************************
C  END OF ITZIBI
C***********************************************************************
      END
C
      SUBROUTINE ITZMID(L,ITER,DIFH,DIFNRM,DIFQN,QNEXT)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INTEGER L,ITER
      DOUBLE PRECISION DIFH(L),DIFNRM,DIFQN
      LOGICAL QNEXT
      IF (ITER.LT.L) THEN
        DIFH(ITER+1) = DIFNRM
      ELSE
        IF (QNEXT) THEN
        LM1 = L-1
          DO 119 J=1,LM1
            DIFH(J)=DIFH(J+1)
119       CONTINUE
        ENDIF
        DIFH(L)=DIFNRM
      ENDIF
      IF (ITER.GE.L-1) THEN
        DIFQN = DIFH(1)+DIFH(L)
      LM1=L-1
        DO 1191 J=2,LM1
          DIFQN = DIFQN+2.0D0*DIFH(J)
1191    CONTINUE
        DIFQN = DIFQN/DBLE(FLOAT(2*L-2))
      ENDIF
      RETURN
      END
C
      SUBROUTINE ITSCAL(N,Z,XW,FLAG)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INTEGER N
      DOUBLE PRECISION Z(N),XW(N)
      LOGICAL FLAG
C*BEGIN
      IF (FLAG) THEN
        DO 10 J=1,N
          Z(J) = Z(J) / XW(J)
10      CONTINUE
      ELSE
        DO 20 J=1,N
          Z(J) = Z(J) * XW(J)
20      CONTINUE
      ENDIF
      RETURN
      END
C
      DOUBLE PRECISION FUNCTION SPRODI( N, X, Y , SCAL )
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INTEGER N
      DOUBLE PRECISION X(N),Y(N),SCAL(*)
      INTEGER I
      DOUBLE PRECISION S
C*    BEGIN
      S=0.0D0
      DO 10 I=1,N
C       S=S+(X(I)/SCAL(I))*(Y(I)/SCAL(I))
        S=S+X(I)*Y(I)
10    CONTINUE
C!    SPRODI=S/DBLE(FLOAT(N))
        SPRODI=S
      RETURN
      END
C
C
C*****************************************************************
C*                    << ITPACK V2 >>                            *
C*                 MODIFIED 9 NOV. 1982                          *
C*****************************************************************
      SUBROUTINE VFILL (N,V,VAL)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C
C     VFILL FILLS A VECTOR, V, WITH A CONSTANT VALUE, VAL.
C
      DOUBLE PRECISION    V(N), VAL
      IF (N .LE. 0) RETURN
      NR=MOD(N,4)
C
C THE FOLLOWING CONSTRUCT ASSUMES A ZERO PASS DO LOOP.
C
      IS=1
      NRP1 = NR+ 1
      GOTO(1,2,3,4), NRP1
    4   IS=4
        V(1)=VAL
        V(2)=VAL
        V(3)=VAL
        GOTO 1
    3   IS=3
        V(1)=VAL
        V(2)=VAL
        GOTO 1
    2   IS=2
        V(1)=VAL
    1 DO 10 I=IS,N,4
        V(I)  =VAL
        V(I+1)=VAL
        V(I+2)=VAL
        V(I+3)=VAL
 10   CONTINUE
      RETURN
      END
      SUBROUTINE DCITSL(NV,N0,N1,B,BC,Z,X1,X2,
     1         RTOL,ATOL,MAXIT,INFO,IFAIL)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *                      U.MAAS                               *
C     *                    17.4.1986                              *
C     *           LAST CHANGE: U.MAAS APR.20,1986                 *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C    STRUCTURE OF THE MATRIX                                           *
C     < X X                 4 6|           >   <      >   <      >     *
C     < X X X                 5|           >   <      >   <      >     *
C     <   X X X                |           > * <      > = <      >     *
C     <     X X X              |           >   <      >   <      >     *
C     <       X X X            |           >   <      >   <      >     *
C     <         X X X          |           >   <      >   <      >     *
C     <           X X X        |           >   <      >   <      >     *
C     <             X X X      |           >   <      >   <      >     *
C     <               X X X    |           >   <      >   <      >     *
C     <                 X X X  |           >   <      >   <      >     *
C     < 2                 X X X|           >   <      >   <      >     *
C     < 1 3                 X X|           >   <      >   <      >     *
C                                                                      *
C   N0 = DIMENSION OF THE SUBBLOCKS X                                  *
C   N1 = NUMBER OF BLOCKS IN THE TRIDIAGONAL SUBSYSTEMS                *
C     INFO = 1 SOLUTION WITH INITIAL GUESS = ZERO                      *
C     INFO = 2 SOLUTION WITH INITIAL GUESS GIVEN                       *
C     INFO = 3 SOLUTION WITH DECOMPOSITION GIVEN                       *
C     INFO = 4 SOLUTION WITH DECOMPOSITION AND INITIAL GUESS GIVEN     *
C     IF INFO IS NEGATIVE METHOD OF JACOBI IS USED                     *
C     IF INFO IS POSITIVE GS- METHOD IS USED                           *
C     RETURN CODES (IFAIL)
C     IFAIL =  0 SYSTEM IS A TRIDIAGONAL MATRIX
C     IFAIL =  1 ERROR DURING DECOMPOSITION
C     IFAIL =  2 NOT SUCCEDED WITHIN MAXIT ITERATIONS
C     IFAIL =  9 BAD SYSTEM (NOT PENTADIAG.)
C     IFAIL <  0 SUCCESSFUL RETURN, IFAIL GIVES NUMBER OF ITERATIONS
C***********************************************************************
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION B(N0,N0,N1,3),BC(N0,N0,6),Z(N0,N1)
      DIMENSION X1(N0,N1),X2(N0,N1)
      DATA ZERO/0.0D0/
C     DATA OMEGA/0.5D0/
C***********************************************************************
C
C     INITIALIZE AND CHECK                                             C
C
C***********************************************************************
      IF(INFO.GE.3.OR.INFO.LE.-3) GOTO 50
C***********************************************************************
C     DECOMPOSE TRIDIAGONAL SUBMATRIX
C***********************************************************************
      CALL DGTFA(N1,N0,B(1,1,1,1),ILL)
      IF(ILL.NE.0) GOTO 9200
C***********************************************************************
C     SET INITIAL GUESS
C***********************************************************************
   50 CONTINUE
      IF(IABS(INFO).EQ.2.OR.IABS(INFO).EQ.4) GOTO 100
C---- SET INITIAL GUESS TO ZERO
      DO 60 J=1,N1
      DO 60 I=1,N0
      X1(I,J)=ZERO
   60 CONTINUE
C***********************************************************************
C
C     DECIDE ABOUT ITERATION METHOD                                    C
C
C***********************************************************************
  100 CONTINUE
C     IF(INFO.GT.0) GOTO 300
      GOTO 200
C***********************************************************************
C
C     ITERATION FOR JACOBIAN METHOD OR JOR                             C
C
C***********************************************************************
  200 CONTINUE
      INFTRI=-1
      NITER=0
C***********************************************************************
C     ITERATION LOOP
C***********************************************************************
  250 CONTINUE
      NITER=NITER+1
      IF(NITER.GT.MAXIT) GOTO 950
C---- CALCULATE RIGHT HAND SIDE
      DO 260 J=1,N1
      DO 260 I=1,N0
      X2(I,J)=Z(I,J)
 260  CONTINUE
      CALL DGQMMV(N0,BC(1,1,1),X1(1,1   ),X2(1,N1  ),-1)
      CALL DGQMMV(N0,BC(1,1,2),X1(1,1   ),X2(1,N1-1),-1)
      CALL DGQMMV(N0,BC(1,1,3),X1(1,2   ),X2(1,N1  ),-1)
      CALL DGQMMV(N0,BC(1,1,4),X1(1,N1-1),X2(1,1   ),-1)
      CALL DGQMMV(N0,BC(1,1,5),X1(1,N1  ),X2(1,2   ),-1)
      CALL DGQMMV(N0,BC(1,1,6),X1(1,N1  ),X2(1,1   ),-1)
C---- SOLVE EQUATION SYSTEM
      CALL DGTSL(N1,N0,B(1,1,1,1),X2(1,1))
C---- STORE NEW SOLUTION AND CALCULATE ERROR
CREL  RELEUK=ZERO
CREL  RELMAX=ZERO
      ABSMAX=ZERO
      ABSEUK=ZERO
      DO 290 J=1,N1
      DO 290 I=1,N0
C---- OMEGA
C     OMEGA = 1.5D0
C     X2(I,J)=OMEGA*X2(I,J)+ (1.0D0-OMEGA) * X1(I,J)
      D=X2(I,J)-X1(I,J)
CREL  UNEW=D/DMAX1(ATOL,DABS(X2(I,J)))
CREL  RELMAX=DMAX1(RELMAX,UNEW)
CREL  RELEUK=RELEUK+UNEW*UNEW
      ABSMAX=DMAX1(ABSMAX,DABS(D))
      ABSEUK=ABSEUK+D*D
      X1(I,J)=X2(I,J)
 290  CONTINUE
C---- CALCULATE OVERALL ERROR AND PRINT
CREL  RELEUK=DSQRT(RELEUK/FLOAT(NV))
CREL  WRITE(6,842) RELMAX,RELEUK,ABSMAX,ABSEUK
      ABSEUK=DSQRT(ABSEUK/FLOAT(NV))
C     WRITE(6,*) ABSMAX,ABSEUK
C     ERRO(NITER)=ABSMAX
      IF(NITER.EQ.1) THEN
        ABSEU0=ABSEUK
        ABSMA0=ABSMAX
      ENDIF
C---- ABEND IF SOLUTION SEEMS TO DIVERGE
      IF(ABSMAX.GT.1.D3*ABSMA0) GOTO 950
C---- ACCEPT SOLUTION IF ERRORS/INITIAL.ERRORS ARE SMALLER THEN RTOL
      IF(ABSMAX.LT.ABSMA0*RTOL.AND.ABSEUK.LT.ABSEU0*RTOL) GOTO 600
C---- ACCEPT SOLUTION IF ERRORS ARE SMALLER THEN ATOL
      IF(ABSMAX.LT.ATOL.AND.ABSEUK.LT.ATOL.AND.NITER.GT.1) GOTO 600
      GOTO 250
C***********************************************************************
C     STORE SOLUTION                                                  C
C***********************************************************************
  600 CONTINUE
      IFAIL=-NITER
C---- STORE SOLUTION IN Z
      DO 610 J=1,N1
      DO 610 I=1,N0
      Z(I,J)=X1(I,J)
  610 CONTINUE
      RETURN
C***********************************************************************
C     ERROR EXITS                                                     C
C***********************************************************************
C----- LESS THAN 3 SUBMATRICES
 9100 IFAIL= 9
      RETURN
 9200 IFAIL= 1
      RETURN
  950 IFAIL= 2
C     WRITE(6,888) (ERRO(I),I=1,NITER)
C 888 FORMAT(3X,' CORRECTIONS DURING ITERATION ',/,1(3X,1PE11.3))
  889  FORMAT(13(1PE9.2))
      RETURN
C***********************************************************************
C     END OF NITSL                                                   C
C***********************************************************************
      END
      SUBROUTINE DTPREI(JOB,N0,NB,N,NDM,A,NDPMAX,NDP,H,
     1        IPIVOT,IFAIL)
C***********************************************************************
C                                                                      *
C     ************************************************************     *
C     *                                                          *     *
C     *  COMPUTATION AND FACTORIZATION OF PRECONDITIONERS FOR    *     *
C     *    BLOCK-TRIDIAGONAL MATRICES                            *     *
C     *                AUTHOR: ULRICH MAAS  2.10.1987            *     *
C     *                                                          *     *
C     *                LAST CHANGE:  2.10.1987/MAAS              *     *
C     *                                                          *     *
C     ************************************************************     *
C                                                                      *
C***********************************************************************
C                                                                      *
C     NOTE: NO CHECK FOR SUFFICIENT ARRAY STORAGE PERFORMED!!!         *
C                                                                      *
C***********************************************************************
C                                                                      *
C     INPUT DATA:                                                      *
C                                                                      *
C      JOB          INFORMATION ABOUT TASK                             *
C                     1 NO PRECONDITIONER (H=I)                        *
C                     2 PRECONDITIONER = ROW SUM                       *
C                     3 PRECONDITIONER = INNER DIAGONAL BLOCKS         *
C                     4 PRECONDITIONER = INNER TRIDIAGONAL BLOCKS      *
C                     5 PRECONDITIONER = INCOMPLETE FACTORIZATION      *
C                                                                      *
C      JOB          INFORMATION ABOUT TASK                             *
C                                                                      *
C      N            DIMENSION OF THE SYSTEM                            *
C                                                                      *
C                                                                      *
C      A            ORIGINAL MATRIX                                    *
C                                                                      *
C                                                                      *
C     OUTPUT DATA:                                                     *
C                                                                      *
C      IFAIL        RETURN CODE                                        *
C                     IFAIL = 0 : SUCESSFULL COMPLETION                *
C                     IFAIL < 0 : FAILURE                              *
C                     SEE SUBROUTINES FOR FURTHER INFORMATION          *
C                                                                      *
C      H            PRECONDITIONER IN DECOMPOSED FORM                  *
C                                                                      *
C     WORKING ARRAYS:                                                  *
C                                                                      *
C                                                                      *
C      IPIVOT(N)    INTEGER WORK SPACE                                 *
C***********************************************************************
C                                                                      *
C***********************************************************************
C***********************************************************************
C     CHAPTER I.: STORAGE ORGANIZATION                                 *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION IPIVOT(N)
      DIMENSION A(NDM),H(NDPMAX)
C***********************************************************************
C     CHAPTER II.: INITIALIZE AND CHECK TASK
C***********************************************************************
      IFAIL=0
C***********************************************************************
C                                                                      *
C     CHAPTER III.: CHECK FOR KIND OF PRECONDITIONER                   *
C                                                                      *
C***********************************************************************
C***********************************************************************
C     CHAPTER III.0: CHECK FOR TYPE OF MATRIX                          *
C***********************************************************************
      IF(JOB.EQ.1) GOTO 100
      IF(JOB.EQ.2) GOTO 200
      IF(JOB.EQ.3) GOTO 300
      IF(JOB.EQ.4) GOTO 400
      IF(JOB.EQ.5) GOTO 910
      GOTO 910
C***********************************************************************
C     CHAPTER II.1: NO PRECONDITIONER                                  *
C***********************************************************************
  100 CONTINUE
      NDP=1
      GOTO 700
C***********************************************************************
C     CHAPTER II.2: PRECONDITIONER = ROW SUM                           *
C***********************************************************************
  200 CONTINUE
      NDP = N
      IF(N.GT.NDPMAX) GOTO 910
      CALL TRIRSU(N0,NB,A,H,JOB)
      GOTO 700
C***********************************************************************
C     CHAPTER II.3: PRECONDITIONER = INNER DIAGONAL BLOCKS             *
C***********************************************************************
  300 CONTINUE
      NDP = NB*N0*N0
      IF(NDP.GT.NDPMAX) GOTO 910
      NANF= 1
      NEND= NB*N0*N0
      DO 310 I=NANF,NEND
      H(I)=A(I+NB*N0*N0)
  310 CONTINUE
      DO 320 I= 1,NB
      IFIR=(I-1)*N0*N0+1
      IP  =(I-1)*N0    +1
      CALL DGEFA(H(IFIR),N0,N0,IPIVOT(IP),ILL)
      IF(ILL.GT.0) GOTO 920
  320 CONTINUE
      GOTO 700
C***********************************************************************
C     CHAPTER II.3: PRECONDITIONER = INNER TRIDIAGONAL                 *
C***********************************************************************
  400 CONTINUE
      NDP =3*NB*N0*N0
      IF(NDP.GT.NDPMAX) GOTO 910
      NANF=          N0*N0 + 1
      NEND=3*NB*N0*N0
      DO 410 I=NANF,NEND
      H(I)=A(I)
  410 CONTINUE
      CALL DGTFA(NB,N0,H,ILL)
      IF(ILL.NE.0) GOTO 920
      GOTO 700
C***********************************************************************
C                                                                      *
C     REGULAR EXIT                                                     *
C                                                                      *
C***********************************************************************
  700 CONTINUE
      RETURN
C***********************************************************************
C                                                                      *
C     CHAPTER VI.: ERROR EXITS                                         *
C                                                                      *
C***********************************************************************
C---- NOT ENOUGH STORAGE FOR PRECONDITIONER
  910 CONTINUE
      IFAIL=-8
      RETURN
C---- ERROR DURING DECOMPOSITION
  920 CONTINUE
      IFAIL=-4
      RETURN
C***********************************************************************
C                                                                      *
C     END OF LINSOL                                                    *
C                                                                      *
C***********************************************************************
      END
      SUBROUTINE DTPREC(JOB,N0,NB,N,NDM,A,NDP,H,
     1        IPIVOT,R,Z,IFAIL)
C***********************************************************************
C                                                                      *
C     ************************************************************     *
C     *                                                          *     *
C     *  COMPUTATION AND FACTORIZATION OF PRECONDITIONERS FOR    *     *
C     *    BLOCK-NONADIAGONAL MATRICES                           *     *
C     *                AUTHOR: ULRICH MAAS  2.10.1987            *     *
C     *                                                          *     *
C     *                LAST CHANGE:  2.10.1987/MAAS              *     *
C     *                                                          *     *
C     ************************************************************     *
C                                                                      *
C***********************************************************************
C                                                                      *
C     NOTE: NO CHECK FOR SUFFICIENT ARRAY STORAGE PERFORMED!!!         *
C                                                                      *
C***********************************************************************
C                                                                      *
C     INPUT DATA:                                                      *
C                                                                      *
C      JOB          INFORMATION ABOUT TASK                             *
C                     1 NO PRECONDITIONER (H=I)                        *
C                     2 PRECONDITIONER = ROW SUM                       *
C                     3 PRECONDITIONER = INNER DIAGONAL BLOCKS         *
C                     4 PRECONDITIONER = INNER TRIDIAGONAL BLOCKS      *
C                     5 PRECONDITIONER = INCOMPLETE FACTORIZATION      *
C                                                                      *
C      JOB          INFORMATION ABOUT TASK                             *
C                                                                      *
C      N            DIMENSION OF THE SYSTEM                            *
C                                                                      *
C                                                                      *
C      A            ORIGINAL MATRIX                                    *
C                                                                      *
C                                                                      *
C     OUTPUT DATA:                                                     *
C                                                                      *
C      IFAIL        RETURN CODE                                        *
C                     IFAIL = 0 : SUCESSFULL COMPLETION                *
C                     IFAIL < 0 : FAILURE                              *
C                     SEE SUBROUTINES FOR FURTHER INFORMATION          *
C                                                                      *
C      H            PRECONDITIONER IN DECOMPOSED FORM                  *
C                                                                      *
C     WORKING ARRAYS:                                                  *
C                                                                      *
C                                                                      *
C      IPIVOT(N)    INTEGER WORK SPACE                                 *
C***********************************************************************
C                                                                      *
C***********************************************************************
C***********************************************************************
C     CHAPTER I.: STORAGE ORGANIZATION                                 *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION IPIVOT(N)
      DIMENSION A(NDM),H(NDP)
      DIMENSION R(N),Z(N)
C***********************************************************************
C     CHAPTER II.: INITIALIZE AND CHECK TASK
C***********************************************************************
      IFAIL=0
C***********************************************************************
C                                                                      *
C     CHAPTER III.: CHECK FOR KIND OF PRECONDITIONER                   *
C                                                                      *
C***********************************************************************
C***********************************************************************
C     CHAPTER III.0: CHECK FOR TYPE OF MATRIX                          *
C***********************************************************************
      IF(JOB.EQ.1) GOTO 100
      IF(JOB.EQ.2) GOTO 200
      IF(JOB.EQ.3) GOTO 300
      IF(JOB.EQ.4) GOTO 400
C     IF(JOB.EQ.5) GOTO 500
      GOTO 910
C***********************************************************************
C     CHAPTER II.1: NO PRECONDITIONER                                  *
C***********************************************************************
  100 CONTINUE
      DO 110 I= 1,N
      Z(I)= R(I)
  110 CONTINUE
      GOTO 700
C***********************************************************************
C     CHAPTER II.2: PRECONDITIONER = ROW SUM                           *
C***********************************************************************
  200 CONTINUE
      DO 210 I= 1,N
      Z(I)= R(I)/H(I)
  210 CONTINUE
      GOTO 700
C***********************************************************************
C     CHAPTER II.3: PRECONDITIONER = INNER DIAGONAL BLOCKS             *
C***********************************************************************
  300 CONTINUE
      DO 310 I= 1,N
      Z(I)= R(I)
  310 CONTINUE
      DO 320 I= 1,NB
      IFIR=(I-1)*N0*N0+1
      IP  =(I-1)*N0    +1
      CALL DGESL(H(IFIR),N0,N0,IPIVOT(IP),Z(IP),0)
  320 CONTINUE
      GOTO 700
C***********************************************************************
C     CHAPTER II.3: PRECONDITIONER = INNER TRIDIAGONAL                 *
C***********************************************************************
  400 CONTINUE
      DO 410 I= 1,N
      Z(I)= R(I)
  410 CONTINUE
      CALL DGTSL(NB,N0,H,Z)
      GOTO 700
C***********************************************************************
C                                                                      *
C     REGULAR EXIT                                                     *
C                                                                      *
C***********************************************************************
  700 CONTINUE
      RETURN
C***********************************************************************
C                                                                      *
C     CHAPTER VI.: ERROR EXITS                                         *
C                                                                      *
C***********************************************************************
C---- NOT ENOUGH STORAGE FOR PRECONDITIONER
  910 CONTINUE
      IFAIL=-8
      RETURN
C***********************************************************************
C                                                                      *
C     END OF LINSOL                                                    *
C                                                                      *
C***********************************************************************
      END
        SUBROUTINE MPINV (SIZE,N,M,MINNM,A,AINV,S,E,U,V,
     1                     WORK,IRANK,IERR)
C
C       FUNCTION:
CF
CF      Calculates the Moore-Penrose pseudo-inverse of the N by M
CF      matrix A, and stores the result in AINV.  The singular
CF      values of A are returned in S.  The left singular vectors
CF      are returned in U, and the right singular vectors are
CF      returned in V.
CF
C       USAGE:
CU
CU      CALL MPINV (SIZE,N,M,MINNM,A,AINV,S,E,U,V,WORK,IERR)
CU
C       INPUTS:
CI
CI      SIZE    integer - leading dimension of all 2-dim. arrays.
CI              SIZE must be greater than or equal to max(N,M).
CI
CI      N       integer - first dimension of matrix A.
CI
CI      M       integer - second dimension of matrix A.
CI
CI      MINNM   integer - MINNM = min(N,M).
CI
CI      A       double precision - array of dimension SIZE by M
CI              containing the N by M matrix A.
CI
C       OUTPUTS:
CO     
CO      AINV    double precision - array of dimension SIZE by N 
CO              containing the M by N matrix AINV which is the 
CO              pseudo-inverse of A.
CO
CO      S       double precision - vector of dimension MINNM containing
CO              the singular values of A if IERR = 0; otherwise, see
CO              the description of LINPACK routine DSVDC.
CO
CO      E       double precision - vector of dimension MINNM used by
CO              LINPACK routine DSVDC; normally zero.
CO
CO      U       double precision - array of dimension SIZE by N
CO              containing the left singular vectors of A.
CO
CO      V       double precision - array of dimension size by M
CO              containing the right singular vectors of A.
CO
CO      WORK    double precision - vector of dimension N used by
CO              LINPACK routine DSVDC.
CO
CO      IRANK   integer - the calculated rank of the matrix A used
CO              in the construction of the pseudo-inverse.
co
CO      IERR    integer - error condition returned from dsvdc.  If
CO              IERR is not zero, then AINV is not correct.
CO
C       ALGORITHM:
CA
CA      Computes the singular value decomposition of A and forms
CA      the sum of the outer products of the left and right singular
CA      vectors of the matrix A divided by the corresponding singular
CA      value, for all non-zero singular values.
CA
C       MACHINE DEPENDENCIES:
CM
CM       none
CM
C       HISTORY:
CH
CH      written by:             Birdwell & Laub
CH      date:                   May 17, 1985
CH      current version:        2.0
CH      modifications:          modifications so that it works 
CH                              under standard F77:bb:8-86.
CH                              add minnm argument:bb:8-86.
C       ROUTINES CALLED:
CC
CC      LINPACK: DSVDC, DROT, DAXPY, DDOT, DSCAL, DSWAP, DNRM2, DROTG
CC
C       COMMON MEMORY USED:
CM
CM       none
CM
C----------------------------------------------------------------------
C       written for:    The CASCADE Project
C                       Oak Ridge National Laboratory
C                       U.S. Department of Energy
C                       Contract number DE-AC05-840R21400
C                       Subcontract number 37B-7685 S13
C                       Organization:   The University of Tennessee
C----------------------------------------------------------------------
C       THIS SOFTWARE IS IN THE PUBLIC DOMAIN
C       NO RESTRICTIONS ON ITS USE ARE IMPLIED
C----------------------------------------------------------------------
C
C--global variables:
C
        INTEGER                 SIZE
        INTEGER                 N
        INTEGER                 M
        INTEGER                 IRANK
        INTEGER                 IERR
C
        DOUBLE PRECISION        A(SIZE,M)
        DOUBLE PRECISION        AINV(SIZE,N)
        DOUBLE PRECISION        S(MINNM)
        DOUBLE PRECISION        E(MINNM)
        DOUBLE PRECISION        U(SIZE,N)
        DOUBLE PRECISION        V(SIZE,M)
        DOUBLE PRECISION        WORK(N)
C
C--local variables:
C
        INTEGER                 I
        INTEGER                 J
C
        DOUBLE PRECISION        EPS
C
C--code:
C
C  Call LINPACK routine to compute SVD.
C
        CALL DSVDC (A,SIZE,N,M,S,E,U,SIZE,V,SIZE,WORK,21,IERR)
        IF (IERR .NE. 0) RETURN
C
C  Compute machine epsilon.
C
        EPS = 1.0D0
 10     IF (EPS + 1.D0 .NE. 1.D0) THEN
          EPS = EPS / 2.D0
          GO TO 10
        END IF
        EPS = EPS * 2.D0
C
        EPS = EPS * S(1)
C
        DO 20, J = 1, N
          DO 20, I = 1, M
            AINV(I,J) = 0.D0
 20     CONTINUE
C
        IRANK = 1
 30     IF (S(IRANK) .GT. EPS .AND. IRANK .LE. MIN(N,M)) THEN
          DO 40, J = 1, N
            DO 40, I = 1, M
              AINV(I,J) = AINV(I,J)
     1          + V(I,IRANK)*U(J,IRANK)/S(IRANK)
 40       CONTINUE
          IRANK = IRANK + 1
          GO TO 30
        END IF
C
        IRANK = IRANK - 1
C
        RETURN
        END
      subroutine dsvdc(x,ldx,n,p,s,e,u,ldu,v,ldv,work,job,info)
      integer ldx,n,p,ldu,ldv,job,info
      double precision x(ldx,1),s(1),e(1),u(ldu,1),v(ldv,1),work(1)
c
c
c     dsvdc is a subroutine to reduce a double precision nxp matrix x
c     by orthogonal transformations u and v to diagonal form.  the
c     diagonal elements s(i) are the singular values of x.  the
c     columns of u are the corresponding left singular vectors,
c     and the columns of v the right singular vectors.
c
c     on entry
c
c         x         double precision(ldx,p), where ldx.ge.n.
c                   x contains the matrix whose singular value
c                   decomposition is to be computed.  x is
c                   destroyed by dsvdc.
c
c         ldx       integer.
c                   ldx is the leading dimension of the array x.
c
c         n         integer.
c                   n is the number of rows of the matrix x.
c
c         p         integer.
c                   p is the number of columns of the matrix x.
c
c         ldu       integer.
c                   ldu is the leading dimension of the array u.
c                   (see below).
c
c         ldv       integer.
c                   ldv is the leading dimension of the array v.
c                   (see below).
c
c         work      double precision(n).
c                   work is a scratch array.
c
c         job       integer.
c                   job controls the computation of the singular
c                   vectors.  it has the decimal expansion ab
c                   with the following meaning
c
c                        a.eq.0    do not compute the left singular
c                                  vectors.
c                        a.eq.1    return the n left singular vectors
c                                  in u.
c                        a.ge.2    return the first min(n,p) singular
c                                  vectors in u.
c                        b.eq.0    do not compute the right singular
c                                  vectors.
c                        b.eq.1    return the right singular vectors
c                                  in v.
c
c     on return
c
c         s         double precision(mm), where mm=min(n+1,p).
c                   the first min(n,p) entries of s contain the
c                   singular values of x arranged in descending
c                   order of magnitude.
c
c         e         double precision(p), 
c                   e ordinarily contains zeros.  however see the
c                   discussion of info for exceptions.
c
c         u         double precision(ldu,k), where ldu.ge.n.  if
c                                   joba.eq.1 then k.eq.n, if joba.ge.2
c                                   then k.eq.min(n,p).
c                   u contains the matrix of left singular vectors.
c                   u is not referenced if joba.eq.0.  if n.le.p
c                   or if joba.eq.2, then u may be identified with x
c                   in the subroutine call.
c
c         v         double precision(ldv,p), where ldv.ge.p.
c                   v contains the matrix of right singular vectors.
c                   v is not referenced if job.eq.0.  if p.le.n,
c                   then v may be identified with x in the
c                   subroutine call.
c
c         info      integer.
c                   the singular values (and their corresponding
c                   singular vectors) s(info+1),s(info+2),...,s(m)
c                   are correct (here m=min(n,p)).  thus if
c                   info.eq.0, all the singular values and their
c                   vectors are correct.  in any event, the matrix
c                   b = trans(u)*x*v is the bidiagonal matrix
c                   with the elements of s on its diagonal and the
c                   elements of e on its super-diagonal (trans(u)
c                   is the transpose of u).  thus the singular
c                   values of x and b are the same.
c
c     linpack. this version dated 08/14/78 .
c              correction made to shift 2/84.
c     g.w. stewart, university of maryland, argonne national lab.
c
c     dsvdc uses the following functions and subprograms.
c
c     external drot
c     blas daxpy,ddot,dscal,dswap,dnrm2,drotg
c     fortran dabs,dmax1,max0,min0,mod,dsqrt
c
c     internal variables
c
      integer i,iter,j,jobu,k,kase,kk,l,ll,lls,lm1,lp1,ls,lu,m,maxit,
     *        mm,mm1,mp1,nct,nctp1,ncu,nrt,nrtp1
      double precision ddot,t,r
      double precision b,c,cs,el,emm1,f,g,dnrm2,scale,shift,sl,sm,sn,
     *                 smm1,t1,test,ztest
      logical wantu,wantv
c
c
c     set the maximum number of iterations.
c
      maxit = 30
c
c     determine what is to be computed.
c
      wantu = .false.
      wantv = .false.
      jobu = mod(job,100)/10
      ncu = n
      if (jobu .gt. 1) ncu = min0(n,p)
      if (jobu .ne. 0) wantu = .true.
      if (mod(job,10) .ne. 0) wantv = .true.
c
c     reduce x to bidiagonal form, storing the diagonal elements
c     in s and the super-diagonal elements in e.
c
      info = 0
      nct = min0(n-1,p)
      nrt = max0(0,min0(p-2,n))
      lu = max0(nct,nrt)
      if (lu .lt. 1) go to 170
      do 160 l = 1, lu
         lp1 = l + 1
         if (l .gt. nct) go to 20
c
c           compute the transformation for the l-th column and
c           place the l-th diagonal in s(l).
c
            s(l) = dnrm2(n-l+1,x(l,l),1)
            if (s(l) .eq. 0.0d0) go to 10
               if (x(l,l) .ne. 0.0d0) s(l) = dsign(s(l),x(l,l))
               call dscal(n-l+1,1.0d0/s(l),x(l,l),1)
               x(l,l) = 1.0d0 + x(l,l)
   10       continue
            s(l) = -s(l)
   20    continue
         if (p .lt. lp1) go to 50
         do 40 j = lp1, p
            if (l .gt. nct) go to 30
            if (s(l) .eq. 0.0d0) go to 30
c
c              apply the transformation.
c
               t = -ddot(n-l+1,x(l,l),1,x(l,j),1)/x(l,l)
               call daxpy(n-l+1,t,x(l,l),1,x(l,j),1)
   30       continue
c
c           place the l-th row of x into  e for the
c           subsequent calculation of the row transformation.
c
            e(j) = x(l,j)
   40    continue
   50    continue
         if (.not.wantu .or. l .gt. nct) go to 70
c
c           place the transformation in u for subsequent back
c           multiplication.
c
            do 60 i = l, n
               u(i,l) = x(i,l)
   60       continue
   70    continue
         if (l .gt. nrt) go to 150
c
c           compute the l-th row transformation and place the
c           l-th super-diagonal in e(l).
c
            e(l) = dnrm2(p-l,e(lp1),1)
            if (e(l) .eq. 0.0d0) go to 80
               if (e(lp1) .ne. 0.0d0) e(l) = dsign(e(l),e(lp1))
               call dscal(p-l,1.0d0/e(l),e(lp1),1)
               e(lp1) = 1.0d0 + e(lp1)
   80       continue
            e(l) = -e(l)
            if (lp1 .gt. n .or. e(l) .eq. 0.0d0) go to 120
c
c              apply the transformation.
c
               do 90 i = lp1, n
                  work(i) = 0.0d0
   90          continue
               do 100 j = lp1, p
                  call daxpy(n-l,e(j),x(lp1,j),1,work(lp1),1)
  100          continue
               do 110 j = lp1, p
                  call daxpy(n-l,-e(j)/e(lp1),work(lp1),1,x(lp1,j),1)
  110          continue
  120       continue
            if (.not.wantv) go to 140
c
c              place the transformation in v for subsequent
c              back multiplication.
c
               do 130 i = lp1, p
                  v(i,l) = e(i)
  130          continue
  140       continue
  150    continue
  160 continue
  170 continue
c
c     set up the final bidiagonal matrix or order m.
c
      m = min0(p,n+1)
      nctp1 = nct + 1
      nrtp1 = nrt + 1
      if (nct .lt. p) s(nctp1) = x(nctp1,nctp1)
      if (n .lt. m) s(m) = 0.0d0
      if (nrtp1 .lt. m) e(nrtp1) = x(nrtp1,m)
      e(m) = 0.0d0
c
c     if required, generate u.
c
      if (.not.wantu) go to 300
         if (ncu .lt. nctp1) go to 200
         do 190 j = nctp1, ncu
            do 180 i = 1, n
               u(i,j) = 0.0d0
  180       continue
            u(j,j) = 1.0d0
  190    continue
  200    continue
         if (nct .lt. 1) go to 290
         do 280 ll = 1, nct
            l = nct - ll + 1
            if (s(l) .eq. 0.0d0) go to 250
               lp1 = l + 1
               if (ncu .lt. lp1) go to 220
               do 210 j = lp1, ncu
                  t = -ddot(n-l+1,u(l,l),1,u(l,j),1)/u(l,l)
                  call daxpy(n-l+1,t,u(l,l),1,u(l,j),1)
  210          continue
  220          continue
               call dscal(n-l+1,-1.0d0,u(l,l),1)
               u(l,l) = 1.0d0 + u(l,l)
               lm1 = l - 1
               if (lm1 .lt. 1) go to 240
               do 230 i = 1, lm1
                  u(i,l) = 0.0d0
  230          continue
  240          continue
            go to 270
  250       continue
               do 260 i = 1, n
                  u(i,l) = 0.0d0
  260          continue
               u(l,l) = 1.0d0
  270       continue
  280    continue
  290    continue
  300 continue
c
c     if it is required, generate v.
c
      if (.not.wantv) go to 350
         do 340 ll = 1, p
            l = p - ll + 1
            lp1 = l + 1
            if (l .gt. nrt) go to 320
            if (e(l) .eq. 0.0d0) go to 320
               do 310 j = lp1, p
                  t = -ddot(p-l,v(lp1,l),1,v(lp1,j),1)/v(lp1,l)
                  call daxpy(p-l,t,v(lp1,l),1,v(lp1,j),1)
  310          continue
  320       continue
            do 330 i = 1, p
               v(i,l) = 0.0d0
  330       continue
            v(l,l) = 1.0d0
  340    continue
  350 continue
c
c     main iteration loop for the singular values.
c
      mm = m
      iter = 0
  360 continue
c
c        quit if all the singular values have been found.
c
c     ...exit
         if (m .eq. 0) go to 620
c
c        if too many iterations have been performed, set
c        flag and return.
c
         if (iter .lt. maxit) go to 370
            info = m
c     ......exit
            go to 620
  370    continue
c
c        this section of the program inspects for
c        negligible elements in the s and e arrays.  on
c        completion the variables kase and l are set as follows.
c
c           kase = 1     if s(m) and e(l-1) are negligible and l.lt.m
c           kase = 2     if s(l) is negligible and l.lt.m
c           kase = 3     if e(l-1) is negligible, l.lt.m, and
c                        s(l), ..., s(m) are not negligible (qr step).
c           kase = 4     if e(m-1) is negligible (convergence).
c
         do 390 ll = 1, m
            l = m - ll
c        ...exit
            if (l .eq. 0) go to 400
            test = dabs(s(l)) + dabs(s(l+1))
            ztest = test + dabs(e(l))
            if (ztest .ne. test) go to 380
               e(l) = 0.0d0
c        ......exit
               go to 400
  380       continue
  390    continue
  400    continue
         if (l .ne. m - 1) go to 410
            kase = 4
         go to 480
  410    continue
            lp1 = l + 1
            mp1 = m + 1
            do 430 lls = lp1, mp1
               ls = m - lls + lp1
c           ...exit
               if (ls .eq. l) go to 440
               test = 0.0d0
               if (ls .ne. m) test = test + dabs(e(ls))
               if (ls .ne. l + 1) test = test + dabs(e(ls-1))
               ztest = test + dabs(s(ls))
               if (ztest .ne. test) go to 420
                  s(ls) = 0.0d0
c           ......exit
                  go to 440
  420          continue
  430       continue
  440       continue
            if (ls .ne. l) go to 450
               kase = 3
            go to 470
  450       continue
            if (ls .ne. m) go to 460
               kase = 1
            go to 470
  460       continue
               kase = 2
               l = ls
  470       continue
  480    continue
         l = l + 1
c
c        perform the task indicated by kase.
c
         go to (490,520,540,570), kase
c
c        deflate negligible s(m).
c
  490    continue
            mm1 = m - 1
            f = e(m-1)
            e(m-1) = 0.0d0
            do 510 kk = l, mm1
               k = mm1 - kk + l
               t1 = s(k)
               call drotg(t1,f,cs,sn)
               s(k) = t1
               if (k .eq. l) go to 500
                  f = -sn*e(k-1)
                  e(k-1) = cs*e(k-1)
  500          continue
               if (wantv) call drot(p,v(1,k),1,v(1,m),1,cs,sn)
  510       continue
         go to 610
c
c        split at negligible s(l).
c
  520    continue
            f = e(l-1)
            e(l-1) = 0.0d0
            do 530 k = l, m
               t1 = s(k)
               call drotg(t1,f,cs,sn)
               s(k) = t1
               f = -sn*e(k)
               e(k) = cs*e(k)
               if (wantu) call drot(n,u(1,k),1,u(1,l-1),1,cs,sn)
  530       continue
         go to 610
c
c        perform one qr step.
c
  540    continue
c
c           calculate the shift.
c
            scale = dmax1(dabs(s(m)),dabs(s(m-1)),dabs(e(m-1)),
     *                    dabs(s(l)),dabs(e(l)))
            sm = s(m)/scale
            smm1 = s(m-1)/scale
            emm1 = e(m-1)/scale
            sl = s(l)/scale
            el = e(l)/scale
            b = ((smm1 + sm)*(smm1 - sm) + emm1**2)/2.0d0
            c = (sm*emm1)**2
            shift = 0.0d0
            if (b .eq. 0.0d0 .and. c .eq. 0.0d0) go to 550
               shift = dsqrt(b**2+c)
               if (b .lt. 0.0d0) shift = -shift
               shift = c/(b + shift)
  550       continue
            f = (sl + sm)*(sl - sm) + shift
            g = sl*el
c
c           chase zeros.
c
            mm1 = m - 1
            do 560 k = l, mm1
               call drotg(f,g,cs,sn)
               if (k .ne. l) e(k-1) = f
               f = cs*s(k) + sn*e(k)
               e(k) = cs*e(k) - sn*s(k)
               g = sn*s(k+1)
               s(k+1) = cs*s(k+1)
               if (wantv) call drot(p,v(1,k),1,v(1,k+1),1,cs,sn)
               call drotg(f,g,cs,sn)
               s(k) = f
               f = cs*e(k) + sn*s(k+1)
               s(k+1) = -sn*e(k) + cs*s(k+1)
               g = sn*e(k+1)
               e(k+1) = cs*e(k+1)
               if (wantu .and. k .lt. n)
     *            call drot(n,u(1,k),1,u(1,k+1),1,cs,sn)
  560       continue
            e(m-1) = f
            iter = iter + 1
         go to 610
c
c        convergence.
c
  570    continue
c
c           make the singular value  positive.
c
            if (s(l) .ge. 0.0d0) go to 580
               s(l) = -s(l)
               if (wantv) call dscal(p,-1.0d0,v(1,l),1)
  580       continue
c
c           order the singular value.
c
  590       if (l .eq. mm) go to 600
c           ...exit
               if (s(l) .ge. s(l+1)) go to 600
               t = s(l)
               s(l) = s(l+1)
               s(l+1) = t
               if (wantv .and. l .lt. p)
     *            call dswap(p,v(1,l),1,v(1,l+1),1)
               if (wantu .and. l .lt. n)
     *            call dswap(n,u(1,l),1,u(1,l+1),1)
               l = l + 1
            go to 590
  600       continue
            iter = 0
            m = m - 1
  610    continue
      go to 360
  620 continue
      return
      end
