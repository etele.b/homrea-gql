      SUBROUTINE DNLSQN(N,X,PHI,M,XI,YI,EPS,ITMAX,RW,LRW,IW,LIW,INFO,
     1      MSGFIL,IFLAG)
C----------------------------------------------------------------------
C
C   NUMERICAL SOLUTION OF NONLINEAR (N) LEAST SQUARES (LSQ)
C   PROBLEMS USING INTERNAL NUMERICAL (N) APPROXIMATION OF JACOBIAN
C   MATRIX, ESPECIALLY DESIGNED FOR NUMERICALLY SENSITIVE PROBLEMS
C
C  ********** REVISION 1 ********** LATEST CHANGE : JULY 1ST, 81 (RM)
C
C     REF.:   P.DEUFLHARD / V.APOSTOLESCU
C             A STUDY OF THE GAUSS-NEWTON METHOD FOR THE SOLUTION
C             OF NONLINEAR LEAST SQUARES PROBLEMS
C             IN: SPECIAL TOPICS OF APPLIED MATHEMATICS
C                 (ED. BY J.FREHSE, D.PALLASCHKE, U.TROTTENBERG)
C                 P. 129 - 150 (1980)
C
C
C
C     EXTERNAL SUBROUTINE (TO BE SUPPLIED BY THE USER)
C     -------------------------------------------------
C
C        PHI(N,X,XI,F,M,IFLAG)
C            IMPLEMENTATION OF VECTOR MODEL FUNCTION (INPUT)
C           X(N)         PARAMETER VECTOR (INPUT)
C           XI(M)        ARRAY FOR COMMUNICATION
C           F(M)         COMPUTED VALUES OF MODEL FUNCTION (OUTPUT)
C           IFLAG        ERROR FLAG
C
C
C     INPUT PARAMETERS (* MARKS TRANSIENT PARAMETERS)
C     -----------------------------------------------
C
C        N              NUMBER OF PARAMETERS TO BE ESTIMATED (.LE.100 )
C      * X(N)           INITIAL ESTIMATE OF PARAMETERS
C        M              NUMBER OF COMPONENTS OF MODEL FUNCTION(.LE.256)
C        XI(M)          ARRAY FOR USER COMMUNICATION WITH PHI
C        YI(M)          OBSERVED EXPERIMENTAL DATA
C     (*)EPS            REQUIRED RELATIVE PRECISION OF
C                       SOLUTION COMPONENTS (.GE.DSQRT(EPMACH*TEN))
C        ITMAX          MAXIMUM PERMITTED NUMBER OF ITERATIONS
C      * INFO           INFORMATION PARAMETER
C                       -1    NO PRINT
C                        0    PRINT OF INITIAL GUESS OF PARAMETERS,
C                             ITERATIVE VALUES OF LEVEL FUNCTIONS,
C                             SUB-CONDITION NUMBER,SENSITIVITY
C                             NUMBER, FINAL VALUES OF PARAMETERS
C                       +1    ADDITIONALLY ITERATES X(N)
C                       +2    ADDITIONALLY RESIDUALS
C
C     OUTPUT PARAMETERS
C     -----------------
C
C        X(N)           SOLUTION VALUES (OR FINAL VALUES, RESPECTIVELY)
C        EPS            FINALLY ACHIEVED ACCURACY
C                       (REASONABLY ADAPTED TO GIVEN PROBLEM,
C                        MAY DIFFER FROM USER-PRESCRIBED ACCURACY)
C        INFO           >0    NUMBER OF FUNCTION EVALUATIONS
C                             NEEDED TO OBTAIN THE SOLUTION
C                       <0    NLSQN FAILED TO CONVERGE
C                       -1    TERMINATION, SINCE ITERATION DIVERGES
C                             INITIAL GUESS TOO BAD OR MODEL TOO FAR
C                             FROM BEING COMPATIBLE
C                       -2    TERMINATION AFTER ITMAX ITERATIONS
C                             ( AS INDICATED BY INPUT PARAMETER ITMAX )
C                       -3    TERMINATION, SINCE RELAXATION
C                             STRATEGY DID NOT SUCCEED
C                       -4    TERMINATION SINCE MORE THAN ICMAX
C                             (INTERNAL PARAMETER) RANK-REDUCTIONS
C                             PERFORMED
C                       -9    REAL WORK SPACE TO SMALL
C                       -10   INTEGER WORK SPACE TO SMALL
C
C                       IN CASE OF FAILURE:
C                       - USE BETTER INITIAL GUESS
C                       - OR REFINE MODEL
C                       - OR TURN TO GENERAL OPTIMIZATION ROUTINE
C
C
C----------------------------------------------------------------------
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C
      EXTERNAL PHI
      INTEGER  INFO,ITMAX,N,M,IW,LIW,LRW
C
      DOUBLE PRECISION     X,XI,YI,EPS,RW
C
      DIMENSION X(N),XI(M),YI(M),RW(LRW),IW(LIW)
      NR1=1
      NR2=NR1+M*N
      NR3=NR2+N*N
      NR4=NR3+N
      NR5=NR4+N
      NR6=NR5+N
      NR7=NR6+N
      NR8=NR7+N
      NR9=NR8+N
      NR10=NR9+N
      NR11=NR10+N
      NR12=NR11+N
      NR13=NR12+N
      NR14=NR13+M
      NR15=NR14+M
      NR16=NR15+M
      NR17=NR16+M
      NR18=NR17+M
      LRNEED=NR18-1
      IF(LRNEED.GT.LRW) GOTO 910
      NI1=1
      NI2=NI1+N
      LINEED=NI2-1
      IF(LINEED.GT.LIW) GOTO 920
      CALL NLSQN1(N,X,PHI,M,XI,YI,EPS,ITMAX,INFO,MSGFIL,
     1  RW(NR1),RW(NR2),RW(NR3),RW(NR4),RW(NR5),RW(NR6),RW(NR7),RW(NR8),
     1  RW(NR9),RW(NR10),RW(NR11),RW(NR12),RW(NR13),RW(NR14),RW(NR15),
     1  RW(NR16),RW(NR17),IW(NI1),IFLAG)
      RETURN
 910  INFO=-9
      RETURN
 920  INFO=-10
      RETURN
      END
      SUBROUTINE NLSQN1(N,X,PHI,M,XI,YI,EPS,ITMAX,INFO,MSGFIL,
     1  A,AH,D,DX,DXH,DX1,DX1A,ETA,V,XA,XW,WK,F,FA,FD,FH,U,PIVOT,IFLAG)
C----------------------------------------------------------------------
C
C   NUMERICAL SOLUTION OF NONLINEAR (N) LEAST SQUARES (LSQ)
C   PROBLEMS USING INTERNAL NUMERICAL (N) APPROXIMATION OF JACOBIAN
C   MATRIX, ESPECIALLY DESIGNED FOR NUMERICALLY SENSITIVE PROBLEMS
C
C  ********** REVISION 1 ********** LATEST CHANGE : JULY 1ST, 81 (RM)
C
C     REF.:   P.DEUFLHARD / V.APOSTOLESCU
C             A STUDY OF THE GAUSS-NEWTON METHOD FOR THE SOLUTION
C             OF NONLINEAR LEAST SQUARES PROBLEMS
C             IN: SPECIAL TOPICS OF APPLIED MATHEMATICS
C                 (ED. BY J.FREHSE, D.PALLASCHKE, U.TROTTENBERG)
C                 P. 129 - 150 (1980)
C
C
C
C     EXTERNAL SUBROUTINE (TO BE SUPPLIED BY THE USER)
C     -------------------------------------------------
C
C        PHI(N,X,XI,F,M) IMPLEMENTATION OF VECTOR MODEL FUNCTION (INPUT)
C           X(N)         PARAMETER VECTOR (INPUT)
C           XI(M)        ARRAY FOR COMMUNICATION
C           F(M)         COMPUTED VALUES OF MODEL FUNCTION (OUTPUT)
C
C
C     INPUT PARAMETERS (* MARKS TRANSIENT PARAMETERS)
C     -----------------------------------------------
C
C        N              NUMBER OF PARAMETERS TO BE ESTIMATED (.LE.100 )
C      * X(N)           INITIAL ESTIMATE OF PARAMETERS
C        M              NUMBER OF COMPONENTS OF MODEL FUNCTION(.LE.256)
C        XI(M)          ARRAY FOR USER COMMUNICATION WITH PHI
C        YI(M)          OBSERVED EXPERIMENTAL DATA
C     (*)EPS            REQUIRED RELATIVE PRECISION OF
C                       SOLUTION COMPONENTS (.GE.DSQRT(EPMACH*TEN))
C        ITMAX          MAXIMUM PERMITTED NUMBER OF ITERATIONS
C      * INFO           INFORMATION PARAMETER
C                       -1    NO PRINT
C                        0    PRINT OF INITIAL GUESS OF PARAMETERS,
C                             ITERATIVE VALUES OF LEVEL FUNCTIONS,
C                             SUB-CONDITION NUMBER,SENSITIVITY
C                             NUMBER, FINAL VALUES OF PARAMETERS
C                       +1    ADDITIONALLY ITERATES X(N)
C                       +2    ADDITIONALLY RESIDUALS
C
C     OUTPUT PARAMETERS
C     -----------------
C
C        X(N)           SOLUTION VALUES (OR FINAL VALUES, RESPECTIVELY)
C        EPS            FINALLY ACHIEVED ACCURACY
C                       (REASONABLY ADAPTED TO GIVEN PROBLEM,
C                        MAY DIFFER FROM USER-PRESCRIBED ACCURACY)
C        INFO           >0    NUMBER OF FUNCTION EVALUATIONS
C                             NEEDED TO OBTAIN THE SOLUTION
C                       <0    NLSQN FAILED TO CONVERGE
C                       -1    TERMINATION, SINCE ITERATION DIVERGES
C                             INITIAL GUESS TOO BAD OR MODEL TOO FAR
C                             FROM BEING COMPATIBLE
C                       -2    TERMINATION AFTER ITMAX ITERATIONS
C                             ( AS INDICATED BY INPUT PARAMETER ITMAX )
C                       -3    TERMINATION, SINCE RELAXATION
C                             STRATEGY DID NOT SUCCEED
C                       -4    TERMINATION SINCE MORE THAN ICMAX
C                             (INTERNAL PARAMETER) RANK-REDUCTIONS
C                             PERFORMED
C                       IN CASE OF FAILURE:
C                       - USE BETTER INITIAL GUESS
C                       - OR REFINE MODEL
C                       - OR TURN TO GENERAL OPTIMIZATION ROUTINE
C
C
C----------------------------------------------------------------------
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      EXTERNAL PHI
C
      INTEGER  I,        IC,       ICMAX,    IDIV,     IFCTEV,   INFO
      INTEGER  IRANK,    IRANKA,   IRKMAX,   IRK1,     ITER,     ITMAX
      INTEGER  K,        KONV,     KT,       KPRINT,   K1
      INTEGER  L,        LEVEL,    M,        N
      INTEGER PIVOT(N)
C
      DOUBLE PRECISION  COND,  D1,    EPCUT, EPDIFF, EPMACH,   EPS
      DOUBLE PRECISION  ETAD,  ETADIF,FC,    FCA
      DOUBLE PRECISION  FCMIN, FCS,   H,     HG,     S
      DOUBLE PRECISION  SFC1,  SFC2,  SK,    ST,     SU,       SUMF
      DOUBLE PRECISION  SUMFA, SUMX,  SUMXA, T,      TMIN
C
      DOUBLE PRECISION  A(M,N),AH(N,N)
      DOUBLE PRECISION D(N),DX(N),DXH(N),DX1(N),DX1A(N),ETA(N),
     1     V(N),XA(N),XW(N),WK(N)
      DOUBLE PRECISION F(M),FA(M),FD(M),FH(M),U(M)
      DOUBLE PRECISION X(N),YI(M),XI(M)
C
C
      DATA ZERO/0.D0/    , SMALL1/1.D-30/ , EPCUT/1.D-5/
      DATA FCMIN/0.01D0/ , SMALL2/0.1D0/  , HALF/0.5D0/
      DATA FCS1/0.7D0/   , ONE/1.D0/      , EIGHT/8.D0/
      DATA TEN/1.D1/     , TEN3/1.D3/
C
C
C  RELATIVE MACHINE PRECISION
C  (ADAPTED TO IBM 370/168, UNIVERSITY OF HEIDELBERG)
      EPMACH = 1. E-16
C
C  INTERNAL PARAMETERS, STANDARD VALUES FIXED BELOW
C  TO BE ALTERED, IF NECESSARY, BY THE SKILLFUL USER
C
C  COND       MAXIMUM PERMITTED SUB-CONDITION
C             NUMBER OF JACOBIAN MATRIX
C  EPCUT      CUT-OFF PARAMETER FOR ROUND-OFF ERRORS IN
C             RELAXATION STRATEGY
C  EPDIFF     CONTROL PARAMETER FOR NUMERICAL DIFFERENTIATION
C  ETADIF     INITIAL RELATIVE DEVIATION FOR NUMERICAL
C             DIFFERENTIATION
C  FC         STARTING VALUE OF RELAXATION FACTOR
C  FCMIN      MINIMUM PERMITTED VALUE OF RELAXATION FACTOR
C  IRANK      STARTING VALUE OF PSEUDO-RANK OF JACOBIAN MATRIX
C  IRKMAX     MAXIMUM PERMITTED PSEUDO-RANK OF JACOBIAN MATRIX
C  ICMAX      MAXIMUM PERMITTED NUMBER OF RANK-REDUCTION-STEPS
C  IDIVM      MAXIMUM PERMITTED NUMBER OF SUCCESSIVE
C             NON-CONTRACTIVE STEPS
C
      COND = ONE/EPMACH
      EPDIFF = EPMACH * TEN3
      ETADIF = DSQRT(EPDIFF)
      IRANK = N
      IRKMAX = N
      ICMAX= ITMAX
      IDIVM = 3
C
C  INITIAL PREPARATIONS TO START NLSQN
C
      ETAD = DSQRT(EPMACH*TEN)
      SKAP = ZERO
      FN=FLOAT(N)
      FM=FLOAT(M)
      FNEPS2=FN*EPS*EPS
      EPMACH = EPMACH* TEN* FN
      EPS = DMAX1(EPS,ETAD)
      KPRINT = INFO
      FC = FCMIN
      FCA = FC
      IFCTEV = 0
      IC = 0
      IRANKA = IRANK
      ITER = 0
      IDIV = 0
      SFC1 = ZERO
      TMIN = ONE
C----------------------------------------------------------------------
C  INITIAL SCALING
C
      DO    10  I=1,N
            S = DABS(X(I))
            IF  (S.LT.EPMACH)  S = ONE
            ETA(I) = ETADIF
            XW(I) = S
10          CONTINUE
      IF  (KPRINT.LT.0)  GO TO 1
      WRITE(MSGFIL,1000)
      WRITE(MSGFIL,1001) M
      WRITE(MSGFIL,1003) (X(I), I=1,N)
      WRITE(MSGFIL,1000)
      WRITE(MSGFIL,1004)
C----------------------------------------------------------------------
C----------------------------------------------------------------------
C  COMPUTATION OF RESIDUAL VECTOR
1     CALL  PHI(N,X,XI,FD,M,IFLAG)
      IFCTEV = IFCTEV+1
      KONV = 1
      SUMF = ZERO
      DO    11  I=1,M
            S = (FD(I)-YI(I))
            F(I) = S
            U(I) = S
            SUMF = SUMF+S*S
11          CONTINUE
      IF  (ITER.EQ.0)  GO TO 20
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C  FIRST MONOTONICITY TEST  (LEVEL-FUNCTION: T(X|I)
C
      KT = 1
      IF  (SUMF.LE.SUMFA)  LEVEL = 1
      GO TO 44
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C  DIFFERENCE APPROXIMATION OF THE NEGATIVE JACOBIAN MATRIX
C  INCLUDING FEED-BACK DEVICE
C
20    SUMFA = SUMF
2     DO    21  I=1,N
            K1 = 0
211         IFCTEV = IFCTEV+1
            T = X(I)
            S = DSIGN(ETA(I)*XW(I),T)
            X(I) = T+S
            CALL PHI(N,X,XI,FH,M,IFLAG)
            X(I) = T
            SU = ZERO
            DO   212  K=1,M
                 HG = DMAX1(DABS(FD(K)),DABS(FH(K)))
                 T = FD(K)-FH(K)
                 IF  (HG.NE.ZERO)  SU = SU +(T/HG)*(T/HG)
212              A(K,I) = T/S
            SU = DSQRT(SU/FM)
            IF  ((SU.EQ.ZERO).OR.(K1.GT.0))  GO TO 21
            ETA(I) = DMAX1(EPDIFF,DSQRT(ETAD/SU)*ETA(I))
            ETA(I) = DMIN1(FCMIN,ETA(I))
            K1 = 1
            IF  ((TMIN.GE.SMALL2).AND.(SU.LT.EPDIFF))  GO TO 211
21          CONTINUE
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C  SOLUTION OF THE LINEAR SYSTEM
C
4     KT = 0
      LEVEL = 0
      DO    41  I=1,N
            ST = XW(I)
            DO   411  K=1,M
                 A(K,I) = A(K,I)*ST
411              CONTINUE
41          CONTINUE
C
      DO    42  I=1,M
            FA(I) = F(I)
            U(I) = F(I)
42          CONTINUE
C
C  HOUSEHOLDER TRIANGULARIZATION
C
43    CALL DECOMP (A, M, N, IRANK, COND, D, PIVOT, KT, AH,WK)
C
      IF  (IRANK.EQ.0)  GO TO 93
      D1 = DABS(D(1))
      SK = D1/DABS(D(IRANK))
C
C  (BEST) LINEAR LEAST SQUARES SOLUTION
C
44    CALL SOLVE (A, V, U, M, N, IRANK, D, PIVOT, KT, AH, FH,WK)
C
      SUMX = ZERO
      IF  ((ITER.EQ.0).OR.(KT.GT.0))  GO TO 47
      IF  (KT.EQ.0)  SFC1 = ZERO
      SFC2 = ZERO
      TMIN = ZERO
47    DO   471  L=1,N
           T = V(L)
           H = XW(L)
           S = T*H
           SUMX = SUMX+T*T
           IF  (KT.LE.0)  GO TO 4712
           DX1(L) = S
           GO TO 471
4712       TMIN = DMAX1(TMIN,DABS(T))
           IF  (ITER.EQ.0)  GO TO 4713
           T = T-DX1A(L)/H
           SFC2 = SFC2+T*T
           IF  (KT.LT.0)  GO TO 4714
           T = DX(L)/H
           SFC1 = SFC1+T*T
4713       XA(L) = X(L)
4714       DXH(L) = S
471        CONTINUE
      IF  (KT.LE.0)  GO TO 5
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C  SECOND MONOTONICITY TEST
C  IN TERMS OF NATURAL LEVEL-FUNCTION
C
48    IF  (SUMX.LE.SUMXA) LEVEL = LEVEL+1
C
      IF(SUMX.LT.FNEPS2.AND.FC.EQ.ONE) GO TO 8
C
      IF(LEVEL) 6,6,7
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C  TEST OF ACCURACY (NATURALLY SCALED L-INFINITY NORM)
C
5     IF  (TMIN.GT.EPS)  KONV = 0
C
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C  STEPLENGTH CONTROL
C
      IF  (ITER.GT.0)  GO TO 51
      IF  (KT.LT.0)  FC = FCMIN
      DO    50  I=1,N
           DX(I) = DXH(I)
50         CONTINUE
      GO TO 54
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C  PREDICTION OF RELAXATION FACTOR
C
51    IF  (IRANK.EQ.N)  GO TO 513
      DO   511  L=1,N
           K=PIVOT(L)
           V(L) = DX1A(K)/XW(K)
511        CONTINUE
      T = ZERO
      IRK1 = IRANK+1
      DO   512  I= IRK1,N
           S = V(I)
           DO  5121  L=1,IRANK
               S = S-V(L)*AH(L,I)
5121           CONTINUE
           IF  (I.EQ.IRK1) GO TO 5123
           I1 = I-1
           DO  5122  L=IRK1,I1
               S = S-V(L)*AH(L,I)
5122           CONTINUE
5123       S = S/D(I)
           T = T+S*S
           V(I)=S
512        CONTINUE
      SFC2 = SFC2-T
C
513   IF  (SUMX.GT.ZERO)  GO TO 514
      FC = ONE
      GO TO 54
C
C
514   SFC2 = DMAX1(SFC2,SUMX*EPCUT)
      FCS = DSQRT(SFC1/SFC2)*FCA
      DO   52  I=1,N
           DX(I) = DXH(I)
52         CONTINUE
      IF  (FCS.GE.FCMIN)  GO TO 532
      IF  (IRANK.LE.IRANKA)  GO TO 531
      FC = FCMIN
      GO TO 54
C
531   CONTINUE
      IF  (KPRINT.GE.0)  WRITE(MSGFIL,1005) SUMX, FCS, IRANK, SK
      GO TO 621
C
532   FC = ONE
      IF  (FCS.LE.FCS1)  FC = FCS
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C
54    CONTINUE
      IF  (KPRINT.LT.0)  GO TO 55
      IF  (KPRINT.GT.0.AND.ITER.GT.0)  WRITE(MSGFIL,1000)
      IF  (KPRINT.GT.0.AND.ITER.GT.0)  WRITE(MSGFIL,1004)
      WRITE(MSGFIL,1006) ITER, SUMX, IRANK, SUMF, SK, D1
C
55    ITER = ITER+1
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C  TRIAL VALUE OF NEXT ITERATE
C
      DO   551  I=1,N
           X(I) = XA(I)+FC*DX(I)
551        CONTINUE
C
      SUMFA = SUMF
      SUMXA = SUMX
C
      IF (KONV.EQ.1) GO TO 8
C
      IF  (ITER-ITMAX)  1, 1, 92
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C  REDUCTION OF RELAXATION FACTOR
C
6     CONTINUE
      IF  (KPRINT.GE.0)  WRITE(MSGFIL,1007) SUMX, FC, SUMF
      T = (DSQRT(SUMX/SUMXA)+FC-ONE)/FC
      FC = FC/(DSQRT(EIGHT*T+ONE)-ONE)
      IF  (FC.LT.FCMIN)  GO TO 62
      DO    61  I=1,N
            X(I) = XA(I)+FC*DX(I)
61          CONTINUE
      GO TO 1
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C  REDUCTION OF RANK
C
62    ITER = ITER-1
621   KT = -1
      IRANK = IRANK-1
      IF  (IRANK)  93,93,43
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C  PREPARATIONS TO START THE FOLLOWING ITERATION STEP
C
7     IDIV = IDIV +1
C
C  TEST ON LOCAL CONTRACTION OF MAPPING
C
      IF  (SUMXA.LT.SFC1)  IDIV = 0
      IF  (IDIV.GT.IDIVM)      GO TO 91
C
      IF  (KPRINT.LT.0)  GO TO 70
      WRITE(MSGFIL,1007)  SUMX, FC, SUMF
      IF  (KPRINT.LT.1)  GOTO 70
      WRITE(MSGFIL,1008)
      WRITE(MSGFIL,1003) (X(I), I=1,N)
      IF (KPRINT.LT.2) GOTO 70
      WRITE(MSGFIL,1009)
      WRITE(MSGFIL,1003) (F(I), I=1,M)
70    CONTINUE
      FCA = FC
      IF  ((IRANK.LT.N).AND.(IRANKA.GE.IRANK))  IC = IC + 1
      IF  (IC.GT.ICMAX)  GO TO 94
      IRANKA = IRANK
      IRANK = MIN0(IRKMAX,IRANK+1)
C
C  RESCALING
      DO    71  I=1,N
            DX1A(I) = DX1(I)
            T = (DABS(X(I))+DABS(XA(I)))*HALF
            IF  (T.LT.DABS(DX(I)))  GO TO 71
            IF  (T.LT.SMALL1)  T = ONE
            XW(I) = T
71          CONTINUE
      GO TO 2
C----------------------------------------------------------------------
C----------------------------------------------------------------------
C  SOLUTION EXIT
C
8     CONTINUE
C     IF  ( ITER.GT.1 .AND. FCA.NE.ONE )  GO TO 7
      EPS=TMIN
      INFO = IFCTEV
      IF  (KPRINT.LT.0)  RETURN
C
      WRITE(MSGFIL,1007) SUMX, FC, SUMF
      WRITE(MSGFIL,1010) ITER, INFO
      GOTO 82
81    WRITE(MSGFIL,1016)
82    WRITE(MSGFIL,1003) (X(I), I=1,N)
C
      WRITE(MSGFIL,1011) TMIN
      IF (FCA.EQ.ONE.AND.FC.EQ.ONE) SKAP = DSQRT(SUMXA/SFC1)
      IF (SKAP.GT.ZERO) GO TO 83
      WRITE(MSGFIL,1018)
      RETURN
83    WRITE(MSGFIL,1012) SKAP
      RETURN
C----------------------------------------------------------------------
C  FAIL EXIT
C
C  ITERATION DIVERGES
91    INFO = -1
      IF  (KPRINT.LT.0)  RETURN
      WRITE(MSGFIL,1013)
      GO TO 81
C
C  TERMINATION AFTER ITMAX ITERATIONS
92    INFO = -2
      IF  (KPRINT.LT.0)  RETURN
      ITER=ITER-1
      WRITE(MSGFIL,1014) ITER
      GO TO 81
C
C  RELAXATION-STRATEGY DOES NOT SUCCEED
93    INFO = -3
      IF  (KPRINT.LT.0)  RETURN
      WRITE(MSGFIL,1015)
      GO TO 81
C
C  MORE THAN ICMAX RANK-REDUCTIONS
94    INFO = -4
      IF  (KPRINT.LT.0)  RETURN
      IC=IC-1
      WRITE(MSGFIL,1017) IC
      GO TO 81
C
C======================================================================
C
1000  FORMAT(1H1)
1001  FORMAT(1H0,5X,'NUMBER OF COMPONENTS OF MODEL FUNCTION',I4,//,6X,
     1       'INITIAL GUESS OF PARAMETERS',/)
1003  FORMAT(1H ,D24.11)
1004  FORMAT(1H ,5X,'IT',4X,'LEVELX',4X,'RELFC',2X,'RANK',4X,
     1       'LEVELF',4X,'SUBCOND(J)',2X,'SENSITIVITY')
1005  FORMAT(1H ,5X,'NA',2X,D10.4,2X,F5.3,3X,I2,16X,D8.2)
1006  FORMAT(1H0,4X,I3,2X,D10.4,10X,I2,3X,D10.4,3X,D8.2,5X,D8.2)
1007  FORMAT(1H ,9X,D10.4,2X,F5.3,8X,D10.4)
1008  FORMAT(1H0)
1009  FORMAT(1H0,5X,'RESIDUAL VECTOR',/)
1010  FORMAT(1H1,//,6X,'SOLUTION OBTAINED AFTER',I3,3X,'ITERATIONS BY'
     1      ,I4,3X,'FUNCTION EVALUATIONS',//// ,6X,'SOLUTION ',/)
1011  FORMAT(1H0,//,6X,'FINALLY ACHIEVED ACCURACY :       EPS = ',2X,
     1       D10.3)
1012  FORMAT(1H0,   5X,'INCOMPATIBILITY FACTOR    :  ',F10.6)
1013  FORMAT(1H1,//,6X,'TERMINATION, SINCE ITERATION DIVERGES',/ ,6X,
     1       'INITIAL GUESS TOO BAD OR MODEL TOO FAR FROM ',
     2       'BEING COMPATIBLE',
     3       //// )
1014  FORMAT(1H1,// ,6X,'USER-PRESCRIBED TERMINATION AFTER ',
     1       I3,' ITERATIONS')
1015  FORMAT(1H1,//,6X,'TERMINATION SINCE RELAXATION STRATEGY DID NOT'
     1      ,'SUCCEED')
1016  FORMAT(1H0,5X,'FINAL PARAMETER ITERATES',/)
1017  FORMAT(1H1,// ,6X,'TERMINATION SINCE MORE THAN  ',I4,
     1       ' RANK-REDUCTIONS PERFORMED',// )
1018  FORMAT(1H0,   5X,'INCOMPATIBILITY FACTOR    :  NOT AVAILABLE')
C
C  **********  LAST CARD OF NLSQN  **********
C
      END
      SUBROUTINE DECOMP (A,M,N,IRANK,COND,D,PIVOT,KRED,AH,V)
C----------------------------------------------------------------------
C
C  DECOMPOSITION OF AN (M,N)-MATRIX A (M.GE.N)
C  USING HOUSEHOLDER TRIANGULARIZATION
C  (AND CHOLESKY DECOMPOSITION, IF IRANK.LT.N)
C  TO BE USED IN CONNECTION WITH SUBROUTINE SOLVE
C
C  ********** REVISION 1 ********** LATEST CHANGE: JULY 1ST, 81 (RM)
C
C REFERENCES:
C (1) P. BUSINGER,G.H. GOLUB:
C     LINEAR LEAST SQUARES SOLUTIONS BY HOUSEHOLDER TRANSFORMATIONS
C     NUMER. MATH. 7,269-276 (1965)
C (2) P. DEUFLHARD:
C     A MODIFIED NEWTON METHOD FOR THE SOLUTION OF ILL-CONDITIONED
C     SYSTEMS OF NONLINEAR EQUATIONS ...
C     NUMER. MATH. 22,289-315 (1974)
C (3) P. DEUFLHARD, W. SAUTTER:
C     ON RANK-DEFICIENT PSEUDO-INVERSES
C     J.LIN.ALG.APPL.(1980)
C
C  INPUT PARAMETERS (* MARKS TRANSIENT PARAMETERS)
C
C  * A(M,N)        INPUT MATRIX
C    M             NUMBER OF ROWS OF MATRIX A
C    N             NUMBER OF COLUMNS OF MATRIX A (.LE.M)
C (*)IRANK         PSEUDO-RANK OF MATRIX A
C                  (IF IRANK.LE.0 RETURN )
C    COND          PERMITTED SUB-CONDITION NUMBER DABS(D(1)/D(IRANK))
C                  (LOWER BOUND OF CONDITION NUMBER OF A)
C    KRED          .GE.0     HOUSEHOLDER TRIANGULARIZATION
C                            (BUILD-UP OF RANK-DEFICIENT PSEUDO-
C                            INVERSE, IF IRANK.LT.N; STORE AH USED
C                            FOR COMPUTATIONAL SAVINGS)
C                  .LT.0     REDUCTION OF PSEUDO-RANK OF MATRIX A
C                            SKIPPING HOUSEHOLDER TRIANGULARIZATION
C                            BUILD-UP OF NEW PSEUDO-INVERSE
C
C  OUTPUT PARAMETERS
C
C    A(M,N)        OUTPUT MATRIX UPDATING PRODUCT OF HOUSEHOLDER
C                  TRANSFORMATIONS AND UPPER TRIANGULAR MATRIX
C   (IRANK)        NEW PSEUDO-RANK OF MATRIX A
C                  DETERMINED SO THAT DABS(D(1)/D(IRANK)).LT.COND
C    D(N)          DIAGONAL ELEMENTS OF UPPER TRIANGULAR MATRIX
C                  ORDERED WITH RESPECT TO ABSOLUTE VALUE
C    PIVOT(N)      INDEX-VECTOR STORING PERMUTATION OF COLUMNS DUE TO
C                  PIVOTING
C    AH(N,N)       MATRIX UPDATING PSEUDO-INVERSE OF MATRIX A
C                  (ONLY USED, IF IRANK.LT.N)
C
C----------------------------------------------------------------------
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C         |
C         |   ADJUST DIMENSIONS TO THOSE IN THE CALLING PROGRAM
C         V
      DIMENSION A(M,N),AH(N,N),V(N),D(N)
      INTEGER PIVOT(N)
C
      DATA  ZERO/0.D0/ , ONE/1.D0/
C
C RELATIVE MACHINE PRECISION
C (ADAPTED TO IBM 370/168, UNIVERSITY OF HEIDELBERG)
      EPMACH = 1.D-16
C
      SMALL = DSQRT(EPMACH*1.D1)
C
      IF(IRANK.LE.0) RETURN
      IF(IRANK.GT.N) IRANK=N
C
      IF(KRED.LT.0) GOTO 2
      DO 10 J=1,N
10    PIVOT(J)=J
C
C  HOUSEHOLDER TRIANGULARIZATION DUE TO BUSINGER-GOLUB
C  (COLUMN PIVOTING MODIFIED DUE TO BJOERCK)
      K1=1
      JD=1
1     K=K1
      IF(K.EQ.N) GOTO 12
      K1=K+1
      IF(JD.EQ.0) GOTO 111
11    DO 110 J=K,N
      S=ZERO
      DO 1101 I=K,M
      T=A(I,J)
1101  S=S+T*T
110   D(J)=S
C  COLUMN PIVOTING
111   H=D(K)
      JJ=K
      DO 112 J=K1,N
      S=D(J)
      IF(S.LE.H) GOTO 112
      H=S
      JJ=J
112   CONTINUE
      IF(JD.EQ.1) HMAX=H*SMALL
      JD=0
      IF(H.GE.HMAX) GOTO 113
      JD=1
      GOTO 11
113   IF(JJ.EQ.K) GOTO 12
C  COLUMN INTERCHANGE
      I=PIVOT(K)
      PIVOT(K)=PIVOT(JJ)
      PIVOT(JJ)=I
      D(JJ)=D(K)
      DO 115 I=1,M
      T=A(I,K)
      A(I,K)=A(I,JJ)
115   A(I,JJ)=T
C
12    H=ZERO
      DO 121 I=K,M
      T=A(I,K)
121   H=H+T*T
      T=DSQRT(H)
C  A-PRIORI TEST ON PSEUDO-RANK
      IF(K.EQ.1) DD=T/COND
      IF(T.GT.DD.AND.K.LE.IRANK) GOTO 14
      IRANK=K-1
      IF(IRANK.EQ.0) RETURN
      GOTO 2
14    S=A(K,K)
      IF(S.GT.ZERO) T=-T
      D(K)=T
      A(K,K)=S-T
      IF(K.EQ.N) RETURN
      T=ONE/(H-S*T)
      DO 15 J=K1,N
      S=ZERO
      DO 151 I=K,M
151   S=S+A(I,K)*A(I,J)
      S=S*T
      DO 152 I=K,M
152   A(I,J)=A(I,J)-A(I,K)*S
      S=A(K,J)
15    D(J)=D(J)-S*S
      IF(K.LT.IRANK) GOTO 1
C
C  BUILD-UP OF PSEUDO-INVERSE BY CHOLESKY DECOMPOSITION
C
2     IRK1=IRANK+1
      DO 20 J=IRK1,N
      DO 21 II=1,IRANK
      I=IRK1-II
      S=A(I,J)
      IF(II.EQ.1) GOTO 210
      DO 2111 JJ=I1,IRANK
2111  S=S-A(I,JJ)*V(JJ)
210   I1=I
      V(I)=S/D(I)
21    AH(I,J)=V(I)
      DO 22 I=IRK1,J
      S=ZERO
      I1=I-1
      DO 221 JJ=1,I1
221   S=S+AH(JJ,I)*V(JJ)
      IF(I.EQ.J) GOTO 22
      V(I)=-S/D(I)
      AH(I,J)=-V(I)
22    CONTINUE
20    D(J)=DSQRT(S+ONE)
C
C  END DECOMP
C
      RETURN
      END
      SUBROUTINE  SOLVE (A,X,B,M,N,IRANK,D,PIVOT,KRED,AH,BH,V)
C----------------------------------------------------------------------
C  DETERMINATION OF BEST LSQ-SOLUTION OF THE LINEAR (M,N)-SYSTEM
C                            AX=B
C  USING DECOMPOSITION OF MATRIX A
C  AS OBTAINED FROM SUBROUTINE DECOMP
C
C  ********** REVISION 1 ********** LATEST CHANGE: JAN.29,1980 (DD,CPP)
C
C  REFERENCES: SEE SUBROUTINE DECOMP
C
C
C  INPUT PARAMETERS (* MARKS TRANSIENT PARAMETERS)
C
C    A(M,N)        SEE OUTPUT OF DECOMP
C  * B(M)          RIGHT-HAND SIDE OF LINEAR SYSTEM
C    M             SEE  INPUT OF DECOMP
C    N             SEE  INPUT OF DECOMP
C    IRANK         SEE OUTPUT OF DECOMP
C    D(N)          SEE OUTPUT OF DECOMP
C    PIVOT(N)      SEE OUTPUT OF DECOMP
C    KRED          SEE  INPUT OF DECOMP
C                  .EQ.0 :STORAGE BH(N) USED FOR COMPUTATIONAL SAVINGS
C    AH(N,N)       SEE OUTPUT OF DECOMP
C
C  OUTPUT PARAMETERS
C
C    X(N)          BEST LSQ-SOLUTION OF LINEAR SYSTEM
C    BH(N)         VECTOR STORING A CERTAIN INTERMEDIATE RESULT
C                  USED FOR KRED=-1
C                  (REDUCTION OF PSEUDO-RANK)
C----------------------------------------------------------------------
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION A(M,N),AH(N,N),B(M),X(N),BH(N),D(N),V(N)
      INTEGER PIVOT(N)
C
      DATA ZERO/0.D0/
C
      IF(IRANK.LT.0) RETURN
      IF(IRANK.GT.0) GOTO 100
C
C  SOLUTION FOR PSEUDO-RANK ZERO
      DO 10 I=1,N
10    X(I)=ZERO
      RETURN
C
C  COMPUTATIONAL SIMPLIFICATION FOR PSEUDO-RANK REDUCTION
100   IF(KRED.GE.0) GOTO 1
      DO 101 I=1,IRANK
101   B(I)=BH(I)
      GOTO 2
C
C  HOUSEHOLDER TRANSFORMATIONS OF THE RIGHT-HAND SIDE
1     DO 11 J=1,IRANK
      S=ZERO
      DO 111 I=J,M
111   S=S+A(I,J)*B(I)
      S=S/(D(J)*A(J,J))
      DO 112 I=J,M
112   B(I)=B(I)+A(I,J)*S
      IF(KRED.EQ.0) BH(J)=B(J)
11    CONTINUE
C
C  SOLUTION OF THE UPPER TRIANGULAR SYSTEM
2     IRK1=IRANK+1
      DO 21 II=1,IRANK
      I=IRK1-II
      S=B(I)
      IF(II.EQ.1) GOTO 210
      DO 2111 JJ=I1,IRANK
2111  S=S-A(I,JJ)*V(JJ)
210   I1=I
21    V(I)=S/D(I)
      IF(IRANK.EQ.N) GOTO 3
C
C  COMPUTATION OF THE BEST LSQ-SOLUTION
      DO 221 J=IRK1,N
      S=ZERO
      J1=J-1
      DO 2211 I=1,J1
2211  S=S+AH(I,J)*V(I)
221   V(J)=-S/D(J)
      N1=N+1
      DO 222 JJ=1,N
      J=N1-JJ
      S=ZERO
      IF(JJ.EQ.1) GOTO 2222
      DO 2221 I=J1,N
2221  S=S+AH(J,I)*V(I)
      IF(J.LE.IRANK) GOTO 2223
2222  J1=J
      S=-(V(J)+S)/D(J)
      GOTO 222
2223  S=V(J)-S
222   V(J)=S
C
C  BACK-PERMUTATION OF SOLUTION COMPONENTS
3     DO 30 J=1,N
      I=PIVOT(J)
30    X(I)=V(J)
C
C  END SOLVE
C
      RETURN
      END
