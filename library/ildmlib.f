C***********************************************************************
C***********************************************************************
C***********************************************************************
C
C     ugeevs now called with permutation option
C
C***********************************************************************
C***********************************************************************
C***********************************************************************
      SUBROUTINE EGVBAS(JOB,JOBJAC,MDIM,RDIM,NZEV,NEQ,RHS,JACRHS,
     1       TIME,SMF,ERVCOL,ERVROW,TRACOL,TRAROW,
     1       AR,AI,AVR,AVL,XJAC,SCHUR,RPAR,IPAR,IPRI,MSGFIL,IERR)
C***********************************************************************
C
C   Input:
C
C    JOB         : control parameter
C
C                    real parts of the eigenvalues appear in decreasing
C                    order along the diagonal
C
C                    JOB <  0  calculate right and left eigenvalues
C                    JOB =  0  calculate only the Jacobian
C                    JOB >  0  calculate Schur vectors and inv. subsp.
C
C
C                   |JOB|=  5  put zero eigenvalues at the bottom
C                   |JOB|=  4  do analysis for J J^T
C   JOBJAC       : calculation of Jacobian
C                  0: analytical
C                  1: numerical
C    RHS    name of subroutine for right hand side
C    JACRHS name of analytical Jacobian
C
C  MDIM    (input/output)
C          on input MDIM gives the desired dimension of the subspace 1
C          on output it gives the final dimension, which is given
C          by MDIM - 1 if the separating eigenvalue is complex
C          and by MDIM otherwise
C
C
C    IPRI                : control parameter for output
C                           IPRI = 0 no printed output
C                           IPRI = 1 print eigenvalues
C                           IPRI = 2 print eigenvalues and eigenvectors
C
C    MSGFIL              : FORTRAN unit for output
C
C    NEQ                 : dimension of the system
C
C    SMF                 : values of dependent variables
C
C    ERVCOL              : transformation matrices into conserved and
C    ERVROW                reactive variables
C
C
C   Output:
C
C    TRACOL              : basis of right eigenvectors
C    TRAROW              : basis of left eigenvectors
C    AR,AI               : eigenvalues
C    AVR,AVL             : eigenvectors
C    XJAC                : Jacobian of the system
C    IERR                : error flag
C
C   Arrays for user communication with FCN:
C
C    TIME,RPAR(*),IPAR(*)
C
C   Work arrays:
C
C    VRS(NEQ),WL1(NEQ),DEL(NEQ),IW(LIW),RW(LRW)
C
C***********************************************************************
C***********************************************************************
C     STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      EXTERNAL RHS,JACRHS
      CHARACTER*1 DIR,ZEV
      LOGICAL TRAN
      DOUBLE PRECISION, DIMENSION(NEQ)                 :: VRS,WL1,DEL
      DOUBLE PRECISION, DIMENSION(NEQ,NEQ)             :: TEMP
      DOUBLE PRECISION, DIMENSION(2*NEQ*NEQ)           :: RW
      INTEGER,          DIMENSION(2*NEQ)               :: IW
      LOGICAL,          DIMENSION(NEQ)                 :: TW
      DIMENSION SMF(NEQ)
      DIMENSION ERVCOL(NEQ,NEQ),ERVROW(NEQ,NEQ)
      DIMENSION TRACOL(NEQ,NEQ),TRAROW(NEQ,NEQ)
      DIMENSION AR(NEQ),AI(NEQ),AVR(NEQ,NEQ),AVL(NEQ,NEQ),XJAC(NEQ,NEQ)
      DIMENSION RPAR(*),IPAR(*)
      DIMENSION SCHUR(NEQ,NEQ)
C***********************************************************************
C     Initial preparations
C***********************************************************************
      LRW = 2 * NEQ*NEQ
      LIW = 2 * NEQ
      LTW =     NEQ
      IERR=0
C***********************************************************************
C     Set control parameters
C***********************************************************************
      NN = NEQ * NEQ
      IF(JOB.LT.0)       JOBHV =  -1
      IF(JOB.EQ.0)       JOBHV =   0
      IF(JOB.GT.0)       JOBHV =   1
        DIR = 'D'
        ZEV = 'T'
      IF(IABS(JOB).EQ.5) THEN
        DIR = 'D'
        ZEV = 'B'
      ENDIF
      TRAN = .FALSE.
C----- this is for the MMT
      IF(IABS(JOB).EQ.4) THEN
        DIR = 'A'
        ZEV = 'N'
        TRAN = .TRUE.
      ENDIF
C***********************************************************************
C     COPY SMF into VRS and calculate Jacobian
C***********************************************************************
      IOB = 0
      VRS = SMF
      IF(JOBJAC.EQ.0) THEN
C---- calculate Jacobian
      CALL JACRHS(NEQ,TIME,VRS,WL1,XJAC,RPAR,IPAR,IERJAC)
      ELSE
C---- not tested, added for slava
      CALL HOMJAC(IOB,RHS,NEQ,TIME,VRS,WL1,XJAC,ERVROW,
     1     ERVCOL,RPAR,IPAR,MSGFIL,IERJAC)
      ENDIF
      IF(IERJAC.GT.0) GOTO 910
C***********************************************************************
C     Exit if invariant subspaces are not computed
C***********************************************************************
      IF(JOB.EQ.0)  GOTO 600
C***********************************************************************
C     copy Jacobian into Schur Matrix
C***********************************************************************
      IF(TRAN) THEN
        SCHUR = MATMUL(XJAC,TRANSPOSE(XJAC))
C       SCHUR = (XJAC+TRANSPOSE(XJAC))/2.0
C       XJAC  = SCHUR
      ELSE
        SCHUR = XJAC
      ENDIF
C***********************************************************************
C     Eigenvalue decomposition
C***********************************************************************
      IF(MDIM.NE.NZEV) THEN
      IEREGV = IPRI
      RDIM = 0
c     IEREGV = 1
      CALL HOMEVC(JOBHV,NEQ,SCHUR,NEQ,MDIM,RDIM,DIR,ZEV,
     $             AR,AI,TRAROW,NEQ,TRACOL,NEQ,AVR,NEQ,SRCONE,SRCONV,
     $        RW,LRW,IW,LIW,TW,LTW,RPAR,IPAR,MSGFIL,IEREGV)
      IF(IEREGV.GT.0) GOTO 920
      IF(JOBHV.LT.0) CALL DCOPY(NN,TRACOL,1,AVR,1)
      ELSE
C***********************************************************************
C     Store ERV in TRA
C***********************************************************************
      TRACOL = ERVCOL
      TRAROW = ERVROW
      AVR    = ERVCOL
C***********************************************************************
C     Get matrix ERVROW * JAC * ERVCOL
C***********************************************************************
      TEMP  =MATMUL(XJAC,ERVCOL)
      SCHUR= MATMUL(ERVROW,TEMP)
      ENDIF
C***********************************************************************
C     update
C***********************************************************************
      AVL = AVR
      CALL DGEINV(NEQ,AVL,RW,IW,IERUPD)
      IF(IERUPD.NE.0) GOTO 980
C***********************************************************************
C     SOLUTION EXIT
C***********************************************************************
  600 CONTINUE
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
  910 CONTINUE
      IERR = IERJAC
      WRITE(MSGFIL,911) IERR
  911 FORMAT(' ',' Computation of Jacobian failed, IERR =',I3)
      IERR = 1
      GOTO 999
  920 CONTINUE
      WRITE(MSGFIL,921) IEREGV
  921 FORMAT(' ',' Computation of eigenvalues failed, IERR =',I3)
      IERR = 2
      GOTO 999
C 930 CONTINUE
C     WRITE(MSGFIL,931)
C 931 FORMAT(' ',' Storage problems                         ')
C     IERR = 3
C     GOTO 999
  980 CONTINUE
      WRITE(MSGFIL,981) IERUPD
  981 FORMAT(' ',' Update of transformation matrix failed, IERUPD =',I3)
      IERR = 8
      GOTO 999
  999 CONTINUE
C     WRITE(MSGFIL,998)
C 998 FORMAT(/,3(' ',' +++++++ ERROR IN EGVBAS +++++++',/))
      RETURN
      END
      SUBROUTINE UPDBAS(INFO,NEQ,AROW,ACOL,HELP,IPVT,IERR)
C***********************************************************************
C                                                                      *
C     *************************************************************    *
C     *                                                           *    *
C     *    UPDATE TRANSFORMATION MATRIX                           *    *
C     *                                                           *    *
C     *************************************************************    *
C                                                                      *
C***********************************************************************
C
C   Input:
C
C    INFO                : control parameter
C
C                          INFO = 0: TRACOL is a non-singular matrix
C                          INFO = 1: TRACOL is a unitary matrix
C
C    NEQ                 : dimension of the system
C
C    ACOL(NEQ,NEQ)       : transformation matrices
C
C
C   Output:
C
C    AROW(NEQ,NEQ)       : inverse of ACOL
C    ACOL(NEQ,NEQ)       : unchanged on exit
C
C    IERR                : 0 for regular exit
C                          > 0 if error has occured
C
C   Work arrays:
C
C    HELP(NEQ),IPVT(NEQ)
C
C***********************************************************************
C***********************************************************************
C     STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION AROW(NEQ,NEQ),ACOL(NEQ,NEQ),HELP(NEQ),IPVT(NEQ)
C**********************************************************************C
C     INITIALIZE
C**********************************************************************C
      IERR = 0
      NN=NEQ*NEQ
      IF(INFO.EQ.0) GOTO 100
      IF(INFO.EQ.1) GOTO 200
                    GOTO 900
C***********************************************************************
C     COMPUTE INVERSE                                                  *
C***********************************************************************
  100 CONTINUE
      CALL DCOPY(NN,ACOL,1,AROW,1)
      CALL DGEINV(NEQ,AROW,HELP,IPVT,ILL)
      IF(ILL.GT.0) GOTO 910
      GOTO 700
C***********************************************************************
C     COMPUTE TRANSPOSE                                                *
C***********************************************************************
  200 CONTINUE
      DO 210 I = 1,NEQ
      DO 210 J = 1,NEQ
      AROW(I,J) = ACOL(J,I)
  210 CONTINUE
      GOTO 700
C***********************************************************************
C     REGULAR EXIT
C***********************************************************************
  700 CONTINUE
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
  900 CONTINUE
      IERR = 1
      RETURN
  910 CONTINUE
      IERR = 2
      RETURN
C***********************************************************************
C     END OF -UPDBAS-
C***********************************************************************
      END
      SUBROUTINE HOMJAC(INFO,RHS,N,T,Y,DEL0,A,ROW,COL,
     1      RPAR,IPAR,MSGFIL,IERR)
C***********************************************************************
C
C     ************************************************************
C     *       CALCULATE JACOBIAN CORRESPONDING TO THE SYSTEM     *
C     *                     D/Dt(Z)=F(Z,t)                       *
C     ************************************************************
C
C***********************************************************************
C
C   INPUT:
C
C     INFO:         task
C                     INFO = 0   ---  compute Jacobian  dF(Y)/dY
C                     INFO = 1   ---  compute transformed Jacobian
C                    if INFO = 1 then the following task is performed:
C                     suppose the transformation  M * Y = Z
C                     and the system dz/dT = f(z,T) (computed by RHS)
C                     In this case the routine returns the transformed
C                     Jacobian:    M**(-1) * dF(M*Y) / dY
C                   <<or simply spoken just M**(-1) dF/dz M ??>>
C     RHS:        external user-supplied subroutine
C     N:          dimension of the system
C     T:          value of independent variable
C     Y(N)        dependent variable array
C     COL(N,N)    transformation matrix M      (not ref. for INFO = 0)
C     ROW(N,N)    transformation matrix M^(-1) (not ref. for INFO = 0)
C     MSGFIL      fortran file number for error messages
C
C  Output:
C
C     IERR          0 on regular exit
C                   changed to a positive value if an error occured
C     DEL0(N)       residual for input array Y
C     A(N,N)        Jacobian
C                   A(i,j) = dF(i)/dY(j)
C  Work arrays:
C
C
C  Arrays for user communication (not changed):
C
C     RPAR(*), IPAR(*)
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      EXTERNAL RHS
      DIMENSION Y(N),DEL0(N),A(N,N)
      DOUBLE PRECISION, DIMENSION(N) :: WRK,DEL
      DIMENSION ROW(*),COL(*),RPAR(*),IPAR(*)
C***********************************************************************
C     Initial values
C***********************************************************************
      DATA ZERO/0.0D0/,ETAD/1.D-6/,ATOL/1.D-6/,ONE/1.0D0/
C***********************************************************************
C     initial checks
C***********************************************************************
      IERR = 0
      IF(INFO.NE.0.AND.INFO.NE.1) GOTO 900
C***********************************************************************
C     Calculate residual (unperturbed)
C***********************************************************************
      IF(INFO.EQ.0) THEN
        CALL RHS(N,T,Y,DEL0,RPAR,IPAR,IERHS)
        IF(IERHS.GT.0) GOTO 910
      ELSE
        CALL DGEMV('N',N,N,ONE,COL,N,Y,1,ZERO,DEL,1)
        CALL RHS(N,T,DEL,WRK,RPAR,IPAR,IERHS)
        IF(IERHS.GT.0) GOTO 910
        CALL DGEMV('N',N,N,ONE,ROW,N,WRK,1,ZERO,DEL0,1)
      ENDIF
C***********************************************************************
C     Loop over Columns
C***********************************************************************
      DO 100 K=1,N
      U=ETAD*DMAX1(DABS(Y(K)),ATOL)
CNEW
C     U= -U
C     IF(DEL0(K).LT.ZERO) U=-U
C     IF(DEL0(K).LT.ZERO.AND.(.NOT.(Y(K).LT.U))) U=-U
      IF(INFO.EQ.0) THEN
        W=Y(K)
        Y(K)=W+U
        CALL RHS(N,T,Y,DEL,RPAR,IPAR,IERHS)
        IF(IERHS.GT.0) GOTO 910
        Y(K)=W
      ELSE
        W=Y(K)
        Y(K)=W+U
        CALL DGEMV('N',N,N,ONE,COL,N,Y,1,ZERO,DEL,1)
        CALL RHS(N,T,DEL,WRK,RPAR,IPAR,IERHS)
        IF(IERHS.GT.0) GOTO 910
        CALL DGEMV('N',N,N,ONE,ROW,N,WRK,1,ZERO,DEL,1)
        Y(K)=W
      ENDIF
      DO 50 I=1,N
  50  A(I,K)=(DEL(I)-DEL0(I))/U
 100  CONTINUE
      RETURN
C***********************************************************************
C     error exits
C***********************************************************************
 900  IERR =  1
      RETURN
 910  IERR =  2
      WRITE(MSGFIL,911)
  911 FORMAT(' Error detected in RHS evaluation called by HOMJAC ')
      RETURN
C***********************************************************************
C     End of -HOMJAC-
C***********************************************************************
      END
      SUBROUTINE HOMEVC(JOB,N,A,LDA,MDIM,RDIM,DIR,ZEV,
     $             WR,WI,VL,LDVL,VR,LDVR,VS,LDVS,SRCONE,SRCONV,
     $        RWORK,LRW,IWORK,LIW,TWORK,LTW,RPAR,IPAR,MSGFIL,INFO)
C***********************************************************************
C
C     ************************************************************
C     *                                                          *
C     *       Driver routine for the calculation of              *
C     *                Invariant Subspaces                       *
C     *                                                          *
C     *                    Ulrich Maas 28.2.1994                 *
C     *                                                          *
C     ************************************************************
C
C***********************************************************************
C     ..
C
C  HOMEVC
C  =======
C
C  HOMEVC computes for an N-by-N real nonsymmetric matrix A, the
C  eigenvalues, the Schur vectors and the invariant subspaces.
C
C
C  Arguments
C  =========
C
C  JOB     (input) INTEGER
C          task
C          JOB = -1  calculate right and left eigenvalues
C          JOB =  0  calculate Schur vectors
C          JOB =  1  calculate Schur vectors and invariant subsp.
C
C  N       (input) INTEGER
C          dimension of the system
C
C  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
C          On entry, the N-by-N matrix A.
C          On exit, A has been overwritten by the matrix N of the
C          Schur decomposition, where M * Q = Q * N
C
C  LDA     (input) INTEGER
C          The leading dimension of the array A.  LDA >= max(1,N).
C
C  MDIM    (input/output)
C          on input MDIM gives the desired dimension of the subspace 1
C          on output it gives the final dimension, which is given
C          by MDIM - 1 if the separating eigenvalue is complex
C          and by MDIM otherwise
C
C  DIR     (input) CHARACTER
C          'N'  eigenvalues are not sorted
C          'A'  eigenvalues are sorted according to ascending real part
C          'D'  eigenvalues are sorted according to descending real part
C
C  ZEV     (input) CHARACTER
C          'T'  zero eigenvalues appear at the top
C          'B'  zero eigenvalues appear at the bottom
C          'N'  zero eigenvalues are not separated explicitely
C
C  ISORT   (input) INTEGER
C          Info flag for subroutine xelect
C
C  XELECT  (input) EXTERNAL
C          Name of the subroutine which gives the info flags for
C          sorting the Schur vectors.
C          If sorting of the Schur vectors is desired, the user has
C          to supply a subroutine
C
C     SUBROUTINE XELECT(NOS,ISORT,MDIM,RDIM,WR,WI,N,Q,
C                                      LDVS,IWORK,RPAR,IPAR)
C                  RPAR(*) double precision array for user
C                          communication with XELECT
C                  IPAR(*) integer array for user
C                          communication with XELECT
C                       RPAR and IPAR are not touched
C                  Q(LDVS,N) matrix of Schur vectors. This is
C                          included for use in the subroutine.
C                          However, Q must NOT be changed by XELECT
C           On output IWORK (integer of dimension N) has to specify
C           the desired order of the eigenvalues in the Schur
C           decomposition.
C           IWORK(I) = K means: eigenvalue I has to be at position K.
C           IWORK(I) = 0 means: eigenvalue shall be in the lower
C                        right of the matrix.
C            IWORK = 3, 0, 2, 1 e.g., means that the eigenvalues
C            should be ordered such the first one has the final
C            position 3, the second is swapped to the end,
C            the third one is swapped to the second position
C            and the 4th one is swapped to the second position.
C            Note that 3, 4, 2, 1 would produce the same result
C            in this case
C
C  WR      (output) DOUBLE PRECISION array, dimension (N)
C  WI      (output) DOUBLE PRECISION array, dimension (N)
C          WR and WI contain the real and imaginary parts,
C          respectively, of the computed eigenvalues.  Complex
C          conjugate pairs of eigenvalues will appear consecutively
C          with the eigenvalue having the positive imaginary part
C          first.
C
C  VR      (output) DOUBLE PRECISION array, dimension (LDVR,N)
C          VR contains in its first 1:MDIM columns a set of vectors
C          spanning the eigenspace associated with the eigenvalues
C          1:MDIM, and in its MDIM+1:N columns a set of vectors
C          spanning the eigenspace associated with the eigenvalues
C          MDIM+1,N, where MDIM is the corrected dimension of
C          the subspace 1 (see above)
C          IF JOB=-1 VR simply contains the right eigenv. (see UGEESX)
C
C  LDVR    (input) INTEGER
C          The leading dimension of the array VR.  LDVR >= N
C
C  VL      (output) DOUBLE PRECISION array, dimension (LDVL,N)
C          VL contains the inverse of VR
C          IF JOB=-1 VL simply contains the left eigenv. (see UGEESX)
C
C  LDVL    (input) INTEGER
C          The leading dimension of the array VL.  LDVL >= N
C
C  VS      (output) DOUBLE PRECISION array, dimension (LDVS,N)
C          VS contains the Schur vectors after ordering of the
C          eigenvectors
C
C  LDVS    (input) INTEGER
C          The leading dimension of the array VS.  LDVS >= N
C
C  RWORK   (workspace/output) DOUBLE PRECISION array, dimension (LRW)
C          On exit, if INFO = 0, RWORK(1) returns the optimal LRW.
C
C  LRW     (input) INTEGER
C          The dimension of the array WORK.
C          LRW >= 3*N. LRW is checked
C          For good performance, LRW must generally be larger.
C
C  IWORK   (workspace) INTEGER array, dimension (LIW)
C
C  LIW     (input) INTEGER
C          The dimension of the array IWORK.
C          Set LIW at least to N
C
C  TWORK   (workspace) LOGICAL array, dimension (LTW)
C
C  LTW     (input) INTEGER
C          The dimension of the array TWORK.
C          Set LTW at least to N
C
C
C
C  INFO    (input/output) INTEGER
C         On input INFO > 0 the test output flag is set to true
C         On output
C          = 0:  successful exit
C          < 0:  if INFO = -i, the i-th argument had an illegal value.
C          > 0:  if INFO = i, the QR algorithm failed to compute all the
C                eigenvalues, and no eigenvectors or condition numbers
C                have been computed; elements 1:ILO-1 and i+1:N of WR
C                and WI contain eigenvalues which have converged.
C          = N+1: the eigenvalues could not be reordered because some
C                   eigenvalues were too close to separate (the problem
C                   is very ill-conditioned);
C          = N+2: after reordering, roundoff changed values of some
C                 complex eigenvalues so that leading eigenvalues in
C                 the Schur form no longer satisfy XELECT=.TRUE. This
C                 could also be caused by underflow due to scaling.
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      EXTERNAL XELECT
      DIMENSION A(LDA,*),WR(N),WI(N),VL(LDVL,*),VR(LDVR,*),VS(LDVS,*)
      DIMENSION RPAR(*),IPAR(*)
      LOGICAL TWORK(LTW),LSAME
      DIMENSION RWORK(LRW),IWORK(LIW)
      LOGICAL TEST
      CHARACTER*1 BALANC,JOBVL,JOBVR,JOBVS,SENSE,DIR,ZEV
      PARAMETER (ONE=1.0D0,ZERO=0.0D0)
C***********************************************************************
C
C     Initialize control parameters
C
C***********************************************************************
C***********************************************************************
C     Decide about task
C***********************************************************************
      IF(JOB.EQ.0) THEN
        JOBVL  = 'N'
        JOBVR  = 'N'
        JOBVS  = 'V'
      ELSE IF (JOB.EQ.-1) THEN
        JOBVL  = 'V'
        JOBVR  = 'V'
        JOBVS  = 'V'
      ELSE IF(JOB.EQ.1) THEN
        JOBVL  = 'N'
        JOBVR  = 'N'
        JOBVS  = 'I'
      ELSE
      GOTO 910
      ENDIF
C***********************************************************************
C     Determine flag for testing
C***********************************************************************
      TEST = .FALSE.
      IF(INFO.GT.1) TEST = .TRUE.
      IPRIEV = INFO
      info = 0
      IF(TEST) THEN
        NTEST = N * N
        SENSE  = 'B'
        NCONDE = N
        NCONDV = N
      ELSE
        NTEST = 1
        SENSE  = 'N'
        NCONDE = N
        NCONDV = N
      ENDIF
      TEST = .FALSE.
C***********************************************************************
C     Determine sorting flag
C***********************************************************************
           IF(LSAME(DIR,'N').AND.LSAME(ZEV,'N')) THEN
                                                ISORT =  0
      ELSE IF(LSAME(DIR,'N').AND.LSAME(ZEV,'T')) THEN
                                                ISORT =  1
      ELSE IF(LSAME(DIR,'N').AND.LSAME(ZEV,'B')) THEN
                                                ISORT = -1
      ELSE IF(LSAME(DIR,'D').AND.LSAME(ZEV,'N')) THEN
                                                ISORT =  2
      ELSE IF(LSAME(DIR,'A').AND.LSAME(ZEV,'N')) THEN
                                                ISORT = -2
      ELSE IF(LSAME(DIR,'D').AND.LSAME(ZEV,'T')) THEN
                                                ISORT =  3
      ELSE IF(LSAME(DIR,'A').AND.LSAME(ZEV,'T')) THEN
                                                ISORT = -3
      ELSE IF(LSAME(DIR,'D').AND.LSAME(ZEV,'B')) THEN
                                                ISORT =  4
      ELSE IF(LSAME(DIR,'A').AND.LSAME(ZEV,'B')) THEN
                                                ISORT = -4
C-Sl- Additional sorting by absolute value------------------------------
      ELSE IF(LSAME(DIR,'S').AND.LSAME(ZEV,'S')) THEN
                                                ISORT =  5
C-Sl- End of sort definition--------------------------------------------
      ELSE
        GOTO 920
      ENDIF
      BALANC = 'P'
C***********************************************************************
C     Setup pointers for work space and check dimensions
C***********************************************************************
C---- real work space
      ITEST  = 1
      ISCALE = ITEST  + NTEST
      ICONDE = ISCALE + N
      ICONDV = ICONDE + NCONDE
      NEXT   = ICONDV + NCONDV
      IPW    = NEXT
      LEFTRW = LRW - NEXT + 1
      IF(LEFTRW.LT.1) THEN
        WRITE(6,*) ' REAL WORK ARRAY TOO SMALL IN HOMEVC '
        STOP
      ENDIF
      IF(LTW.LT.N) THEN
        WRITE(6,*) ' LOGICAL WORK ARRAY TOO SMALL IN HOMEVC '
        STOP
      ENDIF
C***********************************************************************
C     store original matrix for testing
C***********************************************************************
      IF(TEST) CALL DLACPY('F',N,N,A,LDA,RWORK(ITEST),N)
C***********************************************************************
C     Call eigenvalue routine
C***********************************************************************
      MDIMI = MDIM
      CALL UGEEVS(BALANC,JOBVL,JOBVR,JOBVS,SENSE,ISORT,XELECT,MDIM,RDIM,
     $       N,A,LDA,WR,WI,VL,LDVL,VR,LDVR,VS,LDVS,
     $       ILO,IHI,RWORK(ISCALE),ABNRM,
     $       SRCONE,SRCONV,RWORK(ICONDE),RWORK(ICONDV),
     $       RWORK(IPW),LEFTRW,IWORK,LIW,TWORK,RPAR,IPAR,INFO )
      if(1.lt.1) then
      if(mdim.lt.n) then
      rrr = dabs(wr(mdim+1)/WR(MDIM))
      if(rrr.gt.0.99d0.and.rrr.lt.1.01d0) then
        write(6,*) mdim,mdimi
        write(6,*) 'srcon',srcone,srconv,WR(MDIM),wr(mdim+1),INFO
c        do 10111 i=1,n
c        write(6,*) wr(i),wi(i)
10111  continue
      endif
      endif
      endif
C***********************************************************************
C
C     Do testing
C
C***********************************************************************
C     IF(MDIMI.NE.MDIM) THEN
C     CALL DSWAP(N,VS(1,MDIM+1),1,VS(1,MDIM+2),1)
C     CALL DSWAP(N,VR(1,MDIM+1),1,VR(1,MDIM+2),1)
C     CALL DSWAP(N,VL(MDIM+1,1),LDVL,VL(MDIM+2,1),LDVL)
C     ENDIF
      IF(IPRIEV.EQ.1) THEN
C---- write eigenvalues
      WRITE(MSGFIL,*) ' Computed eigenvalues'
      WRITE(MSGFIL,85) (WR(I),WI(I),I=1,N)
      ENDIF
      IF(TEST) THEN
C***********************************************************************
C     write eigenvalues and eigenvectors
C***********************************************************************
      WRITE(MSGFIL,*) ' UGEEVS returned with INFO=',INFO
      WRITE(MSGFIL,*) ' It was called with MDIM =',MDIMI,
     1                ' and returned with MDIM =',MDIM
      WRITE(MSGFIL,*) ' Original matrix '
      DO 105 I=1,N
      WRITE(MSGFIL,88) (RWORK(ITEST-1+I+(J-1)*N),J=1,N)
 105  CONTINUE
C---- write eigenvalues
      WRITE(MSGFIL,*) ' Computed eigenvalues'
      WRITE(MSGFIL,85) (WR(I),WI(I),I=1,N)
C---- write Schur vectors
      WRITE(MSGFIL,*) ' Schur vectors '
      DO 110 I=1,N
      WRITE(MSGFIL,88) (VS(I,J),J=1,N)
 110  CONTINUE
C---- write Schur vectors
      WRITE(MSGFIL,*) ' Matrix N '
      DO 115 I=1,N
      WRITE(MSGFIL,88) (A(I,J),J=1,N)
 115  CONTINUE
C---- write right matrix
      WRITE(MSGFIL,*) ' Column matrix '
      DO 120 I=1,N
      WRITE(MSGFIL,88) (VR(I,J),J=1,N)
 120  CONTINUE
C---- write left matrix
      WRITE(MSGFIL,*) ' Row matrix '
      DO 130 I=1,N
      WRITE(MSGFIL,88) (VL(I,J),J=1,N)
 130  CONTINUE
  85  FORMAT(2X,1PE14.7,' + i * ',1PE14.7)
  88  format(5(2x,1pe14.7))
C***********************************************************************
C     calculate product of Q
C***********************************************************************
      IF(LEFTRW.LT.N*N) STOP
C---- product of Q
      CALL DGEMM('N','T',N,N,N,ONE,VS,LDVS,VS,LDVS,ZERO,RWORK(IPW),N)
      WRITE(MSGFIL,*) ' Q * Q^T  (should give identity matrix)   '
      DO 140 I=1,N
      WRITE(MSGFIL,88) (RWORK(IPW-1+I+(J-1)*N),J=1,N)
 140  CONTINUE
C---- product of VL * VR
      CALL DGEMM('N','N',N,N,N,ONE,VL,LDVL,VR,LDVR,ZERO,RWORK(IPW),N)
      WRITE(MSGFIL,*) ' VL * VR   (should give identity or nearly ',
     $                  'block diagonal matrix)  '
      DO 150 I=1,N
      WRITE(MSGFIL,88) (RWORK(IPW-1+I+(J-1)*N),J=1,N)
 150  CONTINUE
C---- product of VL * A * VR
      CALL DGEMM('N','N',N,N,N,ONE,VL,LDVL,RWORK(ITEST),N,
     $                ZERO,RWORK(IPW),N)
      CALL DGEMM('N','N',N,N,N,ONE,RWORK(IPW),N,VR,LDVR,
     $                ZERO,RWORK(ITEST),N)
      WRITE(MSGFIL,*) ' VL * A * VR  (should give diag(M11,M22), ',
     1                ' where M11 and M22 are MDIM and (N-MDIM)-dim. ',
     2                ' upper triangular submatrices'
      DO 160 I=1,N
      WRITE(MSGFIL,88) (RWORK(ITEST-1+I+(J-1)*N),J=1,N)
 160  CONTINUE
C---- product of Q  * N * Q^T
      CALL DGEMM('N','N',N,N,N,ONE,VS,LDVS,A,LDA,
     $                ZERO,RWORK(IPW),N)
      CALL DGEMM('N','T',N,N,N,ONE,RWORK(IPW),N,VS,LDVS,
     $                ZERO,RWORK(ITEST),N)
      WRITE(MSGFIL,*) ' Q * N * Q^T  (should give original matrix)   '
      DO 170 I=1,N
      WRITE(MSGFIL,88) (RWORK(ITEST-1+I+(J-1)*N),J=1,N)
 170  CONTINUE
C---- write condition numbers
C     write(6,*) 'srcone,srconv,',srcone,srconv
      ENDIF
C***********************************************************************
C
C***********************************************************************
      RETURN
C***********************************************************************
C     Error exits
C***********************************************************************
  910 WRITE(MSGFIL,*) ' HOMEVC called with illegal argument JOB '
      GOTO 999
  920 WRITE(MSGFIL,*) ' HOMEVC called with illegal arguments DIR or ZEV'
      GOTO 999
  999 CONTINUE
      STOP
      END
      SUBROUTINE UGEEVS( BALANC, JOBVL, JOBVR, JOBVS,
     $          SENSE, ISORT, XELECT, MDIM,RDIM, N, A, LDA, WR, WI,
     $          VL, LDVL, VR, LDVR, VS,LDVS,ILO, IHI, SCALE, ABNRM,
     $   SRCONE,SRCONV,RCONDE, RCONDV, WORK, LWORK, IWORK, LIWORK,
     $               LSEL,RPAR,IPAR,INFO )
C***********************************************************************
C
C     ************************************************************
C     *                                                          *
C     *  Driver routine for the calculation of Schur vectors and *
C     *  Schur vectors including sorting of the Schur vectors    *
C     *  This is a "mixed and extended" version of the LAPACK    *
C     *  routines DGEESX and DGEEVX                              *
C     *                                                          *
C     *                    Ulrich Maas 28.2.1994                 *
C     *            Last Change: May 10th 96                      *
C     *                                                          *
C     ************************************************************
C
C     ..
C
C  Purpose
C  =======
C
C  DGEEVX computes for an N-by-N real nonsymmetric matrix A, the
C  eigenvalues and, optionally, the left and/or right eigenvectors.
C
C  Optionally also, it computes a balancing transformation to improve
C  the conditioning of the eigenvalues and eigenvectors (ILO, IHI,
C  SCALE, and ABNRM), reciprocal condition numbers for the eigenvalues
C  (RCONDE), and reciprocal condition numbers for the right
C  eigenvectors (RCONDV).
C
C  The left eigenvectors of A are the same as the right eigenvectors of
C  A**T.  If u(j) and v(j) are the left and right eigenvectors,
C  respectively, corresponding to the eigenvalue lambda(j), then
C  (u(j)**T)*A = lambda(j)*(u(j)**T) and A*v(j) = lambda(j) * v(j).
C
C  The computed eigenvectors are normalized to have Euclidean norm
C  equal to 1 and largest component real.
C
C  Balancing a matrix means permuting the rows and columns to make it
C  more nearly upper triangular, and applying a diagonal similarity
C  transformation D * A * D**(-1), where D is a diagonal matrix, to
C  make its rows and columns closer in norm and the condition numbers
C  of its eigenvalues and eigenvectors smaller.  The computed
C  reciprocal condition numbers correspond to the balanced matrix.
C  Permuting rows and columns will not change the condition numbers
C  (in exact arithmetic) but diagonal scaling will.  For further
C  explanation of balancing, see section 4.10.2 of the LAPACK
C  Users' Guide.
C
C  Arguments
C  =========
C
C  BALANC  (input) CHARACTER*1
C          Indicates how the input matrix should be diagonally scaled
C          and/or permuted to improve the conditioning of its
C          eigenvalues.
C          = 'N': Do not diagonally scale or permute;
C          = 'P': Perform permutations to make the matrix more nearly
C                 upper triangular. Do not diagonally scale;
C          = 'S': Diagonally scale the matrix, i.e. replace A by
C                 D*A*D**(-1), where D is a diagonal matrix chosen
C                 to make the rows and columns of A more equal in
C                 norm. Do not permute;
C          = 'B': Both diagonally scale and permute A.
C
C          Computed reciprocal condition numbers will be for the matrix
C          after balancing and/or permuting. Permuting does not change
C          condition numbers (in exact arithmetic), but balancing does.
C
C  JOBVL   (input) CHARACTER*1
C          = 'N': left eigenvectors of A are not computed;
C          = 'V': left eigenvectors of A are computed.
C          If SENSE = 'E' or 'B', JOBVL must = 'V'.
C
C  JOBVR   (input) CHARACTER*1
C          = 'N': right eigenvectors of A are not computed;
C          = 'V': right eigenvectors of A are computed.
C          If SENSE = 'E' or 'B', JOBVR must = 'V'.
C
C  JOBVS    (input) CHARACTER*1
C          = 'N': Schur vectors of A are not computed;
C          = 'V': Schur vectors of A are computed.
C          = 'I': Invariant subspaces are computed
C
C  SENSE   (input) CHARACTER*1
C          Determines which reciprocal condition numbers are computed.
C          = 'N': None are computed;
C          = 'E': Computed for eigenvalues only;
C          = 'V': Computed for right eigenvectors only;
C          = 'B': Computed for eigenvalues and right eigenvectors.
C
C          If SENSE = 'E' or 'B', both left and right eigenvectors
C          must also be computed (JOBVL = 'V' and JOBVR = 'V').
C
C  ISORT   (input) INTEGER
C          Info flag for subroutine xelect
C
C  XELECT  (input) EXTERNAL
C          Name of the subroutine which gives the info flags for
C          sorting the Schur vectors.
C          If sorting of the Schur vectors is desired, the user has
C          to supply a subroutine
C
C     SUBROUTINE XELECT(NOS,ISORT,MDIM,RDIM,WR,WI,N,Q,
C                                   LDVS,IWORK,RPAR,IPAR)
C                  RPAR(*) double precision array for user
C                          communication with XELECT
C                  IPAR(*) integer array for user
C                          communication with XELECT
C                       RPAR and IPAR are not touched
C                  Q(LDVS,N) matrix of Schur vectors. This is
C                          included for use in the subroutine.
C                          However, Q must NOT be changed by XELECT
C           On output IWORK (integer of dimension N) has to specify
C           the desired order of the eigenvalues in the Schur
C           decomposition.
C           IWORK(I) = K means: eigenvalue I has to be at position K.
C           IWORK(I) = 0 means: eigenvalue shall be in the lower
C                        right of the matrix.
C            IWORK = 3, 0, 2, 1 e.g., means that the eigenvalues
C            should be ordered such the first one has the final
C            position 3, the second is swapped to the end,
C            the third one is swapped to the second position
C            and the 4th one is swapped to the second position.
C            Note that 3, 4, 2, 1 would produce the same result
C            in this case
C          XELECT is called successively, ordering one eigenvalue after
C          the other
C
C  MDIM    (input/output)
C          on input MDIM gives the desired dimension of the subspace 1
C          on output it gives the final dimension, which is given
C          by MDIM - 1 if the separating eigenvalue is complex
C          and by MDIM otherwise
C
C  N       (input) INTEGER
C          The order of the matrix A. N >= 0.
C
C  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
C          On entry, the N-by-N matrix A.
C          On exit, A has been overwritten.
C          If JOBVL = 'V' or JOBVR = 'V' or JOBVS = 'V', A contains
C          the matrix N of the Schur decomposition of the A.
C          If JOBVS = 'I' or JOBVR = 'V' or JOBVS = 'V', A contains
C          the matrix N_1 N_2  of the invariant subspace dec. of A.
C
C  LDA     (input) INTEGER
C          The leading dimension of the array A.  LDA >= max(1,N).
C
C  WR      (output) DOUBLE PRECISION array, dimension (N)
C  WI      (output) DOUBLE PRECISION array, dimension (N)
C          WR and WI contain the real and imaginary parts,
C          respectively, of the computed eigenvalues.  Complex
C          conjugate pairs of eigenvalues will appear consecutively
C          with the eigenvalue having the positive imaginary part
C          first.
C
C  VR      (output) DOUBLE PRECISION array, dimension (LDVR,N)
C
C          If JOBVR = 'V', the right eigenvectors v(j) are stored one
C            after another in the columns of VR, in the same order
C            as their eigenvalues.
C            If the j-th eigenvalue is real, then v(j) = VR(:,j),
C            the j-th column of VR.
C            If the j-th and (j+1)-st eigenvalues form a complex
C            conjugate pair, then v(j) = VR(:,j) + i*VR(:,j+1) and
C            v(j+1) = VR(:,j) = i*VR(:,j+1).
C          IF JOBVS = 'I',
C            VR contains in its first 1:MDIM columns a set of vectors
C            spanning the eigenspace associated with the eigenvalues
C            1:MDIM, and in its MDIM+1:N columns a set of vectors
C            spanning the eigenspace associated with the eigenvalues
C            MDIM+1,N, where MDIM is the corrected dimension of
C            the subspace 1 (see above)
C          If JOBVR = 'N' and JOBVS .NE. 'I'  VR is not referenced.
C
C  LDVR    (input) INTEGER
C          The leading dimension of the array VR.  LDVR >= 1, and if
C          JOBVR = 'V' or JOBVS = 'I',  LDVR >= N.
C
C  VL      (output) DOUBLE PRECISION array, dimension (LDVL,N)
C          If JOBVL = 'V', the left eigenvectors u(j) are stored one
C            after another in the rows of VL, in the same order
C            as their eigenvalues.
C            If the j-th eigenvalue is real, then u(j) = VL(j,:),
C            the j-th row of VL.
C            If the j-th and (j+1)-st eigenvalues form a complex
C            conjugate pair, then u(j) = VL(j,:) + i*VL(j+1,:) and
C            u(j+1) = VL(j,:) - i*VL(j+1,:).
C          If JOBVS = 'I', VL contains the inverse of VR
C          If JOBVL = 'N', VL is not referenced.
C
C  LDVL    (input) INTEGER
C          The leading dimension of the array VL.  LDVL >= 1; if
C          JOBVL = 'V' or JOBVS = 'I',  LDVL >= N.
C
C  VS      (output) DOUBLE PRECISION array, dimension (LDVS,N)
C          If JOBVS = 'V', the Schur vectors v(j) are stored one
C          after another in the columns of VS, in the same order
C          as their eigenvalues.
C          If JOBVS = 'N', VS is not referenced.
C
C  LDVS    (input) INTEGER
C          The leading dimension of the array VS.  LDVS >= 1, and if
C          JOBVS = 'V', LDVS >= N.
C
C  ILO,IHI (output) INTEGER
C          ILO and IHI are integer values determined when A was
C          balanced.  The balanced A(i,j) = 0 if I > J and
C          J = 1,...,ILO-1 or I = IHI+1,...,N.
C
C  SCALE   (output) DOUBLE PRECISION array, dimension (N)
C          Details of the permutations and scaling factors applied
C          when balancing A.  If P(j) is the index of the row and column
C          interchanged with row and column j, and D(j) is the scaling
C          factor applied to row and column j, then
C          SCALE(J) = P(J),    for J = 1,...,ILO-1
C                   = D(J),    for J = ILO,...,IHI
C                   = P(J)     for J = IHI+1,...,N.
C          The order in which the interchanges are made is N to IHI+1,
C          then 1 to ILO-1.
C
C  ABNRM   (output) DOUBLE PRECISION
C          The one-norm of the balanced matrix (the maximum
C          of the sum of absolute values of entries of any column).
C
C  RCONDE  (output) DOUBLE PRECISION array, dimension (N)
C          RCONDE(j) is the reciprocal condition number of the j-th
C          eigenvalue.
C
C  RCONDV  (output) DOUBLE PRECISION array, dimension (N)
C          RCONDV(j) is the reciprocal condition number of the j-th
C          right eigenvector.
C
C  WORK    (workspace/output) DOUBLE PRECISION array, dimension (LWORK)
C          On exit, if INFO = 0, WORK(1) returns the optimal LWORK.
C
C  LWORK   (input) INTEGER
C          The dimension of the array WORK.   If SENSE = 'N' or 'E',
C          LWORK >= max(1,2*N), and if JOBVL = 'V' or JOBVR = 'V',
C          LWORK >= 3*N.  If SENSE = 'V' or 'B', LWORK >= N*(N+6).
C          For good performance, LWORK must generally be larger.
C
C  IWORK   (workspace) INTEGER array, dimension (2*N-2)
C
C
C  INFO    (output) INTEGER
C          = 0:  successful exit
C          < 0:  if INFO = -i, the i-th argument had an illegal value.
C          > 0:  if INFO = i, the QR algorithm failed to compute all the
C                eigenvalues, and no eigenvectors or condition numbers
C                have been computed; elements 1:ILO-1 and i+1:N of WR
C                and WI contain eigenvalues which have converged.
C          = N+1: the eigenvalues could not be reordered because some
C                   eigenvalues were too close to separate (the problem
C                   is very ill-conditioned);
C not yet  = N+2: after reordering, roundoff changed values of some
C                 complex eigenvalues so that leading eigenvalues in
C                 the Schur form no longer satisfy XELECT=.TRUE. This
C                 could also be caused by underflow due to scaling.
C
C=======================================================================
C=======================================================================
C
C     This subroutine may not be used commerically
C     or forwarded to third persons
C     without the permission of the author
C
C=======================================================================
C=======================================================================
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C***********************************************************************
C     Externals
C***********************************************************************
      EXTERNAL           DGEBAK, DGEBAL, DGEHRD, DHSEQR, DLABAD, DLACPY,
     $                   DLARTG, DLASCL, DORGHR, DROT, DSCAL, DTREVC,
     $                   DTRSNA, XERBLA
      EXTERNAL XELECT
      EXTERNAL           LSAME, IDAMAX, ILAENV, DLAMCH, DLANGE, DLAPY2,
     $                   DNRM2
      INTRINSIC          MAX, MIN, SQRT
C***********************************************************************
C     Types
C***********************************************************************
      CHARACTER          BALANC, JOBVL, JOBVR, JOBVS, SENSE, JOB, SIDE
      LOGICAL            SCALEA, WANTVL, WANTVR, WNTSNB, WNTSNE, WNTSNN,
     $                   WNTSNV,  WANTVS, WANTST,LSAME,EVONLY,WANTIS,
     $                   LSEL
C***********************************************************************
C     Arrays
C***********************************************************************
      DIMENSION IWORK( LIWORK ), IPAR(*),LSEL(*)
      DIMENSION A(LDA,*),RCONDE(*),RCONDV(*),
     $          SCALE(*),VL(LDVL,*),VR(LDVR,*),VS(LDVS,*),
     $          WI(*),WORK(*),WR(*),RPAR(*)
      DIMENSION DUM( 1 )
C***********************************************************************
C     Parameters
C***********************************************************************
      PARAMETER          ( ZERO = 0.0D0, ONE = 1.0D0 )
C***********************************************************************
C
C     Test the input arguments
C
C***********************************************************************
      INFO = 0
      WANTVL = LSAME( JOBVL, 'V' )
      WANTVR = LSAME( JOBVR, 'V' )
      WNTSNN = LSAME( SENSE, 'N' )
      WNTSNE = LSAME( SENSE, 'E' )
      WNTSNV = LSAME( SENSE, 'V' )
      WNTSNB = LSAME( SENSE, 'B' )
      WANTVS = LSAME( JOBVS, 'V' ) .OR. LSAME(JOBVS,'I')
      WANTIS = LSAME( JOBVS, 'I' )
      WANTST = (ISORT.NE.0)
      IF( .NOT.( LSAME( BALANC, 'N' ) .OR. LSAME( BALANC,
     $    'S' ) .OR. LSAME( BALANC, 'P' ) .OR. LSAME( BALANC, 'B' ) ) )
     $     THEN
         INFO = -1
      ELSE IF(WANTVS.AND.(LSAME(BALANC,'S').OR.LSAME(BALANC,'B'))) THEN
         INFO = -1
      ELSE IF( ( .NOT.WANTVL ) .AND. ( .NOT.LSAME( JOBVL, 'N' ) ) ) THEN
         INFO = -2
      ELSE IF( ( .NOT.WANTVR ) .AND. ( .NOT.LSAME( JOBVR, 'N' ) ) ) THEN
         INFO = -3
C     ELSE IF( .NOT.( WNTSNN .OR. WNTSNE .OR. WNTSNB .OR. WNTSNV ) .OR.
C    $         ( ( WNTSNE .OR. WNTSNB ) .AND. .NOT.( WANTVL .AND.
C    $         WANTVR ) ) ) THEN
C        INFO = -5
      ELSE IF( ( .NOT.WANTVS ) .AND.
     $          (.NOT.WANTIS).AND.( .NOT.LSAME( JOBVS, 'N' ) ) ) THEN
         INFO = -4
      ELSE IF( (.NOT.WANTST).AND.WANTIS) THEN
         INFO = -4
      ELSE IF( (WANTVL .OR. WANTVL).AND.WANTIS) THEN
         INFO = -4
      ELSE IF( N.LT.0 ) THEN
         INFO = -10
      ELSE IF( LDA.LT.MAX( 1, N ) ) THEN
         INFO = -11
      ELSE IF( LDVL.LT.1 .OR. ( WANTVL .AND. LDVL.LT.N ) ) THEN
         INFO = -15
      ELSE IF( LDVR.LT.1 .OR. ( WANTVR .AND. LDVR.LT.N ) ) THEN
         INFO = -17
      END IF
      IF(WANTST.AND.(.NOT.WANTVS)) INFO = -30
      EVONLY = .NOT.(WANTVS.OR.WANTVR.OR.WANTVL)
C***********************************************************************
C
C     Test the input arguments
C
C***********************************************************************
C
C     Compute workspace
C      (Note: Comments in the code beginning "Workspace:" describe the
C       minimal amount of workspace needed at that point in the code,
C       as well as the preferred amount for good performance.
C       NB refers to the optimal block size for the immediately
C       following subroutine, as returned by ILAENV.
C       LWRKH refers to the workspace preferred by DHSEQR, as
C       calculated below. LWRKH is computed assuming ILO=1 and IHI=N,
C       the worst case.)
C
      MINWRK = 1
      IF( INFO.EQ.0 .AND. LWORK.GE.1 ) THEN
         MAXWRK = N + N*ILAENV( 1, 'DGEHRD', ' ', N, 1, N, 0 )
         IF( ( .NOT.WANTVL ) .AND. ( .NOT.WANTVR ) ) THEN
            MINWRK = MAX( 1, 2*N )
            IF( .NOT.WNTSNN )
     $         MINWRK = MAX( MINWRK, N*N+6*N )
            MAXB = MAX( ILAENV( 8, 'DHSEQR', 'SN', N, 1, N, -1 ), 2 )
            IF( WNTSNN ) THEN
               K = MIN( MAXB, N, MAX( 2, ILAENV( 4, 'DHSEQR', 'EN', N,
     $             1, N, -1 ) ) )
            ELSE
               K = MIN( MAXB, N, MAX( 2, ILAENV( 4, 'DHSEQR', 'SN', N,
     $             1, N, -1 ) ) )
            END IF
            LWRKH = MAX( K*( K+2 ), 2*N )
            MAXWRK = MAX( MAXWRK, 1, LWRKH )
            IF( .NOT.WNTSNN )
     $         MAXWRK = MAX( MAXWRK, N*N+6*N )
         ELSE
            MINWRK = MAX( 1, 3*N )
            IF( ( .NOT.WNTSNN ) .AND. ( .NOT.WNTSNE ) )
     $         MINWRK = MAX( MINWRK, N*N+6*N )
            MAXB = MAX( ILAENV( 8, 'DHSEQR', 'SN', N, 1, N, -1 ), 2 )
            K = MIN( MAXB, N, MAX( 2, ILAENV( 4, 'DHSEQR', 'EN', N, 1,
     $          N, -1 ) ) )
            LWRKH = MAX( K*( K+2 ), 2*N )
            MAXWRK = MAX( MAXWRK, 1, LWRKH )
            MAXWRK = MAX( MAXWRK, N+( N-1 )*
     $               ILAENV( 1, 'DORGHR', ' ', N, 1, N, -1 ) )
            IF( ( .NOT.WNTSNN ) .AND. ( .NOT.WNTSNE ) )
     $         MAXWRK = MAX( MAXWRK, N*N+6*N )
            MAXWRK = MAX( MAXWRK, 3*N, 1 )
         END IF
         WORK( 1 ) = MAXWRK
      END IF
      IF( LWORK.LT.MINWRK ) THEN
         write(*,*) LWORK,MINWRK,'LWORK,MINWRK'
         INFO = -29
      END IF
      IF( LIWORK.LT.N ) THEN
         INFO = -31
      END IF
      IF( INFO.NE.0 ) THEN
         CALL XERBLA( 'DGEEVX', -INFO )
         RETURN
      END IF
C***********************************************************************
C     Quick return if possible
C***********************************************************************
      IF( N.EQ.0 ) THEN
        MDIM = 0
        RETURN
      ENDIF
C***********************************************************************
C     Get machine constants
C***********************************************************************
      EPS = DLAMCH( 'P' )
      SMLNUM = DLAMCH( 'S' )
      BIGNUM = ONE / SMLNUM
      CALL DLABAD( SMLNUM, BIGNUM )
      SMLNUM = SQRT( SMLNUM ) / EPS
      BIGNUM = ONE / SMLNUM
C***********************************************************************
C
C     Scale A if max entry outside range [SMLNUM,BIGNUM]
C
C***********************************************************************
      ICOND = 0
      ANRM = DLANGE( 'M', N, N, A, LDA, DUM )
      SCALEA = .FALSE.
      IF( ANRM.GT.ZERO .AND. ANRM.LT.SMLNUM ) THEN
         SCALEA = .TRUE.
         CSCALE = SMLNUM
      ELSE IF( ANRM.GT.BIGNUM ) THEN
         SCALEA = .TRUE.
         CSCALE = BIGNUM
      END IF
      IF( SCALEA )
     $   CALL DLASCL( 'G', 0, 0, ANRM, CSCALE, N, N, A, LDA, IERR )
C***********************************************************************
C
C     Balance the matrix and compute ABNRM
C
C***********************************************************************
      CALL DGEBAL( BALANC, N, A, LDA, ILO, IHI, SCALE, IERR )
      ABNRM = DLANGE( '1', N, N, A, LDA, DUM )
      IF( SCALEA ) THEN
         DUM( 1 ) = ABNRM
         CALL DLASCL( 'G', 0, 0, CSCALE, ANRM, 1, 1, DUM, 1, IERR )
         ABNRM = DUM( 1 )
      END IF
C***********************************************************************
C
C     Reduce to upper Hessenb. form (Workspace: need 2*N, prefer N+N*NB)
C
C***********************************************************************
      ITAU = 1
      IWRK = ITAU + N
      CALL DGEHRD( N, ILO, IHI, A, LDA, WORK( ITAU ), WORK( IWRK ),
     $             LWORK-IWRK+1, IERR )
C***********************************************************************
C
C     Calculation of the Schur vectors
C
C***********************************************************************
      IF( WANTVL ) THEN
C----    Want left eigenvectors: Copy Householder vectors to VL
         SIDE = 'L'
         CALL DLACPY( 'L', N, N, A, LDA, VL, LDVL )
C
C        Generate orthogonal matrix in VL
C        (Workspace: need 2*N-1, prefer N+(N-1)*NB)
C
         CALL DORGHR( N, ILO, IHI, VL, LDVL, WORK( ITAU ), WORK( IWRK ),
     $                LWORK-IWRK+1, IERR )
C
C        Perform QR iteration, accumulating Schur vectors in VL
C        (Workspace: need 1, prefer LWRKH (see comments) )
C
         IWRK = ITAU
         CALL DHSEQR( 'S', 'V', N, ILO, IHI, A, LDA, WR, WI, VL, LDVL,
     $                WORK( IWRK ), LWORK-IWRK+1, INFO )
C---- Want left and right eigenvectors: Copy Schur vectors to VR
         IF( WANTVR ) THEN
            SIDE = 'B'
            CALL DLACPY( 'F', N, N, VL, LDVL, VR, LDVR )
         END IF
C---- Want Schur vectors: Copy Schur vectors to VS
         IF( WANTVS ) THEN
            CALL DLACPY( 'F', N, N, VL, LDVL, VS, LDVS )
         END IF
C
      ELSE IF( WANTVR ) THEN
C
C        Want right eigenvectors
C        Copy Householder vectors to VR
C
         SIDE = 'R'
         CALL DLACPY( 'L', N, N, A, LDA, VR, LDVR )
C
C        Generate orthogonal matrix in VR
C        (Workspace: need 2*N-1, prefer N+(N-1)*NB)
C
         CALL DORGHR( N, ILO, IHI, VR, LDVR, WORK( ITAU ), WORK( IWRK ),
     $                LWORK-IWRK+1, IERR )
C
C        Perform QR iteration, accumulating Schur vectors in VR
C        (Workspace: need 1, prefer LWRKH (see comments) )
C
         IWRK = ITAU
         CALL DHSEQR( 'S', 'V', N, ILO, IHI, A, LDA, WR, WI, VR, LDVR,
     $                WORK( IWRK ), LWORK-IWRK+1, INFO )
C
C---- Want Schur vectors: Copy Schur vectors to VS
         IF( WANTVS ) THEN
            CALL DLACPY( 'F', N, N, VR, LDVR, VS, LDVS )
         END IF
      ELSE IF( WANTVS ) THEN
C
C        Want Schur vectors
C        Copy Householder vectors to VS
C
         CALL DLACPY( 'L', N, N, A, LDA, VS, LDVS )
C
C        Generate orthogonal matrix in VS
C        (Workspace: need 2*N-1, prefer N+(N-1)*NB)
C
         CALL DORGHR( N, ILO, IHI, VS, LDVS, WORK( ITAU ), WORK( IWRK ),
     $                LWORK-IWRK+1, IERR )
C
C        Perform QR iteration, accumulating Schur vectors in VS
C        (Workspace: need 1, prefer LWRKH (see comments) )
C
         IWRK = ITAU
         CALL DHSEQR( 'S', 'V', N, ILO, IHI, A, LDA, WR, WI, VS, LDVS,
     $                WORK( IWRK ), LWORK-IWRK+1, IEVAL )
         IF( IEVAL.GT.0 ) INFO = IEVAL
C
      ELSE
C
C        Compute eigenvalues only
C        If condition numbers desired, compute Schur form
C
         IF( WNTSNN ) THEN
            JOB = 'E'
         ELSE
            JOB = 'S'
         END IF
C
C        (Workspace: need 1, prefer LWRKH (see comments) )
C
         IWRK = ITAU
         CALL DHSEQR( JOB, 'N', N, ILO, IHI, A, LDA, WR, WI, VS, LDVS,
     $                WORK( IWRK ), LWORK-IWRK+1, INFO )
      END IF
C***********************************************************************
C     Descale the eigenvalues
C***********************************************************************
      IF( SCALEA ) THEN
         CALL DLASCL( 'G', 0, 0, CSCALE, ANRM, N-INFO, 1, WR( INFO+1 ),
     $                MAX( N-INFO, 1 ), IERR )
         CALL DLASCL( 'G', 0, 0, CSCALE, ANRM, N-INFO, 1, WI( INFO+1 ),
     $                MAX( N-INFO, 1 ), IERR )
         IF( INFO.NE.0 ) THEN
            CALL DLASCL( 'G', 0, 0, CSCALE, ANRM, ILO-1, 1, WR, N,
     $                   IERR )
            CALL DLASCL( 'G', 0, 0, CSCALE, ANRM, ILO-1, 1, WI, N,
     $                   IERR )
         END IF
      END IF
C***********************************************************************
C
C   Sort the Schur vectors
C
C***********************************************************************
      IF(WANTST.AND.WANTVS.AND.INFO.EQ.0) THEN
C***********************************************************************
C
C***********************************************************************
      LAUF = 0
  300 CONTINUE
      NOS = N
      CALL XELECT(NOS,ISORT,MDIM,RDIM,WR,WI,N,VS,LDVS,IWORK,RPAR,IPAR)
      DO 310 I = 1,N
        LSEL(I) = .FALSE.
  310 CONTINUE
C---- Look for the eigenvalues which are already sorted
      ILS = 0
      DO 320 I = 1,N
C       IF(IWORK(I).EQ.I) THEN
        IF(IWORK(I).EQ.I.OR.I.LE.LAUF) THEN
                          LSEL(I) = .TRUE.
                          ILS     = I
        ELSE
                          LSEL(I) = .FALSE.
                          GOTO 330
        ENDIF
  320 CONTINUE
  330 CONTINUE
      IF(ILS.LT.LAUF) GOTO 9510
      LAUF = LAUF+1
C---- Now check the next eigenvalue to be selected
      IFOUND = 0
      DO 340 I = ILS+1,N
C     IF(IWORK(I).EQ.ILS+1) THEN
C---- this caused problems if eigenvalues are almost the same
      IF(IWORK(I).LE.ILS+1) THEN
        LSEL(I) = .TRUE.
        IFOUND  = 1
        GOTO 350
      ENDIF
  340 CONTINUE
  350 CONTINUE
C     write(6,8881) 'iwork'
C8881 format(a5,20I3)
C     write(*,8833) (wr(i),iwork(i),lsel(i),i=1,nos)
C8833 FORMAT(' ','llllll',2x,1pe9.2,1x,i2,L)
C---- Now check for continuation
C     IF(IFOUND.EQ.0) THEN
C        WRITE(6,*) ' NO MORE EIGENVALUES TO SORT '
C        GOTO  390
C     ENDIF
      IF(ILS.EQ.N) THEN
C        WRITE(6,*) ' ALL EIGENVALUES SORTED '
         GOTO  390
      ENDIF
C---- calculate condition number for ils + 1 = mdim
C     write(6,*) ILS
C     write(6,*) (lsel(i),i=1,n)
      IF(ILS+1.NE.MDIM) THEN
        JOB = 'N'
      ELSE
        JOB = SENSE
      ENDIF
      CALL DTRSEN(JOB,'V',LSEL,N,A,LDA,VS,LDVS,WR,WI,
     $   MDIMT,SRCONE,SRCONV,WORK(IWRK),LWORK-IWRK+1,IWORK,LIWORK,ICOND)
         IF( ICOND.EQ.-15 ) THEN
C---- Not enough real workspace
            INFO = -29
         ELSE IF( ICOND.EQ.-17 ) THEN
C---- Not enough integer workspace
            INFO = -31
         ELSE IF( ICOND.GT.0 ) THEN
          write(6,*) 'infoinfoinfo=',INFO
C----  DTRSEN failed to reorder or to restore standard Schur form
            INFO = ICOND + N
         END IF
C***********************************************************************
C     Descale the eigenvalues
C***********************************************************************
      IF( SCALEA ) THEN
         II = MAX(N-INFO,1)
         CALL DLASCL('G',0,0,CSCALE,ANRM,N-INFO,1,WR(INFO+1),II,IERR)
         CALL DLASCL('G',0,0,CSCALE,ANRM,N-INFO,1,WI(INFO+1),II,IERR)
         IF( INFO.NE.0 ) THEN
            CALL DLASCL('G',0,0,CSCALE,ANRM,ILO-1,1,WR,N,IERR)
            CALL DLASCL('G',0,0,CSCALE,ANRM,ILO-1,1,WI,N,IERR)
         END IF
      END IF
      IFI = IFI + 1
      GOTO 300
C***********************************************************************
C     This block evaluates the invariant subspace of the lower block   *
C***********************************************************************
  390 continue
      IF(WANTIS) THEN
      IF(MDIM.EQ.0.OR.MDIM.EQ.N) THEN
      CALL DLACPY('F',N,N,VS,LDVS,VR,LDVR)
      ELSE
C---- check whether splitting is not at a complex pair
      IF(A(MDIM+1,MDIM).NE.ZERO) THEN
        MDIM = MDIM - 1
      ENDIF
      IF(MDIM.EQ.0) GOTO 390
      N1 = MDIM
      N2 = N - MDIM
C
C        Solve Sylvester equation for R:
C
C           T11*R - R*T22 = scale*T12
C     use VL as work array
C
      I2 = N1 + 1
      CALL DLACPY('F',N,N,VS,LDVS,VR,LDVR)
      CALL DLACPY('F',N1,N2,A(1,I2),LDA,VL,N1)
      CALL DTRSYL('N','N',-1,N1,N2,A,LDA,A(I2,I2),
     $                       LDA,VL, N1,XSCALE, IERR )
      PFAC = - ONE /XSCALE
      CALL DGEMM ('N','N',N1,N2,N1,PFAC,VR,LDVR,VL,N1,ONE,VR(1,I2),LDVR)
      CALL DGEMM ('N','N',N2,N2,N1,PFAC,VR(I2,1),LDVR,
     $                VL,N1,ONE,VR(I2,I2),LDVR)
      ENDIF
      ENDIF
C***********************************************************************
C     copy schur vectors back into vl,vr
C***********************************************************************
      IF(WANTVL) CALL DLACPY('F',N,N,VS,LDVS,VL,LDVL)
      IF(WANTVR) CALL DLACPY('F',N,N,VS,LDVS,VR,LDVR)
      ENDIF
C***********************************************************************
C     Calculate the eigenvectors
C***********************************************************************
      IF(INFO.EQ.0) THEN
        IF( WANTVL .OR. WANTVR) THEN
C---- Compute left and/or right eigenvectors (Workspace: need 3*N)
         CALL DTREVC( SIDE, 'O', LSEL, N, A, LDA, VL, LDVL, VR, LDVR,
     $                N, NOUT, WORK( IWRK ), IERR )
        ENDIF
C---- Compute condition numbers if desired
C     (Workspace: need N*N+6*N unless SENSE = 'E')
        IF( .NOT.WNTSNN .AND. (.NOT.WANTIS)) THEN
         CALL DTRSNA( SENSE, 'A', LSEL, N, A, LDA, VL, LDVL, VR, LDVR,
     $                RCONDE, RCONDV, N, NOUT, WORK( IWRK ), N, IWORK,
     $                ICOND )
        END IF
      ENDIF
C***********************************************************************
C
C      Unbalancing
C
C***********************************************************************
      IF(WANTVS)
     1   CALL DGEBAK(BALANC,'R',N,ILO,IHI,SCALE,N,VS,LDVS,IERR)
      IF(WANTVL) CALL DGEBAK(BALANC,'L',N,ILO,IHI,SCALE,N,VL,LDVL,IERR)
      IF(WANTVR.OR.WANTIS)
     1     CALL DGEBAK(BALANC,'R',N,ILO,IHI,SCALE,N,VR,LDVR,IERR)
C***********************************************************************
C
C      Descaling
C
C***********************************************************************
C***********************************************************************
C     Descaling of schur system
C***********************************************************************
      IF( SCALEA ) THEN
C---- unscale matrix A
      IF(.NOT.EVONLY) CALL DLASCL('H',0,0,CSCALE,ANRM,N,N,A,LDA,IERR)
C---- unscale condition number of the invariant subspace
       IF((WNTSNV.OR.WNTSNB).AND.INFO.EQ.0) THEN
          DUM(1) = SRCONV
          CALL DLASCL('G',0,0,CSCALE,ANRM,1,1,DUM,1,IERR)
          SRCONV = DUM(1)
       END IF
C---- store diagonal elements in WR
       CALL DCOPY(N,A,LDA+1,WR,1)
      IF( CSCALE.EQ.SMLNUM ) THEN
C----
C           If scaling back towards underflow, adjust WI if an
C           offdiagonal element of a 2-by-2 block in the Schur form
C           underflows.
C
        IF( IEVAL.GT.0 ) THEN
             I1 = IEVAL + 1
             I2 = IHI - 1
             CALL DLASCL('G',0,0,CSCALE,ANRM,ILO-1,1,WI,N,IERR)
        ELSE IF(WANTST) THEN
               I1 = 1
               I2 = N - 1
        ELSE
               I1 = ILO
               I2 = IHI - 1
        END IF
        INXT = I1 - 1
        DO 2120 I = I1, I2
        IF(I.LT.INXT) GOTO 2120
        IF(WI(I).EQ.ZERO) THEN
            INXT = I + 1
        ELSE
          IF(A(I+1,I).EQ.ZERO) THEN
            WI(I) = ZERO
            WI(I+1) = ZERO
          ELSE IF(A(I+1,I).NE.ZERO.AND.A(I,I+1).EQ.ZERO) THEN
            WI(I) = ZERO
            WI(I+1) = ZERO
            IF(I.GT.1)   CALL DSWAP(I-1,A(1,I),1,A(1,I+1),1)
            IF(N.GT.I+1) CALL DSWAP(N-I-1,A(I,I+2),LDA,A(I+1,I+2),LDA)
                         CALL DSWAP(N,VS(1,I),1,VS(1,I+1),1)
                     A( I, I+1 ) = A( I+1, I )
                     A( I+1, I ) = ZERO
          END IF
          INXT = I + 2
        END IF
 2120   CONTINUE
        END IF
         CALL DLASCL('G',0,0,CSCALE,ANRM,
     1              N-IEVAL,1,WI(IEVAL+1),MAX(N-IEVAL,1),IERR)
      ENDIF
C***********************************************************************
C      Scale condition numbers
C***********************************************************************
      IF( SCALEA.AND.INFO.EQ.0 ) THEN
            IF( ( WNTSNV .OR. WNTSNB ) .AND. ICOND.EQ.0 )
     $         CALL DLASCL( 'G', 0, 0, CSCALE, ANRM, N, 1, RCONDV, N,
     $                      IERR )
      END IF
C***********************************************************************
C
C      Normalize eigenvectors and make largest component real
C
C***********************************************************************
C***********************************************************************
C      Left eigenvectors
C***********************************************************************
      IF( WANTVL .AND.INFO.EQ.0) THEN
         DO 20 I = 1, N
            IF( WI( I ).EQ.ZERO ) THEN
               SCL = ONE / DNRM2( N, VL( 1, I ), 1 )
               CALL DSCAL( N, SCL, VL( 1, I ), 1 )
            ELSE IF( WI( I ).GT.ZERO ) THEN
               SCL = ONE / DLAPY2( DNRM2( N, VL( 1, I ), 1 ),
     $               DNRM2( N, VL( 1, I+1 ), 1 ) )
               CALL DSCAL( N, SCL, VL( 1, I ), 1 )
               CALL DSCAL( N, SCL, VL( 1, I+1 ), 1 )
               DO 10 K = 1, N
                  WORK( K ) = VL( K, I )**2 + VL( K, I+1 )**2
   10          CONTINUE
               K = IDAMAX( N, WORK, 1 )
               CALL DLARTG( VL( K, I ), VL( K, I+1 ), CS, SN, R )
               CALL DROT( N, VL( 1, I ), 1, VL( 1, I+1 ), 1, CS, SN )
               VL( K, I+1 ) = ZERO
            END IF
   20    CONTINUE
C---- form transposed
      DO 610 I = 1,N
      DO 610 J = 1,I
      VLIJ = VL(I,J)
      VLJI = VL(J,I)
      VL(I,J) = VLJI
      VL(J,I) = VLIJ
  610 CONTINUE
      END IF
C***********************************************************************
C      Right eigenvectors
C***********************************************************************
      IF( WANTVR.AND.INFO.EQ.0) THEN
         DO 40 I = 1, N
            IF( WI( I ).EQ.ZERO ) THEN
               SCL = ONE / DNRM2( N, VR( 1, I ), 1 )
               CALL DSCAL( N, SCL, VR( 1, I ), 1 )
            ELSE IF( WI( I ).GT.ZERO ) THEN
               SCL = ONE / DLAPY2( DNRM2( N, VR( 1, I ), 1 ),
     $               DNRM2( N, VR( 1, I+1 ), 1 ) )
               CALL DSCAL( N, SCL, VR( 1, I ), 1 )
               CALL DSCAL( N, SCL, VR( 1, I+1 ), 1 )
               DO 30 K = 1, N
                  WORK( K ) = VR( K, I )**2 + VR( K, I+1 )**2
   30          CONTINUE
               K = IDAMAX( N, WORK, 1 )
               CALL DLARTG( VR( K, I ), VR( K, I+1 ), CS, SN, R )
               CALL DROT( N, VR( 1, I ), 1, VR( 1, I+1 ), 1, CS, SN )
               VR( K, I+1 ) = ZERO
            END IF
   40    CONTINUE
      END IF
C***********************************************************************
C     Calculate inverse for invariant subspace
C***********************************************************************
      IF(WANTIS) THEN
      CALL DLACPY('F',N,N,VR,LDVR,VL,LDVL)
      CALL DGETRF(N,N,VL,LDVL,IWORK,IERR)
      IF(IERR.NE.0) THEN
        WRITE(6,*) ' SINGULAR MATRIX'
        STOP
      ENDIF
      CALL DGETRI(N,VL,LDVL,IWORK,WORK(IWRK),LWORK-IWRK+1,IERR)
      IF(IERR.NE.0) THEN
        WRITE(6,*) ' XXXXXXXR MATRIX'
        STOP
      ENDIF
      ENDIF
C
      WORK( 1 ) = MAXWRK
      RETURN
 9510 CONTINUE
      INFO = N + 2
      WRITE(*,*) ' This error in UGEESV should not happen.'
      WRITE(*,*) ' Possible reasons: Error in XELECT.'
      GOTO 9999
 9999 CONTINUE
      RETURN
C
C     End of DGEEVX
C
      END
      SUBROUTINE XELECT(NOS,ISORT,MDIM,RDIM,WR,WI,N,Q,
     1      LDVS,IWORK,RPAR,IPAR)
C***********************************************************************
C
C     ************************************************************
C     *                                                          *
C     *     Calculation of the sorting flags                     *
C     *                                                          *
C     *    see subroutine HOMEVS for details                     *
C     *                                                          *
C     *                                                          *
C     ************************************************************
C
C***********************************************************************
C***********************************************************************
C     Input:
C         N         dimension of the system
C         NOS       number of eigenvalues. This variable is set
C                   by the calling routine to the
C                   number of eigenvalues which have to be sorted
C                   It is only included for compatibility with older
C                   eigenvector routines
C         ISORT     task for sorting, see comments below
C         MDIM      dimension of the first invariant subspace
C                   if MDIM > 0 nothing is done
C                   if MDIM = 0 on entry, MDIM is calculated according
C                   to RDIM (see below)
C
C         WR(N)     real parts of the eigenvalues
C         WI(N)     imaginary parts of the eigenvalues
C         Q(LDVS,N) Schur vectors
C
C     Output:
C         IWORK     vector for sorting the eigenvalues
C
C     RPAR(*),IPAR(*): arrays for user communication
C***********************************************************************
C***********************************************************************
C     Storage Organization
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION WR(N),WI(N),Q(LDVS,N),IWORK(N),RPAR(*),IPAR(*)
      PARAMETER (ZERO=0.0D0,ONE=1.0D0)
C     DATA ATOL/1.D-0/,XLARGE/1.D30/
      DATA ATOL/1.D-4/,XLARGE/1.D30/
C***********************************************************************
C
C
C
C***********************************************************************
      XXXDIM = RDIM
      POSMAX = WR(IDMAX(NOS,WR,1))
      POSMIN = WR(IDMIN(NOS,WR,1))
C---- if RDIM < 0 ensure that XXXDIM is smaller than largest  ev.
      IF(RDIM.LT.0) THEN
        IF(POSMAX.GT.ZERO) THEN
          XXXDIM = MIN(RDIM,-POSMAX)
        ENDIF
      ENDIF
C***********************************************************************
C
C     obsolete block, for compatibility with older versions
C
C***********************************************************************
      IF(ISORT.EQ.0) THEN
        NOS = 0
        DO 101 I = 1,N
        IWORK(I) = 0
  101 CONTINUE
C***********************************************************************
C
C     Block to select the 'zero eigenvalues'
C
C***********************************************************************
      ELSE IF(ISORT.EQ.1) THEN
      NZEV = 0
      DO 110 I=1,NOS
        IF(DSQRT(WR(I)**2+WI(I)**2).LT.ATOL) THEN
          NZEV = NZEV + 1
          IWORK(I) = NZEV
        ELSE
          IWORK(I) = 0
        ENDIF
  110 CONTINUE
      IF(MDIM.EQ.0) MDIM = NZEV
C***********************************************************************
C
C     Block to select the 'non-zero eigenvalues'
C
C***********************************************************************
      ELSE IF(ISORT.EQ.-1) THEN
      NZEV = 0
      DO 120 I=1,NOS
        IF(DSQRT(WR(I)**2+WI(I)**2).GE.ATOL) THEN
          NZEV = NZEV + 1
          IWORK(I) = NZEV
        ELSE
          IWORK(I) = 0
        ENDIF
  120 CONTINUE
      IF(MDIM.EQ.0) MDIM = NZEV
C***********************************************************************
C
C     Block to select the 'most positive eigenvalues (acc. to real p.)'
C
C***********************************************************************
      ELSE IF(ISORT.EQ.2) THEN
      DO 210 I = 1,NOS
      IWORK(I) = 0
  210 CONTINUE
      DO 230 J=1,NOS
      XMAX = -XLARGE
      DO 220 I=1,NOS
C---- check whether this value hasn't been stored
      IF(IWORK(I).EQ.0) THEN
      IF(WR(I).GE.XMAX) THEN
         XMAX = WR(I)
         IND = I
      ENDIF
      ENDIF
  220 CONTINUE
      IWORK(IND) = J
  230 CONTINUE
      IF(MDIM.EQ.0) THEN
      DO 240 I=1,NOS
      IF(WR(I).GE.XXXDIM) MDIM = MDIM + 1
  240 CONTINUE
      ENDIF
C***********************************************************************
C
C     Block to select the 'most negative eigenvalues (acc. to real p.)'
C
C***********************************************************************
      ELSE IF(ISORT.EQ.-2) THEN
      IWORK(1:NOS) = 0
      DO 280 J=1,NOS
      XMIN = XLARGE
      DO 270 I=1,NOS
C---- check whether this value hasn't been stored
      IF(IWORK(I).EQ.0) THEN
      IF(WR(I).LT.XMIN) THEN
         XMIN = WR(I)
         IND = I
      ENDIF
      ENDIF
  270 CONTINUE
      IWORK(IND) = J
  280 CONTINUE
      IF(MDIM.EQ.0) THEN
      DO 290 I=1,NOS
      IF(WR(I).LE.XXXDIM) MDIM = MDIM + 1
  290 CONTINUE
      ENDIF
C***********************************************************************
C
C     Block to select the zero eigenvalues and then the
C       'most positive eigenvalues (acc. to real p.)'
C
C***********************************************************************
      ELSE IF(ISORT.EQ.3) THEN
      NZEV = 0
      DO 310 I=1,NOS
        IF(DSQRT(WR(I)**2+WI(I)**2).LT.ATOL) THEN
          NZEV = NZEV + 1
          IWORK(I) = NZEV
        ELSE
          IWORK(I) = 0
        ENDIF
  310 CONTINUE
      DO 330 J=NZEV+1,NOS
      XMAX = -XLARGE
      DO 320 I=1,NOS
C---- check whether this value hasn't been stored
      IF(IWORK(I).EQ.0) THEN
      IF(WR(I).GE.XMAX) THEN
         XMAX = WR(I)
         IND = I
      ENDIF
      ENDIF
  320 CONTINUE
      IWORK(IND) = J
  330 CONTINUE
      IF(MDIM.EQ.0) THEN
      DO 340 I=1,NOS
      IF(WR(I).GE.XXXDIM) MDIM = MDIM + 1
  340 CONTINUE
      ENDIF
C***********************************************************************
C-Sl- Sorting by absolute values----------------------------------------
C***********************************************************************
C
C     Block to select the zero eigenvalues and then the
C      largest eigenvalues (acc. to absolute values)
C
C***********************************************************************
      ELSE IF(ISORT.EQ.5) THEN
      NZEV = 0
      DO 515 I=1,NOS
        IF(DSQRT(WR(I)**2+WI(I)**2).LT.ATOL) THEN
          NZEV = NZEV + 1
          IWORK(I) = NZEV
        ELSE
          IWORK(I) = 0
        ENDIF
  515 CONTINUE
      DO 535 J=NZEV+1,NOS
C-Sl- Sorting by absolute value when IMAILD=3 - for const. matr.--------
      XMAX = XLARGE
      DO 525 I=1,NOS
C---- check whether this value hasnt been stored
        IF(IWORK(I).EQ.0) THEN
          IF(DSQRT(WR(I)**2+WI(I)**2).LT.XMAX) THEN
            XMAX = DSQRT(WR(I)**2+WI(I)**2)
            IND = I
          ENDIF
C-Sl- End of sorting----------------------------------------------------
        ENDIF
  525 CONTINUE
C-Et ask how its working
	  if (ind>nos) write(*,*) 'eig',wr,wi
      IWORK(IND) = J
  535 CONTINUE
      IF(MDIM.eq.0) then
      DO 545 I=1,NOS
      IF(WR(I).GE.XXXDIM) MDIM = MDIM + 1
  545 CONTINUE
      END IF
C***********************************************************************
C
C     Block to select the zero eigenvalues and then the
C       'most negative eigenvalues (acc. to real p.)'
C
C***********************************************************************
      ELSE IF(ISORT.EQ.-3) THEN
      NZEV = 0
      DO 360 I=1,NOS
        IF(DSQRT(WR(I)**2+WI(I)**2).LT.ATOL) THEN
          NZEV = NZEV + 1
          IWORK(I) = NZEV
        ELSE
          IWORK(I) = 0
        ENDIF
  360 CONTINUE
      DO 380 J=NZEV+1,NOS
      XMIN = XLARGE
      DO 370 I=1,NOS
C---- check whether this value hasn't been stored
      IF(IWORK(I).EQ.0) THEN
      IF(WR(I).LE.XMIN) THEN
         XMIN = WR(I)
         IND = I
      ENDIF
      ENDIF
  370 CONTINUE
      IWORK(IND) = J
  380 CONTINUE
      IF(MDIM.EQ.0) THEN
      DO 390 I=1,NOS
      IF(WR(I).LE.XXXDIM) MDIM = MDIM + 1
  390 CONTINUE
      ENDIF
C***********************************************************************
C
C     Block to select 'most positive eigenvalues (acc. to real p.)'
C       except for the zero eigenvalues
C
C***********************************************************************
      ELSE IF(ISORT.EQ.4) THEN
      NZEV = NOS + 1
      DO 410 I=1,NOS
        IF(DSQRT(WR(I)**2+WI(I)**2).LT.ATOL) THEN
          NZEV = NZEV - 1
C- errr
C         IWORK(I) = NZEV
          IWORK(I) = 0
        ELSE
          IWORK(I) = 0
        ENDIF
  410 CONTINUE
      DO 430 J=1,NZEV-1
      XMAX = -XLARGE
      DO 420 I=1,NOS
C---- check whether this value hasn't been stored
      IF(IWORK(I).EQ.0) THEN
      IF(WR(I).GE.XMAX. AND. (DSQRT(WR(I)**2+WI(I)**2).GT.ATOL)) THEN
         XMAX = WR(I)
         IND = I
      ENDIF
      ENDIF
  420 CONTINUE
      IWORK(IND) = J
  430 CONTINUE
      IF(MDIM.EQ.0) THEN
      DO 440 I=1,NOS
      IF(WR(I).GE.XXXDIM) MDIM = MDIM + 1
  440 CONTINUE
      ENDIF
C***********************************************************************
C
C     Block to select 'most negative eigenvalues (acc. to real p.)'
C       except for the zero eigenvalues
C
C***********************************************************************
      ELSE IF(ISORT.EQ.-4) THEN
      NZEV = NOS + 1
      DO 460 I=1,NOS
        IF(DSQRT(WR(I)**2+WI(I)**2).LT.ATOL) THEN
          NZEV = NZEV - 1
C- errr
C         IWORK(I) = NZEV
          IWORK(I) = 0
        ELSE
          IWORK(I) = 0
        ENDIF
  460 CONTINUE
      DO 480 J=1,NZEV-1
      XMIN = XLARGE
      DO 470 I=1,NOS
C---- check whether this value hasn't been stored
      IF(IWORK(I).EQ.0) THEN
      IF(WR(I).LE.XMIN. AND. (DSQRT(WR(I)**2+WI(I)**2).GT.ATOL)) THEN
         XMIN = WR(I)
         IND = I
      ENDIF
      ENDIF
  470 CONTINUE
      IWORK(IND) = J
  480 CONTINUE
      ELSE
      IF(MDIM.EQ.0) THEN
      DO 490 I=1,NOS
      IF(WR(I).LE.XXXDIM) MDIM = MDIM + 1
  490 CONTINUE
      ENDIF
C***********************************************************************
C     Error exit
C***********************************************************************
      GOTO 900
      ENDIF
C***********************************************************************
C     Regular Exit
C***********************************************************************
 1000 CONTINUE
C     WRITE(6,*) 'iwork',(IWORK(I),I=1,N)
      RETURN
  900 WRITE(6,*) ' wrong value of isort in SELEGV'
      STOP
      END
C***********************************************************************
C
C
C
C       THE SUBROUTINE start here
C
C
C
C
C***********************************************************************
      SUBROUTINE BACKIT(ICALL,N,MDIM,XLIMIT,FUNSYS,Y,YN,YP,
     1       A,LDA,VR,LDVR,VL,LDVL,PM,LDPM,WR,WI,LRW,RW,LIW,IW,
     1       LTW,TW,RPAR,IPAR,INFO,MSGFIL)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *                                                           *
C     *  Program to find points on low-dimensional manifolds      *
C     *                                                           *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C***********************************************************************
C
C  Arguments
C  =========
C
C  ICALL   (input) INTEGER
C          task for subroutine
C              ICALL = 0   return right hand side in YP
C              ICALL = 1   calculate eigenvector information
C              ICALL = 2   calculate point on the manifold
C
C  N       (input) INTEGER
C          dimension of the system
C
C  MDIM    (output) INTEGER
C          gives (for ICALL > 0) the local dimension of the manifold
C
C  XLIMIT  (input) DOUBLE PRECISION
C          smallest value of the eigenvalue, which is to be considered
C          slow
C
C  FUNSYS  (input) EXTERNAL
C          FUNSYS is the name of the user-supplied subroutine, which
C          evaluates the right hand side of the equation system
C          see below
C
C  Y       (input) DOUBLE PRECISION array, dimension (N)
C          On entry, Y contains the current point in the state space.
C
C  YN      (output) DOUBLE PRECISION array, dimension (N)
C          On sucessful completion (for ICALL = 2) YN contains the
C          current point on the local ILDM
C
C  YP      (output) DOUBLE PRECISION array, dimension (N)
C          On successful completion YP contains the reaction rates
C          at the new point (for ICALL = 2) or at the old point
C          (for ICALL < 2)
C
C  A       (output) DOUBLE PRECISION array, dimension (LDA,N)
C          On exit (for ICALL > 0) A is the matrix N of the
C          Schur decomposition, where M * Q = Q * N
C
C  LDA     (input) INTEGER
C          The leading dimension of the array A.  LDA >= max(1,N).
C
C
C  VR      (output) DOUBLE PRECISION array, dimension (LDVR,N)
C          For ICALL > 0
C          VR contains in its first 1:MDIM columns a set of vectors
C          spanning the eigenspace associated with the eigenvalues
C          1:MDIM, and in its MDIM+1:N columns a set of vectors
C          spanning the eigenspace associated with the eigenvalues
C          MDIM+1,N, where MDIM is the dimension of the slow subspace
C          The eigenvalues are sorted according to decreasing real part
C
C  LDVR    (input) INTEGER
C          The leading dimension of the array VR.  LDVR >= N
C
C  VL      (output) DOUBLE PRECISION array, dimension (LDVL,N)
C          For ICALL > 0 VL contains the inverse of VR
C
C  LDVL    (input) INTEGER
C          The leading dimension of the array VL.  LDVL >= N
C
C  PM      (output) DOUBLE PRECISION array, dimension (LDPM,N)
C          For ICALL > PM contains the projection matrix
C                                                 I  0
C                                       PM = VR *      * VL
C                                                 0  I
C
C  WR      (output) DOUBLE PRECISION array, dimension (N)
C  WI      (output) DOUBLE PRECISION array, dimension (N)
C          WR and WI contain the real and imaginary parts,
C          respectively, of the computed eigenvalues.  Complex
C          conjugate pairs of eigenvalues will appear consecutively
C          with the eigenvalue having the positive imaginary part
C          first.
C
C  Note:   the eigenvalues and Schurvectors, etc are evaluated for
C  ----->      ICALL = 2 "almost" at solution point on the ILDM
C              at the point coressponding to the previos iterate, which
C              differs from the solution point by ||YN-Y|| < RTOL, where
C              ||YN-Y|| = (sum_i ((YN-Y)/max(abs(yn),atol))**2)^{1/2}
C          for ICALL = 1 at the point given as input
C
C
C
C  RWORK   (workspace/output) DOUBLE PRECISION array, dimension (LRW)
C
C
C  LRW     (input) INTEGER
C          The dimension of the array WORK.
C          LRW >= N*(N+2). LRW is checked
C          For good performance, LRW must generally be larger.
C
C  IWORK   (workspace) INTEGER array, dimension (LIW)
C
C  LIW     (input) INTEGER
C          The dimension of the array IWORK.
C          Set LIW at least to N
C
C  TWORK   (workspace) LOGICAL array, dimension (LTW)
C
C  LTW     (input) INTEGER
C          The dimension of the array TWORK.
C          Set LTW at least to N
C
C  INFO    (input/output)
C          On input INFO.NE.0 produces test output
C          On output INFO gives the error flag; if INFO .GE.0 on
C          output, everything is o.k. and INFO has not been changed
C
C  MSGFIL  (input) INTEGER that gives the file number for output
C
C
C***********************************************************************
C
C     SUBROUTINE  FUNSYS(N,TTT,YN,YP,RPAR,IPAR,IEMR)
C
C      supply this subroutine to the code
C
C  Arguments:
C
C  N       (input) INTEGER
C          dimension of the system
C
C  TTT     (-----) DOUBLE PRECISION
C          not needed, simply for
C          compatibility with some other subroutines
C
C  YN      (input) DOUBLE PRECISION array, dimension (N)
C          current values of the composition
C
C  YP      (output) DOUBLE PRECISION array, dimension (N)
C          computed right hand side ("reaction rates")
C
C  RPAR    (-----) DOUBLE PRECISION array, dimension (*)
C          This array can be used for user communication. It is
C          not touched by the calling subroutines.
C
C  IPAR    (-----) INTEGER array, dimension (*)
C          This array can be used for user communication. It is
C          not touched by the calling subroutines.
C
C  IEMR    (output) INTEGER
C          set this value to 0 if everything is o.k.
C          set it to another value otherwise
C
C***********************************************************************
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (a-h,o-z)
      EXTERNAL FUNSYS
      PARAMETER (ONE=1.0D0,ZERO=0.0D0)
      DIMENSION Y(N),YP(N),YN(N)
      DIMENSION A(LDA,N),VL(LDVL,N),VR(LDVR,N),PM(LDPM,N),WR(N),WI(N)
      LOGICAL TW
      DIMENSION RW(LRW), IW(LIW), TW(LTW)
      DIMENSION RPAR(*),IPAR(*)
      PARAMETER (NDH = 100)
CCXFY ATOL changed for Surface reaction on 26.11.99
CCXFY      DATA MAXIT/20/,ATOL/1.D-11/,RTOL/1.D-3/
      DATA MAXIT/20/,ATOL/1.D-5/,RTOL/1.D-3/
C***********************************************************************
C     Build up work space
C***********************************************************************
      NR1 = 1
      NR2 = NR1 + N
      NR3 = NR2 + N
      NR4 = NR3 + N*N
      IF(NR4-1.GT.LRW) GOTO 910
C***********************************************************************
C     copy Y to YN
C***********************************************************************
      CALL DCOPY(N,Y,1,YN,1)
      NITER = 0
      IF(INFO.GT.0.AND.ICALL.GT.1) THEN
        WRITE(MSGFIL,812) (YN(I),I=1,N)
        WRITE(MSGFIL,810)
      ENDIF
  812 FORMAT(' Values before iteration ',/,6(1X,1PE10.3))
  813 FORMAT(' Values after iteration ',/,6(1X,1PE10.3))
C---- set the desired dimension of the slow subspace
C     MDIM = 0
      MDIMS = MDIM
   10 CONTINUE
      MDIM = MDIMS
      NITER = NITER + 1
      IF(NITER.GT.MAXIT) GOTO 970
      IF(INFO.GT.1) THEN
      WRITE(6,*) ' current values of the solution '
      WRITE(MSGFIL,*) (YN(I),I=1,N)
      ENDIF
  810 FORMAT(' Iteration monitor ',/,
     1   '    No. | Dim. |    ||f(y)||    |    ||y_n-y||   ',
     1   '|    ||y_n-y||_s',/,
     1   '------------------------------------------------------------')
  811 FORMAT(I7,' |',I5,' |',2X,1PE12.5,2X,'|',
     1    2X,1PE12.5,2X,'|',2X,1PE12.5)
C***********************************************************************
C     evaluate Jacobian
C***********************************************************************
      IF(ICALL.GT.0) THEN
        INFJAC = 0
C---- WR, PM, VL, VR used as work arrays (only because INFJAC = 0)
        CALL HOMJAC(INFJAC,FUNSYS,N,T,YN,YP,A,VL,VR,
     1      RPAR,IPAR,MSGFIL,IERR)
        IF(IERR.NE.0) GOTO 920
      ENDIF
C***********************************************************************
C     call right hand side
C***********************************************************************
      CALL FUNSYS(N,TTT,YN,YP,RPAR,IPAR,IERR)
      IF(IERR.NE.0) GOTO 930
      IF(ICALL.EQ.0) GOTO 600
C***********************************************************************
C    call eigenvalue routine
C***********************************************************************
C---- set INFO such that no test output is produced
      INFOE = INFO - 1
      CALL EGVROU(N,MDIM,XLIMIT,
     1       A,LDA,VR,LDVR,VL,LDVL,PM,LDPM,WR,WI,LRW,RW,LIW,IW,
     1       LTW,TW,INFOE)
      IF(INFOE.NE.0) GOTO 950
C---- if all time-scales are governing, then do not correct YN
      IF(MDIM.EQ.N) GOTO 100
C---- leave loop and do not iterate if ICALL < 2
      IF(ICALL.EQ.1) GOTO 600
C***********************************************************************
C    compute correction vector
C***********************************************************************
C---- copy N^{-1} IN
      NX = N-MDIM
      CALL DLACPY('Full',NX,NX,A(MDIM+1,MDIM+1),LDA,RW(NR3),NX)
      CALL DGEINV(NX,RW(NR3),RW(NR2),IW,IERR)
      IF(IERR.GT.0) GOTO 960
      CALL DGEMV('N',NX,N,ONE,VL(MDIM+1,1),LDVL,YP,1,ZERO,RW(NR2),1)
      FNOR = ZERO
      DO 210 I = 1,NX
      FNOR = RW(NR2-1+I)**2 + FNOR
  210 CONTINUE
      FNOR = SQRT(FNOR)
      CALL DSCAL(N,ZERO,RW(NR1),1)
      CALL DGEMV('N',NX,NX,ONE,RW(NR3),NX,RW(NR2),1,ZERO,RW(MDIM+NR1),1)
      CALL DGEMV('N',N,N,ONE,VR,LDVR,RW(NR1),1,ZERO,RW(NR2),1)
C     CALL DGEMV('N',N,N,ONE,PM,LDPM,YP,1,ZERO,RW(NR2),1)
C---- error based on old value of YN
      CALL DAXPY(N,-ONE,RW(NR2),1,YN,1)
      IAMAX = 0
      ERRTEM = -1.D30
      AERR = ZERO
      DO 220 I = 1,N
      DIFER=RW(NR2-1+I)
      IF(ABS(DIFER).GT.ERRTEM) THEN
        IAMAX = I
        ERRTEM = ABS(DIFER)
      ENDIF
      AERR = DIFER**2 + AERR
  220 CONTINUE
      AERR = SQRT(AERR)
      SERR = ZERO
      ISMAX = 0
      ERRTEM = -1.D30
      DO 230 I = 1,N
      DIFER= RW(NR2-1+I)/MAX(ABS(YN(I)),ATOL)
      IF(ABS(DIFER).GT.ERRTEM) THEN
        ISMAX = I
        ERRTEM = ABS(DIFER)
      ENDIF
      SERR = DIFER**2 + SERR
  230 CONTINUE
      SERR = SQRT(SERR)
C---- error based on old value of YN
C     CALL DAXPY(N,-ONE,RW(NR2),1,YN,1)
      IF(INFO.GE.1) WRITE(MSGFIL,811) NITER,MDIM,FNOR,AERR,SERR
C     write(6,*) IAMAX,RW(NR2-1+IAMAX),ISMAX,RW(NR2-1+ISMAX)
      IF(AERR.LT.ATOL) GOTO 600
CUM   IF(SERR.LT.RTOL) GOTO 600
C     IF(FNOR.LT.RTOL) GOTO 600
      GOTO 10
  100 CONTINUE
C***********************************************************************
C     solution exit
C***********************************************************************
  600 CONTINUE
      IF(INFO.GT.0.AND.ICALL.GT.1) THEN
        WRITE(MSGFIL,813) (YN(I),I=1,N)
      ENDIF
      RETURN
C***********************************************************************
C    error exits
C***********************************************************************
  910 CONTINUE
      INFO = -1
      RETURN
  920 CONTINUE
      INFO = -2
      RETURN
  930 CONTINUE
      INFO = -3
      RETURN
  950 CONTINUE
      INFO = -5
      RETURN
  960 CONTINUE
      WRITE(6,*) 'ierr in matrix inversion:',IERR,' MDIM =',MDIM
      WRITE(6,'(2(2X,1PE9.2))') (WR(I),WI(I),I=1,N)
      WRITE(6,*) 'possible cause: trying to decouple zero eigenvalue'
      INFO = -6
      RETURN
  970 CONTINUE
      INFO = -7
      RETURN
C***********************************************************************
C    end of back iteration subroutine
C***********************************************************************
      END
      SUBROUTINE EGVROU(N,MDIM,XLIMIT,
     1    A,LDA,VR,LDVR,VL,LDVL,PM,LDPM,WR,WI,
     1    LRW,RWORK,LIW,IWORK,LTW,TWORK,INFO)
C***********************************************************************
C
C     *************************************************************
C     *                                                           *
C     *                                                           *
C     *  Program to analyse the eigenvectors of a matrix A        *
C     *                                                           *
C     *                                                           *
C     *************************************************************
C
C***********************************************************************
C
C***********************************************************************
C
C  Arguments
C  =========
C
C  N       (input) INTEGER
C          dimension of the system
C
C  MDIM    (output) INTEGER
C          gives the dimension of the slow subspace
C
C  XLIMIT  (input) DOUBLE PRECISION
C          smallest value of the eigenvalue, which is to be considered
C          slow
C
C  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
C          On entry, the N-by-N matrix A which is to be analyzed
C          On exit, A has been overwritten by the matrix N of the
C          Schur decomposition, where M * Q = Q * N
C
C  LDA     (input) INTEGER
C          The leading dimension of the array A.  LDA >= max(1,N).
C
C
C  VR      (output) DOUBLE PRECISION array, dimension (LDVR,N)
C          VR contains in its first 1:MDIM columns a set of vectors
C          spanning the eigenspace associated with the eigenvalues
C          1:MDIM, and in its MDIM+1:N columns a set of vectors
C          spanning the eigenspace associated with the eigenvalues
C          MDIM+1,N, where MDIM is the dimension of the slow subspace
C          The eigenvalues are sorted according to decreasing real part
C
C  LDVR    (input) INTEGER
C          The leading dimension of the array VR.  LDVR >= N
C
C  VL      (output) DOUBLE PRECISION array, dimension (LDVL,N)
C          VL contains the inverse of VR
C
C  LDVL    (input) INTEGER
C          The leading dimension of the array VL.  LDVL >= N
C
C  PM      (output) DOUBLE PRECISION array, dimension (LDPM,N)
C          PM contains the projection matrix
C                                                 I  0
C                                       PM = VR *      * VL
C                                                 0  I
C
C  WR      (output) DOUBLE PRECISION array, dimension (N)
C  WI      (output) DOUBLE PRECISION array, dimension (N)
C          WR and WI contain the real and imaginary parts,
C          respectively, of the computed eigenvalues.  Complex
C          conjugate pairs of eigenvalues will appear consecutively
C          with the eigenvalue having the positive imaginary part
C          first.
C
C
C  RWORK   (workspace/output) DOUBLE PRECISION array, dimension (LRW)
C          On exit, if INFO = 0, RWORK(1) returns the optimal LRW.
C
C  LRW     (input) INTEGER
C          The dimension of the array WORK.
C          LRW >= 3*N. LRW is checked
C          For good performance, LRW must generally be larger.
C
C  IWORK   (workspace) INTEGER array, dimension (LIW)
C
C  LIW     (input) INTEGER
C          The dimension of the array IWORK.
C          Set LIW at least to N
C
C  TWORK   (workspace) LOGICAL array, dimension (LTW)
C
C  LTW     (input) INTEGER
C          The dimension of the array TWORK.
C          Set LTW at least to N
C
C  INFO    (input/output)
C          On input INFO.NE.0 produces test output
C          On output INFO gives the error flag; if INFO = 0 on
C          output, everything is o.k.
C
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (a-h,o-z)
      EXTERNAL XELECT
      LOGICAL TWORK
      DIMENSION A(LDA,N),VR(LDVR,N),VL(LDVL,N),PM(LDPM,N)
      DIMENSION WR(N),WI(N)
      DIMENSION RPAR(1),IPAR(1)
      DIMENSION RWORK(LRW),IWORK(LIW),TWORK(LTW)
      PARAMETER (ONE=1.0D0,ZERO=0.0D0)
C***********************************************************************
C    initialize
C***********************************************************************
C---- set the desired dimension of the slow subspace
C     MDIM = 0
C***********************************************************************
C     Schur decomposition
C***********************************************************************
      JOB =  1
      INFOO = INFO
      CALL HOMEVC(JOB,N,A,LDA,MDIM,XLIMIT,'D','T',
     $             WR,WI,VL,LDVL,VR,LDVR,PM,LDPM,SRCONE,SRCONV,
     $        RWORK,lrw,IWORK,LIW,TWORK,LTW,RPAR,IPAR,6,INFO)
      IF(INFO.NE.0) THEN
        WRITE(6,*) ' Error in eigenvalue routine, INFO =',INFO
CCXFY TEST changed on 26.11.99
        RETURN
CUM        STOP
      ENDIF
C***********************************************************************
C                                                       I  0
C     This block will compute the projection matrix V *      * V^{-1}
C                                                       0  0
C
C     and store it in the matrix  PM
C***********************************************************************
      IF(MDIM.EQ.0) THEN
      DO 50 I = 1,N
      DO 50 J = 1,N
      PM(I,J) = ZERO
   50 CONTINUE
      ELSE
      CALL DGEMM('N','N',N,N,MDIM,ONE,VR,LDVR,VL,LDVL,ZERO,PM,LDPM)
      ENDIF
C---- the following block is simply a check to see how vectors
C     are projected onto the slow subspace. It simply projects the
C     vectors of the separated subspaces. The vectors MDIM+1:N should
C     zero out when premultiplied by the projection matrix
      IF(INFOO.NE.0) THEN
      DO 80 I = 1,N
      CALL DGEMV('N',N,N,ONE,PM,LDPM,VR(1,I),1,ZERO,RWORK,1)
C     write(6,*) (RWORK(j),j=1,n)
  80  CONTINUE
      ENDIF
      RETURN
C***********************************************************************
C
C***********************************************************************
      END
      SUBROUTINE TESTEV
C***********************************************************************
C     this program builds up a matrix and tests the subroutine
C     UGEEVS
C
C***********************************************************************
      implicit DOUBLE PRECISION (a-h,o-z)
      EXTERNAL XELECT
      parameter (n=5,lrw=1000,liw=1000,ltw=100)
      parameter (lda=n,ldb=n,ldvl=n,ldvr=n,ldvs=n)
      logical twork
      dimension a(lda,n),b(ldb,n),
     1    wr(n),wi(n),vl(ldvl,n),VR(ldvr,n),vs(ldvs,n),
     1    rpar(1),ipar(1),
     1    RWORK(lrw),iwork(liw),twork(ltw)
      parameter (one=1.0d0,zero=0.0d0)
C***********************************************************************
C    build matrix
C***********************************************************************
      data a/  -5.0d0 , -1.0d0 ,  1.0d0 ,  0.0d0, 1.0d0,
     1          2.0d0 ,  0.5d0 , -2.0d0 ,  1.0d0, 1.0d0,
     2          3.0d0 ,  2.0d0 ,  1.0d0 ,  1.0d0, 1.0d0,
     2         -1.0d0 ,  0.0d0 , -1.0d0 ,  0.0d0, 0.3d0,
     2          0.0d0 ,  0.0d0 ,  0.0d0 ,  0.0d0, 0.0d0/
C***********************************************************************
C    initialize
C***********************************************************************
C***********************************************************************
C    transpose matrix for fortran storage
C      this is done because the data statement should be nice to read
C     (column by column)
C***********************************************************************
      do 10 i=1,n
      do 20 j=1,i-1
      aij = a(i,j)
      aji = a(j,i)
      a(i,j) = aji
      a(j,i) = aij
   20 continue
   10 continue
C***********************************************************************
C    set parameters for subspace separation
C***********************************************************************
C---- put the zero eigenvalues at the top left and then sort the
C     remaining eigenvalues such that they appear in the order of their
C     real part with the largest first and the smallest last
C---- set INFO such that test output is produced
      INFO = 1
C---- set the desired dimension of the slow subspace
      MDIM = 2
C***********************************************************************
C     Schur decomposition
C***********************************************************************
      JOB = 1
      MDIM = 0
      RDIM = -1.0D0
      CALL HOMEVC(JOB,N,A,LDA,MDIM,RDIM,'A','N',
     $             WR,WI,VL,LDVL,VR,LDVR,VS,LDVS,SRCONE,SRCONV,
     $        RWORK,lrw,IWORK,LIW,TWORK,LTW,RPAR,IPAR,6,INFO)
      IF(INFO.NE.0) THEN
        WRITE(6,*) ' Error in eigenvalue routine '
        WRITE(6,*) ' Error in eigenvalue routine '
        WRITE(6,*) ' Error in eigenvalue routine '
        STOP
      ENDIF
C***********************************************************************
C                                                       I  0
C     This block will compute the projection matrix V *      * V^{-1}
C                                                       0  I
C
C     and store it in the matrix  b
C***********************************************************************
      CALL DGEMM('N','N',N,N,MDIM,ONE,VR,LDVR,VL,LDVL,ZERO,B,LDB)
C---- the following block is simply a check to see how vectors
C     are projected onto the slow subspace. It simply projects the
C     vectors of the separated subspaces. The vectors MDIM+1:N should
C     zero out when premultiplied by the projection matrix
      DO 80 I = 1,N
       CALL DGEMV('N',N,N,ONE,B,LDB,VR(1,I),1,ZERO,RWORK,1)
      write(6,*) (RWORK(j),j=1,n)
  80  CONTINUE
      STOP
C***********************************************************************
C     end of test program
C***********************************************************************
      END
