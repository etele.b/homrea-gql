/*
 $Id: mgctest.c,v 1.1.1.1 1996/11/04 12:05:46 roitzsch Exp $
 (C)opyright 1996 by Konrad-Zuse-Center, Berlin
 All rights reserved.
 Part of the common environment

$Log: mgctest.c,v $
Revision 1.1.1.1  1996/11/04 12:05:46  roitzsch
minigraph module

*/

#include <stdio.h>
#include <c_parameter.h>

int main(int argc, char *argv[], char *env[])
  {
    int id=-1, no=-1;
    int dummy;
    double x[5], y[5];

    printf("Enter number to start: ");
    fflush(stdout);
    scanf("%d",&dummy);

    printf("Start mgctest\n");
    zibwop(&id, &no);
    if (no==-1)
      { printf("OJeh\n"); return 0; }
    zibset(-1, -1, -1, PREC, DOUBLE);
    zibset(id, no, -1, BACKGRCOL, YELLOW);

    x[0] = 0.1; y[0] = 0.1;
    x[1] = 0.9; y[1] = 0.1;
    x[2] = 0.9; y[2] = 0.9;
    x[3] = 0.1; y[3] = 0.9;
    x[4] = 0.1; y[4] = 0.1;
    zibset(id, no, -1, FILLCOL, RED);
    zibfl(id, no, -1, x, y,  5);

    zibset(id, no, -1, CAPTION, "Rechts oben");
    zibset(id, no, -1, FONTSIZE, BIG);
    zibtx(id, no, -1, 0.5, 0.5,  "ABCDEFDGHIJKLM");

    x[0] = 0.2; y[0] = 0.2;
    x[1] = 0.8; y[1] = 0.2;
    x[2] = 0.8; y[2] = 0.8;
    x[3] = 0.2; y[3] = 0.8;
    x[4] = 0.2; y[4] = 0.2;
    zibset(id, no, -1, PENCOL, YELLOW);
    zibset(id, no, -1, PENSIZE, BIG);
    zibpl(id, no, -1, x, y,  5);

    printf("Enter number to quit: ");
    fflush(stdout);
    scanf("%d",&dummy);
    zibwcl(id,no,-1);

    return 1;
  }
