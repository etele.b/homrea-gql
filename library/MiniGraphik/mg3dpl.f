      SUBROUTINE MG3DPL(NUCALL,SIADD,SINTER,SSCALE,SPOST,NZHD,HEADER,
     1          NC,NO,NF,YA,LDY,IOY,IVERT,NVC,SYMBY,KONS,
     1          YMIN,YMAX)
C***********************************************************************
C 
C  Arguments
C  =========
C
C  NUCALL  (input) INTEGER
C
C          task
C          NUCALL = 0   initialize windows
C          NUCALL > 0   take old windows     
C
C  SIADD   (input) CHARACTER*1 
C          SIADD  = 'A' add new curves to the old ones
C                       replot otherwise
C
C  SINTER  (input) CHARACTER*1 
C          SINTER = 'I' use interactive version with mouse control
C                       use batch version otherwise
C
C  SSCALE  (input) CHARACTER*1 
C          SSCALE = 'S' do internal scaling of the plots
C
C  SPOST   (input) CHARACTER*1 
C          SPOST  = 'P' output on Postscript-file
C
C  NZHD    (input) INTEGER       
C          Number of letters in header
C
C  HEADER  (input) CHARACTER*(*) 
C          Header for the legend
C
C  NC      (input) INTEGER
C          The number of cells to be plotted
C
C  NO      (input) INTEGER
C          The number of points to be plotted
C
C  NF      (input) INTEGER
C          number of functions
C
C  YA      (input) DOUBLE PRECISION array, dimension (LDY,*)
C          The Y-coordinates of the points    
C
C  LDY     (input) INTEGER
C          leading dimension of array YA      
C
C  IOY     (input) INTEGER
C          if IOY = 1 then the loop over the points is assumed to 
C          be over the first index of the array
C          if IOY = 2 then the loop over the points is assumed to 
C          be over the second index of the array
C
C  SYMBY   (input) CHARACTER*8 array, dimension (LDY)
C          The symbols of the Y-coordinates    
C
C  KONS    (input) INTEGER array, dimension (3)
C          The indices of the Y-coordinates to be plotted 
C          If there is a KONS, which is < 0, a two-Y plot will be 
C          generated with all KONS > 0 on the left axis and
C          all KONS < 0 on the right axis
C
C  YMIN    (input/output) DOUBLE PRECISION array, dimension (*)
C          Minimum value of the Y-coordinates
C          If YMIN(I)=YMAX(I) on input, and I is the index of a 
C          species considered for plotting (see KONS), a scaling will 
C          be done and the value of YMIN(I) will be corrected
C
C  YMAX    (input/output) DOUBLE PRECISION array, dimension (*)
C          Maximum value of the Y-coordinates
C          If YMIN(I)=YMAX(I) on input, and I is the index of a 
C          species considered for plotting (see KONS), a scaling will 
C          be done and the value of YMAX(I) will be corrected
C
C     
C***********************************************************************
      INCLUDE 'parameter.h'
      INTRINSIC MAX,MIN
      DOUBLE PRECISION, DIMENSION(*)          :: YMIN,YMAX
      DOUBLE PRECISION, DIMENSION(LDY,*)      :: YA
      INTEGER,          DIMENSION(3)          :: KONS
      INTEGER,          DIMENSION(NVC,NC)     :: IVERT
      REAL,             DIMENSION(NO,3)       :: RW
      CHARACTER(LEN=*) SYMBY(LDY)
      CHARACTER HEADER*(*)
      CHARACTER*1 SIADD,SSCALE,SINTER,SPOST
      COMMON/BICOL/IFGCOL,IBGCOL
C***********************************************************************
C     Local variables
C***********************************************************************
      PARAMETER (XPMI = 0.12,XPMA = 0.6,YPMI=0.1,YPMA=0.9,TOFF=0.05,
     1           XLINO = 0.02) 
      PARAMETER (XLARGE = 1.E30,XSMALL = 1.E-30)
      INTEGER TYPE,BUTTON,CH
      LOGICAL INTER,ICREA,ISCAL,EQUSYM,IADDP,POSTSC,TWOPLO
      DIMENSION XBOX(5),YBOX(5)
C     DIMENSION XML(2),YML(2) 
      PARAMETER (ONE=1.0,ZERO=0.0)
      DIMENSION YMI(3),YMA(3)
      DIMENSION XG(5),YG(5),ZG(5)
      SAVE
C***********************************************************************
C     
C***********************************************************************
      IIK = KONS(1)
      IF(IIK.EQ.0) THEN
        WRITE(*,*) ' enter 3 plotting variables '
        READ(*,*) KONS(1),KONS(2),KONS(3)
      ENDIF
  111 CONTINUE
      IF(IOY.NE.1.AND.IOY.NE.2) THEN
         WRITE(6,*) '  wrong IOY in MG2DPL'
         STOP
      ENDIF
C***********************************************************************
C     Initialize
C***********************************************************************
                                 ISCAL = .FALSE. 
      IF(EQUSYM(1,SSCALE,'S'))   ISCAL = .TRUE.
                                 INTER = .FALSE. 
      IF(EQUSYM(1,SINTER,'I'))   INTER = .TRUE.
                                 IADDP = .FALSE. 
      IF(EQUSYM(1,SIADD,'A'))    IADDP = .TRUE.
                                 POSTSC= .FALSE. 
      IF(EQUSYM(1,SPOST,'P'))    POSTSC= .TRUE.
      ICREA = .FALSE.
C***********************************************************************
C
C     Define and open window 
C
C***********************************************************************
      IASS = 1
C***********************************************************************
C     define kind and size of window 
C***********************************************************************
      IF(NUCALL.EQ.0) THEN
        IF(.NOT.POSTSC) THEN
          IDSCR = SCREEN
          IWORX = 500
          IWORY = 830
          IWWDT = 600
          IWHGT = 370
          CALL ZIBWOP(IDSCR, NOPL)
          CALL ZIBSET(IDSCR, NOPL, IASS, CAPTION,'Output Window')
C         CALL ZIBSET(IDSCR, NOPL, IASS, CAPTION,AAA)
        ELSE
          IDSCR = PS_TEX 
          IWORX =  20
          IWORY =  20
          IWWDT = 600
          IWHGT = 370
          CALL ZIBWOP(IDSCR, NOPL)
          CALL ZIBSET(IDSCR, NOPL,IASS, FILENAME, 'Postscript_9')
        ENDIF
        CALL ZIBSET(IDSCR, NOPL, IASS, WDORGX, IWORX)
        CALL ZIBSET(IDSCR, NOPL, IASS, WDORGY, IWORY)
        CALL ZIBSET(IDSCR, NOPL, IASS, WDWDTH, IWWDT)
        CALL ZIBSET(IDSCR, NOPL, IASS, WDHGHT, IWHGT)
        CALL ZIBSET(IDSCR, NOPL, IASS, MINX, 0.00)
        CALL ZIBSET(IDSCR, NOPL, IASS, MINY, 0.00)
        CALL ZIBSET(IDSCR, NOPL, IASS, MAXX, 1.00)
        CALL ZIBSET(IDSCR, NOPL, IASS, MAXY, 1.00)
      ENDIF
C***********************************************************************
C     define foreground and background colors
C***********************************************************************
      IMCOL1 = 62
      IMCOL2 = 63
      CALL ZIBCOL(IDSCR, IMCOL1, 255,222,173)
      CALL ZIBCOL(IDSCR, IMCOL2, 70, 0, 0)
      IF(0.GT.1.AND.POSTSC) THEN
        IFGCOL = WHITE 
        IBGCOL = IMCOL2
      ELSE
        IFGCOL = BLACK
        IBGCOL = WHITE  
      ENDIF
      CALL ZIBSET(IDSCR, NOPL, IASS, BACKGRCOL, IBGCOL)
      CALL ZIBSET(IDSCR, NOPL, IASS, SCALFIT, 0)
C***********************************************************************
C     Color table  
C***********************************************************************
      DO 45 I = 32,60
      XXX = FLOAT(I-32)/FLOAT(60-32)
      RR = MAX(0.0,2.0*(XXX-0.5))
      BB = MAX(0.0,2.0*(0.5-XXX))
      IF(XXX.LT.0.5) THEN
        GG = MAX(0.0,1.0-2.0*(0.5-XXX))
      ELSE
        GG = MAX(0.0,1.0-2.0*(XXX-0.5))
      ENDIF
      RGBM = MAX(RR,GG,BB)
      RR = RR/RGBM
      GG = GG/RGBM
      BB = BB/RGBM
        IGG= INT(255.0* GG)
        IRR= INT(255.0* RR)
        IBB= INT(255.0* BB)
      CALL ZIBCOL(IDSCR, I, IRR,IGG,IBB)
   45 CONTINUE
C***********************************************************************
C     Check if this has to be an Y1-Y2 plot 
C***********************************************************************
      TWOPLO = .FALSE.
C***********************************************************************
C     Get Extreme Values of the Y-coordinate
C***********************************************************************
      IF(NUCALL.GT.0.AND.IADDP) THEN
C---- do not scale for successive call with IADDP = True
      ELSE
      DO 110 I=1,3
        III = ABS(KONS(I))
        IF(ABS(YMIN(III)-YMAX(III)).LE.XSMALL) THEN
          YMIN(III) =  XLARGE
          YMAX(III) = -XLARGE
          DO 150 L=1,NO
            IF(IOY.EQ.1) THEN
            YMIN(III)  = MIN(YMIN(III),YA(L,III))
            YMAX(III)  = MAX(YMAX(III),YA(L,III))
            ELSE
              YMIN(III)  = MIN(YMIN(III),YA(III,L))
              YMAX(III)  = MAX(YMAX(III),YA(III,L))
            ENDIF
  150     CONTINUE
        ENDIF
        YMI(I) = YMIN(III)
        YMA(I) = YMAX(III)
  110 CONTINUE
      ENDIF
C***********************************************************************
C     Plot
C***********************************************************************
  200 CONTINUE
C***********************************************************************
C     Plot Axis 
C***********************************************************************
      IF(NUCALL.GT.0.AND.IADDP) THEN
C---- do not Plot axis for successive call with IADDP = True
      ELSE
      XLEN = XPMA - XPMI
      CALL PLTAX('X ',IDSCR,NOPL,IASS,XPMI,YPMI,XLEN,5,
     1     YMI(1),YMA(1),SYMBY(KONS(1)))
      XLEN = YPMA - YPMI
      CALL PLTAX('YL',IDSCR,NOPL,IASS,XPMI,YPMI,XLEN,5,
     1     YMI(2),YMA(2),SYMBY(KONS(2)))
      ENDIF
      IF(IADDP.OR.NUCALL.EQ.0) THEN
      CALL ZIBSET(IDSCR,NOPL, IASS, FILLCOL, IBGCOL)
      XLL = XPMA+TOFF+0.09
      XLR = 0.95
      YLU = YPMA
      YLO = 1.0
      XBOX(1) = XLL
      YBOX(1) = YLU
      XBOX(2) = XLR
      YBOX(2) = YLU
      XBOX(3) = XLR
      YBOX(3) = YLO
      XBOX(4) = XLL
      YBOX(4) = YLO
      XBOX(5) = XLL
      YBOX(5) = YLU
      CALL ZIBFL(IDSCR, NOPL, IASS, XBOX, YBOX, 5)
      CALL ZIBSET(IDSCR,NOPL, IASS, FONTCOL, IFGCOL)
C     CALL ZIBTX(IDSCR, NOPL, IASS, XPMA+TOFF+0.09, YPMA,HEADER(1:NZHD))
      ENDIF
C***********************************************************************
C
C     Loop for plotting the curves 
C
C***********************************************************************
C***********************************************************************
C     Scale Y-coordinate 
C***********************************************************************
      DO 290 I=1,3
      III = ABS(KONS(I))
C***********************************************************************
C     Compute Scaling Factors
C***********************************************************************
      YSCA = ONE 
      ICSCA = 0  
      DO 230 L=1,NO
      IF(IOY.EQ.1) THEN
      RW(L,I) = YSCA * (YA(L,III) - YMI(I))/ MAX(YMA(I)-YMI(I),1.E-20)
      ELSE
      RW(L,I) = YSCA * (YA(III,L) - YMI(I))/ MAX(YMA(I)-YMI(I),1.E-20)
      ENDIF
  230 CONTINUE
      IF(I.EQ.1) THEN 
        HDEL = XPMA-XPMI
        HORI = XPMI
      DO 240 L=1,NO
      RW(L,I) = RW(L,I) * HDEL + HORI 
  240 CONTINUE
      ELSE IF(I.EQ.2) THEN
        HDEL = YPMA-YPMI
        HORI = YPMI
      DO 241 L=1,NO
      RW(L,I) = RW(L,I) * HDEL + HORI 
  241 CONTINUE
      ELSE 
      ENDIF
  290 CONTINUE
C***********************************************************************
C     Plot Legend
C***********************************************************************
C     GOTO 290
      XST = 0.06
      XOT = XPMA + TOFF +0.09
      YOT = YPMA - I*XST 
      DAD = 0.03
      NUMCOL = 4
C---- Get number for colors and markers
      INDMAR = 2
      INDCOL = 0
      CALL ZIBSET(IDSCR, NOPL, IASS, FONTCOL, INDCOL)
      CALL ZIBSET(IDSCR, NOPL, IASS, PENCOL, INDCOL)
      CALL ZIBTX(IDSCR, NOPL, IASS, XOT+DAD, YOT, SYMBY(III))
      CALL ZIBSET(IDSCR, NOPL, IASS, PENSIZE, SMALL)
      CALL ZIBSET(IDSCR, NOPL, IASS, LINESTYLE, SOLID)
      CALL ZIBSET(IDSCR, NOPL, IASS, MARKER, INDMAR)
      CALL ZIBSET(IDSCR, NOPL, IASS, MARKCOL, INDCOL)
C     CALL ZIBPM(IDSCR, NOPL, IASS, XOT, YOT, 1)
C     XLML = 0.015
C     XML(1) = XOT-XLML
C     XML(2) = XOT+XLML
C     YML(1) = YOT
C     YML(2) = YOT
C     CALL ZIBPL(IDSCR, NOPL, IASS, XML, YML, 2)
C      
      DO 561 L=1,NC
      IF(NVC.EQ.4) THEN
        XG(1) = RW(IVERT(1,L),1)
        YG(1) = RW(IVERT(1,L),2)
        ZG(1) = RW(IVERT(1,L),3)
        XG(2) = RW(IVERT(2,L),1)
        YG(2) = RW(IVERT(2,L),2)
        ZG(2) = RW(IVERT(2,L),3)
        XG(3) = RW(IVERT(4,L),1)
        YG(3) = RW(IVERT(4,L),2)
        ZG(3) = RW(IVERT(4,L),3)
        XG(4) = RW(IVERT(3,L),1)
        YG(4) = RW(IVERT(3,L),2)
        ZG(4) = RW(IVERT(3,L),3)
        XG(5) = XG(1)
        YG(5) = YG(1)
        ZG(5) = ZG(1)
        ZAV = (ZG(1)+ZG(2)+ZG(3)+ZG(4))/4.0D0
        NUCV = 5
       ELSE IF(NVC.EQ.3) THEN
        XG(1) = RW(IVERT(1,L),1)
        YG(1) = RW(IVERT(1,L),2)
        ZG(1) = RW(IVERT(1,L),3)
        XG(2) = RW(IVERT(2,L),1)
        YG(2) = RW(IVERT(2,L),2)
        ZG(2) = RW(IVERT(2,L),3)
        XG(3) = RW(IVERT(3,L),1)
        YG(3) = RW(IVERT(3,L),2)
        ZG(3) = RW(IVERT(3,L),3)
        XG(4) = XG(1)
        YG(4) = YG(1)
        ZG(4) = ZG(1)
        ZAV = (ZG(1)+ZG(2)+ZG(3))/3.0D0
        NUCV = 4
       ELSE IF(NVC.EQ.2) THEN
        XG(1) = RW(IVERT(1,L),1)
        YG(1) = RW(IVERT(1,L),2)
        ZG(1) = RW(IVERT(1,L),3)
        XG(2) = RW(IVERT(2,L),1)
        YG(2) = RW(IVERT(2,L),2)
        ZG(2) = RW(IVERT(2,L),3)
        XG(3) = RW(IVERT(1,L),1)
        YG(3) = RW(IVERT(1,L),2)
        ZG(3) = RW(IVERT(1,L),3)
        ZAV = (ZG(1)+ZG(2))/2.0D0
        NUCV = 3
      ELSE
        WRITE(*,*) ' NVC IS NOT 2, 3 OR 4 IN MG3DPL'
        STOP
      ENDIF
CGRAY IFCO = 31 - INT(ZG(1)*24.0)
      IFCO = 32 + INT(ZAV * 28.0)
      CALL ZIBSET(IDSCR,NOPL, IASS, FILLCOL, IFCO)
      CALL ZIBFL(IDSCR, NOPL, IASS, XG, YG, NUCV)
      CALL ZIBPL(IDSCR, NOPL, IASS, XG, YG, NUCV)
      IF(2.GT.1) THEN
      Do 454 IIC=1,NVC
      IFCO = 32 + INT(ZG(IIC)**4 * 28.0)
      CALL ZIBSET(IDSCR,NOPL, IASS, MARKCOL, IFCO)
      CALL ZIBSET(IDSCR,NOPL, IASS, MARKER, BULLET)
      CALL ZIBPM(IDSCR, NOPL, IASS, XG(IIC), YG(IIC), 1)
  454 CONTINUE
      ENDIF
  561 CONTINUE
C 290 CONTINUE
C***********************************************************************
C
C     Allow zooming in interactive mode
C
C***********************************************************************
      IF(INTER) THEN
      CALL ZIBSET(IDSCR,NOPL, IASS, FONTCOL, IFGCOL)
      CALL ZIBTX(IDSCR, NOPL, IASS, 0.90, 0.965, 'End     ')
      CALL ZIBTX(IDSCR, NOPL, IASS, 0.90, 0.935, 'Quit    ')
      CALL ZIBTX(IDSCR, NOPL, IASS, 0.90, 0.905, 'New Plot')
      CALL ZIBWT(IDSCR,NOPL,TYPE,BUTTON,X1,Y1,CH)
      IF(X1.GT.0.90.AND.Y1.GT.0.965) GOTO 995
      IF(X1.GT.0.90.AND.Y1.GT.0.935) GOTO 996
      IF(X1.GT.0.90.AND.Y1.GT.0.905) THEN
        WRITE(*,*) ' enter new plotting variables '
        READ(*,*) KONS(1),KONS(2),KONS(3)
        NUCALL = 0 
        CALL ZIBCLR(IDSCR,NOPL,IASS)
        CALL ZIBWCL(IDSCR,NOPL,IASS)
        GOTO 111
      ENDIF
      IF(TYPE.LT.8) GOTO 999
      CALL ZIBGIN(IDSCR,NOPL,RECTANGLE,X1,Y1,X2,Y2)
      IF(X1.GT.X2) THEN
       Z = X1
       X1 = X2 
       X2 = Z
      ENDIF
      IF(Y1.GT.Y2) THEN
       Z = Y1
       Y1 = Y2
       Y2 = Z
      ENDIF
      XBOX(1) = X1
      XBOX(2) = X2
      XBOX(3) = X2
      XBOX(4) = X1
      XBOX(5) = X1
      YBOX(1) = Y1
      YBOX(2) = Y1
      YBOX(3) = Y2
      YBOX(4) = Y2
      YBOX(5) = Y1
      IF(.NOT.ICREA.AND.NUCALL.EQ.0) THEN 
        CALL ZIBWOP(IDSCR,NONEW)
        ICREA = .TRUE.
      ELSE
        CALL ZIBCLR(IDSCR,NONEW,IASS)
      ENDIF
      CALL ZIBSET(IDSCR,NONEW,IASS,CAPTION,'Zoom Window')
      CALL ZIBSET(IDSCR,NONEW,IASS,MINX,X1)
      CALL ZIBSET(IDSCR,NONEW,IASS,MAXX,X2)
      CALL ZIBSET(IDSCR,NONEW,IASS,MINY,Y1)
      CALL ZIBSET(IDSCR,NONEW,IASS,MAXY,Y2)
      CALL ZIBSET(IDSCR,NONEW,IASS,SCALFIT,0)
      CALL ZIBADD(IDSCR,NOPL ,IDSCR, NONEW)
      CALL ZIBSET(IDSCR,NOPL, IASS, BACKGRCOL, IBGCOL)
      CALL ZIBSET(IDSCR,NOPL, IASS, PENCOL, IFGCOL)
      CALL ZIBPL(IDSCR,NOPL,IASS,XBOX,YBOX,5)    
      GOTO 200
      ENDIF 
      IF(.NOT.IADDP) THEN 
        CALL ZIBCLR(IDSCR,NOPL,IASS)
        CALL ZIBWCL(IDSCR,NOPL,IASS)
      ENDIF
  995 CONTINUE
      IF(.NOT.IADDP) THEN 
        CALL ZIBCLR(IDSCR,NOPL,IASS)
        CALL ZIBWCL(IDSCR,NOPL,IASS)
        NUCALL = -1
      ENDIF
      GOTO 999
  996 CONTINUE
      IF(.NOT.IADDP) THEN 
        NUCALL = -1
      ENDIF
      GOTO 999
  999 CONTINUE
C
      NOPL=1
      RETURN
      END
c         SUBROUTINE TPM(IDSCR, NO1, IASS, X, Y, MARK, S, ICOL)
c         INCLUDE 'parameter.h'
c         CHARACTER S*(*)
C
c         CALL ZIBTX(IDSCR, NO1, IASS, X, Y, S)
c         CALL ZIBSET(IDSCR, NO1, IASS, PENSIZE, SMALL)
c         CALL ZIBSET(IDSCR, NO1, IASS, MARKER, MARK)
c         CALL ZIBSET(IDSCR, NO1, IASS, MARKCOL, ICOL)
c
c         CALL TPMRK(IDSCR, NO1, IASS, X+0.2, Y)
c         CALL TCMRK(IDSCR, NO1, IASS, X+0.3, Y)
c         RETURN
