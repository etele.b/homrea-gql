/* $Id: c_parameter.h,v 1.1.1.1 1996/11/04 12:05:46 roitzsch Exp $ */

#ifndef __C_PARAMETER__
#define __C_PARAMETER__

#include <mgconst.h>

/* to handle systems with fortran names without underscore */

#define zibpl c_zibpl
#define zibpm c_zibpm
#define zibfl c_zibfl
#define zibtx c_zibtx
#define zibcol c_zibcol
#define zibset c_zibset
#define zibreq c_zibreq
#define zibclr c_zibclr
#define zibwop c_zibwop
#define zibwcl c_zibwcl
#define zibadd c_zibadd
#define zibsub c_zibsub
#define zibgin c_zibgin
#define zibev c_zibev
#define zibwt c_zibwt
#define zibstr c_zibstr

/* ANSI C */

EXTERN int zibpl(int id, int no, int ass, void *xAdr, void *yAdr, int n);
EXTERN int zibpm(int id, int no, int ass, void *xAdr, void *yAdr, int n);
EXTERN int zibfl(int id,int  no, int ass, void *xAdr, void *yAdr, int n);
EXTERN int zibtx(int id, int no, int ass, double x, double y, char *text);
EXTERN int zibcol(int id, int col_no, int rVal, int gVal, int bVal);
EXTERN int zibset(int id, int no, int ass, int type, ...);
EXTERN int zibreq(int id, int no, int type, void *valAdr);
EXTERN int zibclr(int id, int no, int ass);
EXTERN int zibwop(int *idAdr, int *noAdr);
EXTERN int zibwcl(int id, int no, int ass);
EXTERN int zibadd(int id, int no, int assid, int assno);
EXTERN int zibsub(int id, int no, int assid, int assno);
EXTERN int zibgin(int id, int no, int geo, void *x1koordAdr, void *y1koordAdr,
  void *x2koordAdr, void *y2koordAdr);
EXTERN int zibev(int id, int no, int *typAdr,  int *buttonAdr,
  void *xkoordAdr, void *ykoordAdr, int *chAdr);
EXTERN int zibwt(int id, int no, int *typAdr, int * buttonAdr,
  void *xkoordAdr, void *ykoordAdr, int *chAdr);
EXTERN int zibstr(int id, int no, char *string, int *length);

#endif
