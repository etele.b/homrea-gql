/* $Id: mgconst.h,v 1.1.1.1 1996/11/04 12:05:46 roitzsch Exp $ */

#ifndef __MINI_CONST__
#define __MINI_CONST__

#ifdef __cplusplus
#   define EXTERN extern "C"
#else
#   define EXTERN extern
#endif

#define ON   1
#define OFF  0
#define FLUSH 2

#define PENSIZE	   1
#define LINESTYLE  42
#define FONTSIZE   2
#define FONTPROP   41
#define MARKER	   3

#define MAXWD	   4
#define MAXCOL	   5
#define MAXGRY	   6
#define PREC	   7

#define PENCOL	   8
#define FONTCOL	   9
#define MARKCOL	  10
#define FILLCOL	  11
#define BACKGRCOL 39

#define XRESOL	 12
#define YRESOL	 13
#define TGXCM	 14
#define TGYCM	 15
#define USXTG	 16
#define USYTG	 17
#define XCHSZ	 18
#define YCHSZ    19

#define MINX	 20
#define MINY	 21
#define MAXX	 22
#define MAXY	 23
#define SLMINX	 24
#define SLMINY	 25
#define SLMAXX	 26
#define SLMAXY	 27
#define TOP	 28
#define LEFT	 29
#define BOTTOM	 30
#define RIGHT	 31
#define WDORGX	 32
#define WDORGY	 33
#define WDWDTH	 34
#define WDHGHT	 35
#define CAPTION  36
#define SCALFIT  37
#define FILENAME 38
#define BUFFER   40
#define COPYMODE 43

#define BLACK   0
#define WHITE   1
#define RED     2
#define GREEN   3
#define BLUE    4
#define CYAN    5
#define MAGENTA 6
#define YELLOW  7

#define SMALL	0
#define MEDIUM	1
#define BIG	2

#define DOTTED        0
#define LONG_DOTTED   1
#define DASHED        2
#define LONG_DASHED   3
#define DASHDOT       4
#define DASHDOTDOTTED 5
#define SOLID         6

#define STAR       1
#define CROSS	   2
#define PLUS	   3
#define BULLET	   4
#define CIRCCROSS  5
#define CIRCPLUS   6

#define LINE       1
#define RECTANGLE  2
#define CIRCLE     3

#define SINGLE  4
#define DOUBLE  8

#define TRANSPARENT  0
#define OPAQUE       1

#define GXOR  1
#define GCOPY 0

#define ALL	-1
#define PS       1
#define MAC      2
#define SUNVIEW  3
#define X11      4
#define SCREEN   5
#define PS_QUER  6
#define PS_TEX   7

#endif
