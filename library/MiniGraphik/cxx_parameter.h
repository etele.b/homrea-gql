/* $Id: cxx_parameter.h,v 1.1.1.1 1996/11/04 12:05:46 roitzsch Exp $ */

#ifndef CXX_PARAMETER_H
#define CXX_PARAMETER_H

#include <mgconst.h>

class Plot
{

  public:

    Plot (int type, float size=0.5, const char* caption=0);
    ~Plot();

    int line  (double* x, double* y, int n);
    int line  (float*  x, float*  y, int n);
    int marker(double* x, double* y, int n);
    int marker(float*  x, float*  y, int n);
    int fill  (double* x, double* y, int n);
    int fill  (float*  x, float*  y, int n);

    int text   (double x, double y, const char* text); 

    int set    (int type, int    val);
    int set    (int type, double val);
    int set    (int type, char*  str);

    int request(int type, int*    iVal);
    int request(int type, double* rVal);
    int request(int type, char**  cp);

    int clear  ();		
    void flush ();

    int gin    (int geo, void* x1koordAdr, 
	       void* y1koordAdr, void* x2koordAdr, void* y2koordAdr);
    int event  (int* typAdr,  int* buttonAdr, 
		void* xkoordAdr, void* ykoordAdr, int* chAdr);
    int wait   (int* typAdr,  int* buttonAdr, 
		void* xkoordAdr, void* ykoordAdr, int* chAdr);
    int color  (int col_no, int rVal, int gVal, int bVal);
    int string (char* stringAdr, int* lengthAdr);

  private:
    struct GRAPHIC* graph;
};

#endif
