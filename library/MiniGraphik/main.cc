#include <stdio.h>
#include "MGraph.h"

static void TwoTriangles(MGraph_Context &graphOut);

main()
  {
    MGraph_PSContext myPSOut("out.ps",PS);
    MGraph_ScreenContext myScreenOut("Fenster");

	myScreenOut.SetCC(&myPSOut);

    myPSOut.SetTextSize(SMALL);
    myScreenOut.SetTextSize(BIG);
    myScreenOut.SetColType(MARKCOL, WHITE);

	TwoTriangles(myScreenOut);
	myScreenOut.Clear();

	return 0;
  }

static void TwoTriangles(MGraph_Context &graphOut)
  {
    float xf[4], yf[4];
    double xd[4], yd[4];

    xf[0] = 0.2; yf[0] = 0.1;
    xf[1] = 0.9; yf[1] = 0.8;
    xf[2] = 0.2; yf[2] = 0.9;
    xf[3] = 0.2; yf[3] = 0.1;

    xd[0] = 0.1; yd[0] = 0.2;
    xd[1] = 0.8; yd[1] = 0.1;
    xd[2] = 0.8; yd[2] = 0.9;
    xd[3] = 0.1; yd[3] = 0.2;

    graphOut.Text(0.1, 0.1, "  1     ");
    graphOut.SetColType(FONTCOL, BLUE);
    graphOut.Text(0.1, 0.1, "  1     ");
    graphOut.SetColType(FONTCOL, YELLOW);
    graphOut.Text(0.1, 0.1, "  2     ");
    graphOut.PLine(xf, yf, 4);
    graphOut.PMarker(xf, yf, 4,BULLET);
    graphOut.PLine(xd, yd, 4);
    graphOut.PMarker(xd, yd, 4,STAR);

	return;
  }
