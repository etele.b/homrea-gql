/*
 $Id: mgcxxtest.cc,v 1.1.1.1 1996/11/04 12:05:46 roitzsch Exp $
 (C)opyright 1996 by Konrad-Zuse-Center, Berlin
 All rights reserved.
 Part of the common environment

$Log: mgcxxtest.cc,v $
Revision 1.1.1.1  1996/11/04 12:05:46  roitzsch
minigraph module

*/

#include <iostream.h>

#include <cxx_parameter.h>

int main()
  {
    int stop;
    cin >> stop;

    Plot fenster(SCREEN, 0.5, "ABC");
    
    double x[4]={0.1,0.9,0.9,0.1}, y[4]={0.1,0.1,0.9,0.9};
    fenster.line(x,y,4);
    fenster.flush();

    cin >> stop;

    return 0;
  }
