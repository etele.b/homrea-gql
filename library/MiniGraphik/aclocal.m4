dnl
dnl ------------------------------------------------------------------------
dnl
dnl PAC_PROGRAM_CHECK(VARIABLE, PROG-TO-CHECK-FOR, VALUE-IF-FOUND
dnl               [, VALUE-IF-NOT-FOUND [,FULL-PATH-IF-FOUND])
dnl
dnl The variable named by FULL-PATH-IF-FOUND will be set to the
dnl full path for the program
dnl
dnl A fault with the routine in autoconf is that if PROG-TO-CHECK-FOR
dnl already has a path, it will FAIL!
dnl
dnl An UNDOCUMENTED FEATURE is that if VARIABLE is already set, this
dnl routine DOES NOTHING!
dnl
define(PAC_PROGRAM_CHECK,
[# Extract the first word of "$2", so it can be a program name with args.
set dummy $2; ac_word=[$]2
AC_MSG_CHECKING([for $ac_word])
dnl AC_CACHE_VAL(ac_cv_prog_$1,[
ac_prog_where=""
if test -n "[$]$1"; then
  ac_cv_prog_$1="[$]$1" # Let the user override the test.
else
  ac_first_char=`expr "$2" : "\(.\)"`
  if test "$ac_first_char" = "/" -a -x "$2" ; then
       ac_cv_prog_$1="$3"
       ac_prog_where=$2
  else
      IFS="${IFS= 	}"; ac_save_ifs="$IFS"; IFS="${IFS}:"
      for ac_dir in $PATH; do
        test -z "$ac_dir" && ac_dir=.
        if test -f $ac_dir/$ac_word; then
          ac_cv_prog_$1="$3"
          ac_prog_where=$ac_dir/$ac_word
          break
        fi
      done
      IFS="$ac_save_ifs"
  fi
dnl If no 4th arg is given, leave the cache variable unset,
dnl so AC_CHECK_PROGS will keep looking.
ifelse([$4], , , [  test -z "[$]ac_cv_prog_$1" && ac_cv_prog_$1="$4"
])dnl
fi;dnl])dnl
$1="$ac_cv_prog_$1"
if test -n "$ac_prog_where" ; then
  AC_MSG_RESULT(found $ac_prog_where ([$]$1))
  ifelse([$5], , , [ $5=$ac_prog_where ] )
else
  AC_MSG_RESULT(no)
fi
AC_SUBST($1)dnl
])dnl
dnl
dnl ------------------------------------------------------------------------
dnl
dnl PAC_PROGRAMS_CHECK is like PAC_PROGRAM_CHECK, but with
dnl a list of programs.
dnl
define(PAC_PROGRAMS_CHECK,
[for p in $2
do
PAC_PROGRAM_CHECK($1, [$]p, [$]p, )
test -n "[$]$1" && break
done
ifelse([$3],,, [test -n "[$]$1" || $1="$3"
])])dnl
dnl
dnl ------------------------------------------------------------------------
dnl
dnl May also set F77GETARG (routine to get commandline arguments)
dnl Also sets HAS_F77 (makes sure that the chosen compiler is present!)
dnl
define(PAC_GET_F77,[
if test -z "$USERF77" ; then
case $1 in 
   intelnx|paragon|i860) F77=if77 ;;
   cm5) # TMC Told us this should be f77
        F77=f77 ; if test -z "$USERFLINKER" ; then
		      FLINKER="cmmd-ld -comp $F77"
		  fi ;;
   CRAY|unicos|ymp)
   # The Fortran compiler might be cf77 or f77
   # This wierd function uses the VALUE of the first argument before
   # the second!
   F77=	
   PAC_PROGRAMS_CHECK(F77,cf77 f77)
   ;;
   cray_t3d)        
# The dp switch on the following lines allows compilation of "double precision"
# at the cost of violating the Fortran standard
   echo Setting double precision to be the same as real, in violation 
   echo of the Fortran standard.  This is needed because Cray Fortran 
   echo for the T3D does not support double precision and hence is 
   echo not actually Fortran.
   F77="/mpp/bin/cf77"
   FFLAGS="$FFLAGS -Ccray-t3d -dp"
   F77GETARG="call pxfgetarg(i,s,len(s),ierr)"
   if test -z "$USERFLINKER" ; then
       FLINKER="$F77 -Ccray-t3d"
   fi

   ;;
   hpux) 
    # This may eliminate the need for +U77 ....
    if test "`which $F77`" != "/usr/convex/bin/fc" ; then 
        F77GETARG=["call igetarg(i,s,len(s))"]
	# mgates reports that the +T option makes MPICH work on some HPUX
	# platforms.  The documentation for +T is truely scary; it seems
	# to imply that without it, a Fortran program will NOT run correctly.
	FFLAGS="$FFLAGS +T"
    else
        # The Convex compiler needs to have optimization messages suppressed
        FFLAGS="$FFLAGS -or none"
    fi
    ;;
    convex_spp)  F77="/usr/convex/bin/fc" ;;
    ibmpoe)
         dnl This is intended for the Standard POE/MPL version
	 F77=mpxlf
    ;;
    meiko) 
      PAC_PROGRAMS_CHECK(FCval,f77 apf77 pgf77)
      if test -n "$FCval" -a "$FCval" = f77 ; then
	F77="f77 -g -cg92"
      else
        F77="$FCval"
      fi
      ;;
    ncube)   F77=nf77 ;;
    rs6000)  F77=xlf ;;
esac
fi
if test -z "$USERFLINKER" -a -z "$FLINKER" ; then
    FLINKER="$F77"
fi
#
# Check that the Fortran compiler is actually available:
HAS_F77=
if test -n "$F77" ; then
    PAC_PROGRAM_CHECK(HAS_F77,$F77,1,0)
fi
])dnl
dnl
dnl Fortran runtime for Fortran/C linking
dnl On suns, try
dnl FC_LIB          =/usr/local/lang/SC2.0.1/libM77.a \ 
dnl              /usr/local/lang/SC2.0.1/libF77.a -lm \
dnl              /usr/local/lang/SC2.0.1/libm.a \
dnl              /usr/local/lang/SC2.0.1/libansi.a
dnl
dnl AIX requires -bI:/usr/lpp/xlf/lib/lowsys.exp
dnl ------------------------------------------------------------------------
dnl
dnl Get the format of Fortran names.  Uses F77, FFLAGS, and sets WDEF.
dnl If the test fails, sets NOF77 to 1, HAS_FORTRAN to 0
dnl
define(PAC_GET_FORTNAMES,[
   # Check for strange behavior of Fortran.  For example, some FreeBSD
   # systems use f2c to implement f77, and the version of f2c that they 
   # use generates TWO (!!!) trailing underscores
   # Currently, WDEF is not used but could be...
   #
   # Eventually, we want to be able to override the choices here and
   # force a particular form.  This is particularly useful in systems
   # where a Fortran compiler option is used to force a particular
   # external name format (rs6000 xlf, for example).
   cat > confftest.f <<EOF
       subroutine mpir_init_fop( a )
       integer a
       a = 1
       return
       end
EOF
   $F77 $FFLAGS -c confftest.f > /dev/null 2>&1
   if test ! -s confftest.o ; then
        echo "Unable to test Fortran compiler"
        echo "(compiling a test program failed to produce an "
        echo "object file)."
	NOF77=1
        HAS_FORTRAN=0
   elif test -n "$FORTRANNAMES" ; then
	WDEF="-D$FORTRANNAMES"
   else
    # We have to be careful here, since the name may occur in several
    # forms.  We try to handle this by testing for several forms
    # directly.
    if test $arch_CRAY ; then
     # Cray doesn't accept -a ...
     nameform1=`strings confftest.o | grep mpir_init_fop_  | head -1`
     nameform2=`strings confftest.o | grep MPIR_INIT_FOP   | head -1`
     nameform3=`strings confftest.o | grep mpir_init_fop   | head -1`
     nameform4=`strings confftest.o | grep mpir_init_fop__ | head -1`
    else
     nameform1=`strings -a confftest.o | grep mpir_init_fop_  | head -1`
     nameform2=`strings -a confftest.o | grep MPIR_INIT_FOP   | head -1`
     nameform3=`strings -a confftest.o | grep mpir_init_fop   | head -1`
     nameform4=`strings -a confftest.o | grep mpir_init_fop__ | head -1`
    fi
    /bin/rm -f confftest.f confftest.o
    if test -n "$nameform4" ; then
	echo "Fortran externals are lower case and have 1 or 2 trailing underscores"
	WDEF=-DFORTRANDOUBLEUNDERSCORE
	AC_DEFINE(FORTRANUNDERSCORE)
    elif test -n "$nameform1" ; then
        # We don't set this in CFLAGS; it is a default case
        echo "Fortran externals have a trailing underscore and are lowercase"
	      AC_DEFINE(FORTRANUNDERSCORE)
	WDEF=-DFORTRANUNDERSCORE
    elif test -n "$nameform2" ; then
	echo "Fortran externals are uppercase"
	AC_DEFINE(FORTRANUPPERCASE)    
	WDEF=-DFORTRANCAPS 
    elif test -n "$nameform3" ; then
	echo "Fortran externals are lower case"
	WDEF=-DFORTRANNOUNDERSCORE 
	AC_DEFINE(FORTRANNOUNDERSCORE)
    else
	echo "Unable to determine the form of Fortran external names"
	echo "Make sure that the compiler $F77 can be run on this system"
#	echo "If you have problems linking, try using the -nof77 option"
#        echo "to configure and rebuild MPICH."
	echo "Turning off Fortran (-nof77 being assumed)."
	NOF77=1
        HAS_FORTRAN=0
    fi
#   case $nameform in 
#       MPIR_INIT_FOP | _MPIR_INIT_FOP)
#	echo "Fortran externals are uppercase"     
#	WDEF=-DFORTRANCAPS 
#	;;
#       mpir_init_fop_ | _mpir_init_fop_)   
#	 # We don't set this in CFLAGS; it is a default case
#        echo "Fortran externals have a trailing underscore and are lowercase"
#	WDEF=-DFORTRANUNDERSCORE ;;
#
#       mpir_init_fop | _mpir_init_fop)     
#	echo "Fortran externals are lower case"
#	WDEF=-DFORTRANNOUNDERSCORE 
#	;;
#
#           # Fortran no underscore is the "default" case for the wrappers; 
#	   # having this defined allows us to have an explicit test, 
#	   # particularly since the most common UNIX case is FORTRANUNDERSCORE
#       mpir_init_fop__ | _mpir_init_fop__)  
#	echo "Fortran externals are lower case and have 1 or 2 trailing underscores"
#	WDEF=-DFORTRANDOUBLEUNDERSCORE
#        ;;
#
#       *)
#	echo "Unable to determine the form of Fortran external names"
#	echo "If you have problems linking, try using the -nof77 option"
#        echo "to configure and rebuild MPICH."
#	NOF77=1
#        HAS_FORTRAN=0
#	;;
#   esac
    fi])dnl
dnl
dnl ------------------------------------------------------------------------
dnl PAC_FIND_FCLIB( [carch] )
dnl
dnl Find the libraries needed to link Fortran routines with C main programs
dnl This is ONLY an approximation but DOES handle some simple cases.
dnl Sets FCLIB if it can.  Fortran compiler FULL PATH would help.
dnl
define(PAC_FIND_FCLIB,[
if test -n "$F77" ; then
PAC_PROGRAM_CHECK(FCVal,$F77,,,FCFULLPATH)
AC_MSG_CHECKING([for Fortran libraries to link C programs with])
case $1 in 
    sun4)
    if test "$FCFULLPATH" = /usr/lang/f77 ; then
	# Look for /usr/lang/SC... .   This is tricky, because 
	# we want to find the LATEST versions first
	for dir in /usr/lang/SC2*.*.* /usr/lang/SC2.* /usr/lang/SC2* \
	 	 /usr/lang/SC1.*.* /usr/lang/SC1.* /usr/lang/SC1* ; do
	    if test -d $dir ; then
		if test -s $dir/libF77.a ; then
		    FCLIB="$dir/libF77.a -lm"
		    if test -s $dir/libm.a ; then
			FCLIB="$FCLIB $dir/libm.a"
		    fi
	            break
	        fi
            fi
        done
    fi
    ;;
    solaris)
	# /opt/SUNWspro/SC*/lib/name
	for file in libF77.a libM77.a libsunmath.a ; do
	    for dir in /opt/SUNWspro/SC4.* /opt/SUNWspro/SC4* \
	         /opt/SUNWspro/SC3.* /opt/SUNWspro/SC3* \
		       /opt/SUNWspro/SC2.* /opt/SUNWspro/SC2* ; do
  	        if test -d $dir ; then
		    if test -s $dir/lib/$file ; then
			FCLIB="$FCLIB $dir/lib/$file"
			break
		    fi
                fi
            done
        done
    ;;

    rs6000|aix)
	for file in /usr/lib/libxlf.a /usr/lib/libxlf90.a \
	            /usr/lib/libxlfutil.a ; do
	    if test -s $file ; then
		FCLIB="$FCLIB $file"
            fi
	done
	if test -s /usr/lpp/xlf/lib/lowsys.exp ; then
	    FCLIB="$FCLIB /usr/lpp/xlf/lib/lowsys.exp"
	fi
	;;
    IRIX64|IRIX|irix)
  FCLIB="-lF77 -lU77 -lI77"
    ;;
    alpha)
	for file in libfor.a libutil.a libFutil.a libots.a ; do
	    if test -s /usr/lib/$file ; then
		FCLIB="$FCLIB /usr/lib/$file"
            fi
	done
    ;;
    freebsd|linux)
	AC_MSG_RESULT()
        SaveDEFS="$DEFS"
        SaveLIBS="$LIBS"
	    AC_CHECK_LIB(f2c,c_cos,FCLIB="$FCLIB -lf2c")
	    AC_CHECK_LIB(m,sin,FCLIB="$FCLIB -lm")
         DEFS="$SaveDEFS"
	LIBS="$SaveLIBS"
    ;;
    hpux)
	for file in libf.a libf.sl ; do
	    if test -s /usr/lib/$file ; then
		FCLIB="$FCLIB /usr/lib/$file"
            fi
	done
    ;;
    *)
    :
    ;;
esac
if test -n "$FCLIB" ; then
    AC_MSG_RESULT(found $FCLIB)
else
    AC_MSG_RESULT(none)
fi
fi
])dnl
