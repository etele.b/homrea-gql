      SUBROUTINE MG2DPL(NUCALL,NOPL,
     1          SIADD,SINTER,SSCALE,SPOST,NZHD,HEADER,
     1          NO,NF,XA,LDX,IOX,SYMBX,YA,LDY,IOY,SYMBY,NCONS,KONS,
     1          XMIN,XMAX,YMIN,YMAX,LIW,IW,LRW,RW)
C***********************************************************************
C 
C  Arguments
C  =========
C
C  NUCALL  (input) INTEGER
C
C          task
C          NUCALL = 0   initialize windows
C          NUCALL > 0   take old windows     
C
C  SIADD   (input) CHARACTER*1 
C          SIADD  = 'A' add new curves to the old ones
C                       replot otherwise
C
C  SINTER  (input) CHARACTER*1 
C          SINTER = 'I' use interactive version with mouse control
C                       use batch version otherwise
C
C  SSCALE  (input) CHARACTER*1 
C          SSCALE = 'S' do internal scaling of the plots
C
C  SPOST   (input) CHARACTER*1 
C          SPOST  = 'P' output on Postscript-file
C
C  HEADER  (input) CHARACTER*(*) 
C          Header for the legend
C
C  NO      (input) INTEGER
C          The number of points to be plotted
C
C  XA      (input) DOUBLE PRECISION array, dimension (LDX,*)
C          The X-coordinates of the points    
C
C  LDX     (input) INTEGER
C          leading dimension of array XA      
C
C  IOX     (input) INTEGER
C          if IOX = 1 then the loop over the points is assumed to 
C          be over the first index of the array
C          if IOX = 2 then the loop over the points is assumed to 
C          be over the second index of the array
C
C  SYMBX   (input) CHARACTER*(*)   
C          The symbol of the X-coordinate     
C
C  YA      (input) DOUBLE PRECISION array, dimension (LDY,*)
C          The Y-coordinates of the points    
C
C  LDY     (input) INTEGER
C          leading dimension of array YA      
C
C  IOY     (input) INTEGER
C          if IOY = 1 then the loop over the points is assumed to 
C          be over the first index of the array
C          if IOY = 2 then the loop over the points is assumed to 
C          be over the second index of the array
C
C  SYMBY   (input) CHARACTER*(*) array, dimension (LDY)
C          The symbols of the Y-coordinates    
C
C  NCONS   (input) INTEGER
C          The number of Y-coordinates to be plotted
C
C  KONS    (input) INTEGER array, dimension (NCONS)
C          The indices of the Y-coordinates to be plotted 
C          If there is a KONS, which is < 0, a two-Y plot will be 
C          generated with all KONS > 0 on the left axis and
C          all KONS < 0 on the right axis
C
C  XMIN    (input/output) DOUBLE PRECISION
C          Minimum value of the X-coordinates
C          If XMIN=XMAX on input, a scaling will be done and the value
C          of XMIN will be returned
C
C  XMAX    (input/output) DOUBLE PRECISION
C          Maximum value of the X-coordinates
C          If XMIN=XMAX on input, a scaling will be done and the value
C          of XMAX will be returned
C
C  YMIN    (input/output) DOUBLE PRECISION array, dimension (*)
C          Minimum value of the Y-coordinates
C          If YMIN(I)=YMAX(I) on input, and I is the index of a 
C          species considered for plotting (see KONS), a scaling will 
C          be done and the value of YMIN(I) will be corrected
C
C  YMAX    (input/output) DOUBLE PRECISION array, dimension (*)
C          Maximum value of the Y-coordinates
C          If YMIN(I)=YMAX(I) on input, and I is the index of a 
C          species considered for plotting (see KONS), a scaling will 
C          be done and the value of YMAX(I) will be corrected
C
C  IW      (workspace/output) INTEGER array, dimension (LIW)
C
C  RW      (workspace/output) REAL array, dimension (LRW)
C
C     
C***********************************************************************
      INCLUDE 'parameter.h'
      INTRINSIC MAX,MIN
      DOUBLE PRECISION XMIN,XMAX,YMIN,YMAX
      DOUBLE PRECISION XA(LDX,*),YA(LDY,*)
      DOUBLE PRECISION XLL,YLU
      CHARACTER(LEN=*) SYMBY(*)
      CHARACTER(LEN=*)  SYMBX
      CHARACTER*(NZHD) HEADER
      CHARACTER*20 HTEMP 
      CHARACTER*1 SIADD,SSCALE,SINTER,SPOST
      DIMENSION RW(LRW),IW(LIW)
      COMMON/BICOL/IFGCOL,IBGCOL
C***********************************************************************
C     Local variables
C***********************************************************************
      CHARACTER*8 TENEXP
      PARAMETER (XPMI = 0.12,XPMA = 0.6,YPMI=0.1,YPMA=0.9,TOFF=0.05,
     1           XLINO = 0.02) 
      PARAMETER (XLARGE = 1.E30,XSMALL = 1.E-30)
      DIMENSION YMIN(*),YMAX(*)
      DIMENSION KONS(NCONS)  
      INTEGER TYPE,BUTTON,CH
      LOGICAL INTER,ICREA,ISCAL,EQUSYM,IADDP,POSTSC,TWOPLO
      DIMENSION MIND(6)
      DIMENSION XBOX(5),YBOX(5)
      DIMENSION XML(2),YML(2) 
      PARAMETER (ONE=1.0E0,ZERO=0.0E0)
      DIMENSION TENEXP(0:5)
      SAVE
C***********************************************************************
C     DATA statements 
C***********************************************************************
      DATA MIND/4,3,1,2,5,6/
      DATA TENEXP/
     1    '        ',
     1    '*10     ','*100    ','*1000   ','*10000  ','*100000 '/,
     1     MITEN/0/,MATEN/5/
C***********************************************************************
C     
C***********************************************************************
      IF(IOX.NE.1.AND.IOX.NE.2) THEN
         WRITE(6,*) '  wrong IOX in MG2DPL'
         STOP
      ENDIF
      IF(IOY.NE.1.AND.IOY.NE.2) THEN
         WRITE(6,*) '  wrong IOY in MG2DPL'
         STOP
      ENDIF
C***********************************************************************
C     Initialize
C***********************************************************************
                                 ISCAL = .FALSE. 
      IF(EQUSYM(1,SSCALE,'S'))   ISCAL = .TRUE.
                                 INTER = .FALSE. 
      IF(EQUSYM(1,SINTER,'I'))   INTER = .TRUE.
                                 IADDP = .FALSE. 
      IF(EQUSYM(1,SIADD,'A'))    IADDP = .TRUE.
                                 POSTSC= .FALSE. 
      IF(EQUSYM(1,SPOST,'P'))    POSTSC= .TRUE.
      ICREA = .FALSE.
C***********************************************************************
C
C     Define and open window 
C
C***********************************************************************
      IASS = 1
C***********************************************************************
C     define kind and size of window 
C***********************************************************************
C     POSTSC = .TRUE.
      IF(NUCALL.EQ.0) THEN
        IF(.NOT.POSTSC) THEN
          IDSCR = SCREEN 
C         IDSCR = X11    
          IWORX = 500
          IWORY = 830
          IWWDT = 600
          IWHGT = 370
          CALL ZIBWOP(IDSCR, NOPL)
          CALL ZIBSET(IDSCR, NOPL, IASS, CAPTION,'Output Window')
        ELSE
          IDSCR = PS_TEX 
          IWORX =  20
          IWORY =  20
          IWWDT = 600
          IWHGT = 370
          CALL ZIBWOP(IDSCR, NOPL)
          CALL ZIBSET(IDSCR, NOPL, ASS, FILENAME, 'Postscript_9')
        ENDIF
        CALL ZIBSET(IDSCR, NOPL, IASS, WDORGX, IWORX)
        CALL ZIBSET(IDSCR, NOPL, IASS, WDORGY, IWORY)
        CALL ZIBSET(IDSCR, NOPL, IASS, WDWDTH, IWWDT)
        CALL ZIBSET(IDSCR, NOPL, IASS, WDHGHT, IWHGT)
        CALL ZIBSET(IDSCR, NOPL, IASS, MINX, 0.00)
        CALL ZIBSET(IDSCR, NOPL, IASS, MINY, 0.00)
        CALL ZIBSET(IDSCR, NOPL, IASS, MAXX, 1.00)
        CALL ZIBSET(IDSCR, NOPL, IASS, MAXY, 1.00)
      ENDIF
C***********************************************************************
C     define foreground and background colors
C***********************************************************************
      IMCOL1 = 62
      IMCOL2 = 63
      CALL ZIBCOL(IDSCR, IMCOL1, 255,222,173)
      CALL ZIBCOL(IDSCR, IMCOL2, 70, 0, 0)
      IF(POSTSC) THEN
        IFGCOL = WHITE 
        IBGCOL = IMCOL2
      ELSE
        IFGCOL = BLACK
        IBGCOL = WHITE  
      ENDIF
      CALL ZIBSET(IDSCR, NOPL, IASS, BACKGRCOL, IBGCOL)
      CALL ZIBSET(IDSCR, NOPL, IASS, SCALFIT, 0)
C***********************************************************************
C     Check Storage
C***********************************************************************
      IF(2*NO.GT.LRW) THEN 
        WRITE(6,*) ' LRW too small'
        STOP
      ENDIF
C***********************************************************************
C     Get Extreme Values of the X-coordinate
C***********************************************************************
      IF(NUCALL.GT.0.AND.IADDP) THEN
C---- do not scale for successive call with IADDP = True
      ELSE
        XMI = XMIN
        XMA = XMAX
        IF(ABS(XMI-XMA).LT.XSMALL) THEN
          XMI =  XLARGE
          XMA = -XLARGE
          DO 105 L=1,NO
            IF(IOX.EQ.1) THEN
              XMI  = MIN(XMI,SNGL(XA(L,1)))
              XMA  = MAX(XMA,SNGL(XA(L,1)))
            ELSE
              XMI  = MIN(XMI,SNGL(XA(1,L)))
              XMA  = MAX(XMA,SNGL(XA(1,L)))
            ENDIF
  105     CONTINUE
          XMIN = XMI
          XMAN = XMA
        ENDIF
      ENDIF
C***********************************************************************
C     Scale X-coordinate 
C***********************************************************************
      DO 110 L=1,NO
      IF(IOX.EQ.1) THEN
      RW(L)    = (XA(L,1) - XMI)/(XMA-XMI) 
      ELSE
      RW(L)    = (XA(1,L) - XMI)/(XMA-XMI) 
      ENDIF
  110 CONTINUE
      DO 120 L=1,NO
      RW(L)    = RW(L)    * (XPMA-XPMI) + XPMI 
  120 CONTINUE
C***********************************************************************
C     Check if this has to be an Y1-Y2 plot 
C***********************************************************************
      TWOPLO = .FALSE.
      DO 118 I=1,NCONS
        IF(KONS(I).LT.0) TWOPLO = .TRUE.
  118 CONTINUE
C***********************************************************************
C     Get Extreme Values of the Y-coordinate
C***********************************************************************
      IF(NUCALL.GT.0.AND.IADDP) THEN
C---- do not scale for successive call with IADDP = True
      ELSE
        YMI1 =  XLARGE
        YMA1 = -XLARGE
        YMI2 =  XLARGE
        YMA2 = -XLARGE
        DO 155 I=1,NCONS
        III = ABS(KONS(I))
        IF(ABS(YMIN(III)-YMAX(III)).LT.XSMALL) THEN
          YMIN(III) =  XLARGE
          YMAX(III) = -XLARGE
          DO 150 L=1,NO
            IF(IOY.EQ.1) THEN
            YMIN(III)  = MIN(YMIN(III),YA(L,III))
            YMAX(III)  = MAX(YMAX(III),YA(L,III))
            ELSE
              YMIN(III)  = MIN(YMIN(III),YA(III,L))
              YMAX(III)  = MAX(YMAX(III),YA(III,L))
            ENDIF
  150     CONTINUE
        ENDIF
          IF(KONS(I).GT.0) THEN
            YMI1 = MIN(YMI1,SNGL(YMIN(III)))
            YMA1 = MAX(YMA1,SNGL(YMAX(III)))
          ELSE
            YMI2 = MIN(YMI2,SNGL(YMIN(III)))
            YMA2 = MAX(YMA2,SNGL(YMAX(III)))
          ENDIF
  155   CONTINUE
      ENDIF
C***********************************************************************
C     Plot
C***********************************************************************
  200 CONTINUE
C***********************************************************************
C     Plot Axis 
C***********************************************************************
      IF(NUCALL.GT.0.AND.IADDP) THEN
C---- do not Plot axis for successive call with IADDP = True
      ELSE
      XLEN = XPMA - XPMI
      CALL PLTAX('X ',IDSCR,NOPL,IASS,XPMI,YPMI,XLEN,5,XMI,XMA,SYMBX)
      XLEN = YPMA - YPMI
      CALL PLTAX('YL',IDSCR,NOPL,IASS,XPMI,YPMI,XLEN,5,YMI1,YMA1,'y1')
      IF(TWOPLO) 
     1  CALL PLTAX('YR',IDSCR,NOPL,IASS,XPMA,YPMI,XLEN,5,YMI2,YMA2,'y2')
      ENDIF
      IF(IADDP.OR.NUCALL.EQ.0) THEN
      CALL ZIBSET(IDSCR,NOPL, IASS, FILLCOL, IBGCOL)
      XLL = XPMA+TOFF+0.09
      XLR = 0.95
      YLU = YPMA
      YLO = 1.0
      XBOX(1) = XLL
      YBOX(1) = YLU
      XBOX(2) = XLR
      YBOX(2) = YLU
      XBOX(3) = XLR
      YBOX(3) = YLO
      XBOX(4) = XLL
      YBOX(4) = YLO
      XBOX(5) = XLL
      YBOX(5) = YLU
      HTEMP = HEADER
      CALL ZIBFL(IDSCR, NOPL, IASS, XBOX, YBOX, 5)
      CALL ZIBSET(IDSCR,NOPL, IASS, FONTCOL, IFGCOL)
C     CALL ZIBTX(IDSCR, NOPL, IASS, XLL, YLU,HEADER(1:NZHD))
C     CALL ZIBTX(IDSCR, NOPL, IASS, XLL, YLU,HTEMP)
      ENDIF
C***********************************************************************
C
C     Loop for plotting the curves 
C
C***********************************************************************
C***********************************************************************
C     Scale Y-coordinate 
C***********************************************************************
      DO 290 I=1,NCONS
      III = ABS(KONS(I))
      IF(KONS(I).GT.0) THEN
        YMI = YMI1
        YMA = YMA1
      ELSE
        YMI = YMI2
        YMA = YMA2
      ENDIF
C***********************************************************************
C     Compute Scaling Factors
C***********************************************************************
      YSCA = ONE 
      ICSCA = 0  
      IF(ISCAL) THEN
      DO 175 J=MITEN,MATEN
      SCAF      = 10.0**J
      IF(SCAF*YMIN(III).GE.YMI.AND.SCAF*YMAX(III).LE.YMA) THEN
        YSCA    =  SCAF   
        ICSCA   = J
      ENDIF   
  175 CONTINUE
      ENDIF
      DO 230 L=1,NO
      IF(IOY.EQ.1) THEN
      RW(L+NO) = YSCA * (YA(L,III) - YMI)/(YMA-YMI)
      ELSE
      RW(L+NO) = YSCA * (YA(III,L) - YMI)/(YMA-YMI)
      ENDIF
  230 CONTINUE
      DO 240 L=1,NO
      RW(L+NO) = RW(L+NO) * (YPMA-YPMI) + YPMI 
  240 CONTINUE
C***********************************************************************
C     Plot Legend
C***********************************************************************
C     write(6,*) ' scale',ysca,icsca
C     GOTO 290
      XST = 0.06
      XOT = XPMA + TOFF +0.09
      YOT = YPMA - I*XST 
      DAD = 0.03
      NUMCOL = 4
C---- Get number for colors and markers
      INDMAR = MIND((I-1) / NUMCOL + 1)
      INDCOL = 1+ I - ((I-1) / NUMCOL)*NUMCOL
      CALL ZIBSET(IDSCR, NOPL, IASS, FONTCOL, INDCOL)
      CALL ZIBSET(IDSCR, NOPL, IASS, PENCOL, INDCOL)
      CALL ZIBTX(IDSCR, NOPL, IASS, XOT+DAD, YOT, SYMBY(III))
C---- was 0.05
      CALL ZIBTX(IDSCR, NOPL, IASS, XOT+DAD+0.12, YOT, TENEXP(ICSCA))
      IF(TWOPLO) THEN
C---- was 0.08 
      IF(KONS(I).GT.0) THEN
        CALL ZIBTX(IDSCR, NOPL, IASS, XOT+DAD+0.20, YOT, 'y1')
      ELSE
        CALL ZIBTX(IDSCR, NOPL, IASS, XOT+DAD+0.20, YOT, 'y2')
      ENDIF
      ENDIF
      CALL ZIBSET(IDSCR, NOPL, IASS, PENSIZE, SMALL)
      CALL ZIBSET(IDSCR, NOPL, IASS, LINESTYLE, SOLID)
      CALL ZIBSET(IDSCR, NOPL, IASS, MARKER, INDMAR)
      CALL ZIBSET(IDSCR, NOPL, IASS, MARKCOL, INDCOL)
      CALL ZIBPM(IDSCR, NOPL, IASS, XOT, YOT, 1)
      XLML = 0.015
      XML(1) = XOT-XLML
      XML(2) = XOT+XLML
      YML(1) = YOT
      YML(2) = YOT
      CALL ZIBPL(IDSCR, NOPL, IASS, XML, YML, 2)
      CALL ZIBPL(IDSCR, NOPL, IASS, RW(1), RW(NO+1), NO)
      CALL ZIBPM(IDSCR, NOPL, IASS, RW(1), RW(NO+1), NO)
      CALL ZIBPL(IDSCR, NOPL, IASS, RW(1), RW(NO+1), NO)
C 290 CONTINUE
  290 CONTINUE
C***********************************************************************
C
C     Allow zooming in interactive mode
C
C***********************************************************************
      IF(INTER) THEN
C     write(6,*) 'type,button,x1,y1,ch',type,button,x1,y1,ch
      CALL ZIBSET(IDSCR,NOPL, IASS, FONTCOL, IFGCOL)
      CALL ZIBTX(IDSCR, NOPL, IASS, 0.95, 0.965, 'End ')
      CALL ZIBTX(IDSCR, NOPL, IASS, 0.95, 0.935, 'Quit')
      CALL ZIBWT(IDSCR,NOPL,TYPE,BUTTON,X1,Y1,CH)
      IF(X1.GT.0.95.AND.Y1.GT.0.965) GOTO 995
      IF(X1.GT.0.95.AND.Y1.GT.0.935) GOTO 996
C     write(6,*) 'type,button,x1,y1,ch',type,button,x1,y1,ch
      IF(TYPE.LT.8) GOTO 999
      CALL ZIBGIN(IDSCR,NOPL,RECTANGLE,X1,Y1,X2,Y2)
      IF(X1.GT.X2) THEN
       Z = X1
       X1 = X2 
       X2 = Z
      ENDIF
      IF(Y1.GT.Y2) THEN
       Z = Y1
       Y1 = Y2
       Y2 = Z
      ENDIF
      XBOX(1) = X1
      XBOX(2) = X2
      XBOX(3) = X2
      XBOX(4) = X1
      XBOX(5) = X1
      YBOX(1) = Y1
      YBOX(2) = Y1
      YBOX(3) = Y2
      YBOX(4) = Y2
      YBOX(5) = Y1
      IF(.NOT.ICREA.AND.NUCALL.EQ.0) THEN 
        CALL ZIBWOP(IDSCR,NONEW)
        ICREA = .TRUE.
      ELSE
        CALL ZIBCLR(IDSCR,NONEW,IASS)
      ENDIF
      CALL ZIBSET(IDSCR,NONEW,IASS,CAPTION,'Zoom Window')
      CALL ZIBSET(IDSCR,NONEW,IASS,MINX,X1)
      CALL ZIBSET(IDSCR,NONEW,IASS,MAXX,X2)
      CALL ZIBSET(IDSCR,NONEW,IASS,MINY,Y1)
      CALL ZIBSET(IDSCR,NONEW,IASS,MAXY,Y2)
      CALL ZIBSET(IDSCR,NONEW,IASS,SCALFIT,0)
      CALL ZIBADD(IDSCR,NOPL ,IDSCR, NONEW)
      CALL ZIBSET(IDSCR,NOPL, IASS, BACKGRCOL, IBGCOL)
      CALL ZIBSET(IDSCR,NOPL, IASS, PENCOL, IFGCOL)
      CALL ZIBPL(IDSCR,NOPL,IASS,XBOX,YBOX,5)    
      GOTO 200
      ENDIF 
      IF(.NOT.IADDP) THEN 
        CALL ZIBCLR(IDSCR,NOPL,IASS)
        CALL ZIBWCL(IDSCR,NOPL,IASS)
      ENDIF
  995 CONTINUE
      IF(.NOT.IADDP) THEN 
        CALL ZIBCLR(IDSCR,NOPL,IASS)
        CALL ZIBWCL(IDSCR,NOPL,IASS)
        NUCALL = -1
      ENDIF
      GOTO 999
  996 CONTINUE
      IF(.NOT.IADDP) THEN 
        NUCALL = -1
      ENDIF
      GOTO 999
  999 CONTINUE
C
   	  RETURN
	  END
c         SUBROUTINE TPM(IDSCR, NO1, IASS, X, Y, MARK, S, ICOL)
c         INCLUDE 'parameter.h'
c         CHARACTER S*(*)
C
c         CALL ZIBTX(IDSCR, NO1, IASS, X, Y, S)
c         CALL ZIBSET(IDSCR, NO1, IASS, PENSIZE, SMALL)
c         CALL ZIBSET(IDSCR, NO1, IASS, MARKER, MARK)
c         CALL ZIBSET(IDSCR, NO1, IASS, MARKCOL, ICOL)
c
c         CALL TPMRK(IDSCR, NO1, IASS, X+0.2, Y)
c         CALL TCMRK(IDSCR, NO1, IASS, X+0.3, Y)
c         RETURN
      SUBROUTINE PLTAX(KIND,IDSCR,NOPL,IASS,XO,YO,XL,NTI,XMI,XMA,SYMB)
      INCLUDE 'parameter.h'
      COMMON/BICOL/IFGCOL,IBGCOL
      LOGICAL EQUSYM
      CHARACTER SYMB*(*)
      CHARACTER*20 TICK 
      CHARACTER*2 KIND
      CHARACTER*1 SIGDUM
      DIMENSION XP(2),YP(2)
      DATA XLTI/0.02/
C***********************************************************************
C     Plot Axis 
C***********************************************************************
      CALL ZIBSET(IDSCR,NOPL, IASS, PENCOL, IFGCOL)
      CALL ZIBSET(IDSCR,NOPL, IASS, FONTCOL, IFGCOL)
C***********************************************************************
C
C     Plot X-Axis 
C
C***********************************************************************
      IF(EQUSYM(1,KIND,'X')) THEN
C***********************************************************************
C     Plot Single Axis
C***********************************************************************
      XP(1) = XO
      YP(1) = YO
      XP(2) = XO + XL
      YP(2) = YO 
      CALL ZIBPL(IDSCR, NOPL, IASS, XP, YP, 2)
C***********************************************************************
C     Plot Tickmarks and Labels
C***********************************************************************
      XNTI = (NTI-1)
      DO 20 I=1,NTI
      XOT = XO + (XL * FLOAT(I-1))/XNTI
      XLAB= XMI + FLOAT(I-1)* (XMA-XMI)/XNTI
      XP(1) = XOT
      YP(1) = YO 
      XP(2) = XOT    
      YP(2) = YO  + XLTI
      CALL ZIBPL(IDSCR, NOPL, IASS, XP, YP, 2)
      CALL NUMANA(XLAB,XDUM,IEDUM,SIGDUM,TICK,LCH)
      YOT = YO - 0.04
      CALL ZIBTX(IDSCR, NOPL, IASS, XOT-0.05, YOT,TICK(1:LCH))
   20 CONTINUE
      XOT = XO + XL
C--- was 0.08
      YOT = YO - 0.08
      CALL ZIBTX(IDSCR, NOPL, IASS, XOT, YOT,SYMB)
C***********************************************************************
C
C     Plot Y-Axis 
C
C***********************************************************************
      ELSE
C***********************************************************************
C     Plot Single Axis
C***********************************************************************
        XP(1) = XO
        YP(1) = YO
        XP(2) = XO 
        YP(2) = YO + XL
      CALL ZIBPL(IDSCR, NOPL, IASS, XP, YP, 2)
C***********************************************************************
C     Plot Tickmarks and Labels
C***********************************************************************
      XNTI = (NTI-1)
      DO 70 I=1,NTI
      YOT = YO + (XL * FLOAT(I-1))/XNTI
      XLAB= XMI + FLOAT(I-1)* (XMA-XMI)/XNTI
      XP(1) = XO 
      YP(1) = YOT
      IF(EQUSYM(2,KIND,'YL')) THEN
        XP(2) = XO + XLTI    
      ELSE 
        XP(2) = XO - XLTI    
      ENDIF
      YP(2) = YOT        
      CALL ZIBPL(IDSCR, NOPL, IASS, XP, YP, 2)
      CALL NUMANA(XLAB,XDUM,IEDUM,SIGDUM,TICK,LCH)
      XOT = XO - 0.1  
      IF(EQUSYM(2,KIND,'YL')) THEN
        XOT = XO - 0.11    
      ELSE 
        XOT = XO + 0.005    
      ENDIF
      YOTL= YOT - 0.01
      CALL ZIBTX(IDSCR, NOPL, IASS, XOT, YOTL,TICK(1:LCH))
   70 CONTINUE
      XOT = XO 
      YOT = YO + XL + 0.03
      CALL ZIBTX(IDSCR, NOPL, IASS, XOT, YOT,SYMB)
      ENDIF
      RETURN
      END
