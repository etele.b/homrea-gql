/*
 $Id: mginter.c,v 1.3 1997/06/25 08:55:38 roitzsch Exp $
 (C)opyright 1996 by Konrad-Zuse-Center, Berlin
 All rights reserved.
 Part of the common environment

$Log: mginter.c,v $
Revision 1.3  1997/06/25 08:55:38  roitzsch
Last correction was not O.K.! (copying text)

Revision 1.2  1996/12/03 16:25:54  roitzsch
zibtxt could not plot readonly strings (now we copy the string always)

Revision 1.1.1.1  1996/11/04 12:05:46  roitzsch
minigraph module

*/

#include <stdio.h>
#include <string.h>

#include "minigraph.h"

#define MAXTEXT 1000
#define MAX_ASS 16

GRAPHIC *firstGraph = nil;

static int CopyTrim(char *ftnIn,int lng,char *buf,int bufLng);

int F77NAME(zibpl,ZIBPL)(int *idAdr, int *noAdr, int *assAdr, void *xAdr, void *yAdr, int *nAdr)
  {
    int  id = *idAdr, no = *noAdr, ass = *assAdr, n = *nAdr, i, fail = true,
	 succ = true;
    GRAPHIC *graph = firstGraph;

    if ((n < 0) || (n > MAX_POINTS))
      {
         fprintf(miniErrorFile,"MiniGraphic - zibpl :  n = %d not allowed as array length !\n",
		n);
         return false;
      }
    
    while (graph != nil)
      {
         if (((graph->id) == id) || (id == -1))
           if (((graph->wdNo) == no) || (no == -1))
             {
                if (graph->newScal)
		  if (!ComputeScaling(graph))
		    return false;
                  if (!(graph->PLine)(graph, xAdr, yAdr, n))
		    succ = false;
                fail = false;
                if (ass)
                  for (i = 0; i < 16; i++)
                    if (graph->Ass[i] != nil)
                      if (!F77NAME(zibpl,ZIBPL)(&(graph->Ass[i]->id), &(graph->Ass[i]->wdNo), 
				    assAdr, xAdr, yAdr, nAdr))
			succ = false;
             }
         graph = graph->next;  
      }

    if (fail)
      {
        fprintf(miniErrorFile,"MiniGraphic - zibpl :  no such window  id = %d  no = %d !\n", 
	       id, no);
	return false;
      }
    return succ;
  }





int F77NAME(zibpm,ZIBPM)(int *idAdr, int *noAdr, int *assAdr, void *xAdr, void *yAdr, int *nAdr)
  {
    int  id = *idAdr, no = *noAdr, ass = *assAdr, n = *nAdr, i, fail = true,
	 succ = true;
    GRAPHIC *graph = firstGraph;

    if ((n < 0) || (n > MAX_POINTS))
      {
         fprintf(miniErrorFile,"MiniGraphic - zibpm :  n = %d not allowed as array length !\n",
		n);
         return false;
      }

    while (graph != nil)
      {
        if (((graph->id) == id) || (id == -1))
          if (((graph->wdNo) == no) || (no == -1))
            {
               if (graph->newScal)
                 if (!ComputeScaling(graph))
		   return false;
               if (!(graph->PMarker)(graph, xAdr, yAdr, n))
		 succ = false;
               fail = false;
               if (ass)
                 for (i = 0; i < 16; i++)
                   if (graph->Ass[i] != nil)
                     if (!F77NAME(zibpm,ZIBPM)(&(graph->Ass[i]->id), &(graph->Ass[i]->wdNo), 
				   assAdr, xAdr, yAdr, nAdr))
			succ = false;   
            }
        graph = graph->next;  
      }

    if (fail)
      {
        fprintf(miniErrorFile,"MiniGraphic - zibpm :  no such window  id = %d  no = %d !\n", 
	       id, no);
	return false;
      }

    return succ;
  }





int F77NAME(zibfl,ZIBFL)(int *idAdr, int *noAdr, int *assAdr, void *xAdr, void *yAdr, int *nAdr)
  {
    int  id = *idAdr, no = *noAdr, ass = *assAdr, n = *nAdr, i, fail = true,
	 succ = true;
    GRAPHIC *graph = firstGraph;

    if ((n < 0) || (n > MAX_POINTS))
      {
         fprintf(miniErrorFile,"MiniGraphic - zibfl :  n = %d not allowed as array length !\n",
		n);
         return false;
      }
     
    while (graph != nil)
      {
        if (((graph->id) == id) || (id == -1))
          if (((graph->wdNo) == no) || (no == -1))
            {
               if (graph->newScal)
		 if (!ComputeScaling(graph))
		   return false;
               if (!(graph->Fill)(graph, xAdr, yAdr, n))
		 succ = false;
               fail = false;
               if (ass)
                 for (i = 0; i < 16; i++)
                    if (graph->Ass[i] != nil)
                      if (!F77NAME(zibfl,ZIBFL)(&(graph->Ass[i]->id), &(graph->Ass[i]->wdNo), 
				    assAdr, xAdr, yAdr, nAdr))
			succ = false;      
            }
        graph = graph->next;  
      }
  
    if (fail)
      {
        fprintf(miniErrorFile,"MiniGraphic - zibfl :  no such window  id = %d  no = %d !\n", 
	       id, no);
	return false;
      }

    return succ;
  }





int F77NAME(zibtx,ZIBTX)(int *idAdr, int *noAdr, int *assAdr, void *xAdr, void *yAdr, FtnStrPar TEXT, int TEXTLng)
  {
    int  id = *idAdr, no = *noAdr, ass = *assAdr, i, fail = true, succ = true;
    real  x, y;
    char *text = StrParAdr(TEXT);
    int k, n = StrParLen(TEXT);
    char  copyStr[MAX_STRING+1];
    GRAPHIC  *graph = firstGraph;

    if ((n < 0) || (n > MAXTEXT))
      {
         fprintf(miniErrorFile,"MiniGraphic - zibtx :  Text too long, n = %d !\n", n);
         return false;
      }
      
    while (graph != nil)
      {
        if (((graph->id) == id) || (id == -1))
          if (((graph->wdNo) == no) || (no == -1))
            {
               if (graph->newScal)
                 if (!ComputeScaling(graph))
		           return false;
               if ((graph->prec) == SINGLE)
                 {
                    x = *((float*)xAdr);
                    y = *((float*)yAdr);
                 }
               else
                 {
                    x = *((double*)xAdr);
                    y = *((double*)yAdr);
                 }
               for (k=0; k<n; k++)
                 {
                   if (text[k]=='\0') break;
                   copyStr[k] = text[k];
                 }
               copyStr[k] = '\0';
               if (!(graph->Text)(graph, x, y, copyStr))
		         succ = false;
               fail = false;

               if (ass)
                 for (i = 0; i < 16; i++)
                   if (graph->Ass[i] != nil)
                     if (!F77NAME(zibtx,ZIBTX)(&(graph->Ass[i]->id), &(graph->Ass[i]->wdNo), 
				                  assAdr, xAdr, yAdr, TEXT, n))
			           succ = false;
            }
        graph = graph->next;  
      }

    if (fail)
      {
        fprintf(miniErrorFile,"MiniGraphic - zibtx :  no such window  id = %d  no = %d !\n", 
	       id, no);
	return false;
      }
    return succ;
  }





int F77NAME(zibcol,ZIBCOL)(int *idAdr, int *col_no_Adr, int *rVal_Adr, int *gVal_Adr, int *bVal_Adr)
  {
    int  id = *idAdr, col_no = *col_no_Adr, 
         rVal = *rVal_Adr, gVal = *gVal_Adr, bVal = *bVal_Adr, fail = false,
	 succ = true;
    GRAPHIC  *graph = firstGraph;

    if ((col_no < 32) || (col_no > 95))
      {
         fprintf(miniErrorFile,"MiniGraphic - zibcol :  COLNO = %d  not allowed as ", col_no);
         fprintf(miniErrorFile,"colornumber ! (32..95)\n");
         fail = true;
      }
    if ((rVal < 0) || (rVal > 255))
      {
        fprintf(miniErrorFile,"MiniGraphic - zibcol :  RVAL = %d  not in range ! (0..255)\n", 
	       rVal);
        fail = true;
      }
    if ((gVal < 0) || (gVal > 255))
      {
        fprintf(miniErrorFile,"MiniGraphic - zibcol :  GVAL = %d  not in range ! (0..255)\n",
	       gVal);
        fail = true;
      }
    if ((bVal < 0) || (bVal > 255))
      {
        fprintf(miniErrorFile,"MiniGraphic - zibcol :  BVAL = %d  not in range ! (0..255)\n", 
	       bVal);
        fail = true;
      }

    if (fail)
      return false;

    fail = true;
    while (graph != nil)
      {
        if ((graph->id == id) || (id == -1))
	  {
            if (!(graph->Color)(col_no, rVal, gVal, bVal))
	      succ = false;
	    fail = false;
	  }
        graph = graph->next;
      }

    if (fail)
      {
	fprintf(miniErrorFile,"MiniGraphic - zibcol :  no such driver  id = %d !\n", id);
	return false;
      }
    return succ;
  }





int F77NAME(zibset,ZIBSET)(int *idAdr, int *noAdr, int *assAdr, int *typeAdr, FtnStrPar valAdr, int valAdrLng)
  {
    int  id = *idAdr, no = *noAdr, ass = *assAdr, type = *typeAdr, iVal, 
	     i, fail = true, succ = true;
    real  rVal;
    GRAPHIC  *graph = firstGraph;
    char buf[1024];

    while (graph != nil)
      {
        if (((graph->id) == id) || (id == -1))
          if (((graph->wdNo) == no) || (no == -1))
            {
              switch (type)
                {
            case PREC :
                  iVal = *((int*)valAdr);
                  if ((iVal == SINGLE) || (iVal == DOUBLE))
                    graph->prec = iVal;
                  else
                    {
                      fprintf(miniErrorFile,"MiniGraphic - zibset :  PREC = %d", iVal);
                      fprintf(miniErrorFile," not allowed, only SINGLE (4) or ");
                      fprintf(miniErrorFile,"DOUBLE (8) !\n");
                    }      
                  break;

            case SCALFIT :
                  iVal = *((int*)valAdr);
                  if ((iVal == 0) || (iVal == 1)) graph->scalFit = iVal;
                  else
                    {
                      fprintf(miniErrorFile,"MiniGraphic - zibset :  ");
					  fprintf(miniErrorFile,"SCALFIT = %d ", iVal);
                      fprintf(miniErrorFile,"not allowed, only 0 or 1 !\n");
                   }
                  graph->newScal = true;
                  break;

            case MINX :
                  if ((graph->prec) == SINGLE) rVal = *((float*)valAdr);
                  else rVal = *((double*)valAdr);
                  graph->minX = rVal;
                  graph->slMinX = rVal;
                  graph->newScal = true;
                  break;

            case MINY :
                  if ((graph->prec) == SINGLE) rVal = *((float*)valAdr);
                  else rVal = *((double*)valAdr);
                  graph->minY = rVal;
                  graph->slMinY = rVal;
                  graph->newScal = true;
                  break;

            case MAXX :
                  if ((graph->prec) == SINGLE) rVal = *((float*)valAdr);
                  else rVal = *((double*)valAdr);
                  graph->maxX = rVal;
                  graph->slMaxX = rVal;
                  graph->newScal = true;
                  break;

            case MAXY :
                  if ((graph->prec) == SINGLE) rVal = *((float*)valAdr);
                  else rVal = *((double*)valAdr);
                  graph->maxY = rVal;
                  graph->slMaxY = rVal;
                  graph->newScal = true;
                  break;

            case CAPTION :
            case FILENAME :
                  if(!CopyTrim(StrParAdr(valAdr),StrParLen(valAdr), buf, 1024))
                    { succ = false; break; }
                  if (!(graph->Settings)(graph, type, buf)) succ = false;
                  break;
            default :
                  if (!(graph->Settings)(graph, type, valAdr)) succ = false;
                  break;
                }
            
              fail = false;    
              if (ass)    
                for (i = 0; i < 16; i++)
                  if (graph->Ass[i] != nil)
                    if (!F77NAME(zibset,ZIBSET)(&(graph->Ass[i]->id), &(graph->Ass[i]->wdNo), 
			    	   assAdr, typeAdr, valAdr, valAdrLng))
		      succ = false;
            }
        graph = graph->next;
      }
    if (fail)
      {
         fprintf(miniErrorFile,"MiniGraphic - zibset :  no such window  id = %d  no = %d !\n", 
		id, no);
	 return false;
      }

    return succ;
  }




int F77NAME(zibreq,ZIBREQ)(int *idAdr, int *noAdr, int *typeAdr, void *valAdr)
  {
    int  id = *idAdr, no = *noAdr, type = *typeAdr, fail = true;
    real  rVal;
    GRAPHIC  *graph = firstGraph;

    *((int*)valAdr) = 0;

    while (graph != nil)
      {
        if (((graph->wdNo) == no) && ((graph->id) == id))
          {
             fail = false;
             switch (type)
               {
                  case WDORGX :  *((int*)valAdr) = graph->wdOrgX;
                                 return true;

                  case WDORGY :  *((int*)valAdr) = graph->wdOrgY;
                                 return true;

                  case WDWDTH:   *((int*)valAdr) = graph->wdWdth;
                                 return true;

                  case WDHGHT :  *((int*)valAdr) = graph->wdHght;
                                 return true;

                  case PENSIZE :  *((int*)valAdr) = graph->drPenSz;
                                  return true;

				  case LINESTYLE :  *((int*)valAdr) = graph->linestyle;
								    return true;

				  case COPYMODE :  *((int*)valAdr) = graph->copyMode;
								    return true;

                  case FONTSIZE :  *((int*)valAdr) = graph->drFntSz;
                                   return true;

                  case FONTPROP :  *((int*)valAdr) = graph->FntProp;
                                   return true;

                  case MARKER :  *((int*)valAdr) = graph->mark;
                                 return true;

                  case MAXWD :  *((int*)valAdr) = graph->maxWd;
                                return true;

                  case MAXCOL :  *((int*)valAdr) = graph->maxCol;
                                 return true;

                  case MAXGRY :  *((int*)valAdr) = graph->maxGry;
                                 return true;

                  case PREC :  *((int*)valAdr) = graph->prec;
                               return true;

                  case PENCOL :  *((int*)valAdr) = graph->penCol;
                                 return true;

                  case FONTCOL :  *((int*)valAdr) = graph->fntCol;
                                  return true;

                  case MARKCOL :  *((int*)valAdr) = graph->mrkCol;
                                  return true;

                  case FILLCOL :  *((int*)valAdr) = graph->fllCol;
                                  return true;

                  case BACKGRCOL :  *((int*)valAdr) = graph->backgrCol;
                                    return true;

                  case CAPTION :  *((char**)valAdr) = graph->caption;
                                  return true;

                  case SCALFIT :  *((int*)valAdr) = graph->scalFit;
                                  return true;

                  case FILENAME :  *((char**)valAdr) = graph->fileName;
                                   return true;

                  case BUFFER :  *((int*)valAdr) = graph->buffer;
                                 return true;

                  case MINX :  rVal = graph->minX;
                               break;

                  case MINY :  rVal = graph->minY;
                               break;

                  case MAXX :  rVal = graph->maxX;
                               break;

                  case MAXY :  rVal = graph->maxY;
                               break;

                  case SLMINX :  rVal = graph->slMinX;
                                 break;

                  case SLMINY:  rVal = graph->slMinY;
                                break;

                  case SLMAXX :  rVal = graph->slMaxX;
                                 break;

                  case SLMAXY :  rVal = graph->slMaxY;
                                 break;

                  case TOP :  rVal = graph->top;
                              break;

                  case LEFT :  rVal = graph->left;
                               break;

                  case BOTTOM :  rVal = graph->bottom;
                                 break;

                  case RIGHT :  rVal = graph->right;
                                break;

                  case XRESOL :  rVal = graph->xRes;
                                 break;

                  case YRESOL :  rVal = graph->yRes;
                                 break;

                  case TGXCM :  rVal = graph->drXcm;
                                break;

                  case TGYCM :  rVal = graph->drYcm;
                                break;

                  case USXTG :  rVal = graph->uXdr;
                                break;

                  case USYTG :  rVal = graph->uYdr;
                                break;

                  default :  fprintf(miniErrorFile,"MiniGraphic - zibreq :  Request for "); 
			     fprintf(miniErrorFile,"unknown type %d !\n", type);
                             return false;
               }
             if ((graph->prec) == SINGLE)
               *((float*)valAdr) = rVal;
             else
               *((double*)valAdr) = rVal;

             return true;
          }
        graph = graph->next;
      }

    if (fail)
      {
        fprintf(miniErrorFile,"MiniGraphic - zibreq :  no such window  id = %d  no = %d !\n", 
	       id, no);
	return false;
      }

    return true;
  }





int F77NAME(zibclr,ZIBCLR)(int *idAdr, int *noAdr, int *assAdr)
  {
    int  id = *idAdr, no = *noAdr, ass = *assAdr, i, fail = true, succ = true;
    GRAPHIC  *graph = firstGraph;

    while (graph != nil)
      {
        if (((graph->id) == id) || (id == -1))
          if (((graph->wdNo) == no) || (no == -1))
            {
               if (!(graph->NewPict)(graph))
		 succ = false;
               fail = false;
               if (ass)    
                 for (i = 0; i < 16; i++)
                   if (graph->Ass[i] != nil)
                     if (!F77NAME(zibclr,ZIBCLR)(&(graph->Ass[i]->id), &(graph->Ass[i]->wdNo), 
			     	  assAdr))
			succ = false;    
            }
        graph = graph->next;
      }
    if (fail)
      {
        fprintf(miniErrorFile,"MiniGraphic - zibclr :  no such window  id = %d  no = %d !\n", 
	       id, no);
	return false;
      }

    return succ;
  }





int F77NAME(zibwop,ZIBWOP)(int *idAdr, int *noAdr)
  {
    int  id = *idAdr;
    GRAPHIC  *graph;

    if ((graph = NewGraph(id)) == nil)
      {
         *idAdr = -1;
         return false;
      }

    *idAdr = graph->id;
    *noAdr = graph->wdNo;
    
    return true;
  }





int F77NAME(zibwcl,ZIBWCL)(int *idAdr, int *noAdr, int *assAdr)
  {
    int  id = *idAdr, no = *noAdr, ass = *assAdr, i, fail = true, succ = true;
    GRAPHIC  *graph = firstGraph, *next;

    while (graph != nil)
      {
         next = graph->next;
         if (((graph->id) == id) || (id == -1))
           if (((graph->wdNo) == no) || (no == -1))
             {
                if (ass)
                  for (i = 0; i < 16; i++)
                    if (graph->Ass[i] != nil)
                      {
                        if (!F77NAME(zibwcl,ZIBWCL)(&(graph->Ass[i]->id), 
			    &(graph->Ass[i]->wdNo), assAdr))
			  succ = false;
                         ReturnGraph(graph->Ass[i]);
                      }
                if (!(graph->Close)(graph))
		  succ = false;
                ReturnGraph(graph);
                fail = false;
             }
         graph = next;
      }

    if (fail)
      {
         fprintf(miniErrorFile,"MiniGraphic - zibwcl :  no such window  id = %d  no = %d !\n", 
		id, no);
         return false;
      }

    graph = firstGraph;
    while (graph != nil)
      {
         for (i = 0; i < 16; i++)
           if (graph->Ass[i] != nil)
             if ((graph->Ass[i]->id) == id)
               if ((graph->Ass[i]->wdNo) == no)
                 graph->Ass[i] = nil;
         graph = graph->next;
      }

    return succ;
  }





int F77NAME(zibadd,ZIBADD)(int *idAdr, int *noAdr, int *assidAdr, int *assnoAdr)
  {
    int  id = *idAdr, no = *noAdr, assid = *assidAdr, assno = *assnoAdr, i = 0,
         fail = true, fail_ass = true;
    GRAPHIC  *graph = firstGraph, *assgraph = firstGraph;

    while (graph != nil)
      {
        if ((graph->id) == id) 
          if ((graph->wdNo) == no) 
            {
               fail = false;
               while (assgraph != nil)
                 {
                    if ((assgraph->id) == assid)
                      if ((assgraph->wdNo) == assno)
                        {
                           fail_ass = false;
                           while (graph->Ass[i] != nil && i < 16)
                             i++;
                           if (i < 16)
			     {
                               graph->Ass[i] = assgraph;
			       return true;
			     }
			   else
			     {
			       	fprintf(miniErrorFile,"MiniGraphic - zibadd :  more than %d ",
				       MAX_ASS);
				fprintf(miniErrorFile,"associated windows !\n");
                               	return false;
			     }
                        }           
                    assgraph = assgraph->next;
                 }             
            }
        graph = graph->next;
      }

    if (fail) 
      fprintf(miniErrorFile,"MiniGraphic - zibadd :  no such window  id = %d  no = %d !\n",
	     id, no);
    if (fail_ass)
      {
       fprintf(miniErrorFile,"MiniGraphic - zibadd :  no such window  assid = %d  assno = %d ",
	      id, no);
       fprintf(miniErrorFile,"!\n");
      }

    return false;
  }





int F77NAME(zibsub,ZIBSUB)(int *idAdr, int *noAdr, int *assidAdr, int *assnoAdr)
  {
    int  id = *idAdr, no = *noAdr, assid = *assidAdr, assno = *assnoAdr, i,
         fail = true, fail_ass = true;
    GRAPHIC  *graph = firstGraph, *assgraph;

    while (graph != nil)
      {
        if (((graph->id) == id) || (id == -1))
          if (((graph->wdNo) == no) || (no == -1))
            {
               fail = false;
               assgraph = firstGraph;
               while (assgraph != nil)
                 {
                    if (((assgraph->id) == assid) || (assid == -1))
                      if (((assgraph->wdNo) == assno) || (assno == -1))
                        {
                           fail_ass = false;
                           i = 0;
                           while (graph->Ass[i] != assgraph && i < MAX_ASS)
                             i++;
			   if (i < MAX_ASS)
                             graph->Ass[i] = nil;
			   else
			    {
			     fprintf(miniErrorFile,"MiniGraphic - zibsub :  no association ");
			     fprintf(miniErrorFile,"between id %d, no %d and assid %d, assno %d !",
				    id, no, assid, assno);
			     return false;
			    }
                        }           
                    assgraph = assgraph->next;
                 }             
            }
        graph = graph->next;
      }

    if (fail)
      fprintf(miniErrorFile,"MiniGraphic - zibsub :  no such window  id = %d  no = %d !\n", 
	     id, no);
    if (fail_ass)
      {
       fprintf(miniErrorFile,"MiniGraphic - zibsub :  no such window  assid = %d  assno = %d ",
	      id, no);
       fprintf(miniErrorFile,"!\n");
      }
    if (fail || fail_ass)
      return false;
    return true;
  }





int F77NAME(zibgin,ZIBGIN)(int *idAdr, int *noAdr, int *geoAdr, void *x1koordAdr, void *y1koordAdr, void *x2koordAdr, 
	     void *y2koordAdr)
  {
    int id = *idAdr, no = *noAdr, geo = *geoAdr; 
    GRAPHIC *graph = firstGraph;
  
    *(float*)x1koordAdr = *(float*)y1koordAdr =
    *(float*)x2koordAdr = *(float*)y2koordAdr = 0;

   if (geo < 1 || geo > 3)
      {
         fprintf(miniErrorFile,"MiniGraphic - zibgeo :  geo = %d not allowed !\n", geo);
         return false;
      }

    while (graph != nil)
      {
         if ((graph->id) == id)
           if ((graph->wdNo) == no)
             {
                if (graph->newScal)
                  if (!ComputeScaling(graph))
		    return false;
                return (graph->Gin)(graph, geo, x1koordAdr, y1koordAdr,
                                         	x2koordAdr, y2koordAdr  );
              }
         graph = graph->next;
      }

    fprintf(miniErrorFile,"MiniGraphic - zibgin :  no such window  id = %d  no = %d !\n", 
	   id, no);

    return false;
  }

  
  
  
  
  
  
  
int F77NAME(zibev,ZIBEV)(int *idAdr, int *noAdr, int *typAdr,  int *buttonAdr, void *xkoordAdr, void *ykoordAdr, int *chAdr)
  {
    int  id = *idAdr, no = *noAdr; 
    GRAPHIC  *graph = firstGraph;

    while (graph != nil)
      {
        if ((graph->id) == id)
          if ((graph->wdNo) == no)
            {
               if (graph->newScal)
		 if (!ComputeScaling(graph))
		   return false;
               return (graph->Event)(graph, typAdr, buttonAdr,
				     xkoordAdr, ykoordAdr, chAdr);
            }
        graph = graph->next;
      }

    fprintf(miniErrorFile,"MiniGraphic - zibev :  no such window  id = %d  no = %d !\n", 
	   id, no);
    *typAdr = *buttonAdr = *chAdr = 0;
    *(float*)xkoordAdr = *(float*)ykoordAdr = 0;

    return false;
  }





int F77NAME(zibwt,ZIBWT)(int *idAdr, int *noAdr, int *typAdr,  int *buttonAdr, void *xkoordAdr, void *ykoordAdr, int *chAdr)
  {
    int  id = *idAdr, no = *noAdr; 
    GRAPHIC  *graph = firstGraph;

    while (graph != nil)
      {
         if ((graph->id) == id)
           if ((graph->wdNo) == no)
             {
                if (graph->newScal)
		  ComputeScaling(graph);
                return (graph->Wait)(graph, typAdr, buttonAdr,
				     xkoordAdr, ykoordAdr, chAdr);
             }
         graph = graph->next;
      }

    fprintf(miniErrorFile,"MiniGraphic - zibwt :  no such window  id = %d  no = %d !\n",
	   id, no);
    *typAdr = *buttonAdr = *chAdr = 0;
    *(float*)xkoordAdr = *(float*)ykoordAdr = 0;

    return false;
  }




int F77NAME(zibstr,ZIBSTR)(int *idAdr, int *noAdr, FtnStrPar STRING, int STRINGLng)
  {
    int  id = *idAdr, no = *noAdr; 
    GRAPHIC  *graph = firstGraph;
    char *stringAdr = StrParAdr(STRING);
    int lengthAdr = StrParLen(STRING);

    while (graph != nil)
      {
         if ((graph->id) == id)
           if ((graph->wdNo) == no)
             {
                if (graph->newScal)
		  ComputeScaling(graph);
                (graph->String)(graph, stringAdr, &lengthAdr);

		return true;
             }
         graph = graph->next;
      }

    fprintf(miniErrorFile,"MiniGraphic - zibstr :  no such window  id = %d  no = %d !\n",
	   id, no);

    return false;
  }

/* ++++++++++++++++++++++  copy string, trim blanks +++++++++++++++++++++ */

static int CopyTrim(char *ftnIn,int lng,char *buf,int bufLng)
  {
    int i=0, k;

	for (k=0; k<lng; k++)
	  {
		if (ftnIn[k]==' ') if (i==0) continue;
		if (k>=bufLng) return 0;
		if (ftnIn[k]==0) break;
		buf[i++] = ftnIn[k];
	  }
	for (i--; i>=0; i--) if (buf[i]!=' ') break;
	buf[i+1] = 0;

	return 1;
  }
