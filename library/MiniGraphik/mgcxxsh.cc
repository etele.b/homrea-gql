/*
 $Id: mgcxxsh.cc,v 1.2 1996/11/04 12:57:15 roitzsch Exp $
 (C)opyright 1996 by Konrad-Zuse-Center, Berlin
 All rights reserved.
 Part of the common environment

$Log: mgcxxsh.cc,v $
Revision 1.2  1996/11/04 12:57:15  roitzsch
adding Gin, Event, Wait, Color and String

*/

#include "minigraph.h"
#include <cxx_parameter.h>

Plot::Plot(int type, float size, const char* caption)
  {
    graph = NewGraph(type);
    int bc = YELLOW, buffer = ON, fc = BLACK, pc = BLACK;
    (graph->Settings)(graph, BACKGRCOL, (void*)&bc);
    (graph->Settings)(graph, PENCOL, (void*)&pc);
    (graph->Settings)(graph, FONTCOL, (void*)&fc);
    (graph->Settings)(graph, BUFFER, (void*)&buffer);
    (graph->Settings)(graph, CAPTION, (void*)(caption));
  }

Plot::~Plot()
  {
    (graph->Close)(graph);
    ReturnGraph(graph);
  }

int Plot::line  (double* x, double* y, int n)
  {
    if (graph->newScal)
      if (!ComputeScaling(graph)) return false;
    graph->prec = DOUBLE;
    return (graph->PLine)(graph, (void*)x, (void*)y, n);
  }

int Plot::line  (float*  x, float*  y, int n)
  {
    if (graph->newScal)
      if (!ComputeScaling(graph)) return false;
    graph->prec = SINGLE;
    return (graph->PLine)(graph, (void*)x, (void*)y, n);
  }

int Plot::marker(double* x, double* y, int n)
  {
    if (graph->newScal)
      if (!ComputeScaling(graph)) return false;
    graph->prec = DOUBLE;
    return (graph->PMarker)(graph, (void*)x, (void*)y, n);
  }

int Plot::marker(float*  x, float*  y, int n)
  {
    if (graph->newScal)
      if (!ComputeScaling(graph)) return false;
    graph->prec = SINGLE;
    return (graph->PMarker)(graph, (void*)x, (void*)y, n);
  }

int Plot::fill  (double* x, double* y, int n)
  {
    if (graph->newScal)
      if (!ComputeScaling(graph)) return false;
    graph->prec = DOUBLE;
    return (graph->Fill)(graph, (void*)x, (void*)y, n);
  }

int Plot::fill  (float*  x, float*  y, int n)
  {
    if (graph->newScal)
      if (!ComputeScaling(graph)) return false;
    graph->prec = SINGLE;
    return (graph->Fill)(graph, (void*)x, (void*)y, n);
  }

int Plot::text (double x, double y, const char* text)
  {
    if (graph->newScal)
      if (!ComputeScaling(graph)) return false;
    graph->prec = DOUBLE;
    return (graph->Text)(graph, x, y, text);
  }

int Plot::set (int type, int val)
  {
	switch (type)
	{
      case PREC :
	    if ((val == SINGLE) || (val == DOUBLE)) graph->prec = val;
	    else
	      {
	        fprintf(miniErrorFile,"MiniGraphic - zibset :  PREC = %d", val);
	        fprintf(miniErrorFile," not allowed, only SINGLE (4) or ");
	        fprintf(miniErrorFile,"DOUBLE (8) !\n");
	        return false;
	      }      
	    return true;

      case SCALFIT :
	    if ((val == 0) || (val == 1)) graph->scalFit = val;
	    else
	      {
	        fprintf(miniErrorFile,"MiniGraphic - zibset :  ");
		    fprintf(miniErrorFile,"SCALFIT = %d ", val);
	        fprintf(miniErrorFile,"not allowed, only 0 or 1 !\n");
	        return false;
	      }
	    graph->newScal = true;
	    return true;
	default :
	  return (graph->Settings)(graph, type, (void*)(&val));
	}
  }

int Plot::set (int type, double val)
  {
	switch (type)
	{
	  case MINX :
	    graph->minX = val;
	    graph->slMinX = val;
	    graph->newScal = true;
	    return true;

	  case MINY :
	    graph->minY = val;
	    graph->slMinY = val;
	    graph->newScal = true;
	    return true;

	  case MAXX :
	    graph->maxX = val;
	    graph->slMaxX = val;
	    graph->newScal = true;
	    return true;

	  case MAXY :
        graph->maxY = val;
	    graph->slMaxY = val;
	    graph->newScal = true;
	    return true;
	default :
	  return (graph->Settings)(graph, type, (void*)(&val));
	}
  }

int Plot::set (int type, char* str)
  {
	switch (type)
	{
	  case CAPTION:
	  case FILENAME:
	    return (graph->Settings)(graph, type, str);
	default :
	  return (graph->Settings)(graph, type, (void*)(str));
	}
  }

int Plot::request(int type, int* iVal)
  {
	switch (type)
	{
	  case WDORGX:
	    *iVal = graph->wdOrgX; return true;

	  case WDORGY:
	    *iVal = graph->wdOrgY; return true;

	  case WDWDTH:
	    *iVal = graph->wdWdth; return true;

	  case WDHGHT:
	    *iVal = graph->wdHght; return true;

	  case PENSIZE:
	    *iVal = graph->drPenSz; return true;

	  case LINESTYLE:
	    *iVal = graph->linestyle;  return true;

	  case COPYMODE:
	    *iVal = graph->copyMode; return true;

	  case FONTSIZE:
	    *iVal = graph->drFntSz; return true;

	  case FONTPROP:
	    *iVal = graph->FntProp; return true;

	  case MARKER:
	    *iVal = graph->mark;  return true;

	  case MAXWD:
	    *iVal = graph->maxWd; return true;

	  case MAXCOL:
	    *iVal = graph->maxCol; return true;

	  case MAXGRY:
	    *iVal = graph->maxGry; return true;

	  case PREC:
	    *iVal = graph->prec; return true;

	  case PENCOL:
	    *iVal = graph->penCol; return true;

	  case FONTCOL:
	    *iVal = graph->fntCol; return true;

	  case MARKCOL:
	    *iVal = graph->mrkCol; return true;

	  case FILLCOL:
	    *iVal = graph->fllCol; return true;

	  case BACKGRCOL:
	    *iVal = graph->backgrCol; return true;

	  case SCALFIT:
	    *iVal = graph->scalFit; return true;

	  case BUFFER:
	    *iVal = graph->buffer; return true;

	  default:
	    fprintf(miniErrorFile,"MiniGraphic - zibreq :  Request for "); 
	    fprintf(miniErrorFile,"unknown type %d, int value !\n", type);
	             return false;
	 }
  }

int Plot::request(int type, double*  rVal)
  {
	switch (type)
	{
	  case MINX:
	    *rVal = graph->minX; return true;

	  case MINY:
	    *rVal = graph->minY; return true;

	  case MAXX:
	    *rVal = graph->maxX; return true;

	  case MAXY:
	    *rVal = graph->maxY; return true;

	  case SLMINX:
	    *rVal = graph->slMinX; return true;

	  case SLMINY:
	    *rVal = graph->slMinY; return true;

	  case SLMAXX:
	    *rVal = graph->slMaxX; return true;

	  case SLMAXY:
	    *rVal = graph->slMaxY; return true;

	  case TOP:
	    *rVal = graph->top; return true;

	  case LEFT:
	    *rVal = graph->left; return true;

	  case BOTTOM:
	    *rVal = graph->bottom; return true;

	  case RIGHT:
	    *rVal = graph->right; return true;

	  case XRESOL:
	    *rVal = graph->xRes; return true;

	  case YRESOL:
	    *rVal = graph->yRes; return true;

	  case TGXCM:
	    *rVal = graph->drXcm; return true;

	  case TGYCM:
	    *rVal = graph->drYcm; return true;

	  case USXTG:
	    *rVal = graph->uXdr; return true;

	  case USYTG:
	    *rVal = graph->uYdr; return true;

	  default:
	    fprintf(miniErrorFile,"MiniGraphic - zibreq :  Request for "); 
	    fprintf(miniErrorFile,"unknown type %d, double value !\n", type);
	             return false;
	 }
  }

int Plot::request(int type, char** cp)
  {
	switch (type)
	{
	  case CAPTION:
	    *cp = graph->caption; return true;

	  case FILENAME:
	    *cp = graph->fileName; return true;

	  default:
	    fprintf(miniErrorFile,"MiniGraphic - zibreq :  Request for "); 
	    fprintf(miniErrorFile,"unknown type %d, string value !\n", type);
	             return false;
	 }
  }

int Plot::clear()
  {
    return graph->NewPict(graph);
  }

void Plot::flush()
  {
    int saveBuffer = graph->buffer, flush = FLUSH;
    if (saveBuffer!=FLUSH)
      {
        graph->Settings(graph, BUFFER, (void*)&flush);
        graph->Settings(graph, BUFFER, (void*)&saveBuffer);
      }
    return;
  }

int Plot::gin(int geo, void* x1koordAdr, void* y1koordAdr,
              void* x2koordAdr, void* y2koordAdr)
  {
    return graph->Gin(graph,geo,x1koordAdr,y1koordAdr,
                      x2koordAdr,y2koordAdr);
  }

int Plot::event(int* typAdr, int* buttonAdr, void* xkoordAdr,
                void* ykoordAdr, int* chAdr)
  {
    return graph->Event(graph,typAdr,buttonAdr,
                        xkoordAdr,ykoordAdr,chAdr);
  }

int Plot::wait(int* typAdr, int* buttonAdr, void* xkoordAdr,
               void* ykoordAdr, int* chAdr)
  {
    return graph->Wait(graph,typAdr,buttonAdr,
                      xkoordAdr,ykoordAdr,chAdr);
  }

int Plot::color(int col_no, int rVal, int gVal, int bVal)
  {
    return graph->Color(col_no,rVal,gVal,bVal);
  }

int Plot::string(char* stringAdr, int* lengthAdr)
  {
    return graph->String(graph,stringAdr,lengthAdr);
  }
