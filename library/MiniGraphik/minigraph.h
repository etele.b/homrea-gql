/* $Id: minigraph.h,v 1.1.1.1 1996/11/04 12:05:46 roitzsch Exp $ */

#ifndef __MINIGRAPH__
#define __MINIGRAPH__

#include <stdio.h>

#include "config.h"

#ifdef __cplusplus
#   define EXTERN extern "C"
#   define NO(x)
#else
#   define EXTERN extern
#   define NO(x) x
#endif

#ifndef nil
#define nil 0L
#endif
#ifndef true
#define true 1
#define false 0 
#endif

typedef double real;

#define MAX_STRING 80
#define MAX_POINTS 1000

#include "mgconst.h"

typedef struct GRAPHIC GRAPHIC;

struct GRAPHIC
  {
    int (*PLine)(GRAPHIC *graph, void *xAdr, void *yAdr, int n);
    int (*PMarker)(GRAPHIC *graph, void *xAdr, void *yAdr, int n);
#ifdef __cplusplus
    int (*Text)(GRAPHIC *graph, real x, real y, const char *s);
#else
    int (*Text)(GRAPHIC *graph, real x, real y, char *s);
#endif
    int (*Fill)(GRAPHIC *graph, void *xAdr, void *yAdr, int n);
    int (*Color)(int col_no, int rVal, int gVal, int bVal);
    int (*Settings)(GRAPHIC *graph, int type, void *valAdr);
    int (*NewPict)(GRAPHIC *graph);
    int (*OpenPort)(GRAPHIC *graph);
    int (*Close)(GRAPHIC *graph);
    int (*Gin)(GRAPHIC *graph, int geo, void *x1koordAdr, void *y1koordAdr,
               void *x2koordAdr, void *y2koordAdr);
    int (*Event)(GRAPHIC *graph, int *typAdr, int *buttonAdr,
                 void *xkoordAdr, void *ykoordAdr, int *chAdr);
    int (*Wait)(GRAPHIC *graph, int *typAdr, int *buttonAdr,
                void *xkoordAdr, void *ykoordAdr, int *chAdr);
    int (*String)(GRAPHIC *graph, char *string, int *length);

    real   minX, minY, maxX, maxY,
	   slMinX, slMinY, slMaxX, slMaxY,
	   top, left, bottom, right,
	   drXcm, drYcm, uXdr, uYdr, uXcm, uYcm,
	   drRes,
	   uRes, uPenSz, uFntSz,
	   xRes, yRes;

    int	   ready, id, wdNo, newScal, firstClip,
	   wdOrgX, wdOrgY, wdWdth, wdHght,
	   maxWd, maxCol, maxGry, /*fillP, clipP,*/
	   mark,
	   drPenSz, linestyle, copyMode, drFntSz, FntProp,
	   penCol, fntCol, mrkCol, fllCol, backgrCol,
	   prec, scalFit, buffer, page_count, line_count;

    char   *caption, *fontName, *fileName; /**driverName;*/

    real   xScal, yScal, xTrans, yTrans;

    FILE   *file;

    struct GRAPHIC *next, *Ass[16];
  };

EXTERN GRAPHIC *firstGraph;
EXTERN int ComputeScaling(GRAPHIC *g);
EXTERN GRAPHIC *NewGraph(int id);
EXTERN void ReturnGraph(GRAPHIC *g);
EXTERN int PrintGraph(GRAPHIC *g);
EXTERN int MacInit(GRAPHIC *g);
EXTERN int XInit(GRAPHIC *g);
EXTERN int PSInit(GRAPHIC *g, int type);
EXTERN FILE *miniErrorFile;

#if MACOSAVAIL == 1
#define CHECK_FILE if(miniErrorFile==nil)miniErrorFile=fopen("minigraph.log","w")
#else
#define CHECK_FILE if(miniErrorFile==nil)miniErrorFile=stdout
#endif

EXTERN int c_zibpl(int id, int no, int ass, void *xAdr, void *yAdr, int n);
EXTERN int c_zibpm(int id, int no, int ass, void *xAdr, void *yAdr, int n);
EXTERN int c_zibfl(int id,int  no, int ass, void *xAdr, void *yAdr, int n);
EXTERN int c_zibtx(int id, int no, int ass, double x, double y, char *text);
EXTERN int c_zibcol(int id, int col_no, int rVal, int gVal, int bVal);
EXTERN int c_zibset(int id, int no, int ass, int type, ...);
EXTERN int c_zibreq(int id, int no, int type, void *valAdr);
EXTERN int c_zibclr(int id, int no, int ass);
EXTERN int c_zibwop(int *idAdr, int *noAdr);
EXTERN int c_zibwcl(int id, int no, int ass);
EXTERN int c_zibadd(int id, int no, int assid, int assno);
EXTERN int c_zibsub(int id, int no, int assid, int assno);
EXTERN int c_zibgin(int id, int no, int geo, void *x1koordAdr, void *y1koordAdr,
  void *x2koordAdr, void *y2koordAdr);
EXTERN int c_zibev(int id, int no, int *typAdr,  int *buttonAdr,
  void *xkoordAdr, void *ykoordAdr, int *chAdr);
EXTERN int c_zibwt(int id, int no, int *typAdr, int * buttonAdr,
  void *xkoordAdr, void *ykoordAdr, int *chAdr);
EXTERN int c_zibstr(int id, int no, char *string, int length);

EXTERN int F77NAME(zibpl,ZIBPL)(int *id, int *no, int *ass, void *xAdr, void *yAdr, int *n);
EXTERN int F77NAME(zibpm,ZIBPM)(int *id, int *no, int *ass, void *xAdr, void *yAdr, int *n);
EXTERN int F77NAME(zibfl,ZIBFL)(int *id,int  *no, int *ass, void *xAdr, void *yAdr, int *n);
EXTERN int F77NAME(zibtx,ZIBTX)(int *id, int *no, int *ass, void *x, void *y, FtnStrPar text, int textLng);
EXTERN int F77NAME(zibcol,ZIBCOL)(int *id, int *col_no, int *rVal, int *gVal, int *bVal);
EXTERN int F77NAME(zibset,ZIBSET)(int *id, int *no, int *ass, int *type, FtnStrPar valAdr, int n);
EXTERN int F77NAME(zibreq,ZIBREQ)(int *id, int *no, int *type, void *valAdr);
EXTERN int F77NAME(zibclr,ZIBCLR)(int *id, int *no, int *ass);
EXTERN int F77NAME(zibwop,ZIBWOP)(int *idAdr, int *noAdr);
EXTERN int F77NAME(zibwcl,ZIBWCL)(int *id, int *no, int*ass);
EXTERN int F77NAME(zibadd,ZIBADD)(int *id, int *no, int *assid, int *assno);
EXTERN int F77NAME(zibsub,ZIBSUB)(int *id, int *no, int *assid, int *assno);
EXTERN int F77NAME(zibgin,ZIBGIN)(int *id, int *no, int *geo, void *x1koordAdr, void *y1koordAdr,
  void *x2koordAdr, void *y2koordAdr);
EXTERN int F77NAME(zibev,ZIBEV)(int *id, int* no, int *typAdr,  int *buttonAdr,
  void *xkoordAdr, void *ykoordAdr, int *chAdr);
EXTERN int F77NAME(zibwt,ZIBWT)(int *id, int *no, int *typAdr, int * buttonAdr,
  void *xkoordAdr, void *ykoordAdr, int *chAdr);
EXTERN int F77NAME(zibstr,ZIBSTR)(int *id, int *no, FtnStrPar string, int stringLng);

#endif
