/*
 $Id: psdriv.c,v 1.1.1.1 1996/11/04 12:05:46 roitzsch Exp $
 (C)opyright 1996 by Konrad-Zuse-Center, Berlin
 All rights reserved.
 Part of the common environment

$Log: psdriv.c,v $
Revision 1.1.1.1  1996/11/04 12:05:46  roitzsch
minigraph module

*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
 
#include "minigraph.h"

#if MACOSAVAIL == 1
#define cuserid(X) "Macintosh"
#else
#include <sys/types.h>
#include <unistd.h>
#endif
  
#define K180PI 57.29578

#define PTTOINCH 0.0139
#define INCHTOPT 72.0
#define PTTOCM 0.035277778
#define CMTOPT 28.346457
#define MAX_FILES 16
#define COLORS 64
#define GRAYS 24
#define ALLCOL 96

#define TRANSX(X) (real)((X)*(graph->xScal)+(graph->xTrans)+(graph->wdOrgX))
#define TRANSY(Y) (real)((Y)*(graph->yScal)+(graph->yTrans)+(graph->wdOrgY))
#define Q_TRANSX(X) (real)((X)*(graph->xScal)+(graph->xTrans)+(graph->wdOrgY))
#define Q_TRANSY(Y) (real)((graph->wdOrgX)-((Y)*(graph->yScal)+(graph->yTrans)))

static float red [ALLCOL]   = { 0, 1, 1, 0, 0, 0, 1, 1,
0.000000, 0.043478, 0.086957, 0.130435, 0.173913, 0.217391, 0.260870, 0.304348,
0.347826, 0.391304, 0.434783, 0.478261, 0.521739, 0.565217, 0.608696, 0.652174,
0.695652, 0.739130, 0.782609, 0.826087, 0.869565, 0.913043, 0.956522, 1.000000,

0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.066667,
0.133333, 0.200000, 0.266667, 0.333333, 0.400000, 0.466667, 0.533333, 0.600000,
0.666667, 0.733333, 0.800000, 0.866667, 0.933333, 1.000000, 1.000000, 1.000000,
1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000,
1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000,
1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000,
1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000,
1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000
};
static float green [ALLCOL] = { 0, 1, 0, 1, 0, 1, 0, 1,
0.000000, 0.043478, 0.086957, 0.130435, 0.173913, 0.217391, 0.260870, 0.304348,
0.347826, 0.391304, 0.434783, 0.478261, 0.521739, 0.565217, 0.608696, 0.652174,
0.695652, 0.739130, 0.782609, 0.826087, 0.869565, 0.913043, 0.956522, 1.000000,

0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.066667,
0.133333, 0.200000, 0.266667, 0.333333, 0.400000, 0.466667, 0.533333, 0.533333,
0.533333, 0.533333, 0.533333, 0.533333, 0.533333, 0.533333, 0.533333, 0.533333,
0.533333, 0.533333, 0.533333, 0.533333, 0.533333, 0.466667, 0.400000, 0.333333,
0.266667, 0.200000, 0.133333, 0.066667, 0.000000, 0.066667, 0.133333, 0.200000,
0.266667, 0.333333, 0.400000, 0.466667, 0.533333, 0.600000, 0.666667, 0.733333,
0.800000, 0.866667, 0.933333, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000,
1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000
};
static float blue [ALLCOL]  = { 0, 1, 0, 0, 1, 1, 1, 0,
0.000000, 0.043478, 0.086957, 0.130435, 0.173913, 0.217391, 0.260870, 0.304348,
0.347826, 0.391304, 0.434783, 0.478261, 0.521739, 0.565217, 0.608696, 0.652174,
0.695652, 0.739130, 0.782609, 0.826087, 0.869565, 0.913043, 0.956522, 1.000000,

0.600000, 0.666667, 0.733333, 0.800000, 0.866667, 0.933333, 1.000000, 1.000000,
1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000,
1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 0.933333, 0.866667,
0.800000, 0.733333, 0.666667, 0.600000, 0.533333, 0.466667, 0.400000, 0.333333,
0.266667, 0.200000, 0.133333, 0.066667, 0.000000, 0.000000, 0.000000, 0.000000,
0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
0.000000, 0.000000, 0.000000, 0.000000, 0.270588, 0.337255, 0.403922, 0.470588,
0.537255, 0.600000, 0.666667, 0.733333, 0.800000, 0.866667, 0.933333, 1.000000
};

static int  file[MAX_FILES];
static real fontSize[3] = { 8.0, 10.0, 12.0 },
        lineWidth[3] = { 0.01, 0.6, 1.5 };
static char *pattern[] = { "1 2",
			   "1 4",
			   "3 3",
			   "6 3",
			   "5 3 1 3",
			   "5 3 1 3 1 3",
			   ""              };
static char *psFName[MAX_FILES] = { "minigraph_1.ps",  "minigraph_2.ps",
				   "minigraph_3.ps",  "minigraph_4.ps",
				   "minigraph_5.ps",  "minigraph_6.ps",
				   "minigraph_7.ps",  "minigraph_8.ps",
				   "minigraph_9.ps",  "minigraph_10.ps",
				   "minigraph_11.ps", "minigraph_12.ps",
				   "minigraph_13.ps", "minigraph_14.ps",
				   "minigraph_15.ps", "minigraph_16.ps"  };
static long boundBoxPos[MAX_FILES];
static real posMinX[MAX_FILES],posMaxX[MAX_FILES],posMinY[MAX_FILES],posMaxY[MAX_FILES];
static char *captionName = "MiniGraphic",
	    *fontName ="H"; /* Helvetica */
static real leftMargin = 2.0*CMTOPT, rightMargin = 1.5*CMTOPT,
            topMargin = 2.0*CMTOPT, bottomMargin = 2.0*CMTOPT;

#define MAXPSMARKERS 7
static char *psMarkers[MAXPSMARKERS] =
 { "", "MK1", "MK2", "MK3", "MK4", "MK5", "MK6" };

static int saveCol = -1;

static void Clipping(GRAPHIC *graph)
  {
    int col = graph->backgrCol;

    if (graph->id == PS_QUER)
      {
	fprintf(graph->file, "NP %f %f M\n", (float)(graph->wdOrgX+0.8), 
						   (float)(graph->wdOrgY-0.8));
	fprintf(graph->file, "%f %f L\n", (float)(graph->wdOrgX+0.8),
                       		(float)(graph->wdOrgY+graph->wdWdth+0.5));
	fprintf(graph->file, "%f %f L\n", (float)(graph->wdOrgX-graph->wdHght-0.6), 
                       (float)(graph->wdOrgY+graph->wdWdth+0.5));
	fprintf(graph->file, "%f %f L\n", (float)(graph->wdOrgX-graph->wdHght-0.6),
					   (float)(graph->wdOrgY-0.8));
	fprintf(graph->file, "%f %f L\n", (float)(graph->wdOrgX+0.8),
					   (float)(graph->wdOrgY-0.8));
	fprintf(graph->file, "clip\n");



    if (col!=saveCol)
      {
	    fprintf(graph->file, " %.4f %.4f %.4f SC\n",
		    red[col], green[col], blue[col]);
		saveCol = col;
	  }

	fprintf(graph->file, "NP %f %f M\n", (float)(graph->wdOrgX), 
						   (float)(graph->wdOrgY));
	fprintf(graph->file, "%f %f L\n", (float)(graph->wdOrgX),
                       		(float)(graph->wdOrgY+graph->wdWdth));
	fprintf(graph->file, "%f %f L\n", (float)(graph->wdOrgX-graph->wdHght), 
                       (float)(graph->wdOrgY+graph->wdWdth));
	fprintf(graph->file, "%f %f L\n", (float)(graph->wdOrgX-graph->wdHght),
					   (float)(graph->wdOrgY));
	fprintf(graph->file, "%f %f L\n", (float)(graph->wdOrgX),
					   (float)(graph->wdOrgY));
	fprintf(graph->file, "F\n");

	graph->firstClip = false;
	(graph->line_count) += 13;

	return;
    }



    fprintf(graph->file, "NP %f %f M\n", (float)(graph->wdOrgX-0.6), 
						   (float)(graph->wdOrgY-0.8));
    fprintf(graph->file, "%f %f L\n", (float)(graph->wdOrgX+graph->wdWdth
						   +0.8),
                       (float)(graph->wdOrgY-0.8));
    fprintf(graph->file, "%f %f L\n", (float)(graph->wdOrgX+graph->wdWdth
						   +0.8), 
                       (float)(graph->wdOrgY+graph->wdHght
			       +0.5));
    fprintf(graph->file, "%f %f L\n", (float)(graph->wdOrgX-0.6),
					   (float)(graph->wdOrgY+graph->wdHght
						   +0.5));
    fprintf(graph->file, "%f %f L\n", (float)(graph->wdOrgX-0.6),
					   (float)(graph->wdOrgY-0.8));
    fprintf(graph->file, "clip\n");
    (graph->line_count) += 6;

    if (red[col] != 1 || green[col] != 1 || blue[col] != 1)
      {
    if (col!=saveCol)
      {
	    fprintf(graph->file, " %.4f %.4f %.4f SC\n",
		    red[col], green[col], blue[col]);
		saveCol = col;
	  }
	fprintf(graph->file, "NP %f %f M\n", 
		(float)(graph->wdOrgX-0.6), (float)(graph->wdOrgY-0.8));
	fprintf(graph->file, "%f %f L\n", 
		(float)(graph->wdOrgX+graph->wdWdth+0.8),
                (float)(graph->wdOrgY-0.8));
	fprintf(graph->file, "%f %f L\n", 
		(float)(graph->wdOrgX+graph->wdWdth+0.8), 
                (float)(graph->wdOrgY+graph->wdHght+0.5));
	fprintf(graph->file, "%f %f L\n", (float)(graph->wdOrgX-0.6),
		(float)(graph->wdOrgY+graph->wdHght+0.5));
	fprintf(graph->file, "%f %f L\n", (float)(graph->wdOrgX-0.6),
		(float)(graph->wdOrgY-0.8));
	fprintf(graph->file, "F\n");
	(graph->line_count) += 7;
      }

    graph->firstClip = false;
 
    return;
  }





static int PSOpenPort(GRAPHIC *graph)
 {
    time_t t;

    if ((graph->file) == nil)
      graph->file = fopen(graph->fileName,"w");
    else
      return false;
    if ((graph->file) == nil)
      {
	fprintf(miniErrorFile,"MiniGraphic - zibwop :  Can not open file '%s' !\n", 
	       graph->fileName);
	return false;
      }
    fprintf(graph->file, "%%!PS-Adobe-1.0\n");
    fprintf(graph->file, "%%%%Title: %s\n", graph->fileName);
    fprintf(graph->file, "%%%%Creator: MiniGraphic\n");
    fprintf(graph->file, "%%%%For: %s\n", getlogin());
    time(&t);
    fprintf(graph->file, "%%%%CreationDate: %s", ctime(&t));
    fprintf(graph->file, "%%%%Pages: (atend)\n");
    fprintf(graph->file, "%%%%DocumentFonts: Times-Roman Symbol\n");
    fprintf(graph->file, "%%%%BoundingBox:");
    boundBoxPos[graph->wdNo] = ftell(graph->file);
    fprintf(graph->file, "%7.2f %7.2f %7.2f %7.2f\n",0.0,0.0,0.0,0.0);
    fprintf(graph->file, "%%%%EndComments\n");
    fprintf(graph->file, "/NP {newpath} def\n");
    fprintf(graph->file, "/M  {moveto} def\n");
    fprintf(graph->file, "/L  {lineto} def\n");
    fprintf(graph->file, "/F  {fill} def\n");
    fprintf(graph->file, "/R  {rotate} def\n");
    fprintf(graph->file, "/SL {setlinewidth} def\n");
    fprintf(graph->file, "/SD {setdash} def\n");
    fprintf(graph->file, "/SF {findfont exch scalefont setfont} def\n");
    fprintf(graph->file, "/H  {/Helvetica} def\n");
    fprintf(graph->file, "/SC {setrgbcolor} def\n");
    fprintf(graph->file, "/S  {stroke} def\n");
    fprintf(graph->file, "/SH {show} def\n");
    fprintf(graph->file, "/MK1  {10 /Times-Roman SF (*) stringwidth pop 0.49 mul neg -4.9 rmoveto (*) SH} def\n");
    fprintf(graph->file, "/MK2  {10 /Symbol SF (\\264) stringwidth pop 0.5 mul neg -2.7 rmoveto (\\264) SH} def\n");
    fprintf(graph->file, "/MK3  {10 /Times-Roman SF (+) stringwidth pop 0.5 mul neg -2.5 rmoveto (+) SH} def\n");
    fprintf(graph->file, "/MK4  {10 /Times-Roman SF(\\267) stringwidth pop 0.5 mul neg -3.3 rmoveto (\\267) SH} def\n");
    fprintf(graph->file, "/MK5  {10 /Symbol SF(\\304) stringwidth pop 0.5 mul neg -3.5 rmoveto (\\304) SH} def\n");
    fprintf(graph->file, "/MK6  {10 /Symbol SF(\\305) stringwidth pop 0.5 mul neg -3.5 rmoveto (\\305) SH} def\n");
    fprintf(graph->file, "%%%%EndProlog\n");
    fprintf(graph->file, "%%%%Page ? 1\n");
    fprintf(graph->file, "%.4f SL\n", lineWidth[graph->drPenSz]);
    fprintf(graph->file, "%.4f %s SF\n", fontSize[graph->drFntSz],
					       graph->fontName);
    fprintf(graph->file, "2 setlinejoin\n");
    (graph->line_count) += 32;
    graph->ready = true;

    return true;
  }
 
 
 
 
 
static int PSNewPict(GRAPHIC *graph)
  {
    if (!graph->ready)
      return true;

    if (graph->id == PS_TEX)
      {
	fprintf(miniErrorFile,"MiniGraphic - zibclr :  not possible on TEX-files !");
	return false;
      }
    fprintf(graph->file, "showpage\n");
    fprintf(graph->file, "%%%%Page ? %d\n", ++graph->page_count);
    Clipping(graph);
    graph->line_count += 2;

    return true;
  } 
 
 
 


static int PSClose(GRAPHIC *graph)
  {
    if (graph->ready)
	  {
		if (graph->id != PS_TEX)
		  fprintf(graph->file, "showpage\n");
		fprintf(graph->file, "%%%%Trailer\n");
		fprintf(graph->file, "%%%%Pages: %d\n", graph->page_count++);
/*
		fprintf(graph->file, "%%%%BoundingBox: %d %d %d %d\n", graph->wdOrgX, 
								   graph->wdOrgY,
						 (graph->wdOrgX + graph->wdWdth),
						 (graph->wdOrgY + graph->wdHght)  );
*/
		fflush(graph->file);
		fseek(graph->file,boundBoxPos[graph->wdNo],SEEK_SET);
		fprintf(graph->file,"%7.2f %7.2f %7.2f %7.2f",posMinX[graph->wdNo],posMinY[graph->wdNo],
		        posMaxX[graph->wdNo],posMaxY[graph->wdNo]);
		fclose(graph->file);
		graph->line_count += 4;
		fprintf(miniErrorFile,"MiniGraphic - zibwcl :  %d lines written to ", graph->line_count);
		fprintf(miniErrorFile,"Postscript file '%s' !\n", graph->fileName);
		graph->ready = false;
	  }
    graph->file = nil;
    file[graph->wdNo] = true;
	graph->line_count = 0;
	graph->page_count = 0;

    return true;
  }
 
 
 
 
 
static int PSPLine(GRAPHIC *graph, void *xAdr, void *yAdr, int n)
  {
    int k, col = graph->penCol;
    real x, y, xt, yt;

    if (graph->firstClip)
      Clipping(graph);

    if (col!=saveCol)
      {
	    fprintf(graph->file, " %.4f %.4f %.4f SC\n",
		    red[col], green[col], blue[col]);
		saveCol = col;
	  }

    if ((graph->prec) == SINGLE)
      {
	x = ((float*)xAdr)[0];
	y = ((float*)yAdr)[0];
      }
    else
      {
	x = ((double*)xAdr)[0];
	y = ((double*)yAdr)[0];
      }
	if (graph->id == PS_QUER)
      {
	    xt = Q_TRANSX(x);
	    yt = Q_TRANSY(y);
	    if (xt<posMinX[graph->wdNo]) posMinX[graph->wdNo] = xt;
	    if (xt>posMaxX[graph->wdNo]) posMaxX[graph->wdNo] = xt;
	    if (yt<posMinY[graph->wdNo]) posMinY[graph->wdNo] = yt;
	    if (yt>posMaxY[graph->wdNo]) posMaxY[graph->wdNo] = yt;
        fprintf(graph->file, "NP %.3f %.3f M\n", yt, xt);
      }
    else
      {
	    xt = TRANSX(x);
	    yt = TRANSY(y);
	    if (xt<posMinX[graph->wdNo]) posMinX[graph->wdNo] = xt;
	    if (xt>posMaxX[graph->wdNo]) posMaxX[graph->wdNo] = xt;
	    if (yt<posMinY[graph->wdNo]) posMinY[graph->wdNo] = yt;
	    if (yt>posMaxY[graph->wdNo]) posMaxY[graph->wdNo] = yt;
        fprintf(graph->file, "NP %.3f %.3f M\n", xt, yt);
      }
    for (k = 1; k < n; k++)
      {
        if ((graph->prec)==SINGLE)
          {
	        x = ((float*)xAdr)[k];
	        y = ((float*)yAdr)[k];
	      }
	    else
          {
	        x = ((double*)xAdr)[k];
	        y = ((double*)yAdr)[k];
	      }
		if (graph->id == PS_QUER)
		  {
		    xt = Q_TRANSX(x);
		    yt = Q_TRANSY(y);
		    if (xt<posMinX[graph->wdNo]) posMinX[graph->wdNo] = xt;
		    if (xt>posMaxX[graph->wdNo]) posMaxX[graph->wdNo] = xt;
		    if (yt<posMinY[graph->wdNo]) posMinY[graph->wdNo] = yt;
		    if (yt>posMaxY[graph->wdNo]) posMaxY[graph->wdNo] = yt;
		    fprintf(graph->file, "%.3f %.3f L\n", yt, xt);
		  }
		else
		  {
		    xt = TRANSX(x);
		    yt = TRANSY(y);
		    if (xt<posMinX[graph->wdNo]) posMinX[graph->wdNo] = xt;
		    if (xt>posMaxX[graph->wdNo]) posMaxX[graph->wdNo] = xt;
		    if (yt<posMinY[graph->wdNo]) posMinY[graph->wdNo] = yt;
		    if (yt>posMaxY[graph->wdNo]) posMaxY[graph->wdNo] = yt;
		    fprintf(graph->file, "%.3f %.3f L\n", xt, yt);
	      }
	  }
    fprintf(graph->file, "S\n");
    (graph->line_count) += n+2;

    return true;
  }
 
 
 
 
 
static int PSPMarker(GRAPHIC *graph, void *xAdr, void *yAdr, int n)
  {
    int k, col = graph->mrkCol;
    real x, y;

    if (graph->firstClip)
      Clipping(graph);

    if (col!=saveCol)
      {
	    fprintf(graph->file, " %.4f %.4f %.4f SC\n",
		    red[col], green[col], blue[col]);
		saveCol = col;
	  }

    for (k = 0; k < n; k++)
      {
        if ((graph->prec) == SINGLE)
          {
	    x = ((float*)xAdr)[k];
	    y = ((float*)yAdr)[k];
	  }
        else
          {
	    x = ((double*)xAdr)[k];
	    y = ((double*)yAdr)[k];
	  }

	if (graph->id == PS_QUER)
          fprintf(graph->file, "%.3f %.3f M %s\n",
		  Q_TRANSY(y), Q_TRANSX(x), psMarkers[graph->mark]);
	else
          fprintf(graph->file, "%.3f %.3f M %s\n",
		  TRANSX(x), TRANSY(y), psMarkers[graph->mark]);
      }
    fprintf(graph->file, "%.4f %s SF\n",
            fontSize[graph->drFntSz], graph->fontName);
    (graph->line_count) += n+2;

    return true;
  }
 
 
 
 
 
static int PSString(GRAPHIC *graph, real x, real y, char *s)
  {
    int col = graph->fntCol;
    real xt, yt;

    if (graph->firstClip)
      Clipping(graph);

    if (col!=saveCol)
      {
	    fprintf(graph->file, " %.4f %.4f %.4f SC\n",
		    red[col], green[col], blue[col]);
		saveCol = col;
	  }

    if (graph->id == PS_QUER)
      {
	    xt = Q_TRANSX(x);
	    yt = Q_TRANSY(y);
	    if (xt<posMinX[graph->wdNo]) posMinX[graph->wdNo] = xt;
	    if (xt>posMaxX[graph->wdNo]) posMaxX[graph->wdNo] = xt;
	    if (yt<posMinY[graph->wdNo]) posMinY[graph->wdNo] = yt;
	    if (yt>posMaxY[graph->wdNo]) posMaxY[graph->wdNo] = yt;
        fprintf(graph->file, "%.3f %.3f M 90 R (%s) SH -90 R\n", yt, xt, s);
	  }
    else
      {
	    xt = TRANSX(x);
	    yt = TRANSY(y);
	    if (xt<posMinX[graph->wdNo]) posMinX[graph->wdNo] = xt;
	    if (xt>posMaxX[graph->wdNo]) posMaxX[graph->wdNo] = xt;
	    if (yt<posMinY[graph->wdNo]) posMinY[graph->wdNo] = yt;
	    if (yt>posMaxY[graph->wdNo]) posMaxY[graph->wdNo] = yt;
         fprintf(graph->file, "%.3f %.3f M (%s) SH\n", xt, yt, s);
	  }

    (graph->line_count) += 2;

    return true;
  }
 
 
 
 
static int PSFill(GRAPHIC *graph, void *xAdr, void *yAdr, int n)
  {
    int k, col = graph->fllCol;
    real x, y;
    
    if (graph->firstClip)
      Clipping(graph);

    if ((graph->prec) == SINGLE)
      {
	x = ((float*)xAdr)[0];
	y = ((float*)yAdr)[0];
      }
    else
      {
	x = ((double*)xAdr)[0];
	y = ((double*)yAdr)[0];
      }

    if (col!=saveCol)
      {
	    fprintf(graph->file, " %.4f %.4f %.4f SC\n",
		    red[col], green[col], blue[col]);
		saveCol = col;
	  }
    if (graph->id == PS_QUER)
      fprintf(graph->file, "NP %.3f %.3f M\n",
			   Q_TRANSY(y), Q_TRANSX(x));
    else
      fprintf(graph->file, "NP %.3f %.3f M\n",
			   TRANSX(x), TRANSY(y));

    for (k = 1; k < n; k++)
      {
        if ((graph->prec) == SINGLE)
          {
	    x = ((float*)xAdr)[k];
	    y = ((float*)yAdr)[k];
	  }
        else
          {
	    x = ((double*)xAdr)[k];
	    y = ((double*)yAdr)[k];
	  }
	if (graph->id == PS_QUER)
	  fprintf(graph->file, "%.3f %.3f L\n",
			       Q_TRANSY(y), Q_TRANSX(x));
	else
	  fprintf(graph->file, "%.3f %.3f L\n",
			       TRANSX(x), TRANSY(y));
      }
    fprintf(graph->file, "F\n");
    (graph->line_count) += n+2;

    return true;
  }






static int PSColor(int col_no, int rVal, int gVal, int bVal)
  {
     red[col_no] = rVal/255.0;
     green[col_no] = gVal/255.0;
     blue[col_no] = bVal/255.0;

     return true;
  }





static int PSSettings(GRAPHIC *graph, int type, void *valAdr)
  {
    int iVal, n;

    if (type == FILENAME)
      {
        if ((graph->file)!=nil) PSClose(graph);
		file[graph->wdNo] = false;
        n = strlen((char*)valAdr);
        graph->fileName = (char*)malloc(n+1);
        strncpy(graph->fileName, (char*)valAdr, n);
        (graph->fileName)[n] = '\0';
		return PSOpenPort(graph);
      }

    if (! graph->ready)
      if (!PSOpenPort(graph))
	return false; 

    switch (type)
    {
    case WDORGX :
      iVal = *((int*)valAdr);
      if ((iVal >= 0) && (iVal <= graph->right))
        graph->wdOrgX = iVal;
      else
	{
          fprintf(miniErrorFile,"MiniGraphic - zibset :  wrong value for WDORGX  %d !\n", 
		 iVal);
	  return false;
	}
      break;
      
    case WDORGY :
      iVal = *((int*)valAdr);
      if ((iVal >= 0) && (iVal <= graph->top))
        graph->wdOrgY = iVal;
      else
	{
          fprintf(miniErrorFile,"MiniGraphic - zibset :  wrong value for WDORGY  %d !\n", 
		 iVal);
	  return false;
	}
      break;
    
    case WDWDTH :
      iVal = *((int*)valAdr);
      if (graph->id == PS_QUER)
	{
	  if ((iVal >= 0) && (iVal + graph->wdOrgY <= graph->top))
	    {
	      graph->wdWdth = iVal;
	      break;
	    }
	}
      else
	if ((iVal >= 0) && (iVal + graph->wdOrgX <= graph->right))
	  {
            graph->wdWdth = iVal;
	    break;
	  }
      fprintf(miniErrorFile,"MiniGraphic - zibset :  wrong value for WDWDTH  %d !\n", iVal);
      return false;


    case WDHGHT :
      iVal = *((int*)valAdr);
      if (graph->id == PS_QUER)
	{
	  if ((iVal >= 0) && (graph->wdOrgX - iVal >= graph->left))
	    {
	      graph->wdHght = iVal;
	      break;
	    }
	}
      else
	if ((iVal >= 0) && (iVal + graph->wdOrgY <= graph->top))
	  {
	    graph->wdHght = iVal;
	    break;
	  }
      fprintf(miniErrorFile,"MiniGraphic - zibset :  wrong value for WDHGHT  %d !\n", iVal);
      return false;


    case MARKER:
      iVal = *((int*)valAdr);
      if ((iVal >= 0)&&(iVal < MAXPSMARKERS))
        graph->mark = iVal;
      else
	{
          fprintf(miniErrorFile,"MiniGraphic - zibset: unknown marker %d !\n", iVal);
	  return false;
	}
      break;
      
    case PENSIZE:
      iVal = *((int*)valAdr);
      if ((iVal >= SMALL) && (iVal <= BIG))
        {
          graph->drPenSz = iVal;
          fprintf(graph->file, "%.4f SL\n", 
                  	       lineWidth[graph->drPenSz]);
          (graph->line_count)++;
        }
      else
       {
         fprintf(miniErrorFile,"MiniGraphic - zibset :  wrong value for PENSIZE %d !\n", iVal);
	 return false;
       }
      break;
      
    case LINESTYLE:
      iVal = *((int*)valAdr);
      if ((iVal >= DOTTED) && (iVal <= SOLID))
       {
         graph->linestyle = iVal;
         fprintf(graph->file, "[%s] 0 SD\n", 
                  	      pattern[graph->linestyle]);
         (graph->line_count)++;
       }
      else
	{
          fprintf(miniErrorFile,"MiniGraphic - zibset :  wrong value for LINESTYLE %d !\n", 
	         iVal);
	  return false;
	}
      break;
      
    case FONTSIZE:
      iVal = *((int*)valAdr);
      if ((iVal >= SMALL) && (iVal <= BIG))
        {
          graph->drFntSz = iVal;
          fprintf(graph->file, "%.4f %s SF\n", 
          		       fontSize[graph->drFntSz], graph->fontName);
          (graph->line_count)++;
        }
      else
       {
        fprintf(miniErrorFile,"MiniGraphic - zibset :  wrong value for FONTSIZE %d !\n", iVal);
	return false;
       }
      break;

    case FILLCOL:
      iVal = *((int*)valAdr);
      if ((iVal >= 0) && (iVal < ALLCOL))
        graph->fllCol = iVal;
      else
	{
          fprintf(miniErrorFile,"MiniGraphic - zibset :  unknown gray %d !\n", iVal);
	  return false;
	}
      break;

    case PENCOL : iVal = *((int*)valAdr);
		  if ((iVal >= 0) && (iVal <= ALLCOL))
		    graph->penCol = iVal;
      		  else
		   {
          	     fprintf(miniErrorFile,"MiniGraphic - zibset :  unknown pencolor %d !\n", 
			    iVal);
	  	     return false;
		   }
      		   break;

    case FONTCOL : iVal = *((int*)valAdr);
		   if ((iVal >= 0) && (iVal <= ALLCOL))
		     graph->fntCol = iVal;
      		   else
		    {
          	      fprintf(miniErrorFile,"MiniGraphic - zibset :  unknown fontcolor %d !\n", 
			     iVal);
	  	      return false;
		    }
      		   break;

    case MARKCOL : iVal = *((int*)valAdr);
		   if ((iVal >= 0) && (iVal <= ALLCOL))
		     graph->mrkCol = iVal;
      		   else
		    {
          	      fprintf(miniErrorFile,"MiniGraphic - zibset :  unknown markercolor ");
		      fprintf(miniErrorFile,"%d !\n", iVal);
	  	      return false;
		    }
      		   break;

    case BACKGRCOL : iVal = *((int*)valAdr);
		     if ((iVal >= 0) && (iVal <= ALLCOL))
		       graph->backgrCol = iVal;
      		   else
		    {
          	      fprintf(miniErrorFile,"MiniGraphic - zibset :  unknown backgroundcolor ");
		      fprintf(miniErrorFile,"%d !\n", iVal);
	  	      return false;
		    }
      		   break;

    case FONTPROP :
    case CAPTION :
      return false;

    case BUFFER :
      fprintf(miniErrorFile,"MiniGraphic - zibset :  BUFFER only on X11 !\n");
      return false;

    default:
      fprintf(miniErrorFile,"MiniGraphic - zibset :  unkown type %d !\n", type);
      return false;
    }
    return true;
  } 




static int PSGin(GRAPHIC* NO(graph), int NO(geo),
                 void* NO(x1koordAdr), void* NO(y1koordAdr),
                 void* NO(x2koordAdr), void* NO(y2koordAdr))
 {
   return false;
 }



 
static int PSEvent(GRAPHIC* NO(graph), int* NO(typAdr), int* NO(buttonAdr),
                   void* NO(xkoordAdr), void* NO(ykoordAdr), int* NO(chAdr))
 {
   return false;
 }




static int PSWait(GRAPHIC* NO(graph), int* NO(typAdr), int* NO(buttonAdr),
                  void* NO(xkoordAdr), void* NO(ykoordAdr), int* NO(chAdr))
 {
   return false;
 }
 
 
  
  
   
int PSInit(GRAPHIC *graph, int type)
  {
    static int  first = true;
    int  i, free;
    
    if (first)
     {
       for (i = 0; i < MAX_FILES; i++)
         file[i] = true;
	   
       first = false;
     }

    for (free = 0; free < MAX_FILES; free++)
      if (file[free])  break;
    if (free == MAX_FILES)
     {
       fprintf(miniErrorFile,"MiniGraphic - zibwop :  More then %d postscript files !\n", 
	      MAX_FILES);
       return false;
     }
    file[free] = false;
    graph->fileName = psFName[free];
    posMinX[free] = posMinY[free] = 999.0;
    posMaxX[free] = posMaxY[free] = -1.0;

    graph->PLine    = PSPLine;
    graph->PMarker  = PSPMarker;
    graph->Text     = PSString;
    graph->Fill     = PSFill;
    graph->Color    = PSColor;
    graph->Settings = PSSettings;
    graph->NewPict  = PSNewPict;
    graph->OpenPort = PSOpenPort;
    graph->Close    = PSClose;
    graph->Gin      = PSGin;
    graph->Event    = PSEvent;
    graph->Wait     = PSWait;
    
    graph->bottom = 0.0;
    graph->left   = 0.0;
    graph->top    = 840.6;
    graph->right  = 593.8;
    
    switch (type)
      {
	case PS_QUER : 	graph->wdOrgX = 593.8-rightMargin;
			graph->wdOrgY = bottomMargin;
			graph->wdWdth = 840.6-topMargin-bottomMargin;
			graph->wdHght = 593.8-rightMargin-leftMargin;
			break;
	case PS_TEX :	graph->wdOrgX = 0.0;
			graph->wdOrgY = 0.0;
			graph->wdWdth = 593.8;
			graph->wdHght = 840.6;
			break;
	default :	graph->wdOrgX = leftMargin;
			graph->wdOrgY = bottomMargin;
			graph->wdWdth = 593.8-rightMargin-leftMargin;
			graph->wdHght = 840.6-topMargin-bottomMargin;
      }

    graph->drRes = 72.0/300.0;

    graph->drPenSz = SMALL;
    graph->linestyle = SOLID;
    graph->drFntSz = MEDIUM;
    graph->mark = STAR;

    graph->maxCol = COLORS;
    graph->maxGry = GRAYS;
    graph->penCol =
    graph->fllCol =
    graph->fntCol =
    graph->mrkCol = BLACK;
    graph->backgrCol = WHITE;

    graph->drXcm = PTTOCM;
    graph->drYcm = PTTOCM;

    graph->firstClip = true;
    graph->maxWd = MAX_FILES;
    graph->prec = SINGLE;
    graph->id = type;
    graph->wdNo = free;
    graph->fontName = fontName;
    graph->caption = captionName;
    graph->file = nil;
    for (i = 0; i < 16; i++)
       graph->Ass[i] = nil;

    return true;
  }
