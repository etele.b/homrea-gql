/* $Id$ */

#ifndef _MGGRAPH_
#define _MGGRAPH_
#include <stdio.h>

typedef int MGraph_Color;
/*
typedef struct
  {
	double red, green, blue;
  } RGBColor;
*/
enum MGraph_StdColors { UNDEF_COLOR=-1,
                        BLACK=0,       WHITE=1,       RED=2,
                        GREEN=3,       BLUE=4,        CYAN=5,
                        MAGENTA=6,     YELLOW=7
};
enum MGraph_ColType   {
                        PENCOL=8,      FONTCOL=9,     MARKCOL=10,
                        FILLCOL=11,    BACKCOL=39
};

enum MGraph_Size      { UNDEF_SIZE = -1,
                        SMALL=0,       MEDIUM=1,      BIG=2
};

enum MGraph_PenStyle  { UNDEF_PEN_STYLE = -1,
                        DOTTED=0,      LONG_DOTTED=1, DASHED=2,
                        LONG_DASHED=3, DASHDOT=4,     DASHDOTDOTTED=5,
                        SOLID=6
};

enum MGraph_Marker    {
                        STAR=1,        CROSS=2,       PLUS=3,
                        BULLET=4,      CIRCCROSS=5,   CIRCPLUS=6
};

enum MGraph_TextCopyMode
                      {
                        TRANSPARENT=0, OPAQUE=1
};

enum MGraph_CopyMode  {
                        GCOPY=0,       GXOR=1
};

enum MGraph_PSType    {
                        PS=1,          PS_QUER=6,     PS_TEX=7
};

class MGraph_Context
  {
public:
    virtual ~MGraph_Context() {};
    MGraph_Context(MGraph_Color initBackCol,
                   MGraph_Color initPenCol,
                   MGraph_Color initFontCol,
                   MGraph_Color initMarkCol,
                   MGraph_Color initFillCol);
    virtual int PLine(float *x,float *y, int n) = 0;
    virtual int PLine(double *x,double *y, int n) = 0;
    virtual int Fill(float *x,float *y, int n) = 0;
    virtual int Fill(double *x,double *y, int n) = 0;
    virtual int PMarker(float *x,float *y, int n, MGraph_Marker mark) = 0;
    virtual int PMarker(double *x,double *y, int n, MGraph_Marker mark) = 0;
    virtual int Text(double x,double y, char *s) = 0;
    virtual int SetColType(MGraph_ColType type, MGraph_Color val);
    virtual int SetPenSize(MGraph_Size size);
    virtual int SetTextSize(MGraph_Size size);
    virtual void ReportError(char *s);
    virtual void SetCC(MGraph_Context *cpy);
    virtual void Clear() = 0;
protected:
    MGraph_Color backCol, penCol, fontCol, markCol, fillCol;
    MGraph_Size penSize, textSize;
    MGraph_Context *cc;
  };

class MGraph_ScreenLocData;

class MGraph_ScreenContext : public MGraph_Context
  {
public:
	MGraph_ScreenLocData *impl;
	MGraph_ScreenContext(char *wTitle);
	~MGraph_ScreenContext();
    int PLine(float *x,float *y, int n);
    int PLine(double *x,double *y, int n);
    int Fill(float *x,float *y, int n);
    int Fill(double *x,double *y, int n);
    int PMarker(float *x,float *y, int n, MGraph_Marker mark);
    int PMarker(double *x,double *y, int n, MGraph_Marker mark);
    int Text(double x,double y, char *s);
    void Clear();

    char *GetTitle();
    void ChangeTitle(char *title);
protected:
private:
    char *title;
  };

class MGraph_PSLocData;

class MGraph_PSContext : public MGraph_Context
  {
public:
	MGraph_PSLocData *impl;
	MGraph_PSContext(char *fName, MGraph_PSType initType=PS);
	~MGraph_PSContext();
    int PLine(float *x,float *y, int n);
    int PLine(double *x,double *y, int n);
    int Fill(float *x,float *y, int n);
    int Fill(double *x,double *y, int n);
    int PMarker(float *x,float *y, int n, MGraph_Marker mark);
    int PMarker(double *x,double *y, int n, MGraph_Marker mark);
    int Text(double x,double y, char *s);
    void Clear();

    char *GetFileName();
protected:
private:
    char *fileName;
    MGraph_PSType type;
  };

#endif
