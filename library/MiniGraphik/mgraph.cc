#include <stdio.h>

#include "MGraph.h"

MGraph_Context::MGraph_Context(MGraph_Color initBackCol,
     MGraph_Color initPenCol,  MGraph_Color initFontCol,
     MGraph_Color initMarkCol, MGraph_Color initFillCol)

  : backCol(initBackCol), penCol(initPenCol), fontCol(initFontCol),
    markCol(initMarkCol), fillCol(initFillCol), cc(0),
    penSize(SMALL), textSize(MEDIUM)

  { }

int MGraph_Context::SetColType(MGraph_ColType type, MGraph_Color val)
  {
	MGraph_Color oldVal;

	switch (type)
	{
	  case PENCOL:
	    oldVal = penCol; penCol = val; break;
	  case FONTCOL:
	    oldVal = fontCol; fontCol = val; break;
	  case MARKCOL:
	    oldVal = markCol; markCol = val; break;
	  case FILLCOL:
	    oldVal = fillCol; fillCol = val; break;
	  case BACKCOL:
	    oldVal = backCol; backCol = val; break;
	  default:
	    ReportError("MGraph_Context::SetColType type not allowed\n");
	    break;
	} 
	return oldVal;
  }

int MGraph_Context::SetPenSize(MGraph_Size size)
  {
    MGraph_Size oldVal = penSize;
    penSize = size;
    return oldVal;
  }

int MGraph_Context::SetTextSize(MGraph_Size size)
  {
    MGraph_Size oldVal = textSize;
    textSize = size;
    return oldVal;
  }

void MGraph_Context::ReportError(char *s)
  {
	puts(s);
	return;
  }

void MGraph_Context::SetCC(MGraph_Context *cpy)
  {
    cc = cpy;
    return;
  }
