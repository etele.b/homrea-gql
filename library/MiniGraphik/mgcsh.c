/*
 $Id: mgcsh.c,v 1.1.1.1 1996/11/04 12:05:46 roitzsch Exp $
 (C)opyright 1996 by Konrad-Zuse-Center, Berlin
 All rights reserved.
 Part of the common environment

$Log: mgcsh.c,v $
Revision 1.1.1.1  1996/11/04 12:05:46  roitzsch
minigraph module

*/

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include "minigraph.h"

int c_zibpl(int id, int no, int ass, void *xAdr, void *yAdr, int n)
  {
     return F77NAME(zibpl,ZIBPL)(&id, &no, &ass, xAdr, yAdr, &n);
  }
 
int c_zibpm(int id, int no, int ass, void *xAdr, void *yAdr,int  n)
  {
     return F77NAME(zibpm,ZIBPM)(&id, &no, &ass, xAdr, yAdr, &n);
  }

int c_zibfl(int id, int no, int ass, void *xAdr, void *yAdr, int n)
  {
     return F77NAME(zibfl,ZIBFL)(&id, &no, &ass, xAdr, yAdr, &n);
  }

int c_zibtx(int id, int no, int ass, double x, double y, char  *text)
  {
    GRAPHIC *graph = firstGraph;
    float f_x, f_y;
    DeclStrPar(ftnPar);
    
    MakeStrPar(ftnPar,text,strlen(text));

    if (graph == nil)
      {
	    f_x = (float)x;
	    f_y = (float)y;
	    return F77NAME(zibtx,ZIBTX)(&id, &no, &ass, &f_x, &f_y,
	                                UseStrPar(ftnPar), UseStrParLng(ftnPar));
      }

    while (graph != nil)
      {
	if (id == (graph->id) || id == -1)
	  if (no == (graph->wdNo) || no == -1)
	    if (graph->prec == SINGLE)
	      {
	        f_x = (float)x;
	        f_y = (float)y;
	        return F77NAME(zibtx,ZIBTX)(&id, &no, &ass, &f_x, &f_y,
	                                    UseStrPar(ftnPar), UseStrParLng(ftnPar));
	      }
	    else
              return F77NAME(zibtx,ZIBTX)(&id, &no, &ass, (float*)&x, (float*)&y, 
	                                      UseStrPar(ftnPar), UseStrParLng(ftnPar));
	    graph = graph->next;
      }

    fprintf(miniErrorFile,"MiniGraphic - zibtx :  no such window  id = %d  no = %d !\n", 
	   id, no);

    return false;  
  }

int c_zibcol(int id, int col_no, int rVal, int gVal, int bVal)
  {
     return F77NAME(zibcol,ZIBCOL)(&id, &col_no, &rVal, &gVal, &bVal);
  }

int c_zibset(int id, int no, int ass, int type, ...)
  {
    va_list ap;
    char *valAdr;
    int iVal, valLng;
    float f_rVal;
    double rVal;
    GRAPHIC *graph = firstGraph;
    DeclStrPar(descr);
    va_start(ap,type);

    if ((type >= 1 && type <= 11) || (type >= 32 && type <= 35) 
         || type == 37 || (type >= 39 && type <= 43))
      {
        iVal = va_arg(ap, int);
        return F77NAME(zibset,ZIBSET)(&id, &no, &ass, &type, (FtnStrPar)&iVal, 0);
      }

    else if ((type >= 12 && type <= 17) || (type >= 20 && type <= 31))
	   {
	     rVal = va_arg(ap, double);
	     va_end(ap);
	     while (graph != nil)
      	       {
		 if ((graph->id) == id || id == -1)
	  	   if ((graph->wdNo) == no || no == -1)
	    	     if (graph->prec == DOUBLE)
	               return F77NAME(zibset,ZIBSET)(&id, &no, &ass, &type, (FtnStrPar)&rVal, 0);
		     else
		       {
		         f_rVal = (float)rVal;
	    		 return F77NAME(zibset,ZIBSET)(&id, &no, &ass, &type, (FtnStrPar)&f_rVal, 0);
		       }
	         graph = graph->next;
	       }
	     fprintf(miniErrorFile,"MiniGraphic - zibset :  no such window  ");
	     fprintf(miniErrorFile,"id = %d  no = %d !\n", id, no);
	     return false;
	   } 
     else if (type == 36 || type == 38)
	    {
		  valAdr = va_arg(ap, char *);
		  valLng = strlen(valAdr);
		  MakeStrPar(descr,valAdr,valLng);
		  return F77NAME(zibset,ZIBSET)(&id, &no, &ass, &type,
		                 (FtnStrPar)UseStrPar(descr), UseStrParLng(descr));
	    }
     else  
        fprintf(miniErrorFile,"MiniGraphic - zibset :  unknown type %d !\n", type);

     va_end(ap);

     return true;
  }

int c_zibreq(int id, int no, int type, void *valAdr)
  {
     return F77NAME(zibreq,ZIBREQ)(&id, &no, &type, valAdr);
  }

int c_zibclr(int id,int  no, int ass)
  {
     return F77NAME(zibclr,ZIBCLR)(&id, &no, &ass);
  }


int c_zibwop(int *idAdr, int *noAdr)
  {
     return F77NAME(zibwop,ZIBWOP)(idAdr, noAdr);
  }


int c_zibwcl(int id, int no, int ass)
  {
     return F77NAME(zibwcl,ZIBWCL)(&id, &no, &ass);
  }

int c_zibadd(int id, int no, int assid, int assno)
  {
     return F77NAME(zibadd,ZIBADD)(&id, &no, &assid, &assno);
  }

int c_zibsub(int id, int no, int assid, int assno)
  {
     return F77NAME(zibsub,ZIBSUB)(&id, &no, &assid, &assno);
  }

int c_zibgin(int id, int no, int geo, void *x1koordAdr, void *y1koordAdr, void *x2koordAdr, void *y2koordAdr)
  {
     return F77NAME(zibgin,ZIBGIN)(&id, &no, &geo, x1koordAdr, y1koordAdr, x2koordAdr, 
		    y2koordAdr);
  }

int c_zibev(int id, int no, int *typAdr, int *buttonAdr, void *xkoordAdr, void *ykoordAdr, int  *chAdr)
  {
     return F77NAME(zibev,ZIBEV)(&id, &no, typAdr,  buttonAdr, xkoordAdr, ykoordAdr, chAdr);
  }

int c_zibwt(int  id, int  no, int  *typAdr,  int  *buttonAdr, void  *xkoordAdr, void  *ykoordAdr, int  *chAdr)
  {
     return F77NAME(zibwt,ZIBWT)(&id, &no, typAdr,  buttonAdr, xkoordAdr, ykoordAdr, chAdr);
  }


int c_zibstr(int id, int no, char *string, int length)
  {
     DeclStrPar(ftnStr);
     MakeStrPar(ftnStr,string,length);
     return F77NAME(zibstr,ZIBSTR)(&id, &no, UseStrPar(ftnStr), UseStrParLng(ftnStr));
  }
