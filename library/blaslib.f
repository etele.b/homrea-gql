      DOUBLE PRECISION FUNCTION SCADIF(NEQ,V1,V2,ATOL)
C***********************************************************************
C                                                                      *
C     ************************************************************     *
C     *                                                          *     *
C     *                    NORM OF A VECTOR                      *     *
C     *                                                          *     *
C     ************************************************************     *
C                                                                      *
C***********************************************************************
C
C     INPUT:
C        NEQ      LENGHT OF VECTOR
C        V1(NEQ)  VECTOR
C        V2(NEQ)  VECTOR
C
C     OUTPUT:
C        SCADIF   SQRT( SUM( ((V2-V1)/MAX(DABS(V2),ATOL))**2 ))
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION V1(NEQ),V2(NEQ)
      SUM  = 0.0D0
      DO 20 I = 1,NEQ
20    SUM = SUM + ((V2(I)-V1(I))/DMAX1(ATOL,DABS(V2(I))))**2
      SCADIF = DSQRT(SUM)
      RETURN
C***********************************************************************
C     END OF FUNCTION SCADIF
C***********************************************************************
      END
      SUBROUTINE  SDCOPY(N,DX,INCX,DY,INCY)
C
C     COPIES A VECTOR, X, TO A VECTOR, Y.
C     USES UNROLLED LOOPS FOR INCREMENTS EQUAL TO ONE.
C     JACK DONGARRA, LINPACK, 3/11/78.
C
      REAL             DX(1)
      DOUBLE PRECISION DY(1)
      INTEGER I,INCX,INCY,IX,IY,M,MP1,N
C
      IF(N.LE.0)RETURN
      IF(INCX.EQ.1.AND.INCY.EQ.1)GO TO 20
C
C        CODE FOR UNEQUAL INCREMENTS OR EQUAL INCREMENTS
C          NOT EQUAL TO 1
C
      IX = 1
      IY = 1
      IF(INCX.LT.0)IX = (-N+1)*INCX + 1
      IF(INCY.LT.0)IY = (-N+1)*INCY + 1
      DO 10 I = 1,N
        DY(IY) = DX(IX)
        IX = IX + INCX
        IY = IY + INCY
   10 CONTINUE
      RETURN
C
C        CODE FOR BOTH INCREMENTS EQUAL TO 1
C
C
C        CLEAN-UP LOOP
C
   20 M = MOD(N,7)
      IF( M .EQ. 0 ) GO TO 40
      DO 30 I = 1,M
        DY(I) = DX(I)
   30 CONTINUE
      IF( N .LT. 7 ) RETURN
   40 MP1 = M + 1
      DO 50 I = MP1,N,7
        DY(I) = DX(I)
        DY(I + 1) = DX(I + 1)
        DY(I + 2) = DX(I + 2)
        DY(I + 3) = DX(I + 3)
        DY(I + 4) = DX(I + 4)
        DY(I + 5) = DX(I + 5)
        DY(I + 6) = DX(I + 6)
   50 CONTINUE
      RETURN
      END
      SUBROUTINE  DSCOPY(N,DX,INCX,DY,INCY)
C
C     COPIES A VECTOR, X, TO A VECTOR, Y.
C     USES UNROLLED LOOPS FOR INCREMENTS EQUAL TO ONE.
C     JACK DONGARRA, LINPACK, 3/11/78.
C
      DOUBLE PRECISION DX(1)
      REAL             DY(1)
      INTEGER I,INCX,INCY,IX,IY,M,MP1,N
C
      IF(N.LE.0)RETURN
      IF(INCX.EQ.1.AND.INCY.EQ.1)GO TO 20
C
C        CODE FOR UNEQUAL INCREMENTS OR EQUAL INCREMENTS
C          NOT EQUAL TO 1
C
      IX = 1
      IY = 1
      IF(INCX.LT.0)IX = (-N+1)*INCX + 1
      IF(INCY.LT.0)IY = (-N+1)*INCY + 1
      DO 10 I = 1,N
        DY(IY) = DX(IX)
        IX = IX + INCX
        IY = IY + INCY
   10 CONTINUE
      RETURN
C
C        CODE FOR BOTH INCREMENTS EQUAL TO 1
C
C
C        CLEAN-UP LOOP
C
   20 M = MOD(N,7)
      IF( M .EQ. 0 ) GO TO 40
      DO 30 I = 1,M
        DY(I) = DX(I)
   30 CONTINUE
      IF( N .LT. 7 ) RETURN
   40 MP1 = M + 1
      DO 50 I = MP1,N,7
        DY(I) = DX(I)
        DY(I + 1) = DX(I + 1)
        DY(I + 2) = DX(I + 2)
        DY(I + 3) = DX(I + 3)
        DY(I + 4) = DX(I + 4)
        DY(I + 5) = DX(I + 5)
        DY(I + 6) = DX(I + 6)
   50 CONTINUE
      RETURN
      END
      SUBROUTINE  SCAVEC(N,DX,DY,INCY)
C
C     COPIES A SCALAR, X, TO A VECTOR, Y.
C     USES UNROLLED LOOPS FOR INCREMENTS EQUAL TO ONE.
C
      DOUBLE PRECISION DX
      DOUBLE PRECISION DY(*)
      INTEGER I,INCY,IX,IY,M,MP1,N
C
      IF(N.LE.0)RETURN
      IF(INCY.EQ.1)GO TO 20
C
C        CODE FOR UNEQUAL INCREMENTS OR EQUAL INCREMENTS
C          NOT EQUAL TO 1
C
      IX = 1
      IY = 1
      IF(INCY.LT.0)IY = (-N+1)*INCY + 1
      DO 10 I = 1,N
        DY(IY) = DX
        IY = IY + INCY
   10 CONTINUE
      RETURN
C
C        CODE FOR BOTH INCREMENTS EQUAL TO 1
C
C
C        CLEAN-UP LOOP
C
   20 M = MOD(N,7)
      IF( M .EQ. 0 ) GO TO 40
      DO 30 I = 1,M
        DY(I) = DX
   30 CONTINUE
      IF( N .LT. 7 ) RETURN
   40 MP1 = M + 1
      DO 50 I = MP1,N,7
        DY(I) = DX
        DY(I + 1) = DX
        DY(I + 2) = DX
        DY(I + 3) = DX
        DY(I + 4) = DX
        DY(I + 5) = DX
        DY(I + 6) = DX
   50 CONTINUE
      RETURN
      END
      SUBROUTINE IINIVE(N,DX,DY,INCY)
C
C     COPIES A SCALAR, X, TO A VECTOR, Y.
C
      INTEGER DX
      INTEGER DY(*)
      INTEGER I,INCY,IX,IY,M,MP1,N
C
      IF(N.LE.0)RETURN
      IF(INCY.EQ.1)GO TO 20
C
C        CODE FOR UNEQUAL INCREMENTS OR EQUAL INCREMENTS
C          NOT EQUAL TO 1
C
      IX = 1
      IY = 1
      IF(INCY.LT.0)IY = (-N+1)*INCY + 1
      DO 10 I = 1,N
        DY(IY) = DX
        IY = IY + INCY
   10 CONTINUE
      RETURN
C
C        CODE FOR BOTH INCREMENTS EQUAL TO 1
C
C
C        CLEAN-UP LOOP
C
   20 M = MOD(N,7)
      IF( M .EQ. 0 ) GO TO 40
      DO 30 I = 1,M
        DY(I) = DX
   30 CONTINUE
      IF( N .LT. 7 ) RETURN
   40 MP1 = M + 1
      DO 50 I = MP1,N,7
        DY(I) = DX
        DY(I + 1) = DX
        DY(I + 2) = DX
        DY(I + 3) = DX
        DY(I + 4) = DX
        DY(I + 5) = DX
        DY(I + 6) = DX
   50 CONTINUE
      RETURN
      END
C***********************************************************************
CBLASBLASBLASBLASBLASBLASBLASBLASBLASBLASBLASBLASBLASBLASBLASBLASBLAS
C***********************************************************************
C
C     START OF THE BLOCK FOR BLAS ELEMENTARY VECTOR OPERATIONS
C
C     SEE:
C
C        DONGARRA J. J., DU CROZ J. J., HAMMARLING S.  AND HANSON R. J..
C        AN  EXTENDED  SET OF FORTRAN  BASIC LINEAR ALGEBRA SUBPROGRAMS.
C
C        TECHNICAL  MEMORANDA  NOS. 41 (REVISION 3) AND 81,  MATHEMATICS
C        AND  COMPUTER SCIENCE  DIVISION,  ARGONNE  NATIONAL LABORATORY,
C        9700 SOUTH CASS AVENUE, ARGONNE, ILLINOIS 60439, US.
C
C        OR
C
C        NAG  TECHNICAL REPORTS TR3/87 AND TR4/87,  NUMERICAL ALGORITHMS
C        GROUP  LTD.,  NAG  CENTRAL  OFFICE,  256  BANBURY  ROAD, OXFORD
C        OX2 7DE, UK,  AND  NUMERICAL ALGORITHMS GROUP INC.,  1101  31ST
C        STREET,  SUITE 100,  DOWNERS GROVE,  ILLINOIS 60515-1263,  USA.
C
C***********************************************************************
C***********************************************************************
C
C   DOUBLE PRECISION BLAS
C   =====================
C
C***********************************************************************
C
      DOUBLE PRECISION FUNCTION DROUND(DUMMY)
C-----------------------------------------------------------------------
C THIS ROUTINE COMPUTES THE UNIT ROUNDOFF OF THE MACHINE IN DOUBLE
C PRECISION.  THIS IS DEFINED AS THE SMALLEST POSITIVE MACHINE NUMBER
C U SUCH THAT  1.0D0 + U .NE. 1.0D0 (IN SINGLE PRECISION).
C-----------------------------------------------------------------------
      DOUBLE PRECISION U, COMP
      INTEGER DUMMY
      U = 1.0D0
 10   U = U*0.5D0
      COMP = 1.0D0 + U
      IF (COMP .NE. 1.0D0) GO TO 10
      DROUND = U*2.0D0
      RETURN
C----------------------- END OF FUNCTION DROUND ------------------------
      END
      REAL FUNCTION SROUND(DUMMY)
C-----------------------------------------------------------------------
C THIS ROUTINE COMPUTES THE UNIT ROUNDOFF OF THE MACHINE IN SINGLE
C PRECISION.  THIS IS DEFINED AS THE SMALLEST POSITIVE MACHINE NUMBER
C U SUCH THAT  1.0E0 + U .NE. 1.0E0 (IN SINGLE PRECISION).
C-----------------------------------------------------------------------
      REAL U, COMP
      INTEGER DUMMY
      U = 1.0E0
 10   U = U*0.5E0
      COMP = 1.0E0 + U
      IF (COMP .NE. 1.0E0) GO TO 10
      SROUND = U*2.0E0
      RETURN
C----------------------- END OF FUNCTION SROUND ------------------------
      END
      LOGICAL FUNCTION EQUSYM(N,SYMB1,SYMB2)
C**********************************************************************C
C                                                                      C
C    ***************************************************************   C
C    *                                                             *   C
C    *    THIS PROGRAM COMPARES THE FIRST N CHARACTER OF THE TWO   *   C
C    *    SYMBOLS SYM1 AND SYM2 REGARDLESS OF CASE.                *   C
C    *                 AUTHOR: U. MAAS                             *   C
C    *                         19.08.1993                          *   C
C    *              LAST CHANGE: 19.08.1993 BY U. MAAS             *   C
C    *                                                             *   C
C    ***************************************************************   C
C                                                                      C
C                                                                      C
C**********************************************************************C
C                                                                      C
C     INPUT  :                                                         C
C                                                                      C
C     SYMB1         = STRING 1                                         C
C     SYMB2         = STRING 2                                         C
C     N             = NUMBER OF CHARACTERS TO BE COMPARED              C
C                                                                      C
C     OUTPUT :                                                         C
C                                                                      C
C     EQUSYM        = .TRUE. IF FIRST N CHARACTERS ARE THE SAME        C
C                     .FALSE. OTHERWISE                                C
C                                                                      C
C                                                                      C
C**********************************************************************C
C                                                                      C
C                                                                      C
C                                                                      C
C**********************************************************************C
C     STORAGE ORGANIZATION                                             C
C**********************************************************************C
      LOGICAL LSAME
C     CHARACTER*1 SYMB1(N),SYMB2(N)
C     CHARACTER*1 SYMB1(N),SYMB2(N)
      CHARACTER(N) SYMB1,SYMB2
C**********************************************************************C
C     LOOP OVER CHARACTERS                                             C
C**********************************************************************C
      EQUSYM = .TRUE.
      DO 10 I = 1,N
C     EQUSYM = EQUSYM .AND. LSAME(SYMB1(I),SYMB2(I))
      EQUSYM = EQUSYM .AND. LSAME(SYMB1(I:I),SYMB2(I:I))
   10 CONTINUE
      RETURN
C**********************************************************************C
C     END OF EQUSYM                                                    C
C**********************************************************************C
      END
      integer function idmax(n,dx,incx)
c
c     finds the index of element having max. value.
c
      double precision dx(*),dmax
      integer i,incx,ix,n
c
      idmax = 0
      if( n .lt. 1 ) return
      idmax = 1
      if(n.eq.1)return
      if(incx.eq.1)go to 20
c
c        code for increment not equal to 1
c
      ix = 1
      dmax = dx(1)
      ix = ix + incx
      do 10 i = 2,n
         if(dx(ix).le.dmax) go to 5
         idmax = i
         dmax = dx(ix)
    5    ix = ix + incx
   10 continue
      return
c
c        code for increment equal to 1
c
   20 dmax = dx(1)
      do 30 i = 2,n
         if(dx(i).le.dmax) go to 30
         idmax = i
         dmax = dx(i)
   30 continue
      return
      end
      integer function idmin(n,dx,incx)
c
c     finds the index of element having min. value.
c
      double precision dx(*),dmin
      integer i,incx,ix,n
c
      idmin = 0
      if( n .lt. 1 ) return
      idmin = 1
      if(n.eq.1)return
      if(incx.eq.1)go to 20
c
c        code for increment not equal to 1
c
      ix = 1
      dmin = dx(1)
      ix = ix + incx
      do 10 i = 2,n
         if(dx(ix).ge.dmin) go to 5
         idmin = i
         dmin = dx(ix)
    5    ix = ix + incx
   10 continue
      return
c
c        code for increment equal to 1
c
   20 dmin = dx(1)
      do 30 i = 2,n
         if(dx(i).ge.dmin) go to 30
         idmin = i
         dmin = dx(i)
   30 continue
      return
      end
      double precision function dsum(n,dx,incx)
c
c     takes the sum of the values.
c     u.maas, from dasum by jack dongarra, linpack, 3/11/78.
c
      double precision dx(*),dtemp
      integer i,incx,m,mp1,n,nincx
c
      dsum = 0.0d0
      dtemp = 0.0d0
      if(n.le.0)return
      if(incx.eq.1)go to 20
c
c        code for increment not equal to 1
c
      nincx = n*incx
      do 10 i = 1,nincx,incx
        dtemp = dtemp + dx(i)
   10 continue
      dsum = dtemp
      return
c
c        code for increment equal to 1
c
c
c        clean-up loop
c
   20 m = mod(n,6)
      if( m .eq. 0 ) go to 40
      do 30 i = 1,m
        dtemp = dtemp + dx(i)
   30 continue
      if( n .lt. 6 ) go to 60
   40 mp1 = m + 1
      do 50 i = mp1,n,6
        dtemp = dtemp + dx(i) + dx(i + 1) + dx(i + 2)
     *  + dx(i + 3) + dx(i + 4) + dx(i + 5)
   50 continue
   60 dsum = dtemp
      return
      end
      SUBROUTINE DRMMRM(L,M,N,A,B,C,INFO)
C***********************************************************************
C                                                                      *
C     *************************************************************
C     *                                                           *
C     *         MULTIPLICATION OF TWO DOUBLE PREC. MATRICES       *
C     *                                                           *
C     *                                                           *
C     *                                                           *
C     *************************************************************
C
C
C          INFO >  0      {X} = {X} +  {A}  * {B}
C
C          INFO =  0      {X} =        {A}  * {B}
C
C          INFO <  0      {X} = {X} -  {A}  * {B}
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION A(L,M),B(M,N),C(L,N)
      IF(INFO) 10,20,30
   10 CONTINUE
      DO 11 I=1,L
      DO 11 J=1,N
      DO 11 K=1,M
      C(I,J)=C(I,J)-A(I,K)*B(K,J)
   11 CONTINUE
      GOTO 100
   20 CONTINUE
      DO 21 I=1,L
      DO 21 J=1,N
      C(I,J)=0
      DO 21 K=1,M
      C(I,J)=C(I,J)+A(I,K)*B(K,J)
   21 CONTINUE
      GOTO 100
   30 CONTINUE
      DO 31 I=1,L
      DO 31 J=1,N
      DO 31 K=1,M
      C(I,J)=C(I,J)+A(I,K)*B(K,J)
   31 CONTINUE
      GOTO 100
  100 CONTINUE
      RETURN
C***********************************************************************
C     END OF -DRMMRM-
C***********************************************************************
      END
      SUBROUTINE DIAMAT(N,ALPHA,A,LDA)
C***********************************************************************
C***********************************************************************
C
C  Purpose
C  =======
C
C  DIAMAT forms an n-by-n diagonal matrix A from the vector ALPHA
C
C  Arguments
C  =========
C
C  N       (input) INTEGER
C          The number of columns of the matrix A.  N >= 0.
C
C  ALPHA   (input) DOUBLE PRECISION
C          The vector to which the diagonal elements are to be set.
C
C  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
C          On exit, the matrix of A is set as follows:
C
C           A(i,j) = ZERO  for i.NE.j
C           A(i,i) = ALPHA(i) 
C
C  LDA     (input) INTEGER
C          The leading dimension of the array A.  LDA >= max(1,M).
C
C***********************************************************************
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION ALPHA(*),A(LDA,*)
      PARAMETER (ZERO = 0.0D0)
C***********************************************************************
C     Set the matrix to ZERO  
C***********************************************************************
      DO 10 I = 1, N
      DO 10 J = 1, N
      A(I,J) = ZERO  
   10 CONTINUE
C***********************************************************************
C     Set the diagonal elements to ALPHA 
C***********************************************************************
      DO 20 I = 1,N
      A(I,I) = ALPHA(I)
   20 CONTINUE
C***********************************************************************
C     Exit
C***********************************************************************
      RETURN
      END
      SUBROUTINE DIFNOR(NEQ,V1,LDV1,V2,LDV2,ATOL,SCADIF,SCAMAX,ISCA,
     1 UNSDIF,UNSMAX,IUNS)
C***********************************************************************
C                                                                      *
C***********************************************************************
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION V1(LDV1),V2(LDV2)
      SCADIF  = 0.0D0
      UNSDIF  = 0.0D0
      UNSMAX  = 0.0D0
      SCAMAX  = 0.0D0
      DO 20 I = 1,NEQ
      UNSDEL = ABS(V2(I)-V1(I))
      SCADEL = UNSDEL /MAX(ATOL,ABS(V2(I)))
      UNSDIF = UNSDIF + UNSDEL**2
      SCADIF = SCADIF + SCADEL**2
      IF(SCAMAX.LT.SCADEL) THEN
        ISCA= I
        SCAMAX = SCADEL
      ENDIF
      IF(UNSMAX.LT.UNSDEL) THEN
        IUNS= I
        UNSMAX = UNSDEL
      ENDIF
20    CONTINUE
      SCADIF = SQRT(SCADIF/FLOAT(NEQ))
      UNSDIF = SQRT(UNSDIF/FLOAT(NEQ))
      RETURN
C***********************************************************************
C    
C***********************************************************************
      END
