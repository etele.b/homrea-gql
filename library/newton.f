      SUBROUTINE NEWTON(N,FCN,T,Y,RTOL,ATOL,
     1                  IJOB,NRW,RW,NIW,IW,RPAR,IPAR,LOUT)
C***********************************************************************
C                                                                      *
C     ************************************************************     *
C     *                                                          *     *
C     *       NEWTON SOLVER FOR NOLINEAR EQUATION SYSTEMS        *     *
C     *                                                          *     *
C     *                  AUTHOR: ULRICH MAAS                     *     *
C     *                                                          *     *
C     *              LAST CHANGE:  2.10.1987/MAAS                *     *
C     *                                                          *     *
C     ************************************************************     *
C                                                                      *
C                                                                      *
C***********************************************************************
C
C  EXTERNAL SUBROUTINES (TO BE SUPPLIED BY THE USER):
C  --------------------------------------------------
C
C    FCN(N,#,*,Y,F,*,#,#,RPAR,IPAR)
C                RIGHT-HAND-SIDE F(T,Y) OF THE SYSTEM
C       N       = NUMBER OF DEPENDENT VARIABLES
C       Y       = DEPENDENT VARIABLES ARRAY
C       F       = RESIDUAL OF EQUATIONS
C       RPAR(1) = REAL WORK SPACE FOR USER COMMUUNICATION WITH FCN
C       IPAR(1) = INTEGER WORK SPACE FOR USER COMMUUNICATION WITH FCN
C                   (RPAR AND IPAR ARE NOT ALTERED BY -NEWTON-
C       SUPPLY INTEGER DUMMIES FOR # AND REAL*8 DUMMIES FOR *
C
C    JAC (N,M,T,Y,A,RPAR,IPAR)
C              ANALYTIC JACOBIAN OF THE RIGHT-HAND-SIDE FUNCTIONS F(T,Y)
C              (PASS A DUMMY-NAME, IF NUMERICAL DIFFERENCE APPROXIMATION
C               OF THE JACOBIAN IS WANTED), SEE SUBROUTINE JACOB FOR
C               FURTHER REFERENCES
C
C  INPUT PARAMETERS (* MARKS TRANSIENT PARAMETERS):
C  ------------------------------------------------
C
C  * T                  DUMMY NAME
C  * Y(N)               INITIAL VALUES Y(1),...,Y(N)
C    RTOL               PRESCRIBED RELATIVE PRECISION (.GT.0)
C    ATOL               PRESCRIBED ABSOLUTE PRECISION (.GT.0)
C    IJOB               INTEGER VECTOR, CONTROLLING THE EXECUTION OF
C                       THE JOB
C   * IJOB(1)           PERFORMANCE STATISTICS
C                       =0: NO OUTPUT
C                       =1: STANDARD OUTPUT
C                       =2: ADDITIONALLY INTEGRATION MONITOR
C                       =3: ADDITIONALLY ENHANCED INFORMATION
C                       =4: ADDITIONALLY ITERATION MONITOR ( OF ITERA-
C                           TIVE REALIZATION OF DISCRETIZATION )
C     IJOB(2)           =0: NUMERICAL DIFFERENCE-APPROXIMATION OF THE
C                           JACOBIAN OF THE RIGHT-HAND-SIDE F(T,Y)
C                           INTERNALLY GENERATED
C                       =1: ANALYTIC JACOBIAN SUPPLIED BY THE USER
C
C     IJOB(3)           INFORMATION ABOUT JACOBIAN
C                                                                      *
C          SET IJOB(3)=0 FOR DENSE JACOBIAN                            *
C          SET IJOB(3)=1 FOR BANDED JACOBIAN                           *
C          SET IJOB(3)=2 FOR BLOCK TRIDIAGONAL JACOBIAN                *
C          SET IJOB(3)=3 FOR BLOCK PENTADIAGONAL JACOBIAN              *
C          SET IJOB(3)=4 FOR BLOCK NONADIAGONAL JACOBIAN               *
C                                                                      *
C                                                                      *
C       IJOB(4-6) INFORMATIONS ABOUT SIZE OF MATRIX                    *
C  IF IJOB(3)=1 SET IJOB(4) = LOWER BAND WIDTH                         *
C               SET IJOB(5) = UPPER BAND WIDTH                         *
C  IF IJOB(3)=2 SET IJOB(4) = DIMENSION OF SUBBLOCKS                   *
C               SET IJOB(5) = NUMBER OF SUBBLOCKS                      *
C  IF IJOB(3)=3 SET IJOB(4) = DIMENSION OF SUBSUBBLOCK                 *
C               SET IJOB(5) = NUMBER OF SUBSUBBLOCKS                   *
C               SET IJOB(6) = NUMBER OF SUBBLOCKS                      *
C  IF IJOB(3)=4 SET IJOB(4) = DIMENSION OF SUBSUBBLOCK                 *
C               SET IJOB(5) = NUMBER OF SUBSUBBLOCKS                   *
C               SET IJOB(6) = NUMBER OF SUBBLOCKS                      *
C                                                                      *
C    NRW        DIMENSION OF REAL WORK-SPACE (INTERNALLY CHECKED)      *
C    RW         REAL WORK-SPACE                                        *
C    NIW        DIMENSION OF INTEGER WORK-SPACE                        *
C    IW         INTEGER WORK-SPACE                                     *
C    RPAR       REAL WORK ARRAY FOR USER COMMUNICATION WITH -FCN-      *
C    IPAR       INTEGER WORK ARRAY FOR USER COMMUNICATION WITH -FCN-   *
C
C***********************************************************************
C
C
C  OUTPUT PARAMETERS:
C  ------------------
C
C    Y(N)               FINAL VALUES
C    IJOB(1)    .GE. 0: SUCCESSFUL INTEGRATION
C                       (IJOB(1) NOT ALTERED INTERNALLY)
C               .LT. 0: ERROR CODE AFTER A FAIL RUN OF NEWSOL
C                  =-1: JACOBIAN MATRIX IS SINGULAR
C                  =-2: MORE THAN JRMAX REDUCTIONS OF DAMPING FACTOR
C                  =-3: METHOD HAS NOT CONVERGED AFTER MAXIT ITERATIONS
C                  =-8: REAL OR INTEGER WORK-SPACE IS EXHAUSTED
C
C  OUTPUT AFTER A SUCCESSFULLY COMPLETED TASK ONLY:
C  ------------------------------------------------
C
C    IJOB( 8)      =    NUMBER OF PERFORMED INTEGRATION STEPS
C    IJOB( 9)      =    NUMBER OF FCN-EVALUATIONS
C    IJOB(11)      =    NUMBER OF FORWARD-BACKWARD-SUBSTITUTIONS
C
C***********************************************************************
C
C  THE NUMERICAL SOLUTION OF THE ARISING LINEAR EQUATIONS IS DONE BY
C  MEANS OF THE LINEAR ALGEBRA LIBRARY LINSOL
C
C***********************************************************************
C***********************************************************************
C     CHAPTER I.: STORAGE ORGANIZATION
C***********************************************************************
      IMPLICIT  DOUBLE PRECISION(A-H,O-Z)
      EXTERNAL  FCN,JAC
      DIMENSION Y(N),RW(NRW),RPAR(1)
      DIMENSION IW(NIW),IJOB(20),IMAT(10),IPAR(1)
C***********************************************************************
C     CHAPTER III.: BUILD UP INFORMATION ARRAY FOR MATRIX
C***********************************************************************
      IPRIR = IJOB(1)
      IF(IJOB(3).EQ.0) GOTO 40
      IF(IJOB(3).EQ.1) GOTO 50
      IF(IJOB(3).EQ.2) GOTO 60
      IF(IJOB(3).EQ.3) GOTO 70
      IF(IJOB(3).EQ.4) GOTO 80
C---- ARRAYS FOR DENSE JACOBIAN
   40 CONTINUE
      NDJ=N*N
      NDI=N*N
      IMAT(1)=0
      IMAT(2)=N
      IMAT(3)=N
      IMAT(4)=N
      IMAT(5)=N
      IMAT(7)=NDJ
      IMAT(8)=NDI
      GOTO 90
C---- ARRAYS FOR BANDED JACOBIAN
   50 CONTINUE
      ML=IJOB(4)
      MU=IJOB(5)
      MBH=2*ML+MU+1
      NDJ=N*MBH
      NDI=N*MBH
      IMAT(1)=1
      IMAT(2)=N
      IMAT(3)=ML
      IMAT(4)=MU
      IMAT(5)=MBH
      IMAT(6)=MBH
      IMAT(7)=NDJ
      IMAT(8)=NDI
      GOTO 90
C---- ARRAYS FOR BLOCK TRIDIAGONAL JACOBIAN
   60 CONTINUE
      ND0=IJOB(4)
      ND1=IJOB(5)
      NDJ=  3*ND1*ND0*ND0
      NDI=  3*ND1*ND0*ND0
      IMAT(1)=2
      IMAT(2)=N
      IMAT(3)=ND0
      IMAT(4)=ND1
      IMAT(5)=N
      IMAT(7)=NDJ
      IMAT(8)=NDI
      GOTO 90
C---- ARRAYS FOR BLOCK PENTA DIAGONAL JACOBIAN
   70 CONTINUE
      ND0=IJOB(4)
      ND1=IJOB(5)
      ND2=IJOB(6)
      ND2M=ND2-1
      IF(ND2.EQ.1) ND2M=1
      NDJ = ND0*ND0* (3*ND1*ND2 + 2*ND1*(ND2M))
      NDI = ND0*ND0* (3*ND1*ND2 + 2*ND1*(ND2M))
      IMAT(1)=3
      IMAT(2)=N
      IMAT(3)=ND0
      IMAT(4)=ND1
      IMAT(5)=ND2
      IMAT(7)=NDJ
      IMAT(8)=NDI
      GOTO 90
C---- ARRAYS FOR BLOCK NONA DIAGONAL JACOBIAN
   80 CONTINUE
      ND0=IJOB(4)
      ND1=IJOB(5)
      ND2=IJOB(6)
      ND2M=ND2-1
      IF(ND2.EQ.1) ND2M=1
      NDJ = ND0*ND0* (3*ND1*ND2 + 3*2*ND1*ND2M)
      NDI = ND0*ND0* (3*ND1*ND2 + 3*2*ND1*ND2M)
      IMAT(1)=4
      IMAT(2)=N
      IMAT(3)=ND0
      IMAT(4)=ND1
      IMAT(5)=ND2
      IMAT(7)=NDJ
      IMAT(8)=NDI
      GOTO 90
C***********************************************************************
C     CHAPTER IV.: BUILD UP POINTERS FOR WORK ARRAYS AND CHECK SPACE
C***********************************************************************
   90 CONTINUE
C---- COMPUTE POSITIONS IN INTEGER WORK ARRAYS
      I1  =  1
C---- INDEX OF LAST INTEGER ELEMENT
      I2  = I1 + N
      I3  = I2 + N
      MINIW=I3-1
      LEFTIW=NIW-MINIW
C---- COMPUTE POSITIONS IN REAL WORK ARRAYS
      N1  =  1
      N2  = N1 + NDJ
      N3  = N2 + NDI
      N4  = N3 + N
      N5  = N4 + N
      N6  = N5 + N
      N7  = N6 + N
      N8  = N7 + N
      N9  = N8 + N
      N10 = N9 + N
      N11 = N10+ N
      N12 = N11+ N
      N13 = N12+ N
C---- INDEX OF LAST REAL*8 ELEMENT
      MINRW=N13-1
      LEFTRW=NRW-MINRW
C***********************************************************************
C     CHECK FOR SUFFICIENT WORK AREA
C***********************************************************************
      IF(IJOB(1).GT.0) WRITE(LOUT,810) MINRW,MINIW
      IF(MINRW.GE.NRW .OR. MINIW.GE.NIW) GOTO 910
C***********************************************************************
C
C     CHAPTER V.: CALL OF CORE INTEGRATOR NEWIT1
C
C***********************************************************************
      CALL NEWIT1(N,NDJ,NDI,FCN,JAC,T,Y,
     1          RTOL,ATOL,HMAX,H,IJOB,IMAT,
     1          IW(1),IW(2),
     2          RW(N1),RW(N2),RW(N3),RW(N4),
     4          RW(N6),RW(N7),RW(N8),RW(N9),RW(N10),RW(N11),RW(N12),
     5          RPAR,IPAR,LOUT,LEFTIW,IW(I3),LEFTRW,RW(N13))
C***********************************************************************
C     CHECK RETURN CODE
C***********************************************************************
      IF(IJOB(1).GE.0) RETURN
      GOTO 999
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
C---- WORK SPACE EXHAUSTED
  910 CONTINUE
      IJOB(1)=-8
      IF(MINIW.GT.NIW) WRITE(LOUT,911) MINIW,NIW
  911 FORMAT(1X,' +++++ INTEGER WORK-SPACE EXHAUSTED +++++',/,
     1       1X,' GIVEN SPACE:',I6,'     NEEDED SPACE:',I6)
      IF(MINRW.GT.NRW) WRITE(LOUT,912) MINRW,NRW
  912 FORMAT(1X,' +++++ REAL WORK-SPACE EXHAUSTED +++++',/,
     1       1X,' GIVEN SPACE:',I6,'     NEEDED SPACE:',I6)
      GOTO 999
C---- INTEGRATION FAILED
  999 CONTINUE
      IF(IPRIR.GT.0) WRITE(LOUT,998) IJOB(1)
  998 FORMAT(/1X,' ### ERROR CODE FROM NEWSOL :',I3,' ###',/)
      RETURN
C***********************************************************************
C     FORMAT STATEMENTS
C***********************************************************************
  810 FORMAT(/1X,' **********************************',/,
     1        1X,' *  MINIMAL REQUIRED WORK-SPACE:  *',/,
     2        1X,' *    REAL ARRAY     RW(',I6,')   *',/,
     3        1X,' *    INTEGER ARRAY  IW(',I6,')   *',/,
     4        1X,' **********************************',/ )
C***********************************************************************
C     END OF DRIVER ROUTINE NEWSOL
C***********************************************************************
      END
      SUBROUTINE NEWIT1(N,NDJ,NDI,FCN,JAC,T,Y,
     1       RTOL,ATOL,HMAX,H,IJOB,IMAT,IPIVOT,IFCNP,A,B,
     3       YM,DEL,DEL0,SM,ETA,W1,W2,W3,W4,RPAR,IPAR,LOUT,
     3       LIW,IWORK,LRW,RWORK)
C***********************************************************************
C
C      CORE SOLVER FOR NON- LINEAR EQUATION SYSTEMS
C
C***********************************************************************
C***********************************************************************
C
C     STORAGE ORGANIZATION
C
C***********************************************************************
C***********************************************************************
C     TYPES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      EXTERNAL FCN,JAC
C***********************************************************************
C     DIMENSIONS
C***********************************************************************
      DIMENSION Y(N),YM(N),SM(N),ETA(N),DEL(N),DEL0(N),
     1          W1(N),W2(N),W3(N),W4(N),RPAR(1),IPAR(1)
      DIMENSION A(NDJ),B(NDI)
      DIMENSION RWORK(LRW),IWORK(LIW)
      DIMENSION IPIVOT(N),IFCNP(N),IJOB(20),IMAT(10)
C---- THE ITEMS BELOW ARE DUMMIES
      DIMENSION BV(1),IRV(1),ICV(1)
C***********************************************************************
C     COMMON BLOCKS
C***********************************************************************
C***********************************************************************
C     DATA STATEMENTS
C***********************************************************************
      DATA EPMIN/1.D-10/,ETADIF/1.D-6/,FMIN/1.D-2/,
     1     ONE/1.D0/,ZERO/0.D0/
C  10* RELATIVE MACHINE PRECISION
      DATA EPMACH/1.D-10/
C***********************************************************************
C                                                                      *
C     INITIAL PREPARATIONS                                             *
C                                                                      *
C***********************************************************************
      DO 11 I=1,N
   11 IFCNP(I) = 0
      IMAT(9) = 0
   60 CONTINUE
      JOB=0
      EPDIFF=DSQRT(EPMACH)
      NFCN=0
      NFCNJ=0
      NDEC=0
      NSOL=0
      INIT=0
      MAXIT=150
      JRMAX=30
      NUMIT=0
      NJAC =0
      ALPHA0=1.D0
      ALPHAM=1.D-3
      ICORRM=2
C***********************************************************************
C
C     INITIAL PREPARATIONS
C
C***********************************************************************
  100 CONTINUE
      DO 110 I=1,N
      ETA(I) = ETADIF
      YM(I)  = Y(I)
  110 CONTINUE
      IF(IJOB(1).GE.1) WRITE(LOUT,8010) RTOL,ATOL
      IF(IJOB(1).GE.1) WRITE(LOUT,8020) (Y(I),I=1,N)
C***********************************************************************
C                                                                      *
C     BEGIN OF ITERATION LOOP
C                                                                      *
C***********************************************************************
 1000 CONTINUE
      IF(IJOB(1).GE.1) WRITE(LOUT,8030)  NUMIT,NFCN,NJAC,NFCNJ,NDEC,NSOL
      NUMIT=NUMIT+1
      IF(NUMIT.GT.MAXIT) GOTO 930
C***********************************************************************
C     CALCULATE SCALING FACTORS
C***********************************************************************
      CALL SMSET(N,RTOL,ATOL,YM,SM)
C---- IFCNP(NB1,NB2) = 0
      DO 1105 I=1,N
      IFCNP(I) = 0
 1105 CONTINUE
C***********************************************************************
C     CALCULATE RESIDUAL (UNPERTURBED)
C***********************************************************************
                                     IFCN= 0
CULI  IF(IJOB(7).GE.1.OR.NUMIT.EQ.1) IFCN=-1
      IFCN = -6
      IF(                NUMIT.EQ.1) IFCN=-1
      CALL RESLIM(N,0,1,T,YM,W1,DEL0,BV,IRV,ICV,W4,FCN,RPAR,IPAR,
     1           IFCN,IFCNP)
      IF(IFCN.GE.9) GOTO 960
      IFCN=0
      HK0 = OVNORM(N,DEL0)
      IF(IJOB(1).GE.1) WRITE(LOUT,8040) HK0
      NFCN=NFCN+1
C***********************************************************************
C
C     CALCULATION OF THE JACOBIAN (SCALED)                             *
C
C***********************************************************************
      NJAC=NJAC+1
      IANA=IJOB(2)
C***********************************************************************
C     NUMERICAL DIFFERENCE APPROXIMATION OF JACOBIAN
C***********************************************************************
      IFCN =  1
      IF(IJOB(7).GE.3) IFCN=-1
      CALL JACOB(IANA,IMAT,FCN,JAC,N,T,YM,W4,DEL0,
     1        SM,ETADIF,ETA,EPDIFF,EPMIN,FMIN,
     1        DEL,W1,W2,W3,0,1,BV,IRV,ICV,
     2        NFCNJ,NDJ,A,RPAR,IPAR,IFCN,IFCNP)
      IF(IFCN.GE.9) GOTO 960
      IFCN = 0
      G = 1.0D0
      NZV= 0
      NZC= 0
      NDV= 1
      NDC= 1
      CALL ITMAT(IMAT,N,G,NDJ,A,SM,
     1           NZV,NDV,BV,IRV,ICV,NZC,NDC,BV,IRV,ICV,NDI,B)
C***********************************************************************
C     SCALING OF F(Y0) = DEL0
C***********************************************************************
 1400 CONTINUE
      DO 1410 K=1,N
 1410 DEL0(K)=DEL0(K)/SM(K)
C***********************************************************************
C  COMPUTE ORDINARY CORRECTION VECTOR
C***********************************************************************
      IFAIL=0
      ITML=20
      CALL LINSOL(0,IMAT,N,ATOL,RTOL,ITML,W1,W2,
     1            LRW,RWORK,LIW,IWORK,IPIVOT,NDI,B,DEL0,IFAIL)
      IF((IMAT(1).GE.3).AND.IJOB(1).GE.2)  WRITE(LOUT,8050) -IFAIL
      NDEC=NDEC+1
      IF(IFAIL.GT.0)  GOTO 900
C***********************************************************************
C     DESCALE ORDINARY CORRECTION (DEL0) AND COMPUTE NORM (COR0)
C***********************************************************************
      DO 1610  I=1,N
      DEL0(I) = DEL0(I)*SM(I)
 1610 CONTINUE
      COR0 = WVNORM(N,DEL0,SM)
      IF(IJOB(1).GE.1) WRITE(LOUT,8060) COR0
C***********************************************************************
C
C     CORRECT Y AND TEST NORM OF NEW CORRECTIONS
C
C***********************************************************************
      JJJ= 0
 2000 JJJ=JJJ+1
      IF(JJJ.GT.JRMAX) GOTO 920
      ALPHA=ONE/FLOAT(2**(JJJ-1))
C***********************************************************************
C     COMPUTE NEW ITERATES
C***********************************************************************
      DO 2010  I=1,N
      W3(I) = YM(I)+ALPHA * DEL0(I)
 2010 CONTINUE
C-new
c--- this may lead to errors
      IF(1.GT.2.AND.COR0.LT.ATOL) THEN
      DO 2012  I=1,N
      YM(I) = W3(I)
 2012 CONTINUE
c     write(6,*) 'jumped'
      GOTO 5000
      ENDIF
C***********************************************************************
C     COMPUTE NEW RESIDUALS DEL AND THEIR NORMS HKJ
C***********************************************************************
      IFCN = 0
CULI  IF(IJOB(7).GE.2) IFCN=-1
                       IFCN=-7
      CALL RESLIM(N,0,1,T,W3,W1,DEL,BV,IRV,ICV,W4,FCN,RPAR,IPAR,
     1         IFCN,IFCNP)
      IF(IFCN.GE.9) THEN
         IFCN = 0
!         write(6,*) ' ggggggg'
         GOTO 2000
      ENDIF
      IFCN = 0
      HKJ = OVNORM(N,DEL)
      IF(IJOB(1).GE.1) WRITE(LOUT,8070) HKJ
C***********************************************************************
C     COMPUTE CORRECTION VECTORS
C***********************************************************************
      DO 2110  I=1,N
      DEL(I) = DEL(I)/SM(I)
 2110 CONTINUE
      IFAIL=0
      CALL LINSOL(1,IMAT,N,ATOL,RTOL,ITML,W1,W2,
     1     LRW,RWORK,LIW,IWORK,IPIVOT,NDI,B,DEL,IFAIL)
      IF((IMAT(1).GE.3).AND.IJOB(1).GE.2)  WRITE(LOUT,8050) IFAIL
      IF(IFAIL.GT.0)  GOTO 900
      DO 2130  I=1,N
      DEL(I) = DEL(I)*SM(I)
 2130 CONTINUE
C***********************************************************************
C     COMPUTE NORM OF NEW CORRECTION VECTOR
C***********************************************************************
      CORJ  = WVNORM(N,DEL,SM)
      THETA = CORJ/COR0
      IF(IJOB(1).GE.1) WRITE(LOUT,8080) CORJ
      IF(IJOB(1).GE.1) WRITE(LOUT,8090) THETA
C     IF(HKJ.GT.1.0D0*HK0.OR.THETA.GT.ONE) GOTO 2000
      IF(THETA.GT.ONE.AND.ALPHA.GT.1.D-3) GOTO 2000
C     IF(HKJ.GT.0.0D0*HK0) GOTO 2000
C***********************************************************************
C     STORE STEP
C***********************************************************************
      CORO = CORJ
      DO 2310  I=1,N
 2310 YM(I)=W3(I)
      IF(IJOB(1).GE.1)  WRITE(LOUT,8110) JJJ
      IF(IJOB(1).GE.2)  WRITE(LOUT,8100) (YM(I),I=1,N)
C***********************************************************************
C     TERMINATE
C***********************************************************************
C---- CRITERION : SCALED NORM OF ORDINARY CORRECTION LE RTOL
C                 AND ALPHA = 1.0
CQUI  IF(COR0.LE.RTOL.AND.(DABS(ALPHA-1.0D0).LE.EPMACH)) GOTO 5000
      IF(  (COR0.LE.RTOL.AND.(DABS(ALPHA-1.0D0).LE.EPMACH))
     1   .AND. HK0 .LE.RTOL )
     2 GOTO 5000
      GOTO 1000
C***********************************************************************
C     TERMINATE
C***********************************************************************
 5000 CONTINUE
      DO 5010 I=1,N
 5010 Y(I)=YM(I)
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
  900 CONTINUE
      IF(IJOB(1).GE.1) WRITE(LOUT,901)
  901 FORMAT(1H ,' JACOBIAN MATRIX IS SINGULAR ')
      IJOB(1)=-1
      GOTO 9999
  920 CONTINUE
      IF(IJOB(1).GE.1) WRITE(LOUT,921) JJJ,JRMAX
  921 FORMAT(1H ,' NUMBER OF REDUCTIONS OF DAMPING FACTOR (',I2,
     1  ') EXCEEDS MAXIMUM (',I2,')')
      IJOB(1)=-2
      GOTO 9999
  930 CONTINUE
      IF(IJOB(1).GE.1) WRITE(LOUT,931) MAXIT
  931 FORMAT(1H ,' SOLUTION NOT OBTAINED AFTER ',I4,' ITERATIONS')
      IJOB(1)=-3
      GOTO 9999
  960 CONTINUE
      IF(IJOB(1).GE.1) WRITE(LOUT,961)
  961 FORMAT(1H ,' TERMINATION DUE TO ERRORS IN USER SUPPLIED ROUTINE ')
      IJOB(1)=-9
      GOTO 9999
9999  H=ZERO
      RETURN
C***********************************************************************
C     FORMAT STATEMENTS
C***********************************************************************
C---- NEW CALLS TO NEWSOL
 8010 FORMAT(2X,70('-'),/,3X,
     1 'RELATIVE TOLERANCE ',1PE10.3,3X,'ABSOLUTE TOLERANCE',1PE10.3)
 8020 FORMAT(/,1X,22X,' ****** VALUES BEFORE ITERATION ****** ',
     1 /,1(6(1X,1PE10.3)))
C---- OUTPUT BEFORE BASIC INTEGRATION STEP
 8030 FORMAT(/1X,' ****************  STATISTICS  ****************',/,
     2        1X,' ****  NUMBER OF ITERATIONS        :',I6,' ****',/,
     3        1X,' ****  FUNCTION EVALUATIONS        :',I6,' ****',/,
     4        1X,' ****  JACOBIAN EVALUATIONS        :',I6,' ****',/,
     4        1X,' ****  FUNCTION EVAL. FOR JACOBIAN :',I6,' ****',/,
     5        1X,' ****  DECOMPOSITIONS              :',I6,' ****',/,
     5        1X,' ****  SUBSTITUTIONS               :',I6,' ****',/,
     6        1X,' **********************************************',/)
 8040 FORMAT(/,1X,'   NORM OF RESIDUAL            (HK0)  :',1PE10.3)
 8060 FORMAT(  1X,'   NORM OF ORDINARY CORRECTION (COR0) :',1PE10.3)
 8070 FORMAT(  1X,'   NORM OF RESIDUAL            (HKJ)  :',1PE10.3)
 8080 FORMAT(  1X,'   NORM OF J-TH CORRECTION     (CORJ) :',1PE10.3)
 8090 FORMAT(  1X,'   RATIO OF CORRECTIONS        (THETA):',1PE10.3)
 8100 FORMAT(/,/,1X,19X,' ****** CURRENT VALUES OF SOLUTION ****** ',
     1 /,1(6(1X,1PE10.3)))
 8110 FORMAT(/,1X,'  SOLUTION ACCEPTED IN LOOP NO.:',I3)
 8050 FORMAT(1H ,I4,' ITERATIONS NEEDED IN GS- METHOD')
C***********************************************************************
C     END OF CORE INTEGRATOR NEWIT1
C***********************************************************************
      END
      SUBROUTINE SMSET(NEQ,RTOL,ATOL,Y,WT)
C***********************************************************************
C     THIS SUBROUTINE SETS THE SCALING VECTOR
C     WT ACCORDING TO WT(I)=MAX(ABS(Y(I)),ATOL(I))
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION Y(NEQ),WT(NEQ)
      DO 20 I=1,NEQ
      WT(I)=DMAX1(DABS(Y(I)),ATOL)
20    CONTINUE
      RETURN
C***********************************************************************
C     END OF SUBROUTINE SMSET
C***********************************************************************
      END
